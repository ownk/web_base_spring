package com.devglan.springbootgoogleoauth.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.devglan.springbootgoogleoauth.model.User;

@Service(value = "userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {



    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    	System.out.println(email);
        User user = new User();
        user.setEmail("juan.jerez@ownk.co");
        user.setPassword("password");
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority());
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
    }
}
