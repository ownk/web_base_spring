/**
 * ==========================================================================
 * Librerias principal de core.
 * ==========================================================================
 */
// ------------------------------------------------
var core_TIMERID = 0;
var core_TIMERFUNCTION = new Array();
var core_TIMERPARAM_1 = new Array();
var core_TIMERPARAM_2 = new Array();
var core_TIMERPARAM_3 = new Array();
var core_TIMERPARAM_4 = new Array();
var core_TIMERPARAM_5 = new Array();

function core_encode(str) {
	var result = "";
	for ( var i = 0; i < str.length; i++) {
		result += str.charCodeAt(i).toString(16);
	}
	return result;
}

function core_executetimerfunction(id) {
	try {
		var _FF = core_TIMERFUNCTION[id];
		_FF(core_TIMERPARAM_1[id], core_TIMERPARAM_2[id], core_TIMERPARAM_3[id], core_TIMERPARAM_4[id], core_TIMERPARAM_5[id]);
	} catch (_EH) {

		SimpleLogger.setError(" [ core_executetimerfunction: " + _EH + " ]", null);
	}
}

function core_timer(func, interval, params) {
	core_TIMERFUNCTION[core_TIMERID] = func;
	core_TIMERPARAM_1[core_TIMERID] = null;
	core_TIMERPARAM_2[core_TIMERID] = null;
	core_TIMERPARAM_3[core_TIMERID] = null;
	core_TIMERPARAM_4[core_TIMERID] = null;
	core_TIMERPARAM_5[core_TIMERID] = null;

	try {
		core_TIMERPARAM_1[core_TIMERID] = params[0];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_2[core_TIMERID] = params[1];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_3[core_TIMERID] = params[2];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_4[core_TIMERID] = params[3];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_5[core_TIMERID] = params[4];
	} catch (e) {
	}

	window.setInterval("core_executetimerfunction(" + core_TIMERID + ")", interval);
	core_TIMERID++;
}

function core_timeout(func, interval, params) {
	core_TIMERFUNCTION[core_TIMERID] = func;
	core_TIMERPARAM_1[core_TIMERID] = null;
	core_TIMERPARAM_2[core_TIMERID] = null;
	core_TIMERPARAM_3[core_TIMERID] = null;
	core_TIMERPARAM_4[core_TIMERID] = null;
	core_TIMERPARAM_5[core_TIMERID] = null;

	try {
		core_TIMERPARAM_1[core_TIMERID] = params[0];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_2[core_TIMERID] = params[1];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_3[core_TIMERID] = params[2];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_4[core_TIMERID] = params[3];
	} catch (e) {
	}
	try {
		core_TIMERPARAM_5[core_TIMERID] = params[4];
	} catch (e) {
	}

	window.setTimeout("core_executetimerfunction(" + core_TIMERID + ")", interval);
	core_TIMERID++;
}

// ------------------------------------------------

var core_NEXT_ID = 0;
var core_LIST_ID = new Array();

var core_NEXT_FUNCTION_ID = 0;
var core_LIST_FUNCTION = new Array();

function core_listen(event, elem, func, cancelreturnvalue) {

	var _elem = null;

	try {
		_elem = document.getElementById(elem);
	} catch (_EN) {
	}

	if (_elem != null) {
		elem = _elem;
	}

	try {

		if (elem == window || elem == document || elem.tagName) {
			if (!elem.WITHOSMID) {
				elem.WITHOSMID = true;
				core_NEXT_ID++;
				elem.OSMID = core_NEXT_ID;
				core_LIST_ID[core_NEXT_ID] = elem;
			}

			core_NEXT_FUNCTION_ID++;
			core_LIST_FUNCTION[core_NEXT_FUNCTION_ID] = func;

			var cancel = "false";

			if (cancelreturnvalue) {
				cancel = "true";
			}

			var functionbody = "if(!event){event = window.event;} if(!event.target){ event.target = event.srcElement; } event.event_type = '" + event + "'; var rfunc = core_LIST_FUNCTION[" + core_NEXT_FUNCTION_ID + "]; var robj = core_LIST_ID[" + elem.OSMID + "]; try{ if (event.wheelDelta){ event.delta = event.wheelDelta / 120;} else if (event.detail ){ event.delta = -event.detail / 3; } }catch(E){}; robj.tmposmf = rfunc; var rtrans = robj.tmposmf(event, robj); if(rtrans==undefined){ rtrans = true;}; if(!rtrans || " + cancel + "){ if (event.preventDefault){ event.preventDefault(); }; event.returnValue = false; } ";

			var nfunc = new Function("event", functionbody);

			if (elem.addEventListener) {
				if (event == "mousewheel") {
					event = "DOMMouseScroll";
				}

				elem.addEventListener(event, nfunc, false);

			} else if (elem.attachEvent) {
				elem.attachEvent("on" + event, nfunc);
			}
		}
	} catch (_EN) {
	}
}

// ------------------------------------------------
// Funciones de bloqueo de ventana

function core_block_window() {
	try {
		core_ocultarSelects("bodyContent");
		core_setVisible("block_window", true, true);
	} catch (e) {
	}
}

function core_unblock_window() {
	try {
		core_mostrarSelects("bodyContent");
		core_setVisible("block_window", false);
		core_ocultarLoader();
	} catch (e) {
	}
}

// ------------------------------------------------
// Funciones de ventana

function core_getWindowSize() {
	var myWidth = 0, myHeight = 0;
	if (typeof (window.innerWidth) == 'number') {
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}

	return [ myWidth, myHeight ];
}

// ------------------------------------------------

function core_getElementPosition(obj) {
	var curleft = curtop = 0;

	if (obj.offsetParent) {
		do {
			curleft += obj.offsetLeft - obj.scrollLeft;
			curtop += obj.offsetTop - obj.scrollTop;
		} while ((obj = obj.offsetParent));
	}

	return [ curleft, curtop ];
}

// ------------------------------------------------
// Funciones de navegacion

function core_go(url, block) {
	if (block == undefined || block) {
		core_verLoader();
		core_block_window();
	}

	var des = GLOBAL_CONTEXTPATH + "/" + url;

	window.setTimeout("location.href = '" + des + "';");
}

// ------------------------------------------------
// Funciones Finacieras

function core_pago(monto, interes, nper) {
	core_pago_in = Math.pow(1 + interes, nper);

	core_pago_cuota = monto * (interes * core_pago_in) / (core_pago_in - 1);

	return core_pago_cuota;
}

function core_formatoMoneda(num, currency) {

	if (num == undefined || num == null) {
		return "";
	}

	if (currency == undefined) {
		currency = "$";
	}
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.5000000000000000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10)
		cents = "0" + cents;
	for ( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
		num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
	return (((sign) ? '' : '-') + currency + num + '.' + cents);
}
// ------------------------------------------------
// funciones html

var PLANTILLAS = new Array();

function core_construirHTML(id_padre, id_plantilla, parametros, antes) {

	var htmlplantilla = PLANTILLAS[id_plantilla];
	if (htmlplantilla == null) {
		htmlplantilla = core_getValue(id_plantilla);
		PLANTILLAS[id_plantilla] = core_getValue(id_plantilla);
		$("#" + id_plantilla).empty();
	}

	if (parametros && parametros != null) {
		var tags = core_getKeys(parametros);

		for ( var j = 0; j < tags.length; j++) {
			var parametro;
			var tag = tags[j];
			if (parametros[tag] == null) {
				parametro = '';
			} else {
				parametro = parametros[tag];
			}
			var remplazarTag = tag;
			if (!isNaN(parseInt(tag, 10))) {
				remplazarTag = (parseInt(tag, 10) + 1);
				remplazarTag = '[ ' + (remplazarTag) + ' ]';
			}
			htmlplantilla = core_remplazar(htmlplantilla, remplazarTag, parametro);
		}
	}
	if (antes) {
		$(core_getObjeto(id_padre)).prepend(htmlplantilla);
	} else {
		$(core_getObjeto(id_padre)).append(htmlplantilla);
	}
}

function core_getKeys(array) {

	var keys = [];
	for ( var i in array)
		if (array.hasOwnProperty(i)) {
			keys.push(i);
		}
	return keys;
}

// ------------------------------------------------
// Funciones Para fecha
// Retorna la fecha en formato DD/MM/YY (aplica para los valores retornados en java)

function core_getFecha(milliseconds) {
	//
	if (milliseconds == null) {
		return "";
	}
	var date = new Date(milliseconds * 1000);
	var dia = core_completarTiempo(date.getDate());
	var mes = core_completarTiempo(date.getMonth() + 1);
	return dia + "/" + mes + "/" + date.getFullYear();

}
function core_completarTiempo(str) {
	if (str < 10) {
		str = "0" + str;
	}
	return str;
}

/**
 * Retorna la hora en formato string, por defecto se retorna en formato am/pm.
 * 
 * @param milliseconds -
 *            milisegundo java
 * @param formato_militar -
 *            si es verdadero se retorna la hora en formato militar
 * @returns
 */
function core_getHora(milliseconds, formato_militar) {
	//
	if (milliseconds == null) {
		return "";
	}
	var date = new Date(milliseconds * 1000);

	var hour = date.getHours();
	var mins = date.getMinutes();
	var secs = date.getSeconds();
	if (!formato_militar) {
		var ap = "AM";
		if (hour > 11) {
			ap = "PM";
		}
		if (hour > 12) {
			hour = hour - 12;
		}
		if (hour == 0) {
			hour = 12;
		}
	}

	return core_completarTiempo(hour) + ":" + core_completarTiempo(mins) + ":" + core_completarTiempo(secs) + " " + ap;

}

// --------------------------
function core_getObjetoFecha(fecha) {

	var objDate = new Date();
	try {
		objDate = new Date(fecha.substring(6, 10), parseInt(fecha.substring(3, 5), 10) - 1, fecha.substring(0, 2), 0, 0, 0, 0)
	} catch (e) {
	}

	return objDate;
}
// ------------------------------------------------
// funciones de String

function core_remplazar(orig, oold, nnew) {

	try {
		var cc = orig;

		while (cc.indexOf(oold) >= 0) {
			cc = cc.replace(oold, nnew);
		}

		return cc;
	} catch (e) {
		return orig;
	}
}

function core_remplazarRegex(orig, oold, nnew) {
	// TODO revizar, no reemplazar
	try {
		var cc = orig;
		var regx = new RegExp(oold, 'g');

		while (cc.indexOf(oold) >= 0) {
			cc = cc.replace(regx, nnew);
		}

		return cc;
	} catch (e) {
		return orig;
	}
}

function core_regexTest(str, regex) {
	var rex = new RegExp(regex);
	var res = str.match(rex);
	if (res) {
		return true;
	} else {
		return false;
	}
}

function core_validarCorreo(correo) {
	try {
		return jsonrpc.pcore.validarCorreo(correo);
	} catch (e) {
		SimpleLogger.setError("Error en servicio json validarCorreo", e);
	}
	// Si fall� la validaci�n por jsonrpx se hace por expresi�n regular
	return core_regexTest(correo, /^[\w\_\-\.]+@([\w\_\-]+\.)+[\w\_\-]{2,4}$/);
}
function core_validarNumero(cadena) {
	var redigit = /^[\d]+$/;
	return core_regexTest(cadena, redigit);
}

function core_validarDecimal(cadena, places) {
	if (places == undefined) {
		var redigit = /^[\d]+(\.[\d]*)?$/;
	} else {
		var redigit = new RegExp("^[\\d]+(\\.[\\d]{0," + places + "})?$");
	}
	return core_regexTest(cadena, redigit);
}

function core_validar2Decimal(cadena) {
	var redigit = /^[\d]+(\.[\d]?[\d]?)?$/;
	return core_regexTest(cadena, redigit);
}

function core_validarIP(cadena) {
	// var redigit = /^[0-255]{1,3}\.[0-255]{1,3}\.[0-255]{1,3}\.[0-255]{1,3}$/;
	var redigit = /(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	return core_regexTest(cadena, redigit);
}

/**
 * 
 * @param cadena
 *            String
 * @param numeros
 *            boolean
 * @param caracteresEspeciales
 *            boolean
 * @returns resultadoValidacion boolean
 * 
 * Los caracteres especiales validados son: punto, ap�strofe, gui�n y espacio
 */
function core_validarCadenaCaracteres(cadena, numeros, caracteresEspeciales) {
	var realfa = /^[\w]+$/;
	var realfanumerico = /^[A-Za-z0-9]+$/;
	var realfanumSpecials = /^[A-Za-z0-9������������\.\'\- ]+$/;
	var realfaSpecials = /^[A-Za-z������������\.\'\- ]+$/;
	if (numeros && !caracteresEspeciales) {
		return core_regexTest(cadena, realfanumerico);
	} else if (!numeros && caracteresEspeciales) {
		return core_regexTest(cadena, realfaSpecials);
	} else if (!numeros && !caracteresEspeciales) {
		return core_regexTest(cadena, realfa);
	} else if (numeros && caracteresEspeciales) {
		return core_regexTest(cadena, realfanumSpecials);
	}
}

function core_validarAlfabetico(cadena) {
	var alfabetico = /^[A-Za-z������������\.\'\- ]+$/;
	return core_regexTest(cadena, alfabetico);
}

function core_validarAlfanumerico(cadena) {
	var alfabetico = /^[A-Za-z������������0-9\.\'\- ]+$/;
	return core_regexTest(cadena, alfabetico);
}

function core_validarTelefono(cadena) {
	var telefonalfanumerico = /[0-9]+/;
	return core_regexTest(cadena, telefonalfanumerico);
}

/**
 * Valida el formato de la fecha dd/mm/yyyy
 * 
 * @param fecha
 *            String
 * @return boolean
 */
function core_validarFormatoFecha(fecha) {
	// regular expression to match required date format
	var regexdate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
	return core_regexTest(fecha, regexdate);
}

function core_escape_html(txt) {

	txt = core_remplazar(txt, '"', '-');
	txt = core_remplazar(txt, '\'', '-');
	txt = core_remplazar(txt, '>', '-');
	txt = core_remplazar(txt, '<', '-');

	return txt;
}

function core_strlimpiar(val) {
	if (core_esVacio(val)) {
		return "";
	}

	return core_trim(val);
}

/**
 * Limpia los espacios en blanco al final y al principio. Si es null retorna cadena vac�a.
 * 
 * @param cadena
 * @returns
 */
function core_trimToEmpty(cadena) {
	if (core_esVacio(cadena)) {
		return "";
	}
	return core_trim(cadena);
}

/**
 * Limpia los espacios en blanco al final y al principio.
 * 
 * @param cadena
 * @returns
 */
function core_trim(cadena) {

	if (cadena == null) {
		return null;
	}

	cadena = core_remplazar(cadena, "&nbsp;", " ");

	for ( var i = 0; i < cadena.length;) {
		if (cadena.charAt(i) == " ")
			cadena = cadena.substring(i + 1, cadena.length);
		else
			break;
	}

	for (i = cadena.length - 1; i >= 0; i = cadena.length - 1) {
		if (cadena.charAt(i) == " ")
			cadena = cadena.substring(0, i);
		else
			break;
	}

	return cadena;
}

function core_esVacio(str) {

	str = core_trim(str);
	try {

		if (str == null || str.toLowerCase() == "null") {
			return true;
		}

		var cstr = "A" + str;
		if (cstr.length == 1) {
			return true
		}
	} catch (_e) {
	}

	return false;
}

function core_existeElemento(id) {

	var obj = core_getObjeto(id);

	if (obj == null) {
		return false;
	}
	return true;
}

// ------------------------------------------------
// deprecated
function core_esSeleccionado(idd) {

	try {
		var radioObj = document.getElementById(idd);
		return radioObj.checked;
	} catch (_e) {
	}

	return false;
}

function core_seleccionarTexto(idd, tiempo) {

	var obj = core_getObjeto(idd);

	if (obj == null || (obj.disabled != undefined && obj.disabled)) {
		return false;
	}

	if (tiempo) {
		window.setTimeout("core_seleccionarTexto('" + idd + "')", tiempo);
	} else {

		try {
			window.setTimeout(" try{ var _oobj = document.getElementById('" + idd + "'); _oobj.focus(); _oobj.select(); }catch(_EE){ }", 1);
		} catch (_E) {
		}
	}

	return true;

}

function core_seleccionar(idd, vval) {
	try {
		var _oobj = document.getElementById(idd);

		try {
			_oobj.select();
		} catch (e) {
		}
		try {
			_oobj.setChecked(vval);
		} catch (e) {
		}

		_oobj.checked = vval;
	} catch (E_) {
	}
}

function core_setVisible(idd, vv, isblock) {
	try {
		var _oobj = document.getElementById(idd);
		if (vv) {
			_oobj.style.display = "";
			if (isblock) {
				_oobj.style.display = "block";
			}
			_oobj.esvisible = true;
		} else {
			_oobj.style.display = "none";
			_oobj.esvisible = false;
		}

	} catch (E_) {
	}
}

function core_setVisibilidad(idd, vv) {
	try {
		var _oobj = document.getElementById(idd);
		if (vv) {
			_oobj.style.visibility = "visible";
		} else {
			_oobj.style.visibility = "hidden";
		}

	} catch (E_) {
	}
}

function core_ocultarSelects(idpadre) {
	
	if (navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE 6.0") > 0) {
		
		var padre = core_getObjeto(idpadre);
		if (padre != null) {
			var eleme = padre.getElementsByTagName("SELECT");
			var ii = 0;
			for (ii = 0; ii < eleme.length; ii++) {
				var oselect = eleme[ii];
				oselect.style.visibility = "hidden";
			}
		}
	}
}

function core_mostrarSelects(idpadre) {
	var padre = core_getObjeto(idpadre);
	if (padre != null) {
		var eleme = padre.getElementsByTagName("SELECT");
		var ii = 0;

		for (ii = 0; ii < eleme.length; ii++) {
			var oselect = eleme[ii];
			oselect.style.visibility = "visible";
		}
	}
}

function core_verLoader() {
	core_setVisible("block_window_loader", true, true);
}

function core_ocultarLoader() {
	core_setVisible("block_window_loader", false);
}
// -------------

function core_sendForm(idd) {
	try {
		var _oobj = document.getElementById(idd);
		core_verLoader();
		core_block_window();
		_oobj.submit();
	} catch (_e) {
		SimpleLogger.setError(" [ core_sendForm: " + _e + "]", null);
		core_unblock_window();
	}
}

function core_setDestinoFormulario(idd, destino) {
	try {
		var _oobj = document.getElementById(idd);
		_oobj.action = destino;
	} catch (_e) {
	}
}

function core_getObjeto(idd) {
	var _oobj = document.getElementById(idd);
	return _oobj;
}

function core_esPadre(id_padre, obj_hijo) {

	try {
		while (obj_hijo.tagName != "BODY" && obj_hijo.id != id_padre) {
			obj_hijo = obj_hijo.parentNode;
		}

		if (obj_hijo.id == id_padre) {
			return true;
		}

	} catch (_e) {
	}

	return false;
}

function core_getObjetoVacio(idd) {
	var _oobj = document.getElementById(idd);

	if (_oobj.tagName == "DIV" || _oobj.tagName == "P" || _oobj.tagName == "TD" || _oobj.tagName == "H1" || _oobj.tagName == "H2" || _oobj.tagName == "H3") {
		_oobj.innerHTML = "";
	}

	if (_oobj.tagName == "INPUT" || _oobj.tagName == "SELECT") {
		_oobj.value = "";
	}

	if (_oobj.tagName == "SELECT") {
		while (_oobj.length > 0) {
			_oobj.remove(_oobj.length - 1);
		}
	}

	return _oobj;
}

function core_eliminarObjeto(idd) {

	try {
		var obj = core_getObjeto(idd);
		var parentObject = obj.parentNode;
		parentObject.removeChild(obj);
	} catch (_e) {
	}
}

function core_setFoco(idd, tiempo) {

	if (tiempo) {
		window.setTimeout("core_setFoco('" + idd + "')", tiempo);
	} else {

		try {
			window.setTimeout(" try{ var _oobj = document.getElementById('" + idd + "'); _oobj.focus(); }catch(_EE){}", 1);
		} catch (_E) {
		}
	}

}

function core_mover(idorigen, id_destino, tiempo) {

	if (tiempo) {
		window.setTimeout("core_mover('" + idorigen + "', '" + id_destino + "')", tiempo);
	} else {
		try {
			var origen = document.getElementById(idorigen);
			var destino = document.getElementById(id_destino);

			destino.appendChild(origen);

		} catch (e) {
		}
	}
}

function core_setClassname(idd, cn) {
	try {
		var _oobj = document.getElementById(idd);
		_oobj.className = cn;
	} catch (_EE) {
	}
}

function core_noEditable(ids) {
	try {
		var _oobj = document.getElementById(ids);
		_oobj.disabled = true;
	} catch (E_) {
	}
}

function core_editable(ids) {
	try {
		var _oobj = document.getElementById(ids);
		_oobj.disabled = false;
	} catch (E_) {
	}
}

function core_alert(msg) {

	core_block_window();

	alert(msg);

	core_unblock_window();
}
// -------------

function core_getValue(idd) {
	try {

		var _oobj = document.getElementById(idd);

		if (_oobj.tagName == "INPUT" || _oobj.tagName == "SELECT" || _oobj.tagName == "TEXTAREA") {
			return _oobj.value;
		}

		if (_oobj.tagName == "IMG") {
			return _oobj.src;
		}

		return _oobj.innerHTML;

	} catch (E_) {
	}

	return null;
}

function core_getValueText(idd) {
	var valor = core_getValue(idd);

	try {

		var _oobj = document.getElementById(idd);

		if (_oobj.tagName == "SELECT") {
			return _oobj.options[_oobj.selectedIndex].text;
		}

		if (_oobj.tagName == "OPTION") {
			return _oobj.text;
		}

	} catch (E_) {
	}

	return valor;
}

// -------------

function core_getValueFlotante(idd) {
	var cc = parseFloat(core_getValue(idd));
	if (isNaN(cc)) {
		cc = null;
	}
	return cc;
}

// -------------

function core_getValueEntero(idd) {
	var cc = parseInt(core_getValue(idd), 10);
	if (isNaN(cc)) {
		cc = 0;
	}
	return cc;
}

// valor de 0 a 5
function core_setTransparencia(obj, value) {

	try {
		var testObj = document.getElementById(obj);

		if (testObj == null) {
			testObj = obj;
		}

		testObj.style.opacity = value / 5;
		testObj.style.filter = 'alpha(opacity=' + (value * 20) + ')';

	} catch (e) {
	}
}

// -------------

function core_getMarca(idd) {
	try {
		var _oobj = document.getElementById(idd);
		if (_oobj.checked) {
			return true;
		}
	} catch (E_) {
	}

	try {
		if (idd.checked) {
			return true;
		}
	} catch (E_) {
	}

	return false;
}

// -------------

function core_setMarca(idd, vval) {
	try {
		var _oobj = document.getElementById(idd);

		_oobj.setChecked(vval);
		_oobj.checked = vval;
	} catch (E_) {
		_oobj.checked = vval;
	}
}

// -------------

var core_VALOR = null;

/**
 * Modifica el valor del objeto html
 * 
 * @param idd -
 *            id del objeto html
 * @param vval -
 *            nuevo valor del objeto
 */
function core_setValue(idd, vval) {

	core_VALOR = vval;

	$(core_getObjeto(idd)).each(function(i) {
		setValueObj(this);
	});

}

/**
 * Funcion interna para modificar el valor de un objeto
 * 
 * @param _oobj
 */
function setValueObj(_oobj) {

	if (_oobj.tagName == "INPUT" || _oobj.tagName == "SELECT" || _oobj.tagName == "TEXTAREA") {
		_oobj.value = core_VALOR;
	} else {
		if (_oobj.tagName == "IMG") {
			_oobj.src = core_VALOR;
		} else {
			$(_oobj).html(core_VALOR);
		}
	}
}

// ------------------------------------------------
function core_secureRamdom() {

	var v = 0;
	var x = new Array(16);
	var a = new SecureRandom();
	a.nextBytes(x);
	for ( var i = 0; i < x.length; i++) {
		v += (Math.floor(10 * x[i] / 256) / (Math.pow(10, i + 1)));
	}
	return v;
}

// ------------------------------------------------

function core_aleatorio(inferior, superior) {
	var numPosibilidades = superior - inferior;
	var aleat = core_secureRamdom() * numPosibilidades;
	aleat = Math.round(aleat);
	return parseInt(inferior, 10) + aleat;
}

// ------------------------------------------------
function core_comfirm(msg) {

	core_block_window();
	var dd = confirm(msg);
	core_unblock_window();

	return dd;
}


function core_newDate() {
	return new Date(jsonrpc.pcore.getCurrentTime());
}

function core_timeToStringDate(time) {
	var fecha = new Date(time);

	fecha.setTime(time);

	var dias = [ "domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "s�bado" ];
	var meses = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ]

	return dias[fecha.getDay()] + " " + fecha.getDate() + " de " + meses[fecha.getMonth()] + " de " + fecha.getFullYear();
}

function core_timeToStringHour(time) {
	var fecha = new Date(time);
	fecha.setTime(time);

	var hora = fecha.getHours();
	var minutos = fecha.getMinutes();
	var segundos = fecha.getSeconds();
	var z = (hora >= 12) ? "PM" : "AM";

	return ((hora > 12) ? (hora - 12) : hora) + ":" + ((minutos < 10) ? "0" : "") + minutos + ":" + ((segundos < 10) ? "0" : "") + segundos + " " + z;
}

function core_tiempofaltante(dateinicial, porcentaje) {
	var initdate = new Date();
	var tiempo_transcurrido = initdate.getTime() - dateinicial.getTime();
	var tiempo_total = null;
	if (porcentaje > 0) {
		tiempo_total = 100 * tiempo_transcurrido / porcentaje;
		return parseInt((tiempo_total - tiempo_transcurrido), 10);
	}
	return null;
}

function core_tiempofaltantetexto(dateinicial, porcentaje) {

	var actualdate = new Date();
	var tiempo_transcurrido = actualdate.getTime() - dateinicial.getTime();
	var tiempo_faltante = core_tiempofaltante(dateinicial, porcentaje);

	if (tiempo_transcurrido > 5000) {
		tiempo_faltante = parseInt((tiempo_faltante / 1000), 10);

		if (tiempo_faltante < 5) {
			return "Faltan pocos segundos";
		}

		if (tiempo_faltante < 30) {
			return "Faltan " + tiempo_faltante + " segundos";
		}

		if (tiempo_faltante < 60) {
			return "Falta menos de un minuto.";
		}

		return "Falta menos de " + Math.ceil(tiempo_faltante / 60) + " minutos.";

	}

	return "Calculando tiempo faltante...";
}

function core_calcularDigitoVerificacion(nit) {
	return jsonrpc.pcore.calcularDigitoVerificacion(nit);
}

// ------------------------------------------------

function core_onloadwindow() {
	document.body.style.display = "block";
	core_ocultarLoader();
	core_setVisible("bodyContent", true);
	core_unblock_window();
}

$(core_onloadwindow);

// ------------------------------------------------
// Manejo de Log

function SLogger() {
	this.setError = function(mensaje, e) {
		console.log(mensaje);
		console.log(e);
	}

	this.setInfo = function(mensaje, e) {
		console.log(mensaje);
		console.log(e);
	}
}
var SimpleLogger = new SLogger();

window.onerror = function(mensaje, url, linea) {

	SimpleLogger.setError(mensaje + " [" + url + " : line " + linea + "]", null);
}

// ------------------------------------------------
// Bloqueo del menu contextual

var MENU_INACTIVO = true;

core_listen("load", window, function(e) {

	// Desactiva las animaciones pare IE < 9
	if (navigator.appName == "Microsoft Internet Explorer") {
		var ver = getInternetExplorerVersion();
		if (ver < 9.0) {
			jQuery.fx.off = true;
		}
	}

	if (MENU_INACTIVO) {
		core_listen("contextmenu", document.body, function(e) {
			return false
		});
		core_listen("mouseup", document, function(e) {
			if (e.button == 2 || e.button == 3) {
				return false;
			}
		});
	}

});

// ------------------------------------------------
// Obtiene la versi�n de IE
function getInternetExplorerVersion() {
	var rv = -1; // Return value assumes failure.
	if (navigator.appName == 'Microsoft Internet Explorer') {
		var ua = navigator.userAgent;
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}
	return rv;
}

// ------------------------------------------------
// Bloqueo de la tecla backspace para que no retorne a la pagina anterior

core_listen("keydown", document, function(e) {

	var obj = e.target;
	if (e.keyCode == 8 && obj != null && (obj.tagName != 'INPUT' && obj.tagName != 'TEXTAREA')) {
		return false;
	}

});

// ------------------------------------------------
// Presenta nombre y version de la aplicacion con F9

var KEYDOWN_ACTIVO = true;

core_listen("keydown", document, function(e) {

	if (e.keyCode == 120 && KEYDOWN_ACTIVO) {
		try {

			jsonrpc.pcore.getAplicationName(function(res, e) {
				if (e) {
					return;
				}
				alert(res);
			});
		}

		catch (E) {
		}

		return false;
	}
})

// ------------------------------------------------
// Bloqueo del Menu contextual por el evento dobleclick

core_listen("dblclick", document, function(e, obj) {
	if (e.button == 2 || e.button == 3) {
		return false;
	}
});

// ------------------------------------------------
// Round para flotantes
function core_round(num, places) {
	if (places == undefined) {
		places = 0;
	}
	var pow = Math.pow(10, places);
	return Math.round(num * pow) / pow;
}

// -------------------------------------------------
function core_esVerdad(val) {
	if (core_esVacio(val)) {
		return false;
	}

	val = core_trim(val).toLowerCase();

	return (val == "si" || val == "yes" || val == "true" || val == "1" || val == "y" || val == "s" || val == "t");
}


// ***********************************************************************************************************
//***********************************************************************************************************

//prng4.js - uses Arcfour as a PRNG

function Arcfour() {
  this.i = 0;
  this.j = 0;
  this.S = new Array();
}

// Initialize arcfour context from key, an array of ints, each from [0..255]
function ARC4init(key) {
  var i, j, t;
  for(i = 0; i < 256; ++i)
    this.S[i] = i;
  j = 0;
  for(i = 0; i < 256; ++i) {
    j = (j + this.S[i] + key[i % key.length]) & 255;
    t = this.S[i];
    this.S[i] = this.S[j];
    this.S[j] = t;
  }
  this.i = 0;
  this.j = 0;
}

function ARC4next() {
  var t;
  this.i = (this.i + 1) & 255;
  this.j = (this.j + this.S[this.i]) & 255;
  t = this.S[this.i];
  this.S[this.i] = this.S[this.j];
  this.S[this.j] = t;
  return this.S[(t + this.S[this.i]) & 255];
}

Arcfour.prototype.init = ARC4init;
Arcfour.prototype.next = ARC4next;

// Plug in your RNG constructor here
function prng_newstate() {
  return new Arcfour();
}

// Pool size must be a multiple of 4 and greater than 32.
// An array of bytes the size of the pool will be passed to init()
var rng_psize = 256;


//***********************************************************************************************************
//***********************************************************************************************************


//Random number generator - requires a PRNG backend, e.g. prng4.js

//For best results, put code like
//<body onClick='rng_seed_time();' onKeyPress='rng_seed_time();'>
//in your main HTML document.

var rng_state;
var rng_pool;
var rng_pptr;

//Mix in a 32-bit integer into the pool
function rng_seed_int(x) {
rng_pool[rng_pptr++] ^= x & 255;
rng_pool[rng_pptr++] ^= (x >> 8) & 255;
rng_pool[rng_pptr++] ^= (x >> 16) & 255;
rng_pool[rng_pptr++] ^= (x >> 24) & 255;
if(rng_pptr >= rng_psize) rng_pptr -= rng_psize;
}

//Mix in the current time (w/milliseconds) into the pool
function rng_seed_time() {
rng_seed_int(new Date().getTime());
}

//Initialize the pool with junk if needed.
if(rng_pool == null) {
rng_pool = new Array();
rng_pptr = 0;
var t;
if(navigator.appName == "Netscape" && navigator.appVersion < "5" && window.crypto) {
 // Extract entropy (256 bits) from NS4 RNG if available
 var z = window.crypto.random(32);
 for(t = 0; t < z.length; ++t)
   rng_pool[rng_pptr++] = z.charCodeAt(t) & 255;
}  
while(rng_pptr < rng_psize) {  // extract some randomness from Math.random()
 t = Math.floor(65536 * Math.random());
 rng_pool[rng_pptr++] = t >>> 8;
 rng_pool[rng_pptr++] = t & 255;
}
rng_pptr = 0;
rng_seed_time();
//rng_seed_int(window.screenX);
//rng_seed_int(window.screenY);
}

function rng_get_byte() {
if(rng_state == null) {
 rng_seed_time();
 rng_state = prng_newstate();
 rng_state.init(rng_pool);
 for(rng_pptr = 0; rng_pptr < rng_pool.length; ++rng_pptr)
   rng_pool[rng_pptr] = 0;
 rng_pptr = 0;
 //rng_pool = null;
}
// TODO: allow reseeding after first request
return rng_state.next();
}

function rng_get_bytes(ba) {
var i;
for(i = 0; i < ba.length; ++i) ba[i] = rng_get_byte();
}

function SecureRandom() {}

SecureRandom.prototype.nextBytes = rng_get_bytes;

//***********************************************************************************************************
//***********************************************************************************************************

function core_formatoMoneDeci(numero){ 
    
	var caract = numero.charAt(0);
	var decimales = "2";
	var separador_decimal = "." ;
	var separador_miles = ",";
	
	if( caract !=',' ){
		
		numero=numero.toString().replace(",",".");

	    numero=parseFloat(numero);
    	
    	if(isNaN(numero)){
    		return "";
    	}
    	
    	if(decimales!==undefined){
    		// Redondeamos al n�mero de decimales
    		numero=numero.toFixed(decimales);
    	}
    
    	// Convertimos el punto en separador_decimal
    	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    	if(separador_miles){
    		// A�adimos los separadores de miles
    		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
    		while(miles.test(numero)) {
    			numero=numero.replace(miles, "$1" + separador_miles + "$2");
    		}
    	}
    }else{

    	numero=numero.toString().replace(",", "0.");    	
    	
    }

    return "$"+numero;
}

function core_isAllObligatorioValido(cssObligatoria){
	try {

		var isValido = true;
		
		$("."+cssObligatoria).each(function(i){
			
			if (this.tagName == "INPUT" || this.tagName == "SELECT" || this.tagName == "TEXTAREA") {
				if(core_esVacio(this.value)){
					isValido = false;
				}
				
			} 
			
			
		});
		
		return isValido;
		
	} catch (E_) {
		alert(E_);
	}

	return null;
	
}