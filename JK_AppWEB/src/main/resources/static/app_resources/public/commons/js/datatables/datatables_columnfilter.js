
function  GLOBAL_CREATE_DATATABLE(idTable){
	
	
	//Tabla adicional de objetivos
	if ( !$.fn.dataTable.isDataTable('#'+idTable) ) {
		try{
			
			var textSearch 				= $("#fragment_datatable_search").text();
			var textCopy 				= $("#fragment_datatable_copy").text()
			var textPrint 				= $("#fragment_datatable_print").text()
			var textlengthMenu          = $("#fragment_datatable_lengthMenu").text()
			var textzeroRecords         = $("#fragment_datatable_zeroRecords").text()
			var textinfo                = $("#fragment_datatable_info").text()
			var textinfoEmpty           = $("#fragment_datatable_infoEmpty").text()
			var textinfoFiltered        = $("#fragment_datatable_infoFiltered").text()

			
		
			//Tabla APP
			$('#'+idTable+' thead tr').clone(true).appendTo('#'+idTable+' thead');
			
			$('#'+idTable+' thead tr:eq(1) th').each(
					function(i) {
						var title = $(this).text();
						$(this).html(
								'<input type="text" class="form-control form-control-sm" placeholder="'+
								 title + '" />');

						$('input', this).on('keyup change', function() {
							if (table.column(i).search() !== this.value) {
								table.column(i).search(this.value).draw();
							}
						});
					});

			var table = $('#'+idTable).DataTable({
				
				
				language: {
		            "lengthMenu": textlengthMenu,
		            "zeroRecords": textzeroRecords,
		            "info": textinfo,
		            "infoEmpty": textinfoEmpty,
		            "infoFiltered": textinfoFiltered
		        },
				
				scrollX : true,
				orderCellsTop : true,
				fixedHeader : true,
				dom:
	                /*	--- Layout Structure 
	                	--- Options
	                	l	-	length changing input control
	                	f	-	filtering input
	                	t	-	The table!
	                	i	-	Table information summary
	                	p	-	pagination control
	                	r	-	processing display element
	                	B	-	buttons
	                	R	-	ColReorder
	                	S	-	Select

	                	--- Markup
	                	< and >				- div element
	                	<"class" and >		- div with a class
	                	<"#id" and >		- div with an ID
	                	<"#id.class" and >	- div with an ID and a class

	                	--- Further reading
	                	https://datatables.net/reference/option/dom
	                	--------------------------------------
	                 */
	                "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'Bl>>" +
	                "<'row'<'col-sm-12'tr>>" +
	                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
				buttons: [
	                 /*{
	                 	extend:    'colvis',
	                 	text:      'Column Visibility',
	                 	titleAttr: 'Col visibility',
	                 	className: 'mr-sm-3'
	                 },*/
	                 {
	                     extend: 'pdfHtml5',
	                     text: 'PDF',
	                     titleAttr: 'PDF',
	                     className: 'btn-outline-primary btn-sm mr-1'
	                 },
	                 {
	                     extend: 'excelHtml5',
	                     text: 'Excel',
	                     titleAttr: 'Excel',
	                     className: 'btn-outline-primary btn-sm mr-1'
	                 },
	                 {
	                     extend: 'csvHtml5',
	                     text: 'CSV',
	                     titleAttr: 'CSV',
	                     className: 'btn-outline-primary btn-sm mr-1'
	                 },
	                 {
	                     extend: 'copyHtml5',
	                     text: textCopy,
	                     titleAttr: textCopy,
	                     className: 'btn-outline-primary btn-sm mr-1'
	                 },
	                 {
	                     extend: 'print',
	                     text: textPrint,
	                     titleAttr: textPrint,
	                     className: 'btn-outline-primary btn-sm'
	                 }
	             ]
			});
			
			
			
			
			//Other ta
			
			//Traduccion  
			$("input[type=search]").each(
		     function(i) {
		         $(this).attr("placeholder", textSearch);
		             
		     });
		
		}catch(e){
			
		}
		
		
	
	}
	
	
	
}

$(document).ready
(
	function() {
		
		idDataTablePpal = "dt-table-ppal";
		
		GLOBAL_CREATE_DATATABLE(idDataTablePpal);

	}
);

