/*
 * Se elimina la clase animated luego de que ya se realiza la animacion
 * para que el fullscreen funcione 
 */

$(document).ready(function ($) {
	
	
	var textButtonClose 		= $("#fragment_panel_toolbar_button_close").text();
	var textButtonFullScreen 	= $("#fragment_panel_toolbar_button_fullscreen").text();
	var textButtonCollapse 		= $("#fragment_panel_toolbar_button_collapse").text();
	
	
	var textButtonPanelLocked 	= $("#fragment_panel_toolbar_button_locked").text();
	var textButtonPanelReset 	= $("#fragment_panel_toolbar_button_reset").text();
	var textButtonPanelRefresh	= $("#fragment_panel_toolbar_button_refresh").text();
	
	
	
	
	
	
	
	/* Cambiar texto segun lenguaje*/
    $(".js-panel-close").each(
     function(i) {
         
         $(this).attr("data-original-title", textButtonClose);
             
     });
    
    $(".js-panel-fullscreen").each(
     function(i) {
         
         $(this).attr("data-original-title", textButtonFullScreen);
             
     });
    
    
    $(".js-panel-collapse").each(
     function(i) {
         
         $(this).attr("data-original-title", textButtonCollapse);
             
     });

    
    $(".dropdown-item.js-panel-locked > span").each(
		 function(i) {
		     
		     $(this).text(textButtonPanelLocked);
		         
		 }
    );
    
    $(".dropdown-item.js-panel-reset > span").each(
   		 function(i) {
   		     
   		     $(this).text(textButtonPanelReset);
   		         
   		 }
       );
    
    $(".dropdown-item.js-panel-refresh > span").each(
   		 function(i) {
   		     
   		     $(this).text(textButtonPanelRefresh);
   		         
   		 }
       );

});