//i18nAPPFactory

/*          isolatedAttributeFoo:'@attributeFoo' Contenido del objeto,
            isolatedBindingFoo:'=bindingFoo'     Nombre del objeto,
            isolatedExpressionFoo:'&'			 funcion
*/ 

(function(){
    'use strict';
    var app = angular
        .module('i18nAPPModule',[])
        
    app.directive('texti18n', function($http){
	    return {
	      restrict: 'E',
	      template: '<span>{{texti18n}}</span>',
	      scope: {
	        object: '=object',
	        label: '@label'
	      },
	      
	      link:function(scope, iElement, iAttrs, controller, transcludeFn) {
	    	  
	    	 scope.$watch('object', function(newValue, oldValue) {
					
					if (newValue){
						
						var urlBase = GLOBAL_CONTEXTPATH ;
						var URL = urlBase + "/rest/private/i18n/translate";
						var token = GLOBAL_CRSF;
						
	                	var textToTranslate = newValue;
	                	
	                	$http({
		    				    method: 'POST',
		    				    url: URL,
		    				    data: angular.toJson(textToTranslate),
		    				    headers: {
		    				    	'X-CSRF-TOKEN':token,	
		    				    	'Content-Type': 'application/json'
		    				    }
		    			    
		    			}).then(
		    			    function(success) { // success
		    				
		    					
		    					if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		    						var textTraslated = success.data.detalle;
		    						iElement.text(textTraslated);
		    					}
		    				},
		    				function(error) { // error
		    				    console.log(error);
		    				}
		    			);	  
	                }
	                    
	            }, true);
	    	  
	    	  
	    	  
	    	  
	    	scope.$watch('label', function(newValue, oldValue) {
				
				if (newValue){
					
					var urlBase = GLOBAL_CONTEXTPATH ;
					var URL = urlBase + "/rest/private/i18n/translate";
					var token = GLOBAL_CRSF;
					
                	var textToTranslate = newValue;
                	
                	$http({
	    				    method: 'POST',
	    				    url: URL,
	    				    data: angular.toJson(textToTranslate),
	    				    headers: {
	    				    	'X-CSRF-TOKEN':token,	
	    				    	'Content-Type': 'application/json'
	    				    }
	    			    
	    			}).then(
	    			    function(success) { // success
	    				
	    					
	    					if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
	    						var textTraslated = success.data.detalle;
	    						iElement.text(textTraslated);
	    					}
	    				},
	    				function(error) { // error
	    				    console.log(error);
	    				}
	    			);	  
                }
                    
            }, true);
			
			
	    	
	      }
	      
	      
	    }
	      
	  });
    
    

	
})();