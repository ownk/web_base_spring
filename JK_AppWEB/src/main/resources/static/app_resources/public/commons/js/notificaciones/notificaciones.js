/**
 * =========================================================
 * Se crea el objetos necesarios para las notificaciones
 * ========================================================
 */


$(document).ready(function() {

	//Mensajes Generales APP
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "progressBar": true,
			  "preventDuplicates": false,
			  "positionClass": "toast-bottom-right",
			  "onclick": null,
			  "showDuration": "400",
			  "hideDuration": "3000",
			  "timeOut": "7000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			};
	
	
	var cantMensajeGenerales=$('#mensajesGeneralesAPP').val();
	if(cantMensajeGenerales > 0){
		for(var i=0;i<cantMensajeGenerales;i++){
			var tipoMensaje=$('#msj-general-tipoMensaje-'+i).val();
			var procesoMensaje=$('#msj-general-procesoMensaje-'+i).val();
			var datoMensaje=$('#msj-general-datoMensaje-'+i).val();		
			toastr[tipoMensaje](datoMensaje,procesoMensaje);
		}
	}
	
	
	//Mensajes Generales APP
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "progressBar": true,
			  "preventDuplicates": false,
			  "positionClass": "toast-bottom-right",
			  "onclick": null,
			  "showDuration": "400",
			  "hideDuration": "3000",
			  "timeOut": "7000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			};
	
	
	var cantMensajePagina=$('#mensajesPorPagina').val();
	if(cantMensajePagina > 0){
		for(var i=0;i<cantMensajePagina;i++){
			var tipoMensaje=$('#msj-pagina-tipoMensaje-'+i).val();
			var procesoMensaje=$('#msj-pagina-procesoMensaje-'+i).val();
			var datoMensaje=$('#msj-pagina-datoMensaje-'+i).val();		
			toastr[tipoMensaje](datoMensaje,procesoMensaje);
		}
	}
	
	$('.loading').hide();
	
	
});

$(window).on("unload", function(e) {
    $('.loading').show();    
});