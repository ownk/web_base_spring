(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.games = [];	
			
			
			
			$scope.infoGroup = function(idPlantilla) {
				
				var infoTemplate = {};
				
				infoTemplate.templt_templt = idPlantilla;
				
				
				$scope.pass1_show_error= false;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/client/games/groups/group";
				
				var token = GLOBAL_CRSF;
				
				$http({
		            method: 'POST',
		            data: angular.toJson(infoTemplate),
		            url: URL,
		            headers: {
		            	'X-CSRF-TOKEN':token,	
		            	'Content-Type': 'application/json'
		            }
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		
		            		$scope.group=success.data.detalle;
		            		
		            		
		            	}else{
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	
		            }
		        );
			}
			
	  
		  }]
	  );
	  
	
	
	
	


})();


$(document).ready
(
	function() {
		
		//Table to new group
		if ( !$.fn.dataTable.isDataTable( '#table_games' ) ) {
			
			
			try{
					
				var textSearch 				= $("#fragment_datatable_search").text();
				var textCopy 				= $("#fragment_datatable_copy").text()
				var textPrint 				= $("#fragment_datatable_print").text()
				var textlengthMenu          = $("#fragment_datatable_lengthMenu").text()
				var textzeroRecords         = $("#fragment_datatable_zeroRecords").text()
				var textinfo                = $("#fragment_datatable_info").text()
				var textinfoEmpty           = $("#fragment_datatable_infoEmpty").text()
				var textinfoFiltered        = $("#fragment_datatable_infoFiltered").text()
			
				
			
				//Tabla APP
				$('#table_games thead tr').clone(true).appendTo('.table_games thead');
				
				$('#table_games thead tr:eq(1) th').each(
						function(i) {
							var title = $(this).text();
							$(this).html(
									'<input type="text" class="form-control form-control-sm" placeholder="'+
									 title + '" />');
			
							$('input', this).on('keyup change', function() {
								if (table.column(i).search() !== this.value) {
									table.column(i).search(this.value).draw();
								}
							});
						});
				
			
				var table = $('#table_games').DataTable({
					
					
					language: {
			            "lengthMenu": textlengthMenu,
			            "zeroRecords": textzeroRecords,
			            "info": textinfo,
			            "infoEmpty": textinfoEmpty,
			            "infoFiltered": textinfoFiltered
			        },
					
					
					orderCellsTop : true,
					fixedHeader : true,
					dom:
			            /*	--- Layout Structure 
			            	--- Options
			            	l	-	length changing input control
			            	f	-	filtering input
			            	t	-	The table!
			            	i	-	Table information summary
			            	p	-	pagination control
			            	r	-	processing display element
			            	B	-	buttons
			            	R	-	ColReorder
			            	S	-	Select
			
			            	--- Markup
			            	< and >				- div element
			            	<"class" and >		- div with a class
			            	<"#id" and >		- div with an ID
			            	<"#id.class" and >	- div with an ID and a class
			
			            	--- Further reading
			            	https://datatables.net/reference/option/dom
			            	--------------------------------------
			             */
			            "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'l>>" +
			            "<'row'<'col-sm-12'tr>>" +
			            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
					
				});
				
				
				
				
				//Other ta
				
				//Traduccion  
				$("input[type=search]").each(
			     function(i) {
			         $(this).attr("placeholder", textSearch);
			             
			     });
			
			}catch(e){
				
			}
			
		    
		}
		
		
	
	}

);