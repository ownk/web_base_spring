(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.games= [];	
			
			
			$scope.infoGame = function (game){
				
				
				infoGame(game.game_game,game.game_state);
				
				
			}
			
			
			$scope.getGames = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/game/all_games";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.games = success.data.detalle;
		            		
		            		
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	
		            }
		        );
			};
			
			$scope.getGames();
			
			
		  }]
	  );
	
	


})();


function infoGame(idGame,stateGame){
	
	
	core_setValue('GameDetails.game_game', idGame);
	core_setValue('GameDetails.game_state', stateGame);
	core_sendForm('form_game_details');
	
	
}
