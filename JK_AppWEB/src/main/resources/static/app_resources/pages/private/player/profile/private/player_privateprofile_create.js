(function() {
	
	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.questions = [];
			$scope.avatars = [];		
			$scope.pets = [];
			$scope.ships = [];	
			
		    $scope.slickAvatarsConfig={
					infinite: false,
					speed:300,
			        slidesToShow:5,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                   
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    
		                },
		                destroy: function (event, slick) {
		                  
		                },
		                edge: function (event, slick, direction) {
		                    
		                },
		                reInit: function (event, slick) {
		                    
		                },
		                init: function (event, slick) {
		              
		                },
		                setPosition: function (evnet, slick) {
		                    
		                },
		                swipe: function (event, slick, direction) {
		                    
		                }
		            }
		 	};
			
			
			$scope.slickPetsConfig={
					infinite: false,
					speed:300,
			        slidesToShow: 5,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                    //console.log('before change');
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    //console.log('breakpoint');
		                },
		                destroy: function (event, slick) {
		                    //console.log('destroy');
		                },
		                edge: function (event, slick, direction) {
		                    //console.log('edge');
		                },
		                reInit: function (event, slick) {
		                    //console.log('re-init');
		                },
		                init: function (event, slick) {
		                    //console.log('init');
		                },
		                setPosition: function (evnet, slick) {
		                    //console.log('setPosition');
		                },
		                swipe: function (event, slick, direction) {
		                    //console.log('swipe');
		                }
		            }
		            
					
			};
			
			$scope.slickShipsConfig={
					infinite: false,
					speed:300,
			        slidesToShow: 5,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                    //console.log('before change');
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    //console.log('breakpoint');
		                },
		                destroy: function (event, slick) {
		                    //console.log('destroy');
		                },
		                edge: function (event, slick, direction) {
		                    //console.log('edge');
		                },
		                reInit: function (event, slick) {
		                    //console.log('re-init');
		                },
		                init: function (event, slick) {
		                    //console.log('init');
		                },
		                setPosition: function (evnet, slick) {
		                    //console.log('setPosition');
		                },
		                swipe: function (event, slick, direction) {
		                    //console.log('swipe');
		                }
		            }
		            
					
			};
			
			
			
			$scope.getQuestions = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/game/questions";
				
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.questions = success.data.detalle;
		            		
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.nick="";
	            		$scope.nick_show_error= true; 
		            }
		        );
			};
			$scope.getAvatars = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/game/avatars";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.avatars = success.data.detalle;
		            		
		        			
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.nick="";
	            		$scope.nick_show_error= true; 
		            }
		        );
			};
			
			$scope.getPets = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/game/pets";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.pets = success.data.detalle;
		            		
		        			
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.nick="";
	            		$scope.nick_show_error= true; 
		            }
		        );
			};
			
			$scope.getShips = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/game/ships";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.ships = success.data.detalle;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.nick="";
	            		$scope.nick_show_error= true; 
		            }
		        );
			};
			
			
			$scope.selectAvatar = function(item) {
				
				
				for (var i = 0; i < $scope.avatars.length; i++) {
					$scope.avatars[i].isSelected = false;
				}

				if (item.isSelected) {
					item.isSelected = false;

				} else {
					item.isSelected = true;

				}
				
				

			};

			$scope.selectPet = function(item) {
				
				for (var i = 0; i < $scope.pets.length; i++) {
					$scope.pets[i].isSelected = false;
				}

				if (item.isSelected) {
					item.isSelected = false;

				} else {
					item.isSelected = true;

				}

			};
			
			$scope.selectShip = function(item) {
				
				for (var i = 0; i < $scope.ships.length; i++) {
					$scope.ships[i].isSelected = false;
				}

				if (item.isSelected) {
					item.isSelected = false;

				} else {
					item.isSelected = true;

				}

			};
			
			
			$scope.selectQuestion = function(question, answer) {
				
				question.isSelected=true;
				question.answer_answer = answer;
				console.log(question);
			}
			
			
			$scope.save = function() {
				
				var isOk = true;
				var playerCharacter = {}; 
				
				var avatars = $scope.avatars;

				for (var j = 0; j < avatars.length; j++) {

					if (avatars[j].isSelected) {

						playerCharacter.avatar_avatar = avatars[j].avatar_avatar;
					}
				}
				
				
				var pets = $scope.pets;

				for (var j = 0; j < pets.length; j++) {

					if (pets[j].isSelected) {

						playerCharacter.pet_pet = pets[j].pet_pet;
					}
				}
				
			
				var ships = $scope.ships;

				for (var j = 0; j < ships.length; j++) {

					if (ships[j].isSelected) {

						playerCharacter.ship_ship = ships[j].ship_ship;
					}
				}
				
				
				
				var questions = $scope.questions;
				var questionsSelected = [];
				var totalQuestionsSelected = 0;

				for (var j = 0; j < questions.length; j++) {

					if (questions[j].isSelected) {
						
						questionsSelected[totalQuestionsSelected] = questions[j];
						
						
						totalQuestionsSelected++;
					}
				}
				
				
				playerCharacter.questions = questionsSelected;
				
							
				console.log(angular.toJson(playerCharacter));

				
				if(isOk){
					var URL = GLOBAL_CONTEXTPATH + "/rest/private/player/profile/private/create";
					
					var token = GLOBAL_CRSF;
				    
					$http({
			            method: 'POST',
			            data: angular.toJson(playerCharacter),
			            url: URL,
			            headers: {
			            	'X-CSRF-TOKEN':token,	
			            	'Content-Type': 'application/json'
			            }
			        }).then(
			            function(success) { // success
			            	
			            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
			            		
			            		
			            		$scope.pets = success.data.detalle;
			            		
			            		$window.location.href = $scope.GLOBAL_CONTEXTPATH +success.data.detalle;
			            		
			            	}else{
			            		
			            	}
			            	
			            },
			            function(error) { // error
			            	$scope.nick="";
		            		$scope.nick_show_error= true; 
			            }
			        );
				}
				
			}
			
			
			$scope.openModalAvatarDetail = function (avatar){
				
				$scope.avatarMoreInfo = avatar;
				
				$scope.showAvatarDetail = true;
				
			}
			
			$scope.openModalShipDetail = function (ship){
				
				$scope.shipMoreInfo = ship;
				
				$scope.showShipDetail = true;
				
			}
			
			$scope.openModalPetDetail = function (pet){
				
				$scope.petMoreInfo = pet;
				
				$scope.showPetDetail = true;
				
			}
		
				
				
			//init
			$scope.getQuestions();
			$scope.getAvatars();
			$scope.getPets();
			$scope.getShips();
			
			
			
			
	  
		  }]
	  );
	  
	  app.directive('wizard', [ function() {
		return {
			restrict : 'EA',
			scope : {
				stepChanging : '='
			},
			compile : function(element, attr) {
				element.steps({
					bodyTag : attr.bodyTag,
					autoFocus: true,
					transitionEffect: "slideLeft",
					onInit: function (event, current)
					{
						console.log("iniciando steps");
						
						$("#wizard_player").show();
						$(".div_info_character").show();
					},
					onStepChanging: function (event, currentIndex, newIndex)
				    {
						$("#slickAvatars .slick-prev").click();
						$("#slickShips .slick-prev").click();
						$("#slickPets .slick-prev").click();
						
						
						var avatar = $("#slickAvatars .isSelected");
				    	var pet = $("#slickPets .isSelected");
				    	var ship = $("#slickShips .isSelected");
						
						
						var isOk = true;
						
						if(currentIndex == 0 && avatar.length == 0){
							
				    		isOk = false;
				    	}else if(currentIndex == 1 && pet.length == 0){
				    		
				    		isOk = false;
				    	}else if(currentIndex == 2 && ship.length == 0){
				    		isOk = false;
				    	}
				    	
				    	return isOk;
				    },
				    onStepChanged: function (event, currentIndex, priorIndex)
				    {
				    	
				    	$('.slider-nav').slick('slickGoTo', 0);
				    	
				    },
				    onFinishing: function (event, currentIndex)
				    {
				    	console.log("onFinishing");
				    	
				    	
				    	$("#save-btn").click();
				    	
				        return true;
				    },
				    onFinished: function (event, currentIndex)
				    {
				        console.log("onFinished");
				    }
					
					
				});

				return {
					// pre-link
					pre : function() {
						
					},
					// post-link
					post : function(scope, element) {
						element.on('stepChanging', scope.stepChanging);
					}
				}
			}
		};
	} ]);
	
	
	
	
	


})();





