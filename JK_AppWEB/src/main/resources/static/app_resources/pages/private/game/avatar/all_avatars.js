(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.avatars = [];		
			
			$scope.slickAvatarsConfig={
					infinite: false,
					speed:300,
			        slidesToShow:5,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                   
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    
		                },
		                destroy: function (event, slick) {
		                  
		                },
		                edge: function (event, slick, direction) {
		                    
		                },
		                reInit: function (event, slick) {
		                    
		                },
		                init: function (event, slick) {
		                	$(".div_info_character").show();
		                },
		                setPosition: function (evnet, slick) {
		                    
		                },
		                swipe: function (event, slick, direction) {
		                    
		                }
		            }
		 	};
			
			
			
			$scope.getAvatars = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/game/avatars";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.avatars = success.data.detalle;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.nick="";
	            		$scope.nick_show_error= true; 
		            }
		        );
			};
			
			
			$scope.openModalAvatarDetail = function (avatar){
				
				$scope.avatarMoreInfo = avatar;
				
				$scope.showAvatarDetail = true;
				
			}
				
			//init
			$scope.getAvatars();
			
			
	  
		  }]
	  );
	  
	
	
	
	
	


})();





