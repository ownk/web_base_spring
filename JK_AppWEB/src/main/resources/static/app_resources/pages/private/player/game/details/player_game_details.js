(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.mathesByGame = [];	
			$scope.game_game = "";
			
			
			
			
			$scope.getMatchesByGame = function (game_game) {
				
				$scope.game_game = game_game;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/player/game/details/matches/"+$scope.game_game;
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.mathesByGame = success.data.detalle;
		            	
		            	}
		            	
		            },
		            function(error) { // error
		            	
		            }
		        );
			};
			
			
			
			
		  }]
	  );
	
	


})();



$(document).ready
(
	function() {
		
		//Extra datatable
		var idTableObjectives = "dt-table-lrnobjs";
		
		GLOBAL_CREATE_DATATABLE(idTableObjectives);
		
	}
);