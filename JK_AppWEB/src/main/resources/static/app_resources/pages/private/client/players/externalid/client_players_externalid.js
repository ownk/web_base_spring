(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	
			
	  
		  }]
	  );
	
	

    Dropzone.options.myAwesomeDropzone = {

        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 1,

        // Dropzone settings
        init: function() {
            var myDropzone = this;

            this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });
			
			
			this.on("addedfile", function(file) { 
			
				$('#btn_checkImage').removeAttr('disabled');
			
			});
			
            this.on("sendingmultiple", function() {
				
            });
            this.on("successmultiple", function(files, response) {
            						
				$('#btn_refreshpage').click();
			
            });
            this.on("errormultiple", function(files, response) {
				
            });
        }

    }
    
    //Traduccion
    var textDropzoneMessage = $("#fragment_dropzone_message").text();
    $("#my-awesome-dropzone.dz-default.dz-message > span").text(textDropzoneMessage);
	

})();



