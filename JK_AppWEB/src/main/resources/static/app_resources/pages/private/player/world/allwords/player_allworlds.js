(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.worldsByPlayer = [];	
			$scope.quickGamesByPlayer = [];
			$scope.worldMoreInfo = {};	
			$scope.showWorldDetail = false;
			
			$scope.slickWorldsConfig={
					infinite: false,
					speed:300,
			        slidesToShow: 4,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                   
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    
		                },
		                destroy: function (event, slick) {
		                  
		                },
		                edge: function (event, slick, direction) {
		                    
		                },
		                reInit: function (event, slick) {
		                    
		                },
		                init: function (event, slick) {
		                	$(".div_worlds").show();
		                	$(".div_info_world").show();
		                	
		                },
		                setPosition: function (evnet, slick) {
		                    
		                },
		                swipe: function (event, slick, direction) {
		                    
		                }
		            }
		 	};
			
			$scope.openModalWorldDetail = function (world){
				
				$scope.worldMoreInfo = world;
				
				$scope.showWorldDetail = true;
				
			}
		
			
			$scope.playWorld = function (world){
				
				
				gotoWorld(world.world_world);
				
				
			}
			
			
			$scope.infoGame = function (game){
				
				
				infoGame(game.game_game);
				
				
			}
			
			$scope.playGame = function (game){
				
				playGame(game.game_game);
			}
			
			$scope.getWorldsByPlayer = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/player/world/allworlds/get";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.worldsByPlayer = success.data.detalle;
		            		
		            		buildWorlds(success.data.detalle);
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	
		            }
		        );
			};
			
			
			$scope.getQuickGamesByPlayer = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/player/world/allworlds/quickgames";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.quickGamesByPlayer = success.data.detalle;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	
		            }
		        );
			};
			
					
			//init
			$scope.getWorldsByPlayer();
			$scope.getQuickGamesByPlayer();
			
		  }]
	  );
	
	


})();


function buildWorlds(worlds){
	
	var bubble_map = new Datamap({
		  element: document.getElementById("worlds_map"),
		  responsive : true,
		  
		  geographyConfig: {
			  
			highlightFillColor : '#8a8a8a',
			highlightBorderWidth : 0,
			borderColor: 'rgba(0, 0, 0, 0.1)',
		  },
		  bubblesConfig: {
		        borderWidth: 1,
		        borderOpacity: 0.5,
		        borderColor: '#424242',
		        popupOnHover: true,
		        radius: null,
		        popupTemplate: function(geography, data) {
		          return '<div class="hoverinfo"><strong>' + data.name + '</strong></div>';
		        },
		        fillOpacity: 0.8,
		        animate: true,
		        highlightOnHover: true,
		        highlightFillColor: 'rgb(0, 0, 0, 0.5)',
		        highlightBorderColor: 'rgb(0, 0, 0, 0.8)',
		        highlightBorderWidth: 1,
		        highlightBorderOpacity: 1,
		        highlightFillOpacity: 1,
		        exitDelay: 100,
		        key: JSON.stringify
		  },
		  
		  fills : {
				defaultFill : "rgba(0, 0, 0, 0.2)",
				inactive : "rgb(100, 100, 100, 0.7)",
				active : "rgb(0, 148, 113, 0.9)"
		  },
		  done: function(datamap) {
			  
	            datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
	                
	            });
	        }
		});
	
	
	
	var bubbles = [];
	
	var totalWordls = worlds.length;
	
	for (var i = 0; i < totalWordls; i++) {
		
		var fillColor = 'inactive';
		
		if(worlds[i].world_state == 'ACT'){
			fillColor = 'active';
		}
		
		bubbles[i] = {};
		
		var bubble = {};
		
		bubble.name= worlds[i].world_world;
		bubble.radius= worlds[i].world_size;
		bubble.yeild= 0;
		bubble.fillKey= fillColor;
		bubble.significance= worlds[i].world_descri;
		bubble.longitude= worlds[i].world_positn_x;
		bubble.latitude= worlds[i].world_positn_y;
		bubble.state = worlds[i].world_state;
		
		bubbles[i] = bubble;
		
	}
	
	
	bubble_map.bubbles(
			
		  bubbles, {
		  popupTemplate: function(geo, data) {
		    return ['<div class="hoverinfo">'+data.significance+'</div>'].join('');
		  }
	});
	
	d3.selectAll(".datamaps-bubble").on('click', function(bubble) {
		
		if(bubble.state == 'ACT'){
			gotoWorld(bubble.name);
		}else{
			alert('World is not active yet');
		}
		
		
		
		
	});

	

	
}

function gotoWorld(idWorld){
	
	
	core_setValue('World.world_world', idWorld);
	core_sendForm('form_world');
	
	
}

function infoGame(idGame){
	
	
	core_setValue('GameDetails.game_game', idGame);
	core_sendForm('form_game_details');
	
	
}



function playGame(idGame){
	
	
	core_setValue('GamePlay.game_game', idGame);
	core_sendForm('form_game_play');
	
	
}