(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.player = {};
			$scope.player_name = "";
			
			
			$scope.getPlayer = function (playername) {
				
				
				$scope.player_name = playername;
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/player/profile/public/details/"+$scope.player_name;
				
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.player = success.data.detalle;
		            		
		            		var labels = [];
		            		var data   = [];
		            		
		            		for (var i = 0; i < $scope.player.listAttributes.length; i++) {
		            			labels[i] = $scope.player.listAttributes[i].atrb_name;
		            			data[i] = $scope.player.listAttributes[i].atrpl_value;
								
							} 
		            		
		            		radarChart(labels, data);
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.player=undefined;
	            		 
		            }
		        );
			};
				
			
			
	  
		  }]
	  );
	  
	

})();




/* radar chart */
var radarChart = function(p_labels, p_data )
{
	
	var textPlayerName = $("#fragment_chart_player_name").text(); 
	
	
    var config = {
        type: "radar",
        data:
        {
            labels: p_labels,
            datasets: [
            {
                label: textPlayerName,
                pointRadius: 4,
                borderDashOffset: 2,
                backgroundColor: "rgba(136,106,181, 0.2)",
                borderColor: "rgba(0,0,0,0)",
                pointBackgroundColor: color.primary._500,
                pointBorderColor: color.primary._500,
                pointHoverBackgroundColor: color.primary._500,
                pointHoverBorderColor: color.primary._500,
                data: p_data
            }
            ]
        },
        options:
        {
            responsive: true,
        }
    }

    new Chart($("#radarChart > canvas").get(0).getContext("2d"), config);

}