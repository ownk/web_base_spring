(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.player = {};
			
			$scope.search_active = false;
			$scope.search_error = false;
			$scope.search_ok = false;
			
			
			$scope.searchPlayer = function () {
				
				$scope.search_active = true;
				$scope.search_ok = false;
				$scope.search_error = false;
				
				
				var nick = $scope.nick;
				
				if(nick != undefined && nick.length>0){
					
					var URL = GLOBAL_CONTEXTPATH + "/rest/private/player/search/nick/"+nick;
					
					$http({
			            method: 'GET',
			            url: URL
			        }).then(
			            function(success) { // success
			            	
			            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
			            		
			            		$scope.search_ok = true;
			            		$scope.search_error = false;
			            		$scope.player = success.data.detalle;
			            		
			            	}else{
			            		$scope.search_ok = false;
			            		$scope.search_error = true;
			            	}
			            	
			            },
			            function(error) { // error
			            	$scope.search_error = true;
			            }
			        );
					
					
				}else{
					$scope.search_active = false;
				}
				
				
				
				
			};
				
		
	  
		  }]
	  );
	  
	

})();