(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			
			
		  }]
	  );
	
	


})();


function createCode(){
	
	var gamegroup = $("#group_games").val();
	
	if(gamegroup==null){
		
		$("#group_games").focus();
		return;
	}
	
	var codes = $("#codes").val();
	
	if(codes==null){
		
		$("#codes").focus();
		return;
	}
	
	core_sendForm('form_addcode');
	
}


function sendCode(){
	
	var toName = $("#toName").val();
	
	if(toName==null){
		
		$("#toName").focus();
		return;
	}
	
	var toEmail = $("#toEmail").val();
	
	if(toEmail==null){
		
		$("#toEmail").focus();
		return;
	}
	
	core_sendForm('form_sendcode');
	
}

function deleteCode(){
	

	core_sendForm('form_deletecode');
	
}


function selectCodeToSend(acode){
	
	core_setValue('acode_send', acode);
}

function selectCodeToDelete(acode){
	
	core_setValue('acode_delete', acode);
}

