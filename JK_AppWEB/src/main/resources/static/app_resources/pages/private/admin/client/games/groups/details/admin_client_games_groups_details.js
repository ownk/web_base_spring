(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.games = [];	
			
			
			
			$scope.getGames = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/admin/client/games/groups/games";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.games = success.data.detalle;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.nick="";
	            		$scope.nick_show_error= true; 
		            }
		        );
			};
			
			
			$scope.selectGame = function(idGame) {
				
				for (var i = 0; i < $scope.games.length; i++) {
					
					if($scope.games[i].game_game == idGame ){
						
						if ($scope.games[i].isSelected) {
							$scope.games[i].isSelected = false;

						} else {
							$scope.games[i].isSelected = true;

						}
						
						
					}
				}
			};
			
			
			$scope.infoGroup = function(idPlantilla) {
				
				var idClient = $("#templt_client").val();
				
				var infoTemplate = {};
				
				
				infoTemplate.templt_client = idClient;
				infoTemplate.templt_templt = idPlantilla;
				
				
				$scope.pass1_show_error= false;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/admin/client/games/groups/group";
				
				var token = GLOBAL_CRSF;
				
				$http({
		            method: 'POST',
		            data: angular.toJson(infoTemplate),
		            url: URL,
		            headers: {
		            	'X-CSRF-TOKEN':token,	
		            	'Content-Type': 'application/json'
		            }
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		
		            		$scope.group=success.data.detalle;
		            		
		            		
		            	}else{
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	
		            }
		        );
			}
			
			
			
			
			$scope.createGroup = function() {
				
				
				
				
				var isOk = true;
				var templateGame = {}; 
				
				var groupDescription = $("#templt_descri").val();
				var idClient = $("#templt_client").val();
				
				var games = $scope.games;
				var gamesSelected = [];
				var totalGamesSelected = 0;

				for (var j = 0; j < games.length; j++) {

					if (games[j].isSelected) {
						
						gamesSelected[totalGamesSelected] = games[j];
						totalGamesSelected++;
					}
				}
				
				
				templateGame.games = gamesSelected;
				templateGame.templt_client = idClient;
				templateGame.templt_descri = groupDescription;
				
				
				if(groupDescription==""){
					$("#templt_descri").focus();
					return;
				}

				
				
				if(templateGame.games.length<1){
					$("#table_games").focus();
					return;
				}
				
				
				if(isOk){
					
					console.log(angular.toJson(templateGame));
					
					var URL = GLOBAL_CONTEXTPATH + "/rest/private/admin/client/games/groups/create";
					
					var token = GLOBAL_CRSF;
				    
					$http({
			            method: 'POST',
			            data: angular.toJson(templateGame),
			            url: URL,
			            headers: {
			            	'X-CSRF-TOKEN':token,	
			            	'Content-Type': 'application/json'
			            }
			        }).then(
			            function(success) { // success
			            	
			            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
			            		location.reload();
			            		
			            	}else{
			            		location.reload();
			            	}
			            	
			            },
			            function(error) { // error
			            	
			            }
			        );
				}
				
			}
			
			

			$scope.openModalNewGroup = function(){
				
				
				
			}
					
				
			//init
			$scope.getGames();
			
	  
		  }]
	  );
	  
	
	
	
	


})();


$(document).ready
(
	function() {
		
		
		//Table to new group
		if ( !$.fn.dataTable.isDataTable( '#table_games' ) ) {
			
			
			try{
					
				var textSearch 				= $("#fragment_datatable_search").text();
				var textCopy 				= $("#fragment_datatable_copy").text()
				var textPrint 				= $("#fragment_datatable_print").text()
				var textlengthMenu          = $("#fragment_datatable_lengthMenu").text()
				var textzeroRecords         = $("#fragment_datatable_zeroRecords").text()
				var textinfo                = $("#fragment_datatable_info").text()
				var textinfoEmpty           = $("#fragment_datatable_infoEmpty").text()
				var textinfoFiltered        = $("#fragment_datatable_infoFiltered").text()
			
				
			
				//Tabla APP
				$('#table_games thead tr').clone(true).appendTo('.table_games thead');
				
				$('#table_games thead tr:eq(1) th').each(
						function(i) {
							var title = $(this).text();
							$(this).html(
									'<input type="text" class="form-control form-control-sm" placeholder="'+
									 title + '" />');
			
							$('input', this).on('keyup change', function() {
								if (table.column(i).search() !== this.value) {
									table.column(i).search(this.value).draw();
								}
							});
						});
				
			
				var table = $('#table_games').DataTable({
					
					
					language: {
			            "lengthMenu": textlengthMenu,
			            "zeroRecords": textzeroRecords,
			            "info": textinfo,
			            "infoEmpty": textinfoEmpty,
			            "infoFiltered": textinfoFiltered
			        },
					
					
					orderCellsTop : true,
					fixedHeader : true,
					dom:
			            /*	--- Layout Structure 
			            	--- Options
			            	l	-	length changing input control
			            	f	-	filtering input
			            	t	-	The table!
			            	i	-	Table information summary
			            	p	-	pagination control
			            	r	-	processing display element
			            	B	-	buttons
			            	R	-	ColReorder
			            	S	-	Select
			
			            	--- Markup
			            	< and >				- div element
			            	<"class" and >		- div with a class
			            	<"#id" and >		- div with an ID
			            	<"#id.class" and >	- div with an ID and a class
			
			            	--- Further reading
			            	https://datatables.net/reference/option/dom
			            	--------------------------------------
			             */
			            "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'l>>" +
			            "<'row'<'col-sm-12'tr>>" +
			            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
					
				});
				
				
				
				
				//Other ta
				
				//Traduccion  
				$("input[type=search]").each(
			     function(i) {
			         $(this).attr("placeholder", textSearch);
			             
			     });
			
			}catch(e){
				
			}
			
		    
		}
		
		
	
	}

);