(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			
			
			$scope.slickGamesConfig={
					infinite: false,
					speed:300,
			        slidesToShow: 4,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                   
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    
		                },
		                destroy: function (event, slick) {
		                  
		                },
		                edge: function (event, slick, direction) {
		                    
		                },
		                reInit: function (event, slick) {
		                    
		                },
		                init: function (event, slick) {
		              
		                },
		                setPosition: function (evnet, slick) {
		                    
		                },
		                swipe: function (event, slick, direction) {
		                    
		                }
		            }
		 	};
			
			
			
		  }]
	  );
	
	


})();


function infoGame(idGame,stateGame){
	
	
	core_setValue('GameDetails.game_game', idGame);
	core_setValue('GameDetails.game_state', stateGame);
	core_sendForm('form_game_details');
	
	
}