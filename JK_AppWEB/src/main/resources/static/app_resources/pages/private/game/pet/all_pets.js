(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.pets = [];
			
			
			
			
			$scope.slickPetsConfig={
					infinite: false,
					speed:300,
			        slidesToShow: 5,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                    //console.log('before change');
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    //console.log('breakpoint');
		                },
		                destroy: function (event, slick) {
		                    //console.log('destroy');
		                },
		                edge: function (event, slick, direction) {
		                    //console.log('edge');
		                },
		                reInit: function (event, slick) {
		                    //console.log('re-init');
		                },
		                init: function (event, slick) {
		                	$(".div_info_character").show();
		                },
		                setPosition: function (evnet, slick) {
		                    //console.log('setPosition');
		                },
		                swipe: function (event, slick, direction) {
		                    //console.log('swipe');
		                }
		            }
		            
					
			};
			
			$scope.getPets = function () {
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/game/pets";
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.pets = success.data.detalle;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.nick="";
	            		$scope.nick_show_error= true; 
		            }
		        );
			};
			
			
			
			$scope.openModalPetDetail = function (pet){
				
				$scope.petMoreInfo = pet;
				
				$scope.showPetDetail = true;
				
			}
			//init
			$scope.getPets();
			
			
	  
		  }]
	  );
	  
	
	
	
	
	


})();





