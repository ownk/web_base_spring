(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.gamesByPlayer = [];	
			$scope.gameMoreInfo = {};	
			$scope.showGameDetail = false;
			$scope.world_world = "";
			
			
			$scope.slickGamesConfig={
					infinite: false,
					speed:300,
			        slidesToShow: 4,
		            slidesToScroll: 1,
		            dots: false,
		            autoplay: false,
		            method: {},
		            event: {
		                beforeChange: function (event, slick, currentSlide, nextSlide) {
		                   
		                },
		                afterChange: function (event, slick, currentSlide, nextSlide) {

		                },
		                breakpoint: function (event, slick, breakpoint) {
		                    
		                },
		                destroy: function (event, slick) {
		                  
		                },
		                edge: function (event, slick, direction) {
		                    
		                },
		                reInit: function (event, slick) {
		                    
		                },
		                init: function (event, slick) {
		                	$(".div_info_game").show();
		                },
		                setPosition: function (evnet, slick) {
		                    
		                },
		                swipe: function (event, slick, direction) {
		                    
		                }
		            }
		 	};
			
			$scope.openModalGameDetail = function (world){
				
				$scope.worldMoreInfo = world;
				
				$scope.showGameDetail = true;
				
			}
			
			$scope.infoGame = function (game){
				
				
				infoGame(game.game_game);
				
				
			}
			
			$scope.playGame = function (game){
				
				playGame(game.game_game);
			}
			
			$scope.getWorldByPlayer = function (world_world) {
				
				$scope.world_world = world_world;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/player/world/details/get/"+$scope.world_world;
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.gamesByPlayer = success.data.detalle.games;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	
		            }
		        );
			};
			
			
		  }]
	  );
	
	


})();


function infoGame(idGame){
	
	
	core_setValue('GameDetails.game_game', idGame);
	core_sendForm('form_game_details');
	
	
}



function playGame(idGame){
	
	
	core_setValue('GamePlay.game_game', idGame);
	core_sendForm('form_game_play');
	
	
}