(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			
			
		  }]
	  );
	
	


})();


function createCode(){
	
	core_sendForm('form_addcode');
}

function sincro(){
	
	core_sendForm('form_sincro');
}


function sendCode(){
	
	var toName = $("#toName").val();
	
	if(toName==null){
		
		$("#toName").focus();
		return;
	}
	
	var toEmail = $("#toEmail").val();
	
	if(toEmail==null){
		
		$("#toEmail").focus();
		return;
	}
	
	core_sendForm('form_sendcode');
	
}

function deleteCode(){
	

	core_sendForm('form_deletecode');
	
}


function selectCodeToSend(rcode){
	
	core_setValue('rcode_send', rcode);
}

function selectCodeToDelete(rcode){
	
	core_setValue('rcode_delete', rcode);
}
