(function() {

	var app = angular.module('juegoHackerAngularJsAPP', ['slickCarousel', 'i18nAPPModule'])
	  .controller
	  ('pageAppCtrl', ['$http','$scope', '$interval', '$filter', '$sce', '$window',function($http, $scope, $interval, $filter, $sce, $window)
		  {
		  	$scope.GLOBAL_CONTEXTPATH = GLOBAL_CONTEXTPATH;		
			$scope.showTextEncrypt = false;
		  	
		  	
			$scope.encryptText = function () {
				

			    var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var aesUtil = new AesUtil(128, 1000);
			    var key = $('#key').val();
			    
			    var textToEncrypt = $scope.textToEncrypt;
				
				
			    var ciphertext = aesUtil.encrypt(salt, iv, key, textToEncrypt);
			    var aesText= (iv + "::" + salt + "::" + ciphertext);
			    var textCrypt = btoa(aesText); 
				
				
				
				
							
				var token = GLOBAL_CRSF;
				var URL = GLOBAL_CONTEXTPATH + "/rest/private/admin/user/parameters/crypto";
				
				
				$http({
		            method: 'POST',
		            url: URL,
		            data: textCrypt,
				    headers: {
				    	'X-CSRF-TOKEN':token,	
				    	'Content-Type': 'application/json'
				    }
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta == "OK" || success.data.detalle!=undefined){
		            		
		            		
		            		$scope.textEncrypt = success.data.detalle;
		            		$scope.showTextEncrypt = true;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.showTextEncrypt = false;
		            	
		            	
		            }
		        );
			};
		  	
			
		  }]
	  );
	
	


})();


function createParameter(){
	
	core_sendForm('form_newparameter');
}


function editParameter(cnfg_cnfg,cnfg_valor,cnfg_descri){
	
	$('#edit_parameter_id').val(cnfg_cnfg);
	$('#edit_parameter_id_view').val(cnfg_cnfg);
	$('#edit_parameter_descri').val(cnfg_descri);
	$('#edit_parameter_value').val(cnfg_valor);
	
}

function saveEditParameter(){
	
	core_sendForm('form_editparameter');
}




