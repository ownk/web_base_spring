(function() {
	var app = angular.module('juegoHackerAngularJsAPP', []);
})();


//Controller
(function() {
	angular.module('juegoHackerAngularJsAPP').controller('pageAppCtrl',
			[ '$scope', '$filter', '$http', '$sce', function($scope, $filter, $http, $sce) {

		$scope.isEmailValido = function () {
			
			var emailuser = $scope.emailuser;
			
			if(emailuser != undefined && emailuser.length>0){
			
				$scope.emailuser_show_error= false;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/public/user/forgetpassword/email/"+emailuser;
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		

							$scope.emailuser="";
		            		$scope.emailuser_show_error= true; 
		            		
		            	}
		            	
		            },
		            function(error) { // error

						$scope.emailuser="";
	            		$scope.emailuser_show_error= true; 
		            }
		        );
				
				
			}
		};	
				
	} ]);
})();

function recaptchaCallback() {
    $('#js-login-btn').removeAttr('disabled');
};

function recaptchaCallbackExpired() {
    $('#js-login-btn').attr('disabled','true');
};



function  enviarForm() {
	
	
	var isInfoValid = true;
	
	var invalidElements = [];
	$('.info-to-validate').each(function(i){
		
		$(this).removeClass('border-right-0');
		
		if(!this.checkValidity()){
			isInfoValid = false;
			
		}
	});
	
	
	
	$('#div_info_to_validate').addClass('was-validated');
	
	if(isInfoValid){
		try{
			
			var email_nosecure =  $('#email_nosecure').val();
			
			
		    var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);

		    var aesUtil = new AesUtil(128, 1000);
		    
		    var key = $('#key').val();
		    var texto = email_nosecure;
		    
		    var ciphertext = aesUtil.encrypt(salt, iv, key, texto);

		    var aesEmail= (iv + "::" + salt + "::" + ciphertext);
		    var email = btoa(aesEmail);
		   
		    $('#email').val(email);
		       
		    core_sendForm('email_secure');
			
		}catch(e){
			
		}
	
	}	
	
	
}



