function  enviarForm() {
	
	var pass_nosecure =  $('#pass_nosecure').val();
	var token_nosecure =  $('#token_nosecure').val();
	
	
    var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);

    var aesUtil = new AesUtil(128, 1000);
    
    var key = $('#key').val();
    
    try{
	    var ciphertext_pass = aesUtil.encrypt(salt, iv, key, pass_nosecure);
	    var aesPass= (iv + "::" + salt + "::" + ciphertext_pass);
	    var pass = btoa(aesPass);
	    $('#pass').val(pass);
    }catch(e){
    	
    }
    
    try{
	    var ciphertext_token = aesUtil.encrypt(salt, iv, key, token_nosecure);
	    var aesToken= (iv + "::" + salt + "::" + ciphertext_token);
	    var token = btoa(aesToken);
	    $('#token').val(token);
    }catch(e){
    	
    }

    
       
    core_sendForm('pass_form_secure');
    
    
    
	
}

function recaptchaCallback() {
    $('#js-login-btn').removeAttr('disabled');
};

function recaptchaCallbackExpired() {
    $('#js-login-btn').attr('disabled','true');
};
