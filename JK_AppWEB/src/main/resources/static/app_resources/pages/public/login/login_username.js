$(document).ready(function ($) {
	
	var isButtonDisabled = $("#isButtonLoginDisabled").val();
	
	if(isButtonDisabled==1){
		$('#js-login-btn').attr('disabled','true');
	}
	
	
	$( ".app_lang_radio" ).each(function( index ) {
		
		if($( this ).val() == GLOBAL_LANG_VALUE){
			$( this ).prop("checked", true);
		}
		
		
	});
	
	
	$(".app_lang_radio").change(function () {
		var lang = $('.app_lang_radio:checked').val();
        if (lang != GLOBAL_LANG_VALUE){
        	
        	changeToLang(lang);
        	
        }
    });
	
	var locallang =sessionStorage.getItem(GLOBAL_BROWSER_STORAGE_LANG); 
	
	if (typeof(locallang) != 'undefined' && locallang != null)
	{
	   
		if(locallang != GLOBAL_LANG_VALUE){
			changeToLang(locallang);
		}
		
	}else{
		sessionStorage.clear();
        sessionStorage.setItem(GLOBAL_BROWSER_STORAGE_LANG, GLOBAL_LANG_VALUE);
	}
	
	
});

function changeToLang(lang){
	
	sessionStorage.clear();
    sessionStorage.setItem(GLOBAL_BROWSER_STORAGE_LANG, lang);
    
    var login_url = $("#login_url").val();
    
    var href = window.location.href;
    window.location.href = login_url+"?"+GLOBAL_LANG_PARAM+"="+lang;
	
	
}


function  enviarForm() {
	
	var username_nosecure =  $('#username_nosecure').val();
	
	
    var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);

    var aesUtil = new AesUtil(128, 1000);
    
    var key = $('#key').val();
    var texto = username_nosecure;
    
    var ciphertext = aesUtil.encrypt(salt, iv, key, texto);

    var aesUserName= (iv + "::" + salt + "::" + ciphertext);
    var username = btoa(aesUserName);
    
    
    var lang = $('.app_lang_radio:checked').val();
   
    $('#lang').val(lang);
    $('#username').val(username);
       
    core_sendForm('login_form_secure');
    
    
    
	
}

function recaptchaCallback() {
    $('#js-login-btn').removeAttr('disabled');
};

function recaptchaCallbackExpired() {
    $('#js-login-btn').attr('disabled','true');
};
