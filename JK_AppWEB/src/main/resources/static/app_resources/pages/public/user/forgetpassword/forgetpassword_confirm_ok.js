(function() {
	var app = angular.module('juegoHackerAngularJsAPP', []);
})();


//Controller
(function() {
	angular.module('juegoHackerAngularJsAPP').controller('pageAppCtrl',
			[ '$scope', '$filter', '$http', '$sce', function($scope, $filter, $http, $sce) {

			
		$scope.isPasswordValid = function () {
			
			//var tpid = $scope.client_tpident_nosecure;
			//var nroid = $scope.client_numident_nosecure;
			
			var userName =  $('#user_name_nosecure').val();
			var userNick =  $('#user_nick_nosecure').val();
			var userPass =  $('#user_pass_nosecure').val();
			var user = {};
			
			if(userName != null && userNick!= null && userPass!= ""){
				
				var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var aesUtil = new AesUtil(128, 1000);
			    var key = $('#key').val();
			    
			    var ciphertextUserName = aesUtil.encrypt(salt, iv, key, userName);
			    var aesUserName= (iv + "::" + salt + "::" + ciphertextUserName);
			    var UserNameCrypt = btoa(aesUserName); 
			    
			    var ciphertextUserNick = aesUtil.encrypt(salt, iv, key, userNick);
			    var aesUserNick= (iv + "::" + salt + "::" + ciphertextUserNick);
			    var UserNickCrypt = btoa(aesUserNick); 
			    
			    var ciphertextUserPass = aesUtil.encrypt(salt, iv, key, userPass);
			    var aesUserPass= (iv + "::" + salt + "::" + ciphertextUserPass);
			    var UserPassCrypt = btoa(aesUserPass); 
				
				
				user.user_name = UserNameCrypt;
				user.user_nick = UserNickCrypt;
				user.user_pass = UserPassCrypt;
				
				$scope.pass1_show_error= false;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/public/user/forgetpassword/pass/validation";
				
				$http({
		            method: 'POST',
		            data: angular.toJson(user),
		            url: URL,
		            headers: {
		            	'Content-Type': 'application/json'
		            }
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		
		            		$scope.pass1="";
		            		$scope.pass1_show_error=true;
		            		
		            	}else{
		            		$scope.pass1_show_error=false;
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.pass1="";
	            		$scope.pass1_show_error=true;
		            }
		        );
								
			}
		};		
				
				
				
				
				
		$scope.isPassEquals = function () {
			
			$scope.pass2_show_error= false;
			
			var pass1 = $scope.pass1;
			var pass2 = $scope.pass2;
			
			if(pass1 != pass2){
				$scope.pass2="";
        		$scope.pass2_show_error= true; 
        		$("#js-login-btn").attr("disabled",true);
			} else {
				$("#js-login-btn").attr("disabled",false);
			}
		};
			
			
				
	} ]);
})();



$(document).ready(function ($) {
	
	$("#user_pass_nosecure").bootstrapStrength({
		slimBar: true
	});
	
	
});


function  enviarFormSavePassword() {
	
	
	var isInfoValid = true;
	
	var invalidElements = [];
	$('.info-to-validate').each(function(i){
		
		$(this).removeClass('border-right-0');
		
		if(!this.checkValidity()){
			isInfoValid = false;
			
		}
	});
	
	
	if($('.field_password>.progress>.progress-bar').attr('aria-valuenow')!=100){
		$("#user_pass_nosecure").val("");
		$("#confirmacionPass_nosecure").val("");
		$(".password-not-valid").css("display","block");
		isInfoValid = false;
	}
	
	
	$('#div_info_to_validate').addClass('was-validated');
	
	
	if(isInfoValid){
		try{
			var pass1_nosecure =  $('#user_pass_nosecure').val();
			var pass2_nosecure =  $('#confirmacionPass_nosecure').val();
			
			
		    var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		    var aesUtil = new AesUtil(128, 1000);
		    var key = $('#key').val();


		    
		    var cipherpass1 = aesUtil.encrypt(salt, iv, key, pass1_nosecure);
		    var aesPass1= (iv + "::" + salt + "::" + cipherpass1);
		    var pass1 = btoa(aesPass1);
		   
		    $('#Usuario-user_pass').val(pass1);
		    
		    var cipherpass2 = aesUtil.encrypt(salt, iv, key, pass2_nosecure);
		    var aesPass2= (iv + "::" + salt + "::" + cipherpass2);
		    var pass2 = btoa(aesPass2);
		   
		    $('#confirmacionPass').val(pass2);
		    core_sendForm('save_password_secure');
			
		}catch(e){
			
		}
	}
	
	
    
    
    
	
}
