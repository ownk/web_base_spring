(function() {
	var app = angular.module('juegoHackerAngularJsAPP', []);
})();


//Controller
(function() {
	angular.module('juegoHackerAngularJsAPP').controller('pageAppCtrl',
			[ '$scope', '$filter', '$http', '$sce', function($scope, $filter, $http, $sce) {
				
		
			
			
		$scope.isNickValido = function () {
			//var admin_username = $scope.admin_username;
			var admin_username =  $('#user_nick_nosecure').val();
			
			if(admin_username != undefined && admin_username.length>0){
			
				$scope.admin_username_show_error= false;
				
				var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);

			    var aesUtil = new AesUtil(128, 1000);
			    
			    var key = $('#key').val();
			    
			    var ciphertextNick = aesUtil.encrypt(salt, iv, key, admin_username);
			    var aesNick= (iv + "::" + salt + "::" + ciphertextNick);
			    var nickCrypt = btoa(aesNick); 
			
				var URL = GLOBAL_CONTEXTPATH + "/rest/public/register/client/nick/"+nickCrypt;
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		
		            		$scope.admin_username="";
		            		$scope.admin_username_show_error= true; 
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.admin_username="";
	            		$scope.admin_username_show_error= true; 
		            }
		        );
				
				
			}
		};
		
		
		$scope.isEmailValido = function () {
			//var emailuser = $scope.emailuser;
			var emailuser =  $('#user_email_nosecure').val();
			
			if(emailuser != undefined && emailuser.length>0){
			
				$scope.emailuser_show_error= false;
				
				var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var aesUtil = new AesUtil(128, 1000);
			    var key = $('#key').val();
				
				
				var ciphertextEmailuser = aesUtil.encrypt(salt, iv, key, emailuser);
			    var aesEmailuser= (iv + "::" + salt + "::" + ciphertextEmailuser);
			    var emailUserCrypt = btoa(aesEmailuser); 
			    
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/public/register/client/email/"+emailUserCrypt;
				
				$http({
		            method: 'GET',
		            url: URL
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		
							$scope.emailuser="";
		            		$scope.emailuser_show_error= true; 
		            		
		            	}
		            	
		            },
		            function(error) { // error

						$scope.emailuser="";
	            		$scope.emailuser_show_error= true; 
		            }
		        );
								
			}
		};
		

		
		$scope.isPassEquals = function () {
			
			$scope.pass2_show_error= false;
			
			var pass1 = $scope.pass1;
			var pass2 = $scope.pass2;
			
			if(pass1 != pass2){
				$scope.pass2="";
        		$scope.pass2_show_error= true;				
			}
		};
		
		
		$scope.isClientValid = function () {
			
			//var tpid = $scope.client_tpident_nosecure;
			//var nroid = $scope.client_numident_nosecure;
			
			var nroid =  $('#client_numident_nosecure').val();
			var tpid =  $('#client_tpident_nosecure').val();
			var client = {};
			
			if(tpid != undefined && nroid!= undefined){
				
				
				var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var aesUtil = new AesUtil(128, 1000);
			    var key = $('#key').val();
				
			    var ciphertextTpid= aesUtil.encrypt(salt, iv, key, tpid);
			    var aesTpid= (iv + "::" + salt + "::" + ciphertextTpid);
			    var tpidlCrypt = btoa(aesTpid); 
			    
			    
			    var ciphertextNroid= aesUtil.encrypt(salt, iv, key, nroid);
			    var aesNroid= (iv + "::" + salt + "::" + ciphertextNroid);
			    var nroidlCrypt = btoa(aesNroid);  
				
				client.client_tpident = tpidlCrypt;
				client.client_numident = nroidlCrypt;
				
				$scope.client_numident_show_error= false;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/public/register/client/exist";
				
				$http({
		            method: 'POST',
		            data: angular.toJson(client),
		            url: URL,
		            headers: {
		            	'Content-Type': 'application/json'
		            }
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		
		            		$scope.client_numident_nosecure="";
		            		$scope.client_numident_show_error=true;
		            		
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.client_numident_nosecure="";
	            		$scope.client_numident_show_error=true;
		            }
		        );
								
			}
		};
		
		
		$scope.isPasswordValid = function () {
			
			//var tpid = $scope.client_tpident_nosecure;
			//var nroid = $scope.client_numident_nosecure;
			
			var userName =  $('#user_name_nosecure').val();
			var userNick =  $('#user_nick_nosecure').val();
			var userPass =  $('#user_pass_nosecure').val();
			var clienteName =  $('#client_name_nosecure').val();
			
			var user = {};
			var client = {};
			
			if(userName != null && userNick!= null && userPass!= "" && clienteName!= ""){
				
				var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);

			    var aesUtil = new AesUtil(128, 1000);
			    
			    var key = $('#key').val();
			    
			    var ciphertextUserName = aesUtil.encrypt(salt, iv, key, userName);
			    var aesUserName= (iv + "::" + salt + "::" + ciphertextUserName);
			    var UserNameCrypt = btoa(aesUserName); 
			    
			    var ciphertextUserNick = aesUtil.encrypt(salt, iv, key, userNick);
			    var aesUserNick= (iv + "::" + salt + "::" + ciphertextUserNick);
			    var UserNickCrypt = btoa(aesUserNick); 
			    
			    var ciphertextUserPass = aesUtil.encrypt(salt, iv, key, userPass);
			    var aesUserPass= (iv + "::" + salt + "::" + ciphertextUserPass);
			    var UserPassCrypt = btoa(aesUserPass); 
			    
			    var ciphertextClienteName = aesUtil.encrypt(salt, iv, key, clienteName);
			    var aesClienteName= (iv + "::" + salt + "::" + ciphertextClienteName);
			    var ClienteNameCrypt = btoa(aesClienteName); 
				
				
				user.user_name = UserNameCrypt;
				user.user_nick = UserNickCrypt;
				user.user_pass = UserPassCrypt;
				user.client_name=ClienteNameCrypt;
				
				$scope.pass1_show_error= false;
				
				var URL = GLOBAL_CONTEXTPATH + "/rest/public/register/client/pass/validation";
				
				$http({
		            method: 'POST',
		            data: angular.toJson(user),
		            url: URL,
		            headers: {
		            	'Content-Type': 'application/json'
		            }
		        }).then(
		            function(success) { // success
		            	
		            	if(success.data.codigoRespuesta != "OK" || success.data.detalle!=true){
		            		
		            		$scope.pass1="";
		            		$scope.pass1_show_error=true;
		            		
		            	}else{
		            		$scope.pass1_show_error=false;
		            	}
		            	
		            },
		            function(error) { // error
		            	$scope.pass1="";
	            		$scope.pass1_show_error=true;
		            }
		        );
								
			}
		};
		
				
	} ]);
})();

function recaptchaCallback() {
    $('#js-login-btn').removeAttr('disabled');
};

function recaptchaCallbackExpired() {
    $('#js-login-btn').attr('disabled','true');
};



$(document).ready(function ($) {
	$("#user_pass_nosecure").bootstrapStrength({
		slimBar: true
	});
	
	
	$(":input").inputmask();
	
});


function enviarForm() {
	
	var isInfoValid = true;
	
	var invalidElements = [];
	$('.info-to-validate').each(function(i){
		
		$(this).removeClass('border-right-0');
		
		if(!this.checkValidity()){
			isInfoValid = false;
				
		}
		 
	});
	
	
	if($('.field_password>.progress>.progress-bar').attr('aria-valuenow')!=100){
		$("#user_pass_nosecure").val("");
		$("#confirmacionPass_nosecure").val("");
		$(".password-not-valid").css("display","block");
		isInfoValid = false;
	}
	
	
	$('#div_info_to_validate').addClass('was-validated');
	
	if(isInfoValid){
		try{
			var client_name_nosecure =  $('#client_name_nosecure').val().trim();
			var client_numident_nosecure =  $('#client_numident_nosecure').val().trim();
			var client_tpident_nosecure =  $('#client_tpident_nosecure').val().trim();
			
			var user_name_nosecure =  $('#user_name_nosecure').val().trim();
			var user_nick_nosecure =  $('#user_nick_nosecure').val().trim();
			var user_pass_nosecure =  $('#user_pass_nosecure').val().trim();
			var user_email_nosecure =  $('#user_email_nosecure').val().trim();
			
			var confirmacionPass_nosecure =  $('#confirmacionPass_nosecure').val().trim();
			var terms_nosecure =  $('#terms_nosecure').val();
			var codigoAcceso_nosecure =  $('#codigoAcceso_nosecure').val().trim();
		
			
			
		    var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		    var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		
		    var aesUtil = new AesUtil(128, 1000);
		    
		    var key = $('#key').val();
		  
		    var ciphertextClienteName = aesUtil.encrypt(salt, iv, key, client_name_nosecure);
		    var aesClienteName= (iv + "::" + salt + "::" + ciphertextClienteName);
		    var ClienteName = btoa(aesClienteName); 
		    $('#Client-client_name').val(ClienteName);
		    
		    var ciphertextClienteNumIdent = aesUtil.encrypt(salt, iv, key, client_numident_nosecure);
		    var aesClienteNumIdent= (iv + "::" + salt + "::" + ciphertextClienteNumIdent);
		    var ClienteNumIdent = btoa(aesClienteNumIdent); 
		    $('#Client-client_numident').val(ClienteNumIdent);
		    
		    var ciphertextClienteTipoIdent = aesUtil.encrypt(salt, iv, key, client_tpident_nosecure);
		    var aesClienteTipoIdent= (iv + "::" + salt + "::" + ciphertextClienteTipoIdent);
		    var ClienteTipoIdent = btoa(aesClienteTipoIdent); 
		    $('#Client-client_tpident').val(ClienteTipoIdent);
		    
		    var ciphertextUserName = aesUtil.encrypt(salt, iv, key, user_name_nosecure);
		    var aesUserName= (iv + "::" + salt + "::" + ciphertextUserName);
		    var username = btoa(aesUserName); 
		    $('#Usuario-user_name').val(username);
		    
		    var ciphertextUserNick = aesUtil.encrypt(salt, iv, key, user_nick_nosecure);
		    var aesUserNick= (iv + "::" + salt + "::" + ciphertextUserNick);
		    var userNick = btoa(aesUserNick); 
		    $('#Usuario-user_nick').val(userNick);
		    
		    var ciphertextUserPass = aesUtil.encrypt(salt, iv, key, user_pass_nosecure);
		    var aesUserPass= (iv + "::" + salt + "::" + ciphertextUserPass);
		    var userPass = btoa(aesUserPass); 
		    $('#Usuario-user_pass').val(userPass);
		    
		    
		    var ciphertextUserEmail = aesUtil.encrypt(salt, iv, key, user_email_nosecure);
		    var aesUserEmail= (iv + "::" + salt + "::" + ciphertextUserEmail);
		    var userEmail = btoa(aesUserEmail); 
		    $('#Usuario-user_email').val(userEmail);
		    
		    var ciphertextConfirmarPass = aesUtil.encrypt(salt, iv, key, confirmacionPass_nosecure);
		    var aesUserConfirmarPass= (iv + "::" + salt + "::" + ciphertextConfirmarPass);
		    var confirmarPass = btoa(aesUserConfirmarPass); 
		    $('#confirmacionPass').val(confirmarPass);
		    
		    var ciphertextCodigoAcceso = aesUtil.encrypt(salt, iv, key, codigoAcceso_nosecure);
		    var aesCodigoAcceso= (iv + "::" + salt + "::" + ciphertextCodigoAcceso);
		    var CodigoAccesoCrypt = btoa(aesCodigoAcceso); 
		    $('#codigoAcceso').val(CodigoAccesoCrypt);
		    
		    
		    $('#terms').val(terms_nosecure);
		    $('#terminosCondiciones').val(terms_nosecure);
		   
		    
		
		       
		    core_sendForm('js-login');
		}catch(e){
			
		}
	}
	
	
}

