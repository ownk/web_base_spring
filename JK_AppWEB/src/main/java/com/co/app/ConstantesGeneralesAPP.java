package com.co.app;

public class ConstantesGeneralesAPP {


	/*
	 * -----------------------------------------------------------------------------------------------------------------------
	 * TRANSVERSALES
	 * -----------------------------------------------------------------------------------------------------------------------
	 */
	

	public static final String PAGES_HTML		="pages";
	public static final String PUBLIC_PAGE_PATH 	="/public";
	public static final String PRIVATE_PAGE_PATH 	="/private";

	
	public static final String URL_SYSTEMINFO	  			=PUBLIC_PAGE_PATH+"/systeminfo";
	public static final String URL_LOGIN_USERNAME  			=PUBLIC_PAGE_PATH+"/login/login_username";	
	public static final String URL_LOGIN_PASSWORD 			=PUBLIC_PAGE_PATH+"/login/login_password";
	public static final String URL_ACCESO_DENEGADO 			=PUBLIC_PAGE_PATH+"/access_denied";
	
	public static final String URL_LOGOUT 					=PRIVATE_PAGE_PATH+"/logout";
	
	public static final String ENCONDING = "UTF-8";
	
	
	public static final String USARIO_GENERICO_APP = "APP_GENERIC_USER";
	
	public static final String PATTERN_FECHA	= "yyyy-MM-dd HH:mm:ss";


	/*
	 * -----------------------------------------------------------------------------------------------------------------------
	 * CODIGOS ERROR
	 * -----------------------------------------------------------------------------------------------------------------------
	 */
	public static final String WEB_LANG_PARAMETER = "lang";
	
	
	/*
	 * -----------------------------------------------------------------------------------------------------------------------
	 * CODIGOS ERROR
	 * -----------------------------------------------------------------------------------------------------------------------
	 */
	public static final String PARAMETER_PAGE_ERROR				="PAGE_ERROR"				;
	public static final String PAGE_ERROR_ACCESS_NV				="PAGE_ERROR_ACCESS_NV"		;
	public static final String PAGE_ERROR_PARAMETERS_NV			="PAGE_ERROR_PARAMETERS_NV"	;

	
	public static final String ERROR_SESSION_USER_PASS_NV       = "ERROR_SESSION_USER_PASS_NV"  ;
	public static final String ERROR_CTRL_OPER_NL               = "ERROR_CTRL_OPER_NL"          ;
	public static final String ERROR_SESSION_NL                 = "ERROR_SESSION_NL"            ;
	public static final String ERROR_SESSION_USER_NV            = "ERROR_SESSION_USER_NV"       ;
	public static final String ERROR_BD					        = "ERROR_BD"        			;
	public static final String ERROR_INFO_NV					= "ERROR_INFO_NV"  				;
	public static final String ERROR_SESSION_INT_NV				= "ERROR_SESSION_INT_NV"  		;
	public static final String ERROR_CNFG_NE					= "ERROR_CNFG_NE"  				;
	public static final String ERROR_SPRING_USERNAME_NE			= "ERROR_SPRING_USERNAME_NE"	;
	public static final String ERROR_UNIQUE_SESSION_NV			= "ERROR_UNIQUE_SESSION_NV"		;
	public static final String ERROR_LANG_NV				    = "ERROR_LANG_NV"			    ;
	
	
	
	public static final String MSG_APP_OK          				= "MSG_APP_OK"					  ;
	public static final String MSG_APP_CANCEL_OPER_BY_USER      = "MSG_APP_CANCEL_OPER_BY_USER"	  ;
	public static final String MSG_APP_ERROR_OPER_WITH_ERRORS   = "MSG_APP_ERROR_OPER_WITH_ERRORS";
	public static final String MSG_APP_WARN_EMAIL_NO_SEND		= "MSG_APP_WARN_EMAIL_NO_SEND"	  ;
	public static final String MSG_APP_ERROR_INFO_NV           	= "MSG_APP_ERROR_INFO_NV"         ;
	public static final String MSG_APP_ERROR_GENERAL_NC         = "MFG_APP_ERROR_GENERAL_NC"      ;
	public static final String MSG_APP_ERROR_UNIQUE_SESSION     = "MSG_APP_ERROR_UNIQUE_SESSION"  ;
	public static final String MSG_APP_ERROR_LANG_NV     		= "MSG_APP_ERROR_LANG_NV"         ;
	
	public static final String OK					        	= "OK"        					  ;
	public static final String ERROR				        	= "ERROR"        				  ;


	/*
	 * -----------------------------------------------------------------------------------------------------------------------
	 * Parametros GEnerales
	 * -----------------------------------------------------------------------------------------------------------------------
	 */
	
	public static final String CNFG_APP_LOGO_INTERNET_URL				        ="app.logo.internet.url" 		;
	public static final String CNFG_APP_PAGE_INTERNET_URL				        ="app.owner.internet.url" 		;
	public static final String CNFG_APP_WEB_PUBLIC_URLBASE   		          	="app.web.publico.urlbase";
	public static final String CNFG_LOGIN_USERNAME_ATTEMPT_FAIlED_MAX           ="login.username.attempt_failed.max_attempt";
	public static final String CNFG_FILES_UPLOAD_GENERAL_PATH   		        ="files.upload.general.path";
	public static final String CNFG_CLIENT_PROFILE_IMAGE_UPLOAD_BASEPATH    	="client.profile.image.upload.basepath";
	public static final String CNFG_CLIENT_PROFILE_IMAGE_RELATIVE_BASEURL       ="client.profile.image.relative.baseurl";
	public static final String CNFG_AUT_USER_USERNAME_VALIDATION_REGEX          ="aut.user.username.validation.regex";
	public static final String CNFG_AUT_USER_USERNAME_VALIDATION_FRONT_REGEX    ="aut.user.username.validation.front.regex";
	public static final String CNFG_AUT_USER_EMAIL_VALIDATION_REGEX          	="aut.user.email.validation.regex";
	public static final String CNFG_GENERAL_REGEX_ALFANUMERIC		          	="general.regex.alfanumeric";
	public static final String CNFG_SESSION_INACTIVE_MAX_TIME		          	="session.inactive.max_time";
	
	
	/*
	 * -----------------------------------------------------------------------------------------------------------------------
	 * Parametros Archivo configuracion
	 * -----------------------------------------------------------------------------------------------------------------------
	 */
	
	public static final String APP_POSTGRES_PGCRYTO_KEY							="app.postgres.pgcryto.key";
	public static final String APP_POSTGRES_PGCRYTO_ALGO                        ="app.postgres.pgcryto.algo";
	public static final String APP_SERVER_SERVLET_CONTEXT_PATH                  ="server.servlet.context-path";
	public static final String APP_AES_ENCRYPT_KEY                              ="app.aes.encrypt.key";
	public static final String APP_LOGIN_PASSWORD_ATTEMPT_FAILED_MAX_ATTEMPT    ="app.login.password.attempt-failed.max-attempt";
	public static final String APP_LOGIN_USERNAME_ATTEMPT_FAILED_MAX_ATTEMPT    ="app.login.username.attempt-failed.max-attempt";
	public static final String APP_GOOGLE_RECAPTCHA_URL                         ="app.google.recaptcha.url";
	public static final String APP_GOOGLE_RECAPTCHA_SECRET_KEY                  ="app.google.recaptcha.secret-key";
	public static final String APP_GOOGLE_RECAPTCHA_SITE_KEY                    ="app.google.recaptcha.site-key";

	
	


}




