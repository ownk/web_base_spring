package com.co.app;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.co.app.backend.service.business.commons.AppPararameterCryptoService; 


@Configuration
public class MailConfiguracion {
	
	//Mail Info
	@Value("${app.mail.username}")
	private String mailUsername;
	
	@Value("${app.mail.password}")
	private String mailPassword;

	@Value("${app.mail.port}")
	private String mailPort;

	@Value("${app.mail.host}")
	private String mailHost;

	@Value("${app.mail.properties.smtp.auth}")
	private String mailSmtpAuth;

	@Value("${app.mail.properties.smtp.starttls.enable}")
	private String mailSmtpStarttlsEnable;
	
	@Value("${app.mail.properties.smtp.socketFactory.class}")
	private String mailSmtpSocketFactoryClass;

	@Value("${app.mail.properties.smtp.socketFactory.port}")
	private String mailSmtpSocketFactoryPort;
	
	@Value("${app.mail.transport.protocol}")
	private String mailTrasnsportProtocol;
	


	private ITemplateResolver textTemplateResolver() {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setPrefix("/templates/email/text/");
		templateResolver.setSuffix(".txt");
		templateResolver.setTemplateMode(TemplateMode.TEXT);
		templateResolver.setCharacterEncoding("UTF8");
		templateResolver.setCheckExistence(true);
		templateResolver.setCacheable(false);
		return templateResolver;
	}

	private ITemplateResolver htmlTemplateResolver() {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setPrefix("/templates/email/html/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCharacterEncoding("UTF8");
		templateResolver.setCheckExistence(true);
		templateResolver.setCacheable(false);
		return templateResolver;
	}


	private ITemplateResolver fileTemplateResolver() {
		FileTemplateResolver templateResolver = new FileTemplateResolver();
		templateResolver.setPrefix("/templates/email/file/"); //Change based on your environment
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCharacterEncoding("UTF8");
		templateResolver.setCheckExistence(true);
		templateResolver.setCacheable(false);
		return templateResolver;
	}

	@Bean(name = "textTemplateEngine")
	public TemplateEngine textTemplateEngine() {
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.addTemplateResolver(textTemplateResolver());
		return templateEngine;
	}

	@Bean(name = "htmlTemplateEngine")
	public TemplateEngine htmlTemplateEngine() {
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.addTemplateResolver(htmlTemplateResolver());
		return templateEngine;
	}

	@Bean(name = "fileTemplateEngine")
	public TemplateEngine fileTemplateEngine() {
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.addTemplateResolver(fileTemplateResolver());
		return templateEngine;
	}

	@Bean
	public JavaMailSender getMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		
		int port = Integer.parseInt(getMailPort());
		
		mailSender.setHost(getMailHost());
		mailSender.setPort(port);
		mailSender.setUsername(getMailUsername());
		mailSender.setPassword(getMailPassword());
		Properties javaMailProperties = new Properties();

		javaMailProperties.put("mail.transport.protocol", getMailTrasnsportProtocol());
		javaMailProperties.put("mail.smtp.auth", getMailSmtpAuth());
	    javaMailProperties.put("mail.smtp.starttls.enable", getMailSmtpStarttlsEnable());
	    javaMailProperties.put("mail.smtp.socketFactory.port", getMailSmtpSocketFactoryPort());
	    javaMailProperties.put("mail.smtp.socketFactory.class",getMailSmtpSocketFactoryClass());

		mailSender.setJavaMailProperties(javaMailProperties);
		return mailSender;
	}

	
	public String getMailUsername() {
		
		if (mailUsername != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailUsername);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailPassword() {
		
		if (mailPassword != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailPassword);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailPort() {
		
		if (mailPort != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailPort);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailHost() {
		
		if (mailHost != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailHost);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailSmtpAuth() {
		
		if (mailSmtpAuth != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailSmtpAuth);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailSmtpStarttlsEnable() {
		
		if (mailSmtpStarttlsEnable != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailSmtpStarttlsEnable);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailSmtpSocketFactoryClass() {
		
		
		if (mailSmtpSocketFactoryClass != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailSmtpSocketFactoryClass);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailSmtpSocketFactoryPort() {
		
		if (mailSmtpSocketFactoryPort != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailSmtpSocketFactoryPort);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getMailTrasnsportProtocol() {
		
		if (mailTrasnsportProtocol != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(mailTrasnsportProtocol);

			return decryptText;

		} else {
			return null;
		}
		
	}





}
