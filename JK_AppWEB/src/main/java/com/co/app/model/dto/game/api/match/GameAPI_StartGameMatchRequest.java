package com.co.app.model.dto.game.api.match;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Request to start a match of specific game")
public class GameAPI_StartGameMatchRequest {
	@ApiModelProperty(notes = "Unique identifier of the tokenApp. See more info in service authapp", example = "6ace2bdc-4154-45f6-8c2c-fcbb1731bca2", required = true, position = 0)
	private String appToken;
	@ApiModelProperty(notes = "Unique identifier of game. This Parameter is sent in the game redirection url with the same name. Example: http://[url_game]?gameToken=J%2BnqE8xBMCP%2B7lDF%2BZEThZdRQ%2FuKlE47MjdV%2F8lJ7AntHWGUa6sEIdfZVNm3fOWyq5E%2FFwSbkCw%3D", example = "J%2BnqE8xBMCP%2B7lDF%2BZEThZdRQ%2FuKlE47MjdV%2F8lJ7AntHWGUa6sEIdfZVNm3fOWyq5E%2FFwSbkCw%3D", required = true, position = 1)
	private String gameToken;
	
	
	public String getAppToken() {
		return appToken;
	}


	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public String getGameToken() {
		return gameToken;
	}


	public void setGameToken(String gameToken) {
		this.gameToken = gameToken;
	}


	@Override
	public String toString() {
		return "InitGameRequest [appToken=" + appToken + ", gameToken=" + gameToken + "]";
	}
}
