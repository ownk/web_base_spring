/*
 * MIT License 
 * 
 * Copyright (c) 2018 Ownk
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 */
/**
 * 
 */

package com.co.app.model.dto.systeminfo;

/**
 * <h1>RestInfo</h1>
 *
 * @author Master_Zen (Ownk) 
 * @version 1.0
 */

public class RestInfo {
	
	private String nameClassRest;
	private String namePathRest;
	
	public String getNameClassRest() {
		return nameClassRest;
	}
	public void setNameClassRest(String nameClassRest) {
		this.nameClassRest = nameClassRest;
	}
	public String getNamePathRest() {
		return namePathRest;
	}
	public void setNamePathRest(String namePathRest) {
		this.namePathRest = namePathRest;
	}
	
}
