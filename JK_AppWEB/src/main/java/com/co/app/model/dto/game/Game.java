package com.co.app.model.dto.game;

import java.util.ArrayList;
import java.util.List;

import com.co.app.model.dto.general.MensajeApp;

public class Game implements Comparable<Game>{
	
	public static final String STATE_ACTIVE = "ACT";
	public static final String STATE_INACTIVE = "INA";
	
	public static final String TYPE_GAME_PUBLIC = "PUB";
	public static final String TYPE_GAME_APP= "APP";
	
	private List<LearningObjective> learningObjectives;
	

	private String game_game;
	private String game_name;
	private String game_descri;
	private String game_image;
	private String game_tpgame;
	private String game_url;
	private String game_state;
	private String game_imbibe;
	private String game_trailer;
	private String game_trailer_url;
	private String game_color;

	
	public String getGame_game() {
		return game_game;
	}
	
	public void setGame_game(String game_game) {
		this.game_game = game_game;
	}
	
	public String getGame_name() {
		return game_name;
	}
	
	public void setGame_name(String game_name) {
		this.game_name = game_name;
	}
	
	public String getGame_descri() {
		return game_descri;
	}
	
	public void setGame_descri(String game_descri) {
		this.game_descri = game_descri;
	}
	
	public String getGame_tpgame() {
		return game_tpgame;
	}
	
	public String getGame_image() {
		return game_image;
	}

	public void setGame_image(String game_image) {
		this.game_image = game_image;
	}

	public void setGame_tpgame(String game_tpgame) {
		this.game_tpgame = game_tpgame;
	}

	public String getGame_url() {
		return game_url;
	}

	public void setGame_url(String game_url) {
		this.game_url = game_url;
	}

	public String getGame_state() {
		return game_state;
	}

	public void setGame_state(String game_state) {
		this.game_state = game_state;
	}
	
	public String getGame_imbibe() {
		return game_imbibe;
	}

	public void setGame_imbibe(String game_imbibe) {
		this.game_imbibe = game_imbibe;
	}
	
	public String getGame_trailer() {
		return game_trailer;
	}

	public void setGame_trailer(String game_trailer) {
		this.game_trailer = game_trailer;
	}

	
	public String getGame_trailer_url() {
		return game_trailer_url;
	}

	public void setGame_trailer_url(String game_trailer_url) {
		this.game_trailer_url = game_trailer_url;
	}

	public String getGame_color() {
		return game_color;
	}

	public void setGame_color(String game_color) {
		this.game_color = game_color;
	}

	@Override
	public int compareTo(Game o) {
		
		if(o!=null & game_state!=null) {
			return this.getGame_state().compareTo(o.getGame_state());
		}else {
			return -1;
		}
		
				
	}

	public List<LearningObjective> getLearningObjectives() {
		return learningObjectives;
	}

	public void setLearningObjectives(List<LearningObjective> learningObjectives) {
		this.learningObjectives = learningObjectives;
	}
	
	
	
	
	
	
}