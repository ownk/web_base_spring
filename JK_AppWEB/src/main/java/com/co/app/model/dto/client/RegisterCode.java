package com.co.app.model.dto.client;

import java.util.Date;
import java.util.List;

import com.co.app.model.dto.general.MensajeBD;

public class RegisterCode {
	
	
	public static final String STATE_ACTIVE = "ACT";
	public static final String STATE_USE 	= "USE";
	

	private String rcode_rcode;
	private String rcode_code;
	private String rcode_state;
	private String rcode_client;
	private Date rcode_date_start;
	private Date rcode_date_validity;
	private Date rcode_date_used;
	
	Client client;
	List<SendEmailRegisterCode> sendEmaiRc;

	
	public String getRcode_rcode() {
		return rcode_rcode;
	}
	
	public void setRcode_rcode(String rcode_rcode) {
		this.rcode_rcode = rcode_rcode;
	}
	
	public String getRcode_code() {
		return rcode_code;
	}
	
	public void setRcode_code(String rcode_code) {
		this.rcode_code = rcode_code;
	}
	
	public String getRcode_state() {
		return rcode_state;
	}
	
	public void setRcode_state(String rcode_state) {
		this.rcode_state = rcode_state;
	}
	
	public String getRcode_client() {
		return rcode_client;
	}
	
	public void setRcode_client(String rcode_client) {
		this.rcode_client = rcode_client;
	}
	
	public Date getRcode_date_start() {
		return rcode_date_start;
	}
	
	public void setRcode_date_start(Date rcode_date_start) {
		this.rcode_date_start = rcode_date_start;
	}
	
	public Date getRcode_date_validity() {
		return rcode_date_validity;
	}
	
	public void setRcode_date_validity(Date rcode_date_validity) {
		this.rcode_date_validity = rcode_date_validity;
	}
	
	public Date getRcode_date_used() {
		return rcode_date_used;
	}
	
	public void setRcode_date_used(Date rcode_date_used) {
		this.rcode_date_used = rcode_date_used;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<SendEmailRegisterCode> getSendEmaiRc() {
		return sendEmaiRc;
	}

	public void setSendEmaiRc(List<SendEmailRegisterCode> sendEmaiRc) {
		this.sendEmaiRc = sendEmaiRc;
	}
	
	
	
	
}
