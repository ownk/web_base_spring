package com.co.app.model.dto.game;

public class AttribPlayer{

	private String atrpl_player;
	private String atrpl_chrt;
	private String atrpl_atrb;
	private int atrpl_value;
	private String atrb_descri;
	private String atrb_name;
	private String atrb_icon;
	private String atrb_color;
	
	
	public String getAtrpl_player() {
		return atrpl_player;
	}
	public void setAtrpl_player(String atrpl_player) {
		this.atrpl_player = atrpl_player;
	}
	public String getAtrpl_chrt() {
		return atrpl_chrt;
	}
	public void setAtrpl_chrt(String atrpl_chrt) {
		this.atrpl_chrt = atrpl_chrt;
	}
	public String getAtrpl_atrb() {
		return atrpl_atrb;
	}
	public void setAtrpl_atrb(String atrpl_atrb) {
		this.atrpl_atrb = atrpl_atrb;
	}
	public int getAtrpl_value() {
		return atrpl_value;
	}
	public void setAtrpl_value(int atrpl_value) {
		this.atrpl_value = atrpl_value;
	}
	
	public String getAtrb_descri() {
		return atrb_descri;
	}
	public void setAtrb_descri(String atrb_descri) {
		this.atrb_descri = atrb_descri;
	}
	public String getAtrb_name() {
		return atrb_name;
	}
	public void setAtrb_name(String atrb_name) {
		this.atrb_name = atrb_name;
	}
	public String getAtrb_icon() {
		return atrb_icon;
	}
	public void setAtrb_icon(String atrb_icon) {
		this.atrb_icon = atrb_icon;
	}
	public String getAtrb_color() {
		return atrb_color;
	}
	public void setAtrb_color(String atrb_color) {
		this.atrb_color = atrb_color;
	}
	
	

}
