package com.co.app.model.dto.game;

import java.util.Date;
import java.util.List;

import com.co.app.model.dto.user.User;

public class Player {
	
	
	public static final String PLAYER_DEFAULT_TYPE ="1";
	
	private String 	player_player;
	private String 	player_user;
	private String 	player_name;
	private Date 	player_datecr;
	private Date 	player_dateac;
	private String 	player_type;
	private int 	player_score;
	private String 	player_state;
	private String 	player_tpident;
	private String	player_numident;
	private String 	player_id_ext;
	
	private List<AttribPlayer> listAttributes;
	
	private Avatar avatar;
	private Pet pet;
	private Ship ship;
	private User usuario;
	
	private List<Match> listMatch;
	
	
	public String getPlayer_player() {
		return player_player;
	}
	public void setPlayer_player(String player_player) {
		this.player_player = player_player;
	}
	public String getPlayer_user() {
		return player_user;
	}
	public void setPlayer_user(String player_user) {
		this.player_user = player_user;
	}
	public String getPlayer_name() {
		return player_name;
	}
	public void setPlayer_name(String player_name) {
		this.player_name = player_name;
	}
	public Date getPlayer_datecr() {
		return player_datecr;
	}
	public void setPlayer_datecr(Date player_datecr) {
		this.player_datecr = player_datecr;
	}
	public Date getPlayer_dateac() {
		return player_dateac;
	}
	public void setPlayer_dateac(Date player_dateac) {
		this.player_dateac = player_dateac;
	}
	public String getPlayer_type() {
		return player_type;
	}
	public void setPlayer_type(String player_type) {
		this.player_type = player_type;
	}
	public int getPlayer_score() {
		return player_score;
	}
	public void setPlayer_score(int player_score) {
		this.player_score = player_score;
	}
	public String getPlayer_state() {
		return player_state;
	}
	public void setPlayer_state(String player_state) {
		this.player_state = player_state;
	}
	public String getPlayer_tpident() {
		return player_tpident;
	}
	public void setPlayer_tpident(String player_tpident) {
		this.player_tpident = player_tpident;
	}
	public String getPlayer_numident() {
		return player_numident;
	}
	public void setPlayer_numident(String player_numident) {
		this.player_numident = player_numident;
	}
	public String getPlayer_id_ext() {
		return player_id_ext;
	}
	public void setPlayer_id_ext(String player_id_ext) {
		this.player_id_ext = player_id_ext;
	}
	public List<AttribPlayer> getListAttributes() {
		return listAttributes;
	}
	public void setListAttributes(List<AttribPlayer> listAttributes) {
		this.listAttributes = listAttributes;
	}
	public Avatar getAvatar() {
		return avatar;
	}
	public void setAvatar(Avatar avatar) {
		this.avatar = avatar;
	}
	public Pet getPet() {
		return pet;
	}
	public void setPet(Pet pet) {
		this.pet = pet;
	}
	public Ship getShip() {
		return ship;
	}
	public void setShip(Ship ship) {
		this.ship = ship;
	}
	public User getUsuario() {
		return usuario;
	}
	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}
	
	public List<Match> getListMatch() {
		return listMatch;
	}
	public void setListMatch(List<Match> listMatch) {
		this.listMatch = listMatch;
	}
	
	
	
	
	
	
	
	
	
	
	
}
