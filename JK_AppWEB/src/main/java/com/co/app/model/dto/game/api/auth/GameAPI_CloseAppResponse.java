package com.co.app.model.dto.game.api.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(description = "Class representing a Response  tracked by the application Auth.")
public class GameAPI_CloseAppResponse {
	
	
	@ApiModelProperty(notes = "Close APP result. true/false ", example = "true", required = true, position = 0)
	Boolean isAppClosed;
	
	


	public Boolean getIsAppClosed() {
		return isAppClosed;
	}




	public void setIsAppClosed(Boolean isAppClosed) {
		this.isAppClosed = isAppClosed;
	}




	@Override
	public String toString() {
		return "CloseAppResponse [isAppClosed=" + isAppClosed + "]";
	}
}
