package com.co.app.model.dto.game;

import java.util.Date;

public class Match {

	public static final String STATE_INI = "INI";
	public static final String STATE_END = "END";
	public static final String STATE_PENDING = "PEND";
	
	public static final String RESULT_WIN = "WIN";
	public static final String RESULT_LOSE = "LOSE";
	public static final String RESULT_PENDING = "PEND";
	
	private String match_match;
	private String match_player;
	private String match_game;
	private Date match_start;
	private Date match_end;
	private int match_final_score = 0;
	private int match_game_score = 0;
	private String match_state = STATE_INI;
	private String match_result= RESULT_PENDING;
	
	Game game;
	
	
	public String getMatch_match() {
		return match_match;
	}
	
	public void setMatch_match(String match_match) {
		this.match_match = match_match;
	}
	
	public String getMatch_player() {
		return match_player;
	}
	
	public void setMatch_player(String match_player) {
		this.match_player = match_player;
	}
	
	public String getMatch_game() {
		return match_game;
	}
	
	public void setMatch_game(String match_game) {
		this.match_game = match_game;
	}
	
	public Date getMatch_start() {
		return match_start;
	}

	public void setMatch_start(Date match_start) {
		this.match_start = match_start;
		
	}

	public Date getMatch_end() {
		return match_end;
	}

	public void setMatch_end(Date match_end) {
		this.match_end = match_end;
	}

	public int getMatch_final_score() {
		return match_final_score;
	}
	
	public void setMatch_final_score(int match_final_score) {
		this.match_final_score = match_final_score;
	}

	
	public int getMatch_game_score() {
		return match_game_score;
	}

	public void setMatch_game_score(int match_game_score) {
		this.match_game_score = match_game_score;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getMatch_state() {
		return match_state;
	}

	public void setMatch_state(String match_state) {
		this.match_state = match_state;
	}

	public String getMatch_result() {
		return match_result;
	}

	public void setMatch_result(String match_result) {
		this.match_result = match_result;
	}
	
	
		
	
}
