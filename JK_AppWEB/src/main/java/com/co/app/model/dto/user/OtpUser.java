package com.co.app.model.dto.user;

import java.util.Date;

public class OtpUser {

	private String otpu_otpu;
	private String otpu_otp;
	private Date otpu_date_star;
	private Date otpu_date_end;
	private Date otpu_date_used;
	private String otpu_state;
	private String otpu_user;
	
	
	public String getOtpu_otpu() {
		return otpu_otpu;
	}
	public void setOtpu_otpu(String otpu_otpu) {
		this.otpu_otpu = otpu_otpu;
	}
	public String getOtpu_otp() {
		return otpu_otp;
	}
	public void setOtpu_otp(String otpu_otp) {
		this.otpu_otp = otpu_otp;
	}
	public Date getOtpu_date_star() {
		return otpu_date_star;
	}
	public void setOtpu_date_star(Date otpu_date_star) {
		this.otpu_date_star = otpu_date_star;
	}
	public Date getOtpu_date_end() {
		return otpu_date_end;
	}
	public void setOtpu_date_end(Date otpu_date_end) {
		this.otpu_date_end = otpu_date_end;
	}
	public Date getOtpu_date_used() {
		return otpu_date_used;
	}
	public void setOtpu_date_used(Date otpu_date_used) {
		this.otpu_date_used = otpu_date_used;
	}
	public String getOtpu_state() {
		return otpu_state;
	}
	public void setOtpu_state(String otpu_state) {
		this.otpu_state = otpu_state;
	}
	public String getOtpu_user() {
		return otpu_user;
	}
	public void setOtpu_user(String otpu_user) {
		this.otpu_user = otpu_user;
	}
	
	
}
