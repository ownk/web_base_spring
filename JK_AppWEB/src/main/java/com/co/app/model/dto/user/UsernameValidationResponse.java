package com.co.app.model.dto.user;

public class UsernameValidationResponse {
	
	
	public static final String USERNICK_OK = "OK";
	public static final String USERNICK_ERR_USNV_FULL = "ERR_NICK_USNV_FULL";
	public static final String USERNICK_ERR_USNV_SIMILARYL = "ERR_NICK_USNV_SIMILARY";
	public static final String USERNICK_ERR_NICK_REGEX_FAIL = "ERR_NICK_REGEX_FAIL";
	
	private String messageResponse;
	private String codeResponse;
		
	private boolean isValid;

	public String getMessageResponse() {
		return messageResponse;
	}

	public void setMessageResponse(String messageResponse) {
		this.messageResponse = messageResponse;
	}

	public String getCodeResponse() {
		return codeResponse;
	}

	public void setCodeResponse(String codeResponse) {
		this.codeResponse = codeResponse;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	
	
	
	
		

}
