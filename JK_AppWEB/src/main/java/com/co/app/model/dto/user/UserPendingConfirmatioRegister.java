package com.co.app.model.dto.user;

import com.co.app.model.dto.client.AccessCode;

public class UserPendingConfirmatioRegister {
	
	User user;
	AccessCode accessCode;
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public AccessCode getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(AccessCode accessCode) {
		this.accessCode = accessCode;
	}
	
	

}
