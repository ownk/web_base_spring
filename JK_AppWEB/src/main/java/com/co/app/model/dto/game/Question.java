package com.co.app.model.dto.game;

import java.util.ArrayList;
import java.util.List;

public class Question {
	String questn_questn;
	String questn_descri;
	String answer_answer;
	
	
	List<Answer> answers;
	
	
	public String getQuestn_questn() {
		return questn_questn;
	}
	
	public void setQuestn_questn(String questn_questn) {
		this.questn_questn = questn_questn;
	}
	
	public String getQuestn_descri() {
		return questn_descri;
	}
	
	public void setQuestn_descri(String questn_descri) {
		this.questn_descri = questn_descri;
	}
	
	public void addAnswer(Answer answer) {
		if(answers == null) {
			answers = new ArrayList<Answer>();
			
		}
		
		if(answers.contains(answer)) {

			answers.remove(answer);
		}
		
		answers.add(answer);
		
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public String getAnswer_answer() {
		return answer_answer;
	}

	public void setAnswer_answer(String answer_answer) {
		this.answer_answer = answer_answer;
	}
	
	
	
	
	
}
