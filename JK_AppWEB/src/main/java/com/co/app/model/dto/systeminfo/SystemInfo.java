/*
 * MIT License 
 * 
 * Copyright (c) 2018 Ownk
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 */

package com.co.app.model.dto.systeminfo;

/**
 *
 * <h1>SystemInfo</h1>
 *
 * Dto de las propiedades del sistema donde se ejecuta la aplicacion.
 *
 * @author Master_Zen (Ownk) 
 * @version 1.0
 * 
 */
public class SystemInfo {
	
	private String fileEncoding;
	
	private String javaClassPath;
	private String javaHome;
	private String javaRuntimeVersion;
	private String javaSpecificationName;
	private String javaSpecificationVendor;
	private String javaSpecificationVersion;
	private String javaVendor;
	private String javaVersion;
	
	private String userName;
	private String userTimeZone;
	private String userLanguage;
	private String userCountry;
	private String userDir;
	
	private String osArch;
	private String osName;
	private String osVersion;
	
	public String getFileEncoding() {
		return fileEncoding;
	}
	public void setFileEncoding(String fileEncoding) {
		this.fileEncoding = fileEncoding;
	}
	public String getJavaClassPath() {
		return javaClassPath;
	}
	public void setJavaClassPath(String javaClassPath) {
		this.javaClassPath = javaClassPath;
	}
	public String getJavaHome() {
		return javaHome;
	}
	public void setJavaHome(String javaHome) {
		this.javaHome = javaHome;
	}
	public String getJavaRuntimeVersion() {
		return javaRuntimeVersion;
	}
	public void setJavaRuntimeVersion(String javaRuntimeVersion) {
		this.javaRuntimeVersion = javaRuntimeVersion;
	}
	public String getJavaSpecificationName() {
		return javaSpecificationName;
	}
	public void setJavaSpecificationName(String javaSpecificationName) {
		this.javaSpecificationName = javaSpecificationName;
	}
	public String getJavaSpecificationVendor() {
		return javaSpecificationVendor;
	}
	public void setJavaSpecificationVendor(String javaSpecificationVendor) {
		this.javaSpecificationVendor = javaSpecificationVendor;
	}
	public String getJavaSpecificationVersion() {
		return javaSpecificationVersion;
	}
	public void setJavaSpecificationVersion(String javaSpecificationVersion) {
		this.javaSpecificationVersion = javaSpecificationVersion;
	}
	public String getJavaVendor() {
		return javaVendor;
	}
	public void setJavaVendor(String javaVendor) {
		this.javaVendor = javaVendor;
	}
	public String getJavaVersion() {
		return javaVersion;
	}
	public void setJavaVersion(String javaVersion) {
		this.javaVersion = javaVersion;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserTimeZone() {
		return userTimeZone;
	}
	public void setUserTimeZone(String userTimeZone) {
		this.userTimeZone = userTimeZone;
	}
	public String getUserLanguage() {
		return userLanguage;
	}
	public void setUserLanguage(String userLanguage) {
		this.userLanguage = userLanguage;
	}
	public String getUserCountry() {
		return userCountry;
	}
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	public String getUserDir() {
		return userDir;
	}
	public void setUserDir(String userDir) {
		this.userDir = userDir;
	}
	public String getOsArch() {
		return osArch;
	}
	public void setOsArch(String osArch) {
		this.osArch = osArch;
	}
	public String getOsName() {
		return osName;
	}
	public void setOsName(String osName) {
		this.osName = osName;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
}
