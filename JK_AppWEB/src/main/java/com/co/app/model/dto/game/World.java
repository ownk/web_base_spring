package com.co.app.model.dto.game;

import java.util.List;

public class World implements Comparable<World>{
	
	public static final String STATE_ACTIVE = "ACT";
	public static final String STATE_INACTIVE = "INA";

	
	private String world_world;
	private String world_name;
	private String world_descri;
	
	private double world_positn_x;
	private double world_positn_y;
	private double world_size;
	
	private String world_picture;
	private String world_state;
	
	private List<Game> games;
	
	
	public String getWorld_world() {
		return world_world;
	}
	public void setWorld_world(String world_world) {
		this.world_world = world_world;
	}	
	public String getWorld_name() {
		return world_name;
	}
	public void setWorld_name(String world_name) {
		this.world_name = world_name;
	}
	public String getWorld_descri() {
		return world_descri;
	}
	public void setWorld_descri(String world_descri) {
		this.world_descri = world_descri;
	}
	public double getWorld_positn_x() {
		return world_positn_x;
	}
	public void setWorld_positn_x(double world_positn_x) {
		this.world_positn_x = world_positn_x;
	}
	public double getWorld_positn_y() {
		return world_positn_y;
	}
	public void setWorld_positn_y(double world_positn_y) {
		this.world_positn_y = world_positn_y;
	}
	public double getWorld_size() {
		return world_size;
	}
	public void setWorld_size(double world_size) {
		this.world_size = world_size;
	}
	public String getWorld_picture() {
		return world_picture;
	}
	public void setWorld_picture(String world_picture) {
		this.world_picture = world_picture;
	}
	public String getWorld_state() {
		return world_state;
	}
	public void setWorld_state(String world_state) {
		this.world_state = world_state;
	}
	public List<Game> getGames() {
		return games;
	}
	public void setGames(List<Game> games) {
		this.games = games;
	}
	
	
	@Override
	public int compareTo(World o) {
		

		if(o!=null & world_state!=null) {
			return this.getWorld_state().compareTo(o.getWorld_state());
		}else {
			return -1;
		}
		
		
	}
	
	
	

	
}
