package com.co.app.model.dto.account;


public class AcAccount {

	private String acac_acode;
	private String acac_acnt;
	
	public String getAcac_acode() {
		return acac_acode;
	}
	public void setAcac_acode(String acac_acode) {
		this.acac_acode = acac_acode;
	}
	public String getAcac_acnt() {
		return acac_acnt;
	}
	public void setAcac_acnt(String acac_acnt) {
		this.acac_acnt = acac_acnt;
	}
	
	
}
