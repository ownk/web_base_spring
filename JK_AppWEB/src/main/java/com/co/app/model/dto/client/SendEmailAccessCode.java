package com.co.app.model.dto.client;

import java.util.Date;

public class SendEmailAccessCode {

	private String seac_seac; 		
	private String seac_name;
	private String seac_email;		
	private String seac_acode;		
    private String seac_user;		
    private Date seac_date_send;
    
    
	public String getSeac_seac() {
		return seac_seac;
	}
	public void setSeac_seac(String seac_seac) {
		this.seac_seac = seac_seac;
	}
	public String getSeac_name() {
		return seac_name;
	}
	public void setSeac_name(String seac_name) {
		this.seac_name = seac_name;
	}
	public String getSeac_email() {
		return seac_email;
	}
	public void setSeac_email(String seac_email) {
		this.seac_email = seac_email;
	}
	public String getSeac_acode() {
		return seac_acode;
	}
	public void setSeac_acode(String seac_acode) {
		this.seac_acode = seac_acode;
	}
	public String getSeac_user() {
		return seac_user;
	}
	public void setSeac_user(String seac_user) {
		this.seac_user = seac_user;
	}
	public Date getSeac_date_send() {
		return seac_date_send;
	}
	public void setSeac_date_send(Date seac_date_send) {
		this.seac_date_send = seac_date_send;
	}

    
	
}
