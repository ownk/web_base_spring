package com.co.app.model.dto.game;

public class AnswerQuestion {
	
	String anwqtn_answer;
	String anwqtn_questn;
	
	
	public String getAnwqtn_answer() {
		return anwqtn_answer;
	}
	
	public void setAnwqtn_answer(String anwqtn_answer) {
		this.anwqtn_answer = anwqtn_answer;
	}
	
	public String getAnwqtn_questn() {
		return anwqtn_questn;
	}
	
	public void setAnwqtn_questn(String anwqtn_questn) {
		this.anwqtn_questn = anwqtn_questn;
	}
	
}
