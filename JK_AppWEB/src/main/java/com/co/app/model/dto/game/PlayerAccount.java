package com.co.app.model.dto.game;


public class PlayerAccount {
	
		
	private String placn_player;
	private String placn_acnt;
	
	
	public String getPlacn_player() {
		return placn_player;
	}
	
	public void setPlacn_player(String placn_player) {
		this.placn_player = placn_player;
	}
	
	public String getPlacn_acnt() {
		return placn_acnt;
	}
	
	public void setPlacn_acnt(String placn_acnt) {
		this.placn_acnt = placn_acnt;
	}
	
	
	
	
	
	
}
