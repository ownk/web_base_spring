package com.co.app.model.dto.game;

import java.util.List;

public class Pet {

	private String pet_pet;
	private String pet_name;
	private String pet_descri;
	private String pet_skin_url;
	private String pet_chrt;
	
	private List<AttribChrt> listAttributes;
	
	public String getPet_pet() {
		return pet_pet;
	}
	
	public void setPet_pet(String pet_pet) {
		this.pet_pet = pet_pet;
	}
	
	public String getPet_name() {
		return pet_name;
	}
	
	public void setPet_name(String pet_name) {
		this.pet_name = pet_name;
	}
	
	public String getPet_descri() {
		return pet_descri;
	}
	
	public void setPet_descri(String pet_descri) {
		this.pet_descri = pet_descri;
	}

	public String getPet_skin_url() {
		return pet_skin_url;
	}

	public void setPet_skin_url(String pet_skin_url) {
		this.pet_skin_url = pet_skin_url;
	}

	public String getPet_chrt() {
		return pet_chrt;
	}

	public void setPet_chrt(String pet_chrt) {
		this.pet_chrt = pet_chrt;
	}

	
	public List<AttribChrt> getListAttributes() {
		return listAttributes;
	}

	public void setListAttributes(List<AttribChrt> listAttributes) {
		this.listAttributes = listAttributes;
	}	

	
}
