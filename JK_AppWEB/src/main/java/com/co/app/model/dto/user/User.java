package com.co.app.model.dto.user;

import java.util.Date;

public class User {

	private String user_user;
	private String user_name;
	private String user_nick;
	private String user_pass;
	private String user_email;
	private String user_state;
	private Date user_dcrea;
	private String user_state_desc;
	
	private OtpUser otpUser;
	
	public String getUser_user() {
		return user_user;
	}

	public void setUser_user(String user_user) {
		this.user_user = user_user;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_nick() {
		return user_nick;
	}

	public void setUser_nick(String user_nick) {
		this.user_nick = user_nick;
	}

	public String getUser_pass() {
		return user_pass;
	}

	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUser_state() {
		return user_state;
	}

	public void setUser_state(String user_state) {
		this.user_state = user_state;
	}

	public Date getUser_dcrea() {
		return user_dcrea;
	}

	public void setUser_dcrea(Date user_dcrea) {
		this.user_dcrea = user_dcrea;
	}

	public String getUser_state_desc() {
		return user_state_desc;
	}

	public void setUser_state_desc(String user_state_desc) {
		this.user_state_desc = user_state_desc;
	}

	public OtpUser getOtpUser() {
		return otpUser;
	}

	public void setOtpUser(OtpUser otpUser) {
		this.otpUser = otpUser;
	}
	
	
	
}
