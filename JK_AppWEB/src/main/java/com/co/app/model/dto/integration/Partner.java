package com.co.app.model.dto.integration;

import java.util.Arrays;
import java.util.Map;


public class Partner {

	private String partnr_partnr;
	private String partnr_name;
	private String partnr_sitekey;
	private String partnr_accesskey;
	private String partnr_state;
	
	
	public String getPartnr_partnr() {
		return partnr_partnr;
	}
	public void setPartnr_partnr(String partnr_partnr) {
		this.partnr_partnr = partnr_partnr;
	}
	public String getPartnr_name() {
		return partnr_name;
	}
	public void setPartnr_name(String partnr_name) {
		this.partnr_name = partnr_name;
	}
	public String getPartnr_sitekey() {
		return partnr_sitekey;
	}
	public void setPartnr_sitekey(String partnr_sitekey) {
		this.partnr_sitekey = partnr_sitekey;
	}
	public String getPartnr_accesskey() {
		return partnr_accesskey;
	}
	public void setPartnr_accesskey(String partnr_accesskey) {
		this.partnr_accesskey = partnr_accesskey;
	}
	public String getPartnr_state() {
		return partnr_state;
	}
	public void setPartnr_state(String partnr_state) {
		this.partnr_state = partnr_state;
	}
	
	
	
	

}
