package com.co.app.model.dto.account;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.game.Player;

public class Account {

	private String acnt_acnt;
	private Date acnt_datecr;
	private String acnt_state;
	private Date acnt_date_opening;
	private Date acnt_date_validity;
	private String acnt_tpac;
	private String acnt_acode_init;
	
	Player playerPpal;
	AccessCode accessCodeInit;
	
	
	List<Player> players;
	List<AccessCode> accessCodes;
	
	public String getAcnt_acnt() {
		return acnt_acnt;
	}
	
	public void setAcnt_acnt(String acnt_acnt) {
		this.acnt_acnt = acnt_acnt;
	}
	
	public Date getAcnt_datecr() {
		return acnt_datecr;
	}
	
	public void setAcnt_datecr(Date acnt_datecr) {
		this.acnt_datecr = acnt_datecr;
	}
	
	public String getAcnt_state() {
		return acnt_state;
	}
	
	public void setAcnt_state(String acnt_state) {
		this.acnt_state = acnt_state;
	}
	
	public Date getAcnt_date_opening() {
		return acnt_date_opening;
	}
	
	public void setAcnt_date_opening(Date acnt_date_opening) {
		this.acnt_date_opening = acnt_date_opening;
	}
	
	public Date getAcnt_date_validity() {
		return acnt_date_validity;
	}
	
	public void setAcnt_date_validity(Date acnt_date_validity) {
		this.acnt_date_validity = acnt_date_validity;
	}
	
	public String getAcnt_tpac() {
		return acnt_tpac;
	}
	
	public void setAcnt_tpac(String acnt_tpac) {
		this.acnt_tpac = acnt_tpac;
	}
	
	public String getAcnt_acode_init() {
		return acnt_acode_init;
	}
	
	public void setAcnt_acode_init(String acnt_acode_init) {
		this.acnt_acode_init = acnt_acode_init;
	}

	public Player getPlayerPpal() {
		return playerPpal;
	}

	public void setPlayerPpal(Player playerPpal) {
		this.playerPpal = playerPpal;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public List<AccessCode> getAccessCodes() {
		return accessCodes;
	}

	public void setAccessCodes(List<AccessCode> accessCodes) {
		this.accessCodes = accessCodes;
	}
	
	
	
	
	public AccessCode getAccessCodeInit() {
		return accessCodeInit;
	}

	public void setAccessCodeInit(AccessCode accessCodeInit) {
		this.accessCodeInit = accessCodeInit;
	}

	public void addPlayer(Player player) {
		if(players == null) {
			players = new ArrayList<Player>();
			
		}
		
		if(players.contains(player)) {

			players.remove(player);
		}
		
		playerPpal = player;
		
		players.add(player);
		
	}
	
	
	public void addAccesCode(AccessCode accessCode) {
		if(accessCodes == null) {
			accessCodes = new ArrayList<AccessCode>();
			
		}
		
		if(accessCodes.contains(accessCode)) {

			accessCodes.remove(accessCode);
		}
		
		accessCodes.add(accessCode);
		
	}
	

	
	
}
