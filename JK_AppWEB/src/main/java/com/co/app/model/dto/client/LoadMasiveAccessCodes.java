package com.co.app.model.dto.client;

import java.util.Date;

public class LoadMasiveAccessCodes {
	
	public static final String STATE_PENDING = "PEND";
	public static final String STATE_END = "END";
	public static final String STATE_PROCESSING = "PRO";
	
	
	
	private String lmac_lmac;     
	private String lmac_state;    
	private int lmac_file_trg; 
	private String lmac_file_url;
	private Date lmac_date_init;
	private Date lmac_date_end; 
    private String lmac_user; 		
    private String lmac_client;   
    private String lmac_templt;
    private String lmac_descri;
    
    
    
    TemplateGames templateGames;
    
    
	public String getLmac_lmac() {
		return lmac_lmac;
	}
	public void setLmac_lmac(String lmac_lmac) {
		this.lmac_lmac = lmac_lmac;
	}
	public String getLmac_state() {
		return lmac_state;
	}
	public void setLmac_state(String lmac_state) {
		this.lmac_state = lmac_state;
	}
	public int getLmac_file_trg() {
		return lmac_file_trg;
	}
	public void setLmac_file_trg(int lmac_file_trg) {
		this.lmac_file_trg = lmac_file_trg;
	}
	public String getLmac_file_url() {
		return lmac_file_url;
	}
	public void setLmac_file_url(String lmac_file_url) {
		this.lmac_file_url = lmac_file_url;
	}
	public Date getLmac_date_init() {
		return lmac_date_init;
	}
	public void setLmac_date_init(Date lmac_date_init) {
		this.lmac_date_init = lmac_date_init;
	}
	public Date getLmac_date_end() {
		return lmac_date_end;
	}
	public void setLmac_date_end(Date lmac_date_end) {
		this.lmac_date_end = lmac_date_end;
	}
	public String getLmac_user() {
		return lmac_user;
	}
	public void setLmac_user(String lmac_user) {
		this.lmac_user = lmac_user;
	}
	public String getLmac_client() {
		return lmac_client;
	}
	public void setLmac_client(String lmac_client) {
		this.lmac_client = lmac_client;
	}
	public String getLmac_templt() {
		return lmac_templt;
	}
	public void setLmac_templt(String lmac_templt) {
		this.lmac_templt = lmac_templt;
	}
	public String getLmac_descri() {
		return lmac_descri;
	}
	public void setLmac_descri(String lmac_descri) {
		this.lmac_descri = lmac_descri;
	}
	public TemplateGames getTemplateGames() {
		return templateGames;
	}
	public void setTemplateGames(TemplateGames templateGames) {
		this.templateGames = templateGames;
	}  
	
	
	
	
	
}
