package com.co.app.model.dto.game.api.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "API authentication request")
public class GameAPI_AuthRequest {

	@ApiModelProperty(notes = "Unique identifier of the siteKey for a partner. No two siteKey can have the same id. ", example = "SiteKeyP1", required = true, position = 2)
	private String siteKey;

	@ApiModelProperty(notes = "Unique identifier of the accessKey for a partner. No two accessKey can have the same id.", example = "AccessKeyP1", required = true, position = 0)
	private String accessKey;

	public String getSiteKey() {
		return siteKey;
	}

	public void setSiteKey(String siteKey) {
		this.siteKey = siteKey;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	@Override
	public String toString() {
		return "AuthRequest [accessKey=" + accessKey + ", siteKey=" + siteKey + "]";
	}

}
