package com.co.app.model.dto.session;

import java.util.Date;

public class UsuarioIPNavegadorSessionDTO {
	String login;
	Date fechaUltimoIngreso;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public Date getFechaUltimoIngreso() {
		return fechaUltimoIngreso;
	}
	public void setFechaUltimoIngreso(Date fechaUltimoIngreso) {
		this.fechaUltimoIngreso = fechaUltimoIngreso;
	}
	@Override
	public String toString() {
		return "UsuarioIPNavegadorSessionDTO [login=" + login + ", fechaUltimoIngreso="
				+ fechaUltimoIngreso + "]";
	}
	

	

	
}
