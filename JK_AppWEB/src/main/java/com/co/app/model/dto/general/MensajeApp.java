package com.co.app.model.dto.general;

public class MensajeApp {
	
	public final static String TIPO_EXITO = "success";
	public final static String TIPO_INFO = "info";
	public final static String TIPO_ADVERTENCIA = "warning";
	public final static String TIPO_ERROR = "error";
	
	String tipoMensaje;
	String procesoMensaje;
	String datoMensaje;
	
	public MensajeApp() {
		tipoMensaje = null;
		procesoMensaje = null;
		datoMensaje = null;
	}
	
	public MensajeApp(String tipoMensaje, String procesoMensaje, String datoMensaje) {
		super();
		this.tipoMensaje = tipoMensaje;
		this.procesoMensaje = procesoMensaje;
		this.datoMensaje = datoMensaje;
	}
	public String getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	public String getDatoMensaje() {
		return datoMensaje;
	}
	public void setDatoMensaje(String datoMensaje) {
		this.datoMensaje = datoMensaje;
	}
	public String getProcesoMensaje() {
		return procesoMensaje;
	}
	public void setProcesoMensaje(String procesoMensaje) {
		this.procesoMensaje = procesoMensaje;
	}
	@Override
	public String toString() {
		return "tmens=" + tipoMensaje + "&pmens=" + procesoMensaje + "&dmens=" + datoMensaje;
	}
	
	

}
