package com.co.app.model.dto.general;

import com.co.app.model.dto.session.SessionAppUsuario;

public class ControlOperacionServicio {
	
	String usuario;
	String ip;
	String navegador;
	
	
	public ControlOperacionServicio(String usuario,	String ip,	String navegador) {
		this.usuario = usuario; 
		this.ip = ip;
		this.navegador = navegador; 
	}
	
	public ControlOperacionServicio(SessionAppUsuario sessionAppUsuario) {
		this.usuario = sessionAppUsuario.getUsuario().getUser_user(); 
		this.ip = sessionAppUsuario.getIp();
		this.navegador = sessionAppUsuario.getNavegador(); 
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getNavegador() {
		return navegador;
	}
	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}
	
	
	

}
