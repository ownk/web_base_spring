package com.co.app.model.dto.game.api.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a Auth tracked by the application game.")
public class GameAPI_CloseAppRequest {

	@ApiModelProperty(notes = "Unique identifier of the siteKey. No two siteKey can have the same id.", example = "SK1234", required = true, position = 2)
	private String siteKey;

	@ApiModelProperty(notes = "Unique identifier of the accessKey. No two keys can have the same id.", example = "AK1234", required = true, position = 0)
	private String accessKey;

	@ApiModelProperty(notes = "Unique identifier of the appToken. No two keys can have the same id.", example = "AT1234", required = true, position = 0)
	private String appToken;
	
	public String getSiteKey() {
		return siteKey;
	}

	public void setSiteKey(String siteKey) {
		this.siteKey = siteKey;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	
	

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	@Override
	public String toString() {
		return "AuthRequest [accessKey=" + accessKey + ", siteKey=" + siteKey + "]";
	}

}
