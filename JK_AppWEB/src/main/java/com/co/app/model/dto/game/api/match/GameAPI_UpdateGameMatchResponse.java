package com.co.app.model.dto.game.api.match;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Response to update a match of a specific game")
public class GameAPI_UpdateGameMatchResponse {
	
	@ApiModelProperty(notes = "Result of a match update. true/false ", example = "true", required = true, position = 0)
	Boolean isGameUpdated;
	
	
	public Boolean getIsGameUpdated() {
		return isGameUpdated;
	}


	public void setIsGameUpdated(Boolean isGameUpdated) {
		this.isGameUpdated = isGameUpdated;
	}

	@Override
	public String toString() {
		return "GameAPI_RegisterGameDataMatchResponse [isGameUpdate=" + isGameUpdated + "]";
	}
	
}
