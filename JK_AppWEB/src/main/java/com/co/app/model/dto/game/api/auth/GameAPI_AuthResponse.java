package com.co.app.model.dto.game.api.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(description = "Response for game API authentication")
public class GameAPI_AuthResponse {
	@ApiModelProperty(notes = "\r\n" + 
			"Unique code of authenticated application. Code to be used for the use of other services", example = "6ace2bdc-4154-45f6-8c2c-fcbb1731bca2", required = true, position = 0)
	private String appToken;

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}


	@Override
	public String toString() {
		return "AuthResponse [appToken=" + appToken + "]";
	}
}
