package com.co.app.model.dto.client;

import java.util.List;

import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;

public class PlayerExternalIDResponse {
	
	List<ErrorBD> errores ;
	List<MensajeBD> mensajes;
	List<Player> players;
	
	
	public List<ErrorBD> getErrores() {
		return errores;
	}
	public void setErrores(List<ErrorBD> errores) {
		this.errores = errores;
	}
	public List<MensajeBD> getMensajes() {
		return mensajes;
	}
	public void setMensajes(List<MensajeBD> mensajes) {
		this.mensajes = mensajes;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	
	
	
	

}
