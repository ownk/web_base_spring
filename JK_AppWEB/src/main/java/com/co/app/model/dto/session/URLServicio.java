package com.co.app.model.dto.session;

public class URLServicio {
	
	private String surl_url    ;
    private Long surl_srvc     ;   
    private String surl_tipo   ;
    
    
	public String getSurl_url() {
		return surl_url;
	}
	public void setSurl_url(String surl_url) {
		this.surl_url = surl_url;
	}
	public Long getSurl_srvc() {
		return surl_srvc;
	}
	public void setSurl_srvc(Long surl_srvc) {
		this.surl_srvc = surl_srvc;
	}
	public String getSurl_tipo() {
		return surl_tipo;
	}
	public void setSurl_tipo(String surl_tipo) {
		this.surl_tipo = surl_tipo;
	}
    
    
	

	
}
