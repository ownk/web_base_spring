package com.co.app.model.dto.client;

import java.util.Date;
import java.util.List;

import com.co.app.model.dto.general.MensajeBD;

public class Client {

	private String client_client;
	private String client_tpident;
	private String client_numident;
	private String client_name;
	private String client_state;
	private String client_image;
	private Date client_datecr;
	private String client_state_desc;

	
	// Persona
	List<MensajeBD> mensajes;
	
	
	public String getClient_client() {
		return client_client;
	}


	public void setClient_client(String client_client) {
		this.client_client = client_client;
	}


	public String getClient_tpident() {
		return client_tpident;
	}


	public void setClient_tpident(String client_tpident) {
		this.client_tpident = client_tpident;
	}


	public String getClient_numident() {
		return client_numident;
	}


	public void setClient_numident(String client_numident) {
		this.client_numident = client_numident;
	}


	public String getClient_name() {
		return client_name;
	}


	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}


	public String getClient_state() {
		return client_state;
	}


	public void setClient_state(String client_state) {
		this.client_state = client_state;
	}

	public String getClient_image() {
		return client_image;
	}


	public void setClient_image(String client_image) {
		this.client_image = client_image;
	}

	public Date getClient_datecr() {
		return client_datecr;
	}


	public void setClient_datecr(Date client_datecr) {
		this.client_datecr = client_datecr;
	}


	public String getClient_state_desc() {
		return client_state_desc;
	}


	public void setClient_state_desc(String client_state_desc) {
		this.client_state_desc = client_state_desc;
	}
	
	
	
}
