package com.co.app.model.dto.game;

import java.util.List;

public class Ship {

	private String ship_ship;
	private String ship_name;
	private String ship_descri;
	private String ship_skin_url;
	private String ship_chrt;
	
	private List<AttribChrt> listAttributes;
	
	public String getShip_ship() {
		return ship_ship;
	}
	
	public void setShip_ship(String ship_ship) {
		this.ship_ship = ship_ship;
	}
	
	public String getShip_name() {
		return ship_name;
	}
	
	public void setShip_name(String ship_name) {
		this.ship_name = ship_name;
	}
	
	public String getShip_descri() {
		return ship_descri;
	}
	
	public void setShip_descri(String ship_descri) {
		this.ship_descri = ship_descri;
	}

	public String getShip_skin_url() {
		return ship_skin_url;
	}

	public void setShip_skin_url(String ship_skin_url) {
		this.ship_skin_url = ship_skin_url;
	}

	public String getShip_chrt() {
		return ship_chrt;
	}

	public void setShip_chrt(String ship_chrt) {
		this.ship_chrt = ship_chrt;
	}
	
	public List<AttribChrt> getListAttributes() {
		return listAttributes;
	}

	public void setListAttributes(List<AttribChrt> listAttributes) {
		this.listAttributes = listAttributes;
	}	
	
	
}
