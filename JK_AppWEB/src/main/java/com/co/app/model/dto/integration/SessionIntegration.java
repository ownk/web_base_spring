package com.co.app.model.dto.integration;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;


public class SessionIntegration {

	private String session_session;
	private String session_partnr;
	private String session_tkn;
	private Date session_date_star;
	private Date session_date_validate;
	private Date session_date_end;
	private String session_state;
	
	
	public String getSession_session() {
		return session_session;
	}
	
	public void setSession_session(String session_session) {
		this.session_session = session_session;
	}
	
	public String getSession_partnr() {
		return session_partnr;
	}
	
	public void setSession_partnr(String session_partnr) {
		this.session_partnr = session_partnr;
	}
	
	public String getSession_tkn() {
		return session_tkn;
	}
	
	public void setSession_tkn(String session_tkn) {
		this.session_tkn = session_tkn;
	}
	
	public Date getSession_date_star() {
		return session_date_star;
	}
	
	public void setSession_date_star(Date session_date_star) {
		this.session_date_star = session_date_star;
	}
	
	public Date getSession_date_validate() {
		return session_date_validate;
	}
	
	public void setSession_date_validate(Date session_date_validate) {
		this.session_date_validate = session_date_validate;
	}
	
	public Date getSession_date_end() {
		return session_date_end;
	}
	
	public void setSession_date_end(Date session_date_end) {
		this.session_date_end = session_date_end;
	}
	
	public String getSession_state() {
		return session_state;
	}
	
	public void setSession_state(String session_state) {
		this.session_state = session_state;
	}
	
	
}
