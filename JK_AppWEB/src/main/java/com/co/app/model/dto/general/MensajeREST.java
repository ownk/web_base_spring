package com.co.app.model.dto.general;

public class MensajeREST<T> {
	
	public static final String CODIGO_RESPUESTA_REST_OK = "OK";
	public static final String CODIGO_RESPUESTA_REST_ERROR_INFO_NV = "ERROR_REST_INFO_NV";
	public static final String CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED = "ERROR_REST_NO_AUTHORIZED";
	public static final String CODIGO_RESPUESTA_REST_ERROR_NC = "ERROR_REST_NC";
	public static final String CODIGO_RESPUESTA_REST_ERROR_SESSION_NL = "ERROR_REST_SESSION_NL";
	
	private T detalle;
	private String codigoRespuesta;

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public T getDetalle() {
		return detalle;
	}

	public void setDetalle(T detalle) {
		this.detalle = detalle;
	}


	
	
	
	
	

}
