package com.co.app.model.dto.game;

public class Score {

	private String score_score;
	private String score_match;
	private String score_chrt;
	private String score_atrb;
	private int score_value;
	public String getScore_score() {
		return score_score;
	}
	public void setScore_score(String score_score) {
		this.score_score = score_score;
	}
	public String getScore_match() {
		return score_match;
	}
	public void setScore_match(String score_match) {
		this.score_match = score_match;
	}
	public String getScore_chrt() {
		return score_chrt;
	}
	public void setScore_chrt(String score_chrt) {
		this.score_chrt = score_chrt;
	}
	public String getScore_atrb() {
		return score_atrb;
	}
	public void setScore_atrb(String score_atrb) {
		this.score_atrb = score_atrb;
	}
	public int getScore_value() {
		return score_value;
	}
	public void setScore_value(int score_value) {
		this.score_value = score_value;
	}
	
	
	
	
		
	
}
