package com.co.app.model.dto.session;

import java.util.ArrayList;
import java.util.List;

public class Menu {
	
	  Long menu_menu;
	  Long menu_orden;
	  String menu_nombre;
	  String menu_descri;
	  String menu_icon;
  
	List<Servicio> services;  
	
	Boolean isMenuSelected = false;
	  
	public List<Servicio> getServices() {
		return services;
	}
	public void setServices(List<Servicio> services) {
		this.services = services;
	}
	public Long getMenu_menu() {
		return menu_menu;
	}
	public void setMenu_menu(Long menu_menu) {
		this.menu_menu = menu_menu;
	}
	public Long getMenu_orden() {
		return menu_orden;
	}
	public void setMenu_orden(Long menu_orden) {
		this.menu_orden = menu_orden;
	}
	public String getMenu_nombre() {
		return menu_nombre;
	}
	public void setMenu_nombre(String menu_nombre) {
		this.menu_nombre = menu_nombre;
	}
	public String getMenu_descri() {
		return menu_descri;
	}
	public void setMenu_descri(String menu_descri) {
		this.menu_descri = menu_descri;
	}
	public String getMenu_icon() {
		return menu_icon;
	}
	public void setMenu_icon(String menu_icon) {
		this.menu_icon = menu_icon;
	}
	  
	public void addService(Servicio service) {
		if(services == null) {
			services = new ArrayList<Servicio>();
			
		}
		
		if(services.contains(service)) {

			services.remove(service);
		}
		
		services.add(service);
		
	}
	public Boolean getIsMenuSelected() {
		return isMenuSelected;
	}
	public void setIsMenuSelected(Boolean isMenuSelected) {
		this.isMenuSelected = isMenuSelected;
	}
	  
	
	  
  
	
	

}
