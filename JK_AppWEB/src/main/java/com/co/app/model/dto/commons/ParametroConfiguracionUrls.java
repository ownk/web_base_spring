package com.co.app.model.dto.commons;

public class ParametroConfiguracionUrls {
	
	
	Long urls_urls;
	String urls_prov;
	String urls_id;
	String urls_valor;
	public Long getUrls_urls() {
		return urls_urls;
	}
	public void setUrls_urls(Long urls_urls) {
		this.urls_urls = urls_urls;
	}
	public String getUrls_prov() {
		return urls_prov;
	}
	public void setUrls_prov(String urls_prov) {
		this.urls_prov = urls_prov;
	}
	public String getUrls_id() {
		return urls_id;
	}
	public void setUrls_id(String urls_id) {
		this.urls_id = urls_id;
	}
	public String getUrls_valor() {
		return urls_valor;
	}
	public void setUrls_valor(String urls_valor) {
		this.urls_valor = urls_valor;
	}
	
	
	
	
	

}
