package com.co.app.model.dto.session;

public class Rol {

	String rol_rol;
	String rol_descri;
	String rol_is_sistema;

	private Object empList;

	public String getRol_is_sistema() {
		return rol_is_sistema;
	}

	public void setRol_is_sistema(String rol_is_sistema) {
		this.rol_is_sistema = rol_is_sistema;
	}

	public String getRol_rol() {
		return rol_rol;
	}

	public void setRol_rol(String rol_rol) {
		this.rol_rol = rol_rol;
	}

	public String getRol_descri() {
		return rol_descri;
	}

	public void setRol_descri(String rol_descri) {
		this.rol_descri = rol_descri;
	}

	public Object getEmpList() {
		return empList;
	}

	public void setEmpList(Object empList) {
		this.empList = empList;
	}

	@Override
	public String toString() {
		return "Rol [rol_rol=" + rol_rol + ", rol_descri=" + rol_descri + ", rol_is_sistema=" + rol_is_sistema + ", empList=" + empList + "]";
	}

}
