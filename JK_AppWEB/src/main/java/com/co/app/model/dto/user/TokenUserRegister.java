package com.co.app.model.dto.user;

import java.util.Date;

public class TokenUserRegister {

	String user_user;
	String otp;
	String codigoAcceso;
	Date fechaGeneracion;
	Integer minutosValidez;
	
	
	public TokenUserRegister() {
		// TODO Auto-generated constructor stub
	}
	
	public TokenUserRegister(String user_user, String codigoAcceso, String otp, Date fechaGeneracion, Integer minutosValidez) {
		this.user_user= user_user;
		this.otp = otp;
		this.fechaGeneracion = fechaGeneracion;
		this.minutosValidez = minutosValidez;
		this.codigoAcceso = codigoAcceso;

	}
	
	public String getUser_user() {
		return user_user;
	}
	public void setUser_user(String user_user) {
		this.user_user = user_user;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	public Integer getMinutosValidez() {
		return minutosValidez;
	}
	public void setMinutosValidez(Integer minutosValidez) {
		this.minutosValidez = minutosValidez;
	}

	public String getCodigoAcceso() {
		return codigoAcceso;
	}

	public void setCodigoAcceso(String codigoAcceso) {
		this.codigoAcceso = codigoAcceso;
	}
	
	
	
	
}
