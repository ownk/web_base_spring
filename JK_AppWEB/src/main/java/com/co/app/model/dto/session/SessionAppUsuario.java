package com.co.app.model.dto.session;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.user.User;

public class SessionAppUsuario {
	
	HttpSession httpSession;
	User usuario;
	Client cliente;
	
	String urlActual;
	String ip;
	String navegador;
	
	Locale locale;
	
	Date sysdate;
	
	//Listado de mensajes web por url
	List<Modulo> menuUsuario; 
	
	//Listado de mensajes web por url
	HashMap<String, List<MensajeApp>> mensajesAPP;
	
	public SessionAppUsuario(Locale locale, HttpSession httpSession, User usuario, String ip, String navegador, List<Modulo> menuUsuario, Date sysdate) {
		this.ip= ip;
		this.navegador = navegador;
		this.httpSession = httpSession;
		this.usuario = usuario;
		this.mensajesAPP = new HashMap<String, List<MensajeApp>>();
		this.menuUsuario = menuUsuario;
		this.locale = locale;
		this.sysdate = sysdate;
		
	}
	
	public SessionAppUsuario() {
		
	}
	
	public HttpSession getHttpSession() {
		return httpSession;
	}
	
	
	
	public User getUsuario() {
		return usuario;
	}
	
	
	
	
	
	public Client getCliente() {
		return cliente;
	}

	public void setCliente(Client cliente) {
		this.cliente = cliente;
	}

	public void setHttpSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}
	

	
	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}
	
	public String getUrlActual() {
		return urlActual;
	}

	public void setUrlActual(String urlActual) {
		this.urlActual = urlActual;
	}



	public String getIp() {
		return ip;
	}



	public void setIp(String ip) {
		this.ip = ip;
	}



	public String getNavegador() {
		return navegador;
	}



	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}
	
	public void agregarMensajeAPP(String url, MensajeApp mensajeWeb) {
		
		
		
		String urlNormalizada = normalizarUrl(url);
		
		
		if(mensajesAPP!=null) {
			if(mensajesAPP.containsKey(url)) {
				
				List<MensajeApp> list = mensajesAPP.get(urlNormalizada);
				
				if(list!=null) {
					list.add(mensajeWeb);
					mensajesAPP.remove(url);
					mensajesAPP.put(urlNormalizada, list);
				}
			}else {
				
				List<MensajeApp> list = new ArrayList<MensajeApp>();
				list.add(mensajeWeb);
				mensajesAPP.put(urlNormalizada, list);
			}
		}
		
	}
	
	
	public void eliminarMensajeAPP(String url) {
		
		String urlNormalizada = normalizarUrl(url);
		
		if(mensajesAPP!=null) {
			if(mensajesAPP.containsKey(urlNormalizada)) {
				mensajesAPP.remove(urlNormalizada);
			}
		}
		
		
		
		
	}
	
	public List<MensajeApp> getMensajesAPP(String url, boolean eliminarMensajes){
		
		
		String urlNormalizada = normalizarUrl(url);
		
		
		if(mensajesAPP!=null) {
			if(mensajesAPP.containsKey(urlNormalizada)) {
				
				if(eliminarMensajes) {
					return mensajesAPP.remove(urlNormalizada);
				}else {
					return mensajesAPP.get(urlNormalizada);
				}
				
			}else {
				return null;	
			}
		}{
			return null;
		}
		
	}

	public List<Modulo> getMenuUsuario() {
		return menuUsuario;
	}

	public void setMenuUsuario(List<Modulo> menuUsuario) {
		this.menuUsuario = menuUsuario;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public HashMap<String, List<MensajeApp>> getMensajesAPP() {
		return mensajesAPP;
	}

	public void setMensajesAPP(HashMap<String, List<MensajeApp>> mensajesAPP) {
		this.mensajesAPP = mensajesAPP;
	}

	public Date getSysdate() {
		return sysdate;
	}

	public void setSysdate(Date sysdate) {
		this.sysdate = sysdate;
	}

	public String normalizarUrl(String url) {

		String normalizeUrl = "";
		
		Boolean isUrlNormalizada = true;
		
		if(url!=null) {
			boolean starstWith = url.startsWith("/");
			
			if(starstWith) {
				
				try {
					normalizeUrl = url.substring(1, url.length());
					isUrlNormalizada = false;
				} catch (Exception e) {
					
				}
				
			    	
			}
			
			
			
		}
		
		if(isUrlNormalizada) {
			normalizeUrl = url;
		}
		
		
		
		return normalizeUrl;
	}

	

	public static String getCurrentIDSession(HttpServletRequest request) {
		
		HttpSession currentSession = request.getSession(false);
		
		if(currentSession!=null) {
			return currentSession.getId();
		}
		
		
		return null;
	
	}
	
	
	
	
}
