package com.co.app.model.dto.user;

import org.springframework.security.core.GrantedAuthority;

public class UserSpringGrant implements GrantedAuthority {
    /**
	 * 
	 */
	
	public static final String GRANT_USERNAME_VALID = "GRANT_USERNAME_VALID";
	public static final String GRANT_USER_AUTENTICATED = "GRANT_USER_AUTENTICATED";
	private static final long serialVersionUID = 1L;
    private String rol;
	public  UserSpringGrant(String rol) {
		this.rol = rol;
	}
	@Override
    public String getAuthority() {
        return rol;
    }
	
}
