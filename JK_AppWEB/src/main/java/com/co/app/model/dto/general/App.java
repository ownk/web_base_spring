package com.co.app.model.dto.general;

import java.io.Serializable;
import java.util.Date;

public class App implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String app_app;
	
	private String app_nomb;
	
	private String app_vers;
	
	private Date app_fcrea;
	
	private Date app_factu;

	public String getApp_app() {
		return app_app;
	}

	public void setApp_app(String app_app) {
		this.app_app = app_app;
	}

	public String getApp_nomb() {
		return app_nomb;
	}

	public void setApp_nomb(String app_nomb) {
		this.app_nomb = app_nomb;
	}

	public String getApp_vers() {
		return app_vers;
	}

	public void setApp_vers(String app_vers) {
		this.app_vers = app_vers;
	}

	public Date getApp_fcrea() {
		return app_fcrea;
	}

	public void setApp_fcrea(Date app_fcrea) {
		this.app_fcrea = app_fcrea;
	}

	public Date getApp_factu() {
		return app_factu;
	}

	public void setApp_factu(Date app_factu) {
		this.app_factu = app_factu;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	

 
}
