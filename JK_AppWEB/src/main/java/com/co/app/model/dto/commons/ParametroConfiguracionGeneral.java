package com.co.app.model.dto.commons;

public class ParametroConfiguracionGeneral {
	
	
	public static int TIPO_DATO_NUMERICO = 1;
	public static int TIPO_DATO_STRING = 2;
	public static int TIPO_DATO_BOOLEAN = 3;
	
	String cnfg_cnfg;
	String cnfg_categoria;
	String cnfg_descri;
	String cnfg_valor;
	int cnfg_tipo_dato;
	String cnfg_clasif;
	
	public String getCnfg_cnfg() {
		return cnfg_cnfg;
	}
	public void setCnfg_cnfg(String cnfg_cnfg) {
		this.cnfg_cnfg = cnfg_cnfg;
	}
	
	public String getCnfg_categoria() {
		return cnfg_categoria;
	}
	public void setCnfg_categoria(String cnfg_categoria) {
		this.cnfg_categoria = cnfg_categoria;
	}
	public String getCnfg_descri() {
		return cnfg_descri;
	}
	public void setCnfg_descri(String cnfg_descri) {
		this.cnfg_descri = cnfg_descri;
	}
	public String getCnfg_valor() {
		return cnfg_valor;
	}
	public void setCnfg_valor(String cnfg_valor) {
		this.cnfg_valor = cnfg_valor;
	}
	public int getCnfg_tipo_dato() {
		return cnfg_tipo_dato;
	}
	public void setCnfg_tipo_dato(int cnfg_tipo_dato) {
		this.cnfg_tipo_dato = cnfg_tipo_dato;
	}
	public String getCnfg_clasif() {
		return cnfg_clasif;
	}
	public void setCnfg_clasif(String cnfg_clasif) {
		this.cnfg_clasif = cnfg_clasif;
	}
	
	
	

}
