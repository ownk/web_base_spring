package com.co.app.model.dto.client;

import java.util.Date;

public class DetailLoadMasiveAccessCode {
	
	public static final String STATE_ERROR ="ERR";
	public static final String STATE_OK ="OK";
	public static final String STATE_PENDING ="PEND";
	
	private String dlmac_dlmac;      
	private String dlmac_lmac;  
    private String dlmac_name;
	private String dlmac_email;    	
	private String dlmac_state;    	
	private String dlmac_mnsg;   	
	private int dlmac_file_id_reg;
    private Date dlmac_date_reg;  
    private Date dlmac_date_upd;
    
	public String getDlmac_dlmac() {
		return dlmac_dlmac;
	}
	public void setDlmac_dlmac(String dlmac_dlmac) {
		this.dlmac_dlmac = dlmac_dlmac;
	}
	public String getDlmac_lmac() {
		return dlmac_lmac;
	}
	public void setDlmac_lmac(String dlmac_lmac) {
		this.dlmac_lmac = dlmac_lmac;
	}
	public String getDlmac_email() {
		return dlmac_email;
	}
	public void setDlmac_email(String dlmac_email) {
		this.dlmac_email = dlmac_email;
	}
	public String getDlmac_state() {
		return dlmac_state;
	}
	public void setDlmac_state(String dlmac_state) {
		this.dlmac_state = dlmac_state;
	}
	public String getDlmac_mnsg() {
		return dlmac_mnsg;
	}
	public void setDlmac_mnsg(String dlmac_mnsg) {
		this.dlmac_mnsg = dlmac_mnsg;
	}
	public int getDlmac_file_id_reg() {
		return dlmac_file_id_reg;
	}
	public void setDlmac_file_id_reg(int dlmac_file_id_reg) {
		this.dlmac_file_id_reg = dlmac_file_id_reg;
	}
	public Date getDlmac_date_reg() {
		return dlmac_date_reg;
	}
	public void setDlmac_date_reg(Date dlmac_date_reg) {
		this.dlmac_date_reg = dlmac_date_reg;
	}
	public Date getDlmac_date_upd() {
		return dlmac_date_upd;
	}
	public void setDlmac_date_upd(Date dlmac_date_upd) {
		this.dlmac_date_upd = dlmac_date_upd;
	}
	public String getDlmac_name() {
		return dlmac_name;
	}
	public void setDlmac_name(String dlmac_name) {
		this.dlmac_name = dlmac_name;
	}   
    
    
    
    
}
