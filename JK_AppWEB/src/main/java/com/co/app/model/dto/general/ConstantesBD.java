package com.co.app.model.dto.general;

public abstract class ConstantesBD {
	public static final String COD_EXITOSO = "OK";
	public static final String COD_FALLIDO = "ERROR";
	
	public static final String PARAMETER_COD_RPTA 			= "p_cod_rta";
	public static final String PARAMETER_CTRL_OPERACION 	= "p_ctrl_operacion";
	public static final String PARAMETER_ERRORES 			= "p_errores";
	public static final String PARAMETER_MENSAJES 			= "p_mensajes";
	
	public static final String PARAMETER_PGCRYPTO_KEY 		= "p_pgcrypto_key";
	public static final String PARAMETER_PGCRYPTO_ALGORITMO = "p_pgcrypto_alg";
	
	
	public static final String USER_GENERAL_APP = "APP";
	
	
}
