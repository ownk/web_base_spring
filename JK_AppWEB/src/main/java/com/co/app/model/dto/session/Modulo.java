package com.co.app.model.dto.session;

import java.util.ArrayList;
import java.util.List;

public class Modulo {
	
	String mdlo_mdlo;
	String mdlo_descri;
	String mdlo_nomb;
	
	List<Menu> menus;
	
	
	public String getMdlo_mdlo() {
		return mdlo_mdlo;
	}
	public void setMdlo_mdlo(String mdlo_mdlo) {
		this.mdlo_mdlo = mdlo_mdlo;
	}
	public String getMdlo_descri() {
		return mdlo_descri;
	}
	public void setMdlo_descri(String mdlo_descri) {
		this.mdlo_descri = mdlo_descri;
	}
	public String getMdlo_nomb() {
		return mdlo_nomb;
	}
	public void setMdlo_nomb(String mdlo_nomb) {
		this.mdlo_nomb = mdlo_nomb;
	}
	public List<Menu> getMenus() {
		return menus;
	}
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
	
	public void addMenu(Menu menu) {
		if(menus == null) {
			menus = new ArrayList<Menu>();
			
		}
		
		if(menus.contains(menu)) {

			menus.remove(menu);
		}
		
		menus.add(menu);
		
	}
	  
	
	

}
