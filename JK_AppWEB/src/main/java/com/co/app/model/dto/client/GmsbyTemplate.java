package com.co.app.model.dto.client;

import java.util.List;

import com.co.app.model.dto.game.Game;

public class GmsbyTemplate {
	
	private String tempgm_templt;
	private String tempgm_game;
	
	
	public String getTempgm_templt() {
		return tempgm_templt;
	}
	
	public void setTempgm_templt(String tempgm_templt) {
		this.tempgm_templt = tempgm_templt;
	}
	
	public String getTempgm_game() {
		return tempgm_game;
	}
	
	public void setTempgm_game(String tempgm_game) {
		this.tempgm_game = tempgm_game;
	}
	
	
}
