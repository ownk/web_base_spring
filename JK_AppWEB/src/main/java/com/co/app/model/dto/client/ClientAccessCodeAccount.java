package com.co.app.model.dto.client;

import com.co.app.model.dto.account.Account;

public class ClientAccessCodeAccount {
	String client_client;
	AccessCode accessCode;
	Account account;
	

	
	
	public String getClient_client() {
		return client_client;
	}
	public void setClient_client(String client_client) {
		this.client_client = client_client;
	}
	public AccessCode getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(AccessCode accessCode) {
		this.accessCode = accessCode;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}

	
}
