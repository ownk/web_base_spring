package com.co.app.model.dto.client;

import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;

public class DetalleReporteClientPlayersObjective {
	Player player;
	Game game;
	LearningObjective learningObjective;
	Match match;
	
	
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public LearningObjective getLearningObjective() {
		return learningObjective;
	}
	public void setLearningObjective(LearningObjective learningObjective) {
		this.learningObjective = learningObjective;
	}
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	
	
}
