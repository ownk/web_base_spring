package com.co.app.model.dto.game;

import java.util.List;

public class Avatar {

	private String avatar_avatar;
	private String avatar_name;
	private String avatar_descri;
	private String avatar_skin_url;
	private String avatar_chrt;
	
	private List<AttribChrt> listAttributes;
	
	public String getAvatar_avatar() {
		return avatar_avatar;
	}
	
	public void setAvatar_avatar(String avatar_avatar) {
		this.avatar_avatar = avatar_avatar;
	}
	
	public String getAvatar_name() {
		return avatar_name;
	}
	
	public void setAvatar_name(String avatar_name) {
		this.avatar_name = avatar_name;
	}
	
	public String getAvatar_descri() {
		return avatar_descri;
	}
	
	public void setAvatar_descri(String avatar_descri) {
		this.avatar_descri = avatar_descri;
	}

	public String getAvatar_skin_url() {
		return avatar_skin_url;
	}

	public void setAvatar_skin_url(String avatar_skin_url) {
		this.avatar_skin_url = avatar_skin_url;
	}

	public String getAvatar_chrt() {
		return avatar_chrt;
	}

	public void setAvatar_chrt(String avatar_chrt) {
		this.avatar_chrt = avatar_chrt;
	}

	public List<AttribChrt> getListAttributes() {
		return listAttributes;
	}

	public void setListAttributes(List<AttribChrt> listAttributes) {
		this.listAttributes = listAttributes;
	}
	
	
	
	
}
