package com.co.app.model.dto.systeminfo;

public class TestConexionMyBatis {
	
	public static final String ESTADO_CONEXION_OK = "OK";
	public static final String ESTADO_CONEXION_FALLIDO = "ERROR";
	
	
	String usuario;
	String url;
	String driver;
	String estadoConexion;
	String descripcionConexion;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getEstadoConexion() {
		return estadoConexion;
	}

	public void setEstadoConexion(String estadoConexion) {
		this.estadoConexion = estadoConexion;
	}

	public String getDescripcionConexion() {
		return descripcionConexion;
	}

	public void setDescripcionConexion(String descripcionConexion) {
		this.descripcionConexion = descripcionConexion;
	}
	
	

	
	
}
