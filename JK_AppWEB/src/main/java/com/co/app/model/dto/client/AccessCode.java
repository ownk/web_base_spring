package com.co.app.model.dto.client;

import java.util.Date;
import java.util.List;

public class AccessCode {
	

	public static final String STATE_ACTIVE = "ACT";
	public static final String STATE_USE 	= "USE";

	private String acode_acode;
	private String acode_code;
	private String acode_state;
	private String acode_client;
	private String acode_templt;
	
	private Date acode_date_start;
	private Date acode_date_validity;
	
	
	TemplateGames templateGames;
	List<SendEmailAccessCode> sendEmaiAc;
	
	public String getAcode_acode() {
		return acode_acode;
	}
	
	public void setAcode_acode(String acode_acode) {
		this.acode_acode = acode_acode;
	}
	
	public String getAcode_code() {
		return acode_code;
	}
	
	public void setAcode_code(String acode_code) {
		this.acode_code = acode_code;
	}
	
	public String getAcode_state() {
		return acode_state;
	}
	
	public void setAcode_state(String acode_state) {
		this.acode_state = acode_state;
	}
	
	public String getAcode_client() {
		return acode_client;
	}
	
	public void setAcode_client(String acode_client) {
		this.acode_client = acode_client;
	}
	
	public Date getAcode_date_start() {
		return acode_date_start;
	}
	
	public void setAcode_date_start(Date acode_date_start) {
		this.acode_date_start = acode_date_start;
	}
	
	public Date getAcode_date_validity() {
		return acode_date_validity;
	}
	
	public void setAcode_date_validity(Date acode_date_validity) {
		this.acode_date_validity = acode_date_validity;
	}

	public String getAcode_templt() {
		return acode_templt;
	}

	public void setAcode_templt(String acode_templt) {
		this.acode_templt = acode_templt;
	}

	public TemplateGames getTemplateGames() {
		return templateGames;
	}

	public void setTemplateGames(TemplateGames templateGames) {
		this.templateGames = templateGames;
	}

	public List<SendEmailAccessCode> getSendEmaiAc() {
		return sendEmaiAc;
	}

	public void setSendEmaiAc(List<SendEmailAccessCode> sendEmaiAc) {
		this.sendEmaiAc = sendEmaiAc;
	}
	
	
	
	
}
