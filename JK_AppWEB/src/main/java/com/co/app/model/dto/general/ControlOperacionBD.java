package com.co.app.model.dto.general;

public class ControlOperacionBD {
	private String lgtr_trans; 
	private String lgtr_terminal;
	private String lgtr_user;
	private String lgtr_fech_oper;
	private String lgtr_hora_oper;
	private String lgtr_observacion;
	
	
	
	
	public ControlOperacionBD(String trans, String terminal, String usuario, String fech_oper, String hora_oper,
			String observacion) {
		super();
		this.lgtr_trans = trans;
		this.lgtr_terminal = terminal;
		this.lgtr_user = usuario;
		this.lgtr_fech_oper = fech_oper;
		this.lgtr_hora_oper = hora_oper;
		this.lgtr_observacion = observacion;
	}
	
	public ControlOperacionBD(String usuario, String terminal, String observacion) {
		
		this.lgtr_terminal = terminal;
		this.lgtr_user = usuario;
		
	}

	public String getLgtr_trans() {
		return lgtr_trans;
	}

	public void setLgtr_trans(String lgtr_trans) {
		this.lgtr_trans = lgtr_trans;
	}

	public String getLgtr_terminal() {
		return lgtr_terminal;
	}

	public void setLgtr_terminal(String lgtr_terminal) {
		this.lgtr_terminal = lgtr_terminal;
	}

	public String getLgtr_user() {
		return lgtr_user;
	}

	public void setLgtr_user(String lgtr_user) {
		this.lgtr_user = lgtr_user;
	}

	public String getLgtr_fech_oper() {
		return lgtr_fech_oper;
	}

	public void setLgtr_fech_oper(String lgtr_fech_oper) {
		this.lgtr_fech_oper = lgtr_fech_oper;
	}

	public String getLgtr_hora_oper() {
		return lgtr_hora_oper;
	}

	public void setLgtr_hora_oper(String lgtr_hora_oper) {
		this.lgtr_hora_oper = lgtr_hora_oper;
	}

	public String getLgtr_observacion() {
		return lgtr_observacion;
	}

	public void setLgtr_observacion(String lgtr_observacion) {
		this.lgtr_observacion = lgtr_observacion;
	}
	
	
	
}
