package com.co.app.model.dto.session;

import java.io.Serializable;

public class EquipoSeguroDTO implements Serializable  {


	private static final long serialVersionUID = -8280105594715537201L;
	
	private String cookie;
	private String ipLocal;
	private String ipRemota; 
	private String hostRemoto;
	private String userAgent;
	private String nombreServidor;
	private String usuario;
	private String navegador;
	private String nroDiasExperaCookie;
	private Boolean isCookieSecure;
	private String nombreCookie;
	private String tipoUsuario;
	private String naturalezaUsuario;
	
	
	public String getNroDiasExperaCookie() {
		return nroDiasExperaCookie;
	}

	public void setNroDiasExperaCookie(String nroDiasExperaCookie) {
		this.nroDiasExperaCookie = nroDiasExperaCookie;
	}

	public Boolean getIsCookieSecure() {
		return isCookieSecure;
	}

	public void setIsCookieSecure(Boolean isCookieSecure) {
		this.isCookieSecure = isCookieSecure;
	}

	public String getNombreCookie() {
		return nombreCookie;
	}

	public void setNombreCookie(String nombreCookie) {
		this.nombreCookie = nombreCookie;
	}

	private boolean equipoValido;
	
	public String getCookie() {
		return cookie;
	}
	
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	
	public boolean isEquipoValido() {
		return equipoValido;
	}
	
	public void setEquipoValido(boolean equipoValido) {
		this.equipoValido = equipoValido;
	}

	public String getIpLocal() {
		return ipLocal;
	}

	public void setIpLocal(String ipLocal) {
		this.ipLocal = ipLocal;
	}

	public String getIpRemota() {
		return ipRemota;
	}

	public void setIpRemota(String ipRemota) {
		this.ipRemota = ipRemota;
	}

	public String getHostRemoto() {
		return hostRemoto;
	}

	public void setHostRemoto(String hostRemoto) {
		this.hostRemoto = hostRemoto;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getNombreServidor() {
		return nombreServidor;
	}

	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNavegador() {
		return navegador;
	}

	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getNaturalezaUsuario() {
		return naturalezaUsuario;
	}

	public void setNaturalezaUsuario(String naturalezaUsuario) {
		this.naturalezaUsuario = naturalezaUsuario;
	}	
	
	
	
}
