package com.co.app.model.dto.game;

public class Answer {
	
	String answer_answer;
	String answer_descri;
	int answer_value; 
	String answer_acateg;
	
	public String getAnswer_answer() {
		return answer_answer;
	}
	
	public void setAnswer_answer(String answer_answer) {
		this.answer_answer = answer_answer;
	}
	
	public String getAnswer_descri() {
		return answer_descri;
	}
	
	public void setAnswer_descri(String answer_descri) {
		this.answer_descri = answer_descri;
	}
	
	public int getAnswer_value() {
		return answer_value;
	}
	
	public void setAnswer_value(int answer_value) {
		this.answer_value = answer_value;
	}
	
	public String getAnswer_acateg() {
		return answer_acateg;
	}
	
	public void setAnswer_acateg(String answer_acateg) {
		this.answer_acateg = answer_acateg;
	}
	
	
	
}
