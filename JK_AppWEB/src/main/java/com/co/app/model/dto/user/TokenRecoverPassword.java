package com.co.app.model.dto.user;

import java.util.Date;

public class TokenRecoverPassword {

	String user_email;
	String otp;
	Date fechaGeneracion;
	Integer minutosValidez;
	
	public TokenRecoverPassword() {
		// TODO Auto-generated constructor stub
	}
	
	public TokenRecoverPassword(String user_email,  String otp, Date fechaGeneracion, Integer minutosValidez) {
		this.user_email= user_email;
		this.otp = otp;
		this.fechaGeneracion = fechaGeneracion;
		this.minutosValidez = minutosValidez;
	}
	
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	public Integer getMinutosValidez() {
		return minutosValidez;
	}
	public void setMinutosValidez(Integer minutosValidez) {
		this.minutosValidez = minutosValidez;
	}
	
}
