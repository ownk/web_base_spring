package com.co.app.model.dto.session;

import java.util.ArrayList;
import java.util.List;

public class Servicio {
	
	//Informacion del servicio
	Long srvc_srvc;
	String srvc_mdlo;
	String srvc_descri;
	String srvc_nomb;
	
	//URL Principal del servicio
	String surl_url;
	
	//Informacion urls
	List<URLServicio> listUrls;
	
	//Informacion del modulo
	String mdlo_mdlo;
	String mdlo_descri;
	String mdlo_nomb;
	
	//Informacion del menu
	String menu_nombre;
	String menu_icon;
	Long menu_menu;
	
	//
	Boolean isServiceSelected = false;
	
	public Long getSrvc_srvc() {
		return srvc_srvc;
	}
	public void setSrvc_srvc(Long srvc_srvc) {
		this.srvc_srvc = srvc_srvc;
	}
	public String getSrvc_mdlo() {
		return srvc_mdlo;
	}
	public void setSrvc_mdlo(String srvc_mdlo) {
		this.srvc_mdlo = srvc_mdlo;
	}
	public String getSrvc_descri() {
		return srvc_descri;
	}
	public void setSrvc_descri(String srvc_descri) {
		this.srvc_descri = srvc_descri;
	}
	public String getSrvc_nomb() {
		return srvc_nomb;
	}
	public void setSrvc_nomb(String srvc_nomb) {
		this.srvc_nomb = srvc_nomb;
	}
	public String getSurl_url() {
		return surl_url;
	}
	public void setSurl_url(String surl_url) {
		this.surl_url = surl_url;
	}
	
	public String getMdlo_mdlo() {
		return mdlo_mdlo;
	}
	public void setMdlo_mdlo(String mdlo_mdlo) {
		this.mdlo_mdlo = mdlo_mdlo;
	}
	public String getMdlo_descri() {
		return mdlo_descri;
	}
	public void setMdlo_descri(String mdlo_descri) {
		this.mdlo_descri = mdlo_descri;
	}
	public String getMdlo_nomb() {
		return mdlo_nomb;
	}
	public void setMdlo_nomb(String mdlo_nomb) {
		this.mdlo_nomb = mdlo_nomb;
	}
	public String getMenu_nombre() {
		return menu_nombre;
	}
	public void setMenu_nombre(String menu_nombre) {
		this.menu_nombre = menu_nombre;
	}
	public String getMenu_icon() {
		return menu_icon;
	}
	public void setMenu_icon(String menu_icon) {
		this.menu_icon = menu_icon;
	}
	public Long getMenu_menu() {
		return menu_menu;
	}
	public void setMenu_menu(Long menu_menu) {
		this.menu_menu = menu_menu;
	}
	public Boolean getIsServiceSelected() {
		return isServiceSelected;
	}
	public void setIsServiceSelected(Boolean isServiceSelected) {
		this.isServiceSelected = isServiceSelected;
	}
	
	public void addUrl(URLServicio url) {
		if(listUrls == null) {
			listUrls = new ArrayList<URLServicio>();
			
		}
		
		if(listUrls.contains(url)) {

			listUrls.remove(url);
		}
		
		listUrls.add(url);
		
	}
	public List<URLServicio> getListUrls() {
		return listUrls;
	}
	public void setListUrls(List<URLServicio> listUrls) {
		this.listUrls = listUrls;
	}
	
	
	

	
}
