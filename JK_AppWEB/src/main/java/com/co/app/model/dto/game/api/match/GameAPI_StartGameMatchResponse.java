package com.co.app.model.dto.game.api.match;

import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Player;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Response for the start of a match of a specific game")
public class GameAPI_StartGameMatchResponse {
	@ApiModelProperty(notes = "Unique identifier of the match. No two keys can have the same information.", example = "45782bdc-4154-45f6-8c2c-fcbb1731bca2", required = true, position = 0)
	private String gameMatchID;
	
	@ApiModelProperty(notes = "Game details.", required = true, position = 1)
	private Game game;
	
	@ApiModelProperty(notes = "Player details.", required = true, position = 2)
	private Player player;
		
	
	
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}

	
	public String getGameMatchID() {
		return gameMatchID;
	}


	public void setGameMatchID(String gameMatchID) {
		this.gameMatchID = gameMatchID;
	}
	
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	@Override
	public String toString() {
		return "GameAPI_StartGameMatchResponse [gameMatchID=" + gameMatchID +", player=" +player+", game="+game+ "]";
	}


	
}
