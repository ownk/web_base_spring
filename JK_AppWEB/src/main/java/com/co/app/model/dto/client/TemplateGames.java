package com.co.app.model.dto.client;

import java.util.List;

import com.co.app.model.dto.game.Game;

public class TemplateGames {
	
	private String templt_templt;
	private String templt_client;
	private String templt_descri;
	
	private List<Game> games;
	
	public String getTemplt_templt() {
		return templt_templt;
	}
	
	public void setTemplt_templt(String templt_templt) {
		this.templt_templt = templt_templt;
	}
	
	public String getTemplt_client() {
		return templt_client;
	}
	
	public void setTemplt_client(String templt_client) {
		this.templt_client = templt_client;
	}
	
	public String getTemplt_descri() {
		return templt_descri;
	}
	
	public void setTemplt_descri(String templt_descri) {
		this.templt_descri = templt_descri;
	}

	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}	
	
	public static TemplateGames createBasicTemplate(String idTemplt) {
		TemplateGames templateGames = new TemplateGames();
		templateGames.setTemplt_templt(idTemplt);
		
		return templateGames; 
	}
	
	
	
}
