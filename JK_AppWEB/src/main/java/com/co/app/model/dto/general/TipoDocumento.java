package com.co.app.model.dto.general;

public class TipoDocumento {
	private String tdoc_tdoc ;
	private String tdoc_doc  ;
	private String tdoc_tper ;
	
	public String getTdoc_tdoc() {
		return tdoc_tdoc;
	}
	public void setTdoc_tdoc(String tdoc_tdoc) {
		this.tdoc_tdoc = tdoc_tdoc;
	}
	public String getTdoc_doc() {
		return tdoc_doc;
	}
	public void setTdoc_doc(String tdoc_doc) {
		this.tdoc_doc = tdoc_doc;
	}
	public String getTdoc_tper() {
		return tdoc_tper;
	}
	public void setTdoc_tper(String tdoc_tper) {
		this.tdoc_tper = tdoc_tper;
	}
                             
	
}
