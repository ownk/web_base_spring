package com.co.app.model.dto.systeminfo;

public class GitInfo {
	
	String version;
	CommitInfo commitInfo;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public CommitInfo getCommitInfo() {
		return commitInfo;
	}
	public void setCommitInfo(CommitInfo commitInfo) {
		this.commitInfo = commitInfo;
	}
	

	
}
