package com.co.app.model.dto.game;

public class LearningObjective{
	
	public static final String CLASIF_DEFAULT = "MEDIO";

	
	private String lrnobj_lrnobj;
	private String lrnobj_name;
	private String lrnobj_descri;
	private String lrnobj_icon;
	private String lrnobj_color;
	private String lrnobj_clasif=CLASIF_DEFAULT;
	
	
	public String getLrnobj_lrnobj() {
		return lrnobj_lrnobj;
	}
	
	public void setLrnobj_lrnobj(String lrnobj_lrnobj) {
		this.lrnobj_lrnobj = lrnobj_lrnobj;
	}
	
	public String getLrnobj_name() {
		return lrnobj_name;
	}
	
	public void setLrnobj_name(String lrnobj_name) {
		this.lrnobj_name = lrnobj_name;
	}
	
	public String getLrnobj_descri() {
		return lrnobj_descri;
	}
	
	public void setLrnobj_descri(String lrnobj_descri) {
		this.lrnobj_descri = lrnobj_descri;
	}
	
	public String getLrnobj_icon() {
		return lrnobj_icon;
	}
	
	public void setLrnobj_icon(String lrnobj_icon) {
		this.lrnobj_icon = lrnobj_icon;
	}
	
	public String getLrnobj_color() {
		return lrnobj_color;
	}
	
	public void setLrnobj_color(String lrnobj_color) {
		this.lrnobj_color = lrnobj_color;
	}

	public String getLrnobj_clasif() {
		return lrnobj_clasif;
	}

	public void setLrnobj_clasif(String lrnobj_clasif) {
		this.lrnobj_clasif = lrnobj_clasif;
	}
	
	
}