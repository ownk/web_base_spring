package com.co.app.model.dto.game;

public class LearningObjByGame{
	
	private String lrnogm_lrnobj;
	private String lrnogm_game;
	
	
	public String getLrnogm_lrnobj() {
		return lrnogm_lrnobj;
	}
	public void setLrnogm_lrnobj(String lrnogm_lrnobj) {
		this.lrnogm_lrnobj = lrnogm_lrnobj;
	}
	public String getLrnogm_game() {
		return lrnogm_game;
	}
	public void setLrnogm_game(String lrnogm_game) {
		this.lrnogm_game = lrnogm_game;
	}
	
	
	
	
	
	
	
}