package com.co.app.model.dto.game.api.commons;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a general REST message of API")
public class GameAPI_RESTMessage<T> {
	
	public static final String REST_OK = "OK";
	public static final String REST_ERROR_INFO_NV = "ERROR_REST_INFO_NV";
	public static final String REST_ERROR_NC = "ERROR_REST_NC";
	
	
	
	@ApiModelProperty(notes = "Status CODE of REST Service . [OK]/[ERROR_REST_INFO_NV]/[ERROR_REST_NC]", example = "OK", required = true, position = 0)
	private String statusCode;
	
	@ApiModelProperty(notes = "Data result of REST Service. See details on each service", required = false, position = 1)
	private T data;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}

	
	

}
