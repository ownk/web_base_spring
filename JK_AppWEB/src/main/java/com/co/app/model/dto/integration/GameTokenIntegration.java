package com.co.app.model.dto.integration;

import java.util.Date;


public class GameTokenIntegration {

	private String gtoken_gtoken;
	private String gtoken_player;
	private String gtoken_game;
	private String gtoken_sapp;
	private Date gtoken_date_star;
	private Date gtoken_date_validate;
	private Date gtoken_date_end;
	private String gtoken_state;
	
	
	public String getGtoken_gtoken() {
		return gtoken_gtoken;
	}
	public void setGtoken_gtoken(String gtoken_gtoken) {
		this.gtoken_gtoken = gtoken_gtoken;
	}
	public String getGtoken_player() {
		return gtoken_player;
	}
	public void setGtoken_player(String gtoken_player) {
		this.gtoken_player = gtoken_player;
	}
	public String getGtoken_game() {
		return gtoken_game;
	}
	public void setGtoken_game(String gtoken_game) {
		this.gtoken_game = gtoken_game;
	}
	public String getGtoken_sapp() {
		return gtoken_sapp;
	}
	public void setGtoken_sapp(String gtoken_sapp) {
		this.gtoken_sapp = gtoken_sapp;
	}
	public Date getGtoken_date_star() {
		return gtoken_date_star;
	}
	public void setGtoken_date_star(Date gtoken_date_star) {
		this.gtoken_date_star = gtoken_date_star;
	}
	public Date getGtoken_date_validate() {
		return gtoken_date_validate;
	}
	public void setGtoken_date_validate(Date gtoken_date_validate) {
		this.gtoken_date_validate = gtoken_date_validate;
	}
	public Date getGtoken_date_end() {
		return gtoken_date_end;
	}
	public void setGtoken_date_end(Date gtoken_date_end) {
		this.gtoken_date_end = gtoken_date_end;
	}
	public String getGtoken_state() {
		return gtoken_state;
	}
	public void setGtoken_state(String gtoken_state) {
		this.gtoken_state = gtoken_state;
	}
	

	
	
}
