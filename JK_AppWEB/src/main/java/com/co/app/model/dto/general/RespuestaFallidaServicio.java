package com.co.app.model.dto.general;

public class RespuestaFallidaServicio extends Exception {
	
	private static final long serialVersionUID = 1L;
	private String codigoRespuesta;
	private String mensaje;
	
	
	public RespuestaFallidaServicio(String codigoRespuesta, String mensaje) {
		super(mensaje);
		this.codigoRespuesta = codigoRespuesta;
		this.mensaje = mensaje;
		
		
	}
	
	public RespuestaFallidaServicio(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
		this.mensaje = codigoRespuesta;
	}
	
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	

	
	 
	
	
	

}
