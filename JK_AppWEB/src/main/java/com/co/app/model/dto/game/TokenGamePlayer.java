package com.co.app.model.dto.game;

import java.util.Date;

public class TokenGamePlayer {

	String player_player;
	String game_game;
	Date fechaGeneracion;
	
	public TokenGamePlayer(String player_player, String game_game, Date fechaGeneracion) {
		this.player_player= player_player;
		this.game_game = game_game;
		this.fechaGeneracion = fechaGeneracion;
		
	}

	public String getPlayer_player() {
		return player_player;
	}

	public void setPlayer_player(String player_player) {
		this.player_player = player_player;
	}

	public String getGame_game() {
		return game_game;
	}

	public void setGame_game(String game_game) {
		this.game_game = game_game;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}


	
	
}
