package com.co.app.model.dto.user;

import java.util.Date;

public class PasswordChange {

	private String  passch_passch;  
	private String  passch_user;    
	private String  passch_password;
	private String  passch_newpass; 
	private Date    passch_datereg; 
	private String  passch_browser; 
	private String  passch_ip;
	
	
	public String getPassch_passch() {
		return passch_passch;
	}
	public void setPassch_passch(String passch_passch) {
		this.passch_passch = passch_passch;
	}
	public String getPassch_user() {
		return passch_user;
	}
	public void setPassch_user(String passch_user) {
		this.passch_user = passch_user;
	}
	public String getPassch_password() {
		return passch_password;
	}
	public void setPassch_password(String passch_password) {
		this.passch_password = passch_password;
	}
	public String getPassch_newpass() {
		return passch_newpass;
	}
	public void setPassch_newpass(String passch_newpass) {
		this.passch_newpass = passch_newpass;
	}
	public Date getPassch_datereg() {
		return passch_datereg;
	}
	public void setPassch_datereg(Date passch_datereg) {
		this.passch_datereg = passch_datereg;
	}
	public String getPassch_browser() {
		return passch_browser;
	}
	public void setPassch_browser(String passch_browser) {
		this.passch_browser = passch_browser;
	}
	public String getPassch_ip() {
		return passch_ip;
	}
	public void setPassch_ip(String passch_ip) {
		this.passch_ip = passch_ip;
	}      
	
	
	
}
