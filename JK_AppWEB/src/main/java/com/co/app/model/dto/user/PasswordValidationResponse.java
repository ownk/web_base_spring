package com.co.app.model.dto.user;

public class PasswordValidationResponse {
	
	
	public static final String PASS_OK = "OK";
	public static final String PASS_ERR_PSNV_FULL = "ERR_PASS_PSNV_FULL";
	public static final String PASS_ERR_PSNV_SIMILARY = "ERR_PASS_PSNV_SIMILARY";
	public static final String PASS_ERR_NAME_SIMILARY = "ERR_PASS_NAME_SIMILARY";
	public static final String PASS_ERR_NICK_SIMILARY = "ERR_PASS_NICK_SIMILARY";
	public static final String PASS_ERR_REGEX_FAIL= "ERR_PASS_REGEX_FAIL";
	public static final String PASS_ERR_USED_BEFORE= "ERR_PASS_USED_BEFORE";


	
	
	
	private String messageResponse;
	private String codeResponse;
		
	private boolean isValid;

	public String getMessageResponse() {
		return messageResponse;
	}

	public void setMessageResponse(String messageResponse) {
		this.messageResponse = messageResponse;
	}

	public String getCodeResponse() {
		return codeResponse;
	}

	public void setCodeResponse(String codeResponse) {
		this.codeResponse = codeResponse;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	
	
	
	
		

}
