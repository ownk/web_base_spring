package com.co.app.model.dto.game;

public class AttribChrt{

	private String atrch_chrt;
	private String atrch_atrb;
	private int atrch_init_value;

	private String atrb_descri;
	private String atrb_name;
	private String atrb_icon;
	private String atrb_color;
	
	
	public String getAtrch_chrt() {
		return atrch_chrt;
	}
	public void setAtrch_chrt(String atrch_chrt) {
		this.atrch_chrt = atrch_chrt;
	}
	public String getAtrch_atrb() {
		return atrch_atrb;
	}
	public void setAtrch_atrb(String atrch_atrb) {
		this.atrch_atrb = atrch_atrb;
	}
	public int getAtrch_init_value() {
		return atrch_init_value;
	}
	public void setAtrch_init_value(int atrch_init_value) {
		this.atrch_init_value = atrch_init_value;
	}
	public String getAtrb_descri() {
		return atrb_descri;
	}
	public void setAtrb_descri(String atrb_descri) {
		this.atrb_descri = atrb_descri;
	}
	public String getAtrb_name() {
		return atrb_name;
	}
	public void setAtrb_name(String atrb_name) {
		this.atrb_name = atrb_name;
	}
	public String getAtrb_icon() {
		return atrb_icon;
	}
	public void setAtrb_icon(String atrb_icon) {
		this.atrb_icon = atrb_icon;
	}
	public String getAtrb_color() {
		return atrb_color;
	}
	public void setAtrb_color(String atrb_color) {
		this.atrb_color = atrb_color;
	}


	

}
