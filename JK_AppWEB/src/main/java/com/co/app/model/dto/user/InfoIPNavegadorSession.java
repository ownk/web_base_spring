package com.co.app.model.dto.user;

import java.util.Date;

public class InfoIPNavegadorSession {

    private String  iflgn_iflgn;  
	private String  iflgn_user;    
	private String  iflgn_browser;
	private String  iflgn_ip; 
	private String  iflgn_session;
	
	private Date    iflgn_date_last_ingr;
	
	
	public String getIflgn_iflgn() {
		return iflgn_iflgn;
	}
	public void setIflgn_iflgn(String iflgn_iflgn) {
		this.iflgn_iflgn = iflgn_iflgn;
	}
	public String getIflgn_user() {
		return iflgn_user;
	}
	public void setIflgn_user(String iflgn_user) {
		this.iflgn_user = iflgn_user;
	}
	public String getIflgn_browser() {
		return iflgn_browser;
	}
	public void setIflgn_browser(String iflgn_browser) {
		this.iflgn_browser = iflgn_browser;
	}
	public String getIflgn_ip() {
		return iflgn_ip;
	}
	public void setIflgn_ip(String iflgn_ip) {
		this.iflgn_ip = iflgn_ip;
	}
	public Date getIflgn_date_last_ingr() {
		return iflgn_date_last_ingr;
	}
	public void setIflgn_date_last_ingr(Date iflgn_date_last_ingr) {
		this.iflgn_date_last_ingr = iflgn_date_last_ingr;
	}
	public String getIflgn_session() {
		return iflgn_session;
	}
	public void setIflgn_session(String iflgn_session) {
		this.iflgn_session = iflgn_session;
	}
	
	
	
	
	
	
	
}
