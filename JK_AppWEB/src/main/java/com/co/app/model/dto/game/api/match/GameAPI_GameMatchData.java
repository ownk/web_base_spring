package com.co.app.model.dto.game.api.match;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(description = "Class representing a new DataGame tracked by the application game.")
public class GameAPI_GameMatchData {

	@ApiModelProperty(notes = "Result of match. WIN, PEND, LOSE ", example = "WIN", required = true, position = 0)
	private String match_result;
	
	@ApiModelProperty(notes = "General score of match. Integer value", example = "43", required = true, position = 1)
	private int match_general_score = 0;
	
	public String getMatch_result() {
		return match_result;
	}

	public void setMatch_result(String match_result) {
		this.match_result = match_result;
	}

	public int getMatch_general_score() {
		return match_general_score;
	}

	public void setMatch_general_score(int match_general_score) {
		this.match_general_score = match_general_score;
	}

	@Override
	public String toString() {
		return "GameAPI_DataMatch [match_general_score=" + match_general_score+ ", match_result=" + match_result+ "]";
	}

	
	
	
	
	
}
