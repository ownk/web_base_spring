package com.co.app.model.dto.client;

import java.util.Date;

public class SendEmailRegisterCode {

	private String serc_serc; 		
	private String serc_email;		
	private String serc_rcode;		
    private String serc_user;
    private String serc_name;
    
    private Date serc_date_send;

    
    
    
	public String getSerc_serc() {
		return serc_serc;
	}
	public void setSerc_serc(String serc_serc) {
		this.serc_serc = serc_serc;
	}
	public String getSerc_email() {
		return serc_email;
	}
	public void setSerc_email(String serc_email) {
		this.serc_email = serc_email;
	}
	public String getSerc_rcode() {
		return serc_rcode;
	}
	public void setSerc_rcode(String serc_rcode) {
		this.serc_rcode = serc_rcode;
	}
	public String getSerc_user() {
		return serc_user;
	}
	public void setSerc_user(String serc_user) {
		this.serc_user = serc_user;
	}
	public Date getSerc_date_send() {
		return serc_date_send;
	}
	public void setSerc_date_send(Date serc_date_send) {
		this.serc_date_send = serc_date_send;
	}
	public String getSerc_name() {
		return serc_name;
	}
	public void setSerc_name(String serc_name) {
		this.serc_name = serc_name;
	}

	
    
	
}
