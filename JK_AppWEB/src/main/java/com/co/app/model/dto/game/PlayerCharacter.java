package com.co.app.model.dto.game;

import java.util.List;

public class PlayerCharacter {
	
	String avatar_avatar;
	String ship_ship;
	String pet_pet;
	
	List<Question> questions;

	public String getAvatar_avatar() {
		return avatar_avatar;
	}

	public void setAvatar_avatar(String avatar_avatar) {
		this.avatar_avatar = avatar_avatar;
	}

	public String getShip_ship() {
		return ship_ship;
	}

	public void setShip_ship(String ship_ship) {
		this.ship_ship = ship_ship;
	}

	public String getPet_pet() {
		return pet_pet;
	}

	public void setPet_pet(String pet_pet) {
		this.pet_pet = pet_pet;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	
	

}
