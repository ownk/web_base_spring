package com.co.app.model.dto.game.api.match;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Request for update a match of specific game")
public class GameAPI_UpdateGameMatchRequest {
	@ApiModelProperty(notes = "Unique identifier of the tokenApp. See more info in service authapp", example = "6ace2bdc-4154-45f6-8c2c-fcbb1731bca2", required = true, position = 0)
	private String appToken;
	
	@ApiModelProperty(notes = "Unique identifier of the match. See more info in servive match/start", example = "45782bdc-4154-45f6-8c2c-fcbb1731bca2", required = true, position = 1)
	private String gameMatchID;
	
	@ApiModelProperty(notes = "JSON information to update math. See more info into GameAPI_GameMatchData", required = true, position = 2)
	private GameAPI_GameMatchData gameMatchData;
	
	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public String getGameMatchID() {
		return gameMatchID;
	}

	public void setGameMatchID(String gameMatchID) {
		this.gameMatchID = gameMatchID;
	}

	public GameAPI_GameMatchData getGameMatchData() {
		return gameMatchData;
	}

	public void setGameMatchData(GameAPI_GameMatchData gameMatchData) {
		this.gameMatchData = gameMatchData;
	}

	@Override
	public String toString() {
		return "RegisterGameRequest [appToken=" + appToken + ", gameMatchID=" + gameMatchID + ", gameMatchData "+gameMatchData+"]";
	}

}
