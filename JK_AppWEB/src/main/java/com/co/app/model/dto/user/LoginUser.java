package com.co.app.model.dto.user;

import java.util.Date;

public class LoginUser {

	private String  login_login;  
	private String  login_user;    
	private String  login_nick;
	private String  login_pass; 
	private Date    login_datereg; 
	private String  login_browser; 
	private String  login_ip;
	
	
	public String getLogin_login() {
		return login_login;
	}
	public void setLogin_login(String login_login) {
		this.login_login = login_login;
	}
	public String getLogin_user() {
		return login_user;
	}
	public void setLogin_user(String login_user) {
		this.login_user = login_user;
	}
	public String getLogin_nick() {
		return login_nick;
	}
	public void setLogin_nick(String login_nick) {
		this.login_nick = login_nick;
	}
	public String getLogin_pass() {
		return login_pass;
	}
	public void setLogin_pass(String login_pass) {
		this.login_pass = login_pass;
	}
	public Date getLogin_datereg() {
		return login_datereg;
	}
	public void setLogin_datereg(Date login_datereg) {
		this.login_datereg = login_datereg;
	}
	public String getLogin_browser() {
		return login_browser;
	}
	public void setLogin_browser(String login_browser) {
		this.login_browser = login_browser;
	}
	public String getLogin_ip() {
		return login_ip;
	}
	public void setLogin_ip(String login_ip) {
		this.login_ip = login_ip;
	}
	
		
	
	
}
