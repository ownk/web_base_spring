package com.co.app;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionListener implements HttpSessionListener {
	private Logger logger = LoggerFactory.getLogger(SessionListener.class);
  
    public SessionListener() {
        super();
    }
 
  
    public void sessionCreated(final HttpSessionEvent event) {
 
    	//logger.error("Session created: "+event.getSession().getId());
    	
    }
    public void sessionDestroyed(final HttpSessionEvent event) {
    	
    	HttpSession session = event.getSession();
    	
    	if(session!=null) {
    		
    		//logger.error("Session destroyed: "+event.getSession().getId());
    	}else {
    		//logger.error("Session destroyed error: "+event);
    	}
    	
    	
    	
    	
    }
}
