package com.co.app.backend.service.business.captcha;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.co.app.backend.service.business.app.AppService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class RecaptchaService {

	@Autowired
	private RestTemplate restTemplate;


	@Autowired
	AppService appService;
	
	String recaptchaUrl = null;
	String recaptchaSecretKey = null;
	String recaptchaSiteKey = null;

	public RecaptchaResult getResult(String remoteIp, String response) {
		
		
		String recaptchaUrl = getRecaptchaUrl();
		String recaptchaSecretKey = getRecaptchaSecretKey();
		
		
		return restTemplate.postForEntity(recaptchaUrl, createRequest(recaptchaSecretKey, remoteIp, response),
				RecaptchaResult.class).getBody();
	}

	private MultiValueMap<String, String> createRequest(String secret, String remoteIp, String response) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("secret", secret);
		map.add("remoteip", remoteIp);
		map.add("response", response);
		return map;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class RecaptchaResult {
		@JsonProperty("success")
		private boolean success;
		@JsonProperty("error-codes")
		private ArrayList<String> errorCodes;
		@JsonProperty("challenge_ts")
		private String challegeTs;
		@JsonProperty("hostname")
		private String hostName;

		public boolean isSuccess() {
			return success;
		}

		public ArrayList<String> getErrorCodes() {
			return errorCodes;
		}

		public String getChallegeTs() {
			return challegeTs;
		}

		public String getHostName() {
			return hostName;
		}
	}
	
	public String getRecaptchaSiteKey() {
		
		
		if(recaptchaSiteKey!=null &&!StringUtils.isEmpty(recaptchaSiteKey)) {
			return recaptchaSiteKey;
		}else {
			
			recaptchaSiteKey = appService.getGoogle_recaptchaSiteKey();
		}
		
		
		return recaptchaSiteKey;
	}

	public String getRecaptchaUrl() {
		
		
		
		if(recaptchaUrl!=null &&!StringUtils.isEmpty(recaptchaUrl)) {
			return recaptchaUrl;
		}else {
			
			recaptchaUrl = appService.getGoogle_recaptchaUrl();
		}
		
		
		
		return recaptchaUrl;
	}

	public String getRecaptchaSecretKey() {
		

		if(recaptchaSecretKey!=null &&!StringUtils.isEmpty(recaptchaSecretKey)) {
			return recaptchaSecretKey;
		}else {
			
			 recaptchaSecretKey = appService.getGoogle_recaptchaSecretKey();
		}
		
		
		return recaptchaSecretKey;
	}
	
	

	
	

}