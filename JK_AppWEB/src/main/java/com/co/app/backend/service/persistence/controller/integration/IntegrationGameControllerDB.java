package com.co.app.backend.service.persistence.controller.integration;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.persistence.CommonsParameterBDGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.ResponseFailedBDGenerator;
import com.co.app.backend.service.persistence.mapper.integration.IntegrationGameMapper;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Score;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;
import com.co.app.model.dto.integration.GameTokenIntegration;
import com.co.app.model.dto.integration.Partner;
import com.co.app.model.dto.integration.SessionIntegration;


@Component
public class IntegrationGameControllerDB {
	
	
	private Logger logger = LoggerFactory.getLogger(IntegrationGameControllerDB.class);
	
	@Autowired
	private IntegrationGameMapper mapper;
	
	@Autowired
	ResponseFailedBDGenerator generadorExcepcionBD;
	
	@Autowired
	private AppService appServicio;
	
	@Autowired
	private I18nService i18nService;
	
	
	
	public Partner getPartnerPorKeys(ControlOperacionBD controlOperacionBD, String siteKey, String accessKey) throws ResponseFailedBD{

		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		Partner partner = null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_sitekey", siteKey);
			parametrosInOout.put("p_accesskey", accessKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getPartnerPorKeys(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Partner> lista = (List<Partner>) parametrosInOout.get("p_tpartnr");
				if (!lista.isEmpty()) {
					partner = lista.get(0);
				} else {
					return null;
				}
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}		
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return partner;

	}
	
	public List<Partner> getPartners(ControlOperacionBD controlOperacionBD) throws ResponseFailedBD{

		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Partner> partners = null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getPartners(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				partners = (List<Partner>) parametrosInOout.get("p_tpartnr");
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}		
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return partners;

	}
	
	public Boolean crearPartner(ControlOperacionBD controlOperacionBD, Partner partner)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tpartnr", partner);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.crearPartner(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean inactivarPartner(ControlOperacionBD controlOperacionBD, String idPartner)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_partnr_partnr", idPartner);
			mapper.inactivarPartner(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}

	public String iniciarSession(ControlOperacionBD controlOperacionBD, String idPartner, String token)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		String idSession = "";

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_partnr_partnr", idPartner);
			parametrosInOout.put("p_token", token);
			mapper.iniciarSession(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				idSession =(String) parametrosInOout.get("p_session_session");
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return idSession;

	}
	
	public Boolean finalizarSession(ControlOperacionBD controlOperacionBD, String idSession, String token)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_session_session", idSession);
			parametrosInOout.put("p_token", token);
			mapper.finalizarSession(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public String iniciarMatch(ControlOperacionBD controlOperacionBD, String idPlayer, String idGame, String idSession, String idGToken)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		String idMatch;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_game_game", idGame);
			parametrosInOout.put("p_session_session", idSession);
			parametrosInOout.put("p_gtoken_gtoken", idGToken);


			mapper.iniciarMatch(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				idMatch = (String) parametrosInOout.get("p_match_match");
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		return idMatch;

	}
	

	public Boolean actualizarMatch(ControlOperacionBD controlOperacionBD, Match match, List<Score> score,String idSession)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

				
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tscore", score);
			parametrosInOout.put("p_tmatch", match);
			parametrosInOout.put("p_session_session", idSession);

			mapper.actualizarMatch(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	
	public SessionIntegration getSession(ControlOperacionBD controlOperacionBD, String idSession) throws ResponseFailedBD{

		
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		SessionIntegration session = null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_session_session", idSession);
			mapper.getSession(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<SessionIntegration> lista = (List<SessionIntegration>) parametrosInOout.get("p_tsession");
				if (!lista.isEmpty()) {
					session = lista.get(0);
				} else {
					return null;
				}
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}		
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return session;

	}
	
	public String createGToken(ControlOperacionBD controlOperacionBD, String idPlayer, String idGame, String sessionID)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		String idGToken = null;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_game_game", idGame);
			parametrosInOout.put("p_app_session", sessionID);
			mapper.createGToken(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				idGToken =(String) parametrosInOout.get("p_gtoken_gtoken");
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return idGToken;

	}
	
	public GameTokenIntegration getGToken(ControlOperacionBD controlOperacionBD, String idGToken) throws ResponseFailedBD{

		
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		GameTokenIntegration gtoken = null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_gtoken_gtoken", idGToken);
			mapper.getGToken(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<GameTokenIntegration> lista = (List<GameTokenIntegration>) parametrosInOout.get("p_tgtoken");
				if (!lista.isEmpty()) {
					gtoken = lista.get(0);
				} else {
					return null;
				}
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}		
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return gtoken;

	}
	
	
	
}

