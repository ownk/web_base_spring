package com.co.app.backend.service.business.app;


import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.AppPararameterCryptoService;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.controller.app.AppControllerDB;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.App;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

@Component
public class AppService {
	
	
	private Logger logger = LoggerFactory.getLogger(AppService.class);
	
	
	//General INFO
	@Value("${app.postgres.pgcryto.key}")
	private String postgresBDcryptoKey;
	
	

	@Value("${app.postgres.pgcryto.algo}")
	private String postgresBDcryptoAlgoritmo;
	

	@Value("${server.servlet.context-path}")
	private String contextoAPP;

	@Value("${app.aes.encrypt.key}")
	private String aesEncryptKey;
	
	
	
	
	
	//Login variables
	@Value("${app.login.password.attempt-failed.max-attempt}")
	private String login_password_max_attempt_blocked;
	
	
	@Value("${app.login.username.attempt-failed.max-attempt}")
	private String login_username_max_attempt_blocked;


	//Captcha
	@Value("${app.google.recaptcha.url}")
	private String google_recaptchaUrl;

	@Value("${app.google.recaptcha.secret-key}")
	private String google_recaptchaSecretKey;
	
	
	@Value("${app.google.recaptcha.site-key}")
	private String google_recaptchaSiteKey;
	
	
	//Threads
	@Value("${app.thread.executor.acodes.mailsending.delay}")
	private String thread_acodes_mailsending_delay;
	
	
	
	


	@Autowired
	AppControllerDB controllerDB;

	@Autowired
	ResponseFailedServiceGenerator generadorExcepcionServicio;
	
	
	public ParametroConfiguracionGeneral getParameter(String cnfg_cnfg) throws RespuestaFallidaServicio  {
		
		ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(cnfg_cnfg);
		
		if(parameter==null) {
			
			reloadCache();
			
			parameter =  GeneralParametersCacheService.getParameter(cnfg_cnfg);
			
		}
		
		if(parameter==null) {
			logger.error("Config General: Parameter "+cnfg_cnfg+" No exist.");
			 
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CNFG_NE);
			
		}
		
		
		return parameter;
		
		
		
	}
	
	
	public void  reloadCache(){

		ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio("APP", "0.0.0.0", "NA");
		
		try {
			List<ParametroConfiguracionGeneral> lista = getConfGeneral(controlOperacionServicio);
			
			GeneralParametersCacheService.registerParameters(lista);
			
			
		} catch (RespuestaFallidaServicio e) {
			logger.error(e.getMensaje());
		}
	
	}
	
	
	

	public List<ParametroConfiguracionGeneral> getConfGeneral(ControlOperacionServicio controlOperacion)
			throws RespuestaFallidaServicio {

		List<ParametroConfiguracionGeneral> confGeneral = null;

		if (controlOperacion != null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(),
						controlOperacion.getIp(), null);

				confGeneral = controllerDB.getConfGeneral(controlOperacionBD);
			} catch (ResponseFailedBD e) {

				if (e != null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				} else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}

		} else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return confGeneral;
	}

	public Boolean crearConfGeneral(ControlOperacionServicio controlOperacion,
			ParametroConfiguracionGeneral confGeneral) throws RespuestaFallidaServicio {

		Boolean sinErrores = false;

		if (controlOperacion != null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(),
					controlOperacion.getIp(), null);

			try {
				sinErrores = controllerDB.crearConfGeneral(controlOperacionBD, confGeneral);
				
				reloadCache();
				
				

			} catch (ResponseFailedBD e) {

				if (e != null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				} else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}

		} else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean actualizarConfGeneral(ControlOperacionServicio controlOperacion,
			ParametroConfiguracionGeneral confGeneral) throws RespuestaFallidaServicio {

		Boolean sinErrores = false;

		if (controlOperacion != null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(),
					controlOperacion.getIp(), null);

			try {
				sinErrores = controllerDB.actualizarConfGeneral(controlOperacionBD, confGeneral);
				
				reloadCache();

			} catch (ResponseFailedBD e) {

				if (e != null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				} else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}

		} else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}
	
	
	
	
	public Date getSysdate() throws RespuestaFallidaServicio {

		Date fecha= null;
		
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, "--","--" );
		

		

		ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(),
				controlOperacion.getIp(), null);

		try {
			fecha = controllerDB.getSysdate(controlOperacionBD);


		} catch (ResponseFailedBD e) {

			if (e != null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
				return null;
			} else {
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
			}

		}

		

		return fecha;
	}
	
	
	
	public App getApp() {

		return controllerDB.getInfoApp();
	}

	
	public String getContextoAPP() {
		return contextoAPP;
	}
	
	public String getPublicoURLBase() {
		
		ParametroConfiguracionGeneral parameterPublicUrl;
		try {
			parameterPublicUrl = getParameter(ConstantesGeneralesAPP.CNFG_APP_WEB_PUBLIC_URLBASE);
			
			if(parameterPublicUrl!=null) {
				return parameterPublicUrl.getCnfg_valor();
			}
			
		} catch (RespuestaFallidaServicio e) {
			// TODO Auto-generated catch block
			logger.error(e.getMensaje());
			
			return null;
		}
		
		return null;
	}
	
	
	
	
	
	
	/*
	 * ======================================================
	 * === PARAMETROS ARCHIVO APLICATION.PROPERTIES
	 * ======================================================
	 * Se consultan los parametros especificados en el 
	 * archivo y que son transversales para la aplicacion
	 * ------------------------------------------------------
	 */
	

	public String getPGCryptoKey() {
		
		String idParameter = ConstantesGeneralesAPP.APP_POSTGRES_PGCRYTO_KEY;
		
		
		if (postgresBDcryptoKey != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(postgresBDcryptoKey);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
		
		
	}


	public String getPGCryptoAlgoritmo() {
		
		
		String idParameter = ConstantesGeneralesAPP.APP_POSTGRES_PGCRYTO_ALGO;
		
		
		if (postgresBDcryptoKey != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(postgresBDcryptoAlgoritmo);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
		
		
		
	}


	public String getAesEncryptKey() {
		
		
		String idParameter = ConstantesGeneralesAPP.APP_AES_ENCRYPT_KEY;
		
		
		if (aesEncryptKey != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(aesEncryptKey);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
		
		
	}


	



	public String getLogin_password_max_attempt_blocked() {
		
		
		String idParameter = ConstantesGeneralesAPP.APP_LOGIN_PASSWORD_ATTEMPT_FAILED_MAX_ATTEMPT;
		
		
		if (login_password_max_attempt_blocked != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(login_password_max_attempt_blocked);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
		
		
	}


	public String getLogin_username_max_attempt_blocked() {
		
		String idParameter = ConstantesGeneralesAPP.APP_LOGIN_USERNAME_ATTEMPT_FAILED_MAX_ATTEMPT;
		
		
		if (login_username_max_attempt_blocked != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(login_username_max_attempt_blocked);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
	}


	public String getGoogle_recaptchaUrl() {
		
		
		String idParameter = ConstantesGeneralesAPP.APP_GOOGLE_RECAPTCHA_URL;
		
		
		if (google_recaptchaUrl != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(google_recaptchaUrl);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
		
		
		
	}


	public String getGoogle_recaptchaSecretKey() {
		
		String idParameter = ConstantesGeneralesAPP.APP_GOOGLE_RECAPTCHA_SECRET_KEY;
		
		
		if (google_recaptchaSecretKey != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(google_recaptchaSecretKey);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
		
	
	}


	public String getGoogle_recaptchaSiteKey() {
		
		
	String idParameter = ConstantesGeneralesAPP.APP_GOOGLE_RECAPTCHA_SITE_KEY;
		
		
		if (google_recaptchaSiteKey != null) {
			
			String decryptText = null; 
			
			ParametroConfiguracionGeneral parameter = GeneralParametersCacheService.getParameter(idParameter);
			
			if(parameter==null) {
				
				decryptText  = AppPararameterCryptoService.getInstance().desencriptar(google_recaptchaSiteKey);
				
				parameter = new ParametroConfiguracionGeneral();
				parameter.setCnfg_categoria("");
				parameter.setCnfg_clasif("");
				parameter.setCnfg_cnfg(idParameter);
				parameter.setCnfg_descri("");
				parameter.setCnfg_tipo_dato(1);
				parameter.setCnfg_valor(decryptText);
				
				
				GeneralParametersCacheService.registerParameter(parameter);
				
				return decryptText;
			}else {
				return parameter.getCnfg_valor();
				
			}
			

		} else {
			return null;
		}
		
		
	}


	public String getThread_acodes_mailsending_delay() {
		return thread_acodes_mailsending_delay;
	}
	
	


}