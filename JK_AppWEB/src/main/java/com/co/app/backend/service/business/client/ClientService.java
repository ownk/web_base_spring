package com.co.app.backend.service.business.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.controller.client.ClientControllerDB;
import com.co.app.frontend.controller.publico.client.register.ClientRegisterController;
import com.co.app.frontend.controller.publico.player.register.PlayerRegisterController;
import com.co.app.model.dto.account.Account;
import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.ClientAccessCodeAccount;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.client.RegisterCode;
import com.co.app.model.dto.client.SendEmailAccessCode;
import com.co.app.model.dto.client.SendEmailRegisterCode;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.user.User;
import com.co.app.model.dto.user.UserPendingConfirmatioRegister;

@Component
public class ClientService {

	Logger logger = LoggerFactory.getLogger(ClientService.class);

	@Autowired
	ClientControllerDB controllerDB;


	@Autowired
	ResponseFailedServiceGenerator generadorExcepcionServicio;


	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	MailServicio mailServicio;
	
	@Autowired
	I18nService i18nService;
	
	@Autowired
	AppService appService;
	

	public Boolean crearCliente(ControlOperacionServicio controlOperacion, Client clienteNuevo, User usuarioNuevo, String rcode, String otp) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	

				

					//Se normaliza la informacion 

					usuarioNuevo.setUser_email(usuarioNuevo.getUser_email().toLowerCase());
					usuarioNuevo.setUser_nick(usuarioNuevo.getUser_nick().toLowerCase());

					//Se genera clave cifrada para usuario

					String passCifrado = autenticadorServicio.cifrarInfo(usuarioNuevo.getUser_pass());
					usuarioNuevo.setUser_pass(passCifrado);

					usuarioNuevo.setUser_name(usuarioNuevo.getUser_name());
					clienteNuevo.setClient_name(clienteNuevo.getClient_name());
					clienteNuevo.setClient_numident(clienteNuevo.getClient_numident());
					clienteNuevo.setClient_tpident(clienteNuevo.getClient_tpident());


					sinErrores = controllerDB.crearCliente(controlOperacionBD,  clienteNuevo, usuarioNuevo, rcode, otp);
				


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean validarRegistro(ControlOperacionServicio controlOperacion, String idUsuario,String otp) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.validarRegistro(controlOperacionBD,idUsuario,otp);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public List<TipoDocumento> getTiposDocumento(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {

		List<TipoDocumento> tiposDocumento= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				tiposDocumento = controllerDB.getTiposDocumento(controlOperacionBD);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return tiposDocumento;
	}



	public Client getClientePorIdent(ControlOperacionServicio controlOperacion, String tpIdent, String numIdent) throws RespuestaFallidaServicio {

		Client cliente= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);


				cliente = controllerDB.getClientePorIdent(controlOperacionBD, tpIdent, numIdent);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}




			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return cliente;
	}

	public List<Account> getCuentasPorCliente(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio {

		List<Account> accounts= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				accounts = controllerDB.getCuentasPorCliente(controlOperacionBD, idClient);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return accounts;
	}

	public List<AccessCode> getCodigosAcPorCliente(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio {

		List<AccessCode> acodes= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				acodes = controllerDB.getCodigosAcPorCliente(controlOperacionBD, idClient);
				
				if(acodes!=null) {
					
					List<TemplateGames> plantillas = getPlantillasListaJuegos(controlOperacion, idClient);
					
					
					//Se asocian las plantillas a los codigos de acceso
					for (AccessCode accessCode : acodes) {
						for (TemplateGames templateGames : plantillas) {
							
							if(accessCode.getAcode_templt().equals(templateGames.getTemplt_templt())) {
								accessCode.setTemplateGames(templateGames);
							}
							
						}
					}
					
				}
				
				
				
				
				
				
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return acodes;
	}

	public List<User> getUsuariosPorCliente(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio {

		List<User> users= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				users = controllerDB.getUsuariosPorCliente(controlOperacionBD, idClient);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return users;
	}

	public List<Player> getPlayersPorCliente(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio  {

		List<Player> jugadores = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				jugadores = controllerDB.getPlayersPorCliente(controlOperacionBD,idClient);
				return jugadores;

			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public Client getClientePorUser(ControlOperacionServicio controlOperacion, String idUser) throws RespuestaFallidaServicio {

		Client cliente= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);


				cliente = controllerDB.getClientePorUser(controlOperacionBD, idUser);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}




			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return cliente;
	}

	public Boolean actualizarImagenCliente(ControlOperacionServicio controlOperacion, String idClient, String imagen) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.actualizarImagenCliente(controlOperacionBD,idClient,imagen);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public List<Game> getGamesPorAcode(ControlOperacionServicio controlOperacion, String idAcode) throws RespuestaFallidaServicio  {


		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Game> list = controllerDB.getGamesPorAcode(controlOperacionBD,idAcode);

				return list;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public List<AccessCode> crearAccessCodes(ControlOperacionServicio controlOperacion, AccessCode acode, TemplateGames template) throws RespuestaFallidaServicio  {

		List<AccessCode> listAcodes = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				
				 List<AccessCode> listAccessCodes  = new ArrayList<AccessCode>();
				 listAccessCodes.add(acode);
				
				 List<TemplateGames>  listtemplateGames = new ArrayList<TemplateGames>();
		         listtemplateGames.add(template);
		            
				
		         listAcodes = controllerDB.crearAccessCodes(controlOperacionBD,listAccessCodes,listtemplateGames);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return listAcodes;
	}

	public Boolean crearRegisterCode(ControlOperacionServicio controlOperacion, List<RegisterCode> rcode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.crearRegisterCode(controlOperacionBD,rcode);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean inactivarAccessCode(ControlOperacionServicio controlOperacion, String idAccessCode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.inactivarAccessCode(controlOperacionBD,idAccessCode);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean inactivarRegisterCode(ControlOperacionServicio controlOperacion, String idRegisterCode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.inactivarRegisterCode(controlOperacionBD,idRegisterCode);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}


	public Boolean eliminarRegisterCode(ControlOperacionServicio controlOperacion, String idRegisterCode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.eliminarRegisterCode(controlOperacionBD,idRegisterCode);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public List<Game> getGamesPorCliente(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio  {


		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Game> list = controllerDB.getGamesPorCliente(controlOperacionBD,idClient);

				return list;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public List<ClientAccessCodeAccount> getListClientAccessCodeAccount(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio{

		List<ClientAccessCodeAccount> listClientAccessCodeAccount = null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				List<AccessCode> acodes = controllerDB.getCodigosAcPorCliente(controlOperacionBD, idClient);
				List<Account> accounts = controllerDB.getCuentasPorCliente(controlOperacionBD, idClient);

				
				if(acodes!=null) {
					
					List<TemplateGames> plantillas = getPlantillasListaJuegos(controlOperacion, idClient);
					
					listClientAccessCodeAccount = new ArrayList<ClientAccessCodeAccount>();

					for (AccessCode accessCode : acodes) {
						
						if(plantillas!=null && plantillas.size()>0) {
							for (TemplateGames templateGames : plantillas) {
								
								if(accessCode.getAcode_templt().equals(templateGames.getTemplt_templt())) {
									accessCode.setTemplateGames(templateGames);
								}
								
							}
						}
						
						
						

						ClientAccessCodeAccount clientAccessCodeAccount = new ClientAccessCodeAccount();
						clientAccessCodeAccount.setClient_client(idClient);
						clientAccessCodeAccount.setAccessCode(accessCode);

						if(accounts!=null) {
							for (Account account : accounts) {
								if(accessCode.getAcode_acode().equals(account.getAcnt_acode_init())) {
									clientAccessCodeAccount.setAccount(account);
								}
							}
						}

						listClientAccessCodeAccount.add(clientAccessCodeAccount);

					}


				}



			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return listClientAccessCodeAccount;
	}

	public List<Client> getClientes(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio {

		List<Client> clientes= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				clientes = controllerDB.getClientes(controlOperacionBD);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return clientes;
	}

	public List<RegisterCode> getCodigosRegistro(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio {

		List<RegisterCode> rcodes= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				rcodes = controllerDB.getCodigosRegistro(controlOperacionBD);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return rcodes;
	}

	public List<TemplateGames> getPlantillasListaJuegos(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio {	

		List<TemplateGames> plantillas= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				plantillas = controllerDB.getPlantillasPorCliente(controlOperacionBD, idClient);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return plantillas;
	}
	
	
	public TemplateGames getPlantillaListaJuegos(ControlOperacionServicio controlOperacion, String idCliente,  String idPlantilla) throws RespuestaFallidaServicio {	

		List<TemplateGames> plantillas= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				plantillas = controllerDB.getPlantillasPorCliente(controlOperacionBD, idCliente);
				
				for (TemplateGames templateGames : plantillas) {
					if(templateGames.getTemplt_templt().equals(idPlantilla)) {
						return templateGames;
					}
					
					
				}
				
				
				
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return null;
	}
	

	public Boolean crearCantAccessCode(ControlOperacionServicio controlOperacion,List<TemplateGames> template, int cantidad, String idClient) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.crearCantAccessCode(controlOperacionBD,template,cantidad,idClient);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean crearCantRegisterCode(ControlOperacionServicio controlOperacion, int cantidad) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.crearCantRegisterCode(controlOperacionBD,cantidad);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean crearPlantilla(ControlOperacionServicio controlOperacion, List<Game> listaJuegos, String descriPlantilla, String idClient) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.crearPlantilla(controlOperacionBD,listaJuegos,descriPlantilla,idClient);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}



	public List<Player> getInfoReportePlayersClient(ControlOperacionServicio controlOperacion, String idClient, String idPlayerFilter) throws RespuestaFallidaServicio  {

		List<Player> jugadoresReporte = null;
		
		List<Player> jugadoresCliente = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				
				jugadoresCliente = controllerDB.getPlayersPorCliente(controlOperacionBD, idClient);
				
				jugadoresReporte = controllerDB.getReportePlayersClient(controlOperacionBD,idClient);
				
				for (Player playerCliente : jugadoresCliente) {
					
					for (Player playerReporte : jugadoresReporte) {
						
						
						
						
						if(playerReporte.getPlayer_player().equals(playerCliente.getPlayer_player())) {
							
							if(idPlayerFilter!=null) {
								if(playerCliente.getPlayer_player().equals(idPlayerFilter) ) {
							
									playerReporte.setUsuario(playerCliente.getUsuario());
								}
							}else {
								playerReporte.setUsuario(playerCliente.getUsuario());
							}
						}
						
					}
					
				}
				
				
				
				
				
				return jugadoresReporte;

			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<UserPendingConfirmatioRegister> getPlayersClientEmailNoConfirmado(ControlOperacionServicio controlOperacion, String client_client, String user_user) throws RespuestaFallidaServicio {
		
		
		
		List<UserPendingConfirmatioRegister> listUserPending = new ArrayList<UserPendingConfirmatioRegister>();
	    
        
		if(controlOperacion!=null) {
			
			try {
				
				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				
				
				List<User> listUsers = controllerDB.getPlayersClientEmailNoConfirmado(controlOperacionBD,client_client);
				
				
			    List<ClientAccessCodeAccount> listAccesCodes = getListClientAccessCodeAccount(controlOperacion, client_client);
			       
		        
		        for (ClientAccessCodeAccount clientAccessCodeAccount : listAccesCodes) {
		        	for (User user : listUsers) {
		        		
		        		String idUser = null;
		        		try {
							
						
		        			idUser= clientAccessCodeAccount.getAccount().getPlayerPpal().getPlayer_user();
		        		} catch (Exception e) {
							logger.error(e.getMessage());
						}
		        		
						if(user.getUser_user().equals(idUser)) {
							UserPendingConfirmatioRegister userPendingConfirmatioRegister = new UserPendingConfirmatioRegister();
							userPendingConfirmatioRegister.setAccessCode(clientAccessCodeAccount.getAccessCode());
							userPendingConfirmatioRegister.setUser(user);
							
							
							if(user_user!=null ) {
								
								if(user_user.equals(idUser)) {
									listUserPending.add(userPendingConfirmatioRegister);	
								}
								
							}else {
								listUserPending.add(userPendingConfirmatioRegister);
							}
						}
					}
		        	
		        	
				}
				
		        
		        return listUserPending;
				
				
				
			} catch (ResponseFailedBD e) {
				
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}
			
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
	}

	public String createProcessLoadMasiveAccessCode(ControlOperacionServicio controlOperacion,LoadMasiveAccessCodes lmac) throws RespuestaFallidaServicio  {

		String lmac_lmac= null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				lmac_lmac = controllerDB.createProcessLoadMasiveAccessCode(controlOperacionBD,lmac);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return lmac_lmac;
	}
	
	public List<LoadMasiveAccessCodes> getProcessLoadMasiveAccessCode(ControlOperacionServicio controlOperacion, String idClient, String idProcessFilter ) throws RespuestaFallidaServicio  {

		List<LoadMasiveAccessCodes> lmac=null;
		List<LoadMasiveAccessCodes> lmacFiltered=new ArrayList<LoadMasiveAccessCodes>();
		

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				lmac = controllerDB.getProcessLoadMasiveAccessCode(controlOperacionBD, idClient);
				
				
				if(lmac!=null && lmac.size()>0) {
					
					for (LoadMasiveAccessCodes loadMasiveAccessCodes : lmac) {
						
						TemplateGames templateGames = getPlantillaListaJuegos(controlOperacion, idClient, loadMasiveAccessCodes.getLmac_templt());
						loadMasiveAccessCodes.setTemplateGames(templateGames);
						
						if(idProcessFilter!=null && !StringUtils.isEmpty(idProcessFilter)) {
							
							if(idProcessFilter.equals(loadMasiveAccessCodes.getLmac_lmac())) {
								lmacFiltered.add(loadMasiveAccessCodes);
								break;
							}
							
							
						}else {
							
							lmacFiltered.add(loadMasiveAccessCodes);
						}
						
						
					}
					
					
					
					
				}
				
				


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return lmacFiltered;
	}
	
	
	
	public Boolean updateProcessLoadMasiveAccessCode(ControlOperacionServicio controlOperacion,String lmac_lmac, String lmac_state) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.updateProcessLoadMasiveAccessCode(controlOperacionBD,lmac_lmac,lmac_state);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	
	public List<DetailLoadMasiveAccessCode> createDetailsLoadMasiveAccessCode(ControlOperacionServicio controlOperacion,List<DetailLoadMasiveAccessCode> detailslmac) throws RespuestaFallidaServicio  {

		List<DetailLoadMasiveAccessCode> listDetailslmac = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				listDetailslmac = controllerDB.createDetailsLoadMasiveAccessCode(controlOperacionBD,detailslmac);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return listDetailslmac;
	}
	
	

	public List<DetailLoadMasiveAccessCode> getDetailsLoadMasiveAccessCode(ControlOperacionServicio controlOperacion, String lmac_lmac) throws RespuestaFallidaServicio  {

		List<DetailLoadMasiveAccessCode> dlmac=null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				dlmac = controllerDB.getDetailsLoadMasiveAccessCode(controlOperacionBD,lmac_lmac);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return dlmac;
	}
	
	public Boolean updateDetailsLoadMasiveAccessCode(ControlOperacionServicio controlOperacion,List<DetailLoadMasiveAccessCode> detailslmac) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.updateDetailsLoadMasiveAccessCode(controlOperacionBD,detailslmac);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}
	
	public Boolean deleteAccessCode(ControlOperacionServicio controlOperacion,String acode_acode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.deleteAccessCode(controlOperacionBD,acode_acode);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}
	
	public Boolean deleteRegisterCode(ControlOperacionServicio controlOperacion,String rcode_rcode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.deleteRegisterCode(controlOperacionBD,rcode_rcode);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}
	
	public Client getClienteByID(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio {

		Client cliente= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);


				cliente = controllerDB.getClientePorId(controlOperacionBD, idClient);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}




			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return cliente;
	}
	
	public Email createEmailSendAccesCode(ControlOperacionServicio controlOperacion, String toEmail, String toName, String acode_code) throws Exception {
		
		
		Email emailDto = null;
		
		String urlBase = appService.getPublicoURLBase();
		
		String urlToRegister = urlBase+appService.getContextoAPP()+PlayerRegisterController.URL_REQUEST;
		
		
		String emailSubject = i18nService.translate("send_acode.email.subject");
		String emailMessage = i18nService.translate("send_acode.email.message");



		emailDto = new Email(mailServicio.getMailUsername(), toEmail,emailSubject,emailMessage);

		Map<String, Object> parameterMap = new HashMap<String, Object>();


		ParametroConfiguracionGeneral parameterAppLogoUrl= appService.getParameter(ConstantesGeneralesAPP.CNFG_APP_LOGO_INTERNET_URL);

		String urlAppLogo = "";
		if(parameterAppLogoUrl!=null) {
			urlAppLogo = parameterAppLogoUrl.getCnfg_valor();
		}

		ParametroConfiguracionGeneral parameterAppPageUrl= appService.getParameter(ConstantesGeneralesAPP.CNFG_APP_PAGE_INTERNET_URL);

		String urlAppPage = "";
		if(parameterAppPageUrl!=null) {
			urlAppPage = parameterAppPageUrl.getCnfg_valor();
		}


		String emailHeaderTitle = i18nService.translate("send_acode.email.header.title");
		String emailHeaderSubtitle = i18nService.translate("send_acode.email.header.subtitle");
		String emailContentGreeting = i18nService.translate("send_acode.email.content.greeting");
		String emailContentMessage = i18nService.translate("send_acode.email.content.message");
		String emailContentFooter = i18nService.translate("send_acode.email.content.footer");
		String emailContentButtonRegister = i18nService.translate("send_acode.email.content.button_register");

		parameterMap.put("emailHeaderTitle", emailHeaderTitle);
		parameterMap.put("emailHeaderSubtitle", emailHeaderSubtitle);
		parameterMap.put("emailContentGreeting", emailContentGreeting);
		parameterMap.put("emailContentMessage", emailContentMessage);
		parameterMap.put("emailContentFooter", emailContentFooter);
		parameterMap.put("emailContentButtonRegister", emailContentButtonRegister);

		parameterMap.put("urlAppPage", urlAppPage);
		parameterMap.put("urlAppLogo", urlAppLogo);


		parameterMap.put("urlToRegister", urlToRegister );
		parameterMap.put("name", toName);
		parameterMap.put("email", toEmail);
		parameterMap.put("acode", acode_code);
		emailDto.setParameterMap(parameterMap);
		
		
		
		
		
		
		return emailDto;

		
	}
	
	
	public Email createEmailSendClientCode(ControlOperacionServicio controlOperacion, String toEmail, String toName, String rcode_code) throws Exception {
		
		
		Email emailDto = null;
		
		String urlBase = appService.getPublicoURLBase();
		
		String urlToRegister = urlBase+appService.getContextoAPP()+ClientRegisterController.URL_REQUEST;
		
		
		String emailSubject = i18nService.translate("send_rcode.email.subject");
		String emailMessage = i18nService.translate("send_rcode.email.message");



		emailDto = new Email(mailServicio.getMailUsername(), toEmail,emailSubject,emailMessage);

		Map<String, Object> parameterMap = new HashMap<String, Object>();


		ParametroConfiguracionGeneral parameterAppLogoUrl= appService.getParameter(ConstantesGeneralesAPP.CNFG_APP_LOGO_INTERNET_URL);

		String urlAppLogo = "";
		if(parameterAppLogoUrl!=null) {
			urlAppLogo = parameterAppLogoUrl.getCnfg_valor();
		}

		ParametroConfiguracionGeneral parameterAppPageUrl= appService.getParameter(ConstantesGeneralesAPP.CNFG_APP_PAGE_INTERNET_URL);

		String urlAppPage = "";
		if(parameterAppPageUrl!=null) {
			urlAppPage = parameterAppPageUrl.getCnfg_valor();
		}


		String emailHeaderTitle = i18nService.translate("send_rcode.email.header.title");
		String emailHeaderSubtitle = i18nService.translate("send_rcode.email.header.subtitle");
		String emailContentGreeting = i18nService.translate("send_rcode.email.content.greeting");
		String emailContentMessage = i18nService.translate("send_rcode.email.content.message");
		String emailContentFooter = i18nService.translate("send_rcode.email.content.footer");
		String emailContentButtonRegister = i18nService.translate("send_rcode.email.content.button_register");

		parameterMap.put("emailHeaderTitle", emailHeaderTitle);
		parameterMap.put("emailHeaderSubtitle", emailHeaderSubtitle);
		parameterMap.put("emailContentGreeting", emailContentGreeting);
		parameterMap.put("emailContentMessage", emailContentMessage);
		parameterMap.put("emailContentFooter", emailContentFooter);
		parameterMap.put("emailContentButtonRegister", emailContentButtonRegister);

		parameterMap.put("urlAppPage", urlAppPage);
		parameterMap.put("urlAppLogo", urlAppLogo);


		parameterMap.put("urlToRegister", urlToRegister );
		parameterMap.put("name", toName);
		parameterMap.put("email", toEmail);
		parameterMap.put("rcode", rcode_code);
		emailDto.setParameterMap(parameterMap);
		
		
		return emailDto;

		
	}
	
	
	public Boolean sendEmailAccesCode(Locale locale, ControlOperacionServicio controlOperacionServicio, AccessCode accessCode, SendEmailAccessCode sendEmailAcode  ) throws Exception {
		
		
		Email emailDto = createEmailSendAccesCode(controlOperacionServicio, sendEmailAcode.getSeac_email(), sendEmailAcode.getSeac_name(), accessCode.getAcode_code() );
		
		//Envio de correo de confirmacion
		if(emailDto!=null) {

			try {
				mailServicio.sendHtmlTemplateEmail(locale, emailDto, MailServicio.HTML_TEMPLATE_EMAIL_SEND_ACODE);
				
				registerSendEmailAcode(controlOperacionServicio, sendEmailAcode);
				
				return true;

			} catch (Exception e) {

				throw e;

			}


		}else {
			throw new Exception("ERR_SEND_ACODE_MAIL_NV");
		}
		
		
	}
	
	public Boolean sendEmailClientCode(Locale locale, ControlOperacionServicio controlOperacionServicio, RegisterCode registerCode, SendEmailRegisterCode sendEmailRcode  ) throws Exception {
		
		
		Email emailDto = createEmailSendClientCode(controlOperacionServicio, sendEmailRcode.getSerc_email(), sendEmailRcode.getSerc_name(), registerCode.getRcode_code() );
		
		//Envio de correo de confirmacion
		if(emailDto!=null) {

			try {
				mailServicio.sendHtmlTemplateEmail(locale, emailDto, MailServicio.HTML_TEMPLATE_EMAIL_SEND_RCODE);
				
				registerSendEmailRcode(controlOperacionServicio, sendEmailRcode);
				
				return true;

			} catch (Exception e) {

				throw e;

			}


		}else {
			throw new Exception("ERR_SEND_RCODE_MAIL_NV");
		}
		
		
	}
	
	
	
	
	
	public Boolean registerSendEmailAcode(ControlOperacionServicio controlOperacion, SendEmailAccessCode sendEmailAcode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;
		
		

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {
				
				List<SendEmailAccessCode> list = new ArrayList<SendEmailAccessCode>(); 
				list.add(sendEmailAcode);
				
				sinErrores = controllerDB.registerSendEmailAcode(controlOperacionBD,list);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}
	
	public Boolean registerSendEmailRcode(ControlOperacionServicio controlOperacion,SendEmailRegisterCode sendEmailRcode) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				
				List<SendEmailRegisterCode> list = new ArrayList<SendEmailRegisterCode>(); 
				list.add(sendEmailRcode);
				sinErrores = controllerDB.registerSendEmailRcode(controlOperacionBD,list);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}
	
}

