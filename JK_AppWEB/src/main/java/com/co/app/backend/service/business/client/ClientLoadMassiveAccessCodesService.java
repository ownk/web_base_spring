package com.co.app.backend.service.business.client;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

@Service
public class ClientLoadMassiveAccessCodesService {
	
	private Logger logger = LoggerFactory.getLogger(ClientLoadMassiveAccessCodesService.class);
	
	@Autowired
	ClientService clientService;

	
	@Autowired
	AppService appService;
	
	@Autowired
	I18nService i18nService;
	
	
	@Autowired
	MailServicio mailServicio;
	
	@Autowired
	ClientDetailtLoadMassiveAccessCodesService clientDetailtLoadMassiveAccessCodesService;
	
	
	
	public LoadMasiveAccessCodes registerNewProcess(ControlOperacionServicio controlOperacionServicio, String idUser, String idClient, String idGroupGames, File excel, String processName, List<DetailLoadMasiveAccessCode> details) {
		
		
		
		if(StringUtils.isEmpty(processName)) {
			processName = generateProcessName();
		}
		
		
		
		LoadMasiveAccessCodes lmac = new LoadMasiveAccessCodes();
		lmac.setLmac_client(idClient);
		lmac.setLmac_file_trg(details.size());
		lmac.setLmac_file_url(excel.getAbsolutePath());
		lmac.setLmac_state(LoadMasiveAccessCodes.STATE_PENDING);
		lmac.setLmac_templt(idGroupGames);
		lmac.setLmac_user(idUser);
		lmac.setLmac_descri(processName);
		
		try {
			String lmac_lmac = clientService.createProcessLoadMasiveAccessCode(controlOperacionServicio, lmac);
			
			if(lmac_lmac!=null && !StringUtils.isEmpty(lmac_lmac)) {
				
				lmac.setLmac_lmac(lmac_lmac);
				
				List<LoadMasiveAccessCodes> listProcess = clientService.getProcessLoadMasiveAccessCode(controlOperacionServicio, idClient, lmac_lmac);
				if(listProcess!=null && listProcess.size()>0) {
					lmac = listProcess.get(0);
				}
			}else {
				lmac = null;
			}
		} catch (RespuestaFallidaServicio e) {
			
			logger.debug(e.getMensaje());
			lmac = null;
		}
		
		return lmac;
		
	}
	
	
	
	@Async(value = "executorThreadAcodesMailSending")
	public void executeProcessAsync(Locale locale, ControlOperacionServicio controlOperacionServicio, LoadMasiveAccessCodes loadMasiveAccessCodes, List<DetailLoadMasiveAccessCode> details) {
		
		logger.info(Thread.currentThread().getName());
		
		List<CompletableFuture<TaskCreateSendAccesCode>> listCompletableFuture = new ArrayList<CompletableFuture<TaskCreateSendAccesCode>>();
		
		
		changeProcessState(controlOperacionServicio, loadMasiveAccessCodes.getLmac_lmac(), LoadMasiveAccessCodes.STATE_PROCESSING);
		
		String msgInit = i18nService.translate("ClientLoadMassiveAccesCodesAnsyc.info_detail.MSG_PEND_TO_PROCESS");
		
		
		for (DetailLoadMasiveAccessCode detail : details) {
			detail.setDlmac_lmac(loadMasiveAccessCodes.getLmac_lmac());
			detail.setDlmac_state(DetailLoadMasiveAccessCode.STATE_PENDING);
			detail.setDlmac_mnsg(msgInit);
			
			logger.info("start detail "+detail.getDlmac_email());
			
			CompletableFuture<TaskCreateSendAccesCode> individualProcess = clientDetailtLoadMassiveAccessCodesService.createAccesCodeAndSendAsync(locale, controlOperacionServicio,loadMasiveAccessCodes,detail);
		    listCompletableFuture.add(individualProcess);
		}
		
		CompletableFuture<TaskCreateSendAccesCode>[] arrayCompletableFuture = new  CompletableFuture[listCompletableFuture.size()];
		listCompletableFuture.toArray(arrayCompletableFuture);
		

	    // this will throw an exception if any of the futures complete exceptionally
	    CompletableFuture.allOf(arrayCompletableFuture).join();

	   
	    
	    try {
	    	TaskCreateSendAccesCode[] arraytaskCompleted = Arrays.stream(arrayCompletableFuture).map(CompletableFuture::join).toArray(TaskCreateSendAccesCode[]::new);
			
	    	Boolean isAllOk = true;
			for (TaskCreateSendAccesCode taskCreateSendAccesCode : arraytaskCompleted) {
				
				if(!taskCreateSendAccesCode.getSinErrores()) {
					isAllOk = false;
				}
				
			}
			
			changeProcessState(controlOperacionServicio, loadMasiveAccessCodes.getLmac_lmac(), LoadMasiveAccessCodes.STATE_END);
			
			
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	    
	    
	}
	

	
	
	private void changeProcessState(ControlOperacionServicio controlOperacion, String idProcess, String state) {
		
		try {
			clientService.updateProcessLoadMasiveAccessCode(controlOperacion, idProcess, state);
		} catch (RespuestaFallidaServicio e) {
			
		}
	}
	
	
	
	private String generateProcessName() {
		Date sysdate =null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e1) {
			sysdate = new Date();
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
    	String fecha = format.format(sysdate);
		
		format = new SimpleDateFormat("HHmmssSSS");
		String hora = format.format(sysdate);
		
		String idFileTemp = fecha+"_"+hora;
		return idFileTemp;
	}
	
	
	
	
	
	

}
