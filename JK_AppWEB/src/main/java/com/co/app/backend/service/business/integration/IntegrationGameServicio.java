package com.co.app.backend.service.business.integration;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.controller.integration.IntegrationGameControllerDB;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Score;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.integration.GameTokenIntegration;
import com.co.app.model.dto.integration.Partner;
import com.co.app.model.dto.integration.SessionIntegration;

@Component
public class IntegrationGameServicio {
	
	Logger logger = LoggerFactory.getLogger(IntegrationGameServicio.class);
	
	@Autowired
	IntegrationGameControllerDB controllerDB;
	
	@Autowired
	ResponseFailedServiceGenerator generadorExcepcionServicio;
	

	public Partner getPartner(ControlOperacionServicio controlOperacion, String siteKey, String accessKey) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.getPartnerPorKeys(controlOperacionBD, siteKey, accessKey);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {			
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<Partner> getPartners(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.getPartners(controlOperacionBD);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {			
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public Boolean crearPartner(ControlOperacionServicio controlOperacion,Partner partner) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.crearPartner(controlOperacionBD, partner);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean inactivarPartner(ControlOperacionServicio controlOperacion,String idPartner) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.inactivarPartner(controlOperacionBD, idPartner);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public String iniciarSession(ControlOperacionServicio controlOperacion,String idPartner, String token) throws RespuestaFallidaServicio  {
		
		String idSession = "";
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				idSession = controllerDB.iniciarSession(controlOperacionBD,idPartner, token);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return idSession;
	}
	
	public Boolean finalizarSession(ControlOperacionServicio controlOperacion,String idSession, String token) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.finalizarSession(controlOperacionBD,idSession, token);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	

	public String iniciarMatch(ControlOperacionServicio controlOperacion, String idPlayer,String idGame, String idSession, String idGToken) throws RespuestaFallidaServicio  {
		
		String idMatch;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				idMatch = controllerDB.iniciarMatch(controlOperacionBD,idPlayer,idGame,idSession, idGToken);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return idMatch;
	}
	
	public Boolean actualizarMatch(ControlOperacionServicio controlOperacion,Match match, List<Score> score, String idSession) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.actualizarMatch(controlOperacionBD,match,score, idSession);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public SessionIntegration getSession(ControlOperacionServicio controlOperacion, String idSession) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.getSession(controlOperacionBD, idSession);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {			
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	
	public String createGToken(ControlOperacionServicio controlOperacion,String idPlayer, String idGame, String sessionID) throws RespuestaFallidaServicio  {
		
		String idGtoken = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				idGtoken = controllerDB.createGToken(controlOperacionBD, idPlayer, idGame, sessionID);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return idGtoken;
	}
	
	
	public GameTokenIntegration getGToken(ControlOperacionServicio controlOperacion, String idGToken) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.getGToken(controlOperacionBD, idGToken);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {			
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	
}
