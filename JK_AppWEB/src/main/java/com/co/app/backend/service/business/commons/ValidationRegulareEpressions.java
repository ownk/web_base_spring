package com.co.app.backend.service.business.commons;


import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

@Service
public class ValidationRegulareEpressions {

	@Autowired
	AppService appService;

	private Logger logger = LoggerFactory.getLogger(StringUtils.class);

	public static String textNickName = "^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$";
	public static String textNickNameFront = "[^(?!.*\\\\.\\\\.)(?!.*\\\\.$)[^\\\\W][\\\\w.]{0,29}]?";
	public static String textAlfanumeric= "[A-Za-z0-9\\s]{1,200}$";
	public static String textIdentfication= "[A-Za-z0-9\\s]{1,200}$";

	public Boolean isEmail  (String text) throws RespuestaFallidaServicio {

		Boolean validation = false ;
		String emailRegexp = "[^@]+@[^@]+\\.[a-zA-Z]{2,}";

		ParametroConfiguracionGeneral parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_AUT_USER_EMAIL_VALIDATION_REGEX);

		if (parameter != null) {
			emailRegexp = parameter.getCnfg_valor();
		}


		if(Pattern.matches(emailRegexp, text)) {

			validation = true;
		}


		return validation;
	}

	public Boolean isTextNumber  (String text) throws RespuestaFallidaServicio {

		Boolean validation = false ; 
		

		ParametroConfiguracionGeneral parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_GENERAL_REGEX_ALFANUMERIC);

		if (parameter != null) {
			textAlfanumeric = parameter.getCnfg_valor();
		}

		if(Pattern.matches(textAlfanumeric, text)) {

			validation = true;
		}


		return validation;
	}

	public Boolean isNickName  (String text) throws RespuestaFallidaServicio {

		Boolean validation = false ; 


		ParametroConfiguracionGeneral parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_AUT_USER_USERNAME_VALIDATION_REGEX);

		if (parameter != null) {
			textNickName = parameter.getCnfg_valor();
		}

		if(Pattern.matches(textNickName, text)) {

			validation = true;
		}


		return validation;
	}

	public String getNickName() throws RespuestaFallidaServicio {

		String nickNameRegex = null	;
		ParametroConfiguracionGeneral parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_AUT_USER_USERNAME_VALIDATION_FRONT_REGEX);

		if (parameter != null) {
			nickNameRegex = parameter.getCnfg_valor();
		}else {
			nickNameRegex=textNickNameFront;

		}



		return nickNameRegex;
	}
	
	
	public Boolean isIdentification(String text) throws RespuestaFallidaServicio {

		Boolean validation = false ; 


		ParametroConfiguracionGeneral parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_GENERAL_REGEX_ALFANUMERIC);

		if (parameter != null) {
			textIdentfication = parameter.getCnfg_valor();
		}

		if(Pattern.matches(textIdentfication, text)) {

			validation = true;
		}


		return validation;
	}

}
