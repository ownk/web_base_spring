package com.co.app.backend.service.persistence.controller.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.persistence.CommonsParameterBDGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.ResponseFailedBDGenerator;
import com.co.app.backend.service.persistence.mapper.client.ClientMapper;
import com.co.app.model.dto.account.AcAccount;
import com.co.app.model.dto.account.Account;
import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.GmsbyTemplate;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.client.RegisterCode;
import com.co.app.model.dto.client.SendEmailAccessCode;
import com.co.app.model.dto.client.SendEmailRegisterCode;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjByGame;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.PlayerAccount;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.user.OtpUser;
import com.co.app.model.dto.user.User;


@Component
public class ClientControllerDB {


	private Logger logger = LoggerFactory.getLogger(ClientControllerDB.class);

	@Autowired
	private ClientMapper mapper;

	@Autowired
	ResponseFailedBDGenerator generadorExcepcionBD;

	@Autowired
	private AppService appServicio;

	@Autowired
	private I18nService i18nService;

	public Boolean crearCliente(ControlOperacionBD controlOperacionBD,Client clienteParaCrear, User usuarioNuevo, String rcode, String otp)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tclient", clienteParaCrear);
			parametrosInOout.put("p_tuser", usuarioNuevo);
			parametrosInOout.put("p_rcode", rcode);
			parametrosInOout.put("p_token", otp);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.registrarCliente(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		}  catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean validarRegistro(ControlOperacionBD controlOperacionBD,String idUsuario,String otp)  throws ResponseFailedBD {


		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", idUsuario);
			parametrosInOout.put("p_otpu", otp);
			mapper.validarRegistro(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Client getClientePorIdent(ControlOperacionBD controlOperacionBD, String tpIdent, String numIdent) throws ResponseFailedBD{


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		Client cliente= null;

		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put("p_tp_ident", tpIdent);
			parametrosInOout.put("p_num_ident", numIdent);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getClientePorIdent(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Client> lista = (List<Client>) parametrosInOout.get("p_tclient");
				if (!lista.isEmpty()) {
					cliente = lista.get(0);
				} else {
					return null;
				}
			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}




		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return cliente;

	}


	public List<Account> getCuentasPorCliente(ControlOperacionBD controlOperacionBD, String idClient) throws ResponseFailedBD{


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<Account> listaAccounts= null;
		List<AcAccount> listaAcAccount= null;
		List<PlayerAccount> listaPlayerAccount= null;
		List<Player> listaPlayers= null;
		List<AccessCode> listaAcodes= null;


		String pgcriptoKey 	= appServicio.getPGCryptoKey();

		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put("p_client_client", idClient);

			mapper.getCuentasPorCliente(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				listaAccounts = (List<Account>) parametrosInOout.get("p_taccount");
				listaPlayerAccount = (List<PlayerAccount>) parametrosInOout.get("p_tplacn");
				listaPlayers = (List<Player>) parametrosInOout.get("p_tplayer");
				listaAcAccount = (List<AcAccount>) parametrosInOout.get("p_tacac");
				listaAcodes = (List<AccessCode>) parametrosInOout.get("p_tacode");


				if(listaAccounts!=null) {

					for (Account account : listaAccounts) {

						if(listaPlayerAccount!=null) {
							for (PlayerAccount playerAccount : listaPlayerAccount) {

								if(account.getAcnt_acnt().equals(playerAccount.getPlacn_acnt())) {
									if(listaPlayers!=null) {
										for (Player player : listaPlayers) {
											if(player.getPlayer_player().equals(playerAccount.getPlacn_player())) {
												account.addPlayer(player);
											}
										}
									}

								}

							}
						}


						if(listaAcAccount!=null) {
							for (AcAccount acAccount : listaAcAccount) {


								if(account.getAcnt_acnt().equals(acAccount.getAcac_acnt())) {

									if(listaAcodes!=null) {
										for (AccessCode accessCode : listaAcodes) {
											if(accessCode.getAcode_acode().equals(acAccount.getAcac_acode())) {
												account.addAccesCode(accessCode);
											}

											if(accessCode.getAcode_acode().equals(account.getAcnt_acode_init())) {
												account.setAccessCodeInit(accessCode);
											}

										}
									}

								}




							}
						}


					}
				}

			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return listaAccounts;

	}

	public List<AccessCode> getCodigosAcPorCliente(ControlOperacionBD controlOperacionBD, String idClient) throws ResponseFailedBD{


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<AccessCode> listaAcodes= null;

		List<SendEmailAccessCode> listasendEmailAcode= null;


		String pgcriptoKey 	= appServicio.getPGCryptoKey();


		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getCodigosAcPorCliente(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				listaAcodes = (List<AccessCode>) parametrosInOout.get("p_tacode");
				listasendEmailAcode = (List<SendEmailAccessCode>) parametrosInOout.get("p_tseac");


				for (AccessCode aCode : listaAcodes) {
					List<SendEmailAccessCode> listTempsendEmailAcode= new ArrayList<SendEmailAccessCode>();

					if (listasendEmailAcode!= null) {

						for (SendEmailAccessCode sendEmailAc : listasendEmailAcode) {

							if(aCode.getAcode_acode().equals(sendEmailAc.getSeac_acode())) {

								listTempsendEmailAcode.add(sendEmailAc);
							}
							
						}
					}
					aCode.setSendEmaiAc(listTempsendEmailAcode);

				}

				

			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return listaAcodes;

	}

	public List<User> getUsuariosPorCliente(ControlOperacionBD controlOperacionBD, String idClient) throws ResponseFailedBD{


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<User> listaUsers= null;

		String pgcriptoKey 	= appServicio.getPGCryptoKey();


		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getUsuariosPorCliente(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				listaUsers = (List<User>) parametrosInOout.get("p_tuser");
			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return listaUsers;

	}

	public List<Player> getPlayersPorCliente(ControlOperacionBD controlOperacionBD, String idClient)  throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<Player> players = new ArrayList<Player>();



		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getPlayersPorCliente(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");		

				List<User> usuarios= (List<User>) parametrosInOout.get("p_tuser");	

				for (Player player : players) {
					i18nService.applyi18nPlayer(player);

					if(usuarios!=null) {
						for (User usuario : usuarios) {
							if(usuario.getUser_user().equals(player.getPlayer_user())) {
								player.setUsuario(usuario);
							}
						}
					}

				}





			} else {
				throw exceptionGeneralBD;
			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}

		return players;

	}

	public Client getClientePorUser(ControlOperacionBD controlOperacionBD, String idUser) throws ResponseFailedBD{


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		Client cliente= null;

		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", idUser);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getClientePorUser(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Client> lista = (List<Client>) parametrosInOout.get("p_tclient");
				if (!lista.isEmpty()) {
					cliente = lista.get(0);
				} else {
					return null;
				}
			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return cliente;

	}

	public Boolean actualizarImagenCliente(ControlOperacionBD controlOperacionBD,String idClient, String imagen)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put("p_client_image", imagen);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.actualizarImagenCliente(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		}  catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public List<Game> getGamesPorAcode(ControlOperacionBD controlOperacionBD, String idAcode)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<Game> listaGames = new ArrayList<Game>();

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_acode", idAcode);

			mapper.getGamesPorAcode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				listaGames = (List<Game>) parametrosInOout.get("p_tgame");		

				if(listaGames!=null) {
					Collections.sort(listaGames);

					for (Game game : listaGames) {
						i18nService.applyi18nGame(game);
					}

				}



			} else {
				throw exceptionGeneralBD;
			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}


		return listaGames;

	}

	public List<AccessCode> crearAccessCodes(ControlOperacionBD controlOperacionBD, List<AccessCode> acode, List<TemplateGames> template)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();


		List<AccessCode> accessCodes = null;

		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tacode", acode);
			parametrosInOout.put("p_ttemplt", template);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.crearAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);

			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

				accessCodes = (List<AccessCode>)parametrosInOout.get("p_tacode_out");

				if(accessCodes!=null && accessCodes.size()>0) {
					return accessCodes;
				}else{
					return null;
				}


			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}




	}

	public Boolean crearRegisterCode(ControlOperacionBD controlOperacionBD, List<RegisterCode> rcode)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_trcode", rcode);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.crearRegisterCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean inactivarAccessCode(ControlOperacionBD controlOperacionBD,String idAccessCode)  throws ResponseFailedBD {


		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_acode_acode", idAccessCode);
			mapper.inactivarAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean inactivarRegisterCode(ControlOperacionBD controlOperacionBD,String idRegisterCode)  throws ResponseFailedBD {


		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_rcode_rcode", idRegisterCode);
			mapper.inactivarRegisterCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean eliminarRegisterCode(ControlOperacionBD controlOperacionBD,String idRegisterCode)  throws ResponseFailedBD {


		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_rcode_rcode", idRegisterCode);
			mapper.eliminarRegisterCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public List<Game> getGamesPorCliente(ControlOperacionBD controlOperacionBD, String idClient)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<Game> listaGames = new ArrayList<Game>();

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);

			mapper.getGamesPorCliente(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				listaGames = (List<Game>) parametrosInOout.get("p_tgame");		

				if(listaGames!=null) {
					Collections.sort(listaGames);

					for (Game game : listaGames) {
						i18nService.applyi18nGame(game);
					}

				}



			} else {
				throw exceptionGeneralBD;
			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}


		return listaGames;

	}

	public List<Client> getClientes(ControlOperacionBD controlOperacionBD) throws ResponseFailedBD{


		String pgcriptoKey = appServicio.getPGCryptoKey();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<Client> clientes= null;

		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getClientes(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				clientes = (List<Client>) parametrosInOout.get("p_tclient");

			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return clientes;

	}

	public List<TipoDocumento> getTiposDocumento(ControlOperacionBD controlOperacionBD) throws ResponseFailedBD{


		String pgcriptoKey = appServicio.getPGCryptoKey();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<TipoDocumento> tipoDocumentos= null;

		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			mapper.getTiposDocumento(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				tipoDocumentos = (List<TipoDocumento>) parametrosInOout.get("p_ttdoc");


				if(tipoDocumentos!=null){
					for (TipoDocumento tipoDocumento : tipoDocumentos) {
						i18nService.applyi18nTipoDocumento(tipoDocumento);

					}

				}

			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return tipoDocumentos;

	}

	public List<RegisterCode> getCodigosRegistro(ControlOperacionBD controlOperacionBD) throws ResponseFailedBD{


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<RegisterCode> listaRcodes= null;
		List<Client> listaClientes= null;
		List<SendEmailRegisterCode> listaSendEmailRcode= null;

		String pgcriptoKey 	= appServicio.getPGCryptoKey();


		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getCodigosRegistro(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				listaRcodes = (List<RegisterCode>) parametrosInOout.get("p_trcode");
				listaClientes = (List<Client>) parametrosInOout.get("p_tclient");

				listaSendEmailRcode = (List<SendEmailRegisterCode>) parametrosInOout.get("p_tserc");


				if(listaRcodes !=null) {

					for (RegisterCode registerCode : listaRcodes) {

						if(listaClientes!=null && registerCode.getRcode_client()!=null) {
							for (Client client : listaClientes) {
								if(registerCode.getRcode_client().equals(client.getClient_client())) {
									registerCode.setClient(client);
									break;
								}


							}
						}
						
						List<SendEmailRegisterCode> listTempsendEmailRcode= new ArrayList<SendEmailRegisterCode>();

						if (listaSendEmailRcode!= null) {

							for (SendEmailRegisterCode sendEmailRc : listaSendEmailRcode) {

								if(registerCode.getRcode_rcode().equals(sendEmailRc.getSerc_rcode())) {

									listTempsendEmailRcode.add(sendEmailRc);
								}
								
							}
						}
						registerCode.setSendEmaiRc(listTempsendEmailRcode);


					}

				}





			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return listaRcodes;

	}

	public List<TemplateGames> getPlantillasPorCliente(ControlOperacionBD controlOperacionBD, String idClient) throws ResponseFailedBD{


		String pgcriptoKey = appServicio.getPGCryptoKey();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<TemplateGames> plantillas= null;
		List<Game> listaGames = new ArrayList<Game>();
		List<GmsbyTemplate> juegosPlantillas= null;


		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			mapper.getPlantillas(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				plantillas = (List<TemplateGames>) parametrosInOout.get("p_ttemplt");
				listaGames = (List<Game>) parametrosInOout.get("p_tgame");		
				juegosPlantillas = (List<GmsbyTemplate>) parametrosInOout.get("p_ttempgm");	


				if (plantillas!=null && listaGames!=null &&juegosPlantillas!=null) {
					for (TemplateGames plantilla : plantillas) {	
						List<Game> listaGamesPlantilla = new ArrayList<Game>();
						for (GmsbyTemplate juegosPlantilla:juegosPlantillas) {
							if(juegosPlantilla.getTempgm_templt().equals(plantilla.getTemplt_templt())) {
								for (Game game : listaGames) {
									if (juegosPlantilla.getTempgm_game().equals(game.getGame_game())) {
										listaGamesPlantilla.add(game);
									}
								}

							}
						}
						plantilla.setGames(listaGamesPlantilla);

					}
				}





			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return plantillas;

	}


	public Boolean crearCantAccessCode(ControlOperacionBD controlOperacionBD, List<TemplateGames> template, int cantidad, String idClient)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_cantidad", cantidad);
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put("p_ttemplt", template);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.crearCantAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean crearCantRegisterCode(ControlOperacionBD controlOperacionBD, int cantidad)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_cantidad", cantidad);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.crearCantRegisterCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean crearPlantilla(ControlOperacionBD controlOperacionBD, List<Game> listaJuegos, String descriPlantilla, String idClient)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tgame", listaJuegos);
			parametrosInOout.put("p_templt_descri", descriPlantilla);
			parametrosInOout.put("p_client_client", idClient);


			mapper.crearPlantilla(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}



	public List<Player> getReportePlayersClient(ControlOperacionBD controlOperacionBD, String idClient)  throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<Player> players = new ArrayList<Player>();
		List<Game> games = new ArrayList<Game>();
		List<Match> matches = new ArrayList<Match>();
		List<LearningObjective> lrnobjs = new ArrayList<LearningObjective>();
		List<LearningObjByGame> lrnogms = new ArrayList<LearningObjByGame>();



		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);


			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getReportePlayersClient(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");		
				games = (List<Game>) parametrosInOout.get("p_tgame");	
				matches = (List<Match>) parametrosInOout.get("p_tmatch");	
				lrnobjs = (List<LearningObjective>) parametrosInOout.get("p_tlrnobj");		
				lrnogms = (List<LearningObjByGame>) parametrosInOout.get("p_tlrnogm");

				for (LearningObjective lrnobj:lrnobjs) {

					i18nService.applyi18nLearningObjectives(lrnobj);

				}

				for (Game game:games) {

					i18nService.applyi18nGame(game);
				}


				for (Player player : players) {
					i18nService.applyi18nPlayer(player);

					List<Match> listMatch = new ArrayList<Match>();


					for (Match match : matches) {
						if(match.getMatch_player().equals(player.getPlayer_player())) {

							for (Game game:games) {
								if(game.getGame_game().equals(match.getMatch_game())) {

									List<LearningObjective> listLrnObj = new ArrayList<LearningObjective>();

									for (LearningObjByGame lrnogm:lrnogms) {
										if(lrnogm.getLrnogm_game().equals(game.getGame_game())) {



											for (LearningObjective lrnobj:lrnobjs) {
												if(lrnobj.getLrnobj_lrnobj().equals(lrnogm.getLrnogm_lrnobj())) {
													if(lrnobj.getLrnobj_clasif() ==null) {
														lrnobj.setLrnobj_clasif(lrnobj.CLASIF_DEFAULT);
													}
													listLrnObj.add(lrnobj);
												}

											}


										}
									}

									game.setLearningObjectives(listLrnObj);
									match.setGame(game);

								}
							}
							listMatch.add(match);
						}

					}

					player.setListMatch(listMatch);

				}		

			} else {
				throw exceptionGeneralBD;
			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}

		return players;

	}



	public List<User> getPlayersClientEmailNoConfirmado(ControlOperacionBD controlOperacionBD, String client_client) throws ResponseFailedBD{


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<User> usuarios = null;
		List<OtpUser> otpUsers = null;


		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put("p_client_client", client_client);
			mapper.getPlayersClientEmailNoConfirmado(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				usuarios = (List<User>) parametrosInOout.get("p_tuser");

				if (usuarios!=null && usuarios.size()>0) {

					otpUsers = (List<OtpUser>) parametrosInOout.get("p_totpu");

					if (otpUsers!=null && otpUsers.size()>0) {
						for (User usuario : usuarios) {
							for (OtpUser otpUser : otpUsers) {
								if (otpUser.getOtpu_user().equals(usuario.getUser_user())) {
									usuario.setOtpUser(otpUser);
								}
							}
						}
					}
				}
			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}




		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return usuarios;

	}

	public String createProcessLoadMasiveAccessCode(ControlOperacionBD controlOperacionBD, LoadMasiveAccessCodes lmac)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();

		String lmac_lmac= null;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tlmac", lmac);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.createProcessLoadMasiveAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				lmac_lmac = (String) parametrosInOout.get("p_lmac_lmac");

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return lmac_lmac;

	}

	public List<LoadMasiveAccessCodes> getProcessLoadMasiveAccessCode(ControlOperacionBD controlOperacionBD, String idClient) throws ResponseFailedBD{

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<LoadMasiveAccessCodes> lmac = null;


		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);


			mapper.getProcessLoadMasiveAccessCode(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				lmac = (List<LoadMasiveAccessCodes>) parametrosInOout.get("p_tlmac");

			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}




		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return lmac;

	}

	public Boolean updateProcessLoadMasiveAccessCode(ControlOperacionBD controlOperacionBD, String lmac_lmac, String lmac_state)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();



		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_lmac_lmac", lmac_lmac);
			parametrosInOout.put("p_lmac_state", lmac_state);


			mapper.updateProcessLoadMasiveAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}



	public List<DetailLoadMasiveAccessCode> createDetailsLoadMasiveAccessCode(ControlOperacionBD controlOperacionBD, List<DetailLoadMasiveAccessCode> detaillmac)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();

		List<DetailLoadMasiveAccessCode> listDetailslmac = null;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tdlmac", detaillmac);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.createDetailsLoadMasiveAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				listDetailslmac = (List<DetailLoadMasiveAccessCode>) parametrosInOout.get("p_tdlmac_out");


			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return listDetailslmac;

	}



	public List<DetailLoadMasiveAccessCode> getDetailsLoadMasiveAccessCode(ControlOperacionBD controlOperacionBD, String lmac_lmac) throws ResponseFailedBD{


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		List<DetailLoadMasiveAccessCode> dlmac = null;


		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put("p_lmac_lmac", lmac_lmac);
			mapper.getDetailsLoadMasiveAccessCode(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				dlmac = (List<DetailLoadMasiveAccessCode>) parametrosInOout.get("p_tdlmac");

			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}




		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return dlmac;

	}

	public Boolean updateDetailsLoadMasiveAccessCode(ControlOperacionBD controlOperacionBD, List<DetailLoadMasiveAccessCode> detaillmac)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();

		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tdlmac", detaillmac);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.updateDetailsLoadMasiveAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean deleteAccessCode(ControlOperacionBD controlOperacionBD, String acode_acode)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_acode_acode", acode_acode);


			mapper.deleteAccessCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}
	
	public Boolean deleteRegisterCode(ControlOperacionBD controlOperacionBD, String rcode_rcode)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();


		Boolean sinErrores = false;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_rcode_rcode", rcode_rcode);


			mapper.deleteRegisterCode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Client getClientePorId(ControlOperacionBD controlOperacionBD, String idClient) throws ResponseFailedBD{


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();


		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		Client cliente= null;

		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );



			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client",idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getClientePorId(parametrosInOout);


			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);


			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Client> lista = (List<Client>) parametrosInOout.get("p_tclient");
				if (!lista.isEmpty()) {
					cliente = lista.get(0);
				} else {
					return null;
				}
			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}




		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return cliente;

	}

	public Boolean registerSendEmailAcode(ControlOperacionBD controlOperacionBD, List<SendEmailAccessCode> sendEmailAcode)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();

		Boolean sinErrores = false;


		List<DetailLoadMasiveAccessCode> listDetailslmac = null;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tseac", sendEmailAcode);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.registerSendEmailAcode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;


			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}

	public Boolean registerSendEmailRcode(ControlOperacionBD controlOperacionBD, List<SendEmailRegisterCode> sendEmailRcode)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();

		Boolean sinErrores = false;


		List<DetailLoadMasiveAccessCode> listDetailslmac = null;

		try {


			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_tserc", sendEmailRcode);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);


			mapper.registerSendEmailRcode(parametrosInOout);

			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);


			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;


			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			//session.close();
		}


		return sinErrores;

	}
}

