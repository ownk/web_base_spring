package com.co.app.backend.service.business.user;

public class EncryptEncodeUserRegisterToken {
	
	String encryptEncodeToken;
	String encryptEncodeSalt;
	String encryptEncodeIV;
	
	
	
	public EncryptEncodeUserRegisterToken(String encryptEncodeToken,String encryptEncodeSalt,	String encryptEncodeIV) {
		
		
		this.encryptEncodeToken  =   encryptEncodeToken;
		this.encryptEncodeSalt   =   encryptEncodeSalt;
		this.encryptEncodeIV     =   encryptEncodeIV;
		
	}
	
	
	public String getencryptEncodeToken() {
		return encryptEncodeToken;
	}
	public void setencryptEncodeToken(String encryptEncodeToken) {
		this.encryptEncodeToken = encryptEncodeToken;
	}
	public String getencryptEncodeSalt() {
		return encryptEncodeSalt;
	}
	public void setencryptEncodeSalt(String encryptEncodeSalt) {
		this.encryptEncodeSalt = encryptEncodeSalt;
	}
	public String getencryptEncodeIV() {
		return encryptEncodeIV;
	}
	public void setencryptEncodeIV(String encryptEncodeIV) {
		this.encryptEncodeIV = encryptEncodeIV;
	}
	
	

}
