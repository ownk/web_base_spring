package com.co.app.backend.service.business.mail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.co.app.MailConfiguracion;
import com.co.app.backend.service.business.commons.ValidationRegulareEpressions;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.mail.Email;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;

@Service
public class MailServicio {

	public static final String HTML_TEMPLATE_EMAIL_LOGIN_PASSWORD_BLOCKED = "email_login_password_blocked";
	public static final String HTML_TEMPLATE_EMAIL_SEND_ACODE = "email_send_acode";
	public static final String HTML_TEMPLATE_EMAIL_SEND_RCODE = "email_send_rcode";
	public static final String HTML_TEMPLATE_EMAIL_USER_ACTIVED_MANUALLY = "email_user_actived_manually";
	public static final String HTML_TEMPLATE_EMAIL_REGISTER_CLIENT = "email_register_client";
	public static final String HTML_TEMPLATE_EMAIL_REGISTER_PLAYER = "email_register_player";
	public static final String HTML_TEMPLATE_EMAIL_FORGOT_PASSWORD = "email_forgot_password";

	
	
	private static final Logger log = LoggerFactory.getLogger(MailServicio.class);

	@Autowired
	JavaMailSender mailSender;

	@Autowired
	TemplateEngine templateEngine;

	@Autowired
	TemplateEngine textTemplateEngine;

	@Autowired
	TemplateEngine htmlTemplateEngine;

	@Autowired
	TemplateEngine fileTemplateEngine;

	@Autowired
	MailConfiguracion mailConfiguracion;
	
	@Autowired
	ValidationRegulareEpressions validationRegulareEpressions;
	

	private String mailUsername = null;

	/**
	 * Send emails using templates in Emailer/Templates/ directory
	 * 
	 * @param Email
	 * @return EmailDto
	 * @throws MessagingException
	 * @throws IOException
	 */
	public Email sendFileTemplateEmail(Locale locale, Email emailDto, String htmlTemplateName)
			throws MessagingException, IOException {

		// Prepare the evaluation context
		Context ctx = prepareContext(emailDto);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = this.mailSender.createMimeMessage();
		// Prepare message using a Spring helper
		MimeMessageHelper message = prepareMessage(mimeMessage, emailDto);

		// Create the HTML body using Thymeleaf
		String htmlContent = this.fileTemplateEngine.process(htmlTemplateName, ctx);
		message.setText(htmlContent, true /* isHtml */);
		emailDto.setEmailedMessage(htmlContent);

		log.debug("Processing email request: " + emailDto.toString());

		message = prepareStaticResources(message, emailDto);

		// Send mail
		this.mailSender.send(mimeMessage);

		this.fileTemplateEngine.clearTemplateCache();

		log.info("email sendend");

		return emailDto;

	}

	/**
	 * Send email using Text template
	 * 
	 * @param Email
	 * @return EmailDto
	 * @throws IOException
	 * @throws MessagingException
	 */
	public Email sendTextTemplateEmail(Locale locale, Email emailDto, String textTemplateName)
			throws IOException, MessagingException {

		// Prepare email context
		Context ctx = prepareContext(emailDto);

		// Prepare message
		MimeMessage mimeMessage = this.mailSender.createMimeMessage();
		// Prepare message using a Spring helper
		MimeMessageHelper message = prepareMessage(mimeMessage, emailDto);
		// Create email message using TEXT template
		String textContent = this.textTemplateEngine.process(textTemplateName, ctx); // text/email-text\"

		emailDto.setEmailedMessage(textContent);
		message.setText(textContent);

		// Send email
		this.mailSender.send(mimeMessage);

		return emailDto;

	}

	/**
	 * Send email with html template found in classpath resource
	 * 
	 * @param Email
	 * @return EmailDto
	 * @throws MessagingException
	 * @throws IOException
	 */
	public Email sendHtmlTemplateEmail(Locale locale, Email emailDto, String htmlTemplateName)
			throws MessagingException, IOException {

		// Prepare the evaluation context
		Context ctx = prepareContext(emailDto);
		ctx.setLocale(locale);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = this.mailSender.createMimeMessage();
		MimeMessageHelper message = prepareMessage(mimeMessage, emailDto);

		// Create the HTML body using Thymeleaf
		String htmlContent = this.htmlTemplateEngine.process(htmlTemplateName, ctx);
		message.setText(htmlContent, true /* isHtml */);
		emailDto.setEmailedMessage(htmlContent);

		log.debug("Processing html email request: " + emailDto.toString());

		message = prepareStaticResources(message, emailDto);

		// Send mail
		this.mailSender.send(mimeMessage);

		this.htmlTemplateEngine.clearTemplateCache();

		log.info("email sendend");

		return emailDto;

	}

	/**
	 * Send multiple emails using templates in Emailer/Templates/ directory
	 * 
	 * @param emailDtos
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	public List<Email> sendFileEmails(List<Email> emailDtos, String fileTemplateName)
			throws MessagingException, IOException {

		List<MimeMessage> mimeMessages = new ArrayList<MimeMessage>();
		MimeMessage mimeMessage = null;

		for (Email emailDto : emailDtos) {

			// Prepare the evaluation context
			final Context ctx = prepareContext(emailDto);

			// Prepare message using a Spring helper
			mimeMessage = this.mailSender.createMimeMessage();
			MimeMessageHelper message = prepareMessage(mimeMessage, emailDto);

			// Create the HTML body using Thymeleaf
			String htmlContent = this.fileTemplateEngine.process(fileTemplateName, ctx);
			message.setText(htmlContent, true /* isHtml */);
			emailDto.setEmailedMessage(htmlContent);

			log.debug("Processing emails request: " + emailDto.toString());

			message = prepareStaticResources(message, emailDto);

			mimeMessages.add(mimeMessage);
		}

		// Send mail
		this.mailSender.send(mimeMessages.toArray(new MimeMessage[0]));

		this.fileTemplateEngine.clearTemplateCache();

		log.info("email sendend");

		return emailDtos;

	}

	private MimeMessageHelper prepareMessage(MimeMessage mimeMessage, Email emailDto)
			throws MessagingException, IOException {

		// Prepare message using a Spring helper
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				"UTF-8");
		message.setSubject(emailDto.getSubject());
		message.setFrom(emailDto.getFrom());
		message.setTo(emailDto.getTo());

		if (emailDto.getCc() != null && emailDto.getCc().length != 0) {
			message.setCc(emailDto.getCc());
		}

		if (emailDto.getBcc() != null && emailDto.getBcc().length != 0) {
			message.setBcc(emailDto.getBcc());
		}

		if (emailDto.isHasAttachment()) {
			List<File> attachments = loadResources(
					emailDto.getPathToAttachment() + "/*" + emailDto.getAttachmentName() + "*.*");
			for (File file : attachments) {
				message.addAttachment(file.getName(), file);
			}
		}

		return message;

	}

	private List<File> loadResources(String fileNamePattern) throws IOException {
		PathMatchingResourcePatternResolver fileResolver = new PathMatchingResourcePatternResolver();
		Resource[] resources = null;

		try {
			resources = fileResolver.getResources("file:" + fileNamePattern);
		} catch (Exception e) {

		}
		List<File> attachFiles = new ArrayList<File>();

		for (Resource resource : resources) {
			attachFiles.add(resource.getFile());
		}

		return attachFiles;

	}

	private Context prepareContext(Email emailDto) {
		// Prepare the evaluation context
		Context ctx = new Context();

		if (emailDto.getParameterMap() != null) {
			Set<String> keySet = emailDto.getParameterMap().keySet();
			keySet.forEach(s -> {
				ctx.setVariable(s, emailDto.getParameterMap().get(s));
			});
		}

		if (emailDto.getStaticResourceMap() != null) {
			Set<String> resKeySet = emailDto.getStaticResourceMap().keySet();
			resKeySet.forEach(s -> {
				ctx.setVariable(s, emailDto.getStaticResourceMap().get(s));
			});
		}

		return ctx;
	}

	private MimeMessageHelper prepareStaticResources(MimeMessageHelper message, Email emailDto)
			throws MessagingException {
		Map<String, Object> staticResources = emailDto.getStaticResourceMap();

		if (staticResources != null) {
			for (Map.Entry<String, Object> entry : staticResources.entrySet()) {

				ClassPathResource imageSource = new ClassPathResource("static/" + (String) entry.getValue());
				message.addInline(entry.getKey(), imageSource, "image/png");
				message.addInline((String) entry.getValue(), imageSource, "image/png");

			}
		}

		return message;
	}

	public String getMailUsername() {
		if (mailUsername != null && !StringUtils.isEmpty(mailUsername)) {

			return mailUsername;

		} else {
			mailUsername = mailConfiguracion.getMailUsername();
		}

		return mailUsername;

	}

	public Boolean isStructureEmailOK(String emaildata) {
		
		Boolean isValido = false;
		if(emaildata!=null && !StringUtils.isEmpty(emaildata)) {
			try {
				isValido = validationRegulareEpressions.isEmail(emaildata);
			} catch (RespuestaFallidaServicio e) {
				isValido = false;
			}
		}

		return isValido;
	}

	

}
