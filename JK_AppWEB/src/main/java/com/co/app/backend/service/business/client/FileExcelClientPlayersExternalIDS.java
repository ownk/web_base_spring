package com.co.app.backend.service.business.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.exception.FileStorageException;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.RespuestaFallidaServicio;


@Service
public class FileExcelClientPlayersExternalIDS {
	Logger logger = LoggerFactory.getLogger(FileExcelClientPlayersObjectivesReport.class);

	@Autowired
	AppService appService;

	@Autowired
	I18nService i18nservice;

	private static String[] columns = { 
			"FILE.CLIENT_PLAYERS_EXTERNAL_IDS.COLUMN.1", 
			"FILE.CLIENT_PLAYERS_EXTERNAL_IDS.COLUMN.2",
			"FILE.CLIENT_PLAYERS_EXTERNAL_IDS.COLUMN.3"};
	
	public String getBasePathStorageClient(String idClient) {
		
		String uploadDir = null;

		ParametroConfiguracionGeneral parameter;
		try {
			parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_FILES_UPLOAD_GENERAL_PATH);

			uploadDir = parameter.getCnfg_valor();

			if (uploadDir != null && !uploadDir.isEmpty()) {

				uploadDir = uploadDir + File.separatorChar + "clients" + File.separatorChar + idClient
						+ File.separatorChar + "process" + File.separatorChar + "externalid" + File.separatorChar;

				Path fileStorageLocation = Paths.get(uploadDir).toAbsolutePath().normalize();

				try {
					Files.createDirectories(fileStorageLocation);

					return uploadDir;
				} catch (Exception ex) {
					logger.error(ex.getMessage());

					throw new FileStorageException(
							"Could not create the directory where the uploaded files will be stored.", ex);

				}

			}

			return null;

		} catch (RespuestaFallidaServicio e) {

			logger.error(e.getMensaje());
			return null;
		}
		
	}

	public List<Player> readPlayers(String idClient,File excel){

		List<Player> listaPlayers = new ArrayList<Player>();

		int contadorFilas = 0;

		try {
			
			FileInputStream file = new FileInputStream(excel);


			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0);
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
			Row row;
			// se recorre cada fila hasta el final
			while (rowIterator.hasNext()) {

				Boolean isInfoPlayerOK = true;
				row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();

				DataFormatter formatter = new DataFormatter();

				Cell cell;
				//se recorre cada celda
				if (contadorFilas>1) {
					Player player = new Player();

					while (cellIterator.hasNext()) {



						// se obtiene la celda en específico y se la imprime
						cell = cellIterator.next();
						if (cell.getColumnIndex()==0 ) {
							String numIdent = formatter.formatCellValue(cell);

							if(numIdent!=null) {
								player.setPlayer_numident(numIdent.trim());
							}else {
								player.setPlayer_numident("");
							}


						}else if (cell.getColumnIndex()==1){
							String tpIdent = formatter.formatCellValue(cell);


							if(tpIdent!=null) {
								player.setPlayer_tpident(tpIdent.trim());
							}else {
								player.setPlayer_tpident("");
							}


						}else if (cell.getColumnIndex()==2) {

							String idExtr = formatter.formatCellValue(cell);

							if(idExtr!=null) {
								player.setPlayer_id_ext(idExtr.trim());
							}else {
								player.setPlayer_id_ext("");
							}


						}
					}

					if(isInfoPlayerOK) {
						listaPlayers.add(player);
					}

				}

				contadorFilas++;
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
			return null;
		}
		return listaPlayers;
	}


	//Nueva funcion para generar plantilla
	public File getPlantilla (String idClient, List<Player> players) throws IOException, DecoderException {

		String rutaBase = getBasePathStorageClient(idClient);
		
		//Se genera reporte
		if(rutaBase!=null) {
			
			
			String rutaArchivo = rutaBase  + "TemplateClientPlayersExternalID_" + ""
					+ idClient +".xlsx";
			
			
			int rowNum = 2;
			int columFinal = 0;
			String column = "";
			String hoja = "";
			String encabezado = "";
			byte[] rgbB;
			XSSFColor color;
			Workbook workbook = new XSSFWorkbook();
			hoja = i18nservice.translate("FILE.CLIENT_PLAYERS_EXTERNAL_IDS.LEAF.1");
			encabezado = i18nservice.translate("FILE.CLIENT_PLAYERS_EXTERNAL_IDS.TITTLE");
			Sheet sheet = workbook.createSheet(hoja);
			
			Row headerRow = sheet.createRow(1);
			Row emcabezadoRow = sheet.createRow(0);

			// Estilo para el encabezado
			Font fontEncabezado = workbook.createFont();
			fontEncabezado.setFontHeightInPoints((short) 15);
			fontEncabezado.setBold(true);

			XSSFCellStyle styleEncabezado = (XSSFCellStyle) workbook.createCellStyle();
			styleEncabezado.setFont(fontEncabezado);

			styleEncabezado.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			styleEncabezado.setAlignment(HorizontalAlignment.CENTER);
			styleEncabezado.setVerticalAlignment(VerticalAlignment.CENTER);

			rgbB = Hex.decodeHex("F70F36");
			color = new XSSFColor(rgbB, null);
			styleEncabezado.setFillForegroundColor(color);
			rgbB = null;
			color = null;

			// Estilo para las celdas de encabezado
			Font fontCeldasEc = workbook.createFont();
			fontCeldasEc.setFontHeightInPoints((short) 13);
			fontCeldasEc.setBold(true);
			fontCeldasEc.setItalic(true);
			fontCeldasEc.setColor(IndexedColors.WHITE.getIndex());

			XSSFCellStyle celdasEc = (XSSFCellStyle) workbook.createCellStyle();
			celdasEc.setFont(fontCeldasEc);

			celdasEc.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			celdasEc.setAlignment(HorizontalAlignment.CENTER);
			celdasEc.setVerticalAlignment(VerticalAlignment.CENTER);

			rgbB = Hex.decodeHex("000000");
			color = new XSSFColor(rgbB, null);
			celdasEc.setFillForegroundColor(color);
			rgbB = null;
			color = null;

			//Encabezado
			for (int i = 0; i < columns.length; i++) {
				if (i == 0) {
					columFinal = columns.length - 1;

					CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 0, 0, columFinal);
					sheet.addMergedRegion(cellRangeAddress);
					sheet.setVerticallyCenter(true);

					Cell cell = emcabezadoRow.createCell(i);
					cell.setCellValue(encabezado);
					cell.setCellStyle(styleEncabezado);

				}

				Cell cell = headerRow.createCell(i);
				column = i18nservice.translate(columns[i]);
				cell.setCellValue(column);
				cell.setCellStyle(celdasEc);
				headerRow.setHeightInPoints(30);
			}

			//Listado de players
			if(players!=null && players.size()>0) {
				for (Player player : players) {


					Row row = sheet.createRow(rowNum++);

					Cell cell = row.createCell(0);	

					cell.setCellValue(player.getPlayer_numident());

					cell = row.createCell(1);	

					cell.setCellValue(player.getPlayer_tpident());
					
					cell = row.createCell(2);	

					cell.setCellValue(player.getPlayer_id_ext());

				}
			}
			

			FileOutputStream fileOut = new FileOutputStream(rutaArchivo);
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
			File file = new File(rutaArchivo);
			if (file.exists()) {
				return file;
			} else {
				return null;
			}
			
			
		}
		
		return null;
		
		
		


		

	}
	
	public Resource loadFileAsResource(String idClient, String fileName) {
        try {
        	
        	
        	Path fileStorageLocation = Paths.get(getBasePathStorageClient(idClient))
                    .toAbsolutePath().normalize();
        	
        	
        	Path filePath = fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
        	
        	if(resource.exists()) {
                return resource;
            } else {
               return null;
            }
	    } catch (Exception ex) {
	        logger.error(ex.getMessage());
	    }
        
        return null;
	}
	
	

}
