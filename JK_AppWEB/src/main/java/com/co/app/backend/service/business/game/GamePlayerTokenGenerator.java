package com.co.app.backend.service.business.game;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.EncryptDecryptDESService;
import com.co.app.backend.service.business.commons.StringUtils;
import com.co.app.backend.service.business.integration.IntegrationGameServicio;
import com.co.app.model.dto.game.TokenGamePlayer;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.integration.GameTokenIntegration;

@Component
public class GamePlayerTokenGenerator {
	
	private Logger logger = LoggerFactory.getLogger(GamePlayerTokenGenerator.class);
	
		
	private static final String PARAMETRO_GAME				= "g";
	private static final String PARAMETRO_PLAYER     		= "p";
	private static final String PARAMETRO_FECHA_SOLICITUD  	= "fs";
	private static final String PARAMETRO_MINUTOS_VALIDO   	= "mv";
	private static final String SEPARADOR_VARIABLE			= "&";
	private static final String SEPARADOR_VALOR_VARIABLE	= "=";
	private static final String PATTERN_FECHA				= "yyyy-MM-dd HH:mm:ss";
	
	@Autowired
	EncryptDecryptDESService encripatadorBasicoServicio;
	
	@Autowired
	AppService appService;
	
	@Autowired
	IntegrationGameServicio integrationGameServicio;
	
	

	public String generarOTP() {
		String otp = RandomStringUtils.random(12, true, true);
		return otp;
	}
	
	
	public String generarToken(ControlOperacionServicio controlOperacion, String player_player, String game_game, String idSessionAPP ) {
		
		
		
		String idGtoken = null;;
		try {
			idGtoken = integrationGameServicio.createGToken(controlOperacion, player_player, game_game, idSessionAPP);
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
			return null;
		}
		
		
	    return idGtoken;
	    
		
	}
		
	
	
		
	public TokenGamePlayer obtenerTokenGamePlayer (ControlOperacionServicio controlOperacion, String idGToken){
		
		TokenGamePlayer tokenGamePlayer = null;
		
		Boolean sinErrores = true;


		
		try {
			GameTokenIntegration gameTokenIntegration= integrationGameServicio.getGToken(controlOperacion, idGToken);
			
			
			if(gameTokenIntegration!=null && gameTokenIntegration.getGtoken_gtoken()!=null) {
				
				String player_player = gameTokenIntegration.getGtoken_player();
				String game_game = gameTokenIntegration.getGtoken_game();
				Date fechaGeneracion = gameTokenIntegration.getGtoken_date_star();
				
				
				tokenGamePlayer = new TokenGamePlayer(player_player, game_game, fechaGeneracion);
							
				
				
			}else{
				sinErrores = false;
			}
			
			
			
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
			
			sinErrores = false;
		}
		
		
		
		if(sinErrores){
			return tokenGamePlayer;
		}else{
			return null;
		}
		
		
	}
	
	
	
	
	
}
