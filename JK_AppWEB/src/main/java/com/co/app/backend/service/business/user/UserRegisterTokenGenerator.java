package com.co.app.backend.service.business.user;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA2Service;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.StringUtils;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.user.TokenUserRegister;
import com.co.app.model.dto.user.User;

@Component
public class UserRegisterTokenGenerator {
	
	private Logger logger = LoggerFactory.getLogger(UserRegisterTokenGenerator.class);
	
	private static final String PARAMETRO_OTP 				= "t";
	private static final String PARAMETRO_CODIGO_ACCESO		= "ca";
	private static final String PARAMETRO_USUARIO     		= "u";
	private static final String PARAMETRO_FECHA_SOLICITUD  	= "fs";
	private static final String PARAMETRO_MINUTOS_VALIDO   	= "mv";
	private static final String SEPARADOR_VARIABLE			= "&";
	private static final String SEPARADOR_VALOR_VARIABLE	= "=";
	private static final String PATTERN_FECHA				= "yyyy-MM-dd HH:mm:ss";
	
	
	
	
	@Autowired
	AppService appService;
	
	 
	
		
	public EncryptEncodeUserRegisterToken getEncrypEncodeToken(TokenUserRegister tokenRegistroUsuario)throws Exception {
			    
	    
	    String tokenSinCifrar = "";
		Boolean sinErrores = true;
		
		Integer minutosValidezToken = tokenRegistroUsuario.getMinutosValidez();
		
		
		//Usuario
		if(tokenRegistroUsuario.getUser_user()!=null){
			tokenSinCifrar = tokenSinCifrar+PARAMETRO_USUARIO+SEPARADOR_VALOR_VARIABLE+tokenRegistroUsuario.getUser_user()+SEPARADOR_VARIABLE;
		}else{
			logger.error("ERROR_USER_NL");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
		
		
		//Codigo Acceso
		if(tokenRegistroUsuario.getCodigoAcceso()!=null){
			tokenSinCifrar = tokenSinCifrar+PARAMETRO_CODIGO_ACCESO+SEPARADOR_VALOR_VARIABLE+tokenRegistroUsuario.getCodigoAcceso()+SEPARADOR_VARIABLE;
		}else{
			logger.error("ERROR_ACODE_NL");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
				
				
		
		//OTP
		if(tokenRegistroUsuario.getOtp()!=null){
			
			
			tokenSinCifrar = tokenSinCifrar+PARAMETRO_OTP+SEPARADOR_VALOR_VARIABLE+tokenRegistroUsuario.getOtp()+SEPARADOR_VARIABLE;
				
		}else{
			logger.error("ERROR_OTP_NL");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
		
		//Fecha solicitud
		SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_FECHA);
		String fechaSolicitudString = dateFormat.format(tokenRegistroUsuario.getFechaGeneracion());
		
		if(fechaSolicitudString!=null){
			tokenSinCifrar = tokenSinCifrar+PARAMETRO_FECHA_SOLICITUD+SEPARADOR_VALOR_VARIABLE+fechaSolicitudString+SEPARADOR_VARIABLE;
		}else{
			logger.error("ERROR_FSOL_NL");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
		
		
		//Minutos validez token
		if(minutosValidezToken.intValue()>0){
			
			tokenSinCifrar = tokenSinCifrar+PARAMETRO_MINUTOS_VALIDO+SEPARADOR_VALOR_VARIABLE+minutosValidezToken+SEPARADOR_VARIABLE;
		}else{
			logger.error("ERROR_MVALIDEZ_NV");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
		
		
		if(sinErrores ){
			
			EncryptDecryptSHA2Service encryptDecryptSHA2Service = EncryptDecryptSHA2Service.getInstance();
			
			String encryptSalt = encryptDecryptSHA2Service.generateRandomSalt();
			String encryptIV = 	 encryptDecryptSHA2Service.generateRandomIV();
			String tokenCifrado = encryptDecryptSHA2Service.encrypt(tokenSinCifrar, encryptSalt, encryptIV, appService.getAesEncryptKey() );
			
			String encryptTokenEncode = StringUtils.encodeURIComponent(tokenCifrado);
			String encryptSaltEncode = StringUtils.encodeURIComponent(encryptSalt);
			String encryptIVtEncode = StringUtils.encodeURIComponent(encryptIV);
					
			if(encryptTokenEncode!=null && encryptSaltEncode!=null && encryptSaltEncode!=null) {
				EncryptEncodeUserRegisterToken encryptEncodeUserRegisterToken = new EncryptEncodeUserRegisterToken(encryptTokenEncode, encryptSaltEncode, encryptIVtEncode);
				return encryptEncodeUserRegisterToken;
			}else {
				return null;
			}
			
		}else{
			return null;
		}
		
		
	}
	
	
	public String generarOTP() {
		String otp = RandomStringUtils.random(12, true, true);
		return otp;
	}
	
	public TokenUserRegister generarToken(User usuario, String codigoAcceso, String otp ) {
		
	 	Integer minutosMaximosValidez = 60;
	 	
		Date date = null;
		try {
			date = appService.getSysdate();
		} catch (RespuestaFallidaServicio e) {
			logger.error(e.getMensaje());
		}
	 	
	 	if(date==null) {
	 		date = new Date();
	 	}
	 	
	    
	    TokenUserRegister tokenRegistroUsuario = new TokenUserRegister(usuario.getUser_user(), codigoAcceso,  otp, date, minutosMaximosValidez);
	    
	    
	    return tokenRegistroUsuario;
	    
		
	}
	
		
	public TokenUserRegister decryptToken (String tokenCifrado, String encryptSalt, String encryptIV){
		/*
		 * Se obtiene informacion de token cifrado
		 * ==============================================================
		 * Se crea mapa con valores obtenidos del token con la siguiente
		 * estructura
		 * 
		 * t=valor&u=valor&fs=valor&mv=valor
		 * 
		 */
		
		TokenUserRegister tokenRegistroJugador = new TokenUserRegister();
		
		Boolean sinErrores = true;
		String tokenDescrifrado = null;
		
		
		if(tokenCifrado!=null){
			
			EncryptDecryptSHA2Service encryptDecryptSHA2Service = EncryptDecryptSHA2Service.getInstance();
			
			tokenDescrifrado = encryptDecryptSHA2Service.decrypt(tokenCifrado, encryptSalt, encryptIV, appService.getAesEncryptKey());
		}
		
		
		if(tokenDescrifrado!=null){
			StringTokenizer tokens = new StringTokenizer(tokenDescrifrado,SEPARADOR_VARIABLE);
			Map<String, String> parametros = new HashMap<String, String>();
			
			while(tokens.hasMoreElements()){
				String actualElement = tokens.nextToken();
				StringTokenizer stringTokenizer = new StringTokenizer(actualElement, SEPARADOR_VALOR_VARIABLE);
				String key = stringTokenizer.nextToken();
				String value = stringTokenizer.nextToken();
				parametros.put(key, value);
			}
			
			/*
			 * Se valida si se obtienen los valores esperados
			 * ==============================================================
			 *  
			 * TIPO_SOLICITUD;CELULAR;FECHA_SOLICITUD;MINUTOS_VALIDO;
			 * 
			 */
			
			String codigoAcceso = parametros.get(PARAMETRO_CODIGO_ACCESO);
			
			String otp = parametros.get(PARAMETRO_OTP);
			
			String user_user = parametros.get(PARAMETRO_USUARIO);
			
			String fechaSolicitud = parametros.get(PARAMETRO_FECHA_SOLICITUD);
			
			String minutosValidades = parametros.get(PARAMETRO_MINUTOS_VALIDO);
			
			

			//Se valida el tipo de solicitud
			if(codigoAcceso!=null){
				
				tokenRegistroJugador.setCodigoAcceso(codigoAcceso);
				
			}else{
				sinErrores = false;
				
			}
			
			
			
			//Se valida el tipo de solicitud
			if(otp!=null){
				
				tokenRegistroJugador.setOtp(otp);
				
			}else{
				sinErrores = false;
				
			}
			
			//Se valida celular
			if(user_user!=null){
				
				tokenRegistroJugador.setUser_user(user_user);
				
			}else{

				sinErrores = false;
			}
			
			//Se valida fecha solicitud
			if(fechaSolicitud!=null){
				
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PATTERN_FECHA);
				try {
					Date dateFechaSolicitud = simpleDateFormat.parse(fechaSolicitud);
					
					tokenRegistroJugador.setFechaGeneracion(dateFechaSolicitud);
					
				} catch (Exception e) {
					sinErrores = false;
				}
			}else{
				sinErrores = false;
			}
			
			//Se valida minutos;
			if(minutosValidades!=null){
				
				Integer minutos = Integer.parseInt(minutosValidades);
				
				tokenRegistroJugador.setMinutosValidez(minutos);
				
				
			}else{
				sinErrores = false;
			}
			
			
			
		}else{
			sinErrores = false;
		}
		
		
		
		if(sinErrores){
			return tokenRegistroJugador;
		}else{
			return null;
		}
		
		
	}
	
	
	
	
}
