package com.co.app.backend.service.business.commons;

import java.security.MessageDigest;

public class MD5 {
	private static String CRIPTOALPHA = "ABCDEFGHIJKLMNOPQRSTUBWXYZabcdefghijklmnopqrstubwxyz0123456789.:";
	
	public static String generarMD5(String source) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] bytes = md.digest(source.getBytes());
			return getString(bytes);
		} catch (Exception e) {
			return null;
		}
	}
	
	private static String getString(byte[] bytes) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];

			int a = (int) b + 128;

			int p1 = a / CRIPTOALPHA.length();
			int p2 = a % CRIPTOALPHA.length();

			sb.append(CRIPTOALPHA.charAt(p1));
			sb.append(CRIPTOALPHA.charAt(p2));
		}
		return sb.toString();
	}
}
