package com.co.app.backend.service.business.commons;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA1Service;

@Component
public class RequestParameterService {
	
		
	Logger logger = LoggerFactory.getLogger(RequestParameterService.class);
	
	private Map<String, Object> parametrosRequest;
	
	public Object getParameterToObject(String parameterName,  Class<?> classPadre, Class<?> classContenida, Map<String, Object> mapRequest) {

		try {
			if(classContenida!= null){
				return JavaToXML.createObjectRequest(parameterName, classPadre.getName(), classContenida.getName(), mapRequest);
			
			}else{
				
				return JavaToXML.createObjectRequest(parameterName, classPadre.getName(), classPadre.getName(), mapRequest);
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		return null;
	}
	
	public Map<String, Object> getParameters(HttpServletRequest request, Boolean reiniciarMapa){
		
		if(parametrosRequest== null || reiniciarMapa){
			
			Map<String, Object> mapMultipart = new HashMap<String, Object>();
			ArrayList<Object> files = new ArrayList<Object>();

			try {
				if (ServletFileUpload.isMultipartContent(request)) {
					
					ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
					List<?> fileItemsList = servletFileUpload.parseRequest((RequestContext) request);

					@SuppressWarnings("rawtypes")
					Iterator iter = fileItemsList.iterator();
					while (iter.hasNext()) {
						FileItem item = (FileItem) iter.next();

						if (item.isFormField()) {
							mapMultipart.put(item.getFieldName(), item.getString(ConstantesGeneralesAPP.ENCONDING));
						} else {
							if (item.getFieldName().contains("file")) {
								files.add(item);
							}
						}
					}
					if(files.size()>0){
						mapMultipart.put("file[]", files);
					}
					parametrosRequest = mapMultipart;
				}else{
					Map<String, Object> mapSimple = new HashMap<String, Object>();
					for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
						String param = e.nextElement().toString();
						
						byte[] utf8 = new String(request.getParameter(param)).getBytes(Charset.forName(ConstantesGeneralesAPP.ENCONDING));
						String stringParameter = new String(utf8);
						
						mapSimple.put(param, stringParameter);
					}
					parametrosRequest = mapSimple;
				}	
			} catch (Exception e) {
				logger.error("Error generando el mapa de parametros",e);
			}
		}
		return parametrosRequest;
	}
	
	
	public ArrayList<MultipartFile> getMultipartFiles(HttpServletRequest request){
		
		
		ArrayList<MultipartFile> files = new ArrayList<MultipartFile>();

		try {
			if (ServletFileUpload.isMultipartContent(request)) {
				
				
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		        MultiValueMap<String, MultipartFile> a = multipartRequest.getMultiFileMap(); 
		        
		        Set<Entry<String, List<MultipartFile>>> b = a.entrySet();
		        Iterator<Entry<String, List<MultipartFile>>> i = b.iterator();
		        while(i.hasNext()) { 
		        	
		        	
		            Map.Entry me = (Map.Entry)i.next(); 
		            String key = (String)me.getKey();
		            
		            logger.info("key"+key);
		            
		            if(key!=null && key.equals("file[]")) {
		            	
		            	LinkedList<MultipartFile> linkedList = (LinkedList<MultipartFile>) me.getValue();
		            	
		            	
		            	if(linkedList!=null) {
			            	for(MultipartFile multipartFile: linkedList)
			                {
			            	    logger.info("Original fileName - " + multipartFile.getOriginalFilename());
					            logger.info("fileName - " + multipartFile.getName());
					            files.add(multipartFile);
			                }
		            	}

		            	
		            }
		            
		            
		          
		        } 
		        
		        
			}else{
				return files;
				
			}	
		} catch (Exception e) {
			logger.error("ERR_MULTIPART_FILES",e);
		}
	
		return files;
	}
	
	public Map<String, Object> getMultipartParams(HttpServletRequest request){
		
		
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			if (ServletFileUpload.isMultipartContent(request)) {
				
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		        
				Map<String, String[]> parameterMap = multipartRequest.getParameterMap();
				Set<Entry<String, String[]>> a = parameterMap.entrySet();
				Iterator<Entry<String, String[]>> i = a.iterator();
				
				 while(i.hasNext()) { 
			        	
			        	
			            Map.Entry me = (Map.Entry)i.next(); 
			            String key = (String)me.getKey();
			            String[] val = (String[])me.getValue();
			            
			            
			            logger.info("key"+key);
			            logger.info("val"+val);
			            
			            if(val!=null ) {
			            	parameters.put(key, val[0]);
			            }
			            
			            
			            
			          
			     } 
				
		        
			}else{
				return parameters;
				
			}	
		} catch (Exception e) {
			logger.error("ERR_MULTIPART_PARAM",e);
		}
	
		return parameters;
	}
	
	
	public Object getRequestParameterToObject(HttpServletRequest request, String parameterName,  Class<?> classPadre, Class<?> classContenida) {

		Map<String, Object> mapRequest = getParameters(request, true);
		
		try {
			if(classContenida!= null){
				return JavaToXML.createObjectRequest(parameterName, classPadre.getName(), classContenida.getName(), mapRequest);
			
			}else{
				
				return JavaToXML.createObjectRequest(parameterName, classPadre.getName(), classPadre.getName(), mapRequest);
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		return null;
	}
	
	
	public static  String decryptText(String parameterEncrypt){
		
		String parameterFinalDecrypt = null;

		if (parameterEncrypt != null) {

			EncryptDecryptSHA1Service aesService = EncryptDecryptSHA1Service.getInstance();

			String decryptedParameter = new String(java.util.Base64.getDecoder().decode(parameterEncrypt));

			if (decryptedParameter != null && decryptedParameter.split("::").length == 3) {
				parameterFinalDecrypt = aesService.decrypt(decryptedParameter.split("::")[1],
						decryptedParameter.split("::")[0], aesService.getPrivateKey(),
						decryptedParameter.split("::")[2]);

			}

		}

		return parameterFinalDecrypt;

	}
	
	

}
