package com.co.app.backend.service.business.client;

import org.springframework.beans.factory.annotation.Value;

public class ClientImageStorageProperties {
	
	@Value("${client.image.upload-dir}")
    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

  
}
