package com.co.app.backend.service.business.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.exception.FileStorageException;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

@Service
public class FileExcelClientPlayersObjectivesReport {

	Logger logger = LoggerFactory.getLogger(FileExcelClientPlayersObjectivesReport.class);

	@Autowired
	AppService appService;

	@Autowired
	I18nService i18nservice;
	
	@Autowired
	ClientService clientService;
	
	

	private static String[] columns = { 
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.1", 
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.2", 
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.3", 
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.4",
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.5",
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.6",
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.7",
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.8",
			"FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.COLUMN.9"};

	public String getBasePathStorageClient(String idClient) {
		
		String uploadDir = null;
		
		ParametroConfiguracionGeneral parameter;
		try {
			parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_FILES_UPLOAD_GENERAL_PATH);

			 uploadDir = parameter.getCnfg_valor();

			 if(uploadDir!=null && !uploadDir.isEmpty()) {
				 
				 
				uploadDir = uploadDir+ File.separatorChar+"clients"+File.separatorChar+idClient+File.separatorChar+"reports"+File.separatorChar+"objectives"+File.separatorChar;
				 
				Path fileStorageLocation = Paths.get(uploadDir).toAbsolutePath().normalize();

	            try {
	                Files.createDirectories(fileStorageLocation);
	                
	                 
	                return uploadDir;
	            } catch (Exception ex) {
	            	logger.error(ex.getMessage());
	            	
	                throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
	                
	               
	            }
				 
				 
			 }	
			 
			 return null;
			

		} catch (RespuestaFallidaServicio e) {

			logger.error(e.getMensaje());
			return null;
		}
		
		
	}
	
	
	public File crearReportePlayers(List<Player> playersClient, Client client, String  idPlayerFilter)
			throws IOException, InvalidFormatException, DecoderException {

		String idClient = client.getClient_client();
		String idPlayer = "All";
		
		String rutaBase = getBasePathStorageClient(idClient);
		List<Player>players=new ArrayList<Player>();
		
		
		//Si se especifica player se genera reporte para ese player
		if(idPlayerFilter!=null) {
			
			idPlayer = idPlayerFilter;
			
			for (Player player : playersClient) {
				if(player.getPlayer_player().equals(idPlayerFilter)) {
					players.add(player);
				}
			}
			
			
		}else {
			players=playersClient;
		}
		
		
		//Se genera reporte
		if(rutaBase!=null) {
			String rutaArchivo = rutaBase  + "ClientReportPlayerObjectives_" + ""
					+ idClient +"_"+idPlayer+".xlsx";

			int rowNum = 2;
			int rowFinal = 0;
			int rowInicio = 0;
			int count = 0;
			int columFinal = 0;
			String column = "";
			String hoja = "";
			String encabezado = "";
			byte[] rgbB;
			XSSFColor color;

			if (players != null) {

				// Creacion del libro
				Workbook workbook = new XSSFWorkbook();
				CreationHelper createHelper = workbook.getCreationHelper();
				hoja = i18nservice.translate("FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.LEAF.1");
				encabezado = i18nservice.translate("FILE.CLIENT_PLAYERS_OBJECTIVES_REPORT.TITTLE");
				Sheet sheet = workbook.createSheet(hoja);
				Row headerRow = sheet.createRow(1);
				Row emcabezadoRow = sheet.createRow(0);

				// Estilo para el encabezado
				Font fontEncabezado = workbook.createFont();
				fontEncabezado.setFontHeightInPoints((short) 15);
				fontEncabezado.setBold(true);

				XSSFCellStyle styleEncabezado = (XSSFCellStyle) workbook.createCellStyle();
				styleEncabezado.setFont(fontEncabezado);

				styleEncabezado.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				styleEncabezado.setAlignment(HorizontalAlignment.CENTER);
				styleEncabezado.setVerticalAlignment(VerticalAlignment.CENTER);

				rgbB = Hex.decodeHex("F70F36");
				color = new XSSFColor(rgbB, null);
				styleEncabezado.setFillForegroundColor(color);
				rgbB = null;
				color = null;

				// Estilo para las celdas de encabezado
				Font fontCeldasEc = workbook.createFont();
				fontCeldasEc.setFontHeightInPoints((short) 13);
				fontCeldasEc.setBold(true);
				fontCeldasEc.setItalic(true);
				fontCeldasEc.setColor(IndexedColors.WHITE.getIndex());

				XSSFCellStyle celdasEc = (XSSFCellStyle) workbook.createCellStyle();
				celdasEc.setFont(fontCeldasEc);

				celdasEc.setFillPattern(FillPatternType.SOLID_FOREGROUND);

				celdasEc.setAlignment(HorizontalAlignment.CENTER);
				celdasEc.setVerticalAlignment(VerticalAlignment.CENTER);

				rgbB = Hex.decodeHex("000000");
				color = new XSSFColor(rgbB, null);
				celdasEc.setFillForegroundColor(color);
				rgbB = null;
				color = null;
				// Estilo para la celda que tiene el valor del clasificador del Objetivo
				XSSFCellStyle clasificador = (XSSFCellStyle) workbook.createCellStyle();

				clasificador.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				clasificador.setAlignment(HorizontalAlignment.CENTER);

				clasificador.setBorderLeft(BorderStyle.MEDIUM);
				clasificador.setBorderRight(BorderStyle.MEDIUM);
				clasificador.setBorderBottom(BorderStyle.MEDIUM);
				clasificador.setVerticalAlignment(VerticalAlignment.CENTER);

				// Estilo para las celdas del nombre del player
				CellStyle celdaspl = workbook.createCellStyle();

				celdaspl.setBorderLeft(BorderStyle.MEDIUM);
				celdaspl.setBorderRight(BorderStyle.MEDIUM);
				celdaspl.setBorderBottom(BorderStyle.MEDIUM);
				celdaspl.setAlignment(HorizontalAlignment.CENTER);
				celdaspl.setVerticalAlignment(VerticalAlignment.CENTER);

				// Estilo para las celdas generales
				CellStyle celdas = workbook.createCellStyle();

				celdas.setBorderLeft(BorderStyle.MEDIUM);
				celdas.setBorderRight(BorderStyle.MEDIUM);
				celdas.setBorderBottom(BorderStyle.MEDIUM);
				celdas.setVerticalAlignment(VerticalAlignment.CENTER);

				// Estilo para las celdas de fecha
				CellStyle celdasfecha = workbook.createCellStyle();

				celdasfecha.setBorderLeft(BorderStyle.MEDIUM);
				celdasfecha.setBorderRight(BorderStyle.MEDIUM);
				celdasfecha.setBorderBottom(BorderStyle.MEDIUM);
				celdasfecha.setVerticalAlignment(VerticalAlignment.CENTER);

				celdasfecha.setDataFormat((short) 14);

				// Estilo para el cierre de la linea final
				CellStyle linefinal = workbook.createCellStyle();
				linefinal.setBorderTop(BorderStyle.MEDIUM);

				for (int i = 0; i < columns.length; i++) {
					if (i == 0) {
						columFinal = columns.length - 1;

						CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 0, 0, columFinal);
						sheet.addMergedRegion(cellRangeAddress);
						sheet.setVerticallyCenter(true);

						Cell cell = emcabezadoRow.createCell(i);
						cell.setCellValue(encabezado);
						cell.setCellStyle(styleEncabezado);

					}

					Cell cell = headerRow.createCell(i);
					column = i18nservice.translate(columns[i]);
					cell.setCellValue(column);
					cell.setCellStyle(celdasEc);
					headerRow.setHeightInPoints(30);
				}

				List<LearningObjective> lrnobjs = new ArrayList<LearningObjective>();
				List<Match> matches = new ArrayList<Match>();

				for (Player player : players) {

					matches = player.getListMatch();
					

					Row row = sheet.createRow(rowNum++);
					
					Cell cellpl = row.createCell(0);

					

					Cell cell = row.createCell(0);
					
					for (Match match : matches) {
						
						
						
						Game game = match.getGame();
						
						lrnobjs = game.getLearningObjectives();
						
						if (lrnobjs.size() > 1 && rowInicio == 0  ) {
							if (count >0) {
								rowInicio =rowNum;
								}else {
								rowInicio = rowNum-1;
							}
							rowFinal = lrnobjs.size() + rowInicio - 1;

							CellRangeAddress cellRangeAddressNick = new CellRangeAddress((rowInicio), (rowFinal), 0, 0);
							sheet.setColumnWidth(3, 1000);
							sheet.addMergedRegion(cellRangeAddressNick);
							
						
							CellRangeAddress cellRangeAddressName = new CellRangeAddress((rowInicio), (rowFinal), 1, 1);
							sheet.setColumnWidth(3, 1000);
							sheet.addMergedRegion(cellRangeAddressName);
							
							CellRangeAddress cellRangeAddressNumIdent = new CellRangeAddress((rowInicio), (rowFinal), 2, 2);
							sheet.setColumnWidth(3, 1000);
							sheet.addMergedRegion(cellRangeAddressNumIdent);
							
							CellRangeAddress cellRangeAddressTpIndent = new CellRangeAddress((rowInicio), (rowFinal), 3, 3);
							sheet.setColumnWidth(3, 1000);
							sheet.addMergedRegion(cellRangeAddressTpIndent);
							
							CellRangeAddress cellRangeAddressIdExtr = new CellRangeAddress((rowInicio), (rowFinal), 4, 4);
							sheet.setColumnWidth(3, 1000);
							sheet.addMergedRegion(cellRangeAddressIdExtr);
							
							
							rowInicio = 0;
							rowFinal = 0;
						}
						
						if(count>0) {
							row = sheet.createRow(rowNum++);
						}

						count = 0;

						for (LearningObjective lrnobj : lrnobjs) {
							
							
							cellpl = row.createCell(0);

							cellpl.setCellValue(player.getPlayer_name());
							cellpl.setCellStyle(celdaspl);

							cellpl = row.createCell(1);
							cellpl.setCellValue(player.getUsuario().getUser_name());
							cellpl.setCellStyle(celdaspl);	
							
							cellpl = row.createCell(2);
							cellpl.setCellValue(player.getPlayer_numident());
							cellpl.setCellStyle(celdaspl);	
							
							cellpl = row.createCell(3);
							cellpl.setCellValue(player.getPlayer_tpident());
							cellpl.setCellStyle(celdaspl);	
							
							cellpl = row.createCell(4);
							cellpl.setCellValue(player.getPlayer_id_ext());
							cellpl.setCellStyle(celdaspl);	
							
							count = count + 1;
							cell = row.createCell(5);
							
							if (lrnobj.getLrnobj_clasif().equals("BAJO")) {
								rgbB = Hex.decodeHex(lrnobj.getLrnobj_color());
								color = new XSSFColor(rgbB, null);
								clasificador.setFillForegroundColor(color);
								rgbB = null;
								color = null;
							} else if (lrnobj.getLrnobj_clasif().equals("MEDIO")) {
								rgbB = Hex.decodeHex(lrnobj.getLrnobj_color());
								color = new XSSFColor(rgbB, null);
								clasificador.setFillForegroundColor(color);
								rgbB = null;
								color = null;
							} else if (lrnobj.getLrnobj_clasif().equals("ALTO")) {
								rgbB = Hex.decodeHex(lrnobj.getLrnobj_color());
								color = new XSSFColor(rgbB, null);
								clasificador.setFillForegroundColor(color);
								rgbB = null;
								color = null;
							}

							cell.setCellStyle(clasificador);
							cell.setCellValue(lrnobj.getLrnobj_clasif());

							cell = row.createCell(6);
							cell.setCellValue(lrnobj.getLrnobj_descri());
							cell.setCellStyle(celdas);
							row.setHeightInPoints(50);

							cell = row.createCell(7);
							cell.setCellValue(match.getMatch_end());
							cell.setCellStyle(celdasfecha);

							cell = row.createCell(8);
							cell.setCellValue(game.getGame_name());
							cell.setCellStyle(celdas);

							
							if (count < lrnobjs.size()) {
								row = sheet.createRow(rowNum++);
							}
							
							
						}
						
						
					}
					
					count = 0;

					if (matches.size() == 0) {
						cellpl.setCellValue(player.getPlayer_name());
						cellpl.setCellStyle(celdaspl);

						cellpl = row.createCell(1);
						cellpl.setCellValue(player.getUsuario().getUser_name());
						cellpl.setCellStyle(celdaspl);
						
						cellpl = row.createCell(2);
						cellpl.setCellValue(player.getPlayer_numident());
						cellpl.setCellStyle(celdaspl);	
						
						cellpl = row.createCell(3);
						cellpl.setCellValue(player.getPlayer_tpident());
						cellpl.setCellStyle(celdaspl);	
						
						cellpl = row.createCell(4);
						cellpl.setCellValue(player.getPlayer_id_ext());
						cellpl.setCellStyle(celdaspl);	
						
						cell = row.createCell(5);
						cell.setCellStyle(celdas);
						cell = row.createCell(6);
						cell.setCellStyle(celdas);
						cell = row.createCell(7);
						cell.setCellStyle(celdas);
						cell = row.createCell(8);
						cell.setCellStyle(celdas);
					}

				}

				

				for (int i = 0; i < columns.length; i++) {
					sheet.autoSizeColumn(i);
				}

				FileOutputStream fileOut = new FileOutputStream(rutaArchivo);
				workbook.write(fileOut);
				fileOut.close();

				workbook.close();
				
				
				File file = new File(rutaArchivo);
	        	if(file.exists()) {
	        		return file;
	        	}else {
	        		return null;
	        	}
	        	
	        	
	        	
	        	
				

			}
			
			
		}
		
		return null;
		
		

	}
	
	public Resource loadClientFileAsResource(String idClient, String fileName) {
        try {
        	
        	
        	Path fileStorageLocation = Paths.get(getBasePathStorageClient(idClient))
                    .toAbsolutePath().normalize();
        	
        	
        	Path filePath = fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
        	
        	if(resource.exists()) {
                return resource;
            } else {
               return null;
            }
	    } catch (Exception ex) {
	        logger.error(ex.getMessage());
	    }
        
        return null;
	}


}
