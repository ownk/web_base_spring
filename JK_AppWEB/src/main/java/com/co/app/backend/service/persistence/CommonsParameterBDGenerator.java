package com.co.app.backend.service.persistence;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;

public class CommonsParameterBDGenerator {
			
	public Map<Object, Object> getParametrosComunes(ControlOperacionBD controlOperacionBD, Class<?> clase, String metodo) throws Exception{
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		String nombreclase =  null;
		String prefijoNombreTrx = null;
		Date dateServer = new Date();
		String transaccion = null;
		
		
		
		if(controlOperacionBD.getLgtr_observacion()==null) {
			controlOperacionBD.setLgtr_observacion(getNombreServicio(clase,metodo));
		}
		
		
		nombreclase = clase.getName();
		prefijoNombreTrx = nombreclase.substring(nombreclase.lastIndexOf("."), nombreclase.length());
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String fecha = format.format(dateServer);
		format = new SimpleDateFormat("HHmmssSSS");
		String hora = format.format(dateServer);
		transaccion = prefijoNombreTrx+"_"+fecha+"_"+hora;
		
		controlOperacionBD.setLgtr_trans(transaccion);
		controlOperacionBD.setLgtr_fech_oper(date.toString());
		
		
		format = new SimpleDateFormat("HH:mm:ss");
		hora = format.format(dateServer);
		controlOperacionBD.setLgtr_hora_oper(hora);
		if(controlOperacionBD.getLgtr_terminal()==null || controlOperacionBD.getLgtr_terminal()=="") {
			controlOperacionBD.setLgtr_terminal("0.0.0.0");
		}
		
		
		parametrosInOout.put(ConstantesBD.PARAMETER_CTRL_OPERACION, controlOperacionBD);
		parametrosInOout.put(ConstantesBD.PARAMETER_COD_RPTA, null);
		parametrosInOout.put(ConstantesBD.PARAMETER_ERRORES, null);
		parametrosInOout.put(ConstantesBD.PARAMETER_MENSAJES, null);		
		return parametrosInOout;
	};

	public String getNombreServicio(Class<?> clase, String method ) {

		String nombreServicio = "";
		try {
			String nombreClase = clase.getName();
			nombreServicio = nombreClase+"."+method;
		} catch (Exception e) {
			nombreServicio = new Object(){}.getClass().getEnclosingMethod().getName();
		}
		return nombreServicio;

	}

}
