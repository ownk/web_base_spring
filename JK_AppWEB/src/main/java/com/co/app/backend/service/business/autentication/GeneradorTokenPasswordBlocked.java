package com.co.app.backend.service.business.autentication;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.user.User;

@Component
public class GeneradorTokenPasswordBlocked {
	
	Logger logger = LoggerFactory.getLogger(GeneradorTokenPasswordBlocked.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	MailServicio mailServicio;
	
	@Autowired
	I18nService i18nservice;
	
	@Autowired
	IPNavegadorService ipNavegadorServicio;
	
	@Autowired
	AppService appServicio;
	
	
	public String createSendToken(Locale locale, String userName) {
		

		Email emailDto = null;
		
		String token = RandomStringUtils.random(8, true, true);
		
		final String ip = ipNavegadorServicio.getIP();
		
		ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio("APP", ip, "NA");
		
		try {
			User user = userService.getUsuarioPorNick(controlOperacionServicio, userName);
			String emailSubject = i18nservice.translate("login_password.blocked.email.subject");
			String emailMessage = i18nservice.translate("login_password.blocked.email.message");
			
			
			

			
			emailDto = new Email(mailServicio.getMailUsername(), user.getUser_email(),emailSubject,emailMessage);
						

			Map<String, Object> parameterMap = new HashMap<String, Object>();
			
			ParametroConfiguracionGeneral parameterAppLogoUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_LOGO_INTERNET_URL);
			
			String urlAppLogo = "";
			if(parameterAppLogoUrl!=null) {
				urlAppLogo = parameterAppLogoUrl.getCnfg_valor();
			}

			ParametroConfiguracionGeneral parameterAppPageUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_PAGE_INTERNET_URL);
			
			String urlAppPage = "";
			if(parameterAppPageUrl!=null) {
				urlAppPage = parameterAppPageUrl.getCnfg_valor();
			}
			
			
			String emailHeaderTitle = i18nservice.translate("login_password.blocked.email.header.title");
			String emailHeaderSubtitle = i18nservice.translate("login_password.blocked.email.header.subtitle");
			String emailContentGreeting = i18nservice.translate("login_password.blocked.email.content.greeting");
			String emailContentMessage = i18nservice.translate("login_password.blocked.email.content.message");
			String emailContentFooter = i18nservice.translate("login_password.blocked.email.content.footer");
			
			parameterMap.put("emailHeaderTitle", emailHeaderTitle);
			parameterMap.put("emailHeaderSubtitle", emailHeaderSubtitle);
			parameterMap.put("emailContentGreeting", emailContentGreeting);
			parameterMap.put("emailContentMessage", emailContentMessage);
			parameterMap.put("emailContentFooter", emailContentFooter);
			
			parameterMap.put("urlAppPage", urlAppPage);
			parameterMap.put("urlAppLogo", urlAppLogo);
			
			
			parameterMap.put("tokenBlockedPass", token );
			parameterMap.put("user", user);
			emailDto.setParameterMap(parameterMap);

			//Envio de correo de confirmacion
			if(emailDto!=null) {

				
				try {

					mailServicio.sendHtmlTemplateEmail(locale, emailDto, MailServicio.HTML_TEMPLATE_EMAIL_LOGIN_PASSWORD_BLOCKED);

				} catch (Exception e) {
					token = null;	
					logger.error("ERROR_SEND_MAIL: "+e.getMessage());

				}

			}
						
			
		} catch (RespuestaFallidaServicio e1) {
			// TODO Auto-generated catch block
			logger.error(e1.getMensaje());
			token = null;
		}

		
		return token;
	}

}
