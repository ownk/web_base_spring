package com.co.app.backend.service.business.client;


public class ResponseTaskCreateSendAccessCode {
	
	public static final String COD_RSPTA_OK = "OK";
	public static final String COD_RSPTA_FALLIDA ="FALLIDA";
	public static final String COD_RSPTA_ERROR_NC = "ERROR";
	
	private String codigoRespuesta;
	private String descripcion;
	private Object objectTask;
	
	
	
	public ResponseTaskCreateSendAccessCode(){
		
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Object getObjectTask() {
		return objectTask;
	}

	public void setObjectTask(Object objectTask) {
		this.objectTask = objectTask;
	}

	
	
	
	
	
	
	
}
