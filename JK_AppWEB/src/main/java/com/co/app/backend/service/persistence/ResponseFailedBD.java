package com.co.app.backend.service.persistence;

import java.util.List;

import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;

public class ResponseFailedBD extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<ErrorBD> listaErrores;
	List<MensajeBD> listaMensajes;
	String codigoRespuesta;
	
	public ResponseFailedBD(String codigoRespuesta, String mensaje,  List<ErrorBD> listaErrores, List<MensajeBD> listaMensajes) {
		super(mensaje);
		
		this.codigoRespuesta = codigoRespuesta;
		this.listaErrores = listaErrores;
		this.listaMensajes = listaMensajes;
	}

	public List<ErrorBD> getListaErrores() {
		return listaErrores;
	}

	public void setListaErrores(List<ErrorBD> listaErrores) {
		this.listaErrores = listaErrores;
	}

	public List<MensajeBD> getListaMensajes() {
		return listaMensajes;
	}

	public void setListaMensajes(List<MensajeBD> listaMensajes) {
		this.listaMensajes = listaMensajes;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	
	

}
