package com.co.app.backend.service.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;

@Component
public class ResponseFailedBDGenerator{
	
	Logger logger = LoggerFactory.getLogger(ResponseFailedBDGenerator.class);
	
	
	public ResponseFailedBD generarException(Class<?> clase, String codigoRespuesta, String metodo, Exception exception) {
		
		
		if(codigoRespuesta==null) {
			codigoRespuesta = ConstantesBD.COD_FALLIDO;
		}
		
		logger.debug(getNombreProceso(clase, metodo)+" "+exception.getMessage());
		
		ErrorBD errorBD = new ErrorBD();
		errorBD.setCodigo(ConstantesBD.COD_FALLIDO);
		errorBD.setFeccre(new Date());
		errorBD.setPrcs(getNombreProceso(clase, metodo));
		
		List<ErrorBD> listaErrores = new ArrayList<ErrorBD>();
		listaErrores.add(errorBD);
		
		
		String mensaje = "Sin mensaje";
		
		ResponseFailedBD exceptionGeneralBD = new ResponseFailedBD(codigoRespuesta, mensaje, listaErrores, null);
		
		return exceptionGeneralBD;
		
		
	}
	
	public ResponseFailedBD generarException(Class<?> clase, String codigoRespuesta, String metodo, List<ErrorBD> listaErroresBD, List<MensajeBD> listaMensajesBD) {
		
		if(codigoRespuesta==null) {
			codigoRespuesta = ConstantesBD.COD_FALLIDO;
		}
		
		
		String mensaje = "Error realizando llamado al servicio de bd: ";
		
		
		logger.debug(getNombreProceso(clase, metodo)+" ");
		
		if(listaMensajesBD!=null) {
			logger.debug(getNombreProceso(clase, metodo)+" listaMensajes");
			for (MensajeBD mensajeBD : listaMensajesBD) {
				logger.debug(mensajeBD.getCodigo()+"-"+mensajeBD.getMensaje());
				mensaje = mensaje+" Mensaje BD: "+mensajeBD.getFeccre()+" "+mensajeBD.getCodigo()+": "+mensajeBD.getMensaje();
			}
			
		}
		
		if(listaErroresBD!=null) {
			logger.debug(getNombreProceso(clase, metodo)+" listaMensajes");
			for (ErrorBD errorBD : listaErroresBD) {
				logger.debug(errorBD.getCodigo()+"-"+errorBD.getMensaje());
				mensaje = mensaje+" Error BD: "+errorBD.getFeccre()+" "+errorBD.getCodigo()+": "+errorBD.getMensaje();
			}
			
		}
		
		ResponseFailedBD exceptionGeneralBD = new ResponseFailedBD(codigoRespuesta, mensaje, listaErroresBD, listaMensajesBD);
		
		return exceptionGeneralBD;
		
		
	}
	

	private String getNombreProceso(Class<?> clase, String method ) {

		String nombreProceso = "";
		try {
			String nombreClase = clase.getName();
			nombreProceso = nombreClase+"."+method;
		} catch (Exception e) {
			nombreProceso = new Object(){}.getClass().getEnclosingMethod().getName();
		}
		return nombreProceso;

	}
}