package com.co.app.backend.service.business.thread;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ThreadTemplateService {
	
	private Logger logger = LoggerFactory.getLogger(ThreadTemplateService.class);
	
	
	
	public List<String> readTask(){
		
		List<String> mails = new ArrayList<String>();
		
		for (int i = 0; i < 10; i++) {
			
			String correo = "task_"+i;
			mails.add(correo);
			
		}
	
		return mails;
	
	}


	@Async(value = "executorThreadDefault")
	public void executeProcess() {
		
		logger.info(Thread.currentThread().getName());
		
		List<CompletableFuture<TaskThreadTemplate>> listCompletableFuture = new ArrayList<CompletableFuture<TaskThreadTemplate>>();
		
		List<String> listaCorreos = readTask();
		
		for (String correo : listaCorreos) {
			CompletableFuture<TaskThreadTemplate> individualProcess = createAccesAndSendAccesCode(correo);
		    listCompletableFuture.add(individualProcess);
		}
		
		CompletableFuture<TaskThreadTemplate>[] arrayCompletableFuture = new  CompletableFuture[listCompletableFuture.size()];
		listCompletableFuture.toArray(arrayCompletableFuture);
		

	    // this will throw an exception if any of the futures complete exceptionally
	    CompletableFuture.allOf(arrayCompletableFuture).join();

	   
	    
	    try {
	    	TaskThreadTemplate[] arrayTaskCompleted = Arrays.stream(arrayCompletableFuture).map(CompletableFuture::join).toArray(TaskThreadTemplate[]::new);
			
			for (TaskThreadTemplate task : arrayTaskCompleted) {
				System.out.println(task.getInfo()+":"+task.getIsOk());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	}
	

	
	@Async(value = "executorThreadDefault")
	private CompletableFuture<TaskThreadTemplate> createAccesAndSendAccesCode(String info) {
		logger.info(Thread.currentThread().getName());
		
		TaskThreadTemplate taskCreateSendAccesCode = new TaskThreadTemplate("");
        taskCreateSendAccesCode.setInfo(info);
		
        try {
            logger.info("Task execution started. "+info);
            Thread.sleep(300);
            logger.info("Task execution stopped. "+info);
            
            taskCreateSendAccesCode.setIsOk(true);
            
            
            
        } catch (InterruptedException e) {
        	
        	taskCreateSendAccesCode.setIsOk(true);
        	
            logger.error("Task errror"+e.getMessage());
        }
		
		return CompletableFuture.completedFuture(taskCreateSendAccesCode);
		

    }	
	
	
	
	
	
	
	

}
