package com.co.app.backend.service.persistence.handler.postgres.commons;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.co.app.model.dto.general.MensajeBD;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HandlerMensajesJson extends BaseTypeHandler implements TypeHandler<Object>{
	@Override
	public Object getResult(ResultSet arg0, String arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
		List<MensajeBD> info= new ArrayList<MensajeBD>();
		String respuesta = null;
		try {
			respuesta = (String) cs.getString(columnIndex);
		} catch (Exception e) {
			respuesta = null;
		}
		if (respuesta != null) {
			
			try {
				
				ObjectMapper mapper = new ObjectMapper();
				
				MensajeBD[] usuariosArray = mapper.readValue(respuesta, MensajeBD[].class);
				
				if(usuariosArray!=null & usuariosArray.length>0) {
					for(MensajeBD registro : usuariosArray) {
						info.add(registro);
					}
				}
				
				
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				
			}
			
		}
        return info;       
	}

	@Override
	public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
		return;
	}

	@Override
	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
