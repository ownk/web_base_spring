package com.co.app.backend.service.persistence.mapper.user;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.co.app.model.dto.session.Servicio;
import com.co.app.model.dto.user.InfoIPNavegadorSession;
import com.co.app.model.dto.user.PasswordChange;
import com.co.app.model.dto.user.User;

//@Component
@Mapper
public interface UserMapper {

    public User getUsuarioPorNick(HashMap<Object, Object> parametrosInOout);
    
    public User getUsuarioPorEmail(HashMap<Object, Object> parametrosInOout);
    
    public List<Servicio> getServiciosTpListadoPorUsuario(HashMap<Object, Object> parametrosInOout);
    
    public List<Servicio> getServiciosUsuarioPorURL(HashMap<Object, Object> parametrosInOout);
    
    public User olvidoPassword(HashMap<Object, Object> parametrosInOout);
    
    public User cambioPassword(HashMap<Object, Object> parametrosInOout);
    
    public PasswordChange registroCambioPassword(HashMap<Object, Object> parametrosInOout);
    
    public PasswordChange getHistorialPasswordPorUser(HashMap<Object, Object> parametrosInOout);
    
    public Boolean registroLoginUser(HashMap<Object, Object> parametrosInOout);    
    
    public Boolean registroLoginOk(HashMap<Object, Object> parametrosInOout);
    
    public InfoIPNavegadorSession getLoginOk(HashMap<Object, Object> parametrosInOout);
    
    public Boolean eliminarLoginOk(HashMap<Object, Object> parametrosInOout);
    
    public Boolean registroLoginErr(HashMap<Object, Object> parametrosInOout);
    
    public InfoIPNavegadorSession getLoginErr(HashMap<Object, Object> parametrosInOout);
    
    public Boolean eliminarLoginErr(HashMap<Object, Object> parametrosInOout);
    
    public Boolean registroIpOk(HashMap<Object, Object> parametrosInOout);
    
    public InfoIPNavegadorSession getIpOk(HashMap<Object, Object> parametrosInOout);
    
    public Boolean eliminarIpOk(HashMap<Object, Object> parametrosInOout);
    
    public Boolean registroUaOk(HashMap<Object, Object> parametrosInOout);
    
    public InfoIPNavegadorSession getUaOk(HashMap<Object, Object> parametrosInOout);
    
    public Boolean eliminarUaOk(HashMap<Object, Object> parametrosInOout);
    
    public Boolean validarEmail(HashMap<Object, Object> parametrosInOout);

	public String cumplimientoUserNick(HashMap<Object, Object> parametrosInOout);
	
	public String cumplimientoUserPass(HashMap<Object, Object> parametrosInOout);

    public User getUsuariosEmailNoConfirmado(HashMap<Object, Object> parametrosInOout);
    
    public Boolean activarUsuarioEmailNoConfirmado(HashMap<Object, Object> parametrosInOout);
    
    public Boolean eliminarUsuarioEmailNoConfirmado(HashMap<Object, Object> parametrosInOout);



}