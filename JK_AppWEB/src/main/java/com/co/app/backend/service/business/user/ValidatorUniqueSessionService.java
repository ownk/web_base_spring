package com.co.app.backend.service.business.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.ResponseValidationUniqueSessionDTO;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.session.UsuarioIPNavegadorSessionDTO;
import com.co.app.model.dto.user.InfoIPNavegadorSession;


/**
 * ValidadorIPNavegadorSession
 * ==================================================
 * Clase especializada para la validacion de ip y
 * navegador de una session de usuario. Su objetivo
 * es Cierre de sesion por cambio de IP y/o
 * Navegador (Controlar sesion).
 * 
 * @author ownk
 * @authorUltimaModificacion ownk
 * @fechaCreacion 15/01/2017
 * @fechaModificacion 15/01/2017
 * @version 1.0.0
 *
 */


@Component
public class ValidatorUniqueSessionService {
	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AppService appService;
	
	
	@Autowired
	private IPNavegadorService ipNavegadorServicio;
	
	//Cantidad de minutos en los que la informacion en memoria es valida (en segundos)
	private static final int TIEMPO_MAX_INACVITIDAD_DEFAULT = 2400;
	private static final int ACCESO_FALLIDO_USUARIO_IGUAL_IPNAV_DIFERENTE = 1;
	private static final int ACCESO_FALLIDO_USUARIO_DISTINTO_IPNAV_IGUAL = 2;
	
	
	public static final String  CODIGO_RSPTA_EXITOSA = "OK";
	public static final String  CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR = "ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR";
	public static final String  CODIGO_RSPTA_ERROR_EXISTE_INGRESO_FALLIDO = "ERROR_EXISTE_INGRESO_FALLIDO";
	public static final String  CODIGO_RSPTA_ERROR_INICIANDO_SESSION = "ERROR_INIT_SESSION";
	public static final String  CODIGO_RSPTA_ERROR_NC = "ERROR_NC";
	
	
	private Logger logger = LoggerFactory.getLogger(ValidatorUniqueSessionService.class);
	
	/**
	 * isInfoIpNavegadorValida
	 * ===========================================
	 * Metodo que permite validar para un usuario
	 * si la informacion de IP y Navegador son
	 * validas con respecto a session ya iniciada
	 * 
	 * 
	 *
	 * @param usuario - String de usuario autenticado user|tipoPersona
	 * @param request - HttpServletRequest
	 * @return Boolean
	 * @throws Exception the exception
	 */
	private ResponseValidationUniqueSessionDTO isInfoIpNavegadorValida (String user_user,  HttpServletRequest request, String tiempoMaximoInactividad) throws RespuestaFallidaServicio {
		
		logger.debug(new Exception().getStackTrace()[0].getMethodName());
		ResponseValidationUniqueSessionDTO respuestaValidacionDTO = new ResponseValidationUniqueSessionDTO();
		
		
		
		//Respuesta general de validacion
		String codigoRespuesta = CODIGO_RSPTA_ERROR_NC;
		Boolean isInfoValida = false;
		
		//Se obtiene la informacion a validar
		String ipPorValidar 			= getIP(request);
		String navegadorPorValidar  	= getNavegador(request);
		String sessionPorValidar 		= getCurrentIDSession(request);
		
		logger.debug("ipPorValidar "+ipPorValidar );
		logger.debug("navegadorPorValidar "+navegadorPorValidar );
		
		//Se obtiene la informacion registrada en el sistema
		if(user_user!=null && request!=null){
			
			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(user_user, ipPorValidar, navegadorPorValidar);
			
					
			
			UsuarioIPNavegadorSessionDTO usuarioIPNavegadorSessionDTOporValidar = new UsuarioIPNavegadorSessionDTO();
			usuarioIPNavegadorSessionDTOporValidar.setFechaUltimoIngreso(new Date());
			usuarioIPNavegadorSessionDTOporValidar.setLogin(user_user);
			
			
			InfoIPNavegadorSession infoIPNavegadorSessionDTOporValidar = new InfoIPNavegadorSession();
			infoIPNavegadorSessionDTOporValidar.setIflgn_ip(ipPorValidar);
			infoIPNavegadorSessionDTOporValidar.setIflgn_user(user_user);
			infoIPNavegadorSessionDTOporValidar.setIflgn_browser(navegadorPorValidar);
			
			
			InfoIPNavegadorSession infoUltimoIngresoPorIPNavegador = null;
			try {
				infoUltimoIngresoPorIPNavegador = userService.getIpNavegadorOk(controlOperacion, navegadorPorValidar,ipPorValidar);
			} catch (RespuestaFallidaServicio e1) {
				isInfoValida = false;
				throw e1;
			}
			
			/*
			 * Validacion de unica session por ip navegador
			 * ======================================================
			 * Se valida que no exista el mismo usuario ya 
			 * autenticado con la misma ip y navegador
			 */
			if(infoUltimoIngresoPorIPNavegador!=null){
				
				
				if(infoUltimoIngresoPorIPNavegador!=null && !esInfoIguales(infoUltimoIngresoPorIPNavegador.getIflgn_user(),  user_user)){
					
					//Se permite que para la misma ip y navegador existan usuarios distintos
					codigoRespuesta ="OK";
					isInfoValida = true;
					
					
					
					
				}else{
					codigoRespuesta = "OK";
					isInfoValida = true;
				}
			}else{
				codigoRespuesta = "OK";
				isInfoValida = true;
			}
			
			
			/*
			 * Validacion de informacion de usuario
			 * ======================================================
			 * Se valida que informacion cuando el usuario se 
			 * encuentra autenticado o presenta intentos fallidos 
			 * de ingreso
			 */
			if(isInfoValida){
				/*
				 * Usuario ya se encuentra autenticado
				 * ======================================================
				 * Ya el usuario a ingresado correctamente en un instante
				 * previo
				 */
				
				InfoIPNavegadorSession infoIngresoCorrectoPorUsuario = null;
				try {
					infoIngresoCorrectoPorUsuario = userService.getLoginOk(controlOperacion, user_user);
				} catch (RespuestaFallidaServicio e1) {
					logger.debug(e1.getMensaje());
					throw e1;
				}
				InfoIPNavegadorSession infoIngresoFallidoPorUsuario = null;
				try {
					infoIngresoFallidoPorUsuario = userService.getLoginErr(controlOperacion, user_user);
				} catch (RespuestaFallidaServicio e1) {
					logger.debug(e1.getMensaje());
					throw e1;
				}
				
		
				
				if(infoIngresoCorrectoPorUsuario!=null ){
					
					/*
					 * Existe un Intento de Ingreso fallido 
					 * ======================================================
					 * Se invalida la session ya iniciada debido
					 * a que se ha registrado un intento fallido de 
					 * autenticacion para ese usuario
					 */
					
					if(infoIngresoFallidoPorUsuario!=null){
						
						
						
						
						
						
						
						/*
						 * Ingreso fallido y existe un ingreso valido
						 * ==================================================
						 * Si ya existe exitoso y existe un ingreso fallido
						 * se debe evaluar que el id de session del nuevo 
						 * ingreso no este previamente registrados en las
						 * sessiones invalidas. Si no existe en esas sessiones
						 * puede ingresar correctamente
						 */
						
						if(!sessionPorValidar.equals(infoIngresoCorrectoPorUsuario.getIflgn_session()) && !sessionPorValidar.equals(infoIngresoFallidoPorUsuario)) {
							/*
							 * Session sin iniciar
							 * =========================================================
							 */
							
							Boolean isSessionIniciada = iniciarInfoNavegadorIPSession(user_user,  request);
							
							if(isSessionIniciada){
								codigoRespuesta = "OK";
								isInfoValida = true;
							}else{
								codigoRespuesta = CODIGO_RSPTA_ERROR_INICIANDO_SESSION;
								isInfoValida = false;
							
							}
							
						}else {
							
							
							isInfoValida = false;
							
							logger.debug(CODIGO_RSPTA_ERROR_EXISTE_INGRESO_FALLIDO+" : "+infoIngresoCorrectoPorUsuario.getIflgn_ip());
							codigoRespuesta = CODIGO_RSPTA_ERROR_EXISTE_INGRESO_FALLIDO;
							
						}
						
						
						
						
						
					
						
						
					}else{
						
						
						/*
						 * Fecha ultimo ingreso aun es vigente
						 * =========================================================
						 * Se valida que la ultima fecha de ingreso valido aun siga
						 * vigente y se valida con lo ya registrado en el aplicativo
						 */
						
						if(esUltimoIngresoValidoVigente(user_user,  tiempoMaximoInactividad)){
							
							 /*
							 * Se valida informacion registrada en session
							 * ======================================================
							 * Se toma la informacion de ip, navegador y session ya 
							 * registrado y se valida con la informacion de cada 
							 * peticion
							 */
							
							if(ipPorValidar==null || !ipPorValidar.equals(infoIngresoCorrectoPorUsuario.getIflgn_ip()) ){
								logger.debug(CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR+" : "+infoIngresoCorrectoPorUsuario.getIflgn_ip());
								codigoRespuesta = CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR;
								isInfoValida = false;
								
							}else if (navegadorPorValidar==null || !navegadorPorValidar.equals(infoIngresoCorrectoPorUsuario.getIflgn_browser()) ){
								logger.debug(CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR+" : "+navegadorPorValidar +  " - " +infoIngresoCorrectoPorUsuario.getIflgn_browser());
								codigoRespuesta = CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR;
								isInfoValida = false;
							}else if (sessionPorValidar==null || !sessionPorValidar.equals(infoIngresoCorrectoPorUsuario.getIflgn_session()) ){
								logger.debug(CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR+" : "+sessionPorValidar +  " - " +infoIngresoCorrectoPorUsuario.getIflgn_session());
								codigoRespuesta = CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR;
								isInfoValida = false;
							}else{
								
								codigoRespuesta = "OK";
								isInfoValida = true;
							}
							
							//Se registra ingreso fallido para el usuario que trata de ingresar
							if(!isInfoValida){
								registrarIngresoFallido(user_user, ACCESO_FALLIDO_USUARIO_IGUAL_IPNAV_DIFERENTE, request);
							
							}
								
						 }else{
							 /*
							 * Session caducada
							 * =========================================================
							 * Si la session ha caducado por tiempo de inactividad
							 * se debe inicializar informacion de usuario normalmente
							 */
									
							 Boolean isSessionIniciada = iniciarInfoNavegadorIPSession(user_user, request);
							
							 if(isSessionIniciada){
								codigoRespuesta = "OK";
								isInfoValida = true;
							 }else{
								codigoRespuesta = CODIGO_RSPTA_ERROR_INICIANDO_SESSION;
								isInfoValida = false;
						
							 }
							 
						 }
						
						
						
					}
					
				/*
				 * Usuario NO se encuentra autenticado
				 * ======================================================
				 * El usuario no se ha autenticado previamente
				 */	
				}else{
					
					/*
					 * Existe un Intento de Ingreso fallido 
					 * ======================================================
					 * Se invalida la session ya iniciada debido
					 * a que se ha registrado un intento fallido de 
					 * autenticacion para ese usuario
					 */
					
					if(infoIngresoFallidoPorUsuario!=null){
						
						
						if(infoIngresoFallidoPorUsuario.getIflgn_ip()!= null && infoIngresoFallidoPorUsuario.getIflgn_ip().equals(ipPorValidar)){
							if(infoIngresoFallidoPorUsuario.getIflgn_browser()!=null && infoIngresoFallidoPorUsuario.getIflgn_browser().equals(navegadorPorValidar)){
								if(infoIngresoFallidoPorUsuario.getIflgn_session()!=null && infoIngresoFallidoPorUsuario.getIflgn_session().equals(sessionPorValidar)){
								
								
								
									/*
									 * Session sin iniciar
									 * =========================================================
									 * Si no existe informacion de ultimo ingreso y  presenta
									 * intentos fallidos desde mismo ip navegador con que se 
									 * esta tratando de ingresar se debe iniciar sesssion
									 */
									
									
									Boolean isSessionIniciada = iniciarInfoNavegadorIPSession(user_user,  request);
									
									if(isSessionIniciada){
										codigoRespuesta = "OK";
										isInfoValida = true;
									}else{
										codigoRespuesta = CODIGO_RSPTA_ERROR_INICIANDO_SESSION;
										isInfoValida = false;
									
									}
								}else{
									codigoRespuesta = CODIGO_RSPTA_ERROR_EXISTE_INGRESO_FALLIDO;
									isInfoValida = false;
								}
								
								
							}else{
								codigoRespuesta = CODIGO_RSPTA_ERROR_EXISTE_INGRESO_FALLIDO;
								isInfoValida = false;
							
								
							}
						}else{
							codigoRespuesta = CODIGO_RSPTA_ERROR_EXISTE_INGRESO_FALLIDO;
							isInfoValida = false;
							
							
							
						}
						
					}else{
						 /*
						 * Session sin iniciar
						 * =========================================================
						 * Si no existe informacion de ultimo ingreso y no presenta
						 * intentos fallidos de ingreso se debe inicializar 
						 * informacion de usuario normalmente
						 */
						 
						 try {
								
							Boolean isSessionIniciada = iniciarInfoNavegadorIPSession(user_user,  request);
							
							if(isSessionIniciada){
								codigoRespuesta = "OK";
								isInfoValida = true;
							}else{
								codigoRespuesta = CODIGO_RSPTA_ERROR_INICIANDO_SESSION;
								isInfoValida = false;
							
							}
						 } catch (Exception e) {
							 logger.debug("Excepcion registrando validacion : ",e);
							 codigoRespuesta = CODIGO_RSPTA_ERROR_NC;
							 isInfoValida = false;
					
						 }
						
					}
					
				}
			}
			
		}else{
			codigoRespuesta = CODIGO_RSPTA_ERROR_NC;
			isInfoValida = false;
			
		}
		
		
	
		
		
		logger.debug("Respuesta final de validacion IP: "+ipPorValidar+" Navegador: "+navegadorPorValidar+". Codigo:"+codigoRespuesta + " - "+isInfoValida);
		applyRequieredSessionAcccion(codigoRespuesta, user_user, request);
		respuestaValidacionDTO.setValido(isInfoValida);
		
		
		
		return respuestaValidacionDTO;
	}
	
	/**
	 * registrarIngresoFallido
	 * ===========================================
	 * Metodo que permite registrar una validacion
	 * fallida para un user especifico. Esto 
	 * permite que el sistema identifique que ha
	 * tenido intento de ingreso con ip y navegador
	 * de forma incorrecta
	 * 
	 * tipoFallo:  
	 * 
	 * 
	 *
	 * @param usuario - String de usuario autenticado
	 */
	
	private void registrarIngresoFallido(String user_user,  int tipoAccesoFallido, HttpServletRequest request) {
		
		
		Date sysdate = null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
		}
	 	
	 	if(sysdate==null) {
	 		sysdate = new Date();
	 	}
	 	
		
		
		String ip = getIP(request);
		String browser = getNavegador(request);
		String idSession = getCurrentIDSession(request);
		
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(user_user, ip, browser);
		
		
		if(tipoAccesoFallido == ACCESO_FALLIDO_USUARIO_DISTINTO_IPNAV_IGUAL ){
			
			
			
			
			InfoIPNavegadorSession infoFallida = new InfoIPNavegadorSession();
			infoFallida.setIflgn_ip(ip);	
			infoFallida.setIflgn_browser(browser);
			infoFallida.setIflgn_user(user_user);
			infoFallida.setIflgn_session(idSession);
			
			
			UsuarioIPNavegadorSessionDTO usuario = new UsuarioIPNavegadorSessionDTO();
			usuario.setFechaUltimoIngreso(sysdate);
			usuario.setLogin(user_user);
			
			
			try {
				
				
				userService.eliminarLoginErr(controlOperacion, user_user);
				userService.registroLoginErr(controlOperacion, infoFallida);
				
			} catch (Exception e) {
				logger.error("Error registrando acceso fallido de usuario:"+usuario.toString());
			}
			
			
			
		}else{
			UsuarioIPNavegadorSessionDTO usuario = new UsuarioIPNavegadorSessionDTO();
			usuario.setFechaUltimoIngreso(sysdate);
			usuario.setLogin(user_user);
			
			
			logger.debug(new Exception().getStackTrace()[0].getMethodName()+": usuario = "+usuario.toString());
			
			InfoIPNavegadorSession infoCorrecta =null;
			try {
				infoCorrecta = userService.getLoginOk(controlOperacion, user_user);
			} catch (RespuestaFallidaServicio e1) {
				logger.debug(e1.getMensaje());
			}

			if (infoCorrecta!=null) {
				InfoIPNavegadorSession infoFallida = new InfoIPNavegadorSession();
				infoFallida.setIflgn_ip(getIP(request));	
				infoFallida.setIflgn_browser(getNavegador(request));
				infoFallida.setIflgn_user(user_user);
				infoFallida.setIflgn_session(idSession);
				
				try {
					userService.eliminarLoginErr(controlOperacion, user_user);
					userService.registroLoginErr(controlOperacion, infoFallida);
				} catch (Exception e) {
					logger.error("Error registrando acceso fallido de usuario:"+usuario.toString());
				}
				
			}
		}
		
		

	}
	
	
	
	private Boolean iniciarInfoNavegadorIPSession(String user_user,  HttpServletRequest request) {
		
		
		Date sysdate = null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
		}
	 	
	 	if(sysdate==null) {
	 		sysdate = new Date();
	 	}
		
		Boolean isExitoso = true;
		
		isExitoso = isExitoso && resetValidadorIPNavegador(user_user,  request);
		
		
		if(isExitoso){
			
			String ip = getIP(request);
			String browser = getNavegador(request);
			String idSession = getCurrentIDSession(request);
			
			UsuarioIPNavegadorSessionDTO usuario = new UsuarioIPNavegadorSessionDTO();
			usuario.setFechaUltimoIngreso(sysdate);
			usuario.setLogin(user_user);
			
			
			logger.debug(new Exception().getStackTrace()[0].getMethodName());
			
			InfoIPNavegadorSession infoNavegadorIPSessionDTO = new InfoIPNavegadorSession();
			infoNavegadorIPSessionDTO.setIflgn_user(user_user);
			infoNavegadorIPSessionDTO.setIflgn_ip(ip);
			infoNavegadorIPSessionDTO.setIflgn_browser(browser);
			infoNavegadorIPSessionDTO.setIflgn_session(idSession);

			
			//Registro ip navegador OK
			try {
				ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(user_user, ip, browser);
				
				userService.registroIpNavegadorOk(controlOperacionServicio,infoNavegadorIPSessionDTO);
			} catch (DuplicateKeyException e) {
				isExitoso = true;
			}catch (Exception e) {
				logger.error("Error registrarInfoNavegadorIPSession:"+usuario.toString()+" "+e.getMessage());
				isExitoso = false;
			}
			
			
			//Registro User OK
			try {
				
				ControlOperacionServicio controlOperacion = new ControlOperacionServicio(user_user,ip, browser);
				
				userService.registroLoginOk(controlOperacion, infoNavegadorIPSessionDTO);
				
			} catch (DuplicateKeyException e) {
				isExitoso = true;
				
			} catch (Exception e) {
				logger.error("Error registrarInfoNavegadorIPSession:"+usuario.toString()+" "+e.getMessage());
				isExitoso = false;
			}
			
			isExitoso = isExitoso && registrarFechaUltimoIngresoValido(user_user, ip, browser, idSession);
			
			
		}
		
		
		
		return isExitoso;
	}
	
	private Boolean registrarFechaUltimoIngresoValido(String user_user, String ip, String browser, String idSession){
		
		
		Date sysdate = null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
		}
	 	
	 	if(sysdate==null) {
	 		sysdate = new Date();
	 	}
		
		
		Boolean isExitoso = true;
		
		UsuarioIPNavegadorSessionDTO usuario = new UsuarioIPNavegadorSessionDTO();
		usuario.setFechaUltimoIngreso(sysdate);
		usuario.setLogin(user_user);
		
		logger.debug(new Exception().getStackTrace()[0].getMethodName());
		
		try {
			
			InfoIPNavegadorSession infoIPNavegadorSession = new InfoIPNavegadorSession();
		    infoIPNavegadorSession.setIflgn_browser(browser);
		    infoIPNavegadorSession.setIflgn_ip(ip);
		    infoIPNavegadorSession.setIflgn_date_last_ingr(sysdate);
		    infoIPNavegadorSession.setIflgn_user(user_user);
		    infoIPNavegadorSession.setIflgn_session(idSession);
			
			
			ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(user_user,ip, browser);
			
			userService.eliminarUaOk(controlOperacionServicio, user_user);
			userService.registroUaOk(controlOperacionServicio, infoIPNavegadorSession);
			
		} catch (Exception e) {
			logger.error("Error registrarFechaUltimoIngresoValido:"+usuario.toString()+" "+e.getMessage());
			isExitoso = false;
		}
		
		return isExitoso;
		
	}
	


	private Boolean esUltimoIngresoValidoVigente(String user_user,  String tiempoMaximoInactividad) throws RespuestaFallidaServicio{
		
		
		
		Date sysdate = null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
		}
	 	
	 	if(sysdate==null) {
	 		sysdate = new Date();
	 	}
		
		logger.debug(new Exception().getStackTrace()[0].getMethodName());
		
		UsuarioIPNavegadorSessionDTO usuario = new UsuarioIPNavegadorSessionDTO();
		usuario.setFechaUltimoIngreso(sysdate);
		usuario.setLogin(user_user);
		
		Boolean isUltimoIngresoValidoVigente = true;
		Integer tiempoMaximoPermitido = TIEMPO_MAX_INACVITIDAD_DEFAULT;
		
		String ip = ipNavegadorServicio.getIP();
		String browser = "-";
		
		
		ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(user_user, ip, browser);
		
		InfoIPNavegadorSession usuarioConsultado  = userService.getUaOk(controlOperacionServicio, user_user);
		
		if(usuarioConsultado!= null) {
			Date fechaUltimoIngresoValido = usuarioConsultado.getIflgn_date_last_ingr();
			
			
			if(tiempoMaximoInactividad!=null && !tiempoMaximoInactividad.isEmpty()){
				try{
					tiempoMaximoPermitido = Integer.parseInt(tiempoMaximoInactividad);
				}catch(Exception exception){
					tiempoMaximoPermitido = TIEMPO_MAX_INACVITIDAD_DEFAULT;
				}
			}
			
			if(fechaUltimoIngresoValido!=null){
				
						
				Date fechaVigente = DateUtils.addSeconds(fechaUltimoIngresoValido, tiempoMaximoPermitido.intValue());
				
				if(fechaVigente.before(sysdate)){
					isUltimoIngresoValidoVigente = false;
					
				}
				
			}else{
				isUltimoIngresoValidoVigente = false;
			}
			
			
			
		}else{
			isUltimoIngresoValidoVigente = false;
		}
			
		
		return isUltimoIngresoValidoVigente;
	} 
	
	
	
	
		
	private String getIP(HttpServletRequest request){
		
		String ip = null;
		
		ip=ipNavegadorServicio.getIP(request);	
		
		if(ip==null){
			ip="undefined";
		}
		
		return ip;
	}
	
	private String getNavegador(HttpServletRequest request){
		String navegador = request.getHeader("user-agent");
		if (navegador == null) {
			navegador = request.getHeader("USER-AGENT");
		}
		return navegador;
	}
	
	private Boolean esInfoIguales(String info1, String info2 ){
		Boolean isUsuariosIguales = false;
		
		String info_info1= info1+"";
		String info_info2= info2+"";
		
		if(info_info1.equals(info_info2)){
			isUsuariosIguales = true;
		}
		
		return isUsuariosIguales;
	}
	
	
	private String getCurrentIDSession(HttpServletRequest request) {
		
		return SessionAppUsuario.getCurrentIDSession(request);
		
	}
	
	
	private void applyRequieredSessionAcccion(String codigoRespuestaValidacionIPNavegador, String user_user, HttpServletRequest request){
		
		Boolean resetValidadorIPNavegador = false;
		
		if(codigoRespuestaValidacionIPNavegador.equals(CODIGO_RSPTA_EXITOSA)){
			 resetValidadorIPNavegador = false;
			
		}
		
		
		if(codigoRespuestaValidacionIPNavegador.equals(CODIGO_RSPTA_ERROR_MISMO_USUARIO_DIFERENTE_IPNAVEGADOR)){
			resetValidadorIPNavegador = false;
			
		}
		
		
		if(codigoRespuestaValidacionIPNavegador.equals(CODIGO_RSPTA_ERROR_EXISTE_INGRESO_FALLIDO)){
			resetValidadorIPNavegador = true;
			
		}
			
		
		if(codigoRespuestaValidacionIPNavegador.equals(CODIGO_RSPTA_ERROR_INICIANDO_SESSION)){
			resetValidadorIPNavegador = true;
			
		}
				
		if(codigoRespuestaValidacionIPNavegador.equals(CODIGO_RSPTA_ERROR_NC)){
			resetValidadorIPNavegador = true;
			
		}
	

		
		if (resetValidadorIPNavegador) {

			resetValidadorIPNavegador(user_user, request);

		}
		
		
		
		
	}
	
	
	public ResponseValidationUniqueSessionDTO validarUniqueSession(String user_user,HttpServletRequest request)  throws Exception{
		
		String tiempoMaximoInactividad = ""+TIEMPO_MAX_INACVITIDAD_DEFAULT;
		try {
			ParametroConfiguracionGeneral parameterSessionInactiveMaxTime = appService.getParameter(ConstantesGeneralesAPP.CNFG_SESSION_INACTIVE_MAX_TIME);
			if(parameterSessionInactiveMaxTime!=null){
				tiempoMaximoInactividad= parameterSessionInactiveMaxTime.getCnfg_valor();
			}
		} catch (Exception e) {
			
			logger.debug(e.getMessage());
			tiempoMaximoInactividad = ""+TIEMPO_MAX_INACVITIDAD_DEFAULT;
		}
		
		ResponseValidationUniqueSessionDTO respuestaValidacion = isInfoIpNavegadorValida(user_user, request, tiempoMaximoInactividad);
		return respuestaValidacion;
	}
	
	/**
	 * Metodo para inicializar validacion de IP y navegador
	 * para un user especifico. Este metodo elimina
	 * toda la informacion para el user especifico y permite
	 * reiniciar las validaciones de ip y navegador
	 * 
	 */
	
	public Boolean resetValidadorIPNavegador(String user_user,  HttpServletRequest request){
		
		
		
		Date sysdate = null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
		}
	 	
	 	if(sysdate==null) {
	 		sysdate = new Date();
	 	}
		
		Boolean isAllOK = true;
		
		UsuarioIPNavegadorSessionDTO usuario = new UsuarioIPNavegadorSessionDTO();
		usuario.setFechaUltimoIngreso(sysdate);
		usuario.setLogin(user_user);
		
		String ip = getIP(request);
		String browser = getNavegador(request);
		
		
		InfoIPNavegadorSession infoIPNavegadorSessionDTO = new InfoIPNavegadorSession();
		infoIPNavegadorSessionDTO.setIflgn_ip(ip);
		infoIPNavegadorSessionDTO.setIflgn_browser(browser);
		infoIPNavegadorSessionDTO.setIflgn_user(user_user);
		
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(user_user, ip, browser);
		
		
		
		//Se elimina la informacion especifica por usuario
		try {
			isAllOK = isAllOK && userService.eliminarLoginOk(controlOperacion, user_user);
			
		} catch (Exception e) {
			logger.error("Error resetValieliminarIngresosFallidosPorUsuario:"+usuario.toString()+" "+e.getMessage());
			isAllOK = isAllOK && false;
		}
		
		
		//Se elimina la informacion especifica por usuario
		try {
			isAllOK = isAllOK && userService.eliminarLoginErr(controlOperacion, user_user);;
			
		} catch (Exception e) {
			logger.error("Error resetValieliminarIngresosFallidosPorUsuario:"+usuario.toString()+" "+e.getMessage());
			isAllOK = isAllOK && false;
		}	
		
		
		//Se elimina la informacion especifica por usuario
		try {
			isAllOK = isAllOK && userService.eliminarUaOk(controlOperacion, user_user);;
			
		} catch (Exception e) {
			logger.error("Error resetValidadorIPNavegador:"+usuario.toString()+" "+e.getMessage());
			isAllOK = isAllOK && false;
		}
		
		//Se elimina la informacion especifica por usuario
		try {
			isAllOK = isAllOK && userService.eliminarIpOk(controlOperacion, user_user);;
			
		} catch (Exception e) {
			logger.error("Error resetValidadorIPNavegador:"+usuario.toString()+" "+e.getMessage());
			isAllOK = isAllOK && false;
		}
			
				
		
		return isAllOK;
		
	}

	

	

	
	
}
