package com.co.app.backend.service.business.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.exception.FileStorageException;
import com.co.app.model.dto.exception.MyFileNotFoundException;
import com.co.app.model.dto.general.RespuestaFallidaServicio;


@Service
public class ClientImageProfileService {

	
	
	Logger logger = LoggerFactory.getLogger(ClientImageProfileService.class);
    
    @Autowired
    private ClientService clientService;
    
    
    @Autowired
    private AppService appService;
    
    
    
    private Path fileStorageLocation;
    
    

    private Path getPathStorage()  {
    	
    	ParametroConfiguracionGeneral parameter;
		try {
			parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_CLIENT_PROFILE_IMAGE_UPLOAD_BASEPATH);
			
			String uploadDir = parameter.getCnfg_valor();
			
			
    		fileStorageLocation = Paths.get(uploadDir).toAbsolutePath().normalize();

            try {
                Files.createDirectories(fileStorageLocation);
            } catch (Exception ex) {
            	
                throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
            }
    		
    		return fileStorageLocation;
	    	
			
			
		} catch (RespuestaFallidaServicio e) {
			
			return fileStorageLocation;
		}
    	
    	
    	
    }
    

    public String storeFile(String idClient, MultipartFile file) {
        // Normalize file name
    	
    	UUID uuid = UUID.randomUUID();
    	String uuidString = uuid.toString();
    	
    	
    	String finalFileName = "cl_profile_"+idClient+".png";
    	
    	
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("ERR_IMG_INV_PATH" + fileName);
            }
            
            
            if(!fileName.endsWith(".png")) {
            	throw new FileStorageException("ERR_IMG_INV_PNG" + fileName);
            	
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.getPathStorage().resolve(finalFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            
            
            
            

            return finalFileName;
        } catch (IOException ex) {
            throw new FileStorageException("ERR_IMG_NC", ex);
        }
    }
    
    
    

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.getPathStorage().resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }
    
	public String getBaseRelativePath() {
		
		String relativePath = null;
		
		ParametroConfiguracionGeneral parameter;
		try {
			parameter = appService.getParameter(ConstantesGeneralesAPP.CNFG_CLIENT_PROFILE_IMAGE_RELATIVE_BASEURL);
			
			 relativePath = parameter.getCnfg_valor();
			
	    	
		} catch (RespuestaFallidaServicio e) {
			
			logger.error(e.getMensaje());
		}
    	
		
		return relativePath;
	}

	
	
    
}
