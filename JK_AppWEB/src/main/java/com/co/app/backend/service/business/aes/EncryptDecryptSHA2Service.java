package com.co.app.backend.service.business.aes;

import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptDecryptSHA2Service {
	private Logger logger = LoggerFactory.getLogger(EncryptDecryptSHA2Service.class);
	
	
   private static EncryptDecryptSHA2Service aesServiceInstance;
    
    public static EncryptDecryptSHA2Service getInstance() {
    	
    	if(aesServiceInstance==null) {
    		aesServiceInstance = new EncryptDecryptSHA2Service();
    		
    	}
    	
    	return aesServiceInstance;
    }
	    
	

    public  String encrypt(String strToEncrypt, String salt,  String iv, String key ) 
    {
        try
        {
        	//byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        	
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
             
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
             
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } 
        catch (Exception e) 
        {
        	logger.error(e.getMessage()+". Errror encrypt "+strToEncrypt);
        }
        return null;
    }
    
    public  String decrypt(String strToDecrypt, String salt,  String iv, String key) {
        try
        {
            //byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
             
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
             
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } 
        catch (Exception e) {
        	logger.error(e.getMessage()+". Errror decrypt "+strToDecrypt);
        }
        return null;
    }
    
    
    public  String generateRandomSalt() {
    	String encryptSalt = RandomStringUtils.random(32, true, true);
		
    	return encryptSalt;
		
    }
    
    public  String generateRandomIV() {
    	
    	//encryptIV should be 16 characters only
    	String encryptIV = 	 RandomStringUtils.random(16, true, true);
    	
    	return encryptIV;
    }

    

    /*
    public static void main(String[] args) 
    {
        
    	
    	String originalString = "howtodoinjava.com";
         
        String encryptedString = EncryptDecryptSHA256Service.encrypt(originalString, salt, iv, secretKey ) ;
        String decryptedString = EncryptDecryptSHA256Service.decrypt(encryptedString, salt, iv, secretKey) ;
          
        System.out.println(originalString);
        System.out.println(encryptedString);
        System.out.println(decryptedString);
    }
    */
    
    
    
    

}
