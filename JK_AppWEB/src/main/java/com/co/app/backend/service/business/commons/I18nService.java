package com.co.app.backend.service.business.commons;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.co.app.model.dto.game.Answer;
import com.co.app.model.dto.game.AttribChrt;
import com.co.app.model.dto.game.AttribPlayer;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Question;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.session.Servicio;
 
@Component
public class I18nService  {
	
	@Autowired
	MessageSource messageSource;
	
	public String translate(String value) {
		String translate = value;
		if(value!=null) {
			
			translate= translate.replaceAll("\"", "").trim();		
			String newValue = null;
			
			try {
				newValue = messageSource.getMessage(translate, null, LocaleContextHolder.getLocale());
			} catch (Exception e) {
				
				Locale locale = LocaleContextHolder.getLocale();
				
				translate = "¿¿¿"+translate+"_"+locale.getDisplayLanguage()+"???";
			}
					
			if(newValue!=null ) {
				translate = newValue;
			}		
			
		}
		
		return translate;
	}
	
	public void applyi18nWorld(World world) {
		if(world!=null) {
			world.setWorld_descri(translate(world.getWorld_descri()));
			world.setWorld_name(translate(world.getWorld_name()));
		}
		
	}
	
	
	public void applyi18nGame(Game game) {
		if(game!=null) {
			game.setGame_descri(translate(game.getGame_descri()));
			game.setGame_name(translate(game.getGame_name()));
		}
		
	}

	public void applyi18nShip(Ship ship) {
		if(ship!=null) {
			ship.setShip_name(translate(ship.getShip_name()));
			ship.setShip_descri(translate(ship.getShip_descri()));
			

			if(ship.getListAttributes()!=null) {
				List<AttribChrt> list= ship.getListAttributes();
				for (AttribChrt att : list) {
					att.setAtrb_name(translate(att.getAtrb_name()));
					att.setAtrb_descri(translate(att.getAtrb_descri()));
				}
			}
		}
		
	}

	public void applyi18nPet(Pet pet) {
		if(pet!=null) {
			pet.setPet_name(translate(pet.getPet_name()));
			pet.setPet_descri(translate(pet.getPet_descri()));
			
			if(pet.getListAttributes()!=null) {
				List<AttribChrt> list= pet.getListAttributes();
				for (AttribChrt att : list) {
					att.setAtrb_name(translate(att.getAtrb_name()));
					att.setAtrb_descri(translate(att.getAtrb_descri()));
				}
			}
			
		}
	}

	public void applyi18nAvatar(Avatar avatar) {
		if(avatar!=null) {
			avatar.setAvatar_name(translate(avatar.getAvatar_name()));
			avatar.setAvatar_descri(translate(avatar.getAvatar_descri()));
			

			if(avatar.getListAttributes()!=null) {
				List<AttribChrt> list= avatar.getListAttributes();
				for (AttribChrt att : list) {
					att.setAtrb_name(translate(att.getAtrb_name()));
					att.setAtrb_descri(translate(att.getAtrb_descri()));
				}
			}
		}
		
	}

	public void applyi18nPlayer(Player player) {
		if(player!=null) {
			
			if(player.getListAttributes()!=null) {
				List<AttribPlayer> list= player.getListAttributes();
				for (AttribPlayer attribPlayer : list) {
					attribPlayer.setAtrb_name(translate(attribPlayer.getAtrb_name()));
					attribPlayer.setAtrb_descri(translate(attribPlayer.getAtrb_descri()));
				}
			}
			
		}
		
	}
	
	
	
	
	public void applyi18nServicio(Servicio servicio) {
		if(servicio!=null) {
			
			servicio.setMdlo_descri(translate(servicio.getMdlo_descri()));
			servicio.setMdlo_nomb(translate(servicio.getMdlo_nomb()));
			servicio.setMenu_nombre(translate(servicio.getMenu_nombre()));
			servicio.setSrvc_nomb(translate(servicio.getSrvc_nomb()));
			servicio.setSrvc_descri(translate(servicio.getSrvc_descri()));
		}
		
	}
	
	
	
	
	
	public void applyi18nQuestion(Question question) {
		if(question!=null) {
			
			question.setQuestn_descri(translate(question.getQuestn_descri()));
			
		}
		
	}
	
	
	public void applyi18nAnswer(Answer answer) {
		if(answer!=null) {
			
			answer.setAnswer_descri(translate(answer.getAnswer_descri()));
			
		}
		
	}
	
	public void applyi18nTipoDocumento(TipoDocumento tipoDocumento) {
		if(tipoDocumento!=null) {
			
			tipoDocumento.setTdoc_doc(translate(tipoDocumento.getTdoc_doc()));
			
		}
		
	}
	
	public void applyi18nLearningObjectives(LearningObjective lrnobj) {
		if(lrnobj!=null) {
			
			lrnobj.setLrnobj_descri(translate(lrnobj.getLrnobj_descri()));
			
		}
		
	}
	
	
}