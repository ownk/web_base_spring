package com.co.app.backend.service.persistence.controller.player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.persistence.CommonsParameterBDGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.ResponseFailedBDGenerator;
import com.co.app.backend.service.persistence.mapper.player.PlayerMapper;
import com.co.app.model.dto.account.Account;
import com.co.app.model.dto.client.PlayerExternalIDResponse;
import com.co.app.model.dto.game.AnswerQuestion;
import com.co.app.model.dto.game.AttribPlayer;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Score;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.user.User;


@Component
public class PlayerControllerDB {
	
	
	private Logger logger = LoggerFactory.getLogger(PlayerControllerDB.class);
	
	@Autowired
	private PlayerMapper mapper;
	
	@Autowired
	private ResponseFailedBDGenerator generadorExcepcionBD;
	
	@Autowired
	private AppService appServicio;
	

	@Autowired
	private I18nService i18nService;
	
	
	public List<TipoDocumento> getTiposDocumento(ControlOperacionBD controlOperacionBD) throws ResponseFailedBD{
		
		String pgcriptoKey = appServicio.getPGCryptoKey();
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<TipoDocumento> tipoDocumentos= null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			mapper.getTiposDocumento(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				tipoDocumentos = (List<TipoDocumento>) parametrosInOout.get("p_ttdoc");
				
				
				if(tipoDocumentos!=null){
					for (TipoDocumento tipoDocumento : tipoDocumentos) {
						i18nService.applyi18nTipoDocumento(tipoDocumento);
						
					}
					
				}
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
						
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return tipoDocumentos;

	}
	
	
	
	public Boolean registrarJugador(ControlOperacionBD controlOperacionBD,User usuarioParaCrear,  String codigo_acceso, String otp, Player player)  throws ResponseFailedBD {

		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tuser", usuarioParaCrear);
			parametrosInOout.put("p_codigo_acceso", codigo_acceso);
			parametrosInOout.put("p_token", otp);
			parametrosInOout.put("p_tplayer", player);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.registrarJugador(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		}  catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean validarRegistro(ControlOperacionBD controlOperacionBD,String idUsuario,String otp, String codigo_acceso)  throws ResponseFailedBD {

				
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", idUsuario);
			parametrosInOout.put("p_otpu", otp);
			parametrosInOout.put("p_codigo_acceso", codigo_acceso);
			mapper.validarRegistro(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	
	
	
	public Boolean validarLog(ControlOperacionBD controlOperacionBD)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio

			mapper.validarLog(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		}catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public List<Player> getPlayersByUser(ControlOperacionBD controlOperacionBD, String idUser)  throws ResponseFailedBD {

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = new ArrayList<Player>();
		
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", idUser);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerJugadorPorUsuario(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");
				
					
				if(players!=null && players.size()>0) {
					
					List<AttribPlayer> atributos =null;
					
					atributos = (List<AttribPlayer>) parametrosInOout.get("p_tatrpl");
					
					if(atributos!=null && atributos.size()>0) {
						
						for (Player player : players) {
							
							List<AttribPlayer> listAttrPlayer = new ArrayList<AttribPlayer>();
							
							for (AttribPlayer attribPlayer : atributos) {
								if(attribPlayer.getAtrpl_player().equals(player.getPlayer_player())) {
									
									
									
									listAttrPlayer.add(attribPlayer);
								}
							}
							
							player.setListAttributes(listAttrPlayer);

							i18nService.applyi18nPlayer(player);
							
						}
					}
					
				}
				
				
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	
	
	public List<Player> getPlayerByID(ControlOperacionBD controlOperacionBD, String idPlayer)  throws ResponseFailedBD {

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = new ArrayList<Player>();
		
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerJugadorPorID(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");
				
					
				if(players!=null && players.size()>0) {
					
					List<AttribPlayer> atributos =null;
					
					atributos = (List<AttribPlayer>) parametrosInOout.get("p_tatrpl");
					
					if(atributos!=null && atributos.size()>0) {
						
						for (Player player : players) {
							
							List<AttribPlayer> listAttrPlayer = new ArrayList<AttribPlayer>();
							
							for (AttribPlayer attribPlayer : atributos) {
								if(attribPlayer.getAtrpl_player().equals(player.getPlayer_player())) {
									
									
									
									listAttrPlayer.add(attribPlayer);
								}
							}
							
							player.setListAttributes(listAttrPlayer);

							i18nService.applyi18nPlayer(player);
							
						}
					}
					
				}
				
				
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	

	public Boolean asignarAvatar(ControlOperacionBD controlOperacionBD, String idPlayer, String idAvatar)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_avatar_avatar", idAvatar);

			mapper.asignarAvatar(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean asignarPet(ControlOperacionBD controlOperacionBD, String idPlayer, String idPet)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_pet_pet", idPet);

			mapper.asignarPet(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}

	public Boolean asignarShip(ControlOperacionBD controlOperacionBD, String idPlayer, String idShip)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_ship_ship", idShip);

			mapper.asignarShip(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	
	public Avatar obtenerAvatarPlayer(ControlOperacionBD controlOperacionBD, String idPlayer)  throws ResponseFailedBD {

		
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		Avatar avatar = null;
		
		
		List<AttribPlayer> atributos = new ArrayList<AttribPlayer>();


		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			
			parametrosInOout.put("p_player_player", idPlayer);
			mapper.obtenerAvatarPlayer(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Avatar> lista =(List<Avatar>) parametrosInOout.get("p_tavatar");
				atributos = (List<AttribPlayer>) parametrosInOout.get("p_tatrpl");
				if (!lista.isEmpty()) {
					avatar = lista.get(0);
					
					i18nService.applyi18nAvatar(avatar);
					
				} else {
					return null;
				}
			} else {
				throw exceptionGeneralBD;
			}
			
		}catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return avatar;

	}
	
	public Pet obtenerPetPlayer(ControlOperacionBD controlOperacionBD, String idPlayer)  throws ResponseFailedBD {

		
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		Pet pet = null;

		
		List<AttribPlayer> atributos = new ArrayList<AttribPlayer>();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			mapper.obtenerPetPlayer(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Pet> lista =(List<Pet>) parametrosInOout.get("p_tpet");
				atributos = (List<AttribPlayer>) parametrosInOout.get("p_tatrpl");
				
				if (!lista.isEmpty()) {
					pet = lista.get(0);
					
					i18nService.applyi18nPet(pet);
					
					
				} else {
					return null;
				}
				
			} else {
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return pet;

	}

	public Ship obtenerShipPlayer(ControlOperacionBD controlOperacionBD, String idPlayer)  throws ResponseFailedBD {

	
	
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
	
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		Ship ship = null;
			
		List<AttribPlayer> atributos = new ArrayList<AttribPlayer>();
		try {

		
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);
	
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			mapper.obtenerShipPlayer(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Ship> lista =(List<Ship>) parametrosInOout.get("p_tship");
				atributos = (List<AttribPlayer>) parametrosInOout.get("p_tatrpl");
				
				if (!lista.isEmpty()) {
					ship = lista.get(0);
					
					
					i18nService.applyi18nShip(ship);
					
					
				} else {
					return null;
				}
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
	
		return ship;
	
	}
	
	
	public List<Match> obtenerMatchesByPlayer(ControlOperacionBD controlOperacionBD, String idPlayer, String idGame, int limit, int offset)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Match> listaMatches = null;
		List<Game> listaGames = null;
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_game_game", idGame);
			parametrosInOout.put("p_limit", limit);
			parametrosInOout.put("p_offset", offset);
			mapper.obtenerMatchesPlayer(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				listaMatches = (List<Match>) parametrosInOout.get("p_tmatch");
				listaGames = (List<Game>) parametrosInOout.get("p_tgame");
				
				
				if(listaMatches!=null && listaGames!=null) {
					
					for (Game game : listaGames) {
						i18nService.applyi18nGame(game);
					}
					
					
					
					for (Match match : listaMatches) {
						for (Game game : listaGames) {
							if(game.getGame_game().equals(match.getMatch_game())) {
								
								match.setGame(game);
							}
						}
						
						
					}
					
				}
				
				return listaMatches;	
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
	
	}
	

	
	public List<Score> obtenerMatchScore(ControlOperacionBD controlOperacionBD, String idMatch)  throws ResponseFailedBD {

		
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
	
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Score> listaScore = null;
	
		try {

		
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);
	
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_match_match", idMatch);
			mapper.obtenerMatchScore(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				listaScore =(List<Score>) parametrosInOout.get("p_tscore");
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
	
		return listaScore;
	
	}
	
	public Boolean actualizarTipoJugador(ControlOperacionBD controlOperacionBD, String idPlayer, String tpPlayer)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_player_type", tpPlayer);

			mapper.actualizarTipoJugador(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public List<World> obtenerPlayerWordls(ControlOperacionBD controlOperacionBD, String idPlayer)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<World> listaWorlds = new ArrayList<World>();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);

			mapper.obtenerPlayerWordls(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaWorlds = (List<World>) parametrosInOout.get("p_tworld");		
				
				if(listaWorlds!=null) {
					Collections.sort(listaWorlds);
					
					for (World world : listaWorlds) {
						i18nService.applyi18nWorld(world);
					}
				}
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaWorlds;

	}
	
	public List<Game> obtenerGamesWorld(ControlOperacionBD controlOperacionBD, String idMundo, String idPlayer)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Game> listaGames = new ArrayList<Game>();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_world_world", idMundo);
			parametrosInOout.put("p_player_player", idPlayer);

			mapper.obtenerGamesWorld(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaGames = (List<Game>) parametrosInOout.get("p_tgame");		
				
				if(listaGames!=null) {
					Collections.sort(listaGames);
					
					for (Game game : listaGames) {
						i18nService.applyi18nGame(game);
					}
					
				}
			
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaGames;

	}
	
	public List<Game> obtenerGamesByPlayer(ControlOperacionBD controlOperacionBD , String idPlayer)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Game> listaGames = new ArrayList<Game>();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);

			mapper.obtenerGamesByPlayer(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaGames = (List<Game>) parametrosInOout.get("p_tgame");	
				
				if(listaGames!=null) {
					for (Game game : listaGames) {
						i18nService.applyi18nGame(game);
					}

				}
					
				
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaGames;

	}
	
	public Boolean guardarPeguntas(ControlOperacionBD controlOperacionBD, String idPlayer, List<AnswerQuestion> respuestaPreguntas)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_tanwqtn", respuestaPreguntas);

			mapper.guardarPeguntas(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	
	public Account obtenerCuentaPorPlayerCliente(ControlOperacionBD controlOperacionBD , String idPlayer, String idClient)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		Account cuenta = new Account();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put("p_client_client", idClient);


			mapper.obtenerCuentaPorPlayerCliente(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				cuenta = (Account) parametrosInOout.get("p_taccount");	
								
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return cuenta;

	}
	
	public PlayerExternalIDResponse actualizarInfoPlayer(ControlOperacionBD controlOperacionBD, List<Player> players, String idClient)  throws ResponseFailedBD {

		PlayerExternalIDResponse playerExternalIDResponse = null;
		
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tplayer", players);
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.actualizarInfoPlayer(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			
			playerExternalIDResponse = new PlayerExternalIDResponse(); 
			
			playerExternalIDResponse.setErrores(errores);
			playerExternalIDResponse.setMensajes(mensajes);
			playerExternalIDResponse.setPlayers(players);
			
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return playerExternalIDResponse;

	}
	
}

