package com.co.app.backend.service.business.commons;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


@Component
public class IPNavegadorService {

	private Logger logger = LoggerFactory.getLogger(IPNavegadorService.class);
	
	@Autowired
	HttpServletRequest request;


	public String getIP(){

		return getIP(request);
	}
	
	public String getIDRequest(){

		return getIDRequest(request);
	}
	
	public String getIP(HttpServletRequest request){

		String ipAcceso = null;

		try {

			if (ipAcceso == null) {
				ipAcceso = request.getHeader("X-FORWARDED-FOR");
				
			}

			if (ipAcceso == null) {
				ipAcceso = request.getHeader("x-forwarded-for");
			}

			if (ipAcceso == null) {
				ipAcceso = request.getHeader("REMOTE_HOST");
			}

			if (ipAcceso == null) {
				ipAcceso = request.getHeader("remote_host");
			}

			if (ipAcceso == null || ipAcceso.equals("::1")) {
				ipAcceso = request.getRemoteAddr();
				if(ipAcceso.equals("0:0:0:0:0:0:0:1")){
					ipAcceso = null;
				}
			}


			if(ipAcceso ==null){

				for (Enumeration e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {

					NetworkInterface ni = (NetworkInterface) e.nextElement();
					for (Enumeration ee = ni.getInetAddresses(); ee.hasMoreElements();) {
						InetAddress ip = (InetAddress) ee.nextElement();
						
						if (ip instanceof Inet4Address && !ip.getHostAddress().equals("127.0.0.1")) {
							
							ipAcceso = ip.getHostAddress();
						
						}
					}
				}

			}
			
		} catch (Exception e) {
			logger.error("Error al obtener la IP", e);
		}
		
		return ipAcceso;
	}
	
	public String getNavegador(HttpServletRequest request){
		String navegador = request.getHeader("user-agent");
		if (navegador == null) {
			navegador = request.getHeader("USER-AGENT");
		}
		return navegador;
	}
	
	
	public String getIDRequest(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		
		if(session!=null) {
			String id = session.getId();
			
			if(id!=null && !StringUtils.isEmpty(id)){
				return id;
			}
			
		}
		
		
		return null;
		
	}
	
	
	public String getIDForIPValidation() {
		
		String id = getIP()+"-"+getIDRequest();
		
		return id;
	}
}
