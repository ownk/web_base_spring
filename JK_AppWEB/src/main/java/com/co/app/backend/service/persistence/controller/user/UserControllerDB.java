package com.co.app.backend.service.persistence.controller.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.persistence.CommonsParameterBDGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.ResponseFailedBDGenerator;
import com.co.app.backend.service.persistence.mapper.user.UserMapper;
import com.co.app.model.dto.game.AttribPlayer;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;
import com.co.app.model.dto.session.Servicio;
import com.co.app.model.dto.session.URLServicio;
import com.co.app.model.dto.user.InfoIPNavegadorSession;
import com.co.app.model.dto.user.LoginUser;
import com.co.app.model.dto.user.OtpUser;
import com.co.app.model.dto.user.PasswordChange;
import com.co.app.model.dto.user.User;


@Component
public class UserControllerDB {
	
	
	private Logger logger = LoggerFactory.getLogger(UserControllerDB.class);
	
	@Autowired
	private UserMapper mapper;
	
	@Autowired
	ResponseFailedBDGenerator generadorExcepcionBD;
	
	@Autowired
	private AppService appServicio;
	
	@Autowired
	private I18nService i18nService;
	
	
	
	public User getUsuarioPorNick(ControlOperacionBD controlOperacionBD, String user_nick) throws ResponseFailedBD{

		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		User usuario = null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_nick", user_nick);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getUsuarioPorNick(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<User> lista = (List<User>) parametrosInOout.get("p_tuser");
				if (!lista.isEmpty()) {
					usuario = lista.get(0);
				} else {
					return null;
				}
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			

		
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return usuario;

	}
	
	public User getUsuarioPorEmail(ControlOperacionBD controlOperacionBD, String user_email) throws ResponseFailedBD{

		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		User usuario = null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_email", user_email);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getUsuarioPorEmail(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<User> lista = (List<User>) parametrosInOout.get("p_tuser");
				if (!lista.isEmpty()) {
					usuario = lista.get(0);
				} else {
					return null;
				}
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			

		
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return usuario;

	}
	
	public List<Servicio> getMenuGeneralPorUsuario(ControlOperacionBD controlOperacionBD, String user_user) throws ResponseFailedBD {
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Servicio> listaServicios = new ArrayList<Servicio>();

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), new Object() {}.getClass().getEnclosingMethod().getName());
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tservicio", null);
			parametrosInOout.put("p_tsurl", null);
			parametrosInOout.put("p_user_user", user_user);
			mapper.getServiciosTpListadoPorUsuario(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta,  nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Servicio> servicios = (List<Servicio>) parametrosInOout.get("p_tservicio");
				List<URLServicio> urls = (List<URLServicio>) parametrosInOout.get("p_tsurl");
				
				
				if (servicios.isEmpty()) {
					listaServicios = null;
				} else {
					
					for (Servicio servicio : servicios) {
						i18nService.applyi18nServicio(servicio);
						
						
						if(urls!=null) {
							for (URLServicio urlServicio : urls) {
								
								if(urlServicio.getSurl_srvc().equals(servicio.getSrvc_srvc())) {
									servicio.addUrl(urlServicio);
								}
								
							}
							
						}
						
						
					}
					
					listaServicios = servicios;
					
					
				}
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		}  catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}finally {
			//session.close();
		}

		return listaServicios;

	}
	
	
	
	
	public List<Servicio> getServiciosUsuarioPorURL(ControlOperacionBD controlOperacionBD, String user_user, String url) throws ResponseFailedBD {
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Servicio> listaServicios = new ArrayList<Servicio>();

		try {

			
			//Se crean parametros de control de operacion
			 
			
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), new Object() {}.getClass().getEnclosingMethod().getName());
			
			
			
			//Se crean los parametros de negocio entrada
			parametrosInOout.put("p_user_user", user_user);
			parametrosInOout.put("p_surl_url", url);
			
			
			//Se crean los parametros de negocio de salida
			parametrosInOout.put("p_tservicio", null);
			
			mapper.getServiciosUsuarioPorURL(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Servicio> lista = (List<Servicio>) parametrosInOout.get("p_tservicio");
				if (lista.isEmpty()) {
					listaServicios = null;
				} else {
					
					for (Servicio servicio : lista) {
						i18nService.applyi18nServicio(servicio);
					}
					
					listaServicios = lista;
				}
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			

		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return listaServicios;

	}
	
	public Boolean olvidoPassword(ControlOperacionBD controlOperacionBD, String user_email, String otp)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_otpp", otp);
			parametrosInOout.put("p_email", user_email);
			mapper.olvidoPassword(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean cambioPassword(ControlOperacionBD controlOperacionBD, String otp, String user_email, String password)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_otpp", otp);
			parametrosInOout.put("p_email", user_email);
			parametrosInOout.put("p_pass", password);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.cambioPassword(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean registroCambioPassword(ControlOperacionBD controlOperacionBD,PasswordChange passch)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_newpass", passch);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.registroCambioPassword(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public List<PasswordChange> getHistorialPasswordPorUser(ControlOperacionBD controlOperacionBD,String user_user)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		List<PasswordChange> historialPass = null;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getHistorialPasswordPorUser(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				historialPass = (List<PasswordChange>) parametrosInOout.get("p_tpassch");
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return historialPass;

	}
	
	
	
	public Boolean registroLoginUser(ControlOperacionBD controlOperacionBD,LoginUser loginUsr)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tlogin", loginUsr);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.registroLoginUser(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean registroLoginOk(ControlOperacionBD controlOperacionBD, InfoIPNavegadorSession infoLogin)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tiflgn", infoLogin);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.registroLoginOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public List<InfoIPNavegadorSession> getLoginOk(ControlOperacionBD controlOperacionBD,String user_user)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		List<InfoIPNavegadorSession> loginOk = null;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getLoginOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				loginOk = (List<InfoIPNavegadorSession>) parametrosInOout.get("p_tiflgn");
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return loginOk;

	}
	
	public Boolean eliminarLoginOk(ControlOperacionBD controlOperacionBD, String user_user)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			mapper.eliminarLoginOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean registroLoginErr(ControlOperacionBD controlOperacionBD, InfoIPNavegadorSession infoLogin)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tiflgn", infoLogin);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.registroLoginErr(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public List<InfoIPNavegadorSession> getLoginErr(ControlOperacionBD controlOperacionBD,String user_user)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		List<InfoIPNavegadorSession> loginErr = null;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getLoginErr(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				loginErr = (List<InfoIPNavegadorSession>) parametrosInOout.get("p_tiflgn");
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return loginErr;

	}
	
	public Boolean eliminarLoginErr(ControlOperacionBD controlOperacionBD, String user_user)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			mapper.eliminarLoginErr(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean registroIpOk(ControlOperacionBD controlOperacionBD, InfoIPNavegadorSession infoLogin)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		String pgcriptoAlgoritmo = appServicio.getPGCryptoAlgoritmo();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tiflgn", infoLogin);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_ALGORITMO, pgcriptoAlgoritmo);
			mapper.registroIpOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public List<InfoIPNavegadorSession> getIpOk(ControlOperacionBD controlOperacionBD,String browser, String ip)  throws ResponseFailedBD {


		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		List<InfoIPNavegadorSession> loginErr = null;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_browser", browser);
			parametrosInOout.put("p_ip", ip);
			
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			
			
			mapper.getIpOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				loginErr = (List<InfoIPNavegadorSession>) parametrosInOout.get("p_tiflgn");
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return loginErr;

	}
	
	public Boolean eliminarIpOk(ControlOperacionBD controlOperacionBD, String user_user)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			mapper.eliminarIpOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean registroUaOk(ControlOperacionBD controlOperacionBD, InfoIPNavegadorSession infoLogin)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_tiflgn", infoLogin);
			
			mapper.registroUaOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public List<InfoIPNavegadorSession> getUaOk(ControlOperacionBD controlOperacionBD,String user_user)  throws ResponseFailedBD {


		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		List<InfoIPNavegadorSession> loginErr = null;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			mapper.getUaOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				loginErr = (List<InfoIPNavegadorSession>) parametrosInOout.get("p_tiflgn");
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return loginErr;

	}
	
	public Boolean eliminarUaOk(ControlOperacionBD controlOperacionBD, String user_user)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			
			mapper.eliminarUaOk(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				sinErrores = false;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	

	public Boolean validarEmail(ControlOperacionBD controlOperacionBD,String email)  throws ResponseFailedBD {

		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		Boolean email_valido = false;
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			
			parametrosInOout.put("p_user_email", email);
			mapper.validarEmail(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				email_valido = !((Boolean) parametrosInOout.get("p_exist_email"));
				
			}
		}catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return email_valido;

	}
	
	public String cumplimientoUserNick(ControlOperacionBD controlOperacionBD,String user_nick)  throws ResponseFailedBD {

		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		String nick_valido = "";
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			
			parametrosInOout.put("p_user_nick", user_nick);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);

			mapper.cumplimientoUserNick(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				nick_valido = (String) parametrosInOout.get("p_nick_valid");
				
			}
		}catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return nick_valido;

	}

	public String cumplimientoUserPass(ControlOperacionBD controlOperacionBD,String user_name,String user_nick,String user_pass)  throws ResponseFailedBD {

		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		String pass_valido = "";
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			
			parametrosInOout.put("p_user_name", user_name);
			parametrosInOout.put("p_user_nick", user_nick);
			parametrosInOout.put("p_user_pass", user_pass);
		
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);

			mapper.cumplimientoUserPass(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				pass_valido = (String) parametrosInOout.get("p_pass_valid");
				
			}
		}catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
			
		} finally {
			//session.close();
		}
		
		
		return pass_valido;

	}
	

	public List<User> getUsuariosEmailNoConfirmado(ControlOperacionBD controlOperacionBD) throws ResponseFailedBD{

		
		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<User> usuarios = null;
		List<OtpUser> otpUsers = null;

		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.getUsuariosEmailNoConfirmado(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				usuarios = (List<User>) parametrosInOout.get("p_tuser");
				
				if (usuarios!=null && usuarios.size()>0) {
				
					otpUsers = (List<OtpUser>) parametrosInOout.get("p_totpu");
					
					if (otpUsers!=null && otpUsers.size()>0) {
						for (User usuario : usuarios) {
							for (OtpUser otpUser : otpUsers) {
								if (otpUser.getOtpu_user().equals(usuario.getUser_user())) {
									usuario.setOtpUser(otpUser);
								}
							}
						}
					}
				}
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
			

		
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return usuarios;

	}
	
	public Boolean activarUsuarioEmailNoConfirmado(ControlOperacionBD controlOperacionBD, String user_user)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			mapper.activarUsuarioEmailNoConfirmado(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}
	
	public Boolean eliminarUsuarioEmailNoConfirmado(ControlOperacionBD controlOperacionBD, String user_user)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		
		Boolean sinErrores = false;

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_user_user", user_user);
			mapper.eliminarUsuarioEmailNoConfirmado(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				sinErrores = true;
				
			} else {
				
				throw exceptionGeneralBD;
				
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}
		
		
		return sinErrores;

	}	

}

