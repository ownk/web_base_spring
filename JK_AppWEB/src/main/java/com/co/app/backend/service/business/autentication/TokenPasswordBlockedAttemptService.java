package com.co.app.backend.service.business.autentication;



import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class TokenPasswordBlockedAttemptService {


	private static LoadingCache<String, String> attemptsCache;

	public TokenPasswordBlockedAttemptService() {
		super();
		attemptsCache = CacheBuilder.newBuilder().build(new CacheLoader<String, String>() {
			@Override
			public String load(final String username) {
				return "";
			}
		});
	}

	public static void tokenSucceeded(final String username) {
		attemptsCache.invalidate(username);
	}

	public static boolean registerToken(final String username,String token) {
		try {
			attemptsCache.put(username, token); 
			return true ;
		} catch ( Exception e) {
			return false;
		}
	}

	public static boolean existToken(final String username ) {

		try {
			if (attemptsCache.get(username).isEmpty()) {
				return false ;
			}else {

				return true;
			}


		} catch ( Exception e) {
			return false;
		}

	}

	public static String getToken(final String username ) {

		try {

			String token = attemptsCache.get(username);

			if (token.isEmpty()) {
				return null ;
			}else {

				return token;
			}


		} catch ( Exception e) {
			return null;
		}

	}


}
