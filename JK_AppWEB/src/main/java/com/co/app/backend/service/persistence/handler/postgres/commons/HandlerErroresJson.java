package com.co.app.backend.service.persistence.handler.postgres.commons;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.co.app.model.dto.general.ErrorBD;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HandlerErroresJson extends BaseTypeHandler implements TypeHandler<Object>{
	@Override
	public Object getResult(ResultSet arg0, String arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
		List<ErrorBD> info= new ArrayList<ErrorBD>();
		String respuesta = null;
		try {
			respuesta = (String) cs.getString(columnIndex);
		} catch (Exception e) {
			respuesta = null;
		}
		if (respuesta != null) {
			
			try {
				
				ObjectMapper mapper = new ObjectMapper();
				
				ErrorBD[] infoArray = mapper.readValue(respuesta, ErrorBD[].class);
				
				if(infoArray!=null & infoArray.length>0) {
					for(ErrorBD registro : infoArray) {
						info.add(registro);
					}
				}
				
				
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				
			}
			
		}
        return info;       
	}

	@Override
	public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
		return;
	}

	@Override
	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
