package com.co.app.backend.service.business.user;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.EncryptDecryptDESService;
import com.co.app.backend.service.business.commons.MD5;
import com.co.app.backend.service.business.commons.StringUtils;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.user.TokenRecoverPassword;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class ForgetPasswordTokenGenerator {
	
	private Logger logger = LoggerFactory.getLogger(ForgetPasswordTokenGenerator.class);
	
	private static final String PARAMETRO_OTP 				= "t";
	private static final String PARAMETRO_EMAIL				= "e";
	private static final String PARAMETRO_FECHA_SOLICITUD  	= "fs";
	private static final String PARAMETRO_MINUTOS_VALIDO   	= "mv";
	private static final String SEPARADOR_VARIABLE			= "&";
	private static final String SEPARADOR_VALOR_VARIABLE	= "=";
	private static final String PATTERN_FECHA				= "yyyy-MM-dd HH:mm:ss";
	
	
	private static final String CLAVE_MD5					= "JH_GeneradorTokenOlvidoPassword";
	
	@Autowired
	EncryptDecryptDESService encripatadorBasicoServicio;
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	
	@Autowired
	AppService appService;
	
	
		
	public String obtenerTokenCifrado(TokenRecoverPassword tokenOlvidoPass)throws Exception {
		

	    
	    
	    String token = "";
		Boolean sinErrores = true;
		
		Integer minutosValidezToken = tokenOlvidoPass.getMinutosValidez();
		
		
	
		
		
		//Email
		if(tokenOlvidoPass.getUser_email()!=null){
			token = token+PARAMETRO_EMAIL+SEPARADOR_VALOR_VARIABLE+tokenOlvidoPass.getUser_email()+SEPARADOR_VARIABLE;
		}else{
			
			logger.error("ERROR_EMAIL_NL");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
				
		
		//OTP
		if(tokenOlvidoPass.getOtp()!=null){
			
			
			token = token+PARAMETRO_OTP+SEPARADOR_VALOR_VARIABLE+tokenOlvidoPass.getOtp()+SEPARADOR_VARIABLE;
				
		}else{
			logger.error("ERROR_OTP_NL");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
		
		//Fecha solicitud
		SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_FECHA);
		String fechaSolicitudString = dateFormat.format(tokenOlvidoPass.getFechaGeneracion());
		
		if(fechaSolicitudString!=null){
			token = token+PARAMETRO_FECHA_SOLICITUD+SEPARADOR_VALOR_VARIABLE+fechaSolicitudString+SEPARADOR_VARIABLE;
		}else{
			logger.error("ERROR_FSOL_NL");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
		
		
		//Minutos validez token
		if(minutosValidezToken.intValue()>0){
			token = token+PARAMETRO_MINUTOS_VALIDO+SEPARADOR_VALOR_VARIABLE+minutosValidezToken+SEPARADOR_VARIABLE;
		}else{
			logger.error("ERROR_MVALIDEZ_NV");
			throw new Exception(ConstantesGeneralesAPP.ERROR_INFO_NV);
		}
		
		
		if(sinErrores ){
			
			String tokenCifrado = encripatadorBasicoServicio.encrypt(token);
			
			String tokenCifradoEncode = StringUtils.encodeURIComponent(tokenCifrado);
			
			return tokenCifradoEncode;
		}else{
			return null;
		}
		
		
	}
	
	
	public String generarOTP() {
		String otp = RandomStringUtils.random(12, true, true);
		return otp;
	}
	
	public TokenRecoverPassword generarToken(String email, String otp) {
		
		
		Integer minutosMaximosValidez = 60;
		
		
		Date date = null;
		try {
			date = appService.getSysdate();
		} catch (RespuestaFallidaServicio e) {
			logger.error(e.getMensaje());
		}
	 	
	 	if(date==null) {
	 		date = new Date();
	 	}
	 	
	    
	 	TokenRecoverPassword token = new TokenRecoverPassword(email, otp, date, minutosMaximosValidez );

	    return token;
	    
		
	}
	
	private String generarMD5(String email, String otp) {
		
		return MD5.generarMD5(otp+email+CLAVE_MD5);
	}
	
		
	public TokenRecoverPassword obtenerTokenOlvidoPassword (String tokenCifrado){
		/*
		 * Se obtiene informacion de token cifrado
		 * ==============================================================
		 * Se crea mapa con valores obtenidos del token con la siguiente
		 * estructura
		 * 
		 * t=valor&u=valor&fs=valor&mv=valor
		 * 
		 */
		
		TokenRecoverPassword tokenOlvidoPass = new TokenRecoverPassword();
		
		Boolean sinErrores = true;
		String tokenDescrifrado = null;
		
		
		if(tokenCifrado!=null){
			tokenDescrifrado = encripatadorBasicoServicio.decrypt(tokenCifrado);
		}
		
		
		if(tokenDescrifrado!=null){
			StringTokenizer tokens = new StringTokenizer(tokenDescrifrado,SEPARADOR_VARIABLE);
			Map<String, String> parametros = new HashMap<String, String>();
			
			while(tokens.hasMoreElements()){
				String actualElement = tokens.nextToken();
				StringTokenizer stringTokenizer = new StringTokenizer(actualElement, SEPARADOR_VALOR_VARIABLE);
				String key = stringTokenizer.nextToken();
				String value = stringTokenizer.nextToken();
				parametros.put(key, value);
			}
			
			/*
			 * Se valida si se obtienen los valores esperados
			 * ==============================================================
			 *  
			 * TIPO_SOLICITUD;CELULAR;FECHA_SOLICITUD;MINUTOS_VALIDO;
			 * 
			 */
			
			String email = parametros.get(PARAMETRO_EMAIL);
						
			String otp = parametros.get(PARAMETRO_OTP);
			
			String fechaSolicitud = parametros.get(PARAMETRO_FECHA_SOLICITUD);
			
			String minutosValidades = parametros.get(PARAMETRO_MINUTOS_VALIDO);
			


			//Se valida el tipo de solicitud
			if(email!=null){
				
				tokenOlvidoPass.setUser_email(email);
				
			}else{
				sinErrores = false;
				
			}
			
			
			
			//Se valida el tipo de solicitud
			if(otp!=null){
				
				tokenOlvidoPass.setOtp(otp);
				
			}else{
				sinErrores = false;
				
			}
			
			

			
			
			
			//Se valida fecha solicitud
			if(fechaSolicitud!=null){
				
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PATTERN_FECHA);
				try {
					Date dateFechaSolicitud = simpleDateFormat.parse(fechaSolicitud);
					
					tokenOlvidoPass.setFechaGeneracion(dateFechaSolicitud);
					
				} catch (Exception e) {
					sinErrores = false;
				}
			}else{
				sinErrores = false;
			}
			
			//Se valida minutos;
			if(minutosValidades!=null){
				
				Integer minutos = Integer.parseInt(minutosValidades);
				
				tokenOlvidoPass.setMinutosValidez(minutos);
				
				
			}else{
				sinErrores = false;
			}
			
			
			
		}else{
			sinErrores = false;
		}
		
		
		
		if(sinErrores){
			return tokenOlvidoPass;
		}else{
			return null;
		}
		
		
	}
	
	
	
	
}
