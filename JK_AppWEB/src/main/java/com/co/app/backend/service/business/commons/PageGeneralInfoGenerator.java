package com.co.app.backend.service.business.commons;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.session.Menu;
import com.co.app.model.dto.session.Modulo;
import com.co.app.model.dto.session.Servicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.session.URLServicio;

@Component
public class PageGeneralInfoGenerator{
	
	private static final String APP_UTIL_INFO = "APP_UTIL_INFO";
	private static final String APP_SESSION_INFO = "APP_SESSION_INFO";
	private static final String APP_MENSAJES_GENERALES = "APP_MENSAJES_GENERALES";
	private static final String APP_MENSAJES_POR_PAGINA = "APP_MENSAJES_POR_PAGINA";
	private static final String APP_USER_ALL_MENUS = "APP_USER_ALL_MENUS";
	private static final String APP_CLIENT = "APP_CLIENT";
	private static final String APP_LOCALE_LENGUAGE = "APP_LOCALE_LENGUAGE";
	private static final String APP_LOCALE_COUNTRY = "APP_LOCALE_COUNTRY";
	private static final String APP_BROWSER_STORAGE_LANG = "APP_BROWSER_STORAGE_LANG";
	private static final String APP_LANG_PARAMETER = "APP_LANG_PARAMETER";
	private static final String APP_SYSDATE_STRING_FORMAT = "APP_SYSDATE_STRING_FORMAT";
	private static final String APP_SYSDATE = "APP_SYSDATE";
	public static final String DATE_STRING_FORMAT = "EEEEE, MMMMM dd, yyyy HH:mm:ss";
	
	
	 public static final String[] LANGS_OK = {
	    		"es_CO",
	    		"en_US",
	    		"pt_BR",
	    };
	
	
	
	Logger logger = LoggerFactory.getLogger(PageGeneralInfoGenerator.class);
	
	
	public void generarInfoSessionUsuario(ModelAndView modelAndView, SessionAppUsuario sessionAppUsuario) {
		
		modelAndView.addObject(APP_SESSION_INFO, sessionAppUsuario);
		
		List<MensajeApp> mensajesPorPagina = sessionAppUsuario.getMensajesAPP(sessionAppUsuario.getUrlActual(), true);
		
		
		modelAndView.addObject(APP_MENSAJES_POR_PAGINA, mensajesPorPagina);
		
		getUserMenu(modelAndView, sessionAppUsuario);
		
		
		getClient(modelAndView, sessionAppUsuario);
		
		
		generarLang(modelAndView, sessionAppUsuario.getLocale() );
		
		generarSysdate(modelAndView, sessionAppUsuario.getLocale(), sessionAppUsuario.getSysdate());
		
		
	}

	
	
	public void generarMensajesGeneralesAPP(ModelAndView modelAndView, List<MensajeApp> listaMensajes) {
		
		modelAndView.addObject(APP_MENSAJES_GENERALES, listaMensajes);
		
	}
	
	public void generarLang(ModelAndView modelAndView, Locale locale) {
		
		try {
			
			
			if(locale!=null) {
				
				String lang = locale.getLanguage()+"_"+locale.getCountry();
				
				Boolean isLangValid = isLangValid(lang);
				
				if(!isLangValid) {
					
					
					setDefaultLocale();
				
				}
				
				
			}else {
				
				setDefaultLocale();
			
			}
			
			
			modelAndView.addObject(APP_LOCALE_LENGUAGE, locale.getLanguage());
			modelAndView.addObject(APP_LOCALE_COUNTRY, locale.getCountry());
			modelAndView.addObject(APP_BROWSER_STORAGE_LANG, APP_BROWSER_STORAGE_LANG);
			modelAndView.addObject(APP_LANG_PARAMETER, ConstantesGeneralesAPP.WEB_LANG_PARAMETER);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		
		
	}
	
	
	public void generarSysdate(ModelAndView modelAndView, Locale locale, Date date) {
		
		try {
			
			String dateFormat = "";
			
			if(locale!=null) {
				
				String lang = locale.getLanguage()+"_"+locale.getCountry();
				
				Boolean isLangValid = isLangValid(lang);
				
				if(!isLangValid) {
					setDefaultLocale();
				
				}
				
					
			}else {
				
				setDefaultLocale();
				
			}	
			
	
			SimpleDateFormat simpleDateFormat =new SimpleDateFormat(DATE_STRING_FORMAT, locale);
			dateFormat = simpleDateFormat.format(date);
			
			modelAndView.addObject(APP_SYSDATE_STRING_FORMAT, dateFormat );
			modelAndView.addObject(APP_SYSDATE, date );
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		
		
	}
	
	
	
	private void getUserMenu(ModelAndView modelAndView, SessionAppUsuario sessionAppUsuario) {
		List<Modulo> menuUsuario = sessionAppUsuario.getMenuUsuario();
		
		for (Modulo modulo : menuUsuario) {
			List<Menu> menus = modulo.getMenus();
			for (Menu menu : menus) {
				
				Boolean isMenuSelected= false;
				List<Servicio> servicios = menu.getServices();
				for (Servicio servicio : servicios) {
					
					Boolean isServiceSelected = false;
					
					List<URLServicio> urls = servicio.getListUrls();
					
					
					if(urls!=null) {
						for (URLServicio url : urls) {
							if(url.getSurl_url().equals(sessionAppUsuario.getUrlActual())) {
								isServiceSelected = true;
								isMenuSelected = true;
								
							}
						}
					}else {
						if(servicio.getSurl_url().equals(sessionAppUsuario.getUrlActual())) {
							isServiceSelected = true;
							isMenuSelected = true;
							
						}
					}
					
					
					
					servicio.setIsServiceSelected(isServiceSelected);
						
				}
				
				
				menu.setIsMenuSelected(isMenuSelected);
				
			}
			
		}
		
		modelAndView.addObject(APP_USER_ALL_MENUS, menuUsuario);
		
	}
	
	private void getClient(ModelAndView modelAndView, SessionAppUsuario sessionAppUsuario) {
		
		try {
			Client client = sessionAppUsuario.getCliente();
			
			if(client!=null) {
				
				modelAndView.addObject(APP_CLIENT, client);
			}
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		
		
	}
	
	
	public Boolean isLangValid(String lang) {
		
		if(lang!=null && !StringUtils.isEmpty(lang)) {
			
			boolean contains = Arrays.stream(LANGS_OK).anyMatch(lang::equals);
			return contains;
		}else {
			return true;
		}
		
		
	}
	
	
	private void setDefaultLocale() {
		Locale locale = Locale.forLanguageTag("es-CO");
		Locale.setDefault(locale);
		
	}





}