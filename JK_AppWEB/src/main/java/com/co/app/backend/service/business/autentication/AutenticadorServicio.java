package com.co.app.backend.service.business.autentication;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.business.commons.StringUtils;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.Modulo;
import com.co.app.model.dto.session.Servicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.User;

@Component
public class AutenticadorServicio {
	
	
	private Logger logger = LoggerFactory.getLogger(AutenticadorServicio.class);
	
	@Autowired 
	private IPNavegadorService ipNavegadorServicio;
	
	@Autowired
	private GeneradorSessionApp generadorSession;
	
	@Autowired
	private UserService usuarioServicio;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private ResponseFailedServiceGenerator generadorExcepcionServicio;
	
	
	
	
	public User isUsuarioValido(ControlOperacionServicio controlOperacion, String login, String passSinCifrar) throws RespuestaFallidaServicio {

		
		if(controlOperacion!=null) {
			
			User usuario = usuarioServicio.getUsuarioPorNick(controlOperacion, login);
			
			if(passSinCifrar!=null && isPassWordMatch(passSinCifrar, usuario.getUser_pass())) {
				return usuario;
			}else {
				throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_SESSION_USER_PASS_NV);
			}
				
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
			
		}
		

	}

	

	public Boolean isURLValidaPorUsuario(ControlOperacionServicio controlOperacion, String user_user, String url) throws RespuestaFallidaServicio {

		
		if(controlOperacion!=null) {
			List<Servicio> servicios = usuarioServicio.getServiciosUsuarioPorURL(controlOperacion, user_user, url);
			
			if(servicios!=null && servicios.size()>0) {
				return true;
			}else {
				return false;
			}
				
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
			
		}


	}
	
	
	public Boolean isAccesoPrivadoValido(HttpServletRequest request) throws RespuestaFallidaServicio {

		String url = request.getRequestURI().substring(request.getContextPath().length() + 1);
		
		return isAccesoPrivadoValidoURL(request, url);

	}
	
	
	public Boolean isAccesoPrivadoValidoURL(HttpServletRequest request, String urlWithOutParameters) throws RespuestaFallidaServicio {

		Boolean isAccesoValido = false;
		SessionAppUsuario sessionAppUsuario = null;

		// Se valida si la session app de usuario es correcta
		if (generadorSession != null) {
			sessionAppUsuario = generadorSession.getSessionAppUsuario(request);
			
			ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario.getUsuario().getUser_nick(), sessionAppUsuario.getIp(), sessionAppUsuario.getNavegador());
			

			// Si la session es valida, se verifica el acceso a la url de la peticion
			if (sessionAppUsuario != null) {
				
				isAccesoValido = isURLValidaPorUsuario(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), urlWithOutParameters );
				
				//isAccesoValido = true;
				
			}
		} else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_SESSION_NL); 

		}

		return isAccesoValido;

	}

	
	public SessionAppUsuario iniciarSessionUsuario(Locale locale, HttpServletRequest request, String login, String pass) throws RespuestaFallidaServicio {
		
		// Se valida que el login y password sean correctos
		ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio("APP", ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
		
		
		User usuario = isUsuarioValido(controlOperacionServicio, login, pass);


		SessionAppUsuario sessionAppUsuario = null;

		logger.info("Iniciando autenticacion de usuario");

		if (!StringUtils.esVacio(login) && !StringUtils.esVacio(pass) && usuario!=null ) {

			if (generadorSession != null) {
				
				List<Modulo> modulos = usuarioServicio.getMenuByUser(controlOperacionServicio, usuario.getUser_user());
				

				sessionAppUsuario = this.generadorSession.iniciarSession(locale,request, usuario,modulos );
				
				
				
			} else {
				logger.error("NO EXISTE GENERADOR DE SESSION APP REGISTRADO EN AUTENTICADOR CONTROLLER");
				throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_SESSION_NL);
			}
		} else {
			logger.info("Informacion de usuario incorrecta");
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_SESSION_USER_NV);
		}

		return sessionAppUsuario;
	}

	

	public SessionAppUsuario getSessionAppUsuario(HttpServletRequest request) {
		return generadorSession.getSessionAppUsuario(request);
	}
	
	
	

	public void cerrarSession(HttpServletRequest request) {
		generadorSession.cerrarSession(request);
		
		
		
		
	}
	
	
	public String cifrarInfo(String infoPorCifrar) {
		
		return bCryptPasswordEncoder.encode(infoPorCifrar);
	}
	
	
	public Boolean isPassWordMatch(String passSinCifrar, String passCifrado) {
		
		return bCryptPasswordEncoder.matches(passSinCifrar, passCifrado);
	}
	

	public String getURLWithOutParameters(HttpServletRequest request) {
		
		String url = request.getRequestURI().substring(request.getContextPath().length() + 1);
		
    	int posFInal = url.lastIndexOf("/");
    	
    	url = url.substring(0,posFInal);
    	
    	return url;
		
	}
	
	

}
