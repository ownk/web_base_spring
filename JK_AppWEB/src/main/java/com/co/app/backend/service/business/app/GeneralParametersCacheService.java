package com.co.app.backend.service.business.app;

import java.util.List;

import org.springframework.stereotype.Service;

import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class GeneralParametersCacheService {

	private static LoadingCache<String, ParametroConfiguracionGeneral> parametersCache;

	public GeneralParametersCacheService() {
		super();
		parametersCache = CacheBuilder.newBuilder().build(new CacheLoader<String, ParametroConfiguracionGeneral>() {
			@Override
			public ParametroConfiguracionGeneral load(final String key) {
				return null;
			}
		});
	}


	public static boolean registerParameters(List<ParametroConfiguracionGeneral> parameters) {
		try {
			
			
			for (ParametroConfiguracionGeneral parametroConfiguracionGeneral : parameters) {
				
				registerParameter(parametroConfiguracionGeneral);
			 
			}
			
			return true ;
		} catch ( Exception e) {
			return false;
		}
	}
	
	
	public static boolean registerParameter(ParametroConfiguracionGeneral parameter) {
		try {
			
			String paramaterKey = parameter.getCnfg_cnfg().toUpperCase();
			parametersCache.put(paramaterKey, parameter); 
			
			return true ;
		} catch ( Exception e) {
			return false;
		}
	}

	
	public static ParametroConfiguracionGeneral getParameter(final String cnfg_cnfg ) {

		try {
			
			
			if(cnfg_cnfg!=null) {
				String parameterKey = cnfg_cnfg.toUpperCase();
						
				ParametroConfiguracionGeneral parameter= parametersCache.get(parameterKey);
				
				return parameter;
						
				
			}{
				return null;
			}
			

		} catch ( Exception e) {
			return null;
		}
		
	}



}
