package com.co.app.backend.service.business.game;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.controller.game.GameControllerDB;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Question;
import com.co.app.model.dto.game.Score;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

@Component
public class GameServicio {
	
	Logger logger = LoggerFactory.getLogger(GameServicio.class);
	
	@Autowired
	GameControllerDB controllerDB;
	
	@Autowired
	PlayerServicio playerServicio;
	
	@Autowired
	ResponseFailedServiceGenerator generadorExcepcionServicio;
	
	

	public List<Avatar> obtenerAvatars(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {
		
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return  controllerDB.obtenerAvatars(controlOperacionBD);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<Pet> obtenerPets(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {
		
				
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.obtenerPets(controlOperacionBD);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public List<Ship> obtenerShips(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {
			
		if(controlOperacion!=null) {
				
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				
			try {	
				return controllerDB.obtenerShips(controlOperacionBD);
				
					
			} catch (ResponseFailedBD e) {
					
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
					
			}	
				
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	
	
	
	public List<World> obtenerMundos(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.obtenerMundos(controlOperacionBD);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public Game obtenerGamePorId(ControlOperacionServicio controlOperacion, String idGame) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.obtenerGamePorId(controlOperacionBD,idGame);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public World obtenerMundoPorId(ControlOperacionServicio controlOperacion, String idMundo) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.obtenerMundoPorId(controlOperacionBD,idMundo);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	

	public List<Player> obtenerRankingPlayers(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {
		
		List<Player> jugadores = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				 jugadores = controllerDB.obtenerRankingGlobalPlayers(controlOperacionBD);
				 
				 if(jugadores!=null) {
					 for (Player player : jugadores) {
							playerServicio.completeInfoPlayer(controlOperacion, player);
					 }
				 }
				 
				 
				return jugadores;
				
			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<Player> obtenerRankingGlobalPorFecha(ControlOperacionServicio controlOperacion, Date fechaInicio, Date fechaFin) throws RespuestaFallidaServicio  {
		
		List<Player> jugadores = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				 jugadores = controllerDB.obtenerRankingGlobalPorFecha(controlOperacionBD,fechaInicio,fechaFin);
				 
				 if(jugadores!=null) {
					 for (Player player : jugadores) {
							playerServicio.completeInfoPlayer(controlOperacion, player);
					 }
				 }
				 
				 
				return jugadores;
				
			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<Player> obtenerRankingEmpresaPlayers(ControlOperacionServicio controlOperacion, String idClient) throws RespuestaFallidaServicio  {
		
		List<Player> jugadores = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				 jugadores = controllerDB.obtenerRankingEmpresaPlayers(controlOperacionBD,idClient);
				 
				 if(jugadores!=null) {
					 for (Player player : jugadores) {
							playerServicio.completeInfoPlayer(controlOperacion, player);
					 }
				 }
				 
				 return jugadores;
				
			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public List<Player> obtenerRankingEmpresaPorFecha(ControlOperacionServicio controlOperacion, Date fechaInicio, Date fechaFin, String idClient) throws RespuestaFallidaServicio  {
		
		List<Player> jugadores = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				 jugadores = controllerDB.obtenerRankingEmpresaPorFecha(controlOperacionBD,fechaInicio,fechaFin,idClient);
				 
				 if(jugadores!=null) {
					 for (Player player : jugadores) {
							playerServicio.completeInfoPlayer(controlOperacion, player);
					 }
				 }
				 
				 
				return jugadores;
				
			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public Player getPlayerbyNick(ControlOperacionServicio controlOperacion, String playerNick) throws RespuestaFallidaServicio  {
		
		List<Player> jugadores = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				 jugadores = controllerDB.getPlayerbyNick(controlOperacionBD, playerNick);
				 
				 if(jugadores!=null && jugadores.size()>0) {
					 return jugadores.get(0);
				 }else {
					 return null;
				 }
				 
				
				
			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	
	
	public List<Player> obtenerPosicionRankingEmpresaPl(ControlOperacionServicio controlOperacion, String idClient, String idPlayer) throws RespuestaFallidaServicio  {
		
		List<Player> jugadores = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				 jugadores = controllerDB.obtenerPosicionRankingEmpresaPl(controlOperacionBD,idClient,idPlayer);
				return jugadores;
				
			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<Question> getQuestions(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio {
		
		List<Question> questions= null;
		
		if(controlOperacion!=null) {
			
			try {
				
				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				questions = controllerDB.getQuestions(controlOperacionBD);
			} catch (ResponseFailedBD e) {
				
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}
			
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return questions;
	}
	
	public List<Game> obtenerJuegos(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.obtenerJuegos(controlOperacionBD);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<LearningObjective> getLearningObjectivesByGame(ControlOperacionServicio controlOperacion,String idGame) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.getLearningObjectivesByGame(controlOperacionBD,idGame);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<World> obtenerMundosPorGame(ControlOperacionServicio controlOperacion, String idGame) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.obtenerMundosPorGame(controlOperacionBD,idGame);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	public List<Game> obtenerJuegosPorMundo(ControlOperacionServicio controlOperacion, String idWorld) throws RespuestaFallidaServicio  {
		
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				return controllerDB.obtenerJuegosPorMundo(controlOperacionBD,idWorld);
				
				
			} catch (ResponseFailedBD e) {
				
				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	
}
