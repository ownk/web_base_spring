package com.co.app.backend.service.persistence.handler.postgres.client;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.co.app.backend.service.persistence.handler.postgres.commons.BaseTypeHandler;
import com.co.app.model.dto.client.Client;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class HandlerClientJson extends BaseTypeHandler implements TypeHandler<Object> {
	
	Logger logger = LoggerFactory.getLogger(HandlerClientJson.class);

	@Override
	public Object getResult(ResultSet arg0, String arg1) throws SQLException {
		try {
			
			Array value = arg0.getArray(arg1);
			logger.info(""+value);
		} catch (Exception e) {
			Object value = arg0.getObject(arg1);
			logger.info(""+value);
		}
		return null;
	}

	@Override
	public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
		List<Client> clienteDto = new ArrayList<>();
		
		String respuesta = null;
		try {
			respuesta = (String) cs.getString(columnIndex);
		} catch (Exception e) {
			respuesta = null;
		}
		if (respuesta != null) {
			
			try {
				ObjectMapper mapper = new ObjectMapper();
				Client[] registrosArreglo = mapper.readValue(respuesta, Client[].class);

				if (registrosArreglo != null & registrosArreglo.length > 0) {
					for(Client registro : registrosArreglo) {
						clienteDto.add(registro);
					}
				}

			} catch (Throwable e) {
				logger.error(e.getMessage());
				
			}
			
		}
		return clienteDto;
	}
	
	@Override
	public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType arg3) throws SQLException {
		try {
			
			
			Client objeto = (Client) parameter;
			
			
			if (objeto != null) {
				
				List<Client> listObjetos = new ArrayList<>();
				
				listObjetos.add(objeto);
				
				PGobject pGobject = new PGobject();
				pGobject.setType("text");
				
				//---
				Gson gson = new Gson();
				String jsonResponse = gson.toJson(listObjetos);
				
				//---
				pGobject.setValue(jsonResponse);
				
				ps.setObject(i, pGobject, Types.OTHER);
			}else {
				ps.setObject(i, null, Types.OTHER);
			}
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
