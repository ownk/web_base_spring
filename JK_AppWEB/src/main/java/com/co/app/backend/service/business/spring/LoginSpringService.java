package com.co.app.backend.service.business.spring;

import static java.util.Collections.emptyList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.autentication.LoginUsernameAttemptService;
import com.co.app.backend.service.business.captcha.RecaptchaService;
import com.co.app.backend.service.business.captcha.RecaptchaService.RecaptchaResult;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.backend.service.business.user.ValidatorUniqueSessionService;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.UserDetailsSpring;
import com.co.app.model.dto.user.UserSpring;
import com.co.app.model.dto.user.UserSpringGrant;

@Service
public class LoginSpringService implements UserDetailsService, AuthenticationSuccessHandler, LogoutHandler {



	private Logger logger = LoggerFactory.getLogger(LoginSpringService.class);

	@Autowired
	private UserService usuarioServicio;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	IPNavegadorService ipNavegadorServicio;

	@Autowired
	RequestParameterService requestParameterService;

	@Autowired
	HttpServletRequest request;

	@Autowired
	private LoginUsernameAttemptService loginAttemptService;

	@Autowired
	RecaptchaService recaptchaServicio;

	@Autowired
	ValidatorUniqueSessionService validatorUniqueSessionService;




	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();





	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		String recaptchaResponse = request.getParameter("g-recaptcha-response");
		final String idForIPValidation = ipNavegadorServicio.getIDForIPValidation();

		
		Boolean isUserFounded = false;

		if ( recaptchaResponse != null && loginAttemptService.isBlocked(idForIPValidation)) {

			// I didn't use remoteip here, because it is optional,
			// Reference: https://developers.google.com/recaptcha/docs/verify
			RecaptchaResult result = recaptchaServicio.getResult("", recaptchaResponse);

			if (!result.isSuccess()) {

				throw new RuntimeException("blocked");
			}else {
				
				loginAttemptService.loginSucceeded(idForIPValidation);
			}
			
		}



		UserSpring userSpring = null;


		/*
		 * =======================================================
		 * DECRYPT PARAMETERS
		 * =======================================================
		 * 
		 * -------------------------------------------------------
		 */

		String usernameDecrypt = RequestParameterService.decryptText(username);

		if(usernameDecrypt!=null) {

			/*
			 * =======================================================
			 * VALIDACION EN DOS FASES- FASE 1 Consulta NombreUsuario
			 * =======================================================
			 * Se valida que el nombre de usuario sea valido para el 
			 * sistema.  Para realizar la validacion de nombre de 
			 * usuario a traves de SPRING se asume que la clave es el 
			 * mismo nombre de usuario cifrado
			 * -------------------------------------------------------
			 */

			ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio("APP", idForIPValidation, "NA");


			com.co.app.model.dto.user.User usuario;
			try {
				usuario = usuarioServicio.getUsuarioPorNick(controlOperacionServicio, usernameDecrypt);
				if(usuario!=null) {

					//Se crea un usuario Spring
					userSpring = getUserSpring(usuario);

					//Se cifra el nombre de usuario
					userSpring.setPassword(bCryptPasswordEncoder.encode(username));

					//Se agrega nuevo rol
					List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
					authorities.remove(new UserSpringGrant(UserSpringGrant.GRANT_USER_AUTENTICATED));
					authorities.add(new UserSpringGrant(UserSpringGrant.GRANT_USERNAME_VALID));
					userSpring.setRoles(authorities);

					new User(userSpring.getUserName(), userSpring.getPassword(), emptyList());
					return new UserDetailsSpring(userSpring);

				}else {
					isUserFounded = false;
				}


			} catch (RespuestaFallidaServicio e) {
				logger.debug(e.getMensaje());
				isUserFounded = false;
			}

		}else {
			isUserFounded = false;
		}
		
		
		if(!isUserFounded) {
			
			logger.info("Login encrypted: "+username+" "+ConstantesGeneralesAPP.ERROR_SPRING_USERNAME_NE);
			throw new UsernameNotFoundException(ConstantesGeneralesAPP.ERROR_SPRING_USERNAME_NE);
		}
		
		return null;


	}


	@Override
	public void onAuthenticationSuccess
	(
			final HttpServletRequest request, 
			final HttpServletResponse response,
			final Authentication authentication
			) throws IOException {


		/*
		 * =======================================================
		 * VALIDACION EN DOS FASES- FASE 1 NombreUsuario Valido
		 * =======================================================
		 * Luego de la validacion exitosa de Spring se debe
		 * redireccionar a nueva pagina para solicitud 
		 * de password. Para esto se valida que exista rol 
		 * asignaado en la validacion de nombre de usuario
		 * -------------------------------------------------------
		 */


		String targetUrl = ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		final String idForIPValidation = ipNavegadorServicio.getIDForIPValidation();
		boolean esUsuarioRequierePass = false;

		//Se valida rol asignado
		final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (final GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().equals(UserSpringGrant.GRANT_USERNAME_VALID)) {
				esUsuarioRequierePass = true;
				break;
			}
		}


		if (esUsuarioRequierePass) {
			targetUrl = ConstantesGeneralesAPP.URL_LOGIN_PASSWORD;
			loginAttemptService.loginSucceeded(idForIPValidation);
		} else {
			throw new IllegalStateException();
		}

		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);

		clearAuthenticationAttributes(request);

	}


	public SessionAppUsuario iniciarSessionUsuario(Locale locale, HttpServletRequest request, HttpServletResponse response, String username, String pass) {

		SessionAppUsuario sessionUsuarioValida;



		try {
			sessionUsuarioValida = autenticadorServicio.iniciarSessionUsuario(locale,request, username, pass);

			if(sessionUsuarioValida!=null) {
				//Se crea rol de SPRING para usuario autenticado
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(auth.getAuthorities());
				authorities.remove(new UserSpringGrant(UserSpringGrant.GRANT_USERNAME_VALID));
				authorities.add(new UserSpringGrant(UserSpringGrant.GRANT_USER_AUTENTICATED));
				Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), authorities);
				SecurityContextHolder.getContext().setAuthentication(newAuth);

				return sessionUsuarioValida;

			}else {
				return null;
			}

		} catch (RespuestaFallidaServicio e) {
			return null;
		}

	}


	public Boolean invalidarSessionUsuario(HttpServletRequest request) {

		//Se crea rol de SPRING para usuario autenticado
		try {
			
			
		
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(auth.getAuthorities());
			authorities.remove(new UserSpringGrant(UserSpringGrant.GRANT_USERNAME_VALID));
			authorities.remove(new UserSpringGrant(UserSpringGrant.GRANT_USER_AUTENTICATED));
			Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), authorities);
			SecurityContextHolder.getContext().setAuthentication(newAuth);
	
			clearAuthenticationAttributes(request);
	
			autenticadorServicio.cerrarSession(request);
			
			
			return true;
		
		}catch (Exception e) {
			logger.debug(e.getMessage());
			
			return false;
		}


	}
	
	
	public Boolean reiniciarValidatorUniqueSession(HttpServletRequest request) {

		//Se crea rol de SPRING para usuario autenticado
		try {
			
			SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
					//Se inicializa informacion de validador de session unica
			if(sessionAppUsuario!=null) {
				try {
					validatorUniqueSessionService.resetValidadorIPNavegador(sessionAppUsuario.getUsuario().getUser_user(), request);
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
				
			}
			
			return true;
		
		}catch (Exception e) {
			logger.debug(e.getMessage());
			
			return false;
		}


	}

	/**
	 * Removes temporary authentication-related data which may have been stored in
	 * the session during the authentication process.
	 */
	private final void clearAuthenticationAttributes(final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);

		if (session == null) {
			return;
		}

		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}


	private UserSpring getUserSpring(com.co.app.model.dto.user.User usuario) {

		UserSpring userSpring = null;

		if(usuario!=null) {

			userSpring = new UserSpring();

			if("ACT".equals(usuario.getUser_state())) {
				userSpring.setActive(1);
			}else {
				userSpring.setActive(0);
			}

			userSpring.setId(1);
			userSpring.setUserName(usuario.getUser_nick());

		}

		return userSpring;

	}




	public void setRedirectStrategy(final RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}


	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		
		//Se reinicia validador de unica session por usuario
		reiniciarValidatorUniqueSession(request);
		
		//Se invalida la session actual
		invalidarSessionUsuario(request);
		
		


	}




}
