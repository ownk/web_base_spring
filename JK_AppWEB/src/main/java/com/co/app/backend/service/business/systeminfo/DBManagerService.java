/*
 * MIT License 
 * 
 * Copyright (c) 2018 Ownk
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 */

package com.co.app.backend.service.business.systeminfo;
import java.util.ArrayList;
import java.util.List;

import com.co.app.model.dto.systeminfo.TestConexionMyBatis;

/**
*
* <h1>DBManagerService</h1>
*
* Servicio de manager de mybatis para obtener propiedades de conexion y hacer test de conexiones.
*
* @author Master_Zen (Ownk) 
* @version 1.0
* 
*/
public class DBManagerService {

	public List<TestConexionMyBatis> realizarTestConexiones() {
		
		TestConexionMyBatis  testConexionesMyBatis = new TestConexionMyBatis(); 
		List<TestConexionMyBatis> listaTest = new ArrayList<TestConexionMyBatis>();
		listaTest.add(testConexionesMyBatis);
		
		return listaTest;
	}

}
