package com.co.app.backend.service.business.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.user.EncryptEncodeUserRegisterToken;
import com.co.app.backend.service.business.user.UserRegisterTokenGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.controller.player.PlayerControllerDB;
import com.co.app.frontend.controller.publico.player.register.PlayerRegisterController;
import com.co.app.model.dto.account.Account;
import com.co.app.model.dto.client.PlayerExternalIDResponse;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.game.AnswerQuestion;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Score;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.user.TokenUserRegister;
import com.co.app.model.dto.user.User;

@Component
public class PlayerServicio {

	Logger logger = LoggerFactory.getLogger(PlayerServicio.class);
	
	
	public static final String PARAMETRO_CONFIRMACION_REGISTRO = "confID";
	public static final String PARAMETRO_ENCRYPT_SALT = "e";
	public static final String PARAMETRO_ENCRYPT_IV = "s";

	

	@Autowired
	PlayerControllerDB controllerDB;

	@Autowired
	ResponseFailedServiceGenerator generadorExcepcionServicio;

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	UserRegisterTokenGenerator generadorTokenRegistroUsuario;
	
	@Autowired
	AppService appServicio;
	
	@Autowired
	I18nService i18nservice;
	
	@Autowired
	MailServicio mailServicio;


	public List<TipoDocumento> getTiposDocumento(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {

		List<TipoDocumento> tiposDocumento= null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

				tiposDocumento = controllerDB.getTiposDocumento(controlOperacionBD);
			} catch (ResponseFailedBD e) {


				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return tiposDocumento;
	}

	public Boolean registrarJugador(ControlOperacionServicio controlOperacion, User usuarioNuevo, String codigoAcceso, String otp, Player player) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;


		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	



				//Se normaliza la informacion 

				usuarioNuevo.setUser_email(usuarioNuevo.getUser_email().toLowerCase());
				usuarioNuevo.setUser_nick(usuarioNuevo.getUser_nick().toLowerCase());

				//Se genera clave cifrada para usuario

				String passCifrado = autenticadorServicio.cifrarInfo(usuarioNuevo.getUser_pass());
				usuarioNuevo.setUser_pass(passCifrado);
				usuarioNuevo.setUser_name(usuarioNuevo.getUser_name());
				sinErrores = controllerDB.registrarJugador(controlOperacionBD,  usuarioNuevo, codigoAcceso, otp, player);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean validarRegistro(ControlOperacionServicio controlOperacion, String idUsuario,String otp, String codigo_acceso) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.validarRegistro(controlOperacionBD,idUsuario,otp,codigo_acceso);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}



	public Boolean validarLog(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.validarLog(controlOperacionBD);


			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public List<Player> getPlayers(ControlOperacionServicio controlOperacion, String idUser) throws RespuestaFallidaServicio  {

		List<Player> jugadores = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				jugadores = controllerDB.getPlayersByUser(controlOperacionBD, idUser);
				return jugadores;

			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}


	public Player getPlayerPpalByUser(ControlOperacionServicio controlOperacion, String idUser, Boolean completeInfoPlayer) throws RespuestaFallidaServicio  {



		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Player > players = controllerDB.getPlayersByUser(controlOperacionBD, idUser);

				if(players!=null && players.size()>0) {

					Player player = players.get(0);


					if(completeInfoPlayer) {
						completeInfoPlayer(controlOperacion, player);

					}

					return player ;
				}else {
					return null;
				}


			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			} catch (RespuestaFallidaServicio e) {

				throw e;
			}

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public Player getPlayerByID(ControlOperacionServicio controlOperacion, String idPlayer, Boolean completeInfoPlayer) throws RespuestaFallidaServicio  {



		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Player > players = controllerDB.getPlayerByID(controlOperacionBD, idPlayer);

				if(players!=null && players.size()>0) {

					Player player = players.get(0);


					if(completeInfoPlayer) {
						completeInfoPlayer(controlOperacion, player);

					}

					return player ;
				}else {
					return null;
				}


			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			} catch (RespuestaFallidaServicio e) {

				throw e;
			}

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public void completeInfoPlayer(ControlOperacionServicio controlOperacion, Player player )throws RespuestaFallidaServicio{
		if(controlOperacion!=null) {



			try {
				Avatar avatar = obtenerAvatarPlayer(controlOperacion, player.getPlayer_player());
				Pet pet = obtenerPetPlayer(controlOperacion, player.getPlayer_player());
				Ship ship = obtenerShipPlayer(controlOperacion, player.getPlayer_player());

				player.setAvatar(avatar);
				player.setPet(pet);
				player.setShip(ship);
			} catch (RespuestaFallidaServicio e) {

				throw e;
			}




		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

	}



	public Avatar obtenerAvatarPlayer(ControlOperacionServicio controlOperacion, String idPlayer) throws RespuestaFallidaServicio  {

		Avatar avatar = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				avatar = controllerDB.obtenerAvatarPlayer(controlOperacionBD,idPlayer);

				return avatar;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public Pet obtenerPetPlayer(ControlOperacionServicio controlOperacion, String idPlayer) throws RespuestaFallidaServicio  {

		Pet pet = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				pet = controllerDB.obtenerPetPlayer(controlOperacionBD,idPlayer);
				return pet;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public Ship obtenerShipPlayer(ControlOperacionServicio controlOperacion, String idPlayer) throws RespuestaFallidaServicio  {

		Ship ship = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				ship = controllerDB.obtenerShipPlayer(controlOperacionBD,idPlayer);
				return ship;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}



	public Boolean asignarAvatar(ControlOperacionServicio controlOperacion, String idPlayer,String idAvatar) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.asignarAvatar(controlOperacionBD,idPlayer,idAvatar);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean asignarPet(ControlOperacionServicio controlOperacion, String idPlayer,String idPet) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.asignarPet(controlOperacionBD,idPlayer,idPet);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Boolean asignarShip(ControlOperacionServicio controlOperacion, String idPlayer,String idShip) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.asignarShip(controlOperacionBD,idPlayer,idShip);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}


	public List<Match> obtenerMatchesPlayer(ControlOperacionServicio controlOperacion, String idPlayer, String idGame, int limit, int offset) throws RespuestaFallidaServicio  {

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Match> matches = controllerDB.obtenerMatchesByPlayer(controlOperacionBD, idPlayer, idGame, limit, offset);



				return matches;


			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}


	public List<Match> obtenerMatchsByGamePlayer(ControlOperacionServicio controlOperacion, String idGame, String idPlayer, int limit, int offset) throws RespuestaFallidaServicio  {
		List<Match> matchesByGame = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Match> matches = controllerDB.obtenerMatchesByPlayer(controlOperacionBD, idPlayer, idGame, limit, offset);


				if(matches!=null) {
					for (Match match : matches) {

						if(match.getMatch_game().equals(idGame)) {

							if(matchesByGame==null) {
								matchesByGame = new ArrayList<Match>();
							}

							matchesByGame.add(match);
						}
					}
				}

				return matchesByGame;


			} catch (ResponseFailedBD e) {
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public List<Score> obtenerMatchScore(ControlOperacionServicio controlOperacion, String idMatch) throws RespuestaFallidaServicio  {

		List<Score> scores = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				scores = controllerDB.obtenerMatchScore(controlOperacionBD,idMatch);
				return scores;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}


	public Boolean actualizarTipoJugador(ControlOperacionServicio controlOperacion, String idPlayer,String tpPlayer) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.actualizarTipoJugador(controlOperacionBD,idPlayer,tpPlayer);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}


	public List<World> obtenerPlayerWordls(ControlOperacionServicio controlOperacion, String idPlayer) throws RespuestaFallidaServicio  {


		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<World> list = controllerDB.obtenerPlayerWordls(controlOperacionBD,idPlayer);

				return list;


			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public List<Game> obtenerGamesWorld(ControlOperacionServicio controlOperacion, String idMundo, String idPlayer) throws RespuestaFallidaServicio  {


		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Game> list = controllerDB.obtenerGamesWorld(controlOperacionBD,idMundo,idPlayer);

				return list;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}


	public Game obtenerGameByPlayer(ControlOperacionServicio controlOperacion, String idGame, String idPlayer) throws RespuestaFallidaServicio  {


		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Game> list = obtenerGamesByPlayer(controlOperacion, idPlayer, null);

				Game gameByID = null;
				if(list!=null) {
					for (Game game : list) {

						if(game.getGame_game().equals(idGame)) {
							gameByID = game;

						}

					}
				}

				return gameByID;

			} catch (RespuestaFallidaServicio e) {

				throw e;

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}


	public World getWorldPlayerByID(ControlOperacionServicio controlOperacion, String player_player, String world_world) throws RespuestaFallidaServicio {

		try {
			List<World> worldsByPlayer = obtenerPlayerWordls(controlOperacion, player_player);


			if(worldsByPlayer!=null) {
				for (World world : worldsByPlayer) {

					if(world.getWorld_world().equals(world_world)) {


						List<Game> games = obtenerGamesWorld(controlOperacion, world.getWorld_world(), player_player);

						world.setGames(games);

						return world;


					}

				}

			}


		} catch (RespuestaFallidaServicio e) {
			throw e;
		}

		return null;

	}

	public List<Game> obtenerGamesByPlayer(ControlOperacionServicio controlOperacion, String idPlayer, String state) throws RespuestaFallidaServicio  {



		if(controlOperacion!=null) {



			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				List<Game> list = controllerDB.obtenerGamesByPlayer(controlOperacionBD,idPlayer);


				if(list!=null) {
					if(state!=null) {

						List<Game> listFiltered = new ArrayList<Game>();

						for (Game game : list) {

							if(game.getGame_state().equals(state)) {
								listFiltered.add(game);

							}
						}

						list = listFiltered;

					}
				}

				return list;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}

	public Boolean guardarPeguntas(ControlOperacionServicio controlOperacion, String idPlayer, List<AnswerQuestion> respuestaPreguntas) throws RespuestaFallidaServicio  {

		Boolean sinErrores = false;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				sinErrores = controllerDB.guardarPeguntas(controlOperacionBD, idPlayer, respuestaPreguntas);


			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return sinErrores;
	}

	public Account obtenerCuentaPorPlayerCliente(ControlOperacionServicio controlOperacion, String idPlayer, String idClient) throws RespuestaFallidaServicio  {

		Account cuenta = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				cuenta = controllerDB.obtenerCuentaPorPlayerCliente(controlOperacionBD,idPlayer,idClient);

				return cuenta;

			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
	}
	
	
	
	public Email createEmailConfirmRegister(ControlOperacionServicio controlOperacion, User usuarioRegistrado, String acode_code, String otp ) throws Exception {
		
		
		Email emailDto = null;
		
		//Generar token de registro
		TokenUserRegister tokenRegistroUsuario = generadorTokenRegistroUsuario.generarToken(usuarioRegistrado, acode_code, otp);

		EncryptEncodeUserRegisterToken tokenConfirmacionEnconde = generadorTokenRegistroUsuario.getEncrypEncodeToken(tokenRegistroUsuario);

		String urlBase = appServicio.getPublicoURLBase();

		String urlConfirmRegister = urlBase+appServicio.getContextoAPP()+PlayerRegisterController.URL_CONFIRM+"?"+
				PARAMETRO_CONFIRMACION_REGISTRO+"="+tokenConfirmacionEnconde.getencryptEncodeToken()+
				"&"+PARAMETRO_ENCRYPT_SALT+"="+tokenConfirmacionEnconde.getencryptEncodeSalt()+
				"&"+PARAMETRO_ENCRYPT_IV+"="+tokenConfirmacionEnconde.getencryptEncodeIV();

		String emailSubject = i18nservice.translate("player_register.email.subject");
		String emailMessage = i18nservice.translate("player_register.email.message");



		emailDto = new Email(mailServicio.getMailUsername(), usuarioRegistrado.getUser_email(),emailSubject,emailMessage);

		Map<String, Object> parameterMap = new HashMap<String, Object>();


		ParametroConfiguracionGeneral parameterAppLogoUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_LOGO_INTERNET_URL);

		String urlAppLogo = "";
		if(parameterAppLogoUrl!=null) {
			urlAppLogo = parameterAppLogoUrl.getCnfg_valor();
		}

		ParametroConfiguracionGeneral parameterAppPageUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_PAGE_INTERNET_URL);

		String urlAppPage = "";
		if(parameterAppPageUrl!=null) {
			urlAppPage = parameterAppPageUrl.getCnfg_valor();
		}


		String emailHeaderTitle = i18nservice.translate("player_register.email.header.title");
		String emailHeaderSubtitle = i18nservice.translate("player_register.email.header.subtitle");
		String emailContentGreeting = i18nservice.translate("player_register.email.content.greeting");
		String emailContentMessage = i18nservice.translate("player_register.email.content.message");
		String emailContentFooter = i18nservice.translate("player_register.email.content.footer");
		String emailContentButtonConfirm = i18nservice.translate("player_register.email.content.button_confirm");

		parameterMap.put("emailHeaderTitle", emailHeaderTitle);
		parameterMap.put("emailHeaderSubtitle", emailHeaderSubtitle);
		parameterMap.put("emailContentGreeting", emailContentGreeting);
		parameterMap.put("emailContentMessage", emailContentMessage);
		parameterMap.put("emailContentFooter", emailContentFooter);
		parameterMap.put("emailContentButtonConfirm", emailContentButtonConfirm);

		parameterMap.put("urlAppPage", urlAppPage);
		parameterMap.put("urlAppLogo", urlAppLogo);


		parameterMap.put("urlConfirmRegister", urlConfirmRegister );
		parameterMap.put("user", usuarioRegistrado);
		emailDto.setParameterMap(parameterMap);
		
		return emailDto;

		
	}
	
	public PlayerExternalIDResponse actualizarInfoPlayer(ControlOperacionServicio controlOperacion, List<Player> players, String idClient) throws RespuestaFallidaServicio  {

		PlayerExternalIDResponse playerExternalIDResponse = null;

		if(controlOperacion!=null) {

			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);

			try {	
				playerExternalIDResponse = controllerDB.actualizarInfoPlayer(controlOperacionBD,players,idClient);


			} catch (ResponseFailedBD e) {

				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}	

		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return playerExternalIDResponse;
	}
	


}
