package com.co.app.backend.service.persistence.mapper.integration;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import com.co.app.model.dto.integration.GameTokenIntegration;
import com.co.app.model.dto.integration.Partner;
import com.co.app.model.dto.integration.SessionIntegration;


//@Component
@Mapper
public interface IntegrationGameMapper {

    public Partner getPartnerPorKeys(HashMap<Object, Object> parametrosInOout);
    
    public Partner getPartners(HashMap<Object, Object> parametrosInOout);
    
	public Boolean crearPartner(HashMap<Object, Object> parametrosInOout);
	
	public Boolean inactivarPartner(HashMap<Object, Object> parametrosInOout);

	public String iniciarSession(HashMap<Object, Object> parametrosInOout);

	public Boolean finalizarSession(HashMap<Object, Object> parametrosInOout);
    
    public String iniciarMatch(HashMap<Object, Object> parametrosInOout);

    public Boolean actualizarMatch(HashMap<Object, Object> parametrosInOout);

    public SessionIntegration getSession (HashMap<Object, Object> parametrosInOout);
    
    public String createGToken(HashMap<Object, Object> parametrosInOout);
    
    public GameTokenIntegration getGToken (HashMap<Object, Object> parametrosInOout);
    
    
    
}