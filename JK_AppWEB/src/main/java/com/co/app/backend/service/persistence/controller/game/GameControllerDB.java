package com.co.app.backend.service.persistence.controller.game;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.persistence.CommonsParameterBDGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.ResponseFailedBDGenerator;
import com.co.app.backend.service.persistence.mapper.game.GameMapper;
import com.co.app.model.dto.game.Answer;
import com.co.app.model.dto.game.AnswerQuestion;
import com.co.app.model.dto.game.AttribChrt;
import com.co.app.model.dto.game.AttribPlayer;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Question;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;


@Component
public class GameControllerDB {
	
	
	private Logger logger = LoggerFactory.getLogger(GameControllerDB.class);
	
	@Autowired
	private GameMapper mapper;
	
	@Autowired
	private ResponseFailedBDGenerator generadorExcepcionBD;
	
	@Autowired
	private AppService appServicio;
	
	@Autowired
	private I18nService i18nService;
	
	
	
	public List<Avatar> obtenerAvatars(ControlOperacionBD controlOperacionBD)  throws ResponseFailedBD {
		

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Avatar> listaAvatars= null;
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			mapper.obtenerAvatars(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				List<AttribChrt> listaAtributos= null;
				
				listaAvatars = (List<Avatar>) parametrosInOout.get("p_tavatar");
				listaAtributos = (List<AttribChrt>) parametrosInOout.get("p_tatrch");
				
				
				if(listaAvatars!=null) {
					
					for (Avatar avatar : listaAvatars) {
						
						
						if(listaAtributos!=null) {
							List<AttribChrt> listAttrByCharacter = new ArrayList<AttribChrt>();
							
							for (AttribChrt attribChrt : listaAtributos) {
								
								if(attribChrt.getAtrch_chrt().equals(avatar.getAvatar_chrt())) {
									listAttrByCharacter.add(attribChrt);
									
								}
								
							}
							
							avatar.setListAttributes(listAttrByCharacter);
							
						}
						
						i18nService.applyi18nAvatar(avatar);
						
						
					}
					
					
				}
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		}catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaAvatars;

	}
	
	public List<Pet> obtenerPets(ControlOperacionBD controlOperacionBD)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Pet> listaPets = new ArrayList<Pet>();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			mapper.obtenerPets(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				
				List<AttribChrt> listaAtributos = null;
				
				listaPets = (List<Pet>) parametrosInOout.get("p_tpet");
				listaAtributos = (List<AttribChrt>) parametrosInOout.get("p_tatrch");
				
				if(listaPets!=null) {
					
					for (Pet pet : listaPets) {
						
						
						if(listaAtributos!=null) {
							List<AttribChrt> listAttrByCharacter = new ArrayList<AttribChrt>();
							
							for (AttribChrt attribChrt : listaAtributos) {
								
								if(attribChrt.getAtrch_chrt().equals(pet.getPet_chrt())) {
									listAttrByCharacter.add(attribChrt);
									
								}
								
							}
							
							pet.setListAttributes(listAttrByCharacter);
							
						}
						
						i18nService.applyi18nPet(pet);
					}
					
					
				}
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaPets;

	}

	public List<Ship> obtenerShips(ControlOperacionBD controlOperacionBD)  throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
	
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Ship> listaShips = new ArrayList<Ship>();
		
	
		try {
	
			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);
	
			
			//Se crean los parametros de negocio
			mapper.obtenerShips(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				
				
				List<AttribChrt> listaAtributos = null;
				
				listaShips = (List<Ship>) parametrosInOout.get("p_tship");
				listaAtributos = (List<AttribChrt>) parametrosInOout.get("p_tatrch");
				
				if(listaShips!=null) {
					
					for (Ship ship : listaShips) {
						
						
						if(listaAtributos!=null) {
							List<AttribChrt> listAttrByCharacter = new ArrayList<AttribChrt>();
							
							for (AttribChrt attribChrt : listaAtributos) {
								
								if(attribChrt.getAtrch_chrt().equals(ship.getShip_chrt())) {
									listAttrByCharacter.add(attribChrt);
									
								}
								
							}
							
							ship.setListAttributes(listAttrByCharacter);
							
						}
						
						i18nService.applyi18nShip(ship);
					}
					
					
				}
				
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return listaShips;
	
	}
	
	
	public List<World> obtenerMundos(ControlOperacionBD controlOperacionBD)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<World> listaWorlds = new ArrayList<World>();
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			mapper.obtenerWorlds(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaWorlds = (List<World>) parametrosInOout.get("p_tworld");	
				
				for (World world : listaWorlds) {
					i18nService.applyi18nWorld(world);
				}

				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaWorlds;

	}
	
	public Game obtenerGamePorId(ControlOperacionBD controlOperacionBD, String idGame)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		Game game = new Game();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_game_game", idGame);

			mapper.obtenerGamePorId(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<Game> lista =(List<Game>) parametrosInOout.get("p_tgame");
				
				if (!lista.isEmpty()) {
					game = lista.get(0);
					
					i18nService.applyi18nGame(game);
					
				} else {
					return null;
				}			
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return game;

	}
	
	public World obtenerMundoPorId(ControlOperacionBD controlOperacionBD, String idMundo)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		World world= new World();
		
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_world_world", idMundo);

			mapper.obtenerMundoPorId(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				List<World> lista =(List<World>) parametrosInOout.get("p_tworld");
				
				if (!lista.isEmpty()) {
					world= lista.get(0);
					
					i18nService.applyi18nWorld(world);
					
				} else {
					return null;
				}			
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return world;

	}
	
	public List<Player> obtenerRankingGlobalPlayers(ControlOperacionBD controlOperacionBD)  throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = new ArrayList<Player>();
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerRankingGlobalPlayers(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");		
				
				for (Player player : players) {
					i18nService.applyi18nPlayer(player);
				}
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	
	public List<Player> obtenerRankingGlobalPorFecha(ControlOperacionBD controlOperacionBD, Date fechaInicio, Date fechaFin)  throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String fechaStar = "";
		String fechaEnd = "";
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = new ArrayList<Player>();
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);
	    	
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			
			fechaStar = format.format(fechaInicio);
			fechaEnd = format.format(fechaFin);
			//Se crean los parametros de negocio
			parametrosInOout.put("p_date_star", fechaStar);
			parametrosInOout.put("p_date_end", fechaEnd);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerRankingGlobalPorFecha(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");		
				
				for (Player player : players) {
					i18nService.applyi18nPlayer(player);
				}
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	
	public List<Player> obtenerRankingEmpresaPorFecha(ControlOperacionBD controlOperacionBD, Date fechaInicio, Date fechaFin, String idClient)  throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
		
		String fechaStar = "";
		String fechaEnd = "";
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = new ArrayList<Player>();
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);
	    	
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			
			fechaStar = format.format(fechaInicio);
			fechaEnd = format.format(fechaFin);
			//Se crean los parametros de negocio
			parametrosInOout.put("p_date_star", fechaStar);
			parametrosInOout.put("p_date_end", fechaEnd);
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerRankingEmpresaPorFecha(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");		
				
				for (Player player : players) {
					i18nService.applyi18nPlayer(player);
				}
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	
	public List<Player> obtenerRankingEmpresaPlayers(ControlOperacionBD controlOperacionBD, String idClient)  throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = new ArrayList<Player>();
		
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerRankingEmpresaPlayers(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");		
				
				for (Player player : players) {
					i18nService.applyi18nPlayer(player);
				}
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	
	public List<Player> getPlayerbyNick(ControlOperacionBD controlOperacionBD, String playerNick)  throws ResponseFailedBD {

		String pgcriptoKey 	     = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = null;
		
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_player_nick", playerNick);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerJugadorporNick(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");
				
					
				if(players!=null && players.size()>0) {
					
					List<AttribPlayer> atributos =null;
					
					atributos = (List<AttribPlayer>) parametrosInOout.get("p_tatrpl");
					
					if(atributos!=null && atributos.size()>0) {
						
						for (Player player : players) {
							
							List<AttribPlayer> listAttrPlayer = new ArrayList<AttribPlayer>();
							
							for (AttribPlayer attribPlayer : atributos) {
								if(attribPlayer.getAtrpl_player().equals(player.getPlayer_player())) {
									listAttrPlayer.add(attribPlayer);
								}
							}
							
							player.setListAttributes(listAttrPlayer);
							i18nService.applyi18nPlayer(player);
						}
					}
					
					
					
					
				}
				
				
				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	
	
	
	public List<Player> obtenerPosicionRankingEmpresaPl(ControlOperacionBD controlOperacionBD, String idClient, String idPlayer)  throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Player> players = new ArrayList<Player>();
		
		

		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_client_client", idClient);
			parametrosInOout.put("p_player_player", idPlayer);
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY , pgcriptoKey);
			mapper.obtenerPosicionRankingEmpresaPl(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				players = (List<Player>) parametrosInOout.get("p_tplayer");		
				
				for (Player player : players) {
					i18nService.applyi18nPlayer(player);
				}
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		return players;
		
	}
	
	public List<Question> getQuestions(ControlOperacionBD controlOperacionBD) throws ResponseFailedBD{

		
		String pgcriptoKey = appServicio.getPGCryptoKey();
		
		
		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Question> questions= null;
		List<Answer> answers = null;
		List<AnswerQuestion> anwqtn = null;
		
		try {

			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo );
			
			
			
			//Se crean los parametros de negocio
			mapper.getQuestions(parametrosInOout);
					
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			
			
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				questions = (List<Question>) parametrosInOout.get("p_tquestion");
				answers = (List<Answer>) parametrosInOout.get("p_tanswer");
				anwqtn = (List<AnswerQuestion>) parametrosInOout.get("p_tanwqtn");
				
				if(answers!=null) {
					for (Answer answer: answers) {
						
						i18nService.applyi18nAnswer(answer);
							
					}
				}
				
			
			
				if(questions!=null) {
					
					for (Question question: questions) {
						
						if(anwqtn!=null) {
							for (AnswerQuestion answerQuestion : anwqtn) {
								
								if(answerQuestion.getAnwqtn_questn().equals(question.getQuestn_questn())) {
									
									if(answers!=null) {
										for (Answer answer: answers) {
											if(answer.getAnswer_answer().equals(answerQuestion.getAnwqtn_answer())) {
												question.addAnswer(answer);
												
											}
												
										}
									}
									
								}
								
							}
						}
					
						i18nService.applyi18nQuestion(question);
						
					}
				}
				
				
			} else {
				
				codigoRespuesta = ConstantesBD.COD_FALLIDO;
				
				throw exceptionGeneralBD;
				
			}
						
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			//session.close();
		}

		return questions;

	}
	
	public List<Game> obtenerJuegos(ControlOperacionBD controlOperacionBD)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Game> listaJuegos = new ArrayList<Game>();
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			mapper.obtenerJuegos(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaJuegos = (List<Game>) parametrosInOout.get("p_tgame");	
				
				for (Game game : listaJuegos) {
					i18nService.applyi18nGame(game);
				}

				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaJuegos;
	}

	public List<LearningObjective> getLearningObjectivesByGame(ControlOperacionBD controlOperacionBD, String idGame)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<LearningObjective> listaObjetivos = new ArrayList<LearningObjective>();
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_game_game", idGame);
			mapper.getLearningObjectivesByGame(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaObjetivos = (List<LearningObjective>) parametrosInOout.get("p_tlrnobj");
				
				for (LearningObjective lrnobj:listaObjetivos) {
					
					i18nService.applyi18nLearningObjectives(lrnobj);

					if(lrnobj.getLrnobj_clasif() ==null) {
						lrnobj.setLrnobj_clasif(lrnobj.CLASIF_DEFAULT);
					}
				}
				
						
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaObjetivos;
	}
	
	public List<World> obtenerMundosPorGame(ControlOperacionBD controlOperacionBD,String idGame)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<World> listaWorlds = new ArrayList<World>();
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_game_game", idGame);
			mapper.obtenerMundosPorGame(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaWorlds = (List<World>) parametrosInOout.get("p_tworld");	
				
				for (World world : listaWorlds) {
					i18nService.applyi18nWorld(world);
				}

				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaWorlds;

	}
	
	public List<Game> obtenerJuegosPorMundo(ControlOperacionBD controlOperacionBD, String idWorld)  throws ResponseFailedBD {
		
		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {}.getClass().getEnclosingMethod().getName();
		
		List<Game> listaJuegos = new ArrayList<Game>();
		try {

			
			//Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD, this.getClass(), nombreMetodo);

			
			//Se crean los parametros de negocio
			parametrosInOout.put("p_world_world", idWorld);
			mapper.obtenerJuegosPorMundo(parametrosInOout);
			
			String codigoRespuesta = (String)parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta"+codigoRespuesta);
			
			
			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(), codigoRespuesta, nombreMetodo, errores, mensajes);
			
			if (codigoRespuesta!=null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
								
				listaJuegos = (List<Game>) parametrosInOout.get("p_tgame");	
				
				for (Game game : listaJuegos) {
					i18nService.applyi18nGame(game);
				}

				
				
			} else {
				throw exceptionGeneralBD;
			}
			
		} catch (ResponseFailedBD e) {
			
			throw e;
			
		} catch (Exception e) {	
			ResponseFailedBD exceptionGeneralBD =  generadorExcepcionBD.generarException(this.getClass(),ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		}  finally {
			//session.close();
		}
		
		
		return listaJuegos;
	}
}

