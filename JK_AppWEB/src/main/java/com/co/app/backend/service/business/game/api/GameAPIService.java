package com.co.app.backend.service.business.game.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.business.game.GamePlayerTokenGenerator;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.integration.IntegrationGameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.backend.service.persistence.controller.user.UserControllerDB;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.TokenGamePlayer;
import com.co.app.model.dto.game.api.auth.GameAPI_AuthRequest;
import com.co.app.model.dto.game.api.auth.GameAPI_AuthResponse;
import com.co.app.model.dto.game.api.auth.GameAPI_CloseAppRequest;
import com.co.app.model.dto.game.api.auth.GameAPI_CloseAppResponse;
import com.co.app.model.dto.game.api.match.GameAPI_GameMatchData;
import com.co.app.model.dto.game.api.match.GameAPI_StartGameMatchRequest;
import com.co.app.model.dto.game.api.match.GameAPI_StartGameMatchResponse;
import com.co.app.model.dto.game.api.match.GameAPI_UpdateGameMatchRequest;
import com.co.app.model.dto.game.api.match.GameAPI_UpdateGameMatchResponse;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.integration.Partner;
import com.co.app.model.dto.integration.SessionIntegration;

@Component
public class GameAPIService {
	
	Logger logger = LoggerFactory.getLogger(GameAPIService.class);
	
	@Autowired
	UserControllerDB controllerDB;
	
	
	@Autowired
	ResponseFailedServiceGenerator generadorExcepcionServicio;
	
	@Autowired
	PlayerServicio playerServicio;
	
	@Autowired
	GameServicio gameServicio;
	
	@Autowired
	GamePlayerTokenGenerator gamePlayerTokenGenerator;
	
	@Autowired
	IntegrationGameServicio integrationServicio;
	
	
	

	public GameAPI_AuthResponse authAPP(ControlOperacionServicio controlOperacion,	GameAPI_AuthRequest authRequest) throws RespuestaFallidaServicio {

		GameAPI_AuthResponse authResponse = null;

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				logger.info(controlOperacionBD.toString());
				
				
				Partner partner = integrationServicio.getPartner(controlOperacion, authRequest.getSiteKey(), authRequest.getAccessKey());
				
				if(partner!=null) {
					
					String otp = gamePlayerTokenGenerator.generarOTP();
					
					String idSession = integrationServicio.iniciarSession(controlOperacion, partner.getPartnr_partnr(), otp);
					authResponse = new GameAPI_AuthResponse();
					authResponse.setAppToken(idSession);
					
					
				}else {
					logger.error("ERROR_PARTNER_NE");
					throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
				}
				
				
				
				
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return authResponse;
	}

	public GameAPI_StartGameMatchResponse startMatch(ControlOperacionServicio controlOperacion, GameAPI_StartGameMatchRequest startGameMatchRequest) throws RespuestaFallidaServicio {

		GameAPI_StartGameMatchResponse startGameResponse = null;

		if(controlOperacion!=null) {

			try {

				String appToken = startGameMatchRequest.getAppToken();
				String gameToken = startGameMatchRequest.getGameToken();
				
				
				SessionIntegration sessionIntegration = integrationServicio.getSession(controlOperacion, appToken);
				
				if(sessionIntegration!=null) {
					TokenGamePlayer tokenGamePlayer = gamePlayerTokenGenerator.obtenerTokenGamePlayer(controlOperacion, gameToken);
					
					if(tokenGamePlayer!=null) {
						
						Player player = playerServicio.getPlayerByID(controlOperacion, tokenGamePlayer.getPlayer_player(), true);
						Game game = gameServicio.obtenerGamePorId(controlOperacion, tokenGamePlayer.getGame_game());
						
						if(player!=null & game!=null) {
							
							
							String idMatch = integrationServicio.iniciarMatch(controlOperacion, player.getPlayer_player(), game.getGame_game(), appToken, gameToken);
							
							if(idMatch!=null) {

								startGameResponse = new GameAPI_StartGameMatchResponse();
								startGameResponse.setGameMatchID(idMatch);
								startGameResponse.setPlayer(player);
								startGameResponse.setGame(game);
								
							}else {
								logger.error("ERROR_INIT_MATCH");
								throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
							}
							
						}else{
							logger.error("ERROR_PL_GM_NV");
							throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
						}
						
					}else {
						logger.error("ERROR_TOKEN_PL_NV");
						throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
					}
					
				}else {
					logger.error("ERROR_SESSION_NV");
					throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_SESSION_INT_NV);
					
				}
				
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}

		return startGameResponse;
	}
	
	
	public GameAPI_UpdateGameMatchResponse updateMatch(ControlOperacionServicio controlOperacion, GameAPI_UpdateGameMatchRequest updateGameDataMatchRequest) throws RespuestaFallidaServicio {


		GameAPI_UpdateGameMatchResponse updateGameResponse = null;
		
		if(controlOperacion!=null) {

			try {

				String appToken = updateGameDataMatchRequest.getAppToken();
				String match_match = updateGameDataMatchRequest.getGameMatchID();
				GameAPI_GameMatchData gameMatchData = updateGameDataMatchRequest.getGameMatchData();
				
				SessionIntegration sessionIntegration = integrationServicio.getSession(controlOperacion, appToken);
				
				if(sessionIntegration!=null) {
					
					Match match = new Match();
					match.setMatch_match(match_match);
					match.setMatch_game_score(gameMatchData.getMatch_general_score());
					match.setMatch_result(gameMatchData.getMatch_result());
					
					String state;
					
					if(gameMatchData.getMatch_result().equals(Match.RESULT_WIN) || gameMatchData.getMatch_result().equals(Match.RESULT_LOSE)) {
						state= Match.STATE_END;
					}else {
						state= Match.STATE_PENDING;
					}
					
					
					match.setMatch_state(state);
					
					Boolean isGameUpdated = integrationServicio.actualizarMatch(controlOperacion, match,null,appToken );
					
					updateGameResponse = new GameAPI_UpdateGameMatchResponse();
					updateGameResponse.setIsGameUpdated(isGameUpdated);
					
					
					
					if(state.equals(Match.STATE_END)) {
						
						
						integrationServicio.finalizarSession(controlOperacion, sessionIntegration.getSession_session(), sessionIntegration.getSession_tkn());
					}
					
					
				}else {
					logger.error("ERROR_SESSION_NV");
					throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_SESSION_INT_NV);
					
				}
				
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
			}

			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return updateGameResponse;
	}
	
	public GameAPI_CloseAppResponse closeAPP(ControlOperacionServicio controlOperacion,	GameAPI_CloseAppRequest closeRequest) throws RespuestaFallidaServicio {

		GameAPI_CloseAppResponse closeResponse =null;
		

		if(controlOperacion!=null) {

			try {

				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				logger.info(controlOperacionBD.toString());
				
				
				Partner partner = integrationServicio.getPartner(controlOperacion, closeRequest.getSiteKey(), closeRequest.getAccessKey());
				
				if(partner!=null) {
					
					SessionIntegration sessionIntegration = integrationServicio.getSession(controlOperacion, closeRequest.getAppToken());
					
					Boolean isSessionClosed = false;
					if(sessionIntegration!=null && sessionIntegration.getSession_partnr().equals(partner.getPartnr_partnr())) {
						
						isSessionClosed = integrationServicio.finalizarSession(controlOperacion, sessionIntegration.getSession_session(), sessionIntegration.getSession_tkn());
						
						
						
					}else {
						isSessionClosed = true;
					}
					
					closeResponse = new GameAPI_CloseAppResponse();
					closeResponse.setIsAppClosed(isSessionClosed);
					
					
				}else {
					logger.error("ERROR_PARTNER_NE");
					throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
				}
				
				
				
				
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_INFO_NV);
			}


		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return closeResponse;
	}
}
