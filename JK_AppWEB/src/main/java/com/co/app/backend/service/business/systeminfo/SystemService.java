/*
 * MIT License 
 * 
 * Copyright (c) 2018 Ownk
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 */

package com.co.app.backend.service.business.systeminfo;

import org.springframework.stereotype.Component;

import com.co.app.model.dto.systeminfo.SystemInfo;

/**
 *
 * <h1>SystemService</h1>
 *
 * Esta clase recopilara toda la informacion del sistema en que opera la aplicacion
 *
 * @author Master_Zen (Ownk) 
 * @version 1.0
 * 
 */

@Component
public class SystemService {
	
	/**
	 * Este metodo recopila la informacion del sistema
	*/
	public SystemInfo getInfo() {
		SystemInfo systemInfo = new SystemInfo();
		
		systemInfo.setFileEncoding(System.getProperty("file.encoding"));
		
		systemInfo.setJavaClassPath(System.getProperty("java.class.path"));
		systemInfo.setJavaHome(System.getProperty("java.home"));
		systemInfo.setJavaRuntimeVersion(System.getProperty("java.runtime.version"));
		systemInfo.setJavaSpecificationName(System.getProperty("java.specification.name"));
		systemInfo.setJavaSpecificationVendor(System.getProperty("java.specification.vendor"));
		systemInfo.setJavaSpecificationVersion(System.getProperty("java.specification.version"));
		systemInfo.setJavaVendor(System.getProperty("java.vendor"));
		systemInfo.setJavaVersion(System.getProperty("java.version"));
		
		systemInfo.setUserName(System.getProperty("user.name"));
		systemInfo.setUserTimeZone(System.getProperty("user.timezone"));
		systemInfo.setUserLanguage(System.getProperty("user.language"));
		systemInfo.setUserCountry(System.getProperty("user.country"));
		systemInfo.setUserDir(System.getProperty("user.dir"));
		
		systemInfo.setOsArch(System.getProperty("os.arch"));
		systemInfo.setOsName(System.getProperty("os.name"));
		systemInfo.setOsVersion(System.getProperty("os.version"));
		
		return systemInfo;
	}

}
