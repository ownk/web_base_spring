package com.co.app.backend.service.business.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.commons.ResponseFailedServiceGenerator;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.controller.user.UserControllerDB;
import com.co.app.frontend.controller.publico.player.register.PlayerRegisterController;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.session.Menu;
import com.co.app.model.dto.session.Modulo;
import com.co.app.model.dto.session.Servicio;
import com.co.app.model.dto.user.InfoIPNavegadorSession;
import com.co.app.model.dto.user.LoginUser;
import com.co.app.model.dto.user.PasswordChange;
import com.co.app.model.dto.user.PasswordValidationResponse;
import com.co.app.model.dto.user.TokenUserRegister;
import com.co.app.model.dto.user.User;
import com.co.app.model.dto.user.UsernameValidationResponse;

@Component
public class UserService {
	
	Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	UserControllerDB controllerDB;
	
	
	@Autowired
	ResponseFailedServiceGenerator generadorExcepcionServicio;
	
	@Autowired
	I18nService i18nservice;
	
	@Autowired
	MailServicio mailServicio;
	
	@Autowired
	AppService appService;
	
	

	public User getUsuarioPorNick(ControlOperacionServicio controlOperacion, String user_nick) throws RespuestaFallidaServicio {
		
		User usuario = null;
		
		if(controlOperacion!=null) {
			
			try {
				
				
				
				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				
				
				usuario = controllerDB.getUsuarioPorNick(controlOperacionBD, user_nick);
			} catch (ResponseFailedBD e) {
				
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}
			
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return usuario;
	}
	
	public User getUsuarioPorEmail(ControlOperacionServicio controlOperacion, String user_email) throws RespuestaFallidaServicio {
		
		User usuario = null;
		
		if(controlOperacion!=null) {
			
			try {
				
				
				
				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				
				
				usuario = controllerDB.getUsuarioPorEmail(controlOperacionBD, user_email);
			} catch (ResponseFailedBD e) {
				
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}
			
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return usuario;
	}
	
	
	

	public List<Modulo> getMenuByUser(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio {
		
		
		List<Modulo> modulos = null;
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				List<Servicio> servicios=null;
				servicios = controllerDB.getMenuGeneralPorUsuario(controlOperacionBD, user_user);
				
				LinkedHashMap<String, Modulo> keymodulos = new LinkedHashMap<String, Modulo>();
				
				
				LinkedHashMap<Long, Menu> keymenus = new LinkedHashMap<Long, Menu>();
				
				
				if(servicios!=null) {
					
					for (Servicio servicio : servicios) {
						
						Modulo modulo = new Modulo();
						modulo.setMdlo_descri(servicio.getMdlo_descri());
						modulo.setMdlo_mdlo(servicio.getMdlo_mdlo());
						modulo.setMdlo_nomb(servicio.getMdlo_nomb());
						
						
						keymodulos.put(servicio.getMdlo_mdlo(), modulo);
						
						Menu menu = new Menu();
						menu.setMenu_nombre(servicio.getMenu_nombre());
						menu.setMenu_icon(servicio.getMenu_icon());
						menu.setMenu_menu(servicio.getMenu_menu());
						
						keymenus.put(servicio.getMenu_menu(), menu);
					}
					
					//Se recorren los modulos
					Set<Entry<String, Modulo>> modulosEntry = keymodulos.entrySet();
					Set<Entry<Long, Menu>> menusEntry = keymenus.entrySet();
					
					modulos = new ArrayList<Modulo>();
					for (Entry<String, Modulo> moduloEntry : modulosEntry) {
						
						Modulo modulo = moduloEntry.getValue();
						
						for (Entry<Long, Menu> menuEntry : menusEntry) {
							
							Menu menu = menuEntry.getValue();
							
							for (Servicio servicio : servicios) {
								
								if(menu.getMenu_menu().equals(servicio.getMenu_menu()) && modulo.getMdlo_mdlo().equals(servicio.getMdlo_mdlo())){
									menu.addService(servicio);
									modulo.addMenu(menu);
								}
							}
						}
						
						modulos.add(modulo);
					}
					
					
				}
				
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
				
		
		return modulos;
	}

	
	public List<Servicio> getServiciosUsuarioPorURL(ControlOperacionServicio controlOperacion, String user_user, String surl_url) throws RespuestaFallidaServicio {
		
		List<Servicio> servicios=null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				servicios = controllerDB.getServiciosUsuarioPorURL(controlOperacionBD, user_user, surl_url);
				
				return servicios;
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
				
		
		
	}

	
	public Boolean olvidoPassword(ControlOperacionServicio controlOperacion,String otp, String user_email) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.olvidoPassword(controlOperacionBD,user_email, otp);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean cambioPassword(ControlOperacionServicio controlOperacion,String user_email, String otp, String password) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.cambioPassword(controlOperacionBD,otp,user_email,password);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean registroCambioPassword(ControlOperacionServicio controlOperacion, PasswordChange passch) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.registroCambioPassword(controlOperacionBD,passch);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public List<PasswordChange> getHistorialPasswordPorUser(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		List<PasswordChange> historialPass = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				historialPass = controllerDB.getHistorialPasswordPorUser(controlOperacionBD,user_user);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return historialPass;
	}
	
	public Boolean registroInicioSessionUser(ControlOperacionServicio controlOperacion, LoginUser loginUsr) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.registroLoginUser(controlOperacionBD,loginUsr);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	
	public Boolean registroLoginOk(ControlOperacionServicio controlOperacion, InfoIPNavegadorSession infoLogin) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.registroLoginOk(controlOperacionBD,infoLogin);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public InfoIPNavegadorSession getLoginOk(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		List<InfoIPNavegadorSession> loginOk = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				loginOk = controllerDB.getLoginOk(controlOperacionBD,user_user);
				
				if(loginOk!=null && loginOk.size()>0) {
					return loginOk.get(0);
				}
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return null;
	}
	
	public Boolean eliminarLoginOk(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.eliminarLoginOk(controlOperacionBD,user_user);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean registroLoginErr(ControlOperacionServicio controlOperacion, InfoIPNavegadorSession infoLogin) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.registroLoginErr(controlOperacionBD,infoLogin);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public InfoIPNavegadorSession getLoginErr(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		List<InfoIPNavegadorSession> loginOk = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				loginOk = controllerDB.getLoginErr(controlOperacionBD,user_user);
				if(loginOk!=null && loginOk.size()>0) {
					return loginOk.get(0);
				}
				
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return null;
	}
	
	public Boolean eliminarLoginErr(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.eliminarLoginErr(controlOperacionBD,user_user);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean registroIpNavegadorOk(ControlOperacionServicio controlOperacion, InfoIPNavegadorSession infoLogin) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.registroIpOk(controlOperacionBD,infoLogin);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public InfoIPNavegadorSession getIpNavegadorOk(ControlOperacionServicio controlOperacion, String browser, String ip) throws RespuestaFallidaServicio  {
		
		List<InfoIPNavegadorSession> loginOk = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				loginOk = controllerDB.getIpOk(controlOperacionBD,browser, ip);
				
				if(loginOk!=null && loginOk.size()>0) {
					return loginOk.get(0);
				}
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return null;
	}
	
	public Boolean eliminarIpOk(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.eliminarIpOk(controlOperacionBD,user_user);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean registroUaOk(ControlOperacionServicio controlOperacion, InfoIPNavegadorSession infoLogin) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores = false;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.registroUaOk(controlOperacionBD,infoLogin);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public InfoIPNavegadorSession getUaOk(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		List<InfoIPNavegadorSession> loginOk = null;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				loginOk = controllerDB.getUaOk(controlOperacionBD,user_user);
				
				
				if(loginOk!=null && loginOk.size()>0) {
					return loginOk.get(0);
				}
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return null;
	}
	
	public Boolean eliminarUaOk(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.eliminarUaOk(controlOperacionBD,user_user);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean validarEmail(ControlOperacionServicio controlOperacion, String email) throws RespuestaFallidaServicio  {
		
		Boolean email_valido;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				email_valido = controllerDB.validarEmail(controlOperacionBD,  email);
				
				
			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return email_valido;
	}

	
	public UsernameValidationResponse isUserNickValid(ControlOperacionServicio controlOperacion, String user_nick) throws RespuestaFallidaServicio  {
		
		UsernameValidationResponse usernameValidationResponse = null;
		
		String isUsernameValid;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				isUsernameValid = controllerDB.cumplimientoUserNick(controlOperacionBD, user_nick);
				

				usernameValidationResponse = new UsernameValidationResponse();
				
				if(isUsernameValid!=null && isUsernameValid.equals(UsernameValidationResponse.USERNICK_OK)) {
					
					usernameValidationResponse.setIsValid(true);
					
				}else {
					usernameValidationResponse.setIsValid(false);
				}
				
				usernameValidationResponse.setCodeResponse(isUsernameValid);
				usernameValidationResponse.setMessageResponse(i18nservice.translate(isUsernameValid));
				
				
				
			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return usernameValidationResponse;
	}

	public PasswordValidationResponse IsPasswordValid(ControlOperacionServicio controlOperacion,String user_name, String user_nick,String user_pass_no_encrypt) throws RespuestaFallidaServicio  {
		
		PasswordValidationResponse passwordValidationResponse = null;
		
		
		String isPasswordValid;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				isPasswordValid = controllerDB.cumplimientoUserPass(controlOperacionBD, user_name, user_nick,user_pass_no_encrypt);
				
				passwordValidationResponse = new PasswordValidationResponse();
				
				if(isPasswordValid!=null && isPasswordValid.equals(PasswordValidationResponse.PASS_OK)) {
					
					passwordValidationResponse.setIsValid(true);
					
				}else {
					passwordValidationResponse.setIsValid(false);
				}
				
				passwordValidationResponse.setCodeResponse(isPasswordValid);
				passwordValidationResponse.setMessageResponse(i18nservice.translate(isPasswordValid));
				
				
				
			} catch (ResponseFailedBD e) {

				throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return passwordValidationResponse;
	}

	public List<User> getUsuariosEmailNoConfirmado(ControlOperacionServicio controlOperacion) throws RespuestaFallidaServicio {
		
		List<User> usuarios = null;
		
		if(controlOperacion!=null) {
			
			try {
				
				ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
				
				
				usuarios = controllerDB.getUsuariosEmailNoConfirmado(controlOperacionBD);
				
				
				
				
				
			} catch (ResponseFailedBD e) {
				
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}

			}
			
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return usuarios;
	}
	
	public Boolean activarUsuarioEmailNoConfirmado(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.activarUsuarioEmailNoConfirmado(controlOperacionBD,user_user);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	public Boolean eliminarUsuarioEmailNoConfirmado(ControlOperacionServicio controlOperacion, String user_user) throws RespuestaFallidaServicio  {
		
		Boolean sinErrores;
		
		if(controlOperacion!=null) {
			
			ControlOperacionBD controlOperacionBD = new ControlOperacionBD(controlOperacion.getUsuario(), controlOperacion.getIp(), null);
			
			try {	
				sinErrores = controllerDB.eliminarUsuarioEmailNoConfirmado(controlOperacionBD,user_user);
				
				
			} catch (ResponseFailedBD e) {
				
				if(e!=null && ConstantesBD.COD_EXITOSO.equals(e.getCodigoRespuesta())) {
					return null;
				}else {
					throw generadorExcepcionServicio.generarException(ConstantesGeneralesAPP.ERROR_BD, e);
				}
				
			}	
			
		}else {
			throw new RespuestaFallidaServicio(ConstantesGeneralesAPP.ERROR_CTRL_OPER_NL);
		}
		
		return sinErrores;
	}
	
	
	public Email createEmailUserActivatedManually(ControlOperacionServicio controlOperacion, User user ) throws Exception {
		
		
		Email emailDto = null;
		
		String emailSubject = i18nservice.translate("user_activated_manually.email.subject");
		String emailMessage = i18nservice.translate("user_activated_manually.email.message");



		emailDto = new Email(mailServicio.getMailUsername(), user.getUser_email(),emailSubject,emailMessage);

		Map<String, Object> parameterMap = new HashMap<String, Object>();


		ParametroConfiguracionGeneral parameterAppLogoUrl= appService.getParameter(ConstantesGeneralesAPP.CNFG_APP_LOGO_INTERNET_URL);

		String urlAppLogo = "";
		if(parameterAppLogoUrl!=null) {
			urlAppLogo = parameterAppLogoUrl.getCnfg_valor();
		}

		ParametroConfiguracionGeneral parameterAppPageUrl= appService.getParameter(ConstantesGeneralesAPP.CNFG_APP_PAGE_INTERNET_URL);

		String urlAppPage = "";
		if(parameterAppPageUrl!=null) {
			urlAppPage = parameterAppPageUrl.getCnfg_valor();
		}


		String emailHeaderTitle = i18nservice.translate("user_activated_manually.email.header.title");
		String emailHeaderSubtitle = i18nservice.translate("user_activated_manually.email.header.subtitle");
		String emailContentGreeting = i18nservice.translate("user_activated_manually.email.content.greeting");
		String emailContentMessage = i18nservice.translate("user_activated_manually.email.content.message");
		String emailContentFooter = i18nservice.translate("user_activated_manually.email.content.footer");
		

		parameterMap.put("emailHeaderTitle", emailHeaderTitle);
		parameterMap.put("emailHeaderSubtitle", emailHeaderSubtitle);
		parameterMap.put("emailContentGreeting", emailContentGreeting);
		parameterMap.put("emailContentMessage", emailContentMessage);
		parameterMap.put("emailContentFooter", emailContentFooter);
		

		parameterMap.put("urlAppPage", urlAppPage);
		parameterMap.put("urlAppLogo", urlAppLogo);


		parameterMap.put("user", user);
		emailDto.setParameterMap(parameterMap);
		
		return emailDto;

		
	}
	
	
}
