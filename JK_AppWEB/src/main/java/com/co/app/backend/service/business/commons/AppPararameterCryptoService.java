package com.co.app.backend.service.business.commons;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.co.app.backend.service.business.aes.EncryptDecryptSHA2Service;


public class AppPararameterCryptoService {
	
	private Logger logger = LoggerFactory.getLogger(EncryptDecryptSHA2Service.class);
	
	public static final String PREFIJO_CRYPTO = "APP_PARAMETER_CRYPTO://";
	
	private static final String  AES_KEY_DEFAULT="22299090945883006969587587599393949300224215349008783320229011803330101518575";
	private static final String  AES_IV_DEFAULT="1YZGWFPqIZUeCTqX";
	private static final String  AES_SALT_DEFAULT="xQhZ5D72ivCH8olRWvSxjqDfJziD1Dfd";
	
	
	private String  AES_KEY="";
	private String  AES_IV="";
	private String  AES_SALT="";
	
	
	private static AppPararameterCryptoService instance;
	
	
	public static AppPararameterCryptoService getInstance() {
	
		if(instance==null) {
			instance = new AppPararameterCryptoService();
		}
		
		return instance;
	}
	
	private AppPararameterCryptoService() {
		
		ClassLoader classLoader = getClass().getClassLoader();
	    
	    
	    InputStream inputStream = null;
	    try {
	    	
	    	
	    	String propertiesFile = "dogger.properties";
	    	
	    	URL url = classLoader.getResource(propertiesFile);
	    	URI uri = new URI(url.toString());
	    	
	    	inputStream = new FileInputStream(uri.getPath());
			
	        Properties prop = new Properties();
	        
	        prop.load(inputStream);
			
			// get the property value and print it out
			String key = prop.getProperty("hackdog.image.param.svg.radio");
			String iv = prop.getProperty("hackdog.image.param.svg.x");
			String salt = prop.getProperty("hackdog.image.param.svg.y");
			
			
			
			if(key!=null && !StringUtils.isEmpty(key)) {
				AES_KEY = key.trim();
				
			}else{
				AES_KEY = AES_KEY_DEFAULT;
			}
			
			
			if(iv!=null && !StringUtils.isEmpty(iv)) {
				AES_IV = iv.trim();
				
			}else{
				AES_IV = AES_IV_DEFAULT;
			}
			
			
			if(salt!=null && !StringUtils.isEmpty(salt)) {
				AES_SALT = salt.trim();
				
			}else{
				AES_SALT = AES_SALT_DEFAULT;
			}
			
			
			
 
			String result = "App Parameters hackdog.image.param.svg.radio" + AES_KEY;
			logger.info(result);
			result = "App Parameters hackdog.image.param.svg.x" + AES_IV;
			logger.info(result);
			result = "App Parameters hackdog.image.param.svg.y" + AES_SALT;
			logger.info(result);
			
  
	         
	    
	    }catch (Exception e) {
	    	logger.error(e.getMessage());
	    	
	    	
			AES_KEY = AES_KEY_DEFAULT;
			AES_IV = AES_IV_DEFAULT;
			AES_SALT = AES_SALT_DEFAULT;
			
	    	
		}   
	    finally {
	        if (inputStream != null) {
	            try {
	                inputStream.close();
	            } catch (IOException e) {
	                logger.error(e.getMessage());
	            }
	        }
	    }
	    
	    
	}
	
	/**---------------------------------------------
	 * 			encripta
	 * ---------------------------------------------
	 * Encriptara el mensaje que recibe.
	 * 
	 * @param mensaje
	 * @return
	 */
	public String encriptar(String mensaje) {
		try {
			
			
			
			mensaje = mensaje.trim();
			
			
			
			String strToEncrypt  = EncryptDecryptSHA2Service.getInstance().encrypt(mensaje, AES_SALT, AES_IV, AES_KEY);
			
			return PREFIJO_CRYPTO + strToEncrypt;
		} catch (Exception e) {
			return mensaje;
		}
	}
	
	/**---------------------------------------------
	 * 				desencripta
	 * ---------------------------------------------
	 * 
	 * @param mensaje
	 * @param clave
	 * @return
	 */
	public String desencriptar(String mensaje) {
		try {
			
			AppPararameterCryptoService.getInstance();
			
			mensaje = mensaje.trim();
			if (mensaje != null){
				if (mensaje.indexOf(PREFIJO_CRYPTO) == 0) {
					
					
					String strToDecrypt = mensaje.substring(PREFIJO_CRYPTO.length());
					
					
					String strDecrypt = EncryptDecryptSHA2Service.getInstance().decrypt(strToDecrypt,  AES_SALT, AES_IV, AES_KEY);
					
					if(strDecrypt!=null && !StringUtils.isEmpty(strDecrypt)) {
						return strDecrypt;
					}else {
						return mensaje;
					}
					 
				}
			}

			return mensaje;
		} catch (Exception e) {
			return mensaje;
		}
	}
	
	
	public static void main(String[] args) {
		

		EncryptDecryptSHA2Service crypto= EncryptDecryptSHA2Service.getInstance();
		
		String salt = "xQhZ5D72ivCH8olRWvSxjqDfJziD1Dfd";
		String iv = "1YZGWFPqIZUeCTqX";
		String key = "22299090945883006969587587599393949300224215349008783320229011803330101518575";
		
		System.out.println("key: "+key);
		System.out.println("iv: "+iv);
		System.out.println("salt: "+salt);
		
			
		
		String strEncrypt = AppPararameterCryptoService.PREFIJO_CRYPTO+crypto.encrypt("juegohacker_us", salt, iv, key);
		System.out.println(strEncrypt);
		
		String strToDecrypt = strEncrypt.substring(PREFIJO_CRYPTO.length());
		System.out.println(strToDecrypt);
		
		
		String strDecrypt = crypto.decrypt(strToDecrypt, salt, iv, key);
		System.out.println(strDecrypt);
		
		
	}
	
	
	
}
