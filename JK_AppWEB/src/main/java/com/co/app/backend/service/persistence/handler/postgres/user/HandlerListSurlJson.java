package com.co.app.backend.service.persistence.handler.postgres.user;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.co.app.backend.service.persistence.handler.postgres.commons.BaseTypeHandler;
import com.co.app.model.dto.session.URLServicio;
import com.fasterxml.jackson.databind.ObjectMapper;


public class HandlerListSurlJson extends BaseTypeHandler implements TypeHandler<Object> {
	
	Logger logger = LoggerFactory.getLogger(HandlerListSurlJson.class);

	@Override
	public Object getResult(ResultSet arg0, String arg1) throws SQLException {
		try {
			
			Array value = arg0.getArray(arg1);
			logger.info(""+value);
		} catch (Exception e) {
			Object value = arg0.getObject(arg1);
			logger.info(""+value);
		}
		return null;
	}

	@Override
	public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
		List<URLServicio> urls = new ArrayList<>();
		
		String respuesta = null;
		try {
			respuesta = (String) cs.getString(columnIndex);
		} catch (Exception e) {
			respuesta = null;
		}
		if (respuesta != null) {
			
			try {
				ObjectMapper mapper = new ObjectMapper();
				URLServicio[] registrosArreglo = mapper.readValue(respuesta, URLServicio[].class);

				if (registrosArreglo != null & registrosArreglo.length > 0) {
					for(URLServicio registro : registrosArreglo) {
						urls.add(registro);
					}
				}

			} catch (Throwable e) {
				logger.error(e.getMessage());
				
			}
			
		}
		return urls;
	}
	
	@Override
	public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType arg3) throws SQLException {
		
	}

	@Override
	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
