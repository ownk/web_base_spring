package com.co.app.backend.service.business.commons;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

@Component
public class ResponseFailedServiceGenerator{
	
	Logger logger = LoggerFactory.getLogger(ResponseFailedServiceGenerator.class);
	
	
	public RespuestaFallidaServicio generarException(String codigoRespuestaServicio , ResponseFailedBD respuestaFallidaGeneralBD) {
		
		String mensaje = "Error realizando llamado al servicio: ";
		
		if(respuestaFallidaGeneralBD!=null) {
			
			
			mensaje = " CODIGO RESPUESTA: "+respuestaFallidaGeneralBD.getCodigoRespuesta();
			
			if(respuestaFallidaGeneralBD.getListaErrores()!=null) {
				
				List<ErrorBD> listaErrores = respuestaFallidaGeneralBD.getListaErrores();
				for (ErrorBD errorBD : listaErrores) {
					mensaje = mensaje+" Error: "+errorBD.getFeccre()+" "+errorBD.getCodigo()+": "+errorBD.getMensaje();
				}
			}
			
			if(respuestaFallidaGeneralBD.getListaMensajes()!=null) {
				
				List<MensajeBD> listaMensajes = respuestaFallidaGeneralBD.getListaMensajes();
				for (MensajeBD mensajeBD : listaMensajes) {
					mensaje = mensaje+" Mensaje: "+mensajeBD.getFeccre()+" "+mensajeBD.getCodigo()+": "+mensajeBD.getMensaje();
				}
			}
			
			
		}
		return new RespuestaFallidaServicio(codigoRespuestaServicio, mensaje);
		
		
		
	}

}