package com.co.app.backend.service.business.thread;

public class TaskThreadTemplate {
	
	private String info;
	private Boolean isOk;
	
	public TaskThreadTemplate(String info) {
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Boolean getIsOk() {
		return isOk;
	}

	public void setIsOk(Boolean isOk) {
		this.isOk = isOk;
	}
	

	
	

}
