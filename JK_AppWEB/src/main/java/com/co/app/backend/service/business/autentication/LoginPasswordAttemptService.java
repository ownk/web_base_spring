package com.co.app.backend.service.business.autentication;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.co.app.backend.service.business.app.AppService;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class LoginPasswordAttemptService {

	
	@Autowired
	AppService appService;
	
	
    private final  int MAX_ATTEMPT_DEFAULT = 3;
    private final  int MIN_TIME_BLOCKED_DEFAULT = 3600;
    private final static int MAX_ATTEMPT = 2;
    private static LoadingCache<String, Integer> attemptsCache;
    private String password_max_attempt_blocked = null;
    
    
    

    public LoginPasswordAttemptService() {
        super();
        attemptsCache = CacheBuilder.newBuilder().expireAfterWrite(MIN_TIME_BLOCKED_DEFAULT, TimeUnit.SECONDS).build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(final String key) {
                return 0;
            }
        });
    }

    //

    public static void loginSucceeded(final String key) {
        attemptsCache.invalidate(key);
    }

    public static void loginFailed(final String key) {
        int attempts = 0;
        try {
            attempts = attemptsCache.get(key);
        } catch (final ExecutionException e) {
            attempts = 0;
        }
        attempts++;
        attemptsCache.put(key, attempts);
    }

    public  boolean isBlocked(final String key) {
        try {
            return attemptsCache.get(key) >=  getPassword_max_attempt_blocked();
        } catch (final ExecutionException e) {
            return false;
        }
    }
	public int getPassword_max_attempt_blocked() {
		int intValue = 0;
		
		if(password_max_attempt_blocked==null ) {
			
			password_max_attempt_blocked = appService.getLogin_password_max_attempt_blocked();
			
		}
		
		
		if(password_max_attempt_blocked!=null && !password_max_attempt_blocked.isEmpty()) {
			intValue = Integer.parseInt(password_max_attempt_blocked);
		}
		
		if(intValue==0) {
			intValue = MAX_ATTEMPT_DEFAULT;
		}
		
		
		return intValue;
	}
  
}
