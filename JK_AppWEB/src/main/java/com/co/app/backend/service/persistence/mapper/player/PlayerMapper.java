package com.co.app.backend.service.persistence.mapper.player;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import com.co.app.model.dto.account.Account;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Score;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.user.User;

//@Component
@Mapper
public interface PlayerMapper {

	public TipoDocumento getTiposDocumento(HashMap<Object, Object> parametrosInOout);
	
    public Player registrarJugador(HashMap<Object, Object> parametrosInOout);
    
    public User validarRegistro(HashMap<Object, Object> parametrosInOout);

    public User validarLog(HashMap<Object, Object> parametrosInOout);
    
    public Player obtenerJugadorPorUsuario(HashMap<Object, Object> parametrosInOout);
    
    public Player obtenerJugadorPorID(HashMap<Object, Object> parametrosInOout);
    
    public Avatar obtenerAvatarPlayer(HashMap<Object, Object> parametrosInOout);
    
    public Pet obtenerPetPlayer(HashMap<Object, Object> parametrosInOout);
    
    public Ship obtenerShipPlayer(HashMap<Object, Object> parametrosInOout);
    
    public Boolean asignarAvatar(HashMap<Object, Object> parametrosInOout);
    
    public Boolean asignarPet(HashMap<Object, Object> parametrosInOout);
    
    public Boolean asignarShip(HashMap<Object, Object> parametrosInOout);
    
    public HashMap<String, Object> obtenerMatchesPlayer(HashMap<Object, Object> parametrosInOout);
    
    public Score obtenerMatchScore(HashMap<Object, Object> parametrosInOout);
    
    public Boolean actualizarTipoJugador(HashMap<Object, Object> parametrosInOout);

	public World obtenerPlayerWordls(HashMap<Object, Object> parametrosInOout);
	
	public Game obtenerGamesWorld(HashMap<Object, Object> parametrosInOout);

	public Game obtenerGamesByPlayer(HashMap<Object, Object> parametrosInOout);

	public Boolean guardarPeguntas(HashMap<Object, Object> parametrosInOout);

	public Game obtenerObjectivesPlayer(HashMap<Object, Object> parametrosInOout);
	
	public Account obtenerCuentaPorPlayerCliente(HashMap<Object, Object> parametrosInOout);

    public Boolean actualizarInfoPlayer(HashMap<Object, Object> parametrosInOout);


	
	
    
}