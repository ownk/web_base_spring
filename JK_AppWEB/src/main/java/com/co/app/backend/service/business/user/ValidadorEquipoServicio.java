package com.co.app.backend.service.business.user;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.co.app.backend.service.business.commons.EncryptDecryptDESService;
import com.co.app.model.dto.session.EquipoSeguroDTO;


@Service
public class ValidadorEquipoServicio  {
	

	

	private Logger logger = LoggerFactory.getLogger(ValidadorEquipoServicio.class);
	
	
	private static final String COOKIE_IS_SECURE     = "equiposeguro.cookie.isSecure";
	private static final String COOKIE_DIASEXP       = "equiposeguro.cookie.diasExpiracion";
	private static final String COOKIE_PREFIJO_NOMB  = "equiposeguro.cookie.nombreprefijo";
	
	private final static String SEPARADOR     = "&&";
	private final static String MAP_SEPARADOR = "==";
	private final static String IPLOCAL       = "ipL";
	private final static String IPREMOTA      = "ipR";
	private final static String HOST_REMOTO   = "hR";
	private final static String USERAGENT     = "uA";
	private final static String NOMBRE_SERVIDOR = "nS";
	private static final String USUARIO       = "u";
	
	public EquipoSeguroDTO validaEquipo(EquipoSeguroDTO dto) throws Exception {		
		
		if(dto.getCookie()!=null){
			
			try {
				
				String cookieCifrada = dto.getCookie();
				
				EncryptDecryptDESService encryptDecryptBasic = new EncryptDecryptDESService();
				String cookieDescifrada = encryptDecryptBasic.decrypt(cookieCifrada);
				
				StringTokenizer tokens = new StringTokenizer(cookieDescifrada,SEPARADOR);
				Map<String, String> mapCookie = new HashMap<String, String>();
				
				while(tokens.hasMoreElements()){
					String actualElement = tokens.nextToken();
					StringTokenizer stringTokenizer = new StringTokenizer(actualElement, MAP_SEPARADOR);
					String key = stringTokenizer.nextToken();
					String value = stringTokenizer.nextToken();
					mapCookie.put(key, value);
				}
			
				/*
				 * Se valida la informacion almacenada en la cookie contra el dto.
				 * */
			
				String ipLocal = ""+dto.getIpLocal();
				String ipRemota = ""+dto.getIpRemota();
				String hostRemoto =""+dto.getHostRemoto();
				String userAgent = ""+dto.getUserAgent();
				String nombreServidor = ""+dto.getNombreServidor();
				String usuario = ""+dto.getUsuario();
				
				if(!ipLocal.equals(mapCookie.get(IPLOCAL))){
					dto.setEquipoValido(false);
					return dto;
				}else if(!ipRemota.equals(mapCookie.get(IPREMOTA))){
					dto.setEquipoValido(false);
					return dto;
				}else if(!hostRemoto.equals(mapCookie.get(HOST_REMOTO))){
					dto.setEquipoValido(false);
					return dto;
				}else if(!userAgent.equals(mapCookie.get(USERAGENT))){
					dto.setEquipoValido(false);
					return dto;
				}else if(!nombreServidor.equals(mapCookie.get(NOMBRE_SERVIDOR))){
					dto.setEquipoValido(false);
					return dto;
				}else if(!usuario.equals(mapCookie.get(USUARIO))){
					dto.setEquipoValido(false);
					return dto;
				}else{
					dto.setEquipoValido(true);
					return dto;
				}
			
			} catch (Exception e) {
				dto.setEquipoValido(false);
				return dto;
			}
		}else{
			dto.setEquipoValido(false);
			return dto;
		}
	}

	
	public EquipoSeguroDTO generarValorCookie(EquipoSeguroDTO dto) throws Exception {

		/**
		 * Generacion de cookie
		 * =======================================
		 * Se realiza la creacion de la Cookie con
		 * los siguiente parametros:
		 *  
		 * ipLocal;
		 * ipRemota;
		 * hostRemoto;
		 * UserAgent(Navegador);
		 * NombreServidor;
		 * usuario
		 */

		EncryptDecryptDESService encryptDecryptBasic = new EncryptDecryptDESService();

		String cookieSinCifrar = 		 IPLOCAL        +MAP_SEPARADOR+dto.getIpLocal()   +SEPARADOR
				+IPREMOTA       +MAP_SEPARADOR+dto.getIpRemota()  +SEPARADOR
				+HOST_REMOTO    +MAP_SEPARADOR+dto.getHostRemoto()+SEPARADOR
				+USERAGENT      +MAP_SEPARADOR+dto.getUserAgent() +SEPARADOR
				+NOMBRE_SERVIDOR+MAP_SEPARADOR+dto.getNombreServidor()+SEPARADOR
				+USUARIO        +MAP_SEPARADOR+dto.getUsuario();

		String cookieCifrada = encryptDecryptBasic.encrypt(cookieSinCifrar);

		String isCookieSegura = "N";
		
		//Dias de expiracion
		try {
			String valor = "30";
			if(valor!=null){

				dto.setNroDiasExperaCookie(valor);
			}else{
				dto.setNroDiasExperaCookie("30");
			}

		} catch (Exception e) {
			dto.setNroDiasExperaCookie("30");
		}

		//Cookie secure
		try {

			if("S".equals(isCookieSegura)){
				dto.setIsCookieSecure(true);
			}else{
				dto.setIsCookieSecure(false);
			}	

		} catch (Exception e) {
			dto.setIsCookieSecure(false);
		}

		//Prefijo nombre coookie
		dto.setCookie(cookieCifrada);
		dto.setNombreCookie(generarNombreCookie(dto.getUsuario()));

		return dto;

	}


	public String generarNombreCookie(String usuario) throws Exception {
		String prefijoCookie =null;
		EncryptDecryptDESService encryptDecryptBasic = new EncryptDecryptDESService();
		String usuarioCifrado = encryptDecryptBasic.encrypt(usuario);
		
		
		
		try {
			
			String valor = "JH_APP_COOKIE";
			if(valor!=null){
		
				prefijoCookie = valor;
			}else{
				prefijoCookie = "FS_COOKIE_EQUIPO_SEGURO";
			}
			
		} catch (Exception e) {
			prefijoCookie = "FS_COOKIE_EQUIPO_SEGURO";
		}
		
		
		
		return prefijoCookie+"_"+usuarioCifrado;
	}


}
