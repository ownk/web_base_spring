package com.co.app.backend.service.business.autentication;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.Modulo;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.UserDetailsSpring;
import com.co.app.model.dto.user.LoginUser;
import com.co.app.model.dto.user.User;

@Component
public class GeneradorSessionApp {

	private Logger logger = LoggerFactory.getLogger(GeneradorSessionApp.class);

	public static String TOKEN = "1346798520";

	// atributos que se registran en el inicio de session
	public static String SESSION_TOKEN = "SESSION_TOKEN";
	public static String SESSION_USUARIO = "SESSION_USER";
	public static String SESSION_APP = "SESSION_APP";
	
	

	@Autowired
	IPNavegadorService ipNavegadorServicio;
	
	@Autowired
	ClientService clientService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	AppService appService;
	
	
	public SessionAppUsuario iniciarSession(Locale locale, HttpServletRequest request, User usuario, List<Modulo> modulos) {
		
		
		logger.info("Iniciando session de usuario...");

		SessionAppUsuario sessionAppUsuario = null;

		if (usuario != null) {
			HttpSession session = null;

			// Se crea la session
			if (request.getSession(false) != null) {

				logger.info("Inicializando parametros");	
				limpiarInfoSession(request);
				session = request.getSession();

			} else {
				// Se crea la session
				logger.info("No existe session. Se crea nueva session");
				session = request.getSession();

			}
			
			
			

			// Se registran los parametros de inicio
			sessionAppUsuario = generarSessionAppUsuario(locale, session, request, usuario, modulos);

			// Si la sessionUsuario no fue generada se debe invalidar la session http
			if (sessionAppUsuario == null) {
				session.invalidate();
			}else {
				
				
				
				
				ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);
				LoginUser loginUser = new LoginUser();
				loginUser.setLogin_browser(sessionAppUsuario.getNavegador());
				loginUser.setLogin_datereg(sessionAppUsuario.getSysdate());
				loginUser.setLogin_ip(sessionAppUsuario.getIp());
				loginUser.setLogin_user(usuario.getUser_user());
				loginUser.setLogin_pass(usuario.getUser_pass());
				loginUser.setLogin_nick(usuario.getUser_nick());
				
				
				try {
					Boolean registroInicioSession = userService.registroInicioSessionUser(controlOperacion, loginUser);
				} catch (RespuestaFallidaServicio e) {
					logger.debug(e.getMensaje());
					
				}
			}
		} else {

			logger.info("Usuario invalido");
		}

		return sessionAppUsuario;

	}

	private SessionAppUsuario generarSessionAppUsuario(Locale locale, HttpSession session, HttpServletRequest request, User usuario, List<Modulo> menuUsuario) {
    
		
		Date sysdate;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e1) {
			logger.error(e1.getMensaje());
			
			sysdate = new Date();
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetailsSpring userPrincipal = (UserDetailsSpring) auth.getPrincipal();
		
		logger.debug("Usuario Autenticado Spring "+userPrincipal);
		if(userPrincipal!=null) {
			logger.debug("Usuario Autenticado Spring Name: "+userPrincipal.getName());
			logger.debug("Usuario Autenticado Spring Username: "+userPrincipal.getUsername());
			logger.debug("Usuario Autenticado Spring ID:  "+userPrincipal.getId());
		}
		
		SessionAppUsuario sessionUsuario = null;

		logger.info("Registrando Parametros de inicio de session.");

		// Se agrega el token para identificar que la session es controlada
		session.setAttribute(SESSION_TOKEN, TOKEN);

		// Se agrega el usuario en la session
		session.setAttribute(SESSION_USUARIO, usuario);
		
		
		// Se crea sesion con informacion basica
		sessionUsuario = new SessionAppUsuario(locale, session, usuario, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request), menuUsuario, sysdate );
		
		
		//Se consulta la informacion del cliente asociado al usuairo
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionUsuario);
		Client client =null;
		
		try {
			client = clientService.getClientePorUser(controlOperacion, usuario.getUser_user());
			
			if(client!=null) {
				
				sessionUsuario.setCliente(client);
				
			}
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());
			
		}
		
		session.setAttribute(SESSION_APP, sessionUsuario);
		
		return sessionUsuario;
	}

	public SessionAppUsuario getSessionAppUsuario(HttpServletRequest request) {

		SessionAppUsuario sessionAppUsuario = null;

		HttpSession session = request.getSession(false);

		// Se revisa si ya existe session
		if (session != null) {

			// Se revisa si los parametros de inicio correctos
			String sessionInit = (String) session.getAttribute(GeneradorSessionApp.SESSION_TOKEN);
			if (sessionInit != null && sessionInit.equals(TOKEN)) {

				User usuario = (User) session.getAttribute(SESSION_USUARIO);
				if (usuario != null) {

					sessionAppUsuario = (SessionAppUsuario) session.getAttribute(GeneradorSessionApp.SESSION_APP);
					if (sessionAppUsuario != null) {
						sessionAppUsuario.setHttpSession(session);
						sessionAppUsuario.setUsuario(usuario);
						sessionAppUsuario.setUrlActual(request.getRequestURI().substring(request.getContextPath().length() + 1));

					}
	
				} else {
					logger.info("Session APP INVALIDA. Persona no registrado");
				}

			} else {

				logger.info("Session APP INVALIDA. Usuario no registrado");
			}

		} else {
			logger.info("Session APP INVALIDA. Información incosistente");
		}

		return sessionAppUsuario;

	}
	
	
	public void limpiarInfoSession(HttpServletRequest request) {

		HttpSession session = request.getSession(false);

		if (session != null) {

			session.removeAttribute(SESSION_APP);
			session.removeAttribute(SESSION_TOKEN);
			session.removeAttribute(SESSION_USUARIO);
			session.removeAttribute(TOKEN);

		}

	}

	public void cerrarSession(HttpServletRequest request) {

		HttpSession session = request.getSession(false);

		if (session != null) {

			limpiarInfoSession(request);
			request.getSession(false).invalidate();
		}

	}

}
