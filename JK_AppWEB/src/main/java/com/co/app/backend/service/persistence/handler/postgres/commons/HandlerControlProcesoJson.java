package com.co.app.backend.service.persistence.handler.postgres.commons;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.postgresql.util.PGobject;

import com.co.app.model.dto.general.ControlOperacionBD;
import com.google.gson.Gson;

public class HandlerControlProcesoJson extends BaseTypeHandler implements TypeHandler<Object> {

	@Override
	public Object getResult(ResultSet arg0, String arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getResult(CallableStatement arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType arg3) throws SQLException {
		
		ControlOperacionBD control = (ControlOperacionBD) parameter;

		try {
			if (control != null) {
				
				List<ControlOperacionBD> listControl = new ArrayList<>();
				
				listControl.add(control);
				
				PGobject pGobject = new PGobject();
				pGobject.setType("text");
				
				//---
				Gson gson = new Gson();
				String jsonResponse = gson.toJson(listControl);
				
				//---
				pGobject.setValue(jsonResponse);
				
				ps.setObject(i, pGobject, Types.OTHER);
			}else {
				ps.setObject(i, null, Types.OTHER);
			}
		}
		catch (Exception e) {
			throw new SQLException(e);
		}
	}

	@Override
	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
