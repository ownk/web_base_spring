package com.co.app.backend.service.persistence.mapper.app;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.App;


//@Component
@Mapper
public interface AppMapper {
    
    
	
	public  App getAppInfo();
	
	public ParametroConfiguracionGeneral getConfGeneral(HashMap<Object, Object> parametrosInOout);

	public Boolean crearConfGeneral(HashMap<Object, Object> parametrosInOout);

	public Boolean actualizarConfGeneral(HashMap<Object, Object> parametrosInOout);

	public String getFecha(HashMap<Object, Object> parametrosInOout);


}