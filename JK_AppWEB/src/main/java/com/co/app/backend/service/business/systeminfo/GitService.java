/*
 * MIT License 
 * 
 * Copyright (c) 2018 Ownk
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 */

package com.co.app.backend.service.business.systeminfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.co.app.model.dto.systeminfo.CommitInfo;
import com.co.app.model.dto.systeminfo.GitInfo;

/**
 * <h1>RestService</h1>
 * 
 * Esta clase se encarga de obtener la información de git
 *
 * @author Master_Zen (Ownk) 
 * @version 1.0
 */




@Component
public class GitService {
	
	@Autowired
	ResourceLoader resourceLoader;
	
	//lee la consulta .git y la guarda el en dto de git
	public GitInfo getGitInfo() throws IOException {
		GitInfo gitInfo = new GitInfo();
		
		gitInfo.setVersion(readTagFile());
		gitInfo.setCommitInfo(readCommitFile());
		return gitInfo;
	}
	
	
	private String readTagFile() {
		
		String line;
		try {
			
			Resource resource = resourceLoader.getResource("classpath:static/gitinfo/tag.log");
			
			// read it with BufferedReader
			BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			line = br.readLine();
			br.close();
			return line;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return line = "ERROR al leer tag.log";
		}
	}
	
	private CommitInfo readCommitFile() {
		
		CommitInfo commitInfo = new CommitInfo();
		
		try {
			
			
			// read it with BufferedReader
			Resource resource = resourceLoader.getResource("classpath:static/gitinfo/commitInfo.log");
			
			// read it with BufferedReader
			BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			

			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				i++;
				if(i <= 6) {
					switch (i) {
		            case 1:  String[] parts = line.split("\\(");
		            		 String commitStr = parts[0];
		            		 
		            		 commitInfo.setCommit(commitStr);
		            		 
		            		 String part2 = parts[1];
		            		 parts = part2.split(",");
		            		 int length = parts.length;
				             String headStr = parts[0];
				             
				             commitInfo.setHead(headStr);
				             
				             if(parts.length > 1) {
					             String part = parts[1].substring(1, 7);
					             
					             if(part.contains("origin")) {
						             String originStr = parts[1];
						             
						             commitInfo.setOrigin(originStr);
						             
					             }else {
					            	 String tagStr = parts[1];
					            	 commitInfo.setTag(tagStr);
					            	 if(parts[length-1] != null && length != 1) {
						            	 String originStr = parts[length-1];
							             commitInfo.setOrigin(originStr.replace(")", ""));
						             }
					             }
				             }
		                     break;
		            case 2:  commitInfo.setAutor(line);
		                     break;
		            case 3:  commitInfo.setFecha(line);
		                     break;
		            case 5:  commitInfo.setCommitMessage(line);
		                     break;
					}
				}
			}
			br.close();
			
			return commitInfo;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			commitInfo.setCommitMessage("ERROR al leer commitInfo.log");
			return commitInfo;
		}

		
	}
}
