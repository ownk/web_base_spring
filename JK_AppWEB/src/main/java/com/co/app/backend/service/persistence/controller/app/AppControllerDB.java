package com.co.app.backend.service.persistence.controller.app;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.persistence.CommonsParameterBDGenerator;
import com.co.app.backend.service.persistence.ResponseFailedBD;
import com.co.app.backend.service.persistence.ResponseFailedBDGenerator;
import com.co.app.backend.service.persistence.controller.client.ClientControllerDB;
import com.co.app.backend.service.persistence.mapper.app.AppMapper;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.App;
import com.co.app.model.dto.general.ConstantesBD;
import com.co.app.model.dto.general.ControlOperacionBD;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeBD;

@Component
public class AppControllerDB {

	private Logger logger = LoggerFactory.getLogger(ClientControllerDB.class);

	@Autowired
	private AppService appServicio;

	@Autowired
	AppMapper mapper;

	@Autowired
	ResponseFailedBDGenerator generadorExcepcionBD;

	public App getInfoApp() {

		return mapper.getAppInfo();

	}

	public List<ParametroConfiguracionGeneral> getConfGeneral(ControlOperacionBD controlOperacionBD)
			throws ResponseFailedBD {

		String pgcriptoKey = appServicio.getPGCryptoKey();

		String nombreMetodo = new Object() {
		}.getClass().getEnclosingMethod().getName();

		List<ParametroConfiguracionGeneral> listaCnfGeneral = null;

		try {

			// Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD,
					this.getClass(), nombreMetodo);

			// Se crean los parametros de negocio
			parametrosInOout.put(ConstantesBD.PARAMETER_PGCRYPTO_KEY, pgcriptoKey);
			mapper.getConfGeneral(parametrosInOout);

			String codigoRespuesta = (String) parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta" + codigoRespuesta);

			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);

			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta != null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				listaCnfGeneral = (List<ParametroConfiguracionGeneral>) parametrosInOout.get("p_tcnfg");
			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			// session.close();
		}

		return listaCnfGeneral;

	}

	public Boolean crearConfGeneral(ControlOperacionBD controlOperacionBD, ParametroConfiguracionGeneral confGeneral)
			throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {
		}.getClass().getEnclosingMethod().getName();

		Boolean sinErrores = false;

		try {

			// Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD,
					this.getClass(), nombreMetodo);

			// Se crean los parametros de negocio
			parametrosInOout.put("p_tcnfg", confGeneral);

			mapper.crearConfGeneral(parametrosInOout);

			String codigoRespuesta = (String) parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta" + codigoRespuesta);

			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta != null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			// session.close();
		}

		return sinErrores;

	}

	public Boolean actualizarConfGeneral(ControlOperacionBD controlOperacionBD,
			ParametroConfiguracionGeneral confGeneral) throws ResponseFailedBD {

		HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();

		String nombreMetodo = new Object() {
		}.getClass().getEnclosingMethod().getName();

		Boolean sinErrores = false;

		try {

			// Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD,
					this.getClass(), nombreMetodo);

			// Se crean los parametros de negocio
			parametrosInOout.put("p_tcnfg", confGeneral);

			mapper.actualizarConfGeneral(parametrosInOout);

			String codigoRespuesta = (String) parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta" + codigoRespuesta);

			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta != null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {

				sinErrores = true;

			} else {

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;

		} finally {
			// session.close();
		}

		return sinErrores;

	}

	public Date getSysdate(ControlOperacionBD controlOperacionBD)	throws ResponseFailedBD {


		String nombreMetodo = new Object() {
			
		}.getClass().getEnclosingMethod().getName();

		Date fecha = null;
		try {

			// Se crean parametros de control de operacion
			CommonsParameterBDGenerator fabricaParametros = new CommonsParameterBDGenerator();
			HashMap<Object, Object> parametrosInOout = new HashMap<Object, Object>();
			parametrosInOout = (HashMap<Object, Object>) fabricaParametros.getParametrosComunes(controlOperacionBD,
					this.getClass(), nombreMetodo);

			// Se crean los parametros de negocio
			mapper.getFecha(parametrosInOout);

			String codigoRespuesta = (String) parametrosInOout.get(ConstantesBD.PARAMETER_COD_RPTA);
			logger.debug("Codigo respuesta" + codigoRespuesta);

			List<ErrorBD> errores = (List<ErrorBD>) parametrosInOout.get(ConstantesBD.PARAMETER_ERRORES);
			List<MensajeBD> mensajes = (List<MensajeBD>) parametrosInOout.get(ConstantesBD.PARAMETER_MENSAJES);

			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					codigoRespuesta, nombreMetodo, errores, mensajes);

			if (codigoRespuesta != null && codigoRespuesta.equals(ConstantesBD.COD_EXITOSO)) {
				fecha =  (Date) parametrosInOout.get("p_date");
			} else {

				codigoRespuesta = ConstantesBD.COD_FALLIDO;

				throw exceptionGeneralBD;

			}

		} catch (ResponseFailedBD e) {

			throw e;

		} catch (Exception e) {
			ResponseFailedBD exceptionGeneralBD = generadorExcepcionBD.generarException(this.getClass(),
					ConstantesBD.COD_FALLIDO, nombreMetodo, e);
			throw exceptionGeneralBD;
		} finally {
			// session.close();
		}

		return fecha;

	}
	
}
