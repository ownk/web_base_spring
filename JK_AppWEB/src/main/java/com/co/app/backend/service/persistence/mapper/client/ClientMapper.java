package com.co.app.backend.service.persistence.mapper.client;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import com.co.app.model.dto.account.Account;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.client.RegisterCode;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.user.User;


//@Component
@Mapper
public interface ClientMapper {
    
    public Client registrarCliente(HashMap<Object, Object> parametrosInOout);

	public User validarRegistro(HashMap<Object, Object> parametrosInOout);

	public Client getClientePorIdent(HashMap<Object, Object> parametrosInOout);

	public Account getCuentasPorCliente(HashMap<Object, Object> parametrosInOout);

	public Client getCodigosAcPorCliente(HashMap<Object, Object> parametrosInOout);
	
	public User getUsuariosPorCliente(HashMap<Object, Object> parametrosInOout);

	public Player getPlayersPorCliente(HashMap<Object, Object> parametrosInOout);

	public Client getClientePorUser(HashMap<Object, Object> parametrosInOout);

	public Boolean actualizarImagenCliente(HashMap<Object, Object> parametrosInOout);

	public Game getGamesPorAcode(HashMap<Object, Object> parametrosInOout);

	public Boolean crearAccessCode(HashMap<Object, Object> parametrosInOout);

	public Boolean crearRegisterCode(HashMap<Object, Object> parametrosInOout);

	public Boolean inactivarAccessCode(HashMap<Object, Object> parametrosInOout);

	public Boolean inactivarRegisterCode(HashMap<Object, Object> parametrosInOout);

	public Boolean eliminarRegisterCode(HashMap<Object, Object> parametrosInOout);

	public Game getGamesPorCliente(HashMap<Object, Object> parametrosInOout);
	
	public Client getClientes(HashMap<Object, Object> parametrosInOout);

	public TipoDocumento getTiposDocumento(HashMap<Object, Object> parametrosInOout);

	public RegisterCode getCodigosRegistro(HashMap<Object, Object> parametrosInOout);

	public TemplateGames getPlantillas(HashMap<Object, Object> parametrosInOout);

	public Boolean crearCantAccessCode(HashMap<Object, Object> parametrosInOout);

	public Boolean crearCantRegisterCode(HashMap<Object, Object> parametrosInOout);

	public Boolean crearPlantilla(HashMap<Object, Object> parametrosInOout);

	public Player getReportePlayersClient(HashMap<Object, Object> parametrosInOout);	
	
    public User getPlayersClientEmailNoConfirmado(HashMap<Object, Object> parametrosInOout);

    public User getUsuariosEmailNoConfirmado(HashMap<Object, Object> parametrosInOout);
    
    public Boolean createProcessLoadMasiveAccessCode(HashMap<Object, Object> parametrosInOout);

    public Boolean createDetailsLoadMasiveAccessCode(HashMap<Object, Object> parametrosInOout);

    public Boolean updateProcessLoadMasiveAccessCode(HashMap<Object, Object> parametrosInOout);

    public Boolean updateDetailsLoadMasiveAccessCode(HashMap<Object, Object> parametrosInOout);
    
    public DetailLoadMasiveAccessCode getDetailsLoadMasiveAccessCode(HashMap<Object, Object> parametrosInOout);

    public LoadMasiveAccessCodes getProcessLoadMasiveAccessCode(HashMap<Object, Object> parametrosInOout);

    public Boolean deleteAccessCode(HashMap<Object, Object> parametrosInOout);

	public Client getClientePorId(HashMap<Object, Object> parametrosInOout);

    public Boolean registerSendEmailAcode(HashMap<Object, Object> parametrosInOout);
    
    public Boolean registerSendEmailRcode(HashMap<Object, Object> parametrosInOout);
    
    public Boolean deleteRegisterCode(HashMap<Object, Object> parametrosInOout);
    



}