package com.co.app.backend.service.persistence.mapper.game;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Question;
import com.co.app.model.dto.game.Score;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;

//@Component
@Mapper
public interface GameMapper {

    public HashMap<String, Object> obtenerAvatars(HashMap<Object, Object> parametrosInOout);
    
    public HashMap<String, Object> obtenerPets(HashMap<Object, Object> parametrosInOout);
    
    public HashMap<String, Object> obtenerShips(HashMap<Object, Object> parametrosInOout);
        
	public World obtenerWorlds(HashMap<Object, Object> parametrosInOout);
    
	public Game obtenerGamePorId(HashMap<Object, Object> parametrosInOout);

	public World obtenerMundoPorId(HashMap<Object, Object> parametrosInOout);

	public Player obtenerRankingGlobalPlayers(HashMap<Object, Object> parametrosInOout);

	public Player obtenerRankingEmpresaPlayers(HashMap<Object, Object> parametrosInOout);
	
    public Player obtenerJugadorporNick(HashMap<Object, Object> parametrosInOout);

	public Player obtenerPosicionRankingEmpresaPl(HashMap<Object, Object> parametrosInOout);

	public Player obtenerRankingGlobalPorFecha(HashMap<Object, Object> parametrosInOout);

	public Player obtenerRankingEmpresaPorFecha(HashMap<Object, Object> parametrosInOout);

	public Question getQuestions(HashMap<Object, Object> parametrosInOout);

	public Game obtenerJuegos(HashMap<Object, Object> parametrosInOout);

	public LearningObjective getLearningObjectivesByGame(HashMap<Object, Object> parametrosInOout);	
 
	public World obtenerMundosPorGame(HashMap<Object, Object> parametrosInOout);

	public World obtenerJuegosPorMundo(HashMap<Object, Object> parametrosInOout);
}