package com.co.app.backend.service.business.autentication;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class LoginUsernameAttemptService {
	
	Logger logger = LoggerFactory.getLogger(LoginUsernameAttemptService.class);
	
	@Autowired
	AppService appService; 
	
    private final  int MAX_ATTEMPT_DEFAULT = 3;
    private final  int MIN_TIME_BLOCKED_DEFAULT = 180;
    private static LoadingCache<String, Integer> attemptsCache;
    private String username_max_attempt_blocked;
    
    ParametroConfiguracionGeneral parameter;
    
    
    public LoginUsernameAttemptService() {
        super();
        
        attemptsCache = CacheBuilder.newBuilder().expireAfterWrite(MIN_TIME_BLOCKED_DEFAULT, TimeUnit.SECONDS).build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(final String key) {
                return 0;
            }
            
            
        });
    }

    //

    public static void loginSucceeded(final String key) {
        attemptsCache.invalidate(key);
    }

    public void loginFailed(final String key) {
    	
        int attempts = 0;
        try {
            attempts = attemptsCache.get(key);
        } catch (final ExecutionException e) {
            attempts = 0;
        }
        attempts++;
        attemptsCache.put(key, attempts);
    }

    public  boolean isBlocked(final String key) {
    	
        try {
        	Boolean isBlocked = false;
        	
        	
        	isBlocked = attemptsCache.get(key) >= getUsername_max_attempt_blocked();
        	
        	return isBlocked;
        } catch (final ExecutionException e) {
        	logger.error(e.getMessage());
            return false;
        }
    }

	public int getUsername_max_attempt_blocked() {
		int intValue = 0;
		
		if(username_max_attempt_blocked==null ) {
			
			username_max_attempt_blocked = appService.getLogin_username_max_attempt_blocked();
			
		}
		
		
		if(username_max_attempt_blocked!=null && !username_max_attempt_blocked.isEmpty()) {
			intValue = Integer.parseInt(username_max_attempt_blocked);
		}
		
		if(intValue==0) {
			intValue = MAX_ATTEMPT_DEFAULT;
		}
		
		
		
		return intValue;
	}



    
   
}
