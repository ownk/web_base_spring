package com.co.app.backend.service.persistence.handler.postgres.client;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.co.app.backend.service.persistence.handler.postgres.commons.BaseTypeHandler;
import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.game.Player;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class HandlerLoadMasiveAccessCodesJson extends BaseTypeHandler implements TypeHandler<Object> {
	
	Logger logger = LoggerFactory.getLogger(HandlerLoadMasiveAccessCodesJson.class);

	@Override
	public Object getResult(ResultSet arg0, String arg1) throws SQLException {
		try {
			
			Array value = arg0.getArray(arg1);
			logger.info(""+value);
		} catch (Exception e) {
			Object value = arg0.getObject(arg1);
			logger.info(""+value);
		}
		return null;
	}

	@Override
	public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
		List<LoadMasiveAccessCodes> lmac = new ArrayList<>();
		
		String respuesta = null;
		try {
			respuesta = (String) cs.getString(columnIndex);
		} catch (Exception e) {
			respuesta = null;
		}
		if (respuesta != null) {
			
			try {
				ObjectMapper mapper = new ObjectMapper();
				LoadMasiveAccessCodes[] registrosArreglo = mapper.readValue(respuesta, LoadMasiveAccessCodes[].class);

				if (registrosArreglo != null & registrosArreglo.length > 0) {
					for(LoadMasiveAccessCodes registro : registrosArreglo) {
						lmac.add(registro);
					}
				}

			} catch (Throwable e) {
				logger.error(e.getMessage());
				
			}
			
		}
		return lmac;
	}
	
	@Override
	public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType arg3) throws SQLException {
		try {
			
			List<LoadMasiveAccessCodes> objeto = new ArrayList<LoadMasiveAccessCodes>();
			LoadMasiveAccessCodes lmac =null;
			
			try {
				objeto = (List<LoadMasiveAccessCodes>) parameter;
			} catch (Exception e) {
			}
			
			try {
				lmac=(LoadMasiveAccessCodes) parameter;
			} catch (Exception e) {
			}
		
			if (lmac!=null) {
				objeto.add(lmac);
			}
			
			
			if (objeto != null) {
				
			
				
				PGobject pGobject = new PGobject();
				pGobject.setType("text");
				
				//---
				Gson gson = new Gson();
				String jsonResponse = gson.toJson(objeto);
				
				//---
				pGobject.setValue(jsonResponse);
				
				ps.setObject(i, pGobject, Types.OTHER);
			}else {
				ps.setObject(i, null, Types.OTHER);
			}
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
