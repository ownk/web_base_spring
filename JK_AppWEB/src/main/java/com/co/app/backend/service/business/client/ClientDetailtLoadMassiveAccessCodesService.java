package com.co.app.backend.service.business.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.CodeFlow.ClinitAdder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.client.SendEmailAccessCode;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.mail.Email;

@Service
public class ClientDetailtLoadMassiveAccessCodesService {
	
	
	
	
	private Logger logger = LoggerFactory.getLogger(ClientDetailtLoadMassiveAccessCodesService.class);

	@Autowired
	ClientService clientService;

	
	@Autowired
	AppService appService;
	
	@Autowired
	I18nService i18nService;
	
	
	@Autowired
	MailServicio mailServicio;
	
	
	/*
	 * Es importante que el metodo de registrar detalles no este
	 * en la misma clase que el metodo asyncrono de ejecucion
	 * de la carga. Es con el fin de que se ejecuten en diferentes hilos
	 */

	@Async(value = "executorThreadAcodesMailSending")
	public CompletableFuture<TaskCreateSendAccesCode> createAccesCodeAndSendAsync(Locale locale, ControlOperacionServicio controlOperacionServicio, LoadMasiveAccessCodes process,DetailLoadMasiveAccessCode detailProcess) {
		logger.info(Thread.currentThread().getName());
		
		Boolean sinErrores = true;
		String mensaje = "";
		String error = "";
		
		TaskCreateSendAccesCode taskCreateSendAccesCode = new TaskCreateSendAccesCode();
        taskCreateSendAccesCode.setDetailLoadMasiveAccessCode(detailProcess);
        
		
        try {
        	

            logger.info("Process createAccesAndSendAccesCode :"+detailProcess.getDlmac_email());
          
          
            List<DetailLoadMasiveAccessCode> listDetailsProcess = new ArrayList<DetailLoadMasiveAccessCode>();
            listDetailsProcess.add(detailProcess);
            
            List<DetailLoadMasiveAccessCode> listDetailsProcessCreated= clientService.createDetailsLoadMasiveAccessCode(controlOperacionServicio, listDetailsProcess );
            
            
            try {
            	if(listDetailsProcessCreated!=null && listDetailsProcessCreated.size()>0) {
            		
            		detailProcess = listDetailsProcessCreated.get(0);
            		
            		//Check email
            		
            		Boolean isEmailValid =mailServicio.isStructureEmailOK(detailProcess.getDlmac_email());
            		
            		if(isEmailValid) {

                        AccessCode accessCode = new AccessCode();
                        accessCode.setAcode_client(process.getLmac_client());
                        accessCode.setAcode_templt(process.getLmac_templt());
                        accessCode.setAcode_state(AccessCode.STATE_ACTIVE);
                        
                        TemplateGames templateGames = TemplateGames.createBasicTemplate(process.getLmac_templt());
                        
                        List<AccessCode> listAcodes = null;
                        
                        try {
                			listAcodes = clientService.crearAccessCodes(controlOperacionServicio,accessCode, templateGames);
                		} catch (RespuestaFallidaServicio e1) {
                			error = e1.getMensaje();
                			
                		}
            			
            			if(listAcodes!=null && listAcodes.size()>0) {
            				
            				accessCode = listAcodes.get(0);
            				
            				String acode_acode = accessCode.getAcode_acode();
            				String acode_code = accessCode.getAcode_code();
            				String toEmail= detailProcess.getDlmac_email();
            				String toName = detailProcess.getDlmac_name();
            				
            				try {
            					
            					long delayLong; 
            					try {
            						String delay = appService.getThread_acodes_mailsending_delay();
                					delayLong = Long.parseLong(delay);
                					
                					
    							} catch (Exception e) {
    								delayLong = new Long("5000");
    							}
            					
            					
            					logger.debug("Process createAccesAndSendAccesCode accessCode create:"+listAcodes.get(0).getAcode_acode());
                				mensaje = i18nService.translate("ClientLoadMassiveAccesCodesAnsyc.info_detail.MSG_OK_CREATE_SEND_MAIL");
                				mensaje = mensaje+" "+"Code: "+acode_code;
                				

            					SendEmailAccessCode sendEmailAccessCode = new SendEmailAccessCode();
                				sendEmailAccessCode.setSeac_acode(acode_acode);
                				sendEmailAccessCode.setSeac_email(toEmail);
                				sendEmailAccessCode.setSeac_name(toName);
                				sendEmailAccessCode.setSeac_user(controlOperacionServicio.getUsuario());
                				
            					
            					
            					Thread.sleep(delayLong);
            					clientService.sendEmailAccesCode(locale, controlOperacionServicio, accessCode, sendEmailAccessCode);
            					
            					
                				
                				sinErrores = true;
                				
                				
                				
            					
    						} catch (Exception e) {
    							
    							logger.error(e.getMessage());
    							error = e.getMessage();
    							mensaje = i18nService.translate("ClientLoadMassiveAccesCodesAnsyc.info_detail.MSG_ERROR_CREATE_SEND_MAIL");
    							mensaje = mensaje+" "+error;
    							clientService.deleteAccessCode(controlOperacionServicio, acode_acode);
    							sinErrores = false;
    							
    						}
            				
            				
            				
            			}else {
            				sinErrores=false;
            				mensaje = i18nService.translate("ClientLoadMassiveAccesCodesAnsyc.info_detail.MSG_ERROR_CREATE_SEND_MAIL")+" "+error;
            				
            			}
            			
            			
            		}else {
            			sinErrores=false;
        				mensaje = i18nService.translate("ClientLoadMassiveAccesCodesAnsyc.info_detail.MSG_ERROR_CREATE_SEND_MAIL");
        				error = i18nService.translate("ClientLoadMassiveAccesCodesAnsyc.info_detail.MSG_ERROR_MAIL_NV");
        				mensaje = mensaje + " " +error;
        				
            		}
            		
            		
            		//Update each process detail
            		if(sinErrores) {
                    	detailProcess.setDlmac_state(DetailLoadMasiveAccessCode.STATE_OK);
        				detailProcess.setDlmac_mnsg(mensaje);
        				clientService.updateDetailsLoadMasiveAccessCode(controlOperacionServicio,listDetailsProcessCreated);
        				
        				
                    }else {
             			logger.error(mensaje);
        				detailProcess.setDlmac_state(DetailLoadMasiveAccessCode.STATE_ERROR);
        				detailProcess.setDlmac_mnsg(mensaje);              
        				clientService.updateDetailsLoadMasiveAccessCode(controlOperacionServicio,listDetailsProcessCreated);

                    }
            		

        					
                	
                    
                }else {
                	sinErrores = false;
                }
			} catch (Exception e) {
				logger.error(e.getMessage());
				sinErrores = false;
				
			}
            
            
            
            taskCreateSendAccesCode.setSinErrores(sinErrores);
            
            
            
            
        } catch (Exception e) {
        	
        	taskCreateSendAccesCode.setSinErrores(false);
        	
            logger.error("Task errror"+e.getMessage());
        }
		
		return CompletableFuture.completedFuture(taskCreateSendAccesCode);
		

    }
	
	
}
