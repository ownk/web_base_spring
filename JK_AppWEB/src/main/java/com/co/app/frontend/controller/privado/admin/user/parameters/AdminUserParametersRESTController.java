package com.co.app.frontend.controller.privado.admin.user.parameters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.AppPararameterCryptoService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@RestController
@RequestMapping("/rest/private/admin/user/parameters")
public class AdminUserParametersRESTController {


	private Logger logger = LoggerFactory.getLogger(AdminUserParametersRESTController.class);
	
	@Autowired	
	IPNavegadorService ipNavegadorServicio; 


	@Autowired
	AutenticadorServicio autenticadorServicio;
	


	@RequestMapping(value = "/crypto",
			method = RequestMethod.POST,  
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public MensajeREST<String> encrypt(@RequestBody String text, HttpServletRequest request, HttpServletResponse response) {

	
		MensajeREST<String> mensajeREST = new MensajeREST<String>();
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {
			
			Boolean isInfoValida = true;

			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {
					if(text!=null) {
						
						try {
							String textToEncrypt = RequestParameterService.decryptText(text);
							String textEncrypt = AppPararameterCryptoService.getInstance().encriptar(textToEncrypt);
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
							mensajeREST.setDetalle(textEncrypt);
							
						} catch (Exception e) {
							
							logger.error(e.getMessage());
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
							 
						}
												

					}else{
						isInfoValida = false;
						
					}
										
					
					if(!isInfoValida) {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
					}
						
					

				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}

			} catch (RespuestaFallidaServicio e) {
				isInfoValida = false;

				logger.error(e.getMensaje());

			}

			if (!isInfoValida) {
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
			}

		} else {

			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
		}
		
		
		return mensajeREST;
    	
    	
	}




}