package com.co.app.frontend.controller.privado.player.game;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;


@Controller
public class PlayerGameDetailsController {
	
	private Logger logger = LoggerFactory.getLogger(PlayerGameDetailsController.class);
	
	
	public static final String URL = "/private/player/game/details";
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	RequestParameterService parametrosRequestServicio;
	
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	@Autowired
	PlayerServicio playerService;
	
	
	@Autowired
	GameServicio gameService;
	
	@PostMapping(URL)
	public ModelAndView getGame(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+PlayerGameDetailsController.URL+"/player_game_details";
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					
					
					//Se consulta parametros 
					Game gameSelected = (Game)parametrosRequestServicio.getRequestParameterToObject(request, "Game", Game.class, null);
					
					if(gameSelected!=null) {
						
						//Se consultan los mundos por jugador
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
						
						String idUsuario = sessionAppUsuario.getUsuario().getUser_user();
						
						Player player = playerService.getPlayerPpalByUser(controlOperacionServicio, idUsuario, true);
						if(player!=null) {
							
							Game game = playerService.obtenerGameByPlayer(controlOperacionServicio, gameSelected.getGame_game(), player.getPlayer_player());
							
							if(game!=null) {
								modelAndView.addObject("player", player);
								modelAndView.addObject("game", game);
								
								
								List<LearningObjective> objetivosPorGame = gameService.getLearningObjectivesByGame(controlOperacionServicio, game.getGame_game());	
								modelAndView.addObject("lrnobjs", objetivosPorGame);
								
								List<Match> matches = playerService.obtenerMatchsByGamePlayer(controlOperacionServicio, game.getGame_game(), player.getPlayer_player(),0,0);
								modelAndView.addObject("matches", matches);
								 
								
							}else {
								isInfoValida = false;
							}
						}else {
							isInfoValida = false;
						}
						
						
					}else {
						
						isInfoValida = false;
					}
					
					
					if(!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

					
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}
			
			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
	}
	
	

}
