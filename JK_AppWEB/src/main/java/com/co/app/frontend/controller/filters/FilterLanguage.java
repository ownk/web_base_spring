package com.co.app.frontend.controller.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;

@Component
@Order(1)
public class FilterLanguage implements Filter{

	private Logger logger = LoggerFactory.getLogger(FilterLanguage.class);

	@Autowired
	PageGeneralInfoGenerator pageGeneralInfoGenerator;
	

	@Override
	public void doFilter(
			ServletRequest request, 
			ServletResponse response, 
			FilterChain chain)	throws IOException, ServletException {
		
			//Languaje validation
			validarLanguaje(request, response, chain);
		
	}

	private void validarLanguaje(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		logger.debug(new Exception().getStackTrace()[0].getMethodName());

		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpResp = (HttpServletResponse) response;
		
		String lang = httpReq.getParameter(ConstantesGeneralesAPP.WEB_LANG_PARAMETER);
		
		
		if(lang!=null && !StringUtils.isEmpty(lang)) {
			
			
			String urlAccessDenied = httpReq.getContextPath()+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO+"?error="+ConstantesGeneralesAPP.ERROR_LANG_NV;
			
			try {

				Boolean isLangValid = pageGeneralInfoGenerator.isLangValid(lang);
				
				if (!isLangValid) {

					httpResp.sendRedirect(urlAccessDenied);
				} else {
					chain.doFilter(request, response);
				}

				
			} catch (Exception e) {
				httpResp.sendRedirect(urlAccessDenied);
			}

			
			
			
		}else {
			chain.doFilter(request, response);
		}
		
		
		

	}

}
