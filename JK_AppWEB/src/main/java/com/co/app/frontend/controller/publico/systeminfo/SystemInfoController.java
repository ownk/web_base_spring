package com.co.app.frontend.controller.publico.systeminfo;


import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.client.FileExcelClientPlayersObjectivesReport;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.integration.IntegrationGameServicio;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.backend.service.business.systeminfo.GitService;
import com.co.app.backend.service.business.systeminfo.SystemService;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.general.App;
import com.co.app.model.dto.systeminfo.GitInfo;
import com.co.app.model.dto.systeminfo.SystemInfo;
  
@Controller
public class SystemInfoController {
	
	public static final String URL = "/public/systeminfo";
	
	
	Logger logger = LoggerFactory.getLogger(SystemInfoController.class);
	
	
	@Autowired
	AppService appServicio; 
	
	@Autowired
	UserService usuarioServicio; 
	
	@Autowired
	IntegrationGameServicio integratnServicio; 
	
	@Autowired
	PlayerServicio jugadorServicio; 
	
	@Autowired
	ClientService clienteServicio; 
	
	@Autowired
	GameServicio juegoServicio; 
	
	@Autowired
	FileExcelClientPlayersObjectivesReport report; 
	
	@Autowired
	MailServicio mailServicio; 
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	
	@Autowired
	SystemService  systemService;
	
	@Autowired
	GitService gitService;
	
	@GetMapping(ConstantesGeneralesAPP.URL_SYSTEMINFO)
    public ModelAndView systemInfo(Locale locale, HttpServletRequest request, HttpServletResponse response) {
    	
    	
    	
    	/*
    	 * Variables por controllador
    	 * ==================================
    	 * Se crean las variables especificas
    	 * por controlador 
    	 * ----------------------------------
    	 */
		
		ModelAndView modelAndView = new ModelAndView();
		
    	App app = appServicio.getApp();
    	
    	/*
		  * =====================================
		  *  Informacion git
		  * =====================================
		*/
		
		try {
			GitInfo gitInfo = gitService.getGitInfo();
			modelAndView.addObject("git", gitInfo);
			
		} catch (IOException e) {
			
		}
		
		
		
		/*
		  * =====================================
		  * Informacion system Info
		  * =====================================
		*/
		
		SystemInfo systemInfo = systemService.getInfo();
		
		
		/*
		 * =====================================
		 * Informacion Rest.
		 * =====================================
		 */
		modelAndView.addObject("mensaje", "mensaje ownk");
    	modelAndView.addObject("request_contextPath", request.getContextPath());
    	modelAndView.addObject("response_contentType", response.getContentType());
    	modelAndView.addObject("locale", locale.getCountry());
    	modelAndView.addObject("system", systemInfo);
    	modelAndView.addObject("app", app);
    	
    	
		
    	
    	
    	/*
    	 * Vista HTML por controllador
    	 * ==================================
    	 * Se especifica la vista html que
    	 * se utilizara para la visualizacion
    	 * de informacion
    	 * ----------------------------------
    	 */
    	
    	String view = ConstantesGeneralesAPP.PAGES_HTML+"/public/systeminfo/systeminfo";
		modelAndView.setViewName(view);
		return modelAndView;
    	
    	
    	
    }
	
	
	
    	
    
   

}