package com.co.app.frontend.controller.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.spring.LoginSpringService;
import com.co.app.backend.service.business.user.ValidatorUniqueSessionService;
import com.co.app.model.dto.session.ResponseValidationUniqueSessionDTO;
import com.co.app.model.dto.session.SessionAppUsuario;

@Component
@Order(2)
public class FilterUniqueSession implements Filter{

	private Logger logger = LoggerFactory.getLogger(FilterUniqueSession.class);

	
	@Autowired
	private LoginSpringService  loginSpringService;
	
	@Autowired
	private AutenticadorServicio  autenticadorServicio;
	
	@Autowired
	private ValidatorUniqueSessionService validadorUniqueSessionService;


	@Override
	public void doFilter(
			ServletRequest request, 
			ServletResponse response, 
			FilterChain chain)	throws IOException, ServletException {
		
		
		
		
			//Unique session validation
			validarSessionActive(request, response, chain );
			
			

	}

	private void validarSessionActive(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		logger.debug(new Exception().getStackTrace()[0].getMethodName());

		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpResp = (HttpServletResponse) response;

		
		
		if(httpReq!=null && httpReq.getRequestURI().contains(ConstantesGeneralesAPP.PRIVATE_PAGE_PATH)) {
			
			
			SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(httpReq);
			 
			
			if (sessionAppUsuario!=null && sessionAppUsuario.getHttpSession()!=null) {
				
				String urlAccessDenied = httpReq.getContextPath()+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO+"?error="+ConstantesGeneralesAPP.ERROR_UNIQUE_SESSION_NV;
				
				logger.debug("Validando unique session for "+sessionAppUsuario.getUsuario().getUser_user()+" ID Session: "+sessionAppUsuario.getHttpSession().getId());
				
				try {

					/* 
					 * ===============================================
					 * Validacion IP y Navegador 
					 * ===============================================
					 * 
					 * Si es servicio que requiere autenticacion se
					 * valida que el usuario registrado tenga el 
					 * mismo ip y navegador de una session ya iniciada
					 * 
					 */
					

					ResponseValidationUniqueSessionDTO respuestaValidacionDto = 
							validadorUniqueSessionService.validarUniqueSession(sessionAppUsuario.getUsuario().getUser_user(), httpReq);
					if (respuestaValidacionDto != null && !respuestaValidacionDto.isValido()) {

						
						httpResp.sendRedirect(urlAccessDenied);
						

					} else {
						chain.doFilter(request, response);
					}

					
				} catch (Exception e) {
					httpResp.sendRedirect(urlAccessDenied);
				}

			} else {
				chain.doFilter(request, response);
			}
			
			
			
		}else {
			chain.doFilter(request, response);
		}
		
		
		

	}

}
