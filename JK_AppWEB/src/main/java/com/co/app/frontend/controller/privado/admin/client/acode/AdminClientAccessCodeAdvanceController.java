package com.co.app.frontend.controller.privado.admin.client.acode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientLoadMassiveAccessCodesService;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.client.FileExcelClientAccessCodesAdvance;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.frontend.controller.publico.error.AppInfoNotValidException;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.exception.FileStorageException;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.User;


@Controller
public class AdminClientAccessCodeAdvanceController {
	
	private Logger logger = LoggerFactory.getLogger(AdminClientAccessCodeAdvanceController.class);
	
	
	private static final String EVENT_CANCEL_PROCESS = "CANCEL_PROCESS";
	
	public static final String URL = "/private/admin/client/acodes/advance";

	
	@Autowired
	private AutenticadorServicio autenticadorServicio;
	
	@Autowired
	private PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private RequestParameterService parametrosRequestServicio;
	
	
	@Autowired
	private FileExcelClientAccessCodesAdvance fileExcelClientAccessCodesAdvance;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private ClientLoadMassiveAccessCodesService clientLoadMassiveAccesCodesAnsycService; 
	
	
	
	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getProcess(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/admin_client_acodes_advance";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);

					Client client = (Client)parametrosRequestServicio.getRequestParameterToObject(request, "Client", Client.class, null);
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					String event = request.getParameter("event");
					
					if(client!=null) {
						
						client = clientService.getClienteByID(controlOperacionServicio, client.getClient_client());
						
						if(client!=null) {
							
							
							
							if(event!=null) {
								
								
								if(event.equals(EVENT_CANCEL_PROCESS)) {
									
									
									MensajeApp mensajeApp = new MensajeApp();
									mensajeApp.setTipoMensaje(MensajeApp.TIPO_ADVERTENCIA);
									mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_CANCEL_OPER_BY_USER);
									sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
									
								}
								
							}
							
							
							
							
							List<TemplateGames> templates = clientService.getPlantillasListaJuegos(controlOperacionServicio, client.getClient_client());
							
							
							modelAndView.addObject("templates", templates);
							modelAndView.addObject("client", client);
							
							
							List<LoadMasiveAccessCodes> processes = clientService.getProcessLoadMasiveAccessCode(controlOperacionServicio, client.getClient_client(), null);
							
							if(processes!=null && processes.size()>0) {
								modelAndView.addObject("processes", processes);
								
							}
							
							String idFileTemp = generateIDFileTemp();
								
							modelAndView.addObject("idFileTemp", idFileTemp);
							
							
							
						}else {
							isInfoValida = false;
						}
					}else {
						isInfoValida= false;
					}
					
					
					 
					if(!isInfoValida) {
							modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

						
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}
			
			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
	}
	
	
	
    
    @PostMapping(URL+"/downloadTemplate")
    public ResponseEntity<Resource> downloadTemplate (HttpServletRequest request) throws Exception {
        // Load file as Resource
		
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		Boolean isInfoValida = true;
		
		if(sessionAppUsuario!=null) {
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				
				if(isAccessoValido) {
					
					Client client = (Client)parametrosRequestServicio.getRequestParameterToObject(request, "Client", Client.class, null);
					
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					if(client!=null) {
						
						client = clientService.getClienteByID(controlOperacionServicio, client.getClient_client());
						
						if(client!=null) {
							File fileGEnerado;
							try {
								

								
								ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);
								List<Player> players = clientService.getPlayersPorCliente(controlOperacion, client.getClient_client());
								
								
								fileGEnerado = fileExcelClientAccessCodesAdvance.getTemplate(client.getClient_client());
								
								
								if(fileGEnerado!=null) {
									
									
									Resource resource = fileExcelClientAccessCodesAdvance.loadFileAsResource(client.getClient_client(), fileGEnerado.getName());

							        if(resource!=null) {
								        
								        // Try to determine file's content type
								        String contentType = null;
								        try {
								            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
								        } catch (IOException ex) {
								            logger.info("Could not determine file type.");
								        }
								
								        // Fallback to the default content type if type could not be determined
								        if(contentType == null) {
								            contentType = "application/octet-stream";
								        }
								
								        return ResponseEntity.ok()
								                .contentType(MediaType.parseMediaType(contentType))
								                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
								                .body(resource);
							        }else {
							        	 return null;
							        }
									
									
									
									
								}
								
							} catch (Exception e) {
								logger.error(e.getMessage());
								isInfoValida = false;
							}
							
							
						}
					}	
					
				}else {
					isInfoValida = false;
				}
			
				
			
			
			} catch (RespuestaFallidaServicio e1) {
				logger.debug(e1.getMensaje());
			}
			
			
			
			
			
			
		}
		logger.error(ConstantesGeneralesAPP.ERROR_INFO_NV);
		throw new AppInfoNotValidException(ConstantesGeneralesAPP.ERROR_INFO_NV);
		
		
        
    }

	
	
    @PostMapping(URL+"/uploadFileTemp")
	public ModelAndView uploadFileTemp(HttpServletRequest request) {

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/admin_client_acodes_advance_uploadfiletemp";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				
				if (isAccessoValido) {
					
					
					
					
					Boolean sinErrores = true;

					String idCLient = null;
					String idFileTemp = null;
					
					ArrayList<MultipartFile> files = parametrosRequestServicio.getMultipartFiles(request);

					Map<String, Object> parameters = parametrosRequestServicio.getMultipartParams(request);

					try {
						if (parameters != null) {
							idCLient = (String) parameters.get("idClient");
							idFileTemp  = (String) parameters.get("idFileTemp");
							
							
							
							
							
							if (idCLient != null && idFileTemp!=null) {
								
								ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
								
								Client client = clientService.getClienteByID(controlOperacionServicio, idCLient);
								
								if(client!=null) {
									if (files != null && files.size() > 0) {

										for (MultipartFile multipartFile : files) {

											sinErrores = uploadFileTemp(sessionAppUsuario, idCLient, idFileTemp, multipartFile);
											
										}
										
										modelAndView.addObject("client", client);
										

									} else {

										sinErrores = false;

										logger.error("ERR_NO_FILES");
									}
									
								}else {
									sinErrores = false;

									logger.error("ERR_CLIENT_NE");
								}
								
								

							} else {

								sinErrores = false;

								logger.error("ERR_CLINT_INFO_NV");

							}

						} else {
							sinErrores = false;

							logger.error("ERR_PAGE_INFO_NV");

						}

					} catch (Exception e) {
						sinErrores = false;

						logger.error(e.getMessage());
					}

					generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);

					if (!sinErrores) {
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);

						logger.error("ERR_CLINT_INFO_NV");

					} else {
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);

					}

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

		}

		modelAndView.setViewName(view);
		return modelAndView;

	}
    
    
    @PostMapping(URL+"/processFile")
    public ModelAndView processFile(Locale locale, HttpServletRequest request, HttpServletResponse response,  RedirectAttributes redirect) {
    	ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/admin_client_acodes_advance_processfile";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					String idClient = request.getParameter("idClient");
					
					
					if(idClient!=null) {
						
						ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);
						
						Client client = clientService.getClienteByID(controlOperacion, idClient);
						
						if(client!=null) {

							String idFileTemp = request.getParameter("idFileTemp");
							String group_games  = request.getParameter("group_games");
							String process_name  = request.getParameter("process_name");
							
														
							if(idFileTemp!=null &&  group_games!=null && process_name!=null ) {
								
								Boolean sinErrores = false;
								
								LoadMasiveAccessCodes loadMasiveAccessCodes = processInfoFile(locale, sessionAppUsuario, client.getClient_client(), idFileTemp, group_games, process_name);
								ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
								
								if(loadMasiveAccessCodes!=null) {
									
									sinErrores = true;
									
									MensajeApp mensajeApp = new MensajeApp();
									mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
									mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
									listaMensajesApp.add(mensajeApp);
									
									
								}else {
									
									MensajeApp mensajeApp = new MensajeApp();
									mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
									mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_OPER_WITH_ERRORS);
									listaMensajesApp.add(mensajeApp);
								}
								
								redirect.addFlashAttribute("mensajesApp", listaMensajesApp);
								redirect.addFlashAttribute("client", client);
								redirect.addFlashAttribute("sinErrores", sinErrores );
								redirect.addFlashAttribute("process", loadMasiveAccessCodes );
								
								view = "redirect:"+URL+"/processFile";

								modelAndView.setViewName(view);
								return modelAndView;

								
								
							}else {
								isInfoValida = false;
							}
							
						}else {
							isInfoValida = false;
						}
						 
						
					}else {
						 isInfoValida = false;
					}
					 
					if(!isInfoValida) {
							modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

						
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}
			
			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
	}
    
    @GetMapping(URL+"/processFile")
    public ModelAndView respuestaProcess(Model model,HttpServletRequest request ) {
    	
    	ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/admin_client_acodes_advance_processfile";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/admin_client_acodes_advance_processfile";
					
					ArrayList<MensajeApp> mensajesAPP = (ArrayList<MensajeApp>) model.asMap().get("mensajesApp");
					Client client = (Client) model.asMap().get("client");
					Boolean sinErrores = (Boolean ) model.asMap().get("sinErrores");
					
					

					if(client!=null) {
						
						
						modelAndView.addObject("client", client);
						if(sinErrores!=null && sinErrores) {
						
							LoadMasiveAccessCodes loadMasiveAccessCodes = (LoadMasiveAccessCodes) model.asMap().get("process");
							modelAndView.addObject("process", loadMasiveAccessCodes);
							modelAndView.addObject("ok", true);
							
							
						}else {
							modelAndView.addObject("error", true);
							
						}
						

						
					}else {
						
						if(mensajesAPP==null) {
							mensajesAPP = new ArrayList<MensajeApp>();
						}
						
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						mensajesAPP.add(mensajeWeb);
						
					}	
					
					generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, mensajesAPP);
					generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
					
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}	
				
		
		modelAndView.setViewName(view);
		return modelAndView;

	}
    
	
    
    private Boolean uploadFileTemp(SessionAppUsuario sessionAppUsuario, String idClient, String idFileTemp, MultipartFile file) {
		
		
    	String finalFileName = idFileTemp+".xlsx";
    	
    	String rutaBase = fileExcelClientAccessCodesAdvance.getBasePathStorageClient(idClient);
    	
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        // Check if the file's name contains invalid characters
		if(fileName.contains("..")) {
		    throw new FileStorageException("ERR_FILE_INV_PATH" + fileName);
		}
		
		
		if(!fileName.endsWith(".xlsx")) {
			throw new FileStorageException("ERR_FILE_INV_XLSX" + fileName);
			
		}

		// Copy file to the target location (Replacing existing file with the same name)
		Path fileStorageLocation = Paths.get(rutaBase).toAbsolutePath().normalize();

		try {
		    Files.createDirectories(fileStorageLocation);
		    
		    Path targetLocation = fileStorageLocation.resolve(finalFileName);
		    Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		    
		    
		    
		    File excelToRead = targetLocation.toFile();

		    if(excelToRead!=null && excelToRead.exists()) {
		    	return true;
		    	
		    }
		    

		    
		} catch (Exception ex) {
			
		    throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
		}
		
		
		
		
		return false;

	}
	
	private LoadMasiveAccessCodes processInfoFile(Locale locale, SessionAppUsuario sessionAppUsuario, String idClient,  String idFileTemp, String idGroupGames, String processName) {
		
		
    	
    	String finalFileName = idFileTemp+".xlsx";
    	
    	String rutaBase = fileExcelClientAccessCodesAdvance.getBasePathStorageClient(idClient);
    	
        Path fileStorageLocation = Paths.get(rutaBase).toAbsolutePath().normalize();

		try {
		    Path targetLocation = fileStorageLocation.resolve(finalFileName);
		   
		    File excelToRead = targetLocation.toFile();

		    if(excelToRead!=null && excelToRead.exists()) {
		    	
		    	List<DetailLoadMasiveAccessCode> details = fileExcelClientAccessCodesAdvance.readDetails(idClient, excelToRead);
		    	
		    	ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
		    	
		    	LoadMasiveAccessCodes process = clientLoadMassiveAccesCodesAnsycService.registerNewProcess(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), idClient, idGroupGames, excelToRead,processName, details);
		    	
		    	if(process!=null) {
		    		
		    		
		    		//Se debe ejcutar de forma asincrona los detalles
		    		clientLoadMassiveAccesCodesAnsycService.executeProcessAsync(locale, controlOperacionServicio, process, details);
		    		
		    		return process;
		    	}
		    }
		} catch (Exception ex) {
			
		  
		}
		
		
		
		
		return null;

	}
	
	
	private String generateIDFileTemp() {
		String random = RandomStringUtils.random(10, true, true);
		Date sysdate =null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e1) {
			sysdate = new Date();
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
    	String fecha = format.format(sysdate);
		
		format = new SimpleDateFormat("HHmmssSSS");
		String hora = format.format(sysdate);
		
		String idFileTemp = fecha+"_"+hora+"_"+random;
		return idFileTemp;
	}
	
	
	
   

 
	
	
	

}
