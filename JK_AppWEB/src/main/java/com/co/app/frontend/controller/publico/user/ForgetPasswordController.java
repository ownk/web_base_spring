package com.co.app.frontend.controller.publico.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA1Service;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.captcha.RecaptchaService;
import com.co.app.backend.service.business.captcha.RecaptchaService.RecaptchaResult;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.user.ForgetPasswordTokenGenerator;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.user.PasswordChange;
import com.co.app.model.dto.user.PasswordValidationResponse;
import com.co.app.model.dto.user.TokenRecoverPassword;
import com.co.app.model.dto.user.User;

@Controller
public class ForgetPasswordController {


	public static final String URL_BASE = "/public/user/forgetpassword";
	public static final String URL_REQUEST =URL_BASE+"/request";
	public static final String URL_CREATE =URL_BASE+"/create";
	public static final String URL_RESPONSE =URL_BASE+"/response";
	public static final String URL_CONFIRM =URL_BASE+"/confirm";
	public static final String URL_SAVE =URL_BASE+"/save_password";	

	private static final String PARAMETRO_CONFIRMACION_REGISTRO = "confID";

	Logger logger = LoggerFactory.getLogger(ForgetPasswordController.class);

	@Autowired
	UserService usuarioServicio; 

	@Autowired
	IPNavegadorService ipNavegadorServicio; 

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	ForgetPasswordTokenGenerator generadorTokenOlvidoPassword;

	@Autowired
	MailServicio mailServicio;

	@Autowired
	AppService appServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	AutenticadorServicio autenticadorServicio;


	@Autowired
	RecaptchaService recaptchaServicio;


	@Autowired
	I18nService i18nservice;

	@Autowired
	UserService userService;



	@ModelAttribute("recaptchaSiteKey")
	public String getRecaptchaSiteKey() {
		return recaptchaServicio.getRecaptchaSiteKey();
	}

	@GetMapping(URL_REQUEST)
	public ModelAndView solicitudOlvidoPassword(Locale locale, HttpServletRequest request, HttpServletResponse response) throws RespuestaFallidaServicio {

		ModelAndView modelAndView = new ModelAndView();

		String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
		modelAndView.addObject("key", uniqueKey);

		String view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_create";
		modelAndView.setViewName(view);

		return modelAndView;
	}


	@PostMapping(URL_CREATE)
	public ModelAndView registrarOlvidoPassword(Locale locale, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirect) {

		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		Boolean sinErrores = false;

		String view = null;

		TokenRecoverPassword tokenOlvidoPass = null;

		//Se consulta parametros 
		String email = request.getParameter("email");

		//Validacion de CAPTCHA
		Boolean isCaptchaValido = false;

		String recaptchaResponse = request.getParameter("g-recaptcha-response");
		if ( recaptchaResponse != null) {

			// I didn't use remoteip here, because it is optional,
			// Reference: https://developers.google.com/recaptcha/docs/verify
			RecaptchaResult result = recaptchaServicio.getResult("", recaptchaResponse);

			if (!result.isSuccess()) {

				logger.error("ERROR_CAPTCHA_NV");

				MensajeApp mensajeApp = new MensajeApp(); 
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);


			}else {
				isCaptchaValido = true;
			}
		}




		ModelAndView modelAndView = new ModelAndView();

		String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
		modelAndView.addObject("key", uniqueKey);
		if(email!=null && isCaptchaValido) {


			String emailDecrypt = RequestParameterService.decryptText(email);

			//Generar otp
			String otp = generadorTokenOlvidoPassword.generarOTP();



			Email emailDto = null;

			if(otp!=null) {

				//Crear 
				ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP,ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request) );


				try {
					sinErrores = usuarioServicio.olvidoPassword(controlOperacion, otp, emailDecrypt);

					//Envio de link de confirmacion
					if(sinErrores) {




						//Generar token de registro
						tokenOlvidoPass = generadorTokenOlvidoPassword.generarToken(emailDecrypt, otp);

						String tokenConfirmacionEnconde = generadorTokenOlvidoPassword.obtenerTokenCifrado(tokenOlvidoPass);

						String urlBase = appServicio.getPublicoURLBase();

						String urlConfirmacion = urlBase+appServicio.getContextoAPP()+URL_CONFIRM+"?"+PARAMETRO_CONFIRMACION_REGISTRO+"="+tokenConfirmacionEnconde;

						String emailSubject = i18nservice.translate("forgetpassword.email.subject");
						String emailMessage = i18nservice.translate("forgetpassword.email.message");






						emailDto = new Email(mailServicio.getMailUsername(), emailDecrypt,emailSubject,emailMessage);

						Map<String, Object> parameterMap = new HashMap<String, Object>();

						ParametroConfiguracionGeneral parameterAppLogoUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_LOGO_INTERNET_URL);

						String urlAppLogo = "";
						if(parameterAppLogoUrl!=null) {
							urlAppLogo = parameterAppLogoUrl.getCnfg_valor();
						}

						ParametroConfiguracionGeneral parameterAppPageUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_PAGE_INTERNET_URL);

						String urlAppPage = "";
						if(parameterAppPageUrl!=null) {
							urlAppPage = parameterAppPageUrl.getCnfg_valor();
						}


						String emailHeaderTitle = i18nservice.translate("forgetpassword.email.header.title");
						String emailHeaderSubtitle = i18nservice.translate("forgetpassword.email.header.subtitle");
						String emailContentGreeting = i18nservice.translate("forgetpassword.email.content.greeting");
						String emailContentMessage = i18nservice.translate("forgetpassword.email.content.message");
						String emailContentFooter = i18nservice.translate("forgetpassword.email.content.footer");
						String emailContentButtonConfirm = i18nservice.translate("forgetpassword.email.content.button_confirm");

						parameterMap.put("emailHeaderTitle", emailHeaderTitle);
						parameterMap.put("emailHeaderSubtitle", emailHeaderSubtitle);
						parameterMap.put("emailContentGreeting", emailContentGreeting);
						parameterMap.put("emailContentMessage", emailContentMessage);
						parameterMap.put("emailContentFooter", emailContentFooter);
						parameterMap.put("emailContentButtonConfirm", emailContentButtonConfirm);

						parameterMap.put("urlAppPage", urlAppPage);
						parameterMap.put("urlAppLogo", urlAppLogo);


						parameterMap.put("urlConfirm", urlConfirmacion );

						emailDto.setParameterMap(parameterMap);





					}


				} catch (Exception e) {
					logger.error(e.getMessage());
					sinErrores = false;
				}

				//Envio de email de confirmacion
				if(sinErrores && emailDto!=null) {

					try {


						mailServicio.sendHtmlTemplateEmail(locale, emailDto, MailServicio.HTML_TEMPLATE_EMAIL_FORGOT_PASSWORD);

						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
						listaMensajesApp.add(mensajeApp);



					} catch (Exception e) {

						sinErrores= false;

						logger.error("WARN_EMAIL_NO_SEND");
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ADVERTENCIA);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
						listaMensajesApp.add(mensajeApp);
					}


				}else {

					sinErrores = false;
					logger.error("ERROR_REGISTRO_FALLIDO");

					MensajeApp mensajeApp = new MensajeApp();
					mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
					mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
					listaMensajesApp.add(mensajeApp);
				}



			}else {
				logger.error("ERROR_TOKEN_FALLIDO");
				MensajeApp mensajeApp = new MensajeApp();
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);

			}

		}else {
			logger.error("ERROR_INFO_NV");
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			listaMensajesApp.add(mensajeApp);

		}



		redirect.addFlashAttribute("mensajesApp", listaMensajesApp);
		redirect.addFlashAttribute("tokenOlvidoPass", tokenOlvidoPass);
		redirect.addFlashAttribute("accesoValido", true);
		redirect.addFlashAttribute("sinErrores", sinErrores );

		view = "redirect:"+URL_RESPONSE;

		modelAndView.setViewName(view);

		return modelAndView;
	}


	@GetMapping(URL_RESPONSE)
	public ModelAndView respuestaOlvidoPassword(Model model,HttpServletRequest request ) {

		ModelAndView modelAndView = new ModelAndView();


		String view = "redirect:"+URL_REQUEST;

		ArrayList<MensajeApp> mensajesAPP = (ArrayList<MensajeApp>) model.asMap().get("mensajesApp");
		TokenRecoverPassword tokenOlvidoPass = (TokenRecoverPassword) model.asMap().get("tokenOlvidoPass");
		Boolean accesoValido = (Boolean ) model.asMap().get("accesoValido");
		Boolean sinErrores = (Boolean ) model.asMap().get("sinErrores");

		if(accesoValido!=null && accesoValido) {


			if(sinErrores!=null && sinErrores) { 

				if(tokenOlvidoPass!=null) {

					view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_create_ok";

				}else {

					view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_create_error";
				}
			}else {
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_create_error";
			}


			generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, mensajesAPP);


		}else {
			view = "redirect:"+URL_REQUEST;
		}

		modelAndView.setViewName(view);

		return modelAndView;

	}


	@GetMapping(URL_CONFIRM)
	public ModelAndView confirmarOlvidoPassword(Locale locale, HttpServletRequest request, HttpServletResponse response) {


		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		String confimationID = request.getParameter(PARAMETRO_CONFIRMACION_REGISTRO);
		Boolean sinErrores = false;

		ModelAndView modelAndView = new ModelAndView();

		String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
		modelAndView.addObject("key", uniqueKey);


		String view;

		if(confimationID!=null) {

			TokenRecoverPassword token = generadorTokenOlvidoPassword.obtenerTokenOlvidoPassword(confimationID);
			//confirmar registro
			if(token!=null) {

				ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP,ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request) );
				
				String user_email = token.getUser_email();
				
				try {
					User user = userService.getUsuarioPorEmail(controlOperacion, user_email);

					if(user!=null && user.getUser_user()!=null) {
						modelAndView.addObject("confimationID", confimationID);
						modelAndView.addObject("user", user);
						sinErrores = true;
						
					}	

					
				} catch (RespuestaFallidaServicio e) {
					
					logger.debug(e.getMensaje());
				}
				
				if(!sinErrores) {
					logger.error("ERROR_EMAIL_NV");

					MensajeApp mensajeApp = new MensajeApp();
					mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
					mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
					view = "redirect:"+URL_REQUEST;
					listaMensajesApp.add(mensajeApp);
				}
				
				
				
			}else {

				logger.error("ERROR_TOKEN_NL");
				MensajeApp mensajeApp = new MensajeApp();
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);

			}	



			if(sinErrores) {
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_confirm_ok";
			}else{
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_confirm_error";
			}



		}else {
			logger.error("ERROR_CONFIRM_NV");

			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			view = "redirect:"+URL_REQUEST;
		}



		generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, listaMensajesApp);
		modelAndView.setViewName(view);

		return modelAndView;
	}

	@PostMapping(URL_SAVE)
	public ModelAndView cambiarPassword(Locale locale, HttpServletRequest request, HttpServletResponse response) {


		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		String confimationID = request.getParameter(PARAMETRO_CONFIRMACION_REGISTRO);
		Boolean sinErrores = false;

		ModelAndView modelAndView = new ModelAndView();


		String view;

		if(confimationID!=null) {

			TokenRecoverPassword token = generadorTokenOlvidoPassword.obtenerTokenOlvidoPassword(confimationID);
			//confirmar registro
			if(token!=null) {



				String otp = token.getOtp();

				String pass = request.getParameter("Usuario.user_pass");
				String passDecrypt = RequestParameterService.decryptText(pass);

				if(otp!=null && passDecrypt!=null) {

					//Crear solicitud
					ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP,ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request) );

					String email = token.getUser_email();

					User user = new User();

					try {

						//Consultar usuario por email

						user = usuarioServicio.getUsuarioPorEmail(controlOperacion, email);

						String user_name = user.getUser_name();
						String user_nick = user.getUser_nick();

						//Se valida si la contraseña es permitida por el sistema
						PasswordValidationResponse passwordValidationResponse= usuarioServicio.IsPasswordValid(controlOperacion, user_name, user_nick, passDecrypt);

						if(passwordValidationResponse.isValid()) {
							
							
							//Nuevo password igual al actual
							Boolean isSamePass = autenticadorServicio.isPassWordMatch(passDecrypt, user.getUser_pass());
							
							if(!isSamePass) {
								
								
								//Verificar Historial de Password
								Boolean newPass = true;
								List<PasswordChange> historialPass = null;
								historialPass=usuarioServicio.getHistorialPasswordPorUser(controlOperacion, user.getUser_user());
								
								for (PasswordChange passBefore:historialPass) {
									if (newPass) {
										newPass = autenticadorServicio.isPassWordMatch(passDecrypt, passBefore.getPassch_password());
										if (newPass) {
											newPass=false;
										}else {
											newPass=true;
										}
									}
								}
								
								if (newPass) {
									pass= autenticadorServicio.cifrarInfo(passDecrypt);
									
									//Nuevo password igual al actual
	
									sinErrores = usuarioServicio.cambioPassword(controlOperacion, email, otp, pass);
	
									//Envio de link de confirmacion
									if(sinErrores) {		
										logger.error("EXITO_CONFIRM_OK");
	
										MensajeApp mensajeApp = new MensajeApp();
										mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
										mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
										listaMensajesApp.add(mensajeApp);
										
										//Registro el cambio de contraseña
										 
										PasswordChange passch = new PasswordChange();
										passch.setPassch_browser(ipNavegadorServicio.getNavegador(request));
										passch.setPassch_ip(ipNavegadorServicio.getIP(request));
										passch.setPassch_newpass(pass);
										passch.setPassch_password(user.getUser_pass());
										passch.setPassch_user(user.getUser_user());
										usuarioServicio.registroCambioPassword(controlOperacion, passch);
										
	
									}else {
	
										modelAndView.addObject("error", true);
	
										logger.error("ERROR_CONFIRM_FALLIDO");
										MensajeApp mensajeApp = new MensajeApp();
										mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
										mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
										listaMensajesApp.add(mensajeApp);
									}
									
								}else{
									logger.error("ERROR_PASS_USED_BEFORE");
									MensajeApp mensajeApp = new MensajeApp();
									mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
									mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
									listaMensajesApp.add(mensajeApp);
								}
							}else{
								logger.error("ERROR_PASS_USED_BEFORE");
								MensajeApp mensajeApp = new MensajeApp();
								mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
								mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
								listaMensajesApp.add(mensajeApp);
							}
							


						}else {

							modelAndView.addObject("error", true);

							logger.error("ERROR_CONFIRM_FALLIDO");
							MensajeApp mensajeApp = new MensajeApp();
							mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
							mensajeApp.setDatoMensaje(passwordValidationResponse.getCodeResponse());
							listaMensajesApp.add(mensajeApp);


						}


					} catch (Exception e) {
						logger.error("ERROR_SERVICIO:"+e.getMessage());
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
						listaMensajesApp.add(mensajeApp);
					}

				}else{
					logger.error("ERROR_OTP_NL");
					MensajeApp mensajeApp = new MensajeApp();
					mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
					mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
					listaMensajesApp.add(mensajeApp);
				}
			}else {
				logger.error("ERROR_TOKEN_NL");
				MensajeApp mensajeApp = new MensajeApp();
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);
			}	

			if(sinErrores) {
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_changed_ok";
			}else{
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/forgetpassword_confirm_error";
			}

		}else {
			logger.error("ERROR_CONFIRM_NV");
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			view = "redirect:"+URL_REQUEST;
			listaMensajesApp.add(mensajeApp);
		}

		generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, listaMensajesApp);
		modelAndView.setViewName(view);

		return modelAndView;
	}




}