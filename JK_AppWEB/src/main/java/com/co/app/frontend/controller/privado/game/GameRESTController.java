package com.co.app.frontend.controller.privado.game;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Question;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.User;

@RestController
@RequestMapping("rest/private/game")
public class GameRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio gameServicio;
	
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	private Logger logger = LoggerFactory.getLogger(GameRESTController.class);

	
	@RequestMapping(value = "/questions",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Question>> getQuestions(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			List<Question> list = null;

			MensajeREST<List<Question>> mensajeREST = new MensajeREST<List<Question>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					//Se consulta la informacion para configurar personaje
					
					list= gameServicio.getQuestions(controlOperacionServicio);
					mensajeREST.setDetalle(list);
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<Question>> mensajeREST = new MensajeREST<List<Question>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			return mensajeREST; 
		}
		
		 
		
    }
	
	
	@RequestMapping(value = "/worlds",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<World>> getAllWorlds(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			List<World> list = null;

			MensajeREST<List<World>> mensajeREST = new MensajeREST<List<World>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					//Se consulta la informacion para configurar personaje
					list = new ArrayList<World>();
					
					for (int i = 1; i <= 4; i++) {
						World World = new World();
						World.setWorld_world(""+i);
						World.setWorld_descri("GMS_TWORLD.WORLD_DESCRI."+World.getWorld_world());
						World.setWorld_descri("GMS_TWORLD.WORLD_URI");
						list.add(World);
					}
					
					mensajeREST.setDetalle(list);
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<World>> mensajeREST = new MensajeREST<List<World>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			return mensajeREST; 
		}
		
		 
		
    }
	

	@RequestMapping(value = "/avatars",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Avatar>> getAllAvatars(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			List<Avatar> listaAvatars = null;

			MensajeREST<List<Avatar>> mensajeREST = new MensajeREST<List<Avatar>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					//Se consulta la informacion para configurar personaje
					listaAvatars = gameServicio.obtenerAvatars(controlOperacionServicio);
					mensajeREST.setDetalle(listaAvatars);
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<Avatar>> mensajeREST = new MensajeREST<List<Avatar>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			return mensajeREST; 
		}
		
		 
		
    }
	
	

	@RequestMapping(value = "/pets",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Pet>> getAllPets(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			List<Pet> lista = null;

			MensajeREST<List<Pet>> mensajeREST = new MensajeREST<List<Pet>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					//Se consulta la informacion para configurar personaje
					lista = gameServicio.obtenerPets(controlOperacionServicio);
					mensajeREST.setDetalle(lista);
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<Pet>> mensajeREST = new MensajeREST<List<Pet>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			return mensajeREST; 
		}
		
		 
		
    }
	
	@RequestMapping(value = "/ships",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Ship>> getAllShips(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			List<Ship> lista = null;

			MensajeREST<List<Ship>> mensajeREST = new MensajeREST<List<Ship>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					//Se consulta la informacion para configurar personaje
					lista = gameServicio.obtenerShips(controlOperacionServicio);
					mensajeREST.setDetalle(lista);
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<Ship>> mensajeREST = new MensajeREST<List<Ship>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			return mensajeREST; 
		}
		
		 
		
    }
	

	@RequestMapping(value = "/all_games",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Game>> getGames(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			MensajeREST<List<Game>> mensajeREST = new MensajeREST<List<Game>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			Boolean isInfoValida = true;
			
			try {

				
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
						
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					User user= sessionAppUsuario.getUsuario();
					
			
					//Se consulta la informacion para configurar personaje
					List<Game> games = gameServicio.obtenerJuegos(controlOperacionServicio);
						
					if(games!=null) {
						mensajeREST.setDetalle(games);
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
					}else {
						isInfoValida = false;
					}
					
					
					
					if(!isInfoValida) {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
					}
					
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				logger.error(e.getMensaje());
				
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
		
		}else{
			
			MensajeREST<List<Game>> mensajeREST = new MensajeREST<List<Game>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
  
}