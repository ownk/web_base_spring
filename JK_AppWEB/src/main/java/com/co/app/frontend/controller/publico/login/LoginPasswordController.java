package com.co.app.frontend.controller.publico.login;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA1Service;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.GeneradorTokenPasswordBlocked;
import com.co.app.backend.service.business.autentication.LoginPasswordAttemptService;
import com.co.app.backend.service.business.autentication.TokenPasswordBlockedAttemptService;
import com.co.app.backend.service.business.captcha.RecaptchaService;
import com.co.app.backend.service.business.captcha.RecaptchaService.RecaptchaResult;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.spring.LoginSpringService;
import com.co.app.frontend.controller.privado.home.HomePrivateController;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.UserDetailsSpring;

@Controller
public class LoginPasswordController {

	Logger logger = LoggerFactory.getLogger(LoginPasswordController.class);




	@Autowired
	LoginSpringService loginSpringServicio;

	@Autowired
	RecaptchaService recaptchaServicio;


	@Autowired
	AppService appServicio;

	@Autowired
	LoginPasswordAttemptService loginPasswordAttemptService;

	@Autowired
	GeneradorTokenPasswordBlocked generadorTokenPasswordBlocked ;

	@Autowired
	RequestParameterService requestParameterService;
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;




	@ModelAttribute("recaptchaSiteKey")
	public String getRecaptchaSiteKey() {
		return recaptchaServicio.getRecaptchaSiteKey();
	}



	@PostMapping(ConstantesGeneralesAPP.URL_LOGIN_PASSWORD)
	public ModelAndView iniciarSession(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		String redirect = "redirect:"+ConstantesGeneralesAPP.URL_LOGIN_PASSWORD+"?error";
		ModelAndView modelAndView = new ModelAndView();

		
		try {


			String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
			modelAndView.addObject("key", uniqueKey);


			//Se verifica informacion de captcha

			String recaptchaResponse = request.getParameter("g-recaptcha-response");
			redirect = "redirect:"+ConstantesGeneralesAPP.URL_LOGIN_PASSWORD+"?error=incorrectRecaptcha";
			
			if ( recaptchaResponse != null) {

				// I didn't use remoteip here, because it is optional,
				// Reference: https://developers.google.com/recaptcha/docs/verify
				RecaptchaResult result = recaptchaServicio.getResult("", recaptchaResponse);

				if (!result.isSuccess()) {
						
					logger.error("Incorrect Captcha ");
					modelAndView.clear();

				}else {

					//Se verifica informacion de usuario
					Authentication auth = SecurityContextHolder.getContext().getAuthentication();

					UserDetailsSpring userPrincipal = (UserDetailsSpring) auth.getPrincipal();

					String passEncrypt = request.getParameter("pass");

					if(userPrincipal!=null && passEncrypt!=null) {
						String username = userPrincipal.getUsername();

						String passDecrypt = RequestParameterService.decryptText(passEncrypt);

						if(!loginPasswordAttemptService.isBlocked(username)) {




							SessionAppUsuario sessionUsuario =  loginSpringServicio.iniciarSessionUsuario(locale,request, response, username, passDecrypt);

							if(sessionUsuario!=null) {
								modelAndView.clear();
								redirect = "redirect:"+HomePrivateController.URL;
							}else {

								LoginPasswordAttemptService.loginFailed(username);

							}
						}else {


							

							modelAndView.addObject("blocked", true);
							redirect = ConstantesGeneralesAPP.PAGES_HTML+ConstantesGeneralesAPP.URL_LOGIN_PASSWORD;


							if(!TokenPasswordBlockedAttemptService.existToken(username)) {
								//Cifrar el tokenConfirmacionEnconde
								String token = generadorTokenPasswordBlocked.createSendToken(locale, username);

								if(token!=null) {



									TokenPasswordBlockedAttemptService.registerToken(username, token);


								}

							}else {

								String tokenPorValidar = request.getParameter("token");

								tokenPorValidar = RequestParameterService.decryptText(tokenPorValidar);

								String tokenRegistrado = TokenPasswordBlockedAttemptService.getToken(username);						


								if(tokenPorValidar!=null && tokenRegistrado!=null) {
									if(tokenPorValidar.equals(tokenRegistrado)) {
										SessionAppUsuario sessionUsuario =  loginSpringServicio.iniciarSessionUsuario(locale, request, response, username, passDecrypt);

										if(sessionUsuario!=null) {
											TokenPasswordBlockedAttemptService.tokenSucceeded(username);
											LoginPasswordAttemptService.loginSucceeded(username);
											modelAndView.clear();
											redirect = "redirect:"+HomePrivateController.URL;
										}

									}else {
										modelAndView.addObject("tokenError", true);
									}
								}						

							}


						}

					}


				}
			}else {
				modelAndView.clear();
			}


		} catch (Exception e) {
			modelAndView.clear();
			logger.error(e.getMessage());
		}

		modelAndView.setViewName(redirect);
		return modelAndView;

	}


	@GetMapping(ConstantesGeneralesAPP.URL_LOGIN_PASSWORD)
	public ModelAndView mostrarPaginaSolicitudPass(Locale locale) {
		
		
		

		ModelAndView modelAndView = new ModelAndView();
		String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
		modelAndView.addObject("key", uniqueKey);
		

		generadorInfoComunPorPagina.generarLang(modelAndView, locale);
		
		
		modelAndView.setViewName(ConstantesGeneralesAPP.PAGES_HTML+ConstantesGeneralesAPP.URL_LOGIN_PASSWORD);


		return modelAndView;
	}




}
