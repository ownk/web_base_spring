package com.co.app.frontend.controller.privado.world;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;


@Controller
public class WorldDetailsController {
	
	private Logger logger = LoggerFactory.getLogger(WorldDetailsController.class);
	
	
	public static final String URL = "/private/world/details";
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	RequestParameterService parametrosRequestServicio;
	
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	@Autowired
	PlayerServicio playerService;
	

	@Autowired
	GameServicio gameService;
	
	
	@PostMapping(URL)
	public ModelAndView goToWorldDetails(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			view = ConstantesGeneralesAPP.PAGES_HTML+WorldDetailsController.URL+"/world_details";

			

			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
				
					Boolean isInfoValida = true;
					
					
					
					//Se consulta parametros 
					World worldSelected = (World)parametrosRequestServicio.getRequestParameterToObject(request, "World", World.class, null);
					
					if(worldSelected!=null) {
						
						//Se consultan los mundos por jugador
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
							
						List<Game> gamesPorMundo= new ArrayList<Game>();
						World world = gameService.obtenerMundoPorId(controlOperacionServicio, worldSelected.getWorld_world());
						gamesPorMundo = gameService.obtenerJuegosPorMundo(controlOperacionServicio, world.getWorld_world());
						
						modelAndView.addObject("world", world);
						modelAndView.addObject("games", gamesPorMundo);
						
					}else {
						
						isInfoValida = false;
					}
					
					
					if(!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}	
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}
			
			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
	}
	
	

}
