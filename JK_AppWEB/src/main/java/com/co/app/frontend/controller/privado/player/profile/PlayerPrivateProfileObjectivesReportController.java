package com.co.app.frontend.controller.privado.player.profile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.client.FileExcelClientPlayersObjectivesReport;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.frontend.controller.publico.error.AppInfoNotValidException;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.DetalleReporteClientPlayersObjective;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@Controller
public class PlayerPrivateProfileObjectivesReportController {


	private Logger logger = LoggerFactory.getLogger(PlayerPrivateProfileObjectivesReportController.class);

	public static final String URL = "/private/player/profile/private/objectives/report";
	public static final String DOWNLOAD_PATH = "/downloadFile/";
	

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	FileExcelClientPlayersObjectivesReport generadorReportClientPlayersObjectives; 
	
	@Autowired
	PlayerServicio jugadorServicio;
	
	
	

	
	
	@Autowired
	ClientService clientService;

	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getRanking(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML + PlayerPrivateProfileObjectivesReportController.URL + "/player_privateprofile_objectives_report";
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {
					
					Client client = sessionAppUsuario.getCliente();
					
					Boolean isInfoValida = true;
					
					
					
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
										
					String idUsuario = sessionAppUsuario.getUsuario().getUser_user();
					Player player = jugadorServicio.getPlayerPpalByUser(controlOperacionServicio, idUsuario, false);
					
					List<Player> playervista= new ArrayList<>();

					
					
					
					if(client!=null && player!=null) {
						
						
						// Se consulta ranking
						List<Player> players = clientService.getInfoReportePlayersClient(controlOperacionServicio, client.getClient_client(),null);
						
						File fileGEnerado;
						try {
							fileGEnerado = generadorReportClientPlayersObjectives.crearReportePlayers(players, client, player.getPlayer_player());
							
							
							if(fileGEnerado!=null) {
								
								 String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
							                .path(URL+DOWNLOAD_PATH)
							                .path(fileGEnerado.getName())
							                .toUriString();
								
								
								modelAndView.addObject("dowloadPath", fileDownloadUri);
								
							}
							
						} catch (Exception e) {
							logger.error(e.getMessage());
						}
						
						for (Player player1:players) {
							if (player1.getPlayer_player().equals(player.getPlayer_player())) {
								playervista.add(player1);
							}
						}
						
						List<DetalleReporteClientPlayersObjective> detallesReporte = construirDetallesReporte(playervista);
						
						
						modelAndView.addObject("detallesReporte", detallesReporte);
												
						
					}else {
						isInfoValida = false;
					}
					
					
					if(!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

					
					
					
					

				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
			
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}
	
	@GetMapping(URL+DOWNLOAD_PATH+"{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) throws Exception {
		// Load file as Resource
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {

			Client client = sessionAppUsuario.getCliente();

			if (client != null) {
				Resource resource = generadorReportClientPlayersObjectives.loadClientFileAsResource(client.getClient_client(), fileName);

				if (resource != null) {

					// Try to determine file's content type
					String contentType = null;
					try {
						contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
					} catch (IOException ex) {
						logger.info("Could not determine file type.");
					}

					// Fallback to the default content type if type could not be determined
					if (contentType == null) {
						contentType = "application/octet-stream";
					}

					return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
							.header(HttpHeaders.CONTENT_DISPOSITION,
									"attachment; filename=\"" + resource.getFilename() + "\"")
							.body(resource);
				} 

			} 

		} 
				
		logger.error(ConstantesGeneralesAPP.ERROR_INFO_NV+": "+fileName);
		throw new AppInfoNotValidException(ConstantesGeneralesAPP.ERROR_INFO_NV);
		
		
       
    }
	
	
	

	
	
	private List<DetalleReporteClientPlayersObjective> construirDetallesReporte(List<Player> players) {
		
		
		List<DetalleReporteClientPlayersObjective> detalles = new ArrayList<DetalleReporteClientPlayersObjective>();
		
	    
        
        if (players!=null ) {
        	
        	 
        	
        	List<LearningObjective> lrnobjs = new ArrayList<LearningObjective>();
            List<Match> matches = new ArrayList<Match>();
        
            
            for(Player player: players) {
                matches=player.getListMatch();
               
                
                for (Match match:matches) {
                	
                	Game game = match.getGame();
                	
                	lrnobjs = game.getLearningObjectives();
                    
                    for (LearningObjective lrnobj: lrnobjs) {
                        
                    	DetalleReporteClientPlayersObjective detalle = new DetalleReporteClientPlayersObjective();
                    	detalle.setGame(game);
                    	detalle.setMatch(match);
                    	detalle.setPlayer(player);
                    	detalle.setLearningObjective(lrnobj);
                    	
                    	detalles.add(detalle);
                    }
                }
                
              
                
            }
        
          
        
          
        }
    
        return detalles;
    
	}

}

