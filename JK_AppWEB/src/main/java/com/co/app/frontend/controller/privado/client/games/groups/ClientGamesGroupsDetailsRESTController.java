package com.co.app.frontend.controller.privado.client.games.groups;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("rest/private/client/games/groups")
public class ClientGamesGroupsDetailsRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio juegoServicio;
	
	@Autowired
	ClientService clientService;
	
	
	
	
	private Logger logger = LoggerFactory.getLogger(ClientGamesGroupsDetailsRESTController.class);

	
   
    
    
    @RequestMapping(value = "/group",
            method = RequestMethod.POST, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<TemplateGames> getPlantilla(@RequestBody ObjectNode infoTemplate, HttpServletRequest request, HttpServletResponse response) {
	
    	MensajeREST<TemplateGames> mensajeREST = new MensajeREST<TemplateGames>();
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					Client client = sessionAppUsuario.getCliente();
					
					if(infoTemplate !=null && client!=null) {
						
						String idCliente = client.getClient_client();
						String idPlantilla = infoTemplate.get("templt_templt").asText();
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						TemplateGames template = clientService.getPlantillaListaJuegos(controlOperacionServicio, idCliente, idPlantilla);
						
						
						if(template!=null) {
							mensajeREST.setDetalle(template);							
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
							
						}else {
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
							
						}
						
					}else {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
					}
					
						
						
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
    
  
}