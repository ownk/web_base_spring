package com.co.app.frontend.controller.privado.client.profile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientImageProfileService;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.commons.UploadFileResponse;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@Controller
public class ClientPrivateProfileController {

	private Logger logger = LoggerFactory.getLogger(ClientPrivateProfileController.class);

	public static final String URL = "/private/client/profile/private";

	@Autowired
	private AutenticadorServicio autenticadorServicio;

	@Autowired
	private PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	private ClientService clientService;

	@Autowired
	private ClientImageProfileService clientImageService;

	@Autowired
	private RequestParameterService parametrosRequestServicio;

	@GetMapping(URL)
	public ModelAndView privateProfile(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {

			view = ConstantesGeneralesAPP.PAGES_HTML + URL + "/client_privateprofile_details";

			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {

					Boolean isInfoValida = true;

					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					Client client = sessionAppUsuario.getCliente();
					if (client != null) {

						client = clientService.getClienteByID(controlOperacionServicio, client.getClient_client());
						sessionAppUsuario.setCliente(client);
						modelAndView.addObject("client", client);

					} else {
						isInfoValida = false;
					}

					if (!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
								ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
								ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}

	@PostMapping(URL)
	public ModelAndView uploadImage(HttpServletRequest request) {

		List<UploadFileResponse> listResponse = null;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		if (sessionAppUsuario != null) {

			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				if (isAccessoValido) {

					view = ConstantesGeneralesAPP.PAGES_HTML + URL + "/client_privateprofile_details";

					Boolean sinErrores = true;

					String idCLient = null;

					ArrayList<MultipartFile> files = parametrosRequestServicio.getMultipartFiles(request);

					Map<String, Object> parameters = parametrosRequestServicio.getMultipartParams(request);

					try {
						if (parameters != null) {
							idCLient = (String) parameters.get("idClient");

							Client client = sessionAppUsuario.getCliente();

							if (idCLient != null && client != null && idCLient.equals(client.getClient_client())) {
								if (files != null && files.size() > 0) {

									listResponse = new ArrayList<UploadFileResponse>();

									for (MultipartFile multipartFile : files) {

										UploadFileResponse resp = uploadFile(sessionAppUsuario, idCLient,
												multipartFile);

										if (resp != null) {
											listResponse.add(resp);

											client.setClient_image(resp.getRelativePath());
										}

									}

									sessionAppUsuario.setCliente(client);
									modelAndView.addObject("client", client);

								} else {

									sinErrores = false;

									logger.info("ERR_NO_FILES");
								}

							} else {

								sinErrores = false;

								logger.error("ERR_CLINT_INFO_NV");

							}

						} else {
							sinErrores = false;

							logger.error("ERR_PAGE_INFO_NV");

						}

					} catch (Exception e) {
						sinErrores = false;

						logger.error(e.getMessage());
					}

					generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);

					if (!sinErrores) {
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);

						logger.error("ERR_CLINT_INFO_NV");

					} else {
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);

					}

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

		}

		modelAndView.setViewName(view);
		return modelAndView;

	}

	private UploadFileResponse uploadFile(SessionAppUsuario sessionAppUsuario, String idClient,
			MultipartFile file) {
		String fileName = clientImageService.storeFile(idClient, file);

		String imageRelativePath = clientImageService.getBaseRelativePath() + fileName;

		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);

		UploadFileResponse uploadFileClientImageResponse = null;

		try {
			clientService.actualizarImagenCliente(controlOperacion, idClient, imageRelativePath);

			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
					.path(fileName).toUriString();

			return new UploadFileResponse(fileName, fileDownloadUri, imageRelativePath,
					file.getContentType(), file.getSize());

		} catch (RespuestaFallidaServicio e) {

			logger.error(e.getMensaje());
		}

		return uploadFileClientImageResponse;

	}

}
