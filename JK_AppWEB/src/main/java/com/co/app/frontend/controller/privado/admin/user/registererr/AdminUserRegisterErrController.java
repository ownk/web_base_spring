package com.co.app.frontend.controller.privado.admin.user.registererr;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.OtpUser;
import com.co.app.model.dto.user.User;
import com.co.app.model.dto.user.UserPendingConfirmatioRegister;

@Controller
public class AdminUserRegisterErrController {
    
    private static final String EVENT_ACTIVE_USER = "ACTIVE_USER";
    private static final String EVENT_DELETE_USER = "DELETE_USER";


    private Logger logger = LoggerFactory.getLogger(AdminUserRegisterErrController.class);

    public static final String URL = "/private/admin/user";

    @Autowired
    AutenticadorServicio autenticadorServicio;

    @Autowired
    RequestParameterService parametrosRequestServicio;

    @Autowired
    PageGeneralInfoGenerator generadorInfoComunPorPagina;

    @Autowired
    UserService userService;
    
    @Autowired 
    PlayerServicio playerService;
    
    @Autowired 
    ClientService clienServicio;
    
    
    
    @Autowired
    MailServicio mailServicio;
    
    

    @RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
    public ModelAndView getRanking(Locale locale, HttpServletRequest request, HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView();

        String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

        SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

        if (sessionAppUsuario != null) {
            
            view = ConstantesGeneralesAPP.PAGES_HTML + AdminUserRegisterErrController.URL + "/admin_user_register_err";


            try {
                Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

                if (isAccessoValido) {
                    
                    
                    String event = request.getParameter("event");
                    String user_user = request.getParameter("user_user");
                    Boolean isInfoValida = true;
                    
                    
                    if(event!=null) {
                        ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
                        
                        List<User> listUsers = userService.getUsuariosEmailNoConfirmado(controlOperacionServicio);
                        
                        Boolean userValid = isUserValidForClient(listUsers,user_user);
                        
                        if(event.equals(EVENT_ACTIVE_USER)){
                            
                        	
                            
                            if(user_user!=null && userValid) {
                            	
                                User userToSendMail = null;
                                
                                for (User user : listUsers) {
									if (user.getUser_user().equals(user_user)) {
		                                userToSendMail = user;

									}
								}
                                
                                
                            	
                                Boolean sinErrores = userService.activarUsuarioEmailNoConfirmado(controlOperacionServicio, user_user);
                                    
                                
                                if(sinErrores) {
                                	
                                	if(userToSendMail!=null) {
                    					
                    					try {
    										Email emailDto = userService.createEmailUserActivatedManually(controlOperacionServicio, userToSendMail);
    										mailServicio.sendHtmlTemplateEmail(locale, emailDto, MailServicio.HTML_TEMPLATE_EMAIL_USER_ACTIVED_MANUALLY);
    									} catch (Exception e) {
    										logger.error(e.getMessage());
    									}
                                    }
                                	
                                    MensajeApp mensajeApp = new MensajeApp();
                                    mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
                                    mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
                                    sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
                                            
                                }else {
                                    logger.error("ERROR_ACTIVE_USER");
                                        
                                    MensajeApp mensajeApp = new MensajeApp();
                                    mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
                                    mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_OPER_WITH_ERRORS);
                                    sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
                                }
                            }else{
                                isInfoValida = false;
                            }
                        }else if(event.equals(EVENT_DELETE_USER)){
                            
                            
                            if(user_user!=null && userValid) {

                                
                                Boolean sinErrores = userService.eliminarUsuarioEmailNoConfirmado(controlOperacionServicio, user_user);
                                    
                                if(sinErrores) {
                                	
                                	
                                    MensajeApp mensajeApp = new MensajeApp();
                                    mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
                                    mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
                                    sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
                                            
                                }else {
                                    logger.error("ERROR_ACTIVE_USER");
                                        
                                    MensajeApp mensajeApp = new MensajeApp();
                                    mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
                                    mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_GENERAL_NC);
                                    sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
                                }
                            }else{
                                isInfoValida = false;
                            }
                        }else {
                        	isInfoValida = false;
                        }
                        
                        
                        
                    }    
                        
                    if(!isInfoValida) {
                            modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
                            MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
                            sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
                    }else {
                        // Se consulta los parametros del APP
                        ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
                        List<User> listUsers = userService.getUsuariosEmailNoConfirmado(controlOperacionServicio);
                        
                        modelAndView.addObject("users", listUsers);
                            
                    }
    
                }else {
                    modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
                    MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
                    sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
                }

            } catch (RespuestaFallidaServicio e) {

                modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
                MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
                sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

            }

            generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
        }

        modelAndView.setViewName(view);
        return modelAndView;

    }
    
    Boolean isUserValidForClient(List<User> listaUserValids, String idUser) {
    	
    	try {
    		if(idUser!=null) {
    	    	
    	    	for (User user : listaUserValids) {
    				
    	    		if(user.getUser_user().equals(idUser)) {
    	    			return true;
    	    		}
    	    		
    			}
        	}	
			
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
    	
    	
    	return false;
    	
    }
    

}
