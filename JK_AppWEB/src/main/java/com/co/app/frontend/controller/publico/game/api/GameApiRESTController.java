package com.co.app.frontend.controller.publico.game.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.api.GameAPIService;
import com.co.app.model.dto.game.api.auth.GameAPI_AuthRequest;
import com.co.app.model.dto.game.api.auth.GameAPI_AuthResponse;
import com.co.app.model.dto.game.api.commons.GameAPI_RESTMessage;
import com.co.app.model.dto.game.api.match.GameAPI_StartGameMatchRequest;
import com.co.app.model.dto.game.api.match.GameAPI_StartGameMatchResponse;
import com.co.app.model.dto.game.api.match.GameAPI_UpdateGameMatchRequest;
import com.co.app.model.dto.game.api.match.GameAPI_UpdateGameMatchResponse;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/public/game/api")
@Api(value = "GameAPI", produces = MediaType.APPLICATION_JSON_VALUE)
public class GameApiRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	GameAPIService gameApiService;
		
	
	private Logger logger = LoggerFactory.getLogger(GameApiRESTController.class);

	
	@RequestMapping(value = "/authapp/",
            method = RequestMethod.POST, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ApiOperation("Service for the authentication of a partner using a specific sitekey and accesskey. Required for use other services")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = GameAPI_AuthResponse.class)})
	@ResponseBody
    public GameAPI_RESTMessage<GameAPI_AuthResponse> authapp(@ApiParam("AuthRequest information for a new Auth.") @RequestBody GameAPI_AuthRequest authRequest, HttpServletRequest request, HttpServletResponse response) {
    	
    	
		GameAPI_AuthResponse responseAuth = null;
		GameAPI_RESTMessage<GameAPI_AuthResponse> GameAPI_RESTMessage = new GameAPI_RESTMessage<GameAPI_AuthResponse>();
		
		
		logger.info("auth authRequest...{}",authRequest);	
		
		
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
		try {
			responseAuth = gameApiService.authAPP(controlOperacion, authRequest);
			GameAPI_RESTMessage.setData(responseAuth);
			GameAPI_RESTMessage.setStatusCode(GameAPI_RESTMessage.REST_OK);
			
		} catch (RespuestaFallidaServicio e) {
			responseAuth = null;
			logger.error(e.getMensaje());
			GameAPI_RESTMessage.setStatusCode(GameAPI_RESTMessage.REST_ERROR_INFO_NV);
		}
		
		
		return GameAPI_RESTMessage;
    }
	
	@RequestMapping(value = "/match/start",
            method = RequestMethod.POST, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@ApiOperation("Service to start a match for a specific game using gameToken")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = GameAPI_StartGameMatchResponse.class)})
    public GameAPI_RESTMessage<GameAPI_StartGameMatchResponse> initGameMatch(@ApiParam("Information for init a new match of a specific game") @RequestBody GameAPI_StartGameMatchRequest startGameRequest, HttpServletRequest request, HttpServletResponse response) {
		logger.info("initGame initGameRequest...{}",startGameRequest);	
		GameAPI_StartGameMatchResponse initGameResponse = null;
		GameAPI_RESTMessage<GameAPI_StartGameMatchResponse> GameAPI_RESTMessage = new GameAPI_RESTMessage<GameAPI_StartGameMatchResponse>();
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
		try {
			initGameResponse = gameApiService.startMatch(controlOperacion, startGameRequest);
			GameAPI_RESTMessage.setData(initGameResponse);
			GameAPI_RESTMessage.setStatusCode(GameAPI_RESTMessage.REST_OK);
			
		} catch (RespuestaFallidaServicio e) {
			initGameResponse = null;
			logger.error(e.getMensaje());
			GameAPI_RESTMessage.setStatusCode(GameAPI_RESTMessage.REST_ERROR_INFO_NV);
		}
		
		
		return GameAPI_RESTMessage;
    }
	
	@RequestMapping(value = "/match/update",
            method = RequestMethod.POST, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@ApiOperation("Service for updating the result of a game started")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = GameAPI_UpdateGameMatchResponse.class)})
    public GameAPI_RESTMessage<GameAPI_UpdateGameMatchResponse> updateMatch(@ApiParam("Information to update a match of specific game.") @RequestBody GameAPI_UpdateGameMatchRequest saveGameMatchDataRequest, HttpServletRequest request, HttpServletResponse response) {
	
		logger.info("registerGame registerGameRequest...{}",saveGameMatchDataRequest);	
		GameAPI_UpdateGameMatchResponse registerGameResponse = null;
		GameAPI_RESTMessage<GameAPI_UpdateGameMatchResponse> GameAPI_RESTMessage = new GameAPI_RESTMessage<GameAPI_UpdateGameMatchResponse>();
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
		try {
			registerGameResponse = gameApiService.updateMatch(controlOperacion, saveGameMatchDataRequest);
			GameAPI_RESTMessage.setData(registerGameResponse);
			GameAPI_RESTMessage.setStatusCode(GameAPI_RESTMessage.REST_OK);
			
		} catch (RespuestaFallidaServicio e) {
			registerGameResponse = null;
			logger.error(e.getMensaje());
			GameAPI_RESTMessage.setStatusCode(GameAPI_RESTMessage.REST_ERROR_INFO_NV);
		}
		
		
		return GameAPI_RESTMessage;
    }

}