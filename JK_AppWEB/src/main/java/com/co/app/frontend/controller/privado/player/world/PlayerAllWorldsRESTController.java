package com.co.app.frontend.controller.privado.player.world;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.World;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@RestController
@RequestMapping("rest/private/player/world/allworlds")
public class PlayerAllWorldsRESTController {
	
	
	private static final int MAX_QUICK_GAME = 4;
	
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio juegoServicio;
	
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	
	private Logger logger = LoggerFactory.getLogger(PlayerAllWorldsRESTController.class);

	
	@RequestMapping(value = "/get",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<World>> getAllWorlds(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
						MensajeREST<List<World>> mensajeREST = new MensajeREST<List<World>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					Player player = playerServicio.getPlayerPpalByUser(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), true);
					
					
					if(player!=null) {
						//Se consulta la informacion para configurar personaje
						List<World> list = playerServicio.obtenerPlayerWordls(controlOperacionServicio, player.getPlayer_player());
						
						mensajeREST.setDetalle(list);
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
						
					}else {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
						
					}
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<World>> mensajeREST = new MensajeREST<List<World>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			
			return mensajeREST; 
		}
		
		 
		
    }
	
	
	
	@RequestMapping(value = "/quickgames",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Game>> getQuickGames(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
						MensajeREST<List<Game>> mensajeREST = new MensajeREST<List<Game>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					Player player = playerServicio.getPlayerPpalByUser(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), true);
					
					
					if(player!=null) {
						//Se consulta la informacion para configurar personaje
						List<Game> list = playerServicio.obtenerGamesByPlayer(controlOperacionServicio, player.getPlayer_player() , null);
						
						List<Game> quickGames = new ArrayList<Game>();
						
						
						int totalQuickGame = 1;
						for (Game game : list) {
							
							if(totalQuickGame<=MAX_QUICK_GAME) {
								quickGames.add(game);
							}
							
							totalQuickGame++;
							
						}
						
						
						mensajeREST.setDetalle(quickGames);
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
						
						
					}else {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
						
					}
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<Game>> mensajeREST = new MensajeREST<List<Game>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			
			return mensajeREST; 
		}
		
		 
		
    }
	
	
	
	
	
	
  
}