package com.co.app.frontend.controller.privado.admin.user.parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA1Service;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@Controller
public class AdminUserParametersController {
	
	private static final String EVENT_EDIT_PARAMETER = "EDIT_PARAMETER";

	private Logger logger = LoggerFactory.getLogger(AdminUserParametersController.class);

	public static final String URL = "/private/admin/user/parameters";

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	AppService appServicio;

	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getParameters(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML + AdminUserParametersController.URL + "/admin_user_parameters";


			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {
					
					
					String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
					modelAndView.addObject("key", uniqueKey);
					
					String event = request.getParameter("event");
					
					
					Boolean isInfoValida = true;
					
					
					if(event!=null) {
						
						if(event.equals(EVENT_EDIT_PARAMETER)){
							
							String cnfg_cnfg = request.getParameter("edit_parameter_id");
							String cnfg_valor = request.getParameter("edit_parameter_value");
							
							if(cnfg_cnfg!=null && cnfg_valor!=null) {

								ParametroConfiguracionGeneral confGeneral = new ParametroConfiguracionGeneral();
								confGeneral.setCnfg_cnfg(cnfg_cnfg);
								confGeneral.setCnfg_valor(cnfg_valor);
								ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
	
								Boolean sinErrores = appServicio.actualizarConfGeneral(controlOperacionServicio, confGeneral);
									
								if(sinErrores) {
									MensajeApp mensajeApp = new MensajeApp();
									mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
									mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
									sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
											
								}else {
									logger.error("ERROR_RCODE_FAILED");
										
									MensajeApp mensajeApp = new MensajeApp();
									mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
									mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_OPER_WITH_ERRORS);
									sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
								}
							}else{
								isInfoValida = false;
							}
						}
					}	
						
					if(!isInfoValida) {
							modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}else {
						// Se consulta los parametros del APP
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
	
						List<ParametroConfiguracionGeneral> parameters = appServicio.getConfGeneral(controlOperacionServicio);
						List<ParametroConfiguracionGeneral> listParameters = new ArrayList<ParametroConfiguracionGeneral>();

						if(parameters!=null) {
							for (ParametroConfiguracionGeneral parameter : parameters) {
								if(parameter.getCnfg_clasif().equals("USR")) {
									listParameters.add(parameter);
								}
								
							}
						}		
						modelAndView.addObject("parameters", listParameters);
							
					}
	
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}
	

}
