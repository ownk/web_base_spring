package com.co.app.frontend.controller.privado.client.players.exterrnalid;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.FileExcelClientPlayersExternalIDS;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.frontend.controller.publico.error.AppInfoNotValidException;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.PlayerExternalIDResponse;
import com.co.app.model.dto.exception.FileStorageException;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;


@Controller
public class ClientPlayersExternalIDController {
	
	private Logger logger = LoggerFactory.getLogger(ClientPlayersExternalIDController.class);
	
	
	public static final String URL = "/private/client/players/externalid";

	
	@Autowired
	private AutenticadorServicio autenticadorServicio;
	
	@Autowired
	private PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private RequestParameterService parametrosRequestServicio;
	
	
	@Autowired
	private FileExcelClientPlayersExternalIDS  archivoExcelPlayersIDSExternos;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private PlayerServicio playerServicio;
	
	
	
	@GetMapping(URL)
	public ModelAndView getPlayers(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/client_players_externalid";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);

					
					Client client= sessionAppUsuario.getCliente();
					if(client!=null) {
						
						List<Player> players = clientService.getPlayersPorCliente(controlOperacion, client.getClient_client());
						
						if(players!=null) {
							modelAndView.addObject("players", players);
						}
						
					    String idFileTemp = generateIDFileTemp();
						
						modelAndView.addObject("idFileTemp", idFileTemp);
						modelAndView.addObject("client", client);
						
						
						 
						
					}else {
						 isInfoValida = false;
					}
					 
					if(!isInfoValida) {
							modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

						
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}
			
			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
	}
	
	
	
    
    @GetMapping(URL+"/downloadTemplate")
    public ResponseEntity<Resource> downloadTemplate (HttpServletRequest request) throws Exception {
        // Load file as Resource
		
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				
				if(isAccessoValido) {
					Client client;
					client = sessionAppUsuario.getCliente();

					if(client!=null) {
						File fileGEnerado;
						try {
							

							
							ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);
							List<Player> players = clientService.getPlayersPorCliente(controlOperacion, client.getClient_client());
							
							
							fileGEnerado = archivoExcelPlayersIDSExternos.getPlantilla(client.getClient_client(), players);
							
							
							if(fileGEnerado!=null) {
								
								
								Resource resource = archivoExcelPlayersIDSExternos.loadFileAsResource(client.getClient_client(), fileGEnerado.getName());

						        if(resource!=null) {
							        
							        // Try to determine file's content type
							        String contentType = null;
							        try {
							            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
							        } catch (IOException ex) {
							            logger.info("Could not determine file type.");
							        }
							
							        // Fallback to the default content type if type could not be determined
							        if(contentType == null) {
							            contentType = "application/octet-stream";
							        }
							
							        return ResponseEntity.ok()
							                .contentType(MediaType.parseMediaType(contentType))
							                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
							                .body(resource);
						        }else {
						        	 return null;
						        }
								
								
								
								
							}
							
						} catch (Exception e) {
							logger.error(e.getMessage());
						}
						
						
					}
					
					
				}
			
				
			
			
			} catch (RespuestaFallidaServicio e1) {
				logger.debug(e1.getMensaje());
			}
			
			
			
			
			
			
		}
		
		logger.error(ConstantesGeneralesAPP.ERROR_INFO_NV);
		throw new AppInfoNotValidException(ConstantesGeneralesAPP.ERROR_INFO_NV);
		
		
		
        
    }
	
	
	
    @PostMapping(URL+"/uploadFileTemp")
	public ModelAndView uploadFileTemp(HttpServletRequest request) {

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/client_players_externalid_uploadfiletemp";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				
				if (isAccessoValido) {
					
					
					
					
					Boolean sinErrores = true;

					String idCLient = null;
					String idFileTemp = null;

					ArrayList<MultipartFile> files = parametrosRequestServicio.getMultipartFiles(request);

					Map<String, Object> parameters = parametrosRequestServicio.getMultipartParams(request);

					try {
						if (parameters != null) {
							idCLient = (String) parameters.get("idClient");
							idFileTemp  = (String) parameters.get("idFileTemp");

							Client client = sessionAppUsuario.getCliente();

							if (idCLient != null && client != null && idCLient.equals(client.getClient_client())) {
								if (files != null && files.size() > 0) {

									for (MultipartFile multipartFile : files) {

										sinErrores = uploadFileTemp(sessionAppUsuario, idCLient, idFileTemp, multipartFile);
										
									}
									
									modelAndView.addObject("client", client);
									

								} else {

									sinErrores = false;

									logger.error("ERR_NO_FILES");
								}

							} else {

								sinErrores = false;

								logger.error("ERR_CLINT_INFO_NV");

							}

						} else {
							sinErrores = false;

							logger.error("ERR_PAGE_INFO_NV");

						}

					} catch (Exception e) {
						sinErrores = false;

						logger.error(e.getMessage());
					}

					generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);

					if (!sinErrores) {
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);

						logger.error("ERR_CLINT_INFO_NV");

					} else {
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);

					}

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

		}

		modelAndView.setViewName(view);
		return modelAndView;

	}
    
    
    @PostMapping(URL+"/processFile")
    public ModelAndView processFile(Locale locale, HttpServletRequest request, HttpServletResponse response) {
    	ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/client_players_externalid_processfile";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					Client client= sessionAppUsuario.getCliente();
					if(client!=null) {
						
						

						String idFileTemp = request.getParameter("idFileTemp");
						
						if(idFileTemp!=null) {
							
							modelAndView.addObject("client", client);
							
							PlayerExternalIDResponse playerExternalIDResponse = processExternalID(sessionAppUsuario, client.getClient_client(), idFileTemp);
							
							if(playerExternalIDResponse!=null) {
								
								
								modelAndView.addObject("players", playerExternalIDResponse.getPlayers());
								modelAndView.addObject("errors", playerExternalIDResponse.getErrores());
								modelAndView.addObject("messages", playerExternalIDResponse.getMensajes());
								
							}else {
								
								modelAndView.addObject("error_nc", "true");
								
								MensajeApp mensajeApp = new MensajeApp();
								mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
								mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_OPER_WITH_ERRORS);
								sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
							}
							
						}
								
						
						 
						
					}else {
						 isInfoValida = false;
					}
					 
					if(!isInfoValida) {
							modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

						
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}
			
			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
       
    }
	
    
    private Boolean uploadFileTemp(SessionAppUsuario sessionAppUsuario, String idClient, String idFileTemp, MultipartFile file) {
		
		
    	String finalFileName = idFileTemp+".xlsx";
    	
    	String rutaBase = archivoExcelPlayersIDSExternos.getBasePathStorageClient(idClient);
    	
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        // Check if the file's name contains invalid characters
		if(fileName.contains("..")) {
		    throw new FileStorageException("ERR_IMG_INV_PATH" + fileName);
		}
		
		
		if(!fileName.endsWith(".xlsx")) {
			throw new FileStorageException("ERR_IMG_INV_XLSX" + fileName);
			
		}

		// Copy file to the target location (Replacing existing file with the same name)
		Path fileStorageLocation = Paths.get(rutaBase).toAbsolutePath().normalize();

		try {
		    Files.createDirectories(fileStorageLocation);
		    
		    Path targetLocation = fileStorageLocation.resolve(finalFileName);
		    Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		    
		    
		    
		    File excelToRead = targetLocation.toFile();

		    if(excelToRead!=null && excelToRead.exists()) {
		    	return true;
		    	
		    }
		    

		    
		} catch (Exception ex) {
			
		    throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
		}
		
		
		
		
		return false;

	}
	
	private PlayerExternalIDResponse processExternalID(SessionAppUsuario sessionAppUsuario, String idClient, String idFileTemp) {
		
		
    	
    	String finalFileName = idFileTemp+".xlsx";
    	
    	String rutaBase = archivoExcelPlayersIDSExternos.getBasePathStorageClient(idClient);
    	
        Path fileStorageLocation = Paths.get(rutaBase).toAbsolutePath().normalize();

		try {
		    Path targetLocation = fileStorageLocation.resolve(finalFileName);
		   
		    File excelToRead = targetLocation.toFile();

		    if(excelToRead!=null && excelToRead.exists()) {
		    	List<Player> players = archivoExcelPlayersIDSExternos.readPlayers(idClient, excelToRead);
		    	
		    	
		    	if(players!=null && players.size()>0) {
		    		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);
			    	
			    	return playerServicio.actualizarInfoPlayer(controlOperacion, players, idClient);
		    	}
		    	
		    	
		    	
		    	
		    }
		    

		    
		} catch (Exception ex) {
			
		  
		}
		
		
		
		
		return null;

	}
	
	
	private String generateIDFileTemp() {
		String random = RandomStringUtils.random(10, true, true);
		Date sysdate =null;
		try {
			sysdate = appService.getSysdate();
		} catch (RespuestaFallidaServicio e1) {
			sysdate = new Date();
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
    	String fecha = format.format(sysdate);
		
		format = new SimpleDateFormat("HHmmssSSS");
		String hora = format.format(sysdate);
		
		String idFileTemp = fecha+"_"+hora+"_"+random;
		return idFileTemp;
	}
	
	

 
	
	
	

}
