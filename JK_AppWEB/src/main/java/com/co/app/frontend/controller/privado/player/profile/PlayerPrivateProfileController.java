package com.co.app.frontend.controller.privado.player.profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.client.FileExcelClientPlayersObjectivesReport;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.ValidationRegulareEpressions;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.PlayerExternalIDResponse;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.ErrorBD;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@Controller
public class PlayerPrivateProfileController {

	private static final String EVENT_UPDATE_IDEXT = "UPDATE_IDEXT";

	private Logger logger = LoggerFactory.getLogger(PlayerPrivateProfileController.class);

	public static final String URL = "/private/player/profile/private";

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	PlayerServicio jugadorServicio;

	@Autowired
	FileExcelClientPlayersObjectivesReport generadorReportClientPlayersObjectives;

	@Autowired
	ClientService clientService;
	
	@Autowired
	ValidationRegulareEpressions validationRegulareEpressions;

	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView privateProfile(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {

			view = ConstantesGeneralesAPP.PAGES_HTML + URL + "/player_privateprofile_create";

			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {

					String event = request.getParameter("event");
					Boolean isInfoValida = true;

					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					String idUsuario = sessionAppUsuario.getUsuario().getUser_user();

					Player player = jugadorServicio.getPlayerPpalByUser(controlOperacionServicio, idUsuario, false);
					if (player != null) {

						if (event != null) {

							if (event.equals(EVENT_UPDATE_IDEXT)) {

								
								Client client = sessionAppUsuario.getCliente();
								
								
								if(client!=null) {
									String idExt = request.getParameter("idext");
									
									Boolean sinErrores = false;	
									
									String idExtNormalizado = idExt.trim();
									
									if(StringUtils.isEmpty(idExtNormalizado)) {
										idExtNormalizado= null;
									}
									
									if(idExtNormalizado!=null && !validationRegulareEpressions.isIdentification(idExtNormalizado)) {
										
										sinErrores=false;
									}else {
										
										
										
										player.setPlayer_id_ext(idExtNormalizado);
										
										List<Player> listPlayer = new ArrayList<Player>();
										listPlayer.add(player);

										ControlOperacionServicio controlOperacion = new ControlOperacionServicio(sessionAppUsuario);
										
										PlayerExternalIDResponse playerExternailIDResponse = jugadorServicio.actualizarInfoPlayer(controlOperacion, listPlayer, client.getClient_client());
										player = jugadorServicio.getPlayerPpalByUser(controlOperacionServicio, idUsuario, false);
											
												
										if(playerExternailIDResponse!=null) {
											
											List<ErrorBD> errors = playerExternailIDResponse.getErrores();
											
											if(errors!=null && errors.size()>0) {
												sinErrores=false;
												
											}else {
												sinErrores = true;
											}
											
											
										}
									}
									
									
									
											
											
									if(sinErrores) {
										MensajeApp mensajeApp = new MensajeApp();
										mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
										mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
										sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
									}else {
										MensajeApp mensajeApp = new MensajeApp();
										mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
										mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_OPER_WITH_ERRORS);
										sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeApp);
									}		
									
									
									
								}else {
									isInfoValida = false;
								}
								
								

							} else {
								isInfoValida = false;
							}

						}

						modelAndView.addObject("player", player);

						if (player.getPlayer_type() != null && !StringUtils.isEmpty(player.getPlayer_type())) {

							Avatar avatar = jugadorServicio.obtenerAvatarPlayer(controlOperacionServicio,
									player.getPlayer_player());
							Pet pet = jugadorServicio.obtenerPetPlayer(controlOperacionServicio,
									player.getPlayer_player());
							Ship ship = jugadorServicio.obtenerShipPlayer(controlOperacionServicio,
									player.getPlayer_player());
							List<Match> matches = jugadorServicio.obtenerMatchesPlayer(controlOperacionServicio,
									player.getPlayer_player(), null, 100, 0);

							view = ConstantesGeneralesAPP.PAGES_HTML + URL + "/player_privateprofile_details";

							modelAndView.addObject("user", sessionAppUsuario.getUsuario());
							modelAndView.addObject("avatar", avatar);
							modelAndView.addObject("pet", pet);
							modelAndView.addObject("ship", ship);
							modelAndView.addObject("matches", matches);

						}
					} else {
						isInfoValida = false;
					}

					if (!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
								ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
								ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}

}
