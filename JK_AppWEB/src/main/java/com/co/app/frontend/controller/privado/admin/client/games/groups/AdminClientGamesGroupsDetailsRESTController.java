package com.co.app.frontend.controller.privado.admin.client.games.groups;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.groovy.classgen.asm.util.LoggableTextifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("rest/private/admin/client/games/groups")
public class AdminClientGamesGroupsDetailsRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio juegoServicio;
	
	@Autowired
	ClientService clientService;
	
	
	
	
	private Logger logger = LoggerFactory.getLogger(AdminClientGamesGroupsDetailsRESTController.class);

	
    @RequestMapping(value = "/create", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public MensajeREST<String> createGroup(@RequestBody TemplateGames templateGames, HttpServletRequest request, HttpServletResponse response) {
    	
    	
    	SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			MensajeREST<String> mensajeREST = new MensajeREST<String>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
			Boolean isInfoValida = true;
			
			if(templateGames!=null) {
					
				
				
				try {
					Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
					
					if(isAccessoValido) {
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
						
						
						
						
						if(templateGames.getTemplt_client()!=null && templateGames.getTemplt_descri()!=null && templateGames.getGames()!=null && templateGames.getGames().size()>0) {
							
							
							try {

								Boolean sinErrores = true;
								
								
								sinErrores = sinErrores && clientService.crearPlantilla(controlOperacionServicio, templateGames.getGames(), templateGames.getTemplt_descri(), templateGames.getTemplt_client());
								
								
								if(sinErrores) {
									
									mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
									
									MensajeApp mensajeApp = new MensajeApp();
									mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
									mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
									sessionAppUsuario.agregarMensajeAPP(AdminClientGamesGroupsDetailsController.URL, mensajeApp);
									
								}else {
									isInfoValida = false;
								
								}
								
								
								
							} catch (RespuestaFallidaServicio e) {
								isInfoValida = false;
								
								logger.error(e.getMensaje());
								
							}
							
							
						}else {
							isInfoValida = false;
						}
						
						
						if(!isInfoValida) {
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
							
							
							MensajeApp mensajeApp = new MensajeApp();
							mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
							mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
							sessionAppUsuario.agregarMensajeAPP(AdminClientGamesGroupsDetailsController.URL, mensajeApp);
							
						}
						
						
						
					}else {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
					}
					
				} catch (RespuestaFallidaServicio e) {
					logger.error(e.getMensaje());
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
					
				}
				
				
				
				
			}else {
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
			}
			
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<String> mensajeREST = new MensajeREST<String>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			return mensajeREST; 
		}
		
    }
    
    
    @RequestMapping(value = "/games",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Game>> getGames(HttpServletRequest request, HttpServletResponse response) {
	
    	MensajeREST<List<Game>> mensajeREST = new MensajeREST<List<Game>>();
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						List<Game> games = juegoServicio.obtenerJuegos(controlOperacionServicio);
						
						
						if(games!=null) {
							mensajeREST.setDetalle(games);							
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
							
						}else {
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
							
						}
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
    
    
    @RequestMapping(value = "/group",
            method = RequestMethod.POST, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<TemplateGames> getPlantilla(@RequestBody ObjectNode infoTemplate, HttpServletRequest request, HttpServletResponse response) {
	
    	MensajeREST<TemplateGames> mensajeREST = new MensajeREST<TemplateGames>();
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					if(infoTemplate !=null) {
						
						String idCliente = infoTemplate.get("templt_client").asText();
						String idPlantilla = infoTemplate.get("templt_templt").asText();
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						TemplateGames template = clientService.getPlantillaListaJuegos(controlOperacionServicio, idCliente, idPlantilla);
						
						
						if(template!=null) {
							mensajeREST.setDetalle(template);							
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
							
						}else {
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
							
						}
						
					}else {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
					}
					
						
						
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
    
  
}