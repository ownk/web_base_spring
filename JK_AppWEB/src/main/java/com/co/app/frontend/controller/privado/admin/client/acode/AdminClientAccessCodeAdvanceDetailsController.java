package com.co.app.frontend.controller.privado.admin.client.acode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.FileExcelClientPlayersExternalIDS;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.client.PlayerExternalIDResponse;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.exception.FileStorageException;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;


@Controller
public class AdminClientAccessCodeAdvanceDetailsController {
	
	private Logger logger = LoggerFactory.getLogger(AdminClientAccessCodeAdvanceDetailsController.class);
	
	
	public static final String URL = "/private/admin/client/acodes/advance/details";

	
	@Autowired
	private AutenticadorServicio autenticadorServicio;
	
	@Autowired
	private PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private RequestParameterService parametrosRequestServicio;
	
	
	@Autowired
	private FileExcelClientPlayersExternalIDS  archivoExcelPlayersIDSExternos;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private PlayerServicio playerServicio;
	
	
	
	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getProcess(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/admin_client_acodes_advance_details";
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					Client client = (Client)parametrosRequestServicio.getRequestParameterToObject(request, "Client", Client.class, null);
					String idProcess = request.getParameter("idProcess"); 
							
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					
					if(client!=null) {
						
						client = clientService.getClienteByID(controlOperacionServicio, client.getClient_client());
						
						if(client!=null && idProcess!=null) {
							
							
							List<LoadMasiveAccessCodes> processes = clientService.getProcessLoadMasiveAccessCode(controlOperacionServicio, client.getClient_client(), idProcess);
							
							
							if(processes!=null && processes.size()>0) {
								
								modelAndView.addObject("client", client);
								
								LoadMasiveAccessCodes process = processes.get(0);
								modelAndView.addObject("process", process);
								
								
								List<DetailLoadMasiveAccessCode> detailsProcess = clientService.getDetailsLoadMasiveAccessCode(controlOperacionServicio, process.getLmac_lmac());
								
								if(detailsProcess!=null && detailsProcess.size()>0) {
									
									modelAndView.addObject("details", detailsProcess);
									generarDetailsSummary(modelAndView, process, detailsProcess);
								}
								
							
							
							
							}else {
								isInfoValida = false;
							}
							
						}else {
							isInfoValida = false;
						}
					}else {
						isInfoValida= false;
					}
					
					
					 
					if(!isInfoValida) {
							modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

						
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}
			
			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
	}
	
	
	private void generarDetailsSummary(ModelAndView modelAndView,LoadMasiveAccessCodes loadMasiveAccessCodes,  List<DetailLoadMasiveAccessCode> list) {
		
		
		int errors = 0;
		int ok =0;
		int pend=0;	
		int processed=0;
		int no_processed =0;
		int all = loadMasiveAccessCodes.getLmac_file_trg();
		
		for (DetailLoadMasiveAccessCode detailLoadMasiveAccessCode : list) {
			if(DetailLoadMasiveAccessCode.STATE_OK.equals(detailLoadMasiveAccessCode.getDlmac_state())) {
				ok++;
			}
			
			if(DetailLoadMasiveAccessCode.STATE_ERROR.equals(detailLoadMasiveAccessCode.getDlmac_state())) {
				errors++;
			}
			
			if(DetailLoadMasiveAccessCode.STATE_PENDING.equals(detailLoadMasiveAccessCode.getDlmac_state())) {
				pend++;
			}
			
			processed++;
		}
		
		no_processed = all-processed;
		
		modelAndView.addObject("details_summary_total_error", errors);
		modelAndView.addObject("details_summary_total_ok", ok);
		modelAndView.addObject("details_summary_total_pend", pend);
		modelAndView.addObject("details_summary_total_processed", processed);
		modelAndView.addObject("details_summary_total_no_processed", no_processed);
		modelAndView.addObject("details_summary_total_all", all);
		 
		
	}
	

 
	
	
	

}
