package com.co.app.frontend.controller.privado.player.profile;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.AnswerQuestion;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.PlayerCharacter;
import com.co.app.model.dto.game.Question;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@RestController
@RequestMapping("rest/private/player/profile/private")
public class PlayerPrivateProfileRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio juegoServicio;
	
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	
	private Logger logger = LoggerFactory.getLogger(PlayerPrivateProfileRESTController.class);

	
    @RequestMapping(value = "/create", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public MensajeREST<String> setPlayerCharacter(@RequestBody PlayerCharacter playerCharacter, HttpServletRequest request, HttpServletResponse response) {
    	
    	
    	SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			MensajeREST<String> mensajeREST = new MensajeREST<String>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
			Boolean isInfoValida = true;
			
			if(playerCharacter!=null) {
					
				
				
				try {
					Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
					
					if(isAccessoValido) {
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
						
						Player player = playerServicio.getPlayerPpalByUser(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), false);
						
						if(player!=null) {
							
							
							try {

								Boolean sinErrores = true;
								
								sinErrores = sinErrores && playerServicio.asignarAvatar(controlOperacionServicio, player.getPlayer_player(), playerCharacter.getAvatar_avatar());
								sinErrores = sinErrores && playerServicio.asignarPet(controlOperacionServicio, player.getPlayer_player(), playerCharacter.getPet_pet());
								sinErrores = sinErrores && playerServicio.asignarShip(controlOperacionServicio, player.getPlayer_player(), playerCharacter.getShip_ship());
								
								if(sinErrores) {
									
									//Se actualizan las respuestas del jugador
									if(playerCharacter.getQuestions()!=null ) {
										if(playerCharacter.getQuestions().size()>0) {
											
											List<AnswerQuestion> respuestaPreguntas = new ArrayList<AnswerQuestion>();
											List<Question> questionsSelected = playerCharacter.getQuestions();
											for (Question questionSelected : questionsSelected) {
												AnswerQuestion answerQuestion = new AnswerQuestion();
												answerQuestion.setAnwqtn_answer(questionSelected.getAnswer_answer());
												answerQuestion.setAnwqtn_questn(questionSelected.getQuestn_questn());
												respuestaPreguntas.add(answerQuestion);
											}
											
											
											
											sinErrores=sinErrores && playerServicio.guardarPeguntas(controlOperacionServicio, player.getPlayer_player(), respuestaPreguntas);
											
											
										}
										
									}
									
									
									if(sinErrores) {
										//Cambiar el tipo de jugador
										playerServicio.actualizarTipoJugador(controlOperacionServicio, player.getPlayer_player(), Player.PLAYER_DEFAULT_TYPE);
										//Guardar preguntas
										mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
										mensajeREST.setDetalle(PlayerPrivateProfileController.URL);
									}else {
										isInfoValida = false;
									}
									
								}else {
									isInfoValida = false;
								
								}
								
								
								
							} catch (RespuestaFallidaServicio e) {
								isInfoValida = false;
								
								logger.error(e.getMensaje());
								
							}
							
							
						}else {
							isInfoValida = false;
						}
						
						
						if(!isInfoValida) {
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
						}
						
						
						
					}else {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
					}
					
				} catch (RespuestaFallidaServicio e) {
					logger.error(e.getMensaje());
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
					
				}
				
				
				
				
			}else {
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
			}
			
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<String> mensajeREST = new MensajeREST<String>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			
			return mensajeREST; 
		}
		
    }
    
    
    @RequestMapping(value = "/details",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<Player> getPlayer(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
						MensajeREST<Player> mensajeREST = new MensajeREST<Player>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
			
			Boolean isInfoValida = true;
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						Player player = playerServicio.getPlayerPpalByUser(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), true);
						
						
						if(player!=null) {
							mensajeREST.setDetalle(player);							
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
							
						}else {
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
							
						}
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<Player> mensajeREST = new MensajeREST<Player>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
  
}