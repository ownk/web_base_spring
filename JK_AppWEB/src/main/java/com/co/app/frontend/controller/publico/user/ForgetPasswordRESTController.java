package com.co.app.frontend.controller.publico.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.user.PasswordValidationResponse;
import com.co.app.model.dto.user.User;

@RestController
@RequestMapping("rest/public/user/forgetpassword")
public class ForgetPasswordRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	UserService usuarioServicio;
	
	@Autowired
	MailServicio mailServicio;
	
	
	private Logger logger = LoggerFactory.getLogger(ForgetPasswordRESTController.class);

	
	
	@RequestMapping(value = "/email/{emaildata}", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<Boolean> isEmailValido(@PathVariable("emaildata") String emaildata, HttpServletRequest request, HttpServletResponse response) {
		
		
		Boolean isValido = mailServicio.isStructureEmailOK(emaildata);
		
		//Se debe validar si correo existe
		if(isValido!=null) {
			
			String emailNormalizado = emaildata.toLowerCase();
			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
			try {
				Boolean emailValido = usuarioServicio.validarEmail(controlOperacion, emailNormalizado );
				
				if(emailValido) {					
					isValido = false;
				}else {
					isValido = true;
				}				
				
			} catch (RespuestaFallidaServicio e) {
				
				isValido = false;
			}
			
		}
		
		
		MensajeREST<Boolean> mensajeREST = new MensajeREST<Boolean>();
		mensajeREST.setDetalle(isValido);
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
		
		
		
		
		return mensajeREST;
    }
	
	@RequestMapping(value = "/pass/validation",
			method = RequestMethod.POST, //
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public MensajeREST<Boolean> isPasswordValido(@RequestBody User user, HttpServletRequest request, HttpServletResponse response) {

		//logger.info("Consultando password..."+passworddata);	

		Boolean isValido = true;
		
		if(user!=null) {
			

			/*
			 * =======================================================
			 * DECRYPT PARAMETERS
			 * =======================================================
			 * Desencripta parametros nick 
			 * -------------------------------------------------------
			 */

			String userNameDecrypt = RequestParameterService.decryptText(user.getUser_name());
			String userNickDecrypt = RequestParameterService.decryptText(user.getUser_nick());
			String userPassDecrypt = RequestParameterService.decryptText(user.getUser_pass());

			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));

			PasswordValidationResponse passwordValidationResponse = null;




			try {
				passwordValidationResponse = usuarioServicio.IsPasswordValid(controlOperacion,userNameDecrypt,  userNickDecrypt,userPassDecrypt);

				if(passwordValidationResponse.isValid()) {
					isValido = true;

				}else {

					isValido = false;
				}


			} catch (RespuestaFallidaServicio e) {

				isValido = false;
			}


		}



		MensajeREST<Boolean> mensajeREST = new MensajeREST<Boolean>();
		mensajeREST.setDetalle(isValido);
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);


		return mensajeREST;
	}

}