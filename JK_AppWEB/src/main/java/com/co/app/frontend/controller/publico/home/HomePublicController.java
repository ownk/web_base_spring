package com.co.app.frontend.controller.publico.home;


import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.App;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

import javax.sql.DataSource;

@Controller
public class HomePublicController {
	
	public static final String URL = "/";
	
	
	Logger logger = LoggerFactory.getLogger(HomePublicController.class);
	
	
	@Autowired
	AppService appServicio; 
	
	@Autowired
	UserService usuarioServicio; 

	
	@Autowired
	MailServicio mailServicio; 
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina; 
	
	
	@Autowired
	DataSource datasource;
	
	
	@RequestMapping(URL)
    public ModelAndView inicio(Locale locale, HttpServletRequest request, HttpServletResponse response) {
    	
    	
    	
    	/*
    	 * Variables por controllador
    	 * ==================================
    	 * Se crean las variables especificas
    	 * por controlador 
    	 * ----------------------------------
    	 */
		
    	App app = appServicio.getApp();
    	logger.info("App "+app.getApp_nomb()+" Version:"+app.getApp_vers());
    	
    	
    	

    	/*
    	 * Vista HTML por controllador
    	 * ==================================
    	 * Se especifica la vista html que
    	 * se utilizara para la visualizacion
    	 * de informacion
    	 * ----------------------------------
    	 */
    	
    	
    	ModelAndView modelAndView = new ModelAndView();
        String redirect = "redirect:"+ConstantesGeneralesAPP.URL_LOGIN_USERNAME;
		modelAndView.setViewName(redirect);
		return modelAndView;
    	
    	
    	
    }
	
	
    	
    
   

}