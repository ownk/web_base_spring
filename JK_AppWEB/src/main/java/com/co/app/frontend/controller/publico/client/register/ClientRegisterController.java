package com.co.app.frontend.controller.publico.client.register;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA1Service;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.captcha.RecaptchaService;
import com.co.app.backend.service.business.captcha.RecaptchaService.RecaptchaResult;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.commons.ValidationRegulareEpressions;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.user.EncryptEncodeUserRegisterToken;
import com.co.app.backend.service.business.user.UserRegisterTokenGenerator;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.commons.ParametroConfiguracionGeneral;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.user.TokenUserRegister;
import com.co.app.model.dto.user.User;
  
@Controller
public class ClientRegisterController {
	
	public static final String URL_BASE = "/public/register/client";
	public static final String URL_REQUEST =URL_BASE+"/request";
	public static final String URL_CREATE =URL_BASE+"/create";
	public static final String URL_RESPONSE =URL_BASE+"/response";
	public static final String URL_CONFIRM =URL_BASE+"/confirm";
	
	private static final String PARAMETRO_CONFIRMACION_REGISTRO = "confID";
	private static final String PARAMETRO_ENCRYPT_SALT = "e";
	private static final String PARAMETRO_ENCRYPT_IV = "s";
	
	
	Logger logger = LoggerFactory.getLogger(ClientRegisterController.class);
	
	@Autowired
	UserService usuarioServicio; 

	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	RequestParameterService parametrosRequestServicio;
	
	@Autowired
	UserRegisterTokenGenerator generadorTokenRegistroUsuario;
	
	@Autowired
	MailServicio mailServicio;
	
	@Autowired
	AppService appServicio;
	
	
	@Autowired
	ClientService clientService;
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	
	@Autowired
	RecaptchaService recaptchaServicio;
	
	@Autowired
	I18nService i18nservice;
	
	@Autowired
	ValidationRegulareEpressions validationRegulareEpressions;
	

	@ModelAttribute("recaptchaSiteKey")
	public String getRecaptchaSiteKey() {
	    return recaptchaServicio.getRecaptchaSiteKey();
	}
	
	@GetMapping(URL_REQUEST)
	public ModelAndView infoRegistroCliente(Locale locale, HttpServletRequest request, HttpServletResponse response) throws RespuestaFallidaServicio {
		
		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		ModelAndView modelAndView = new ModelAndView();
		
		String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
		modelAndView.addObject("key", uniqueKey);
		
		String textNickName = validationRegulareEpressions.getNickName();

		modelAndView.addObject("nickName_regex", textNickName);

			
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
		
		
		try {
			List<TipoDocumento> tipos = clientService.getTiposDocumento(controlOperacion);
			modelAndView.addObject("tiposID", tipos);
			
		} catch (RespuestaFallidaServicio e) {
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_GENERAL_NC);	
			listaMensajesApp.add(mensajeApp);
			
		}
		
		generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, listaMensajesApp);
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/client_register_create";
		modelAndView.setViewName(view);
		
		return modelAndView;
	}
	
	
	@PostMapping(URL_CREATE)
	public ModelAndView registrarCliente(Locale locale, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirect) {
		
		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		User usuarioCreado = null;
		
		
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"client_register_create";
		
		//Se consulta parametros 
		User usuarioAdminCliente = (User)parametrosRequestServicio.getRequestParameterToObject(request, "Usuario", User.class, null);
		Client clienteNuevo = (Client)parametrosRequestServicio.getRequestParameterToObject(request, "Client", Client.class, null);
		
		String confirmacionPass = request.getParameter("confirmacionPass");
		String terminosCondiciones = request.getParameter("terminosCondiciones");
		String codigoAcceso = request.getParameter("codigoAcceso");
		
		
		Boolean sinErrores = false;
		
		
		
		
		//Validacion terminos
		Boolean isTerminosValido = false;
		
		if(terminosCondiciones!=null && "on".equals(terminosCondiciones)) {
			isTerminosValido = true;
		}else {
			
			logger.error("ERROR_TERM_COND_NV");
			
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);	
			listaMensajesApp.add(mensajeApp);
			
		}
		
	
		//Validacion de PASS
		Boolean isPassValido = false;
		
		
		if(confirmacionPass!=null && usuarioAdminCliente!=null) {
			if(confirmacionPass.equals(usuarioAdminCliente.getUser_pass())) {
				isPassValido = true;
			}else {
				
				logger.error("ERROR_PASS_NV");
				
				MensajeApp mensajeApp = new MensajeApp();
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);	
				listaMensajesApp.add(mensajeApp);
			}
			
		}
		
		
		
		//Validacion de CAPTCHA
		Boolean isCaptchaValido = false;
		
		String recaptchaResponse = request.getParameter("g-recaptcha-response");
		if ( recaptchaResponse != null) {

			// I didn't use remoteip here, because it is optional,
			// Reference: https://developers.google.com/recaptcha/docs/verify
			RecaptchaResult result = recaptchaServicio.getResult("", recaptchaResponse);

			if (!result.isSuccess()) {
				
				logger.error("ERROR_CAPTCHA_NV");
				
				MensajeApp mensajeApp = new MensajeApp();

				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);	
				listaMensajesApp.add(mensajeApp);
				
				
			}else {
				isCaptchaValido = true;
			}
		}
		
		
		
		
		ModelAndView modelAndView = new ModelAndView();
		if(codigoAcceso!=null && usuarioAdminCliente!=null  && clienteNuevo!=null && isCaptchaValido && isTerminosValido && isPassValido) {
			
			//Generar otp
			String otp = generadorTokenRegistroUsuario.generarOTP();
			
	
			Email emailDto = null;
			
			if(otp!=null) {
				
				//Crear jugador
				ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP,ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request) );
				
				
				try {
					Boolean validaCamposEntrada = true;

		
					
					/*
					 * =======================================================
					 * DECRYPT PARAMETERS
					 * =======================================================
					 * Desencripta parametros de entrada registro cliente
					 * -------------------------------------------------------
					 */
					String clieNameDecrypt = RequestParameterService.decryptText(clienteNuevo.getClient_name());
					String clieTipoIdentDecrypt = RequestParameterService.decryptText(clienteNuevo.getClient_tpident());
					String clieNumIdentDecrypt = RequestParameterService.decryptText(clienteNuevo.getClient_numident());

					String userEmailDecrypt = RequestParameterService.decryptText(usuarioAdminCliente.getUser_email());
					String userNickDecrypt = RequestParameterService.decryptText(usuarioAdminCliente.getUser_nick());
					String userPassDecrypt = RequestParameterService.decryptText(usuarioAdminCliente.getUser_pass());
					String userNameDecrypt = RequestParameterService.decryptText(usuarioAdminCliente.getUser_name());
					String codigoAccesoDecrypt = RequestParameterService.decryptText(codigoAcceso);

					
					/*
					 * =======================================================
					 * VALIDACIONES GENERALES
					 * =======================================================
					 * Validacion de campos
					 * -------------------------------------------------------
					 */
					
					if (!validationRegulareEpressions.isEmail(userEmailDecrypt)) {

						validaCamposEntrada=false;

					}else if(!validationRegulareEpressions.isTextNumber(userNameDecrypt) && validaCamposEntrada) {

						validaCamposEntrada=false;

					}else if(!validationRegulareEpressions.isNickName(userNickDecrypt) && validaCamposEntrada) {

						validaCamposEntrada=false;

					}else if(!validationRegulareEpressions.isTextNumber(clieNameDecrypt) && validaCamposEntrada) {
						
						validaCamposEntrada=false;
						
					}else if(!validationRegulareEpressions.isIdentification(clieNumIdentDecrypt) && validaCamposEntrada) {
						
						validaCamposEntrada=false;
					}
					
					
					if(!isTpidentValido(clieTipoIdentDecrypt)) {
						validaCamposEntrada=false;
					}
					
					
					/*
					 * =======================================================
					 * REGISTRO DE INFO
					 * =======================================================
					 * Si las validaciones exitosas 
					 * -------------------------------------------------------
					 */
					if (validaCamposEntrada) {
						
						
						clienteNuevo.setClient_name(clieNameDecrypt);
						clienteNuevo.setClient_tpident(clieTipoIdentDecrypt);
						clienteNuevo.setClient_numident(clieNumIdentDecrypt);
						
						usuarioAdminCliente.setUser_email(userEmailDecrypt);
						usuarioAdminCliente.setUser_nick(userNickDecrypt);
						usuarioAdminCliente.setUser_pass(userPassDecrypt);
						usuarioAdminCliente.setUser_name(userNameDecrypt);
						
						sinErrores = clientService.crearCliente(controlOperacion, clienteNuevo, usuarioAdminCliente, codigoAccesoDecrypt, otp );
					}
					
					
					//Envio de link de confirmacion
					if(sinErrores) {
						
						usuarioCreado = usuarioServicio.getUsuarioPorNick(controlOperacion, usuarioAdminCliente.getUser_nick());
						
						
						//Generar token de registro
						TokenUserRegister tokenRegistroUsuario = generadorTokenRegistroUsuario.generarToken(usuarioCreado, codigoAcceso, otp);

						EncryptEncodeUserRegisterToken tokenConfirmacionEnconde = generadorTokenRegistroUsuario.getEncrypEncodeToken(tokenRegistroUsuario);

						String urlBase = appServicio.getPublicoURLBase();
						
						String urlConfirmRegister = urlBase+appServicio.getContextoAPP()+URL_CONFIRM+"?"+
						PARAMETRO_CONFIRMACION_REGISTRO+"="+tokenConfirmacionEnconde.getencryptEncodeToken()+
						"&"+PARAMETRO_ENCRYPT_SALT+"="+tokenConfirmacionEnconde.getencryptEncodeSalt()+
						"&"+PARAMETRO_ENCRYPT_IV+"="+tokenConfirmacionEnconde.getencryptEncodeIV();
								
						
						String emailSubject = i18nservice.translate("client_register.email.subject");
						String emailMessage = i18nservice.translate("client_register.email.message");
						
						
						emailDto = new Email(mailServicio.getMailUsername(), usuarioAdminCliente.getUser_email(),emailSubject,emailMessage);
						
						Map<String, Object> parameterMap = new HashMap<String, Object>();
						
						
						ParametroConfiguracionGeneral parameterAppLogoUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_LOGO_INTERNET_URL);
						
						String urlAppLogo = "";
						if(parameterAppLogoUrl!=null) {
							urlAppLogo = parameterAppLogoUrl.getCnfg_valor();
						}

						ParametroConfiguracionGeneral parameterAppPageUrl= appServicio.getParameter(ConstantesGeneralesAPP.CNFG_APP_PAGE_INTERNET_URL);
						
						String urlAppPage = "";
						if(parameterAppPageUrl!=null) {
							urlAppPage = parameterAppPageUrl.getCnfg_valor();
						}
						
						
						String emailHeaderTitle = i18nservice.translate("client_register.email.header.title");
						String emailHeaderSubtitle = i18nservice.translate("client_register.email.header.subtitle");
						String emailContentGreeting = i18nservice.translate("client_register.email.content.greeting");
						String emailContentMessage = i18nservice.translate("client_register.email.content.message");
						String emailContentFooter = i18nservice.translate("client_register.email.content.footer");
						String emailContentButtonConfirm = i18nservice.translate("client_register.email.content.button_confirm");
						
						parameterMap.put("emailHeaderTitle", emailHeaderTitle);
						parameterMap.put("emailHeaderSubtitle", emailHeaderSubtitle);
						parameterMap.put("emailContentGreeting", emailContentGreeting);
						parameterMap.put("emailContentMessage", emailContentMessage);
						parameterMap.put("emailContentFooter", emailContentFooter);
						parameterMap.put("emailContentButtonConfirm", emailContentButtonConfirm);
						
						parameterMap.put("urlAppPage", urlAppPage);
						parameterMap.put("urlAppLogo", urlAppLogo);
						
						
						parameterMap.put("urlConfirmRegister", urlConfirmRegister );
						parameterMap.put("user", usuarioAdminCliente);
						
						
						emailDto.setParameterMap(parameterMap);
						
						
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
						listaMensajesApp.add(mensajeApp);
						
					}
					
					
				} catch (Exception e) {
					
					logger.error("ERROR_NC"+e.getMessage());
					sinErrores = false;
				}
				
				//Envio de correo de confirmacion
				if(sinErrores && emailDto!=null) {
					
					try {
						mailServicio.sendHtmlTemplateEmail(locale, emailDto, MailServicio.HTML_TEMPLATE_EMAIL_REGISTER_CLIENT);
						
						
					} catch (Exception e) {
						
						
						sinErrores = false;
						logger.error("ERROR_SEND_MAIL"+e.getMessage());
						
						
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ADVERTENCIA);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_WARN_EMAIL_NO_SEND);
						listaMensajesApp.add(mensajeApp);
					}
					
					
				}else {
					
					sinErrores = false;
					
					logger.error("ERROR_NC");
					
					MensajeApp mensajeApp = new MensajeApp();
					mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
					mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
					listaMensajesApp.add(mensajeApp);
				}
				
				
				
			}else {
				
				logger.error("ERROR_OTP_NL");
				
				MensajeApp mensajeApp = new MensajeApp();
				
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);
			
			}
			
		}else {
			
			logger.error("ERROR_INFO_NV");
			
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			listaMensajesApp.add(mensajeApp);
			
		}
		
		
		
		redirect.addFlashAttribute("mensajesApp", listaMensajesApp);
		redirect.addFlashAttribute("usuarioCreado", usuarioCreado);
		redirect.addFlashAttribute("accesoValido", true);
		redirect.addFlashAttribute("sinErrores", sinErrores);
		
		view = "redirect:"+URL_RESPONSE;
		
		modelAndView.setViewName(view);
		
		return modelAndView;
	}

	
	@GetMapping(URL_RESPONSE)
	public ModelAndView respuestaRegistro(Model model, HttpServletRequest request ) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		
		String view = "redirect:"+URL_REQUEST;

		ArrayList<MensajeApp> mensajesAPP = (ArrayList<MensajeApp>) model.asMap().get("mensajesApp");
		User usuarioCreado = (User) model.asMap().get("usuarioCreado");
		Boolean accesoValido = (Boolean ) model.asMap().get("accesoValido");
		Boolean sinErrores = (Boolean ) model.asMap().get("sinErrores");
		
		if(accesoValido!=null && accesoValido) {
		
		
			if(sinErrores!=null && sinErrores) {
				if(usuarioCreado!=null) {
					
					view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/client_register_create_ok";
					
				}else {
		
					view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/client_register_create_error";
				}
			}else {
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/client_register_create_error";
			}
	
			
			
			
			generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, mensajesAPP);
			
			
		}else {
			view = "redirect:"+URL_REQUEST;
		}
		
		modelAndView.setViewName(view);
		
		return modelAndView;
		
	}
	
	
	@GetMapping(URL_CONFIRM)
	public ModelAndView confirmarRegistroCliente(Locale locale, HttpServletRequest request, HttpServletResponse response) {
			
		
		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		String confimationID = request.getParameter(PARAMETRO_CONFIRMACION_REGISTRO);
		String encryptSalt = request.getParameter(PARAMETRO_ENCRYPT_SALT);
		String encryptIV = request.getParameter(PARAMETRO_ENCRYPT_IV);
		Boolean sinErrores = false;

		ModelAndView modelAndView = new ModelAndView();
		

		String view;
		
		if(confimationID!=null && encryptSalt!=null && encryptIV!=null) {
			
			TokenUserRegister token = generadorTokenRegistroUsuario.decryptToken(confimationID, encryptSalt, encryptIV);
			//confirmar registro
			if(token!=null) {
				
				String otp = token.getOtp();
				
				if(otp!=null) {
					
					//Crear jugador
					ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP,ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request) );
					
					String idUser = token.getUser_user();
					String codigo_acceso = token.getCodigoAcceso();
					
					try {
						sinErrores = clientService.validarRegistro(controlOperacion, idUser, otp);
						
						//Envio de link de confirmacion
						if(sinErrores) {							
							MensajeApp mensajeApp = new MensajeApp();
							mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
							mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
							listaMensajesApp.add(mensajeApp);
						}else {
							
							logger.error("ERROR_CONFIRM_FALLIDO");
							
							
							MensajeApp mensajeApp = new MensajeApp();
							mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
							mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
							listaMensajesApp.add(mensajeApp);
						}
						
						
					} catch (Exception e) {
						
						logger.error("ERROR_NC:"+e.getMessage());
						
						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
						listaMensajesApp.add(mensajeApp);
						
					}
				
				}else{
					
					logger.error("ERROR_OTP_NL");
					MensajeApp mensajeApp = new MensajeApp();
					mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
					mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
					listaMensajesApp.add(mensajeApp);
				}
			}else {
				
				logger.error("ERROR_TOKEN_NL");
				MensajeApp mensajeApp = new MensajeApp();
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);
		
			}	
			
			

			if(sinErrores) {
				 view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/client_register_confirm_ok";
			}else{
				 view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/client_register_confirm_error";
			}
			
			
			
		}else {
			
			logger.error("ERROR_CONFIRMID_NL");
			
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			view = "redirect:"+URL_REQUEST;
			listaMensajesApp.add(mensajeApp);
		}
		
		
		generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, listaMensajesApp);
		modelAndView.setViewName(view);
		
		return modelAndView;
	}
	
	
	private Boolean isTpidentValido(String tpident) {
		try {
			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, "", "");
			
			
			List<TipoDocumento> tipos = clientService.getTiposDocumento(controlOperacion);
			
			if(tipos!=null) {
				for (TipoDocumento tipoDocumento : tipos) {
					if(tipoDocumento.getTdoc_tdoc().equals(tpident)) {
						return true;
					}
				}
			}else{
				return null;
			}
			
			
			
			
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());;
			
			return false;
			
		}
		
		return false;
	}	
		

			
		

	
	
    	
    
   

}