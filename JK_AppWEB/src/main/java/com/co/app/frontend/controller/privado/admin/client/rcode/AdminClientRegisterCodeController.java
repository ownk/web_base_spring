
package com.co.app.frontend.controller.privado.admin.client.rcode;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.commons.ValidationRegulareEpressions;
import com.co.app.model.dto.client.RegisterCode;
import com.co.app.model.dto.client.SendEmailRegisterCode;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.User;

@Controller
public class AdminClientRegisterCodeController {

	private static final String EVENT_DELETE_CODE = "DELETE_CODE";
	private static final String EVENT_ADD_CODE = "ADD_CODE";
	private static final String EVENT_SEND_CODE = "SEND_CODE";

	private Logger logger = LoggerFactory.getLogger(AdminClientRegisterCodeController.class);

	public static final String URL = "/private/admin/client/rcodes";

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	ClientService clientService;

	@Autowired
	ValidationRegulareEpressions validationRegulareEpressions;

	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView getInfo(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {

			view = ConstantesGeneralesAPP.PAGES_HTML + AdminClientRegisterCodeController.URL + "/admin_client_rcodes";

			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {

					// Se consulta info
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					List<RegisterCode> list = clientService.getCodigosRegistro(controlOperacionServicio);

					modelAndView.addObject("rcodes", list);

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);

		}

		modelAndView.setViewName(view);
		return modelAndView;

	}

	@RequestMapping(value = { URL + "/doevent" }, method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView doevent(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {

			view = "redirect:" + URL;

			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {

					Boolean isInfoValida = true;

					String event = request.getParameter("event");

					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					if (event != null) {

						List<RegisterCode> rcodes = clientService.getCodigosRegistro(controlOperacionServicio);

					

							if (event.equals(EVENT_ADD_CODE)) {

								String codes = request.getParameter("codes");

								if (codes != null) {

									Integer totalCodes = Integer.parseInt(codes);

									Boolean sinErrores = clientService.crearCantRegisterCode(controlOperacionServicio,
											totalCodes);

									if (sinErrores) {
										MensajeApp mensajeApp = new MensajeApp();
										mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
										mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
										sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(),
												mensajeApp);

									} else {
										logger.error("ERROR_RCODE_FAILED");

										MensajeApp mensajeApp = new MensajeApp();
										mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
										mensajeApp
												.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_OPER_WITH_ERRORS);
										sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(),
												mensajeApp);
									}
								} else {
									isInfoValida = false;
								}

							} else if (event.equals(EVENT_DELETE_CODE)) {

								String rcode_rcode = request.getParameter("rcode");

								RegisterCode registerCode = getRcode(rcodes, rcode_rcode);

								if (registerCode != null) {

									if (RegisterCode.STATE_ACTIVE.equals(registerCode.getRcode_state())) {

										ControlOperacionServicio controlOperacion = new ControlOperacionServicio(
												sessionAppUsuario);
										Boolean isOk = clientService.deleteRegisterCode(controlOperacion,
												registerCode.getRcode_rcode());

										if (isOk) {

											MensajeApp mensajeApp = new MensajeApp();
											mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
											mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
											sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);

										} else {
											MensajeApp mensajeApp = new MensajeApp();
											mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
											mensajeApp.setDatoMensaje(
													ConstantesGeneralesAPP.MSG_APP_ERROR_OPER_WITH_ERRORS);
											sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
										}

									} else {
										isInfoValida = false;
									}

								} else {
									isInfoValida = false;
								}

							} else if (event.equals(EVENT_SEND_CODE)) {

								String rcode_rcode = request.getParameter("rcode");
								String toName = request.getParameter("toName");
								String toEmail = request.getParameter("toEmail");

								RegisterCode registerCode = getRcode(rcodes, rcode_rcode);

								if (registerCode != null && RegisterCode.STATE_ACTIVE.equals(registerCode.getRcode_state())) {

									if (!StringUtils.isEmpty(toName) && !StringUtils.isEmpty(toEmail)) {
										
										toEmail = toEmail.trim();

										if (validationRegulareEpressions.isEmail(toEmail)) {

											Boolean isOk = sendRegisterCode(locale, controlOperacionServicio, registerCode, toEmail, toName, sessionAppUsuario.getUsuario());

											if (isOk) {

												MensajeApp mensajeApp = new MensajeApp();
												mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
												mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
												sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);

											} else {
												MensajeApp mensajeApp = new MensajeApp();
												mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
												mensajeApp.setDatoMensaje(
														ConstantesGeneralesAPP.MSG_APP_WARN_EMAIL_NO_SEND);
												sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
											}

										} else {
											MensajeApp mensajeApp = new MensajeApp();
											mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
											mensajeApp.setDatoMensaje(
													"admin_client_rcodes.EVENT_SEND_CODE.ERROR_MAIL_NV");

											sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
										}

									} else {
										MensajeApp mensajeApp = new MensajeApp();
										mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
										mensajeApp.setDatoMensaje(
												"admin_client_rcodes.EVENT_SEND_CODE.ERROR_INFO_TO_SEND_NV");

										sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
									}

								} else {
									isInfoValida = false;
								}

							} else {
								isInfoValida = false;
							}

						

					}
					
					
					if (!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
								ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
								ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(URL, mensajeWeb);
					}
					
					

					modelAndView.setViewName(view);
					return modelAndView;

					

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
							ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(URL, mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null,
						ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(URL, mensajeWeb);

			}

		}

		modelAndView.setViewName(view);
		return modelAndView;

	}

	private RegisterCode getRcode(List<RegisterCode> rcodes, String idRcode) {
		if (idRcode != null) {

			for (RegisterCode rcode : rcodes) {
				if (rcode.getRcode_rcode().equals(idRcode)) {
					return rcode;
				}
			}

		}

		return null;
	}

	private Boolean sendRegisterCode(Locale locale, ControlOperacionServicio controlOperacionServicio,
			RegisterCode registerCode, String toEmail, String toName, User user) {

		SendEmailRegisterCode sendEmailRegisterCode = new SendEmailRegisterCode();
		sendEmailRegisterCode.setSerc_rcode(registerCode.getRcode_rcode());
		sendEmailRegisterCode.setSerc_email(toEmail);
		sendEmailRegisterCode.setSerc_name(toName);
		sendEmailRegisterCode.setSerc_user(user.getUser_user());

		try {
			clientService.sendEmailClientCode(locale, controlOperacionServicio, registerCode, sendEmailRegisterCode);

			return true;
		} catch (Exception e) {

			logger.error(e.getMessage());
			return false;
		}

	}

}
