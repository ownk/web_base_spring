package com.co.app.frontend.controller.publico.client.register;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.user.PasswordValidationResponse;
import com.co.app.model.dto.user.User;
import com.co.app.model.dto.user.UsernameValidationResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("/rest/public/register/client")
public class ClientRegisterRESTController {


	@Autowired
	IPNavegadorService ipNavegadorServicio; 

	@Autowired
	UserService usuarioServicio;

	@Autowired
	MailServicio mailServicio;

	@Autowired
	ClientService clientService;





	private Logger logger = LoggerFactory.getLogger(ClientRegisterRESTController.class);


	@RequestMapping(value = "/nick/{nickdata}",
			method = RequestMethod.GET, 
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public MensajeREST<Boolean> isNickValido(@PathVariable(value="nickdata") String nickdata, HttpServletRequest request, HttpServletResponse response) {

		logger.info("Consultando nick..."+nickdata);	

		Boolean isValido = true;
		Boolean userNickValid = false;
		if(nickdata!=null) {

			/*
			 * =======================================================
			 * DECRYPT PARAMETERS
			 * =======================================================
			 * Desencripta parametros nick 
			 * -------------------------------------------------------
			 */
			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
			String nickdataDecrypt = RequestParameterService.decryptText(nickdata);

			String nickNormalizado = nickdataDecrypt.toLowerCase();
			
			UsernameValidationResponse usernameValidationResponse = null;

			try {
				usernameValidationResponse = usuarioServicio.isUserNickValid(controlOperacion, nickdataDecrypt);
				if(usernameValidationResponse.isValid()) {
					userNickValid=true;

				}
			} catch (RespuestaFallidaServicio e1) {
				logger.debug(e1.getMessage());
			}
			
			if (userNickValid) {
				
				try {
					User usuarioExistente = usuarioServicio.getUsuarioPorNick(controlOperacion,nickNormalizado );

					if(usuarioExistente!=null) {

						isValido = false;
					}else {
						isValido = true;
					}


				} catch (RespuestaFallidaServicio e) {

					isValido = false;
				}

				
			} else {
				
				isValido = false;

			}
			
			
		}



		MensajeREST<Boolean> mensajeREST = new MensajeREST<Boolean>();
		mensajeREST.setDetalle(isValido);
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);


		return mensajeREST;
	}





	@RequestMapping(value = "/email/{emaildata}", //
			method = RequestMethod.GET, //
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public MensajeREST<Boolean> isEmailValido(@PathVariable("emaildata") String emaildata, HttpServletRequest request, HttpServletResponse response) {


		MensajeREST<Boolean> mensajeREST = new MensajeREST<Boolean>();
		
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);

		if(emaildata!=null) {
			
			
			/*
			 * =======================================================
			 * DECRYPT PARAMETERS
			 * =======================================================
			 * Desencripta parametros email 
			 * -------------------------------------------------------
			 */

			String emailDataDecrypt = RequestParameterService.decryptText(emaildata);
			
			
			Boolean isValido = mailServicio.isStructureEmailOK(emailDataDecrypt);

			if(isValido!=null) {

				String emailNormalizado = emailDataDecrypt.toLowerCase();
				ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
				try {
					Boolean emailValido = usuarioServicio.validarEmail(controlOperacion, emailNormalizado );

					if(emailValido) {					
						isValido = true;
					}else {
						isValido = false;
					}				

				} catch (RespuestaFallidaServicio e) {

					isValido = false;
				}

			}
			
			mensajeREST.setDetalle(isValido);

			
		}else{
			mensajeREST.setDetalle(false);
		}
			
		return mensajeREST;
	}


	@RequestMapping(value = "/exist", //
			method = RequestMethod.POST, //
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public MensajeREST<Boolean> isClientValido(@RequestBody Client client, HttpServletRequest request, HttpServletResponse response) {

		Boolean isValido  = false;

		if(client!=null) {
			
			
			
			String tpident = RequestParameterService.decryptText(client.getClient_tpident());
			String numident = RequestParameterService.decryptText(client.getClient_numident());
			
			

			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));

			try {
				Client clientExistente = clientService.getClientePorIdent(controlOperacion, tpident, numident);


				if(clientExistente!= null) {
					isValido = false;

				}else {
					isValido = true;
				}



			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());

				isValido = false;
			}


		}else {
			isValido = false;
		}


		MensajeREST<Boolean> mensajeREST = new MensajeREST<Boolean>();
		mensajeREST.setDetalle(isValido);
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
		return mensajeREST;

	}

	@RequestMapping(value = "/pass/validation",
			method = RequestMethod.POST, //
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public MensajeREST<Boolean> isPasswordValido(@RequestBody ObjectNode objectNode, HttpServletRequest request, HttpServletResponse response) {



		Boolean isValido = true;


		if(objectNode !=null) {


			String userName = objectNode.get("user_name").asText();
			String userNick = objectNode.get("user_nick").asText();
			String userPass = objectNode.get("user_pass").asText();
			String clientName = objectNode.get("client_name").asText();

			/*
			 * =======================================================
			 * DECRYPT PARAMETERS
			 * =======================================================
			 * Desencripta parametros nick 
			 * -------------------------------------------------------
			 */

			String userNameDecrypt = RequestParameterService.decryptText(userName);
			String userNickDecrypt = RequestParameterService.decryptText(userNick);
			String userPassDecrypt = RequestParameterService.decryptText(userPass);
			String clientNameDecrypt = RequestParameterService.decryptText(clientName);

			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
			PasswordValidationResponse passwordValidationResponse = null;




			try {
				passwordValidationResponse = usuarioServicio.IsPasswordValid(controlOperacion, userNameDecrypt,  userNickDecrypt,userPassDecrypt);

				if(passwordValidationResponse.isValid()) {
					passwordValidationResponse = usuarioServicio.IsPasswordValid(controlOperacion, clientNameDecrypt,  userNickDecrypt,userPassDecrypt);
					if(passwordValidationResponse.isValid()) {
						isValido = true;
					}else {
						isValido = false;
					}


				}else {

					isValido = false;
				}


			} catch (RespuestaFallidaServicio e) {

				isValido = false;
			}


		}



		MensajeREST<Boolean> mensajeREST = new MensajeREST<Boolean>();
		mensajeREST.setDetalle(isValido);
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);


		return mensajeREST;
	}




}