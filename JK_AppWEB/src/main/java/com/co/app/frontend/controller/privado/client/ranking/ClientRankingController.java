package com.co.app.frontend.controller.privado.client.ranking;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@Controller
public class ClientRankingController {

	private Logger logger = LoggerFactory.getLogger(ClientRankingController.class);

	public static final String URL = "/private/client/ranking";

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	GameServicio gameServicio;
	
	@Autowired
	AppService appService;

	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getRanking(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML + ClientRankingController.URL + "/client_ranking";


			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {
					Boolean isInfoValida = true;
					
					Client client = sessionAppUsuario.getCliente();
					
					if(client!=null) {
						
						List<Player> players = null; 
							
						// Se consulta ranking
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						String rankingDays = request.getParameter("rankingDays");
						
						if(rankingDays!=null && rankingDays.length()>0) {
							
							
							
							Date sysdate = null;
							try {
								sysdate = appService.getSysdate();
							} catch (RespuestaFallidaServicio e) {
								logger.error(e.getMensaje());
							}
						 	
						 	if(sysdate==null) {
						 		sysdate = new Date();
						 	}
							
							Date endDate = sysdate;
							Calendar cal = Calendar.getInstance();
						    cal.setTime(endDate);
						
						    cal.add(Calendar.DAY_OF_WEEK, -1*Integer.parseInt(rankingDays));
						    
							Date startDate = cal.getTime();
							
							modelAndView.addObject("rankingDays",rankingDays);
							
							players = gameServicio.obtenerRankingEmpresaPorFecha(controlOperacionServicio, startDate, endDate, client.getClient_client());	
							
						}else {
							players = gameServicio.obtenerRankingEmpresaPlayers(controlOperacionServicio, client.getClient_client());
							
						}

						if (players != null) {

							
							modelAndView.addObject("players", players);

						} else {
							isInfoValida = false;
						}
						
					}else {
						isInfoValida = false;
					}
					
					
					
					
					if(!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

					

				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}
	

}
