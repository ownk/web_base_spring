package com.co.app.frontend.controller.privado.player.search;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.User;

@RestController
@RequestMapping("rest/private/player/search")
public class PlayerSearchRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio juegoServicio;
	
	@Autowired
	RequestParameterService parametrosRequestServicio;
	
	@Autowired
	UserService usuarioServicio;
	
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	
	private Logger logger = LoggerFactory.getLogger(PlayerSearchRESTController.class);

	
	@RequestMapping(value = "/nick/{nickdata}",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<Player> getPlayerByNick(@PathVariable(value="nickdata") String nickdata, HttpServletRequest request, HttpServletResponse response) {
	
		MensajeREST<Player> mensajeREST = new MensajeREST<Player>();
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			
			
			try {
				
				String url = autenticadorServicio.getURLWithOutParameters(request);
				
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValidoURL(request, url);
				
				if(isAccessoValido) {
					
					if(nickdata!=null) {
						
						String nickNormalizado = nickdata.toLowerCase();
						ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
						try {
							User usuarioExistente = usuarioServicio.getUsuarioPorNick(controlOperacion,nickNormalizado );
							
							
							if(usuarioExistente!=null) {
								String idUsuario = usuarioExistente.getUser_user();
								Player player= playerServicio.getPlayerPpalByUser(controlOperacion, idUsuario, true);
								
								if(player!=null) {
									mensajeREST.setDetalle(player);							
									mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
									
								}else {
									mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
									
								}
								
								
							}else {
								mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
								
							}
							
							
						} catch (RespuestaFallidaServicio e) {
							logger.error(e.getMensaje());
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
						}
						
					}else {
						
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
						
					
					}
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			
			
		}else {

			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
		}		
		
		
		
		return mensajeREST;
    }
  
	
  
}