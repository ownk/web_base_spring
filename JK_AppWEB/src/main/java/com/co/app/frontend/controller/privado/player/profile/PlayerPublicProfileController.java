package com.co.app.frontend.controller.privado.player.profile;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Avatar;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Pet;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.Ship;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@Controller
public class PlayerPublicProfileController {

	private Logger logger = LoggerFactory.getLogger(PlayerPublicProfileController.class);

	public static final String URL = "/private/player/profile/public";

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	GameServicio gameServicio;

	@Autowired
	PlayerServicio playerServicio;

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@PostMapping(URL)
	public ModelAndView publicProfile(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML + URL + "/player_publicprofile_details";


			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {

					
					Boolean isInfoValida = true;

					// Se consulta parametros
					Player player = (Player) parametrosRequestServicio.getRequestParameterToObject(request, "Player",
							Player.class, null);

					if (player != null ) {

						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(
								sessionAppUsuario);

						player = gameServicio.getPlayerbyNick(controlOperacionServicio, player.getPlayer_name());

						if (player != null && player.getPlayer_type()!=null) {
							modelAndView.addObject("player", player);

							Avatar avatar = playerServicio.obtenerAvatarPlayer(controlOperacionServicio,
									player.getPlayer_player());
							Pet pet = playerServicio.obtenerPetPlayer(controlOperacionServicio,
									player.getPlayer_player());
							Ship ship = playerServicio.obtenerShipPlayer(controlOperacionServicio,
									player.getPlayer_player());

							 List<Match> matches = playerServicio.obtenerMatchesPlayer(controlOperacionServicio, player.getPlayer_player(),null,100,0);
							 
							
							modelAndView.addObject("avatar", avatar);
							modelAndView.addObject("pet", pet);
							modelAndView.addObject("ship", ship);
							modelAndView.addObject("matches", matches);

						} else {

							isInfoValida = false;

						}
					} else {
						isInfoValida = false;
					}

					if(!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

				} else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

				}

			} catch (RespuestaFallidaServicio e) {
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}

}
