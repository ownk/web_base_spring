package com.co.app.frontend.controller.privado.home;

import java.nio.channels.SeekableByteChannel;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.frontend.controller.privado.player.profile.PlayerPrivateProfileController;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@Controller
public class HomePrivateController {
	
	
	public static final String URL = "/private/home";
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoUsuarioAutenticado;
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	@GetMapping(URL)
	public ModelAndView getHome(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					view = ConstantesGeneralesAPP.PAGES_HTML+URL+"/home";
					
					
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
					
					Player player= playerServicio.getPlayerPpalByUser(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), false);
					if(player!=null) {
						  view = "redirect:"+PlayerPrivateProfileController.URL;
					}else {
						generadorInfoUsuarioAutenticado.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
					}
					
					
					
				}
				
			} catch (RespuestaFallidaServicio e) {
				
			}
			
			
			
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
	}
	
	

}
