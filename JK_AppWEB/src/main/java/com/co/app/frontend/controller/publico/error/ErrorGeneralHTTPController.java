package com.co.app.frontend.controller.publico.error;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;

@Controller
public class ErrorGeneralHTTPController implements ErrorController {
	
	public static final String URL="/error";

	@Override
	public String getErrorPath() {
		
	    return URL;
	} 
	
	@RequestMapping(URL)
    public ModelAndView handleError(HttpServletRequest request) {
		
		ModelAndView modelAndView = new ModelAndView();
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
	     
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+"/public/error/error";
		
		
	    if (status != null) {
	        Integer statusCode = Integer.valueOf(status.toString());
	     
	        if(statusCode == HttpStatus.NOT_FOUND.value()) {
	        	
	        	view=ConstantesGeneralesAPP.PAGES_HTML+"/public/error/error-404";
	    		
	        }
	        else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
	        	view= ConstantesGeneralesAPP.PAGES_HTML+"/public/error/error-500";
	        }
	    }
	    
	    modelAndView.setViewName(view);
	    return modelAndView;
	    
	    
    }
    
}