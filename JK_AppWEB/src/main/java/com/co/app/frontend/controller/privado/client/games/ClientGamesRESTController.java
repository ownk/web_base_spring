package com.co.app.frontend.controller.privado.client.games;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@RestController
@RequestMapping("rest/private/client/games")
public class ClientGamesRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio juegoServicio;
	
	
	@Autowired
	ClientService clientService;
	
	
	
	private Logger logger = LoggerFactory.getLogger(ClientGamesRESTController.class);

	
	@RequestMapping(value = "/all",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Game>> getGames(HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			MensajeREST<List<Game>> mensajeREST = new MensajeREST<List<Game>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			Boolean isInfoValida = true;
			
			try {

				
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
						
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					Client client = sessionAppUsuario.getCliente();
					
					
					if(client!=null) {
						//Se consulta la informacion para configurar personaje
						List<Game> games = clientService.getGamesPorCliente(controlOperacionServicio, client.getClient_client());
						
						if(games!=null) {
							mensajeREST.setDetalle(games);
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
						}else {
							isInfoValida = false;
						}
					}else {
						isInfoValida = false;
						
					}
					
					
					if(!isInfoValida) {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
					}
					
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				logger.error(e.getMensaje());
				
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
		
		}else{
			
			MensajeREST<List<Game>> mensajeREST = new MensajeREST<List<Game>>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
	
	
	
	
	
	
	
	
	
	
  
}