
package com.co.app.frontend.controller.privado.client.acode;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.commons.ValidationRegulareEpressions;
import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.ClientAccessCodeAccount;
import com.co.app.model.dto.client.SendEmailAccessCode;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;
import com.co.app.model.dto.user.User;



@Controller
public class ClientAccessCodeController {
	
	
	private static final String EVENT_SEND_CODE = "SEND_CODE";
	
	

	private Logger logger = LoggerFactory.getLogger(ClientAccessCodeController.class);

	public static final String URL = "/private/client/acodes";

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	ClientService clientService;
	
	@Autowired
	ValidationRegulareEpressions validationRegulareEpressions;
	
	

	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getInfo(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML + ClientAccessCodeController.URL + "/client_acodes";


			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {
					
					
					Client client = sessionAppUsuario.getCliente();
					
					Boolean isInfoValida = true;
					
					if(client!=null) {
						
						
						// Se consulta ranking
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						List<ClientAccessCodeAccount> list = clientService.getListClientAccessCodeAccount(controlOperacionServicio, client.getClient_client());
						modelAndView.addObject("acodes", list);
												
						
					}else {
						isInfoValida = false;
					}
					
					
					if(!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

					
					
					
					

				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}
	
	
	@RequestMapping(value = { URL+"/doevent" }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView doevent(Locale locale, HttpServletRequest request, HttpServletResponse response) {
    	
		ModelAndView modelAndView = new ModelAndView();
	    
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
		
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = "redirect:"+URL;
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					Client client = sessionAppUsuario.getCliente();
					String event = request.getParameter("event");
					
					if(client!=null) {
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
						
						client = clientService.getClienteByID(controlOperacionServicio, client.getClient_client());
						
						if(client!=null) {

							if(event!=null) {
								
								List<AccessCode> acodes = clientService.getCodigosAcPorCliente(controlOperacionServicio, client.getClient_client());
								
								if(event.equals(EVENT_SEND_CODE)) {
									
									
									
									String acode_acode = request.getParameter("acode");
									String toName = request.getParameter("toName");
									String toEmail = request.getParameter("toEmail");
									
									
									
									AccessCode accessCode= getAcode(acodes, acode_acode);
									
									if(accessCode!=null && AccessCode.STATE_ACTIVE.equals(accessCode.getAcode_state())) {
										
										if(!StringUtils.isEmpty(toName)&& !StringUtils.isEmpty(toEmail)) {
											
											if(validationRegulareEpressions.isEmail(toEmail)) {
												
												Boolean isOk = sendAccesCode(locale, controlOperacionServicio, accessCode, toEmail, toName, sessionAppUsuario.getUsuario());
												
												if(isOk) {
													
													MensajeApp mensajeApp = new MensajeApp();
													mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
													mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
													sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
													
													
												}else {
													MensajeApp mensajeApp = new MensajeApp();
													mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
													mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_WARN_EMAIL_NO_SEND);
													sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
												}
												
												
											}else {
												MensajeApp mensajeApp = new MensajeApp();
												mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
												mensajeApp.setDatoMensaje("client_acodes.EVENT_SEND_CODE.ERROR_MAIL_NV");
												
												sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
											}
											
											
											
										}else {
											MensajeApp mensajeApp = new MensajeApp();
											mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
											mensajeApp.setDatoMensaje("client_acodes.EVENT_SEND_CODE.ERROR_INFO_TO_SEND_NV");
											
											sessionAppUsuario.agregarMensajeAPP(URL, mensajeApp);
										}
										
										
										
										
									}else {
										isInfoValida = false;
									}
									
								}else {
									isInfoValida = false;
								}
								
							}
							
							
							modelAndView.setViewName(view);
							return modelAndView;
							
						}else {
							isInfoValida = false;
						}
						 
						
					}else {
						 isInfoValida = false;
					}
					 
					
					if(!isInfoValida) {
							modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
							sessionAppUsuario.agregarMensajeAPP(URL, mensajeWeb);
					}

						
					
				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(URL, mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				sessionAppUsuario.agregarMensajeAPP(URL, mensajeWeb);
				
			}
			
		}
		
		modelAndView.setViewName(view);
		return modelAndView;

	}
	
	private AccessCode getAcode(List<AccessCode> acodes, String idAcode) {
		if(idAcode!=null) {
			
			for (AccessCode acode : acodes) {
				if(acode.getAcode_acode().equals(idAcode)) {
					return acode;
				}
			}
			
		}
		
		return null;
	}
	
	

	private Boolean sendAccesCode(Locale locale, ControlOperacionServicio controlOperacionServicio, AccessCode accessCode, String toEmail, String toName, User user) {
		
		
		SendEmailAccessCode sendEmailAcode = new SendEmailAccessCode();
		sendEmailAcode.setSeac_acode(accessCode.getAcode_acode());
		sendEmailAcode.setSeac_email(toEmail);
		sendEmailAcode.setSeac_name(toName);
		sendEmailAcode.setSeac_user(user.getUser_user());
		
		try {
			clientService.sendEmailAccesCode(locale, controlOperacionServicio, accessCode, sendEmailAcode);
			
			return true;
		} catch (Exception e) {
			
			logger.error(e.getMessage());
			return false;
		}
		
		
		
		
	}
	

}
