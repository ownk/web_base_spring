package com.co.app.frontend.controller.privado.player.game;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@RestController
@RequestMapping("rest/private/player/game/details")
public class PlayerGameDetailsRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio juegoServicio;
	
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	
	private Logger logger = LoggerFactory.getLogger(PlayerGameDetailsRESTController.class);

	
	@RequestMapping(value = "/matches/{gameid}",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<List<Match>> getMatchesByGame(@PathVariable(value="gameid") String game_game, HttpServletRequest request, HttpServletResponse response) {
	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			Boolean isInfoValida = true;
			MensajeREST<List<Match>>  mensajeREST = new MensajeREST<List<Match>> ();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			
			try {
				String url = autenticadorServicio.getURLWithOutParameters(request);
				
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValidoURL(request, url);
				
				if(isAccessoValido) {
					
					if(game_game!=null) {
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						Player player = playerServicio.getPlayerPpalByUser(controlOperacionServicio, sessionAppUsuario.getUsuario().getUser_user(), true);
						
						
						if(player!=null) {
							//Se consulta la informacion para configurar personaje
							Game game = playerServicio.obtenerGameByPlayer(controlOperacionServicio, game_game, player.getPlayer_player());
							
							if(game!=null) {
								
								List<Match> matches = playerServicio.obtenerMatchsByGamePlayer(controlOperacionServicio, game.getGame_game(), player.getPlayer_player(),0,0);
								mensajeREST.setDetalle(matches);
							}else {
								isInfoValida = false;
							}
						}else {
							isInfoValida = false;
							
						}
					}
					
					
					if(!isInfoValida) {
						mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
					}
					
					
					
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<List<Match>>  mensajeREST = new MensajeREST<List<Match>> ();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
	
	
	
	
	
	
	
	
	
	
  
}