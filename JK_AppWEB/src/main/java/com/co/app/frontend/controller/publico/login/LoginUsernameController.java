package com.co.app.frontend.controller.publico.login;

import java.util.ArrayList;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA1Service;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.LoginUsernameAttemptService;
import com.co.app.backend.service.business.captcha.RecaptchaService;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.model.dto.general.App;
import com.co.app.model.dto.general.MensajeApp;


@Controller
public class LoginUsernameController {
	
	private Logger logger = LoggerFactory.getLogger(LoginUsernameController.class);
	
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	@Autowired
	LoginUsernameAttemptService loginUsernameAttemptService;
	
	@Autowired
	IPNavegadorService iPNavegadorService;
	
	@Autowired
	RecaptchaService recaptchaServicio;
	
	@Autowired
	AppService appService;
	

	

	@ModelAttribute("recaptchaSiteKey")
	public String getRecaptchaSiteKey() {
		return recaptchaServicio.getRecaptchaSiteKey();
	}

	@RequestMapping(value = { ConstantesGeneralesAPP.URL_LOGIN_USERNAME }, method = { RequestMethod.POST, RequestMethod.GET})
    public ModelAndView getLoginPage(Locale locale ) {
		
		
		
		

        ModelAndView modelAndView = new ModelAndView();
        
        String idForIPValidation = iPNavegadorService.getIDForIPValidation();

		
		if (loginUsernameAttemptService.isBlocked(idForIPValidation)) {
			modelAndView.addObject("enableCaptcha", true);
			
		}
		
		
		
		App app = appService.getApp();
		String app_version = "0.0.0";
		
		if(app!=null) {
			app_version = app.getApp_vers();
			
		}
		
		modelAndView.addObject("app_version", app_version);
		
		
        String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
        modelAndView.addObject("key", uniqueKey);
       
        
    	ArrayList<MensajeApp> listaMensajes = new ArrayList<MensajeApp>();
    	
    	generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, listaMensajes);
        generadorInfoComunPorPagina.generarLang(modelAndView, locale);
        
        modelAndView.setViewName(ConstantesGeneralesAPP.PAGES_HTML+ConstantesGeneralesAPP.URL_LOGIN_USERNAME);
        return modelAndView;
    }

    
}
