package com.co.app.frontend.controller.publico.player.register;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.aes.EncryptDecryptSHA1Service;
import com.co.app.backend.service.business.app.AppService;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.captcha.RecaptchaService;
import com.co.app.backend.service.business.captcha.RecaptchaService.RecaptchaResult;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.commons.ValidationRegulareEpressions;
import com.co.app.backend.service.business.mail.MailServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.backend.service.business.user.UserRegisterTokenGenerator;
import com.co.app.backend.service.business.user.UserService;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.general.TipoDocumento;
import com.co.app.model.dto.mail.Email;
import com.co.app.model.dto.user.TokenUserRegister;
import com.co.app.model.dto.user.User;

@Controller
public class PlayerRegisterController {

	public static final String URL_BASE = "/public/register/player";
	public static final String URL_REQUEST =URL_BASE+"/request";
	public static final String URL_CREATE =URL_BASE+"/create";
	public static final String URL_RESPONSE =URL_BASE+"/response";
	public static final String URL_CONFIRM =URL_BASE+"/confirm";

	

	Logger logger = LoggerFactory.getLogger(PlayerRegisterController.class);

	@Autowired
	UserService usuarioServicio; 

	@Autowired
	ValidationRegulareEpressions validationRegulareEpressions;

	@Autowired
	IPNavegadorService ipNavegadorServicio; 

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	UserRegisterTokenGenerator generadorTokenRegistroUsuario;

	@Autowired
	MailServicio mailServicio;

	@Autowired
	AppService appServicio;


	@Autowired
	PlayerServicio jugadorServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	AutenticadorServicio autenticadorServicio;


	@Autowired
	RecaptchaService recaptchaServicio;




	@ModelAttribute("recaptchaSiteKey")
	public String getRecaptchaSiteKey() {
		return recaptchaServicio.getRecaptchaSiteKey();
	}

	@GetMapping(URL_REQUEST)
	public ModelAndView infoRegistroJugador(Locale locale, HttpServletRequest request, HttpServletResponse response) throws RespuestaFallidaServicio {

		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		
		ModelAndView modelAndView = new ModelAndView();

		String uniqueKey = EncryptDecryptSHA1Service.getInstance().getPrivateKey();
		modelAndView.addObject("key", uniqueKey);

		String textNickName = null;


		textNickName = validationRegulareEpressions.getNickName();

		
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request));
		
		try {
			List<TipoDocumento> tipos = jugadorServicio.getTiposDocumento(controlOperacion);
			modelAndView.addObject("tiposID", tipos);
			
		} catch (RespuestaFallidaServicio e) {
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_GENERAL_NC);	
			listaMensajesApp.add(mensajeApp);
			
		}

		modelAndView.addObject("nickName_regex", textNickName);
		
		generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, listaMensajesApp);

		String view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/player_register_create";
		modelAndView.setViewName(view);

		return modelAndView;
	}


	@PostMapping(URL_CREATE)
	public ModelAndView registrarJugador(Locale locale, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirect) {

		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		User usuarioCreado = null;



		String view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"player_register_create";

		//Se consulta parametros 
		User usuarioNuevo = (User)parametrosRequestServicio.getRequestParameterToObject(request, "Usuario", User.class, null);
		Player playerNuevo = (Player)parametrosRequestServicio.getRequestParameterToObject(request, "Player", Player.class, null);
		
		String confirmacionPass = request.getParameter("confirmacionPass");
		String terminosCondiciones = request.getParameter("terminosCondiciones");
		String codigoAcceso = request.getParameter("codigoAcceso");


		Boolean sinErrores = false;


		//Validacion terminos
		Boolean isTerminosValido = false;

		if(terminosCondiciones!=null && "on".equals(terminosCondiciones)) {
			isTerminosValido = true;
		}else {


			logger.error("ERROR_TERMINOS_NV");

			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			listaMensajesApp.add(mensajeApp);

		}


		//Validacion de PASS
		Boolean isPassValido = false;


		if(confirmacionPass!=null && usuarioNuevo!=null) {
			if(confirmacionPass.equals(usuarioNuevo.getUser_pass())) {
				isPassValido = true;
			}else {
				logger.error("ERROR_PASS_NV");

				MensajeApp mensajeApp = new MensajeApp();
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);
			}

		}



		//Validacion de CAPTCHA
		Boolean isCaptchaValido = false;

		String recaptchaResponse = request.getParameter("g-recaptcha-response");
		if ( recaptchaResponse != null) {

			// I didn't use remoteip here, because it is optional,
			// Reference: https://developers.google.com/recaptcha/docs/verify
			RecaptchaResult result = recaptchaServicio.getResult("", recaptchaResponse);

			if (!result.isSuccess()) {

				logger.error("ERROR_CAPTCHA_NV");

				MensajeApp mensajeApp = new MensajeApp();

				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);


			}else {
				isCaptchaValido = true;
			}
		}




		ModelAndView modelAndView = new ModelAndView();
		if(codigoAcceso!=null && usuarioNuevo!=null  && playerNuevo!=null && isCaptchaValido && isTerminosValido && isPassValido) {

			//Generar otp
			String otp = generadorTokenRegistroUsuario.generarOTP();



			Email emailDto = null;

			if(otp!=null) {

				//Crear jugador
				ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP,ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request) );


				try {
					Boolean validaCamposEntrada = true;

					/*
					 * =======================================================
					 * DECRYPT PARAMETERS
					 * =======================================================
					 * Desencripta parametros de entrada registro jugador
					 * -------------------------------------------------------
					 */

					String userEmailDecrypt = RequestParameterService.decryptText(usuarioNuevo.getUser_email());
					String userNickDecrypt = RequestParameterService.decryptText(usuarioNuevo.getUser_nick());
					String userPassDecrypt = RequestParameterService.decryptText(usuarioNuevo.getUser_pass());
					String userNameDecrypt = RequestParameterService.decryptText(usuarioNuevo.getUser_name());
					String codigoAccesoDecrypt = RequestParameterService.decryptText(codigoAcceso);
					
					String playerNumidentDecrypt = RequestParameterService.decryptText(playerNuevo.getPlayer_numident());
					String playerTpidentDecrypt = RequestParameterService.decryptText(playerNuevo.getPlayer_tpident());
					
					
					/*
					 * =======================================================
					 * VALIDACIONES GENERALES
					 * =======================================================
					 * Validacion de campos
					 * -------------------------------------------------------
					 */
					if (!validationRegulareEpressions.isEmail(userEmailDecrypt)) {

						validaCamposEntrada=false;

					}else if(!validationRegulareEpressions.isTextNumber(userNameDecrypt) && validaCamposEntrada) {

						validaCamposEntrada=false;

					}else if(!validationRegulareEpressions.isNickName(userNickDecrypt) && validaCamposEntrada) {

						validaCamposEntrada=false;

					}else if(!validationRegulareEpressions.isIdentification(playerNumidentDecrypt) && validaCamposEntrada) {
						
						validaCamposEntrada=false;
					}

					
					if(!isTpidentValido(playerTpidentDecrypt)) {
						validaCamposEntrada=false;
					}
				
					
					/*
					 * =======================================================
					 * REGISTRO DE INFO
					 * =======================================================
					 * Si las validaciones exitosas 
					 * -------------------------------------------------------
					 */
					if (validaCamposEntrada) {
						
						usuarioNuevo.setUser_email(userEmailDecrypt);
						usuarioNuevo.setUser_nick(userNickDecrypt);
						usuarioNuevo.setUser_pass(userPassDecrypt);
						usuarioNuevo.setUser_name(userNameDecrypt);
						
						playerNuevo.setPlayer_numident(playerNumidentDecrypt);
						playerNuevo.setPlayer_tpident(playerTpidentDecrypt);
						
						sinErrores = jugadorServicio.registrarJugador(controlOperacion, usuarioNuevo, codigoAccesoDecrypt, otp, playerNuevo );

					}


					//Envio de link de confirmacion
					if(sinErrores) {


						usuarioCreado = usuarioServicio.getUsuarioPorNick(controlOperacion,usuarioNuevo.getUser_nick());
						
						emailDto = jugadorServicio.createEmailConfirmRegister(controlOperacion, usuarioCreado, codigoAcceso, otp);

						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);
						listaMensajesApp.add(mensajeApp);

						

					}


				} catch (Exception e) {
					logger.error("ERROR_NC: "+e.getMessage());

					sinErrores = false;
				}

				//Envio de correo de confirmacion
				if(sinErrores && emailDto!=null) {

					try {
						mailServicio.sendHtmlTemplateEmail(locale, emailDto, MailServicio.HTML_TEMPLATE_EMAIL_REGISTER_PLAYER);


					} catch (Exception e) {

						sinErrores = false;

						logger.error("ERROR_SEND_MAIL: "+e.getMessage());

						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ADVERTENCIA);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_WARN_EMAIL_NO_SEND);
						listaMensajesApp.add(mensajeApp);
					}


				}else {

					sinErrores = false;
					logger.error("ERROR_NC");

					MensajeApp mensajeApp = new MensajeApp();
					mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
					mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
					listaMensajesApp.add(mensajeApp);
				}



			}else {

				logger.error("ERROR_TOKEN_FALLIDO");

				MensajeApp mensajeApp = new MensajeApp();

				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				listaMensajesApp.add(mensajeApp);

			}

		}else {

			logger.error("ERROR_PARAMETERS_NV");

			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			listaMensajesApp.add(mensajeApp);

		}



		redirect.addFlashAttribute("mensajesApp", listaMensajesApp);
		redirect.addFlashAttribute("usuarioCreado", usuarioCreado);
		redirect.addFlashAttribute("sinErrores", sinErrores );
		redirect.addFlashAttribute("accesoValido", true);

		view = "redirect:"+URL_RESPONSE;

		modelAndView.setViewName(view);

		return modelAndView;
	}


	@GetMapping(URL_RESPONSE)
	public ModelAndView respuestaRegistro(Model model,HttpServletRequest request ) {

		ModelAndView modelAndView = new ModelAndView();


		String view = "redirect:"+URL_REQUEST;

		ArrayList<MensajeApp> mensajesAPP = (ArrayList<MensajeApp>) model.asMap().get("mensajesApp");
		User usuarioCreado = (User) model.asMap().get("usuarioCreado");
		Boolean accesoValido = (Boolean ) model.asMap().get("accesoValido");
		Boolean sinErrores = (Boolean ) model.asMap().get("sinErrores");


		if(accesoValido!=null && accesoValido) {


			if(sinErrores!=null && sinErrores) {

				if(usuarioCreado!=null) {

					view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/player_register_create_ok";

				}else {

					view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/player_register_create_error";
				}
			}else {
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/player_register_create_error";
			}




			generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, mensajesAPP);


		}else {
			view = "redirect:"+URL_REQUEST;
		}

		modelAndView.setViewName(view);

		return modelAndView;

	}


	@GetMapping(URL_CONFIRM)
	public ModelAndView confirmarRegistroJugador(Locale locale, HttpServletRequest request, HttpServletResponse response) {


		ArrayList<MensajeApp> listaMensajesApp = new ArrayList<MensajeApp>();
		String confimationID = request.getParameter(PlayerServicio.PARAMETRO_CONFIRMACION_REGISTRO);
		String encryptSalt = request.getParameter(PlayerServicio.PARAMETRO_ENCRYPT_SALT);
		String encryptIV = request.getParameter(PlayerServicio.PARAMETRO_ENCRYPT_IV);
		Boolean sinErrores = false;

		ModelAndView modelAndView = new ModelAndView();


		String view;

		if(confimationID!=null) {

			TokenUserRegister token = generadorTokenRegistroUsuario.decryptToken(confimationID, encryptSalt, encryptIV);
			//confirmar registro
			if(token!=null) {

				String otp = token.getOtp();

				if(otp!=null) {

					//Crear jugador
					ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP,ipNavegadorServicio.getIP(request), ipNavegadorServicio.getNavegador(request) );

					String idUser = token.getUser_user();
					String codigo_acceso = token.getCodigoAcceso();

					try {
						sinErrores = jugadorServicio.validarRegistro(controlOperacion,idUser,otp,codigo_acceso);

						//Envio de link de confirmacion
						if(sinErrores) {							
							MensajeApp mensajeApp = new MensajeApp();
							mensajeApp.setTipoMensaje(MensajeApp.TIPO_EXITO);
							mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_OK);

						}else {
							logger.error("ERROR_CONFIRM_FALLIDO");

							MensajeApp mensajeApp = new MensajeApp();
							mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
							mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
						}


					} catch (Exception e) {

						logger.error("ERROR_NC:"+e.getMessage());

						MensajeApp mensajeApp = new MensajeApp();
						mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
						mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);



					}

				}else{

					logger.error("ERROR_OTP_NL");
					MensajeApp mensajeApp = new MensajeApp();
					mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
					mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
				}
			}else {

				logger.error("ERROR_TOKEN_NL");
				MensajeApp mensajeApp = new MensajeApp();
				mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
				mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);

			}	



			if(sinErrores) {
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/player_register_confirm_ok";
			}else{
				view = ConstantesGeneralesAPP.PAGES_HTML+URL_BASE+"/player_register_confirm_error";
			}



		}else {

			logger.error("ERROR_CONFIRM_NL");
			MensajeApp mensajeApp = new MensajeApp();
			mensajeApp.setTipoMensaje(MensajeApp.TIPO_ERROR);
			mensajeApp.setDatoMensaje(ConstantesGeneralesAPP.MSG_APP_ERROR_INFO_NV);
			view = "redirect:"+URL_REQUEST;
		}


		generadorInfoComunPorPagina.generarMensajesGeneralesAPP(modelAndView, listaMensajesApp);
		modelAndView.setViewName(view);

		return modelAndView;
	}

	
	private Boolean isTpidentValido(String tpident) {
		try {
			ControlOperacionServicio controlOperacion = new ControlOperacionServicio(ConstantesGeneralesAPP.USARIO_GENERICO_APP, "", "");
			
			
			List<TipoDocumento> tipos = jugadorServicio.getTiposDocumento(controlOperacion);
			
			if(tipos!=null) {
				for (TipoDocumento tipoDocumento : tipos) {
					if(tipoDocumento.getTdoc_tdoc().equals(tpident)) {
						return true;
					}
				}
			}else{
				return null;
			}
			
			
			
			
		} catch (RespuestaFallidaServicio e) {
			logger.debug(e.getMensaje());;
			
			return false;
			
		}
		
		return false;
	}	
		




}