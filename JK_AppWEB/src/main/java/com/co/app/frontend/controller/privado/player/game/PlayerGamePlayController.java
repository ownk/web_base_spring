package com.co.app.frontend.controller.privado.player.game;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GamePlayerTokenGenerator;
import com.co.app.backend.service.business.game.api.GameAPIService;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.Match;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.game.api.auth.GameAPI_AuthRequest;
import com.co.app.model.dto.game.api.auth.GameAPI_AuthResponse;
import com.co.app.model.dto.game.api.auth.GameAPI_CloseAppRequest;
import com.co.app.model.dto.game.api.match.GameAPI_GameMatchData;
import com.co.app.model.dto.game.api.match.GameAPI_StartGameMatchRequest;
import com.co.app.model.dto.game.api.match.GameAPI_StartGameMatchResponse;
import com.co.app.model.dto.game.api.match.GameAPI_UpdateGameMatchRequest;
import com.co.app.model.dto.game.api.match.GameAPI_UpdateGameMatchResponse;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;


@Controller
public class PlayerGamePlayController {
	
	private Logger logger = LoggerFactory.getLogger(PlayerGamePlayController.class);
	
	
	public static final String URL = "/private/player/game/play";
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	RequestParameterService parametrosRequestServicio;
	
	
	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;
	
	@Autowired
	PlayerServicio playerService;
	
	@Autowired
	GamePlayerTokenGenerator gamePlayerTokenGenerator; 
	
	@Autowired
	GameAPIService gameAPIService;
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	
	@PostMapping(URL)
	public ModelAndView playGame(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView modelAndView = new ModelAndView();
		
		String view = "redirect:"+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;
	    
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);
		
		if(sessionAppUsuario!=null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML+PlayerGamePlayController.URL+"/player_game_play";
			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);
				
				if(isAccessoValido) {
					
					
					Boolean isInfoValida = true;
					
					
					
					//Se consulta parametros 
					Game gameSelected = (Game)parametrosRequestServicio.getRequestParameterToObject(request, "Game", Game.class, null);
					
					if(gameSelected!=null) {
						
						//Se consultan los mundos por jugador
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
						
						String idUsuario = sessionAppUsuario.getUsuario().getUser_user();
						
						Player player = playerService.getPlayerPpalByUser(controlOperacionServicio, idUsuario, true);
						if(player!=null) {
							
							Game game = playerService.obtenerGameByPlayer(controlOperacionServicio, gameSelected.getGame_game(), player.getPlayer_player());
							
							if(game!=null) {
								
								
								try {
									
									String idSession = SessionAppUsuario.getCurrentIDSession(request);
									
									String token = gamePlayerTokenGenerator.generarToken(controlOperacionServicio, player.getPlayer_player(), game.getGame_game(), idSession);
									
									
									
									
									if(token!=null) {
										
										if(Game.TYPE_GAME_PUBLIC.equals(game.getGame_tpgame())) {
											
											Boolean isPlayOk = playPublicGame(sessionAppUsuario, player, game, token );
											
											if(!isPlayOk) {
												MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.MSG_APP_ERROR_GENERAL_NC);
												sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
											}
											
											
										}
										
										
										String game_url = game.getGame_url()+"?gameToken="+token;
										
										
										modelAndView.addObject("player", player);
										modelAndView.addObject("game", game);
												
										List<Match> matches = playerService.obtenerMatchsByGamePlayer(controlOperacionServicio, game.getGame_game(), player.getPlayer_player(),0,0);
										modelAndView.addObject("matches", matches);
										
										modelAndView.addObject("game_url", game_url);
											
										
									}else {

										isInfoValida = false;
									}
									
									
								} catch (Exception e) {
									logger.error(e.getMessage());
									isInfoValida = false;
								}
								
								
								
								
								
							}else {
								isInfoValida = false;
							}
						}else {
							isInfoValida = false;
						}
						
						
					}else {
						
						isInfoValida = false;
					}
					
					
					if(!isInfoValida) {
					
						
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}

					
					
				}else {
					
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}
				
			} catch (RespuestaFallidaServicio e) {
				
				
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				
			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
		}
		
		
		
		
		modelAndView.setViewName(view);
		return modelAndView;
		
		
		
		
	}
		
	
	private Boolean playPublicGame(SessionAppUsuario sessionAppUsuario, Player player, Game game, String idGToken) {
		
		
		

		ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);
		
		Boolean isPlayOk = false;
		Boolean isGamePlayed = true;
		
		try {
			
			
			List<Match> matches = playerServicio.obtenerMatchsByGamePlayer(controlOperacionServicio, game.getGame_game(), player.getPlayer_player(), 1, 0);
		
			if(matches !=null ) {
				
				if(matches.size()>0) {

					isGamePlayed = true;
					isPlayOk = true;
					
				}
				
				
			}else {
				isGamePlayed = false;
			}
			
			
			
		} catch (RespuestaFallidaServicio e1) {
			logger.error(e1.getMensaje());
		}
		
		
		
		if(true) {
			
			try {
				
				String publicSiteKey = "publicSiteKey";
				String publicAccessKey = "publicAccessKey";
				
				
				GameAPI_AuthRequest authRequest = new GameAPI_AuthRequest();
				authRequest.setSiteKey(publicSiteKey);
				authRequest.setAccessKey(publicAccessKey);
				GameAPI_AuthResponse authResponse = gameAPIService.authAPP(controlOperacionServicio, authRequest);
				
				
				
				try {
					GameAPI_StartGameMatchRequest startGameMatchRequest = new GameAPI_StartGameMatchRequest();
					startGameMatchRequest.setGameToken(idGToken);
					startGameMatchRequest.setAppToken(authResponse.getAppToken());
					
					
					GameAPI_StartGameMatchResponse startGameMatchResponse = gameAPIService.startMatch(controlOperacionServicio, startGameMatchRequest);
					
					
					GameAPI_GameMatchData gameMatchData = new GameAPI_GameMatchData();
					
					if(!isGamePlayed) {
					
						gameMatchData.setMatch_general_score(1);
					}else {
						gameMatchData.setMatch_general_score(0);
					}
					gameMatchData.setMatch_result(Match.RESULT_WIN);
					
					
					GameAPI_UpdateGameMatchRequest updateGameDataMatchRequest = new GameAPI_UpdateGameMatchRequest();
					updateGameDataMatchRequest.setGameMatchID(startGameMatchResponse.getGameMatchID());
					updateGameDataMatchRequest.setAppToken(authResponse.getAppToken());
					updateGameDataMatchRequest.setGameMatchData(gameMatchData);
					
					
					GameAPI_UpdateGameMatchResponse updateGameMatchResponse = gameAPIService.updateMatch(controlOperacionServicio, updateGameDataMatchRequest);
					
					
					if(updateGameMatchResponse.getIsGameUpdated()) {
						isPlayOk = true;
					}else {
						isPlayOk = false;
					}
					
					
					GameAPI_CloseAppRequest closeRequest = new GameAPI_CloseAppRequest();
					closeRequest.setAccessKey(publicAccessKey);
					closeRequest.setSiteKey(publicSiteKey);
					closeRequest.setAppToken(authResponse.getAppToken());
					
					
					gameAPIService.closeAPP(controlOperacionServicio, closeRequest);
					
					
					
				} catch (Exception e) {
					logger.error(e.getMessage());
					
					isPlayOk = false;
				}
				
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				
				isPlayOk = false;
			}
			
			
		}
		
		
		
		
		return isPlayOk;
		
		
	}
	
	

}
