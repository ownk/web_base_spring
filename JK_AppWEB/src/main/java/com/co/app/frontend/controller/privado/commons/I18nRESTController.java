package com.co.app.frontend.controller.privado.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.commons.I18nService;
import com.co.app.model.dto.general.MensajeREST;

@RestController
@RequestMapping("rest/private/i18n")
public class I18nRESTController {
	
	
	@Autowired
	I18nService i18nService;
	
	private Logger logger = LoggerFactory.getLogger(I18nRESTController.class);

	@RequestMapping(value = "/translate",
            method = RequestMethod.POST, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<String> translate(@RequestBody String value, HttpServletRequest request, HttpServletResponse response) {
	
		String translate = i18nService.translate(value);
		
		MensajeREST<String> mensajeREST = new MensajeREST<String>();
		mensajeREST.setDetalle(translate);
		mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
		
		
		return mensajeREST;
    }
   
}