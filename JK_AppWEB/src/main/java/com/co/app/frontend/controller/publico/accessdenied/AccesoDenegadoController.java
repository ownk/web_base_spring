package com.co.app.frontend.controller.publico.accessdenied;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.spring.LoginSpringService;
import com.co.app.model.dto.general.MensajeApp;

@Controller
public class AccesoDenegadoController {
	
	
	@Autowired
	LoginSpringService loginSpringServicio;
	
	@Autowired
	PageGeneralInfoGenerator pageInfoGenerator;
	
	@GetMapping(ConstantesGeneralesAPP.URL_ACCESO_DENEGADO)
	public ModelAndView accessDenied(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		
		List<MensajeApp> listaMensajes = new ArrayList<MensajeApp>();
		MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
		listaMensajes.add(mensajeWeb);
		
		String error=request.getParameter("error");
		
		
		if(error!=null && error.equals(ConstantesGeneralesAPP.ERROR_UNIQUE_SESSION_NV)) {
			
			MensajeApp mensajeWebUniqueSession = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.MSG_APP_ERROR_UNIQUE_SESSION);
			listaMensajes.add(mensajeWebUniqueSession);
			
		}
		
		if(error!=null && error.equals(ConstantesGeneralesAPP.ERROR_LANG_NV)) {
			
			MensajeApp mensajeWebUniqueSession = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.MSG_APP_ERROR_LANG_NV);
			listaMensajes.add(mensajeWebUniqueSession);
			
		}
		
		loginSpringServicio.invalidarSessionUsuario(request);
		
		
		ModelAndView modelAndView = new ModelAndView();
		
		
		pageInfoGenerator.generarMensajesGeneralesAPP(modelAndView, listaMensajes);
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+ConstantesGeneralesAPP.URL_ACCESO_DENEGADO+"/access_denied";
		modelAndView.setViewName(view);
		return modelAndView;
		
	}
	
	

}
