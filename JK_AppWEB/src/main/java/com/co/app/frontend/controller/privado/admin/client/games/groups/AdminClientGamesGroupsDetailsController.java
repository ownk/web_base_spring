
package com.co.app.frontend.controller.privado.admin.client.games.groups;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.commons.PageGeneralInfoGenerator;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.model.dto.client.Client;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeApp;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;



@Controller
public class AdminClientGamesGroupsDetailsController {
	
	private static final String EVENT_ADD_GROUP = "ADD_GROUP";

	private Logger logger = LoggerFactory.getLogger(AdminClientGamesGroupsDetailsController.class);

	public static final String URL = "/private/admin/client/games/groups/details";

	@Autowired
	AutenticadorServicio autenticadorServicio;

	@Autowired
	RequestParameterService parametrosRequestServicio;

	@Autowired
	PageGeneralInfoGenerator generadorInfoComunPorPagina;

	@Autowired
	ClientService clientService;

	@Autowired
	GameServicio gameService;

	
	
	

	@RequestMapping(value = { URL }, method = { RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getInfo(Locale locale, HttpServletRequest request, HttpServletResponse response) {

		
		ModelAndView modelAndView = new ModelAndView();

		String view = "redirect:" + ConstantesGeneralesAPP.URL_ACCESO_DENEGADO;

		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request);

		if (sessionAppUsuario != null) {
			
			view = ConstantesGeneralesAPP.PAGES_HTML + AdminClientGamesGroupsDetailsController.URL + "/admin_client_games_groups_details";


			
			
			try {
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValido(request);

				if (isAccessoValido) {
					
					Client client = (Client)parametrosRequestServicio.getRequestParameterToObject(request, "Client", Client.class, null);
					ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

					
					
					Boolean isInfoValida = true;
					
					if(client!=null) {
						
						modelAndView.addObject("client", client);
				
						client = clientService.getClienteByID(controlOperacionServicio, client.getClient_client());
						
						if(client!=null) {
							
							List<Game> games = gameService.obtenerJuegos(controlOperacionServicio);
							List<TemplateGames> list = clientService.getPlantillasListaJuegos(controlOperacionServicio, client.getClient_client());
							
							modelAndView.addObject("templates", list);
							modelAndView.addObject("client", client);
							modelAndView.addObject("games", games);
							
							
						}else {
							
							isInfoValida = false;
						}
						
					}else {
						isInfoValida = false;
					}
					
					
						
				
					
					
					if(!isInfoValida) {
						modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_PARAMETERS_NV);
						sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
					}
					
					
					
					

				}else {
					modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
					sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);
				}

			} catch (RespuestaFallidaServicio e) {

				modelAndView.addObject(ConstantesGeneralesAPP.PARAMETER_PAGE_ERROR, ConstantesGeneralesAPP.PAGE_ERROR_ACCESS_NV);
				MensajeApp mensajeWeb = new MensajeApp(MensajeApp.TIPO_ERROR, null, e.getCodigoRespuesta());
				sessionAppUsuario.agregarMensajeAPP(sessionAppUsuario.getUrlActual(), mensajeWeb);

			}

			generadorInfoComunPorPagina.generarInfoSessionUsuario(modelAndView, sessionAppUsuario);
			
			
		}

		modelAndView.setViewName(view);
		return modelAndView;

	}
	

}
