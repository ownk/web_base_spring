package com.co.app.frontend.controller.privado.player.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.commons.IPNavegadorService;
import com.co.app.backend.service.business.commons.RequestParameterService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Player;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.MensajeREST;
import com.co.app.model.dto.general.RespuestaFallidaServicio;
import com.co.app.model.dto.session.SessionAppUsuario;

@RestController
@RequestMapping("rest/private/player/profile/public")
public class PlayerPublicProfileRESTController {
	
	
	@Autowired
	IPNavegadorService ipNavegadorServicio; 
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	PlayerServicio playerServicio;
	
	
	@Autowired
	GameServicio gameServicio;
	
	
	@Autowired
	RequestParameterService parametrosRequestServicio;
	
	
	
	
	private Logger logger = LoggerFactory.getLogger(PlayerPublicProfileRESTController.class);

	
  
    
    @RequestMapping(value = "/details/{nickdata}",
            method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
    public MensajeREST<Player> getPlayer(@PathVariable(value="nickdata") String nickdata, HttpServletRequest request, HttpServletResponse response) {
	
    	
    	
    	
		SessionAppUsuario sessionAppUsuario = autenticadorServicio.getSessionAppUsuario(request );
		
		if(sessionAppUsuario!=null) {
						MensajeREST<Player> mensajeREST = new MensajeREST<Player>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
			
			try {
				
				String url = autenticadorServicio.getURLWithOutParameters(request);
				
				Boolean isAccessoValido = autenticadorServicio.isAccesoPrivadoValidoURL(request, url);
				
				if(isAccessoValido) {
					
					
					
					if(nickdata!=null) {
						
						ControlOperacionServicio controlOperacionServicio = new ControlOperacionServicio(sessionAppUsuario);

						Player player = gameServicio.getPlayerbyNick(controlOperacionServicio, nickdata);
						
						playerServicio.completeInfoPlayer(controlOperacionServicio, player);
						

						if(player!=null) {
							mensajeREST.setDetalle(player);							
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_OK);
							
						}else {
							mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_INFO_NV);
							
						}
					}
					
				}else {
					mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NO_AUTHORIZED);
				}
				
			} catch (RespuestaFallidaServicio e) {
				logger.error(e.getMensaje());
				mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_NC);
				
			}
			
			return mensajeREST;
			
			
			
		}else{
			
			MensajeREST<Player> mensajeREST = new MensajeREST<Player>();
			mensajeREST.setCodigoRespuesta(MensajeREST.CODIGO_RESPUESTA_REST_ERROR_SESSION_NL);
			
			return mensajeREST; 
		}
		
    }
  
}