package com.co.app.frontend.controller.publico.error;

public class AppInfoNotValidException  extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2574113836029911433L;

	public AppInfoNotValidException(String error) {
		super(error);
	}

}
