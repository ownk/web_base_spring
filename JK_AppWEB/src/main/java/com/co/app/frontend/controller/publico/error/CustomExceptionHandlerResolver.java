package com.co.app.frontend.controller.publico.error;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.co.app.ConstantesGeneralesAPP;

@Component
@ControllerAdvice
public class CustomExceptionHandlerResolver {
	
	private Logger logger = LoggerFactory.getLogger(CustomExceptionHandlerResolver.class);

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class) 
    public ModelAndView handleError405(HttpServletRequest request, Exception e) { 
    	
    	logger.error("Error 405 handled");
    	
    	if(e!=null) {
    		logger.error(e.getMessage());
    	}
    	
    	
    	ModelAndView modelAndView = new ModelAndView();
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+"/public/error/error-405";
		
	    
	    modelAndView.setViewName(view);
	    return modelAndView;
    }
    
    @ExceptionHandler({NullPointerException.class, ArrayIndexOutOfBoundsException.class, IOException.class, BindException.class, })
    public ModelAndView handleException(Exception e)
    {
    	logger.error("Other Error handled");
    	
    	if(e!=null) {
    		logger.error(e.getMessage());
    	}
    	
    	ModelAndView modelAndView = new ModelAndView();
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+"/public/error/error";
		
	    
	    modelAndView.setViewName(view);
	    return modelAndView;
    }
    
    @ResponseStatus(value=HttpStatus.NOT_FOUND) 
    @ExceptionHandler(AppInfoNotValidException.class)
    public ModelAndView appInfoNotValid(Exception e)
    {
    	logger.error("AppInfoNotValidException handled");
    	
    	if(e!=null) {
    		logger.error(e.getMessage());
    	}
    	
    	ModelAndView modelAndView = new ModelAndView();
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+"/public/error/error-info-nv";
		
	    
	    modelAndView.setViewName(view);
	    return modelAndView;
    }
    
    @ResponseStatus(value=HttpStatus.NOT_FOUND) 
    @ExceptionHandler(Exception.class)
    public ModelAndView entityExceptionHandler(Exception e)
    {
    	logger.error("Other Error handled");
    	
    	if(e!=null) {
    		logger.error(e.getMessage());
    	}
    	
    	ModelAndView modelAndView = new ModelAndView();
		
		String view = ConstantesGeneralesAPP.PAGES_HTML+"/public/error/error-info-nv";
		
	    
	    modelAndView.setViewName(view);
	    return modelAndView;
    }
	
    

}