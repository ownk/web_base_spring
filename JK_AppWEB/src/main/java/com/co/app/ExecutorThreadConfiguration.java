package com.co.app;

import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@Configuration
public class ExecutorThreadConfiguration {
	
	private Logger logger = LoggerFactory.getLogger(ExecutorThreadConfiguration.class);
	
	public static final String ACODES_MAILSENDING_THREAD_PREFIX = "app.thread.executor.acodes.mailsending.process";
	public static final String DEFAULT_THREAD_PREFIX = "app.thread.executor.default.process";
	
	
	//DEFAULT_THREAD
	@Value("${app.thread.executor.default.poolsize.core}")
	private String default_poolsize_core;
	
	@Value("${app.thread.executor.default.poolsize.max}")
	private String default_poolsize_max;
	
	@Value("${app.thread.executor.default.queuecapacity}")
	private String default_queuecapacity;

	
	
	//ACODES_MAILSENDING_THREAD
	@Value("${app.thread.executor.acodes.mailsending.poolsize.core}")
	private String acodes_mailsending_poolsize_core;
	
	@Value("${app.thread.executor.acodes.mailsending.poolsize.max}")
	private String acodes_mailsending_poolsize_max;
	
	
	@Value("${app.thread.executor.acodes.mailsending.queuecapacity}")
	private String acodes_mailsending_queuecapacity;
	
	
	@Bean
    public TaskExecutor executorThreadAcodesMailSending() {
		
		
		
		
		int poolsize_core = 0;
	    int poolsize_max = 0;
	    int queuecapacity = 0;
		try {
			
	        	poolsize_core = Integer.parseInt(acodes_mailsending_poolsize_core);
	            poolsize_max = Integer.parseInt(acodes_mailsending_poolsize_max);
	            queuecapacity = Integer.parseInt(acodes_mailsending_queuecapacity);
	            
	            logger.info("executorThreadAcodesMailSending: especific config");
	            
        }catch (Exception e) {
        	
        	
        	logger.error(e.getMessage());
        	
        	poolsize_core = 5;
        	poolsize_max = 10;
        	queuecapacity = -1;
        	
        	logger.info("executorThreadAcodesMailSending: default app config");
        	
		}
		
		
	    
		
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(poolsize_core);
        executor.setMaxPoolSize(poolsize_max);
        executor.setThreadNamePrefix(ACODES_MAILSENDING_THREAD_PREFIX);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        
        
        
        if(queuecapacity>=0) {
	       	 executor.setQueueCapacity(queuecapacity);
	    }else {
	    	executor.setQueueCapacity(queuecapacity);
	    }
        
        
        executor.initialize();
        return executor;
    } 

    @Bean
    public TaskExecutor executorThreadDefault() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        
        int poolsize_core = 0;
        int poolsize_max = 0;
        int queuecapacity = 0;
        try {
        	poolsize_core = Integer.parseInt(default_poolsize_core);
            poolsize_max = Integer.parseInt(default_poolsize_max);
            queuecapacity = Integer.parseInt(default_queuecapacity);
            
            logger.info("executorThreadDefault: especific config");
            
        }catch (Exception e) {
        	logger.error(e.getMessage());
        	
        	poolsize_core = 5;
        	poolsize_max = 10;
        	queuecapacity = -1;
        	
        	logger.info("executorThreadDefault: default app config");
		}
        
        if(queuecapacity>=0) {
	       	 executor.setQueueCapacity(queuecapacity);
	    }else{
	    	executor.setQueueCapacity(queuecapacity);
	    }
        
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.setCorePoolSize(poolsize_core);
        executor.setMaxPoolSize(poolsize_max);
        executor.setThreadNamePrefix(DEFAULT_THREAD_PREFIX);
        executor.initialize();
        
        return executor;
    }
    

}
