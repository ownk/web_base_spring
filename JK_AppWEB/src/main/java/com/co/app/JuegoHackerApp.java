package com.co.app;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableAsync
public class JuegoHackerApp {

	
	
	@PostConstruct
	public void init() {
		// Setting Spring Boot SetTimeZone
		TimeZone.setDefault(TimeZone.getTimeZone("UTC-5"));
	}

	public static void main(String[] args) {
		SpringApplication.run(JuegoHackerApp.class, args);
		
		//SpringApplication application = new SpringApplication(JuegoHackerApp.class);
        //application.setAdditionalProfiles("ssl");
		
		

	}

}
