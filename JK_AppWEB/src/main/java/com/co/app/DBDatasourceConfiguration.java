package com.co.app;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.co.app.backend.service.business.commons.AppPararameterCryptoService;
import com.zaxxer.hikari.HikariDataSource;


@Configuration
public class DBDatasourceConfiguration {
	

	//Datasource BD
	@Value("${app.datasource.password}")
	private String dsPasswordEncrypt;
	
	@Value("${app.datasource.username}")
	private String dsUsernameEncrypt;
	
	@Value("${app.datasource.url}")
	private String dsUrlEncrypt;
	
	@Value("${app.datasource.idle.minimum}")
	private String dsIdleMinimum;
	
	@Value("${app.datasource.poolsize.maximum}")
	private String dsPoolsizeMaximum;
	
	@Value("${app.datasource.login.timeout}")
	private String dsloginTimeout;
	
	
	@Bean
	public HikariDataSource dataSource() throws SQLException {

		int idle = 0;
		int poolsize = 0;
		int timeout = 0;
		
		
		try {
			idle = Integer.parseInt(getDsIdleMinimum());
			poolsize = Integer.parseInt(getDsPoolsizeMaximum());
			timeout = Integer.parseInt(getDsloginTimeout());
			
			
		} catch (Exception e) {
			idle = 10;
			poolsize = 100;
			timeout = 1000;
		}
		
		
		
		HikariDataSource ds = new HikariDataSource();

		ds.setDriverClassName("org.postgresql.Driver");
		ds.setJdbcUrl(getDsUrl());
		ds.setUsername(getDsUsername());
		ds.setPassword(getDsPassword());
		ds.setMinimumIdle(idle);
		ds.setMaximumPoolSize(poolsize);
		ds.setLoginTimeout(timeout);
		

		return ds;
	}
	
	


	public String getDsPassword() {
		
		if (dsPasswordEncrypt != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(dsPasswordEncrypt);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getDsUsername() {
		
		if (dsUsernameEncrypt != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(dsUsernameEncrypt);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getDsUrl() {
		
		if (dsUrlEncrypt != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(dsUrlEncrypt);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getDsIdleMinimum() {
		
		if (dsIdleMinimum != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(dsIdleMinimum);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getDsPoolsizeMaximum() {
		
		if (dsPoolsizeMaximum != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(dsPoolsizeMaximum);

			return decryptText;

		} else {
			return null;
		}
		
	}


	public String getDsloginTimeout() {
		
		if (dsloginTimeout != null) {

			String decryptText = AppPararameterCryptoService.getInstance().desencriptar(dsloginTimeout);

			return decryptText;

		} else {
			return null;
		}
		
	}
	
	
	

}


