package com.co.app;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Service;

import com.co.app.backend.service.business.autentication.LoginUsernameAttemptService;
import com.co.app.backend.service.business.commons.IPNavegadorService;

@Service
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Autowired
	private LoginUsernameAttemptService loginAttemptService;
	
	@Autowired
    private IPNavegadorService iPNavegadorService;
	

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
   
        final String idForIPValidation = iPNavegadorService.getIDForIPValidation();
 
        loginAttemptService.loginFailed(idForIPValidation);
    
        String url = ConstantesGeneralesAPP.URL_LOGIN_USERNAME+"?error=true";
        redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, url);
        
    }
}