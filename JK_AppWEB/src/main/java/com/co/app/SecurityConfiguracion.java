package com.co.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.co.app.backend.service.business.spring.LoginSpringService;
import com.co.app.external.integration.configuration.JwtAuthenticationFilter1;
import com.co.app.model.dto.user.UserSpringGrant;


@Configuration
@EnableWebSecurity
public class SecurityConfiguracion extends WebSecurityConfigurerAdapter {
//cache
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CustomAuthenticationFailureHandler customAuthenticationFailureHandler;
    

    @Autowired
    private LoginSpringService loginSpringService;
    public static final String[] URLS_PUBLIC_ACCESS = {
    		/*
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            */
    		"/webjars/**",
            "/rest/public/**",
            "/public/register/**",
            "/public/user/**",
            "/public/access_denied",
            "/public/logout",
            "/error",
         
            
    };
    
    public static final String[] URLS_REQUIRED_AUTENTICATION = {
    		"/private/**",
            "/rest/private/**",
    };
    
    
  
    
   
    
    public static final String[] STATIC_PRIVATE_RESOURCES = {
    		"/app_resources/private/**",
    		"/app_resources/pages/private/**",
    };
    
    public static final String[] STATIC_PUBLIC_RESOURCES = {
    		"/app_resources/public/**",
    		"/app_resources/pages/public/**",
    };
    
    @Bean
    public DaoAuthenticationProvider authenticationProvider(LoginSpringService loginSpringServicio) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(loginSpringServicio);
        authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder);
        return authenticationProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider(loginSpringService));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	http.sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.ALWAYS).and()
    	.headers().cacheControl().disable()
        .and()
    	.authorizeRequests()
			.antMatchers("/").permitAll()
			.antMatchers(URLS_PUBLIC_ACCESS).permitAll()
			.antMatchers(ConstantesGeneralesAPP.URL_LOGIN_USERNAME).permitAll()
			.antMatchers(ConstantesGeneralesAPP.URL_LOGIN_PASSWORD).hasAuthority(UserSpringGrant.GRANT_USERNAME_VALID)
			.antMatchers(URLS_REQUIRED_AUTENTICATION).hasAuthority(UserSpringGrant.GRANT_USER_AUTENTICATED)
			.antMatchers(STATIC_PRIVATE_RESOURCES).hasAuthority(UserSpringGrant.GRANT_USER_AUTENTICATED)
			
		.anyRequest()
			.authenticated()
			.and()
			.csrf().ignoringAntMatchers(URLS_PUBLIC_ACCESS).and()
		.formLogin()
			.loginPage(ConstantesGeneralesAPP.URL_LOGIN_USERNAME)
			.successHandler(getLoginSuccessController())
			.failureHandler(authenticationFailureHandler())
			.usernameParameter("username")
			.passwordParameter("username")
		
		.and()
			.logout()
			.addLogoutHandler(getLogoutController())
			.logoutRequestMatcher(new AntPathRequestMatcher(ConstantesGeneralesAPP.URL_LOGOUT))
		.and()
			.exceptionHandling()
			.accessDeniedPage(ConstantesGeneralesAPP.URL_ACCESO_DENEGADO)
		.and()
			.addFilterBefore(new JwtAuthenticationFilter1(),
                        UsernamePasswordAuthenticationFilter.class);
    
    	
    }
    
    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return customAuthenticationFailureHandler;
    }
    
    @Bean
    public AuthenticationSuccessHandler getLoginSuccessController(){
        return loginSpringService;
    }
    
    @Bean
    public LogoutHandler getLogoutController(){
        return loginSpringService;
    }
    
    
    @Override
    public void configure(WebSecurity web) {
    	//Recursos estaticos
    	web.ignoring().antMatchers(STATIC_PUBLIC_RESOURCES);
        web.ignoring().antMatchers(URLS_PUBLIC_ACCESS);
        
    }

}