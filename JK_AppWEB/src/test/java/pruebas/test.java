package pruebas;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.co.app.JuegoHackerApp;
import com.co.app.backend.service.business.autentication.AutenticadorServicio;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.client.AccessCode;
import com.co.app.model.dto.client.DetailLoadMasiveAccessCode;
import com.co.app.model.dto.client.LoadMasiveAccessCodes;
import com.co.app.model.dto.client.RegisterCode;
import com.co.app.model.dto.client.SendEmailAccessCode;
import com.co.app.model.dto.client.SendEmailRegisterCode;
import com.co.app.model.dto.client.TemplateGames;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.general.ControlOperacionServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {JuegoHackerApp.class})


public class test {
	
	
	@Autowired
	AutenticadorServicio autenticadorServicio;
	
	@Autowired
	GameServicio gameServicio;  

	
	@Autowired
	PlayerServicio jugadorServicio; 
	
	@Autowired
	ClientService clientServicio; 
	

	@Test
	public void test() {
		System.out.println("a");
		
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio("Administrador APP", "192.168.88.56", "");
		
		String idClient = "1";

		List<TemplateGames> listaTemplGm= new ArrayList<TemplateGames>();
		TemplateGames tempGms = new TemplateGames();
		
		List<SendEmailAccessCode> listSendEmailAcode = new ArrayList<SendEmailAccessCode>();
		SendEmailAccessCode sendEmailAcode = new SendEmailAccessCode();
		
		sendEmailAcode.setSeac_email("Correo1");
		sendEmailAcode.setSeac_user("1");
		sendEmailAcode.setSeac_acode("20200413-2");
		listSendEmailAcode.add(sendEmailAcode);
		
		List<SendEmailRegisterCode> listSendEmailRcode = new ArrayList<SendEmailRegisterCode>();
		SendEmailRegisterCode sendEmailRcode = new SendEmailRegisterCode();
		
		sendEmailRcode.setSerc_email("Correo1");
		sendEmailRcode.setSerc_user("1");
		sendEmailRcode.setSerc_rcode("1");
		listSendEmailRcode.add(sendEmailRcode);
		
		tempGms.setTemplt_templt("1");
		
		List<Game> listGms = new ArrayList<Game>();
		Game game = new Game();
		game.setGame_game("tempGms");
		listGms.add(game);
		
		tempGms.setGames(listGms);
		listaTemplGm.add(tempGms);
		
		
		LoadMasiveAccessCodes lmac = new LoadMasiveAccessCodes();
		lmac.setLmac_client("1");
		lmac.setLmac_file_trg(10);
		lmac.setLmac_file_url("url");
		lmac.setLmac_templt("1");
		lmac.setLmac_user("1");
		
		List<DetailLoadMasiveAccessCode> detailslmac = new ArrayList<DetailLoadMasiveAccessCode>();
		DetailLoadMasiveAccessCode detaillmac = new DetailLoadMasiveAccessCode();
		detaillmac.setDlmac_email("prueba2");
		detaillmac.setDlmac_file_id_reg(1);
		detaillmac.setDlmac_lmac("123456");
		detaillmac.setDlmac_mnsg("Finaliza");
		detaillmac.setDlmac_state("END");
		detailslmac.add(detaillmac);
		
		String idProcesslmac = "123456";
		String lmac_state = "END";
		String tpIdent = "NJ";
		String numIdent = "123456789";
		String idUser = "2";
		String idPlayerFilter="20191104-1";
		String idPlantilla = "1";
		int cantAc = 1;
		
    	try {
    		
	
    		List<RegisterCode> listAcClient = clientServicio.getCodigosRegistro(controlOperacion);
    		
    		/*
    		List<AccessCode> listAcClient = clientServicio.getCodigosAcPorCliente(controlOperacion, idClient);
    		
    		Boolean ok = clientServicio.registerSendEmailAcode(controlOperacion, listSendEmailAcode);

    		Boolean crearCantAC = clientServicio.registerSendEmailAcode(controlOperacion, listSendEmailAcode);
    		
    		Boolean crearCantAC = clientServicio.crearCantAccessCode(controlOperacion, listaTemplGm, cantAc, idClient);
    		
    		Client clienteId = clientServicio.getClientePorId(controlOperacion, idClient);
    		
    		List<DetailLoadMasiveAccessCode> createDetailslmac=clientServicio.createDetailsLoadMasiveAccessCode(controlOperacion, detailslmac);
    		
    		String lmac_lmac=clientServicio.createProcessLoadMasiveAccessCode(controlOperacion, lmac);
    		
    		Client clientIdent=clientServicio.getClientePorIdent(controlOperacion, tpIdent, numIdent);
    		
    		Client clientUser = clientServicio.getClientePorUser(controlOperacion, idUser);
    		
    		
    		List<Account> acntClient = clientServicio.getCuentasPorCliente(controlOperacion, idClient);
    		
    		List<Game> gamesClient = clientServicio.getGamesPorCliente(controlOperacion, idClient);
    		
    		List<Player> playerInfoReportClient = clientServicio.getInfoReportePlayersClient(controlOperacion, idClient, idPlayerFilter);
    		
    		List<TemplateGames> listTemp=clientServicio.getPlantillasListaJuegos(controlOperacion, idClient);
    		
    		TemplateGames templGmsClient=clientServicio.getPlantillaListaJuegos(controlOperacion, idClient, idPlantilla);
    		
    		List<UserPendingConfirmatioRegister> playerNoConfirm = clientServicio.getPlayersClientEmailNoConfirmado(controlOperacion, idClient, idUser);
    		
    		List<Player> playersClient=clientServicio.getPlayersPorCliente(controlOperacion, idClient);
    		
    		List<User> usersClient=clientServicio.getUsuariosPorCliente(controlOperacion, idClient);
    		
    		List<LoadMasiveAccessCodes> ok=clientServicio.getProcessLoadMasiveAccessCode(controlOperacion,idClient);
    		
    		Boolean updateDetailsLmac=clientServicio.updateDetailsLoadMasiveAccessCode(controlOperacion, detailslmac);		
    		
    		Boolean updateProcessLmac=clientServicio.updateProcessLoadMasiveAccessCode(controlOperacion, idProcesslmac, lmac_state);
    		*/
    	} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	
    		
    		
	}

}
