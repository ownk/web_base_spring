package pruebas;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.co.app.JuegoHackerApp;
import com.co.app.backend.service.business.client.ClientService;
import com.co.app.backend.service.business.game.GameServicio;
import com.co.app.backend.service.business.player.PlayerServicio;
import com.co.app.model.dto.game.Game;
import com.co.app.model.dto.game.LearningObjective;
import com.co.app.model.dto.general.ControlOperacionServicio;
import com.co.app.model.dto.general.RespuestaFallidaServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {JuegoHackerApp.class})


public class Test_CrearPlantillaJuegos {
	
	@Autowired
	GameServicio gameServicio;  

	
	@Autowired
	PlayerServicio jugadorServicio; 
	
	@Autowired
	ClientService clientServicio; 
	


	@Test
	public void test() {
		System.out.println("a");
		
		ControlOperacionServicio controlOperacion = new ControlOperacionServicio("Administrador APP", "192.168.88.56", "");

		String idGame = "101";
    	try {
			String idClient = "1";
			String descriPlantilla ="PLantilla Prueba";
			
			List<Game> juegos = gameServicio.obtenerJuegos(controlOperacion);
			
			
			
			
			Boolean sinErrores = clientServicio.crearPlantilla(controlOperacion, juegos, descriPlantilla, idClient);
		} catch (RespuestaFallidaServicio e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			e.printStackTrace();
		}
    	
    	
    		
    		
	}

}
