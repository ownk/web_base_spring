--
-- Name: type_to_ge_mnsj; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GE_MNSJ AS (
	FECCRE  TIMESTAMP,
	PRCS    VARCHAR(100),
	CODIGO  VARCHAR(100),
	MENSAJE TEXT
);

--ALTER TYPE TYPE_TO_GE_MNSJ OWNER TO juegohacker_us;