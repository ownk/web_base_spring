--
-- Name: type_to_ge_pais; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GE_PAIS AS (
	PAIS_PAIS       INTEGER,
	PAIS_NOMBRE     VARCHAR(128),
	PAIS_COD_ISO    VARCHAR(3),
	PAIS_COD_AREA   VARCHAR(16)
);

--ALTER TYPE TYPE_TO_GE_PAIS OWNER TO juegohacker_us;