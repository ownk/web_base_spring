--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_AVATAR AS (
	AVATAR_AVATAR		TEXT,
    AVATAR_NAME   		TEXT,    
    AVATAR_DESCRI  		TEXT,
    AVATAR_SKIN_URL		TEXT,
	AVATAR_CHRT			TEXT
);

--ALTER TYPE TYPE_TO_GM_AVATAR OWNER TO juegohacker_us;