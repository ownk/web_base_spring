--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_LRNOBJ AS (
	LRNOBJ_LRNOBJ		TEXT,
    LRNOBJ_NAME  		TEXT,
    LRNOBJ_DESCRI  	    TEXT,
    LRNOBJ_ICON		    TEXT,
    LRNOBJ_COLOR	  	TEXT,
    LRNOBJ_CLASIF	  	TEXT
);

--ALTER TYPE TYPE_TO_GM_LRNOBJ OWNER TO juegohacker_us;