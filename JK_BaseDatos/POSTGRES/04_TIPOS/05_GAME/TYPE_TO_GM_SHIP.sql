--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_SHIP AS (
	SHIP_SHIP		TEXT,
    SHIP_NAME   	TEXT,    
    SHIP_DESCRI    	TEXT,
    SHIP_SKIN_URL  	TEXT,
	SHIP_CHRT		TEXT
);

--ALTER TYPE TYPE_TO_GM_SHIP OWNER TO juegohacker_us;