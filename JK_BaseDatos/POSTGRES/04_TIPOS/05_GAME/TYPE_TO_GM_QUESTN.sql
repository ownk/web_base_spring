--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_QUESTN AS (
    QUESTN_QUESTN  			TEXT,
	QUESTN_DESCRI			TEXT
);

--ALTER TYPE TYPE_TO_GM_QUESTN OWNER TO juegohacker_us;