--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_ATRCH AS (
	ATRCH_CHRT			TEXT,
    ATRCH_ATRB   		TEXT,    
    ATRCH_INIT_VALUE  	NUMERIC,
	ATRB_DESCRI			TEXT,
	ATRB_NAME			TEXT,
	ATRB_ICON			TEXT,
	ATRB_COLOR			TEXT
);

--ALTER TYPE TYPE_TO_GM_ATRCH OWNER TO juegohacker_us;