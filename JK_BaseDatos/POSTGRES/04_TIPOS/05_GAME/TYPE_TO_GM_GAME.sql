--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_GAME AS (
    GAME_GAME           TEXT,
    GAME_NAME           TEXT,
    GAME_DESCRI         TEXT,
    GAME_IMAGE          TEXT,
    GAME_TPGAME         TEXT,
    GAME_URL            TEXT,
    GAME_STATE          TEXT,
    GAME_IMBIBE         TEXT,
    GAME_TRAILER        TEXT,
    GAME_TRAILER_URL    TEXT,
	GAME_COLOR			TEXT
);

--ALTER TYPE TYPE_TO_GM_GAME OWNER TO juegohacker_us;