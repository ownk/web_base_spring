--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_PET AS (
	PET_PET			TEXT,
    PET_NAME   		TEXT,    
    PET_DESCRI    	TEXT,
    PET_SKIN_URL   	TEXT,
	PET_CHRT		TEXT
);

--ALTER TYPE TYPE_TO_GM_PET OWNER TO juegohacker_us;