--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_PL_PLACN AS (
	PLACN_PLAYER		TEXT,
    PLACN_ACNT   		TEXT
);

--ALTER TYPE TYPE_TO_PL_PLACN OWNER TO juegohacker_us;