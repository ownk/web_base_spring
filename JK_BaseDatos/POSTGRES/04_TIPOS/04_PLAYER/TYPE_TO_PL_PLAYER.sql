--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_PL_PLAYER AS (
    PLAYER_PLAYER        TEXT,
    PLAYER_USER          TEXT,    
    PLAYER_NAME          TEXT,
    PLAYER_DATECR        TIMESTAMP,
    PLAYER_DATEAC        TIMESTAMP,
    PLAYER_TYPE          TEXT,
    PLAYER_SCORE         NUMERIC,
    PLAYER_STATE         TEXT,
    PLAYER_TPIDENT       TEXT,
    PLAYER_NUMIDENT      TEXT,
    PLAYER_ID_EXT        TEXT
);

--ALTER TYPE TYPE_TO_PL_PLAYER OWNER TO juegohacker_us;