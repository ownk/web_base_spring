--
-- Name: TYPE_TO_IN_PARTNR; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_IN_PARTNR AS (
    PARTNR_PARTNR       TEXT,
    PARTNR_NAME         TEXT,    
    PARTNR_SITEKEY      TEXT,
    PARTNR_ACCESSKEY    TEXT,
    PARTNR_STATE        TEXT
);

--ALTER TYPE TYPE_TO_IN_PARTNR OWNER TO juegohacker_us;