--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_ACODE AS (
	ACODE_ACODE				TEXT,
    ACODE_CODE   			TEXT,    
    ACODE_STATE    			TEXT,
	ACODE_CLIENT       		TEXT,
    ACODE_TEMPLT       		TEXT,
	ACODE_DATE_START      	TIMESTAMP,
	ACODE_DATE_VALIDITY     TIMESTAMP
);

--ALTER TYPE TYPE_TO_CL_ACODE OWNER TO juegohacker_us;