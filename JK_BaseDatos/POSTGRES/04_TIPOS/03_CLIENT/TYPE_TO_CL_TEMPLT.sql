--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_TEMPLT AS (
	TEMPLT_TEMPLT				TEXT,
    TEMPLT_CLIENT  				TEXT,    
    TEMPLT_DESCRI    			TEXT
);

--ALTER TYPE TYPE_TO_CL_TEMPLT OWNER TO juegohacker_us;