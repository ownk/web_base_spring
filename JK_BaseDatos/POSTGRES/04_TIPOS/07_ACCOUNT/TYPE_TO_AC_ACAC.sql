--
-- Name: TYPE_TO_AC_ACAC; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AC_ACAC AS (
	ACAC_ACODE       	TEXT,
    ACAC_ACNT       	TEXT
);

--ALTER TYPE TYPE_TO_AC_ACAC OWNER TO juegohacker_us;