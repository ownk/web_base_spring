--
-- Name: TYPE_TO_AU_USNV; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_USNV AS (
	USNV_USNV   varchar(50),
    USNV_USER   text,          
	USNV_DATE   timestamp , 
    USNV_USIN   text,
	USNV_OBSV   text,      
	USNV_STAT   text       
    
);

--ALTER TYPE TYPE_TO_AU_USNV OWNER TO juegohacker_us;