--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_LOGIN AS (
	LOGIN_LOGIN     TEXT,
    LOGIN_USER      TEXT,    
    LOGIN_NICK      TEXT,
	LOGIN_PASS      TEXT,
	LOGIN_DATEREG   TIMESTAMP,
	LOGIN_BROWSER   TEXT,
    LOGIN_IP        TEXT
);

--ALTER TYPE TYPE_TO_AU_LOGIN OWNER TO juegohacker_us;