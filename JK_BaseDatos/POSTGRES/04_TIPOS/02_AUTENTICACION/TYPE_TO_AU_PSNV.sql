--
-- Name: TYPE_TO_AU_PSNV; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_PSNV AS (
	PSNV_PSNV   varchar(50),
    PSNV_USER   text,          
    PSNV_PASS   text,       
	PSNV_DATE   timestamp , 
	PSNV_OBSV   text,      
	PSNV_STAT   text       
    
);

--ALTER TYPE TYPE_TO_AU_PSNV OWNER TO juegohacker_us;