--
-- Name: type_to_aut_modulo; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_MDLO AS (
	MDLO_MDLO   VARCHAR(100),
	MDLO_DESCRI VARCHAR(200),
	MDLO_NOMB   VARCHAR(100)
);

--ALTER TYPE TYPE_TO_AU_MDLO OWNER TO juegohacker_us;