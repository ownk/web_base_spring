--
-- Name: type_to_au_tokn; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_TOKN AS (
	TOKN_TOKN   INTEGER,
	TOKN_USER   varchar(50),
	TOKN_CODIGO varchar(50),
	TOKN_FECCRE TIMESTAMP,
	TOKN_FECEXP TIMESTAMP,
	TOKN_ESTADO VARCHAR(3)
);

--ALTER TYPE TYPE_TO_AU_TOKN OWNER TO juegohacker_us;