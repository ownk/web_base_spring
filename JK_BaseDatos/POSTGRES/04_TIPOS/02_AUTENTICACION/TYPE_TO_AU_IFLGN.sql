--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_IFLGN AS (
	IFLGN_IFLGN             TEXT,
    IFLGN_USER              TEXT,    
	IFLGN_BROWSER           TEXT,
    IFLGN_IP                TEXT,
    IFLGN_SESSION           TEXT,
	IFLGN_DATE_LAST_INGR    TIMESTAMP
);

--ALTER TYPE TYPE_TO_AU_IFLGN OWNER TO juegohacker_us;