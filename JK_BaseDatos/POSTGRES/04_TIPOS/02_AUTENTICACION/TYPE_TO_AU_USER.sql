--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_USER AS (
	USER_USER       TEXT,
    USER_NAME       TEXT,    
    USER_NICK       TEXT,
	USER_PASS       TEXT,
	USER_EMAIL      TEXT,
	USER_STATE      TEXT,
    USER_DCREA      TIMESTAMP,
    USER_STATE_DESC TEXT
);

--ALTER TYPE TYPE_TO_AU_USER OWNER TO juegohacker_us;