-- Table: aut_tilog

CREATE TABLE aut_tilog
(
    ilog_ilog      INTEGER NOT NULL,
    ilog_user      varchar(50) COLLATE pg_catalog."default" NOT NULL,
    ilog_fecha     TIMESTAMP   NOT NULL,
    ilog_ip        varchar(50) COLLATE pg_catalog."default" NOT NULL,
    ilog_resultado VARCHAR(10) COLLATE pg_catalog."default" NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE aut_tilog
    

COMMENT ON COLUMN aut_tilog.ilog_ilog
    IS 'CONSECUTIVO DEL LOG';

COMMENT ON COLUMN aut_tilog.ilog_user
    IS 'USUARIO QUE GENERO EL LOG DE INICIO';

COMMENT ON COLUMN aut_tilog.ilog_fecha
    IS 'MARCA DE TIEMPO';

COMMENT ON COLUMN aut_tilog.ilog_ip
    IS 'IP DESDE DONDE SE GENERO EL LOG';

COMMENT ON COLUMN aut_tilog.ilog_resultado
    IS 'RESULTADO DEL INTENTO DE INICIO SE SESION';
    
    --
-- Name: aut_tilog pk_aut_tilog; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY aut_tilog
    ADD CONSTRAINT pk_aut_tilog PRIMARY KEY (ilog_ilog);