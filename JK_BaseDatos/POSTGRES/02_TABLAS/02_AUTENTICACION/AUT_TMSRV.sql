-- Table: aut_tmsrv

CREATE TABLE aut_tmsrv
(
    msrv_menu       integer NOT NULL,
    msrv_srvc       integer NOT NULL,
    msrv_orden      integer NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE aut_tmsrv
    
COMMENT ON TABLE aut_tmsrv
    IS 'TABLA QUE ALMACENA LOS MENUS PRESTADOS DENTRO DEL APP, ASĂŤ COMO LOS ITEMS A LOS QUE PERTENECEN';

COMMENT ON COLUMN aut_tmsrv.msrv_menu
    IS 'ID DEL MENU EN EL SISTEMA';

COMMENT ON COLUMN aut_tmsrv.msrv_srvc
    IS 'ID DEL SERVICIO';

COMMENT ON COLUMN aut_tmsrv.msrv_orden
    IS 'ORDEN DEL SERVICIOS DENTRO DEL MENU';
    
    --
-- Name: aut_tmsrv pk_aut_tmsrv; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY aut_tmsrv
    ADD CONSTRAINT pk_aut_tmsrv PRIMARY KEY (msrv_menu, msrv_srvc);