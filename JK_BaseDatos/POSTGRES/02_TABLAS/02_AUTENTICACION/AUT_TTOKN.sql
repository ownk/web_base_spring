-- Table: AUT_TTOKN

CREATE TABLE AUT_TTOKN
(
    tokn_tokn   INTEGER NOT NULL,
    tokn_user   varchar(50) COLLATE pg_catalog."default" NOT NULL,
    tokn_codigo varchar(50) COLLATE pg_catalog."default" NOT NULL,
    tokn_feccre timestamp NOT NULL,
    tokn_fecexp timestamp NOT NULL,
    tokn_estado VARCHAR(3) COLLATE pg_catalog."default" NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE AUT_TTOKN
    

COMMENT ON COLUMN AUT_TTOKN.tokn_tokn
    IS 'CODIGO ID DEL REGISTRO CONSECUTIVO';

COMMENT ON COLUMN AUT_TTOKN.tokn_user
    IS 'USUARIO A LA CUAL PERTENECE EL tokn';

COMMENT ON COLUMN AUT_TTOKN.tokn_codigo
    IS 'TOKEN GENERADO';

COMMENT ON COLUMN AUT_TTOKN.tokn_feccre
    IS 'MARCA DE TIEMPO DE LA GENERACION DEL TOKEN';

COMMENT ON COLUMN AUT_TTOKN.tokn_fecexp
    IS 'MARCA DE TIEMPO DE LA EXPIRACION DEL TOKEN';

COMMENT ON COLUMN AUT_TTOKN.tokn_estado
    IS 'ESTADO DEL TOKEN: ACT (ACTIVO), VEN(VENCIDO), USE(USADO)';
    
    --
-- Name: aut_ttokn pk_aut_ttokn; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY AUT_TTOKN
    ADD CONSTRAINT pk_aut_ttokn PRIMARY KEY (tokn_tokn);