-- Table: aut_trsin

CREATE TABLE aut_trsin
(
    rsin_rol    varchar(20) COLLATE pg_catalog."default" NOT NULL,
    rsin_srvc   integer NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE aut_trsin
    
COMMENT ON TABLE aut_trsin
    IS 'TABLA QUE ALMACENA EL SERVICIO DE INICIO POR ROL';

COMMENT ON COLUMN aut_trsin.rsin_rol
    IS 'ROL DE USUARIO';

COMMENT ON COLUMN aut_trsin.rsin_srvc
    IS 'SERVICIO DE INICIO';
    
    --
-- Name: aut_trsin pk_aut_trsin; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY aut_trsin
    ADD CONSTRAINT pk_aut_trsin PRIMARY KEY (rsin_rol, rsin_srvc);
