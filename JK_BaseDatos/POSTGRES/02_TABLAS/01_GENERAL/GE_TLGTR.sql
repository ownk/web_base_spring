-- Table: ge_tlgtr

CREATE TABLE ge_tlgtr
(
    lgtr_lgtr       VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    lgtr_trans      VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    lgtr_terminal   VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    lgtr_user       VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    lgtr_fech_oper  VARCHAR(12)  COLLATE pg_catalog."default" NOT NULL,
    lgtr_hora_oper  VARCHAR(12)  COLLATE pg_catalog."default" NOT NULL,
    lgtr_observ     TEXT         COLLATE pg_catalog."default" NOT NULL,
    lgtr_prcs       VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    lgtr_id_entidad VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    lgtr_fech_ini   TIMESTAMP NOT NULL,
    lgtr_fech_fin   TIMESTAMP,
    lgtr_crta       VARCHAR(100) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_tlgtr
    
    
COMMENT ON TABLE ge_tlgtr
    IS 'LOG DE TRANSACCIONES';

COMMENT ON COLUMN ge_tlgtr.LGTR_LGTR
    IS 'IDENTIFICADOR UNICO DEL LOG DE TRANSACCION';

COMMENT ON COLUMN ge_tlgtr.lgtr_trans
    IS 'TRANSACCION UNICA ENVIADA POR EL PROCESO DE INTEGRACION';

COMMENT ON COLUMN ge_tlgtr.lgtr_terminal
    IS 'CODIGO DE TERMINAL O MAQUINA DESDE LA CUAL SE REALIZA LA OPERACION';

COMMENT ON COLUMN ge_tlgtr.lgtr_user
    IS 'USUARIO QUE ENVIA LA TRANSACCION';

COMMENT ON COLUMN ge_tlgtr.lgtr_fech_oper
    IS 'FECHA PROPIA DE LA OPERACION. FORMATO DDMMYYYY';

COMMENT ON COLUMN ge_tlgtr.lgtr_hora_oper
    IS 'HORA  PROPIA DE LA OPERACION. FORMATO HH24MISS';

COMMENT ON COLUMN ge_tlgtr.lgtr_observ
    IS 'OBSERVACION REFERENTE A LA OPERACION REALIZADA';

COMMENT ON COLUMN ge_tlgtr.lgtr_prcs
    IS 'PROCESO ASOCIADO A LA TRANSACCION. VER TABLA "CN_SGR_US"."GE_TPRCS';

COMMENT ON COLUMN ge_tlgtr.LGTR_ID_ENTIDAD
    IS 'IDENTIFICADOR UNICO DE ENTIDAD / OBJETO PRINCIPAL QUE SE VE AFECTADO POR LA OPERACION. ';

COMMENT ON COLUMN ge_tlgtr.lgtr_fech_ini
    IS 'FECHA EN LA QUE INICIA LA OPERACION.';

COMMENT ON COLUMN ge_tlgtr.lgtr_fech_fin
    IS 'FECHA EN LA QUE FINALIZA LA OPERACION';

COMMENT ON COLUMN ge_tlgtr.lgtr_crta
    IS 'CODIGO DE RESPUESTA CON LA QUE SE FINALIZA LA OPERACION. VER TABNLA "CN_SGR_US"."GE_TCRTA';
    
--
-- Name: ge_tlgtr pk_ge_tlgtr; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY ge_tlgtr
    ADD CONSTRAINT pk_ge_tlgtr PRIMARY KEY (lgtr_lgtr);