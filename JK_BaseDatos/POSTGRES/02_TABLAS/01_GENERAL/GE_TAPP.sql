-- ***************************************************
-- **          PROYECTO WEB BASE                    **
-- **          REV:26\01\2019                       ** 
-- ***************************************************

-- Table: GE_TAPP

CREATE TABLE GE_TAPP
(   
    APP_APP     TEXT        COLLATE pg_catalog."default" NOT NULL,  
    APP_NOMB    TEXT        COLLATE pg_catalog."default" NOT NULL,  
    APP_VERS    TEXT        COLLATE pg_catalog."default" NOT NULL,  
    APP_FCREA   TIMESTAMP   ,                                       
    APP_FACTU   TIMESTAMP                                           
) 
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_TAPP
    
COMMENT ON COLUMN GE_TAPP.APP_APP   IS 'IDENTIFICADOR ÚNICO DE APP';
COMMENT ON COLUMN GE_TAPP.APP_NOMB  IS 'DESCRIPCIÓN DE APLICACION';
COMMENT ON COLUMN GE_TAPP.APP_VERS  IS 'Version actual DE APLICACION';
COMMENT ON COLUMN GE_TAPP.APP_FCREA IS 'Fecha de creacion APLICACION';
COMMENT ON COLUMN GE_TAPP.APP_FACTU IS 'Fecha de actualizacion APLICACION';
COMMENT ON TABLE  GE_TAPP  IS 'TABLA QUE ALMACENA LA INFORMACION DE APLICACION';

--
-- Name: GE_TAPP pk_GE_TCDER; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY GE_TAPP
    ADD CONSTRAINT pk_GE_TAPP PRIMARY KEY (APP_APP);