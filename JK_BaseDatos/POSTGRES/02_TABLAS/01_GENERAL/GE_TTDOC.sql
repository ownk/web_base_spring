-- Table: ge_ttdoc

CREATE TABLE ge_ttdoc
(
    tdoc_tdoc   VARCHAR(3) COLLATE pg_catalog."default" NOT NULL,
    tdoc_doc    VARCHAR(64) COLLATE pg_catalog."default" NOT NULL,
    tdoc_tper   VARCHAR(3) COLLATE pg_catalog."default" NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_ttdoc
    
    
COMMENT ON TABLE ge_ttdoc
    IS 'TABLA QUE ALMACENA LOS DIFERENTES TIPOS DE DOCUMENTOS CON QUE SE PUEDE REGISTRAR UN USUARIO';

COMMENT ON COLUMN ge_ttdoc.tdoc_tdoc
    IS 'IDENTIFICADOR DEL TIPO DE DOCUMENTO DE IDENTIFICACIĂ“N INSCRITO';

COMMENT ON COLUMN ge_ttdoc.tdoc_doc
    IS 'NOMBRE DEL TIPO DE DOCUMENTO INSCRITO: TARJETA DE IDENTIDAD, CEDULA DE CIUDADANĂŤA, CEDULA DE EXTRANJERĂŤA';
    
--
-- Name: ge_ttdoc pk_ge_ttdoc; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY ge_ttdoc
    ADD CONSTRAINT pk_ge_ttdoc PRIMARY KEY (tdoc_tdoc);