-- ***************************************************
-- **          PROYECTO WEB BASE                    **
-- **          REV:26\01\2019                       ** 
-- ***************************************************

\i 01_SECUENCIAS/01_GENERAL/INSTALL.sql
\i 01_SECUENCIAS/02_AUTENTICACION/INSTALL.sql
\i 01_SECUENCIAS/03_ACCOUNT/INSTALL.sql
\i 01_SECUENCIAS/04_GAMES/INSTALL.sql
\i 01_SECUENCIAS/05_CLIENT/INSTALL.sql
\i 01_SECUENCIAS/06_INTEGRATION/INSTALL.sql
