grant all privileges on all tables in schema juegohacker_schema to juegohacker_us;
grant all privileges on all sequences in schema juegohacker_schema to juegohacker_us;
grant all privileges on all functions in schema juegohacker_schema to juegohacker_us; 

ALTER ROLE juegohacker_us SUPERUSER;	

