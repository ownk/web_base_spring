--Modulodo de USER
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('100','login/login_username','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('100','login/login_password','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('100','logout','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('101','public/user/forgetpassword','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('101','rest/public/user/forgetpassword/email','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('102','private/home','P');


--Modulodo de GAME
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('200','public/register/player/request','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('200','public/register/player/create','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('200','public/register/player/response','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('200','public/register/player/confirm','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('200','rest/public/register/player/nick','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('200','rest/public/register/player/email','S');



INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('201','private/player/profile/private','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('201','rest/private/player/profile/private/create','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('201','rest/private/player/profile/private/details','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('201','private/player/profile/public','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('201','rest/private/player/profile/public/details','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('201','private/player/profile/private/objectives/report','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','private/player/world/allworlds','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','rest/private/player/world/allworlds/get','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','rest/private/player/world/allworlds/quickgames','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','private/player/world/details','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','rest/private/player/world/details/get','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','private/player/game/details','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','rest/private/player/game/details/matches','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('202','private/player/game/play','S');



INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('203','private/ranking/general','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('203','private/player/profile/public','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('203','rest/private/player/profile/public/details','S');


INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('204','rest/private/game/questions','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('204','rest/private/game/worlds','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('204','rest/private/game/avatars','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('204','rest/private/game/pets','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('204','rest/private/game/ships','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('205','private/player/search','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('205','private/player/profile/public','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('205','rest/private/player/search/nick','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('206','private/client/games','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('206','rest/private/client/games/all','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('207','private/client/players','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('207','private/player/profile/public','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('208','private/client/ranking','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('208','private/player/profile/public','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('208','rest/private/player/profile/public/details','S');


INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('209','private/game','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('209','rest/private/game/all_games','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('209','private/game/details','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('209','private/world/details','S');

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('210','private/game/avatar','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('211','private/game/pet','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('212','private/game/ship','P');


--Modulo CLIENT

INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('300','public/register/client/request','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('300','public/register/client/create','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('300','public/register/client/response','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('300','public/register/client/confirm','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('300','rest/public/register/client/nick','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('300','rest/public/register/client/email','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('300','rest/public/register/client/exist','S');


INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('301','private/client/accounts','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('302','private/client/acodes','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('303','private/client/users','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('304','private/client/profile/private','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('304','private/client/profile/private/image','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('305','private/client/players/objectives/report','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('306','private/admin/user/parameters','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('306','rest/private/admin/user/parameters/crypto','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('307','private/client/players/registerfail','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('308','private/client/players/externalid','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('308','private/client/players/externalid/downloadTemplate','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('308','private/client/players/externalid/uploadFileTemp','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('308','private/client/players/externalid/processFile','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('309','private/client/games/groups/details','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('309','rest/private/client/games/groups/group','S');



--Modulo ADMIN
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('400','private/admin/client/rcodes','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('401','private/admin/client/acodes','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('401','private/admin/client/acodes/details','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('402','private/admin/app/parameters','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('403','private/admin/user','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('404','private/admin/client','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('405','private/admin/client/games/groups','P');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('405','private/admin/client/games/groups/details','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('405','rest/private/admin/client/games/groups/games','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('405','rest/private/admin/client/games/groups/create','S');
INSERT INTO AUT_TSURL (SURL_SRVC,SURL_URL,SURL_TIPO) VALUES ('405','rest/private/admin/client/games/groups/group','S');



