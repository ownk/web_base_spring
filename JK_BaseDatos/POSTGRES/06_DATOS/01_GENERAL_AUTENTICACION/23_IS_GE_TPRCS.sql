-- INSERTING into GE_TPRCS
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_qregister.registerclient', 'Api con procedimiento para registrar un cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_qregister.registerplayer', 'Api con procedimientos para crear un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getavatars', 'Api con procedimiento para obtener los avatars');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getbizplranking', 'Api con procedimiento para obtener el ranking por cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getposplbizranking', 'Api con procedimiento para obtener la posicion en la que esta el player en el ranking');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getglobalplranking', 'Api con procedimiento para obtener el ranking global');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getglobalrnkdate', 'Api con procedimiento para obtener el ranking global por fecha');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getbizrnkdate', 'Api con procedimiento para obtener el ranking de los jugadores por cliente por fecha');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getmatchscore', 'Api con procedimiento para obtener el score de un match');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getpets', 'Api con procedimiento para obtener las mascoras');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getplayerbynick', 'Api con procedimiento para obtener un player por su nick');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getships', 'Api con procedimiento para obtener las naves');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getuserplayer', 'Api con procedimiento para obtener un player por su usuario');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getworlds', 'Api con procedimiento para obtener los mundos');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getquestions', 'Api con procedimiento para obtener las preguntas y sus respuestas');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getgames', 'Api con procedimiento para obtener todos los juegos');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getplayerbyid', 'Api con procedimiento para consultar informacion de player por id');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getlrnobjsgame', 'Api con procedimiento para consultar los objetivos de aprendizaje por juego');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getworldsbygame', 'Api con procedimiento para consultar los mundos en los que se encuentra un juego');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qgame.getgamesworld', 'Api con procedimiento para consultar los juegos por mundo');


Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.assignavatar', 'Api con procedimiento para asignar un avatar a un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.assignpet', 'Api con procedimiento para asignar una mascota a un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.assignship', 'Api con procedimiento para asignar una nave a un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getgamebyid', 'Api con procedimiento para obtener un juego por id');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getgamesworld', 'Api con procedimiento para obtener los juegos por mundo');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getplayeravatar', 'Api con procedimiento para obtener el avatar del player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getplayergames', 'Api con procedimiento para obtener los juegos por jugador');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getplayermatches', 'Api con procedimiento para obtener los matchs de un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getplayerpet', 'Api con procedimiento para obtener la mascota de un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getplayership', 'Api con procedimiento para obtener la nave de un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getplayerworlds', 'Api con procedimiento para obtener los mundos de un player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getquestions', 'Api con procedimiento para obtener las preguntas');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getworldbyid', 'Api con procedimiento para obtener los mundos por id');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.updateplayertype', 'Api con procedimiento para actualizar el tipo de Jugador');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.validateregister', 'Api con procedimiento para validar el Registro del Jugador');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.registerquestns', 'Api con procedimiento para registrar las preguntas y respuestas seleccionadas por el player');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.getobjectivesplayer', 'Api con procedimiento para obtener los objetivos de aprendizaje obtenidos por jugador');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_gms_qplayer.updateinfoplayers', 'Api con procedimiento para actualizar la informacion de jugadores (validado por numero y tipo de doumento)');



Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_qactualizar.changepassword', 'Api con procedimiento para actualizar la contraseña de un usuario');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_quser.forgotpassword', 'Api con procedimiento para actualizar validar el otp del cambio de password con olvido de contraseña');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_quser.getusernick', 'Api con procedimiento para obtener los usuario por nick');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_quser.validateemail', 'Api con procedimiento para validar un email');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_quser.getuseremail', 'Api con procedimiento para obtener los usuario por email');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_quser.updateusererrorregister', 'Api con procedimiento para actualizar los estados de usuario y player a activos si en el regsitro sale algo mal');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_quser.getusererrorregister', 'Api con procedimiento para obtener los clientes inactivos por no confirmacion del correo');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_quser.deleteuser', 'Api con procedimiento para eliminar un usuario cambiando su estado a DEL');


Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.validateregister', 'Api con procedimiento para validar el registro del Cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getclient', 'Api con procedimiento para obtener un cliente por Tipo y Numero de Documento');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getclients', 'Api con procedimiento para obtener los clientes');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getaccountsclient', 'Api con procedimiento para obtener las cuentas relacionadas a un cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getacodesclient', 'Api con procedimiento para obtener los codigos de acceso relacionados a un cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getclientusers', 'Api con procedimiento para obtener los usuarios relacionados a un cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getclientplayers', 'Api con procedimiento para obtener los jugadores relacionados a un cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getclientbyuser', 'Api con procedimiento para obtener el cliente por el usuario');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.updateclientimage', 'Api con procedimiento para actualizar la imagen del cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getacodegames', 'Api con procedimiento para obtener los juegos por codigo de acceso');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.createacodes', 'Api con procedimiento para creacion de codigo de acceso');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.creatercodes', 'Api con procedimiento para creacion de codigo de registro');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.inactivateacode', 'Api con procedimiento para inactivar un codigo de acceso');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.inactivatercode', 'Api con procedimiento para inactivar un codigo de registro');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.deleteacode', 'Api con procedimiento para eliminar un codigo de acceso');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.deletercode', 'Api con procedimiento para eliminar un codigo de registro');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getclientgames', 'Api con procedimiento para obtener los juegos por cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getrcodesclient', 'Api con procedimiento para obtener los codigos de registro y cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.gettemplates', 'Api con procedimiento para obtener las plantillas del cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.createacodescant', 'Api con procedimiento para crear multiples codigos de acceso');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.creatercodescant', 'Api con procedimiento para crear multiples codigos de registro');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.createtemplategames', 'Api con procedimiento para crear una plantilla con juegos');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getreportplclient', 'Api con procedimiento para obtener la informacion para reporte de jugadores por cliente');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_cli_qclient.getuserplerrorregister', 'Api con procedimiento para obtener los jugadores de un cliente inactivos por no confirmacion del correo');


Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qpartnr.getpartnrbykeys', 'Api con procedimiento para obtener un partner por AccesKey y SiteKey');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qpartnr.getpartners', 'Api con procedimiento para obtener los partners');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qpartnr.createpartner', 'Api con procedimiento para creacion de partners');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qpartnr.inactivepartner', 'Api con procedimiento para creacion de partners');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.starsession', 'Api con procedimiento para iniciar una session');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.finalizesession', 'Api con procedimiento para finalizar una session');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qplayer.startmatch', 'Api con procedimiento para inicar un match');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qplayer.updatematch', 'Api con procedimiento para finalizar un match');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.getsession', 'Api con procedimiento para obtener la session');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.creategtoken', 'Api con procedimiento para crear gtoken');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.getgtoken', 'Api con procedimiento para obtener gtoken');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.usegtoken', 'Api con procedimiento para usar un gtoken');


Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_ge_qutils.gettdocs', 'Api con procedimiento para obtener los tipos de documento');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_ge_qutils.getcnfgs', 'Api con procedimiento para obtener los parametros generales');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_ge_qutils.createcnfg', 'Api con procedimiento para crear parametros generales');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_ge_qutils.updatecnfg', 'Api con procedimiento para actualizar parametros generales');

Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.registerlogin', 'Api con procedimiento para crear registro de login');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.getlogin', 'Api con procedimiento para consultar los registros de Login');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.registernewpassword', 'Api con procedimiento para insertar cambios de contraseña');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.registerloginok', 'Api con procedimiento para insertar en la tabla de login ok');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.getloginok', 'Api con procedimiento para consultar los registros de Login ok');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.deleteloginok', 'Api con procedimiento para eliminar los registro de Login ok de un usuario');

Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.registerloginerr', 'Api con procedimiento para insertar en la tabla de login error');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.getloginerr', 'Api con procedimiento para consultar los registros de Login Err');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.deleteloginerr', 'Api con procedimiento para eliminar los registro de Login Err de un usuario');

Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.registeripok', 'Api con procedimiento para insertar en la tabla de Ips ok');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.getipok', 'Api con procedimiento para consultar los registros de Ips ok');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.deleteipok', 'Api con procedimiento para eliminar los registro de Ips ok de un usuario');

Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.registeruaok', 'Api con procedimiento para insertar en la tabla UAOK');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.getuaok', 'Api con procedimiento para consultar los registros de la tabla UAOK');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_qlogin.deleteuaok', 'Api con procedimiento para eliminar los registro de la tabla UAOK de un usuario');

Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_quser.nicknamecompliance', 'Api con procedimiento para validar los nombres de usuario');       
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_quser.passwordcompliance', 'Api con procedimiento para validar las contraseñas de usuario'); 
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_quser.getusernickinval', 'Api con procedimiento para validar tabla de usuarios no validos');  
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_quser.getpassinval', 'Api con procedimiento para validar tabla de contraseñas no validas');               
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_aut_quser.getpasswordhistory', 'Api con procedimiento para obtener el historial de contraseñas de un usuario');               


Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_acn_qacnt.getaccountpl', 'Api con procedimiento para obtener la cuenta de un jugador por su cliente');               