-- INSERTING into GE_TCRTA
--API_AUT_QAUTENTICACION.actualizarUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.actualizarUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.actualizarUsuario','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.cambiarEstadoUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.cambiarEstadoUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.cambiarEstadoUsuario','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.cambiarPassword
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.cambiarPassword','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.cambiarPassword','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.establecerPassword
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.establecerPassword','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.establecerPassword','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.generarTokenAutenticacion
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.generarTokenAutenticacion','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.generarTokenAutenticacion','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.generarTokenRecuperacion
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.generarTokenRecuperacion','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.generarTokenRecuperacion','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.getUsuarioTokenRecuperacion
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.getUsuarioTokenRecuperacion','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.getUsuarioTokenRecuperacion','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.obtenerRolesPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.obtenerRolesPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.obtenerRolesPorUsuario','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.obtenerUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.obtenerUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.obtenerUsuario','500', 'Error no controlado', 'es_CO');
--API_AUT_QAUTENTICACION.obtenerUsuarioToken
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QAUTENTICACION.obtenerUsuarioToken','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QAUTENTICACION.obtenerUsuarioToken','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.actualizarPersona
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.actualizarPersona','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.actualizarPersona','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.asignarRolPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.asignarRolPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.asignarRolPorUsuario','500', 'Error no controlado', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_ROL_USUA_DUPL','API_AUT_QUSUARIOS.asignarRolPorUsuario','500','El usuario ya tiene asignado el rol','es_CO');
--API_AUT_QUSUARIOS.asignarRoles
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.asignarRoles','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.asignarRoles','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.crearRol
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.crearRol','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.crearRol','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.crearUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.crearUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.crearUsuario','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.eliminarRol
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.eliminarRol','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.eliminarRol','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.eliminarRolesPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.eliminarRolesPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.eliminarRolesPorUsuario','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.eliminarServicioRol
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.eliminarServicioRol','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.eliminarServicioRol','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.getAllUsuarios
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.getAllUsuarios','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.getAllUsuarios','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.getRol
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.getRol','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.getRol','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.getServiciosPorRol
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.getServiciosPorRol','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.getServiciosPorRol','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.obtenerPersonaAsociadaUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.obtenerPersonaAsociadaUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.obtenerPersonaAsociadaUsuario','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.obtenerPersona
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.obtenerPersona','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.obtenerPersona','500', 'Error no controlado', 'es_CO');
--API_AUT_QUSUARIOS.obtenerUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSUARIOS.obtenerUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSUARIOS.obtenerUsuario','500', 'Error no controlado', 'es_CO');
--API_GE_QMENU.modulosPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QMENU.modulosPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QMENU.modulosPorUsuario','500', 'Error no controlado', 'es_CO');
--API_GE_QMENU.obtenerMenuPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QMENU.obtenerMenuPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QMENU.obtenerMenuPorUsuario','500', 'Error no controlado', 'es_CO');
--API_GE_QMENU.paginaInicioPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QMENU.paginaInicioPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QMENU.paginaInicioPorUsuario','500', 'Error no controlado', 'es_CO');
--API_GE_QMENU.serviciosTpListadoPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QMENU.serviciosTpListadoPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QMENU.serviciosTpListadoPorUsuario','500', 'Error no controlado', 'es_CO');
--API_GE_QMENU.serviciosUsuarioPorUrl
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QMENU.serviciosUsuarioPorUrl','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QMENU.serviciosUsuarioPorUrl','500', 'Error no controlado', 'es_CO');
--API_GE_QMENU.titulosMenuPorUsuario
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QMENU.titulosMenuPorUsuario','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QMENU.titulosMenuPorUsuario','500', 'Error no controlado', 'es_CO');
--API_GE_QPARAMETROS.obtenerCiudDepPais
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QPARAMETROS.obtenerCiudDepPais','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QPARAMETROS.obtenerCiudDepPais','500', 'Error no controlado', 'es_CO');
--API_GE_QPARAMETROS.obtenerListaCiudades
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QPARAMETROS.obtenerListaCiudades','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QPARAMETROS.obtenerListaCiudades','500', 'Error no controlado', 'es_CO');
--API_GE_QPARAMETROS.obtenerListaDeptos
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QPARAMETROS.obtenerListaDeptos','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QPARAMETROS.obtenerListaDeptos','500', 'Error no controlado', 'es_CO');
--API_GE_QPARAMETROS.obtenerListaPaises
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QPARAMETROS.obtenerListaPaises','200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QPARAMETROS.obtenerListaPaises','500', 'Error no controlado', 'es_CO');
--API_GE_QPARAMETROS.obtenerListaTipoDocumentos
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_GE_QPARAMETROS.obtenerListaTipoDocumentos', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_GE_QPARAMETROS.obtenerListaTipoDocumentos','500', 'Error no controlado', 'es_CO');


