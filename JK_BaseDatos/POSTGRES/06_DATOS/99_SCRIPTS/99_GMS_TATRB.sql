/*
nombre_atributo: Sera el nombre del nuevo atributo
icon:            Sera un icono para el atributo
color:           Sera el color qeu se le dara a el atributo (success=verde,info=azul,warning=amarillo,danger=rojo)
*/

Insert into GMS_TATRB
(
ATRB_ATRB,
ATRB_DESCRI,
ATRB_NAME,
ATRB_ICON,
ATRB_COLOR
)
values (
'nombre_atributo',
'GMS_TATRB.ATRB_DESCRI.nombre_atributo',
'GMS_TATRB.ATRB_NAME.nombre_atributo',
'icon',
'color'
);