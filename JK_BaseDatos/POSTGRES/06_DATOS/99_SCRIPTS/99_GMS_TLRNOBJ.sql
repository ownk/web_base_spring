/*
Identificador: Sera un numero a partir del 51
Icono: Sera el icono que tendra el objetivo
Color: Sera el codigo de color en sistema hexadecimal
*/

Insert into GMS_TLRNOBJ
(
LRNOBJ_LRNOBJ,
LRNOBJ_NAME,
LRNOBJ_DESCRI,
LRNOBJ_ICON,
LRNOBJ_COLOR
)
values (
'Identificador',
'GMS_TLRNOBJ.LRNOBJ_NAME.Identificador',
'GMS_TLRNOBJ.LRNOBJ_DESCRI.Identificador',
'Icono',
'Color'
);