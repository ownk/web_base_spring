/*
Identificador: Nombre de la mascota, o en su defecto, un numero desde el 9
Identificador_Personaje: Es el identificador que se ha creado en el script 99_GMS_TCHRT para este personaje
Imagen_pet: imagen de la mascota
*/

Insert into GMS_TPET
(
PET_PET,
PET_NAME,
PET_DESCRI,
PET_SKIN_URL,
PET_CHRT
)
values (
'Identificador',
'GMS_TPET.PET_NAME.Identificador',
'GMS_TPET.PET_DESCRI.Identificador',
'/app_resources/public/commons/img/pets/Imagen_pet',
'Identificador_Personaje'
);