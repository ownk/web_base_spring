-- INSERTING into GMS_TGMWL

-- =============================================
-- PUBLIC WORLD
-- =============================================

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'001',
'0',
1
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'002',
'0',
2
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'003',
'0',
3
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'004',
'0',
4
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'005',
'0',
5
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'006',
'0',
6
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'007',
'0',
7
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'008',
'0',
8
);

-- =============================================
-- FIRST WORLD
-- =============================================

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'101',
'1',
1
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'102',
'1',
2
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'103',
'1',
3
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'104',
'1',
4
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'201',
'2',
1
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'202',
'2',
2
);


Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'203',
'2',
3
);



Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'204',
'2',
4
);



Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'301',
'3',
1
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'302',
'3',
2
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'303',
'3',
3
);

Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'304',
'3',
4
);


Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'401',
'4',
1
);


Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'402',
'4',
2
);


Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'403',
'4',
3
);


Insert into GMS_TGMWL
(
GMWL_GAME,
GMWL_WORLD,
GMWL_PRIORI
)values(
'404',
'4',
4
);

