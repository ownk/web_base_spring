-- INSERTING into GMS_TAVATAR
Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'analista',
'GMS_TAVATAR.AVATAR_NAME.analista',
'GMS_TAVATAR.AVATAR_DESCRI.analista',
'/app_resources/public/commons/img/avatars/analista.png',
'1'
);


Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'escritora',
'GMS_TAVATAR.AVATAR_NAME.escritora',
'GMS_TAVATAR.AVATAR_DESCRI.escritora',
'/app_resources/public/commons/img/avatars/escritora.png',
'2'
);


Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'guru',
'GMS_TAVATAR.AVATAR_NAME.guru',
'GMS_TAVATAR.AVATAR_DESCRI.guru',
'/app_resources/public/commons/img/avatars/guru.png',
'3'
);


Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'ingeniera_social',
'GMS_TAVATAR.AVATAR_NAME.ingeniera_social',
'GMS_TAVATAR.AVATAR_DESCRI.ingeniera_social',
'/app_resources/public/commons/img/avatars/ingeniera_social.png',
'4'
);


Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'maestro_robots',
'GMS_TAVATAR.AVATAR_NAME.maestro_robots',
'GMS_TAVATAR.AVATAR_DESCRI.maestro_robots',
'/app_resources/public/commons/img/avatars/maestro_robots.png',
'5'
);


Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'maestro_trafico',
'GMS_TAVATAR.AVATAR_NAME.maestro_trafico',
'GMS_TAVATAR.AVATAR_DESCRI.maestro_trafico',
'/app_resources/public/commons/img/avatars/maestro_trafico.png',
'6'
);


Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'maestro_inalambrico',
'GMS_TAVATAR.AVATAR_NAME.maestro_inalambrico',
'GMS_TAVATAR.AVATAR_DESCRI.maestro_inalambrico',
'/app_resources/public/commons/img/avatars/maestro_inalambrico.png',
'7'
);


Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'ninja_forense',
'GMS_TAVATAR.AVATAR_NAME.ninja_forense',
'GMS_TAVATAR.AVATAR_DESCRI.ninja_forense',
'/app_resources/public/commons/img/avatars/ninja_forense.png',
'8'
);

