-- INSERTING into GMS_TCHRT(Avatars)
Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'1',
'Personaje analista',
'Avatar'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'2',
'Personaje escritora',
'Avatar'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'3',
'Personaje guru',
'Avatar'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'4',
'Personaje ingeniera social',
'Avatar'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'5',
'Personaje maestro robots',
'Avatar'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'6',
'Personaje maestro trafico',
'Avatar'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'7',
'Personaje maestro inalambrico',
'Avatar'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'8',
'Personaje ninja forense',
'Avatar'
);

-- INSERTING into GMS_TCHRT(Ship)
Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'9',
'Personaje nave 1',
'Ship'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'10',
'Personaje nave 2',
'Ship'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'11',
'Personaje nave 3',
'Ship'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'12',
'Personaje nave 4',
'Ship'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'13',
'Personaje nave 5',
'Ship'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'14',
'Personaje nave 6',
'Ship'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'15',
'Personaje nave 7',
'Ship'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'16',
'Personaje nave 8',
'Avatar'
);

-- INSERTING into GMS_TCHRT(Pets)
Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'17',
'Personaje mascota 1',
'Pet'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'18',
'Personaje mascota 2',
'Pet'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'19',
'Personaje mascota 3',
'Pet'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'20',
'Personaje mascota 4',
'Pet'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'21',
'Personaje mascota 5',
'Pet'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'22',
'Personaje mascota 6',
'Pet'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'23',
'Personaje mascota 7',
'Pet'
);

Insert into GMS_TCHRT
(
CHRT_CHRT,
CHRT_DESCRI,
CHRT_TYPE
)
values (
'24',
'Personaje mascota 8',
'Pet'
);


