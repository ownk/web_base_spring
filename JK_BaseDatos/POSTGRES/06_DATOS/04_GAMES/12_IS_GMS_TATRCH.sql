-- INSERTING into GMS_TATRCH(Avatars)
Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'1',
'life',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'1',
'energy',
32
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'1',
'knowledge',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'1',
'skill',
23
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'1',
'ability',
21
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'2',
'life',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'2',
'energy',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'2',
'knowledge',
23
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'2',
'skill',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'2',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'3',
'life',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'3',
'energy',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'3',
'knowledge',
21
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'3',
'skill',
29
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'3',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'4',
'life',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'4',
'energy',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'4',
'knowledge',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'4',
'skill',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'4',
'ability',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'5',
'life',
32
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'5',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'5',
'knowledge',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'5',
'skill',
23
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'5',
'ability',
21
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'6',
'life',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'6',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'6',
'knowledge',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'6',
'skill',
27
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'6',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'7',
'life',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'7',
'energy',
32
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'7',
'knowledge',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'7',
'skill',
21
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'7',
'ability',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'8',
'life',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'8',
'energy',
33
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'8',
'knowledge',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'8',
'skill',
27
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'8',
'ability',
20
);

-- INSERTING into GMS_TATRCH(Ships)
Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'9',
'life',
31
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'9',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'9',
'knowledge',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'9',
'skill',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'9',
'ability',
21
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'10',
'life',
23
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'10',
'energy',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'10',
'knowledge',
22
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'10',
'skill',
21
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'10',
'ability',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'11',
'life',
31
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'11',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'11',
'knowledge',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'11',
'skill',
22
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'11',
'ability',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'12',
'life',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'12',
'energy',
33
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'12',
'knowledge',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'12',
'skill',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'12',
'ability',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'13',
'life',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'13',
'energy',
33
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'13',
'knowledge',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'13',
'skill',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'13',
'ability',
29
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'14',
'life',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'14',
'energy',
33
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'14',
'knowledge',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'14',
'skill',
23
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'14',
'ability',
21
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'15',
'life',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'15',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'15',
'knowledge',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'15',
'skill',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'15',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'16',
'life',
29
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'16',
'energy',
29
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'16',
'knowledge',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'16',
'skill',
27
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'16',
'ability',
20
);

-- INSERTING into GMS_TATRCH(Pets)
Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'17',
'life',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'17',
'energy',
33
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'17',
'knowledge',
29
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'17',
'skill',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'17',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'18',
'life',
29
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'18',
'energy',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'18',
'knowledge',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'18',
'skill',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'18',
'ability',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'19',
'life',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'19',
'energy',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'19',
'knowledge',
22
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'19',
'skill',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'19',
'ability',
31
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'20',
'life',
31
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'20',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'20',
'knowledge',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'20',
'skill',
27
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'20',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'21',
'life',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'21',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'21',
'knowledge',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'21',
'skill',
28
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'21',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'22',
'life',
25
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'22',
'energy',
27
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'22',
'knowledge',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'22',
'skill',
27
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'22',
'ability',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'23',
'life',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'23',
'energy',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'23',
'knowledge',
24
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'23',
'skill',
26
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'23',
'ability',
28
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'24',
'life',
20
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'24',
'energy',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'24',
'knowledge',
33
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'24',
'skill',
30
);

Insert into GMS_TATRCH
(
ATRCH_CHRT,
ATRCH_ATRB,
ATRCH_INIT_VALUE
)
values (
'24',
'ability',
29
);

