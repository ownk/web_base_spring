-- =============================================
-- PUBLIC GAMES
-- =============================================
Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'001',
'GMS_TGAME.GAME_NAME.001',
'GMS_TGAME.GAME_DESCRI.001',
'/app_resources/public/commons/img/games/001.jpg',
'PUB',
'https://www.pbs.org/wgbh/nova/labs/lab/cyber/research#/newuser',
'ACT',
'YES',
'NO',
'12DCF7' 
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'002',
'GMS_TGAME.GAME_NAME.002',
'GMS_TGAME.GAME_DESCRI.002',
'/app_resources/public/commons/img/games/002.jpg',
'PUB',
'https://phishingquiz.withgoogle.com/',
'ACT',
'YES',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'003',
'GMS_TGAME.GAME_NAME.003',
'GMS_TGAME.GAME_DESCRI.003',
'/app_resources/public/commons/img/games/003.jpg',
'PUB',
'https://howsecureismypassword.net',
'ACT',
'NO',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'004',
'GMS_TGAME.GAME_NAME.004',
'GMS_TGAME.GAME_DESCRI.004',
'/app_resources/public/commons/img/games/004.jpg',
'PUB',
'https://beinternetawesome.withgoogle.com/es-419_all/interland',
'ACT',
'NO',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'005',
'GMS_TGAME.GAME_NAME.005',
'GMS_TGAME.GAME_DESCRI.005',
'/app_resources/public/commons/img/games/005.jpg',
'PUB',
'https://cyberscouts.osi.es/',
'ACT',
'NO',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'006',
'GMS_TGAME.GAME_NAME.006',
'GMS_TGAME.GAME_DESCRI.006',
'/app_resources/public/commons/img/games/006.jpg',
'PUB',
'https://es.khanacademy.org/computing/computer-science/internet-intro#internet-works-intro',
'ACT',
'NO',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'007',
'GMS_TGAME.GAME_NAME.007',
'GMS_TGAME.GAME_DESCRI.007',
'/app_resources/public/commons/img/games/007.jpg',
'PUB',
'https://cybersecuritymonth.eu/references/quiz-demonstration/welcome-to-the-network-and-information-security-quiz',
'ACT',
'NO',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'008',
'GMS_TGAME.GAME_NAME.008',
'GMS_TGAME.GAME_DESCRI.008',
'/app_resources/public/commons/img/games/008.jpg',
'PUB',
'http://factitious.augamestudio.com/#/',
'ACT',
'YES',
'NO',
'12DCF7'
);

-- =============================================
-- FIRST WORLD
-- =============================================

--HackBox
Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_TRAILER_URL,
GAME_COLOR
)values(
'101',
'GMS_TGAME.GAME_NAME.101',
'GMS_TGAME.GAME_DESCRI.101',
'/app_resources/public/commons/img/games/101.jpg',
'APP',
'https://agar3s.github.io/jh-escape-room01/',
'ACT',
'YES',
'YES',
'https://www.youtube.com/embed/T63IaYzZe7Y?feature=oembed',
'12DCF7'
);


--Ransomware
Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_TRAILER_URL,
GAME_COLOR
)values(
'102',
'GMS_TGAME.GAME_NAME.102',
'GMS_TGAME.GAME_DESCRI.102',
'/app_resources/public/commons/img/games/102.jpg',
'APP',
'https://mapedorr.github.io/hg-ransomwaredefense/',
'ACT',
'YES',
'YES',
'https://www.youtube.com/embed/izV-BDd6eyc',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'103',
'GMS_TGAME.GAME_NAME.103',
'GMS_TGAME.GAME_DESCRI.103',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);



Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'104',
'GMS_TGAME.GAME_NAME.104',
'GMS_TGAME.GAME_DESCRI.104',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);


Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'201',
'GMS_TGAME.GAME_NAME.201',
'GMS_TGAME.GAME_DESCRI.201',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);



Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'202',
'GMS_TGAME.GAME_NAME.202',
'GMS_TGAME.GAME_DESCRI.202',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);



Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'203',
'GMS_TGAME.GAME_NAME.203',
'GMS_TGAME.GAME_DESCRI.203',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);



Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'204',
'GMS_TGAME.GAME_NAME.204',
'GMS_TGAME.GAME_DESCRI.204',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);


Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'301',
'GMS_TGAME.GAME_NAME.301',
'GMS_TGAME.GAME_DESCRI.301',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'302',
'GMS_TGAME.GAME_NAME.302',
'GMS_TGAME.GAME_DESCRI.302',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);


Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'303',
'GMS_TGAME.GAME_NAME.303',
'GMS_TGAME.GAME_DESCRI.303',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);

Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'304',
'GMS_TGAME.GAME_NAME.304',
'GMS_TGAME.GAME_DESCRI.304',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);


Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'401',
'GMS_TGAME.GAME_NAME.401',
'GMS_TGAME.GAME_DESCRI.401',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);


Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'402',
'GMS_TGAME.GAME_NAME.402',
'GMS_TGAME.GAME_DESCRI.402',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);


Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'403',
'GMS_TGAME.GAME_NAME.403',
'GMS_TGAME.GAME_DESCRI.403',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);


Insert into GMS_TGAME
(
GAME_GAME,
GAME_NAME,
GAME_DESCRI,
GAME_IMAGE,
GAME_TPGAME,
GAME_URL,
GAME_STATE,
GAME_IMBIBE,
GAME_TRAILER,
GAME_COLOR
)values(
'404',
'GMS_TGAME.GAME_NAME.404',
'GMS_TGAME.GAME_DESCRI.404',
'/app_resources/public/commons/img/games/default.jpg',
'PUB',
'https://juegohacker.com',
'INA',
'NO',
'NO',
'12DCF7'
);