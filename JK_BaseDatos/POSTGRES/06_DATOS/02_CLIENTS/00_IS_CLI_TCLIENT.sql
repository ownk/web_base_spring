-- INSERTING into CLI_TCLIENT
Insert into CLI_TCLIENT 
(
CLIENT_CLIENT,
CLIENT_TPIDENT,
CLIENT_NUMIDENT,
CLIENT_NAME,
CLIENT_STATE,
CLIENT_DATECR,
CLIENT_HASH_TPIDENT,
CLIENT_HASH_NUMIDENT,
CLIENT_IMAGE
)
values (
'0',
pgp_sym_encrypt('NJ','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('1234567890','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('OWNK','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
'ACT',
'2019-10-17',
md5(upper('NJ')),
md5('1234567890'),
'/app_resources/private/img/clients/logo_client.png'
);


Insert into CLI_TCLIENT 
(
CLIENT_CLIENT,
CLIENT_TPIDENT,
CLIENT_NUMIDENT,
CLIENT_NAME,
CLIENT_STATE,
CLIENT_DATECR,
CLIENT_HASH_TPIDENT,
CLIENT_HASH_NUMIDENT,
CLIENT_IMAGE
)
values (
'1',
pgp_sym_encrypt('NJ','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('123456789','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('Hacker Game','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
'ACT',
'2019-10-17',
md5(upper('NJ')),
md5('123456789'),
'/app_resources/private/img/clients/logo_client.png'
);
