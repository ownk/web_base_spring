-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             30/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getplayerpet"
(
	p_prcs              text,
	p_player_player     text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tpet       	TYPE_TT_GM_PET,
    OUT p_tatrpl       	TYPE_TT_GM_ATRPL
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
		
	c_petpl cursor FOR
	SELECT PETPL_PET 	
	FROM GMS_TPETPL
	WHERE p_player_player=PETPL_PLAYER AND PETPL_STATE='ACT';

	c_petskin cursor FOR
	SELECT PETPL_SKIN_URL
	FROM GMS_TPETPL
	WHERE p_player_player=PETPL_PLAYER AND PETPL_STATE='ACT';

    c_pets CURSOR (vc_petpl VARCHAR(50)) FOR
    SELECT
        PET_PET,
        PET_NAME,
		PET_DESCRI,
        PET_SKIN_URL,
		PET_CHRT
    FROM  GMS_TPET
	WHERE PET_PET = vc_petpl;
        
	c_atrpl_chrt CURSOR(vc_atrpl_ch VARCHAR(50)) FOR
    SELECT
        ATRPL_PLAYER,
		ATRPL_CHRT,
		ATRPL_ATRB
        FROM GMS_TATRPL 
		WHERE ATRPL_PLAYER=p_player_player AND ATRPL_CHRT=vc_atrpl_ch;
	
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_pet TYPE_TO_GM_PET;
    tt_gm_pet TYPE_TT_GM_PET;

	to_gm_atrpl TYPE_TO_GM_ATRPL;
    tt_gm_atrpl TYPE_TT_GM_ATRPL;
	
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	v_petpl VARCHAR(50);
	v_petskin TEXT;

BEGIN
    
	OPEN c_petpl;
	FETCH c_petpl INTO v_petpl;
	CLOSE c_petpl;
	
  
	IF (v_petpl IS NOT NULL) THEN
	
		OPEN c_petskin;
		FETCH c_petskin INTO v_petskin;
		CLOSE c_petskin;
		
		FOR i IN c_pets (v_petpl)
		LOOP

			to_gm_pet=gms_qgame$getpet(i.PET_PET);
			to_gm_pet.PET_SKIN_URL=v_petskin;
			tt_gm_pet[COALESCE(array_length(tt_gm_pet, 1), 0)] := to_gm_pet;
			
			--Obtener los atributos del personaje
			FOR j IN c_atrpl_chrt(i.PET_CHRT)
			LOOP
				to_gm_atrpl=gms_qgame$getatrpl(j.ATRPL_PLAYER,j.ATRPL_CHRT,j.ATRPL_ATRB);
				tt_gm_atrpl[COALESCE(array_length(tt_gm_atrpl, 1), 0)] := to_gm_atrpl;
			END LOOP;

		END LOOP;
		p_tpet := tt_gm_pet;
		p_tatrpl := tt_gm_atrpl;

	END IF;
	
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getplayerpet"(text,text)
    
