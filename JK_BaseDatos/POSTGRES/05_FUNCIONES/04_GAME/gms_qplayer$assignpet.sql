-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$assignpet"
(
	p_prcs 				TEXT,
	p_player_player		TEXT,
	p_pet_pet			TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


	c_petpl cursor FOR
	SELECT COUNT (*) 	
	FROM GMS_TPETPL
	WHERE p_player_player=PETPL_PLAYER AND p_pet_pet = PETPL_PET;

	c_skin CURSOR FOR
	SELECT PET_SKIN_URL
	FROM GMS_TPET
	WHERE p_pet_pet=PET_PET;

	c_chrt CURSOR FOR
	SELECT PET_CHRT
	FROM GMS_TPET
	WHERE p_pet_pet=PET_PET;

	c_atributes CURSOR FOR
	SELECT 	ATRCH_CHRT,
			ATRCH_ATRB,
			ATRCH_INIT_VALUE
	FROM GMS_TPET,GMS_TATRCH
	WHERE 	ATRCH_CHRT = PET_CHRT and
			p_pet_pet=PET_PET;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
	v_exist_petpl 	NUMERIC;
	
	v_chrt	VARCHAR(50);
	v_skin	TEXT;
	
	v_cod_mensaje	VARCHAR(100):='OK';

BEGIN

	OPEN c_petpl;
	FETCH c_petpl INTO v_exist_petpl;
	CLOSE c_petpl;

	
	--Obtener el skin predeterminado del pet
	OPEN c_skin;
	FETCH c_skin INTO v_skin;
	CLOSE c_skin;
	
	--Inactivar los avatars del player
		UPDATE GMS_TPETPL
		SET PETPL_STATE = 'INA'
		WHERE PETPL_PLAYER=p_player_player;
	
	IF (v_exist_petpl>0)THEN
		
		--Activacion del Avatar para el Player 
		UPDATE GMS_TPETPL
		SET PETPL_STATE = 'ACT'
		WHERE 	PETPL_PET=p_pet_pet AND 
				PETPL_PLAYER=p_player_player;
		
	ELSE
	
		FOR i IN c_atributes 
		LOOP
			--Asignar los atributos del Personaje al Player
			Insert Into GMS_TATRPL(
					ATRPL_PLAYER,
					ATRPL_CHRT,
					ATRPL_ATRB,
					ATRPL_VALUE
			)VALUES(
					p_player_player,
					i.ATRCH_CHRT,
					i.ATRCH_ATRB,
					i.ATRCH_INIT_VALUE
			);
		END LOOP;
		--Relacionar el Pet con el Player
		Insert Into GMS_TPETPL(
				PETPL_PET,
				PETPL_PLAYER,
				PETPL_STATE,
				PETPL_SKIN_URL
		)VALUES(
				p_pet_pet,
				p_player_player,
				'ACT',
				v_skin
		);
		
	END IF;
	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$assignpet"(text, text, text)
    

