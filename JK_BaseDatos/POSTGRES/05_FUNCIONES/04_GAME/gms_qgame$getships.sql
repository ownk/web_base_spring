-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             28/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getships"
(
	p_prcs              text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tship	        TYPE_TT_GM_SHIP,
    OUT p_tatrch        TYPE_TT_GM_ATRCH
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_ships CURSOR FOR
    SELECT
        SHIP_SHIP,
        SHIP_NAME,
        SHIP_DESCRI,
        SHIP_SKIN_URL,
		SHIP_CHRT
        FROM GMS_TSHIP;
		
		
	c_atrch_atrch CURSOR(vc_atrch VARCHAR(50)) FOR
    SELECT
        ATRCH_CHRT,
		ATRCH_ATRB
		FROM GMS_TATRCH
		WHERE ATRCH_CHRT=vc_atrch;
     

	 
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_ship TYPE_TO_GM_SHIP;
    tt_gm_ship TYPE_TT_GM_SHIP;
	
	to_gm_atrch TYPE_TO_GM_ATRCH;
    tt_gm_atrch TYPE_TT_GM_ATRCH;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	

BEGIN    


    FOR i IN c_ships
    LOOP

        to_gm_ship=gms_qgame$getship(i.SHIP_SHIP);
        tt_gm_ship[COALESCE(array_length(tt_gm_ship, 1), 0)] := to_gm_ship;

		--Obtener los atributos por personaje
		FOR j IN c_atrch_atrch(i.SHIP_CHRT)
		LOOP
			to_gm_atrch=gms_qgame$getatrch(j.ATRCH_CHRT,j.ATRCH_ATRB);
			tt_gm_atrch[COALESCE(array_length(tt_gm_atrch, 1), 0)] := to_gm_atrch;
		END LOOP;

    END LOOP;

    p_tship := tt_gm_ship;
	p_tatrch := tt_gm_atrch;

    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getships"(text)
    
