-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             28/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getavatars"
(
	p_prcs              text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tavatar       TYPE_TT_GM_AVATAR,
    OUT p_tatrch       	TYPE_TT_GM_ATRCH
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_avatars CURSOR FOR
    SELECT
        AVATAR_AVATAR,
        AVATAR_NAME,
        AVATAR_DESCRI,
        AVATAR_SKIN_URL,
		AVATAR_CHRT
        FROM GMS_TAVATAR;
		
	c_atrch_atrch CURSOR(vc_atrch VARCHAR(50)) FOR
    SELECT
        ATRCH_CHRT,
		ATRCH_ATRB
		FROM GMS_TATRCH
		WHERE ATRCH_CHRT=vc_atrch;
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_avatar TYPE_TO_GM_AVATAR;
    tt_gm_avatar TYPE_TT_GM_AVATAR;
	
    to_gm_atrch TYPE_TO_GM_ATRCH;
    tt_gm_atrch TYPE_TT_GM_ATRCH;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	
	v_porcent NUMERIC;
	v_confg VARCHAR(50) = 'game.player.attribute.scale';
	

BEGIN
	
    FOR i IN c_avatars
    LOOP
		
		to_gm_avatar=gms_qgame$getavatar(i.AVATAR_AVATAR);
        tt_gm_avatar[COALESCE(array_length(tt_gm_avatar, 1), 0)] := to_gm_avatar;
	
		--Obtener los atributos por personaje
		FOR j IN c_atrch_atrch(i.AVATAR_CHRT)
		LOOP
			
			to_gm_atrch=gms_qgame$getatrch(j.ATRCH_CHRT,j.ATRCH_ATRB);
				
			tt_gm_atrch[COALESCE(array_length(tt_gm_atrch, 1), 0)] := to_gm_atrch;
		END LOOP;
		
    END LOOP;
    p_tavatar := tt_gm_avatar;
	p_tatrch := tt_gm_atrch;
	
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getavatars"(text)
    
