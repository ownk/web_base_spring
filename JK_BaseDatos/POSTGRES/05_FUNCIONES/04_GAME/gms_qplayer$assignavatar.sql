-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$assignavatar"
(
	p_prcs 				TEXT,
	p_player_player		TEXT,
	p_avatar_avatar		TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


	c_avpl CURSOR FOR
	SELECT COUNT (*) 
	FROM GMS_TAVPL
	WHERE p_player_player=AVPL_PLAYER AND p_avatar_avatar = AVPL_AVATAR;


	c_skin CURSOR FOR
	SELECT AVATAR_SKIN_URL
	FROM GMS_TAVATAR
	WHERE p_avatar_avatar=AVATAR_AVATAR;

	c_atributes CURSOR FOR
	SELECT 	ATRCH_CHRT,
			ATRCH_ATRB,
			ATRCH_INIT_VALUE
	FROM GMS_TAVATAR,GMS_TATRCH
	WHERE 	p_avatar_avatar=AVATAR_AVATAR and
			ATRCH_CHRT=AVATAR_CHRT;
		

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
	v_cod_mensaje	VARCHAR(100):='OK';
	
	v_exist_avpl 	NUMERIC;
	
	v_chrt	VARCHAR(50);
	v_skin	TEXT;
	

BEGIN

	OPEN c_avpl;
	FETCH c_avpl INTO v_exist_avpl;
	CLOSE c_avpl;
	
	--Obtener el skin predeterminado del avatar
	OPEN c_skin;
	FETCH c_skin INTO v_skin;
	CLOSE c_skin;

	--Inactivar los avatars del player
		UPDATE GMS_TAVPL
		SET AVPL_STATE = 'INA'
		WHERE AVPL_PLAYER=p_player_player;
	
	IF (v_exist_avpl>0)THEN
		
		--Activacion del Avatar para el Player 
		UPDATE GMS_TAVPL
		SET AVPL_STATE = 'ACT'
		WHERE 	AVPL_AVATAR=p_avatar_avatar AND 
				AVPL_PLAYER=p_player_player;
		
	ELSE
		
		FOR i IN c_atributes
		LOOP
			--Asignar los atributos del Personaje al Player
			Insert Into GMS_TATRPL(
					ATRPL_PLAYER,
					ATRPL_CHRT,
					ATRPL_ATRB,
					ATRPL_VALUE
			)VALUES(
					p_player_player,
					i.ATRCH_CHRT,
					i.ATRCH_ATRB,
					i.ATRCH_INIT_VALUE
			);
		END LOOP;
		--Relacionar el Avatar con el Player
		Insert Into GMS_TAVPL(
				AVPL_AVATAR,
				AVPL_PLAYER,
				AVPL_STATE,
				AVPL_SKIN_URL
		)VALUES(
				p_avatar_avatar,
				p_player_player,
				'ACT',
				v_skin
	);
		
	END IF;
	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$assignavatar"(text, text, text)
    

