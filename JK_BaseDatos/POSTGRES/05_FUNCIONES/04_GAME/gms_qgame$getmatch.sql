-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             18/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getmatch"
(
	p_match_match     	text
)
RETURNS TYPE_TO_GM_MATCH
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_match CURSOR FOR
     SELECT
        MATCH_MATCH,
        MATCH_PLAYER,
        MATCH_GAME,
        MATCH_START,
        MATCH_END,
		MATCH_FINAL_SCORE,
		MATCH_GAME_SCORE,
		MATCH_STATE,
		MATCH_RESULT
        FROM GMS_TMATCH
        where MATCH_MATCH = p_match_match;
		

	to_gms_match TYPE_TO_GM_MATCH;

BEGIN
	
	--Obtener el player por id
	
	FOR i IN c_match
	LOOP
        to_gms_match := ROW (i.MATCH_MATCH,i.MATCH_PLAYER,i.MATCH_GAME,i.MATCH_START,i.MATCH_END,i.MATCH_FINAL_SCORE,i.MATCH_GAME_SCORE,i.MATCH_STATE,i.MATCH_RESULT)::TYPE_TO_GM_MATCH;
	END LOOP;

	RETURN to_gms_match;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getmatch"(text)
    
