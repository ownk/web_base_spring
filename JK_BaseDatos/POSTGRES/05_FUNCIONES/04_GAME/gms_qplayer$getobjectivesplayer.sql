-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getobjectivesplayer"
(
	p_prcs              text,
	p_player_player     text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tgame         TYPE_TT_GM_GAME,
    OUT p_tlrnobj       TYPE_TT_GM_LRNOBJ,
    OUT p_tlrnogm       TYPE_TT_GM_LRNOGM

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	
    c_lrnobjs CURSOR FOR
    SELECT distinct
        LRNOGM_LRNOBJ
        FROM GMS_TMATCH,gms_tlrnogm
        WHERE MATCH_PLAYER=p_player_player AND MATCH_RESULT='WIN'
        and MATCH_GAME=LRNOGM_GAME;
    
    c_games CURSOR FOR
    SELECT distinct
        MATCH_GAME
        FROM GMS_TMATCH
        WHERE MATCH_PLAYER=p_player_player AND MATCH_RESULT='WIN';

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_game TYPE_TO_GM_GAME;
    tt_gm_game TYPE_TT_GM_GAME;
    
    to_gm_lrnobj TYPE_TO_GM_LRNOBJ;
    tt_gm_lrnobj TYPE_TT_GM_LRNOBJ;
	
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

	v_avpl VARCHAR(50);
	v_avskin TEXT;
    resultado BOOLEAN;


BEGIN
    
	FOR i IN c_lrnobjs
    LOOP
        to_gm_lrnobj=gms_qgame$getlrnobj(i.LRNOGM_LRNOBJ);
		tt_gm_lrnobj[COALESCE(array_length(tt_gm_lrnobj, 1), 0)] := to_gm_lrnobj;

    END LOOP;
    
	FOR j IN c_games
    LOOP
		to_gm_game=gms_qgame$getgame(j.MATCH_GAME);
        tt_gm_game[COALESCE(array_length(tt_gm_game, 1), 0)] := to_gm_game;
    END LOOP;
    SELECT
			*
			FROM ge_qutils$insertlogt(100,to_gm_game||' ENTRO')
			INTO resultado; 
    
    p_tgame := tt_gm_game;
    p_tlrnobj := tt_gm_lrnobj;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getobjectivesplayer"(text, text)
    
