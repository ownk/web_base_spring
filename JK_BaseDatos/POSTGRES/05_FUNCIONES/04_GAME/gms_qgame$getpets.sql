-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             28/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getpets"
(
	p_prcs              text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tpet       	TYPE_TT_GM_PET,
    OUT p_tatrch      	TYPE_TT_GM_ATRCH
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_pets CURSOR FOR
    SELECT
        PET_PET,
        PET_NAME,
		PET_DESCRI,
        PET_SKIN_URL,
		PET_CHRT
        FROM GMS_TPET;
		
	c_atrch_atrch CURSOR(vc_atrch VARCHAR(50)) FOR
    SELECT
        ATRCH_CHRT,
		ATRCH_ATRB
        FROM GMS_TATRCH
		WHERE ATRCH_CHRT=vc_atrch;
      
	   
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_pet TYPE_TO_GM_PET;
    tt_gm_pet TYPE_TT_GM_PET;

    to_gm_atrch TYPE_TO_GM_ATRCH;
    tt_gm_atrch TYPE_TT_GM_ATRCH;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	resultado boolean;

BEGIN    

    FOR i IN c_pets
    LOOP
        to_gm_pet=gms_qgame$getpet(i.PET_PET);
		tt_gm_pet[COALESCE(array_length(tt_gm_pet, 1), 0)] := to_gm_pet;
		
		
		SELECT
			*
			FROM ge_qutils$insertlogt(100,to_gm_pet||'ERROR')
			INTO resultado;
		
		

	
		--Obtener los atributos por personaje
		FOR j IN c_atrch_atrch(i.PET_CHRT)
		LOOP
			to_gm_atrch=gms_qgame$getatrch(j.ATRCH_CHRT,j.ATRCH_ATRB);
			tt_gm_atrch[COALESCE(array_length(tt_gm_atrch, 1), 0)] := to_gm_atrch;
		END LOOP;


    END LOOP;

    p_tpet := tt_gm_pet;
	p_tatrch := tt_gm_atrch;

	
	
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getpets"(text)
    
