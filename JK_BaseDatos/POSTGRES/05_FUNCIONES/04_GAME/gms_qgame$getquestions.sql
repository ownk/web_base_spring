-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             04/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getquestions"
(
	p_prcs              text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tquestn      	TYPE_TT_GM_QUESTN,
    OUT p_tanswer      	TYPE_TT_GM_ANSWER,
    OUT p_tanwqtn      	TYPE_TT_GM_ANWQTN
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_question CURSOR FOR
    SELECT
        QUESTN_QUESTN,
        QUESTN_DESCRI
        FROM GMS_TQUESTN;
        
	c_answer CURSOR FOR
	SELECT 
		ANSWER_ANSWER,
		ANSWER_DESCRI,
		ANSWER_VALUE,
		ANSWER_ACATEG
		FROM GMS_TANSWER;
		
	c_anwqtn CURSOR FOR
    SELECT
        ANWQTN_ANSWER,
        ANWQTN_QUESTN
        FROM GMS_TANWQTN;
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gms_questn TYPE_TO_GM_QUESTN;
    tt_gms_questn TYPE_TT_GM_QUESTN;
	
	to_gms_answer TYPE_TO_GM_ANSWER;
    tt_gms_answer TYPE_TT_GM_ANSWER;
	
	to_gms_anwqtn TYPE_TO_GM_ANWQTN;
    tt_gms_anwqtn TYPE_TT_GM_ANWQTN;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
	
    v_mensaje TYPE_TT_GE_MNSJ;
	
BEGIN
    
    FOR i IN c_question
    LOOP

        to_gms_questn := ROW (i.QUESTN_QUESTN,i.QUESTN_DESCRI)::TYPE_TO_GM_QUESTN;
        tt_gms_questn[COALESCE(array_length(tt_gms_questn, 1), 0)] := to_gms_questn;

    END LOOP;
	
	FOR j IN c_answer
    LOOP

        to_gms_answer := ROW (j.ANSWER_ANSWER,j.ANSWER_DESCRI,j.ANSWER_VALUE,j.ANSWER_ACATEG)::TYPE_TO_GM_ANSWER;
        tt_gms_answer[COALESCE(array_length(tt_gms_answer, 1), 0)] := to_gms_answer;

    END LOOP;
	
	FOR k IN c_anwqtn
    LOOP

        to_gms_anwqtn := ROW (k.ANWQTN_ANSWER,k.ANWQTN_QUESTN)::TYPE_TO_GM_ANWQTN;
        tt_gms_anwqtn[COALESCE(array_length(tt_gms_anwqtn, 1), 0)] := to_gms_anwqtn;

    END LOOP;

    p_tquestn := tt_gms_questn;
	p_tanswer := tt_gms_answer;
	p_tanwqtn := tt_gms_anwqtn;
	
	
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getquestions"(text)
    
