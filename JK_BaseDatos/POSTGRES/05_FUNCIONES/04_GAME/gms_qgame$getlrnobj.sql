-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getlrnobj"
(
	p_lrnobj_lrnobj     	text
)
RETURNS TYPE_TO_GM_LRNOBJ
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_lrnobj CURSOR FOR
	SELECT 
		LRNOBJ_LRNOBJ,
		LRNOBJ_NAME,
		LRNOBJ_DESCRI,
		LRNOBJ_ICON,
		LRNOBJ_COLOR,
        LRNOBJ_CLASIF
		FROM GMS_TLRNOBJ
		WHERE 	LRNOBJ_LRNOBJ=p_lrnobj_lrnobj;

	to_gms_lrnobj TYPE_TO_GM_LRNOBJ;

BEGIN
	
	--Obtener el game por id
	
	FOR j IN c_lrnobj
	LOOP
		to_gms_lrnobj := ROW (j.LRNOBJ_LRNOBJ,j.LRNOBJ_NAME,j.LRNOBJ_DESCRI,j.LRNOBJ_ICON,j.LRNOBJ_COLOR,j.LRNOBJ_CLASIF)::TYPE_TO_GM_LRNOBJ;
	END LOOP;

	RETURN to_gms_lrnobj;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getlrnobj"(text)
    
