-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             18/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getplayer"
(
	p_player_player     	text,
	p_pgcrypto_key      text
)
RETURNS TYPE_TO_PL_PLAYER
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_player CURSOR FOR
    SELECT
        PLAYER_PLAYER,
        PLAYER_USER,
        pgp_sym_decrypt(PLAYER_NAME::bytea,  p_pgcrypto_key) PLAYER_NAME, 
		PLAYER_DATECR,
		PLAYER_DATEAC,
		PLAYER_TYPE,
		PLAYER_SCORE,
		PLAYER_STATE,
		pgp_sym_decrypt(PLAYER_TPIDENT::bytea,  p_pgcrypto_key) PLAYER_TPIDENT, 
		pgp_sym_decrypt(PLAYER_NUMIDENT::bytea,  p_pgcrypto_key) PLAYER_NUMIDENT,
		pgp_sym_decrypt(PLAYER_ID_EXT::bytea,  p_pgcrypto_key) PLAYER_ID_EXT  

        FROM GMS_TPLAYER
		WHERE PLAYER_PLAYER=p_player_player;
		

	to_pl_player TYPE_TO_PL_PLAYER;

BEGIN
	
	--Obtener el player por id
	
	FOR i IN c_player
	LOOP
        to_pl_player := ROW (i.PLAYER_PLAYER,i.PLAYER_USER,i.PLAYER_NAME,i.PLAYER_DATECR,i.PLAYER_DATEAC,i.PLAYER_TYPE,i.PLAYER_SCORE,i.PLAYER_STATE,i.PLAYER_TPIDENT,i.PLAYER_NUMIDENT,i.PLAYER_ID_EXT)::TYPE_TO_PL_PLAYER;
	END LOOP;

	RETURN to_pl_player;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getplayer"(text, text)
    
