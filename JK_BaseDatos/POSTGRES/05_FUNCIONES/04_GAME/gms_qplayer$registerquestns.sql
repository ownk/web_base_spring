-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             18/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$registerquestns"
(
	p_prcs 				TEXT,
	p_player_player		TEXT,
	p_tanwqtn			TYPE_TT_GM_ANWQTN,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;



    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
	resultado BOOLEAN;
	v_cod_mensaje	VARCHAR(100):='OK';

BEGIN
	
	FOR i IN 0..COALESCE(array_length(p_tanwqtn, 1)-1, 0)
	LOOP

	
		--Asignar las preguntas y las respuestas del player
		Insert Into GMS_TQNPL(
				QNPL_PLAYER,
				QNPL_QUESTN,
				QNPL_ANSWER
		)VALUES(
				p_player_player,
				p_tanwqtn[i].ANWQTN_QUESTN,
				p_tanwqtn[i].ANWQTN_ANSWER
		);
	END LOOP;
		
	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$registerquestns"(text, text, TYPE_TT_GM_ANWQTN)
    

