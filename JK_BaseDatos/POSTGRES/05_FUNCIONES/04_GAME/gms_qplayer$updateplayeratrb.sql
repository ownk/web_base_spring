-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             28/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$updateplayeratrb"
(
	p_prcs 				TEXT,
	p_match_match		TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    c_score CURSOR FOR
	SELECT 
		SCORE_SCORE,
		SCORE_MATCH,
		SCORE_CHRT,
		SCORE_ATRB,
		SCORE_VALUE,
        MATCH_PLAYER
	FROM GMS_TSCORE,GMS_TMATCH
	WHERE   SCORE_MATCH=P_MATCH_MATCH AND
            SCORE_MATCH=MATCH_MATCH;

    
    v_atrb_valor NUMERIC;
    v_maximo_atrb NUMERIC = 100;
    
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    resultado BOOLEAN;
 

BEGIN

    
	FOR i IN c_score
	LOOP
        --Se valida si el valor del atributo a obtener no supera el maximo valor
        
        SELECT 
        SUM (ATRPL_VALUE)
        INTO v_atrb_valor
        FROM GMS_TATRPL
        WHERE ATRPL_PLAYER=i.MATCH_PLAYER AND ATRPL_ATRB=i.SCORE_ATRB;
        
        v_atrb_valor:=v_atrb_valor+i.SCORE_VALUE;

        
        SELECT
            *
            FROM ge_qutils$insertlogt(100,v_atrb_valor||' Fuera con este valor')
            INTO resultado;
        IF (v_atrb_valor<=v_maximo_atrb)THEN
             SELECT
                *
                FROM ge_qutils$insertlogt(100,v_atrb_valor||' Entro con este valor')
                INTO resultado;
        
            --Actualizar el valor de los atributos del Personaje del Player
            UPDATE GMS_TATRPL
            SET 
            ATRPL_VALUE = (ATRPL_VALUE+i.SCORE_VALUE)
            WHERE 	ATRPL_PLAYER=i.MATCH_PLAYER AND
                    ATRPL_CHRT=i.SCORE_CHRT AND
                    ATRPL_ATRB=i.SCORE_ATRB ;
            v_atrb_valor:=0;
        END IF;
	END LOOP;

	

	p_cod_rpta := 'OK';
	
	SELECT
		*
		FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
		INTO v_mensaje;
	p_mensajes := v_mensaje;


    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$updateplayeratrb"(text, text)
    

