-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             13/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getworld"
(
	p_world_world     	text
)
RETURNS TYPE_TO_GM_WORLD
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_world	CURSOR FOR
	SELECT 
		WORLD_WORLD,
		WORLD_NAME,
		WORLD_DESCRI,
		WORLD_POSITN_X,
		WORLD_POSITN_Y,
		WORLD_SIZE,
		WORLD_PICTURE,
		WORLD_STATE
		FROM GMS_TWORLD
		WHERE 	WORLD_WORLD=p_world_world;

	to_gm_world TYPE_TO_GM_WORLD;

BEGIN
	
	--Obtener el mundo por id
	
	FOR j IN c_world
	LOOP
		to_gm_world := ROW (j.WORLD_WORLD,j.WORLD_NAME,j.WORLD_DESCRI,j.WORLD_POSITN_X,j.WORLD_POSITN_Y,j.WORLD_SIZE,j.WORLD_PICTURE,j.WORLD_STATE)::TYPE_TO_GM_WORLD;
	END LOOP;

	RETURN to_gm_world;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getworld"(text)
    
