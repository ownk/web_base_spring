-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             04/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION juegohacker_us."gms_qplayer$getquestions"
(
	p_prcs              text,
	OUT p_cod_rpta      text,
	OUT p_errores       juegohacker_us.TYPE_TT_GE_ERRO,
	OUT p_mensajes      juegohacker_us.TYPE_TT_GE_MNSJ,
    OUT p_tquestn      	juegohacker_us.TYPE_TT_GM_QUESTN,
    OUT p_tanswer      	juegohacker_us.TYPE_TT_GM_ANSWER
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_question CURSOR FOR
    SELECT
        QUESTN_QUESTN,
        QUSTN_DESCRI
        FROM juegohacker_us.GMS_TQUESTN;
        
	c_answer CURSOR (vc_questn VARCHAR(50))FOR
	SELECT 
		ANSWER_ANSWER,
		ANSWER_QUESTN,
		ANSWER_DESCRI,
		ANSWER_ACATEG
		FROM juegohacker_us.GMS_TANSWER
		WHERE ANSWER_QUESTN=vc_questn;
	
    to_ge_erro juegohacker_us.TYPE_TO_GE_ERRO;
    tt_ge_erro juegohacker_us.TYPE_TT_GE_ERRO;

    to_gms_questn juegohacker_us.TYPE_TO_GM_QUESTN;
    tt_gms_questn juegohacker_us.TYPE_TT_GM_QUESTN;
	
	to_gms_answer juegohacker_us.TYPE_TO_GM_ANSWER;
    tt_gms_answer juegohacker_us.TYPE_TT_GM_ANSWER;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
	
    v_mensaje juegohacker_us.TYPE_TT_GE_MNSJ;
	
BEGIN
    
    FOR i IN c_question
    LOOP

        to_gms_questn := ROW (i.QUESTN_QUESTN,i.QUESTN_DESCRI)::juegohacker_us.TYPE_TO_GM_QUESTN;
        tt_gms_questn[COALESCE(array_length(tt_gms_questn, 1), 0)] := to_gms_questn;

    END LOOP;

    p_tquestn := tt_gms_questn;
	
	--Obtener las respuestas por pregunta
	
	FOR i IN 0..COALESCE(array_length(p_tquestn, 1)-1, 0)
	LOOP
		FOR j IN c_answer (p_tquestn[i].QUESTN_QUESTN)
		LOOP
			to_gms_game := ROW (j.GAME_GAME,j.GAME_NAME,j.GAME_DESCRI,j.GAME_IMAGE,j.GAME_TPGAME)::juegohacker_us.TYPE_TO_GM_GAME;
			tt_gms_game[COALESCE(array_length(tt_gms_game, 1), 0)] := to_gms_game;
		END LOOP;
		v_game_match:=p_tmatch[i].MATCH_GAME;
		p_tgame := tt_gms_game;

	END LOOP;
	
	
	
    SELECT
        *
        FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::juegohacker_us.TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION juegohacker_us."gms_qplayer$getquestions"(text, text)
    
