-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getlrnobjsgame"
(
	p_prcs              text,
	p_game_game         text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tlrnobj       TYPE_TT_GM_LRNOBJ
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_lrnobjs CURSOR FOR
    SELECT
        LRNOBJ_LRNOBJ
        FROM GMS_TLRNOBJ,GMS_TLRNOGM
		WHERE 	LRNOGM_LRNOBJ=LRNOBJ_LRNOBJ AND
				LRNOGM_GAME=p_game_game;
	
      
	   
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_lrnobj TYPE_TO_GM_LRNOBJ;
    tt_gm_lrnobj TYPE_TT_GM_LRNOBJ;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	resultado boolean;

BEGIN    
	
	
	
    FOR i IN c_lrnobjs
    LOOP
        to_gm_lrnobj=gms_qgame$getlrnobj(i.LRNOBJ_LRNOBJ);
		tt_gm_lrnobj[COALESCE(array_length(tt_gm_lrnobj, 1), 0)] := to_gm_lrnobj;

    END LOOP;

    p_tlrnobj := tt_gm_lrnobj;

	
	
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getlrnobjs"(text)
    
