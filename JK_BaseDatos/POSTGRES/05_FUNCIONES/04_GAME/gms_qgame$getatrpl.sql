-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             14/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getatrpl"
(
	p_player_player		text,
	p_chrt_chrt     	text,
	p_pl_atrb			text
)
RETURNS TYPE_TO_GM_ATRPL
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_atrpl CURSOR FOR
    SELECT
        ATRPL_PLAYER,
        ATRPL_CHRT,
		ATRPL_ATRB,
		ATRPL_VALUE,
		ATRB_DESCRI,
		ATRB_NAME,
		ATRB_ICON,
		ATRB_COLOR
        FROM GMS_TATRPL INNER JOIN GMS_TATRB
		ON GMS_TATRPL.ATRPL_ATRB=GMS_TATRB.ATRB_ATRB
		WHERE ATRPL_PLAYER=p_player_player AND ATRPL_CHRT=p_chrt_chrt AND ATRPL_ATRB=p_pl_atrb;
	
	

	v_confg VARCHAR(50) = 'game.player.attribute.scale';
	v_porcent NUMERIC;

	to_gm_atrpl TYPE_TO_GM_ATRPL;

BEGIN
	
	
	SELECT *
        FROM "ge_qutils$getcnfg_value"(v_confg)
        INTO v_porcent;
	
	--Obtener los atributos del personaje por id
	
	FOR j IN c_atrpl
	LOOP
		to_gm_atrpl := ROW (j.ATRPL_PLAYER,j.ATRPL_CHRT,j.ATRPL_ATRB,(j.ATRPL_VALUE*100)/v_porcent,j.ATRB_DESCRI,j.ATRB_NAME,j.ATRB_ICON,j.ATRB_COLOR)::TYPE_TO_GM_ATRPL;
	END LOOP;

	RETURN to_gm_atrpl;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getatrpl"(text, text, text)
    
