-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             14/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getatrch"
(
	p_chrt_chrt     	text,
	p_chrt_atrb			text
)
RETURNS TYPE_TO_GM_ATRCH
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_atrch CURSOR FOR
    SELECT
        ATRCH_CHRT,
        ATRCH_ATRB,
		ATRCH_INIT_VALUE,
		ATRB_DESCRI,
		ATRB_NAME,
		ATRB_ICON,
		ATRB_COLOR
        FROM GMS_TATRCH INNER JOIN GMS_TATRB
		ON GMS_TATRCH.ATRCH_ATRB=gms_tatrb.atrb_atrb
		WHERE ATRCH_CHRT=p_chrt_chrt 
		AND   ATRCH_ATRB=p_chrt_atrb;
	
	

	v_confg VARCHAR(50) = 'game.player.attribute.scale';
	v_porcent NUMERIC;

    to_gm_atrch TYPE_TO_GM_ATRCH;

BEGIN
	
	
	SELECT *
        FROM "ge_qutils$getcnfg_value"(v_confg)
        INTO v_porcent;
	
	--Obtener los atributos del personaje por id
	
	FOR j IN c_atrch
	LOOP
		to_gm_atrch := ROW (j.ATRCH_CHRT,j.ATRCH_ATRB,(j.ATRCH_INIT_VALUE*100)/v_porcent,j.ATRB_DESCRI,j.ATRB_NAME,j.ATRB_ICON,j.ATRB_COLOR)::TYPE_TO_GM_ATRCH;
	END LOOP;
	RETURN to_gm_atrch;



END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getatrch"(text,text)
    
