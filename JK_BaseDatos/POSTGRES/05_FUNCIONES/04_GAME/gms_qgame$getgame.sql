-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             13/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getgame"
(
	p_game_game     	text
)
RETURNS TYPE_TO_GM_GAME
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_game CURSOR FOR
	SELECT 
		GAME_GAME,
		GAME_NAME,
		GAME_DESCRI,
		GAME_IMAGE,
		GAME_TPGAME,
		GAME_URL,
		GAME_STATE,
        GAME_IMBIBE,
		GAME_TRAILER,
		GAME_TRAILER_URL,
		GAME_COLOR
		FROM GMS_TGAME
		WHERE 	GAME_GAME=p_game_game;

	to_gms_game TYPE_TO_GM_GAME;

BEGIN
	
	--Obtener el game por id
	
	FOR j IN c_game
	LOOP
		to_gms_game := ROW (j.GAME_GAME,j.GAME_NAME,j.GAME_DESCRI,j.GAME_IMAGE,j.GAME_TPGAME,j.GAME_URL,j.GAME_STATE,j.GAME_IMBIBE,j.GAME_TRAILER,j.GAME_TRAILER_URL,j.GAME_COLOR)::TYPE_TO_GM_GAME;
	END LOOP;

	RETURN to_gms_game;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getgame"(text)
    
