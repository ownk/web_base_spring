-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realizo            Descripcion
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             23/01/2020      ownk           Se crea funcion para obtener informacion del login
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_quser$getpassinval"
(
	p_prcs 				TEXT,
	p_pgcrypto_key      TEXT,
    OUT p_tpsnv 		type_tt_au_psnv,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 

AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_au_psnv type_to_au_psnv;
    tt_au_psnv type_tt_au_psnv;
 
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	C_PSNV CURSOR FOR
        SELECT PSNV_PSNV,
               PSNV_USER,
               pgp_sym_decrypt(PSNV_PASS::bytea,  p_pgcrypto_key) PSNV_PASS,
               PSNV_DATE,               
               PSNV_STAT
        FROM AUT_TPSNV;
            
BEGIN

		--Consulta Login
        
		FOR I IN C_PSNV
        LOOP
        
            to_au_psnv := ROW (i.PSNV_PSNV,i.PSNV_USER,i.PSNV_PASS,i.PSNV_DATE,i.PSNV_OSBV,i.PSNV_STAT)::TYPE_to_au_psnv;
            tt_au_psnv[COALESCE(array_length(tt_au_psnv, 1), 0)] := to_au_psnv;
            
        END LOOP;    
            
	p_tpsnv := tt_au_psnv;
   	
    
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

    WHEN OTHERS THEN
    
        p_cod_rpta := 'ERROR';
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;

            SELECT *
            FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
    
            p_mensajes := v_mensaje;

END;

$BODY$;

    --ALTER FUNCTION "aut_quser$getpassinval"(text, text)
    

