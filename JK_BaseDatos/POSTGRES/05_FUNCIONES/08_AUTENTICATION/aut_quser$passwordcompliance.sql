-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�         Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             27/01/2020      ownk           Se crea funcion
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_quser$passwordcompliance"
(
	p_prcs 				TEXT,
	p_user_name			TEXT,
	p_user_nick			TEXT,
	p_user_pass         TEXT,
	p_pgcrypto_key      TEXT,
	OUT p_pass_valid	TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
    to_aut_user TYPE_TO_AU_USER;
    tt_aut_user TYPE_TT_AU_USER;
    

    
    v_psnv_val NUMERIC;
    v_porcentaje NUMERIC;
    v_regex TEXT='aut.user.password.validation.regex';
    v_passch_exist NUMERIC;
    
    R_USERNICK_LOGIC RECORD;
    resultado boolean;
    
BEGIN


    SELECT *
        FROM "ge_qutils$getcnfg_value"('aut.user.password.validation.similary.porcent')
        INTO v_porcentaje;
    
    
    SELECT *
        FROM "usr_quser$getusernick"(p_prcs,p_user_nick,p_pgcrypto_key)
        INTO R_USERNICK_LOGIC;
    
    tt_aut_user:=R_USERNICK_LOGIC.P_TUSER;
    
    p_pass_valid:='OK';
    
    --Se realiza validacion para comprobar si la contrasena 
    --no contiene contrasenas inavilitadas en el sistema
    SELECT 
        COUNT(*)
        INTO STRICT v_psnv_val
        FROM AUT_TPSNV
        WHERE p_user_pass ilike '%'||pgp_sym_decrypt(PSNV_PASS::bytea, p_pgcrypto_key)||'%';
    
   
    IF (v_psnv_val>0) THEN
        p_pass_valid:='ERR_PASS_PSNV_FULL';
        v_psnv_val:=0;
    END IF;


    --Se realiza validacion para comprobar si la contrasena 
    --no es similar a contrasenas inavilitadas en el sistema
    IF (p_pass_valid='OK') THEN
    
        SELECT 
            COUNT(*)
            INTO STRICT v_psnv_val
            FROM AUT_TPSNV
            WHERE similarity(pgp_sym_decrypt(PSNV_PASS::bytea, p_pgcrypto_key),p_user_pass)>=v_porcentaje;
        
        IF(v_psnv_val>0)THEN
            p_pass_valid:='ERR_PASS_PSNV_SIMILARY';
            v_psnv_val:=0;
        END IF;    
    END IF;
    
    
    --Se realiza validacion para comprobar si la contrasena 
    --no es similar a a el nombre de usuario
    IF (p_pass_valid='OK') THEN
        
        IF ((SELECT similarity(p_user_name,p_user_pass)>=v_porcentaje)='true')THEN
            p_pass_valid:='ERR_PASS_NAME_SIMILARY';
        END IF;
        
	END IF;
    
    
    --Se realiza validacion para comprobar si la contrasena 
    --no es similar a a el nick del usuario
    IF (p_pass_valid='OK') THEN
            
        IF ((SELECT similarity(p_user_nick,p_user_pass)>=v_porcentaje)='true')THEN
            p_pass_valid:='ERR_PASS_NICK_SIMILARY';
        END IF;
        
	END IF;
    
    --Se realiza validacion para comprobar si la contrasena 
    --cumple con la expresion regular
    IF (p_pass_valid='OK') THEN
            
        IF (select "ge_qutils$validateregex"(p_user_pass,v_regex)='false')THEN           
            p_pass_valid:='ERR_PASS_REGEX_FAIL';
        END IF;
        
	END IF;
    
  
    
    
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "aut_quser$passwordcompliance"(text, text, text, text, text )
    

