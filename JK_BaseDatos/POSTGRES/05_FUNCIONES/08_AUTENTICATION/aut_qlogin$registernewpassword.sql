-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Version        GAP                Solicitud        Fecha        Realizo            Descripcion
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             23/01/2020      ownk           Se crea funcion 
-- 1001												20/04/2020		ownk		    Se ajusta funcion para genera ID
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_qlogin$registernewpassword"
(
	p_prcs 				TEXT,
	p_newpass 			type_tt_au_passch,
	p_pgcrypto_key      TEXT,
	p_pgcrypto_alg      TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	c_slogin  CURSOR FOR				
	select nextval ('aut_spass');
    
   
    
	
	v_nextval 	NUMERIC;
	v_login_login VARCHAR(50);
    
	resultado BOOLEAN;

		

BEGIN

    
	
	FOR i IN 0..COALESCE(array_length(p_newpass, 1)-1, 0)
	LOOP
	
	--OPEN c_slogin; --1001
	--FETCH c_slogin INTO v_nextval; --1001
	--CLOSE c_slogin; --1001
	
	--v_login_login := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval); --1001
	
	select cast(uuid_generate_v4() as varchar) INTO v_login_login; --1001
	
		-- Inserta información de la nueva contraseña
        
		INSERT INTO AUT_TPASSCH(
				PASSCH_PASSCH, 
				PASSCH_USER, 
				PASSCH_PASSWORD, 
				PASSCH_NEWPASS, 
				PASSCH_DATEREG,
				PASSCH_BROWSER,
				PASSCH_IP 
				
		)VALUES(
				v_login_login,
				p_newpass[i].PASSCH_USER,
				pgp_sym_encrypt(p_newpass[i].PASSCH_PASSWORD,p_pgcrypto_key,p_pgcrypto_alg),
				pgp_sym_encrypt(p_newpass[i].PASSCH_NEWPASS,p_pgcrypto_key,p_pgcrypto_alg),
				CURRENT_TIMESTAMP,
                p_newpass[i].PASSCH_BROWSER,
                pgp_sym_encrypt(p_newpass[i].PASSCH_IP, p_pgcrypto_key,p_pgcrypto_alg)
		);
		
		
	END LOOP;
	
	
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "aut_qlogin$registernewpassword"(text, type_tt_au_passch, text, text )
    

