-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realizo            Descripcion
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             23/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_quser$getusernickinval"
(
	p_prcs 				TEXT,
	p_pgcrypto_key      TEXT,
    OUT p_tusnv 		type_tt_au_usnv,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 

AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_au_usnv TYPE_to_au_usnv;
    tt_au_usnv TYPE_tt_au_usnv;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	C_USNV CURSOR FOR
        SELECT USNV_USNV,
               USNV_USER,
               USNV_DATE,
               pgp_sym_decrypt(USNV_USIN::bytea,  p_pgcrypto_key) USNV_USIN,
               USNV_OBSV,
               USNV_STAT
        FROM AUT_TUSNV;
            
BEGIN

		--Consulta Login
        
		FOR I IN C_USNV
        LOOP
        
            to_au_usnv := ROW (i.USNV_USNV,i.USNV_USER,i.USNV_DATE,i.USNV_USIN,i.USNV_OBSV,i.USNV_STAT)::TYPE_to_au_usnv;
            tt_au_usnv[COALESCE(array_length(tt_au_usnv, 1), 0)] := to_au_usnv;
            
        END LOOP;    
            
	p_tusnv := tt_au_usnv;
   	
    
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

    WHEN OTHERS THEN
    
        p_cod_rpta := 'ERROR';
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;

            SELECT *
            FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
    
            p_mensajes := v_mensaje;

END;

$BODY$;

    --ALTER FUNCTION "aut_quser$getusernickinval"(text, text)
    

