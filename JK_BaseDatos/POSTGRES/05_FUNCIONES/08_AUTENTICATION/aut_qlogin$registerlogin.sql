-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             23/01/2020      ownk           Se crea funcion para registrar login
-- 1001												20/04/2020		ownk		    Se ajusta funcion para genera ID
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_qlogin$registerlogin"
(
	p_prcs 				TEXT,
	p_tlogin 			type_tt_au_login,
	p_pgcrypto_key      TEXT,
	p_pgcrypto_alg      TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	c_slogin  CURSOR FOR				
	select nextval ('aut_slogin');
	
	v_nextval 	NUMERIC;
	v_login_login VARCHAR(50);

	resultado BOOLEAN;

		

BEGIN

    
	
	FOR i IN 0..COALESCE(array_length(p_tlogin, 1)-1, 0)
	LOOP
	
	--OPEN c_slogin; --1001
	--FETCH c_slogin INTO v_nextval; --1001
	--CLOSE c_slogin; --1001
	
	
	--v_login_login := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval);
	
	select cast(uuid_generate_v4() as varchar) INTO v_login_login; --1001
    
        SELECT
        *
        FROM ge_qutils$insertlogt(100,p_tlogin[i].LOGIN_USER||' Resultado Select')
        INTO resultado;
	
		--Crear Login
        
		Insert Into AUT_TLOGIN(
				LOGIN_LOGIN, 
				LOGIN_USER, 
				LOGIN_NICK, 
				LOGIN_PASS, 
				LOGIN_DATEREG,
				LOGIN_BROWSER,
				LOGIN_IP 
				
		)VALUES(
				v_login_login,
				p_tlogin[i].LOGIN_USER,
				pgp_sym_encrypt(p_tlogin[i].LOGIN_NICK,p_pgcrypto_key,p_pgcrypto_alg),
				pgp_sym_encrypt(p_tlogin[i].LOGIN_PASS,p_pgcrypto_key,p_pgcrypto_alg),
				CURRENT_TIMESTAMP,
                p_tlogin[i].LOGIN_BROWSER,
                pgp_sym_encrypt(p_tlogin[i].LOGIN_IP, p_pgcrypto_key,p_pgcrypto_alg)
		);
		
        
        SELECT
        *
        FROM ge_qutils$insertlogt(100,v_login_login||' Termino Insert')
        INTO resultado;
		
	END LOOP;
	
	
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "aut_qlogin$registerlogin"(text, TYPE_TT_AU_LOGIN, text, text )
    

