-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             11/02/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_qlogin$registerloginok"
(
    p_prcs              TEXT,
    p_tiflgn            type_tt_au_iflgn,
    p_pgcrypto_key      TEXT,
    p_pgcrypto_alg      TEXT,
    OUT p_cod_rpta      TEXT,
    OUT p_errores       type_tt_ge_erro,
    OUT p_mensajes      type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;
    
    


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    to_au_iflgn TYPE_TO_AU_IFLGN;
    tt_au_iflgn TYPE_TT_AU_IFLGN;
    v_lgok_lgok VARCHAR(50);

    resultado BOOLEAN;

        

BEGIN
    
    p_cod_rpta := 'OK';

	FOR i IN 0..COALESCE(array_length(p_tiflgn, 1)-1, 0)
    LOOP
        select cast(uuid_generate_v4() as varchar) INTO v_lgok_lgok;
        
        
        --Crear Un Login Ok
           
        Insert Into AUT_TLGOK(
                LGOK_LGOK, 
                LGOK_USER, 
                LGOK_BROWSER, 
                LGOK_IP,
                LGOK_SESSION
        )VALUES(
                v_lgok_lgok,
                p_tiflgn[i].IFLGN_USER,
                p_tiflgn[i].IFLGN_BROWSER,
                pgp_sym_encrypt(p_tiflgn[i].IFLGN_IP, p_pgcrypto_key,p_pgcrypto_alg),
                p_tiflgn[i].IFLGN_SESSION
        );
        
    END LOOP;
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "aut_qlogin$registerloginok"(text, type_tt_au_iflgn, text, text)
    

