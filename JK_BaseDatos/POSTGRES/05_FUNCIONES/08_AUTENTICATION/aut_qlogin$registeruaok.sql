-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             11/02/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_qlogin$registeruaok"
(
    p_prcs              TEXT,
    p_tiflgn            type_tt_au_iflgn,
    OUT p_cod_rpta      TEXT,
    OUT p_errores       type_tt_ge_erro,
    OUT p_mensajes      type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;
    
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    to_au_iflgn TYPE_TO_AU_IFLGN;
    tt_au_iflgn TYPE_TT_AU_IFLGN;
    v_UAOK_UAOK VARCHAR(50);

    resultado BOOLEAN;

        

BEGIN
    
    p_cod_rpta := 'OK';

	FOR i IN 0..COALESCE(array_length(p_tiflgn, 1)-1, 0)
    LOOP
        select cast(uuid_generate_v4() as varchar) INTO v_UAOK_UAOK;
        
        
        --Crear Un Login Ok
           
        Insert Into AUT_TUAOK(
                UAOK_UAOK, 
                UAOK_USER, 
                UAOK_DATE_LAST_INGR 
        )VALUES(
                v_UAOK_UAOK,
                p_tiflgn[i].IFLGN_USER,
                CURRENT_TIMESTAMP
        );
        
    END LOOP;
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "aut_qlogin$registeruaok"(text, type_tt_au_iflgn)
    

