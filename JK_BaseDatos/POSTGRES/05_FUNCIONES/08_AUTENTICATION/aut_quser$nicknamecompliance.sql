-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "aut_quser$nicknamecompliance"
(
	p_prcs              text,
    p_user_nick         text,
    p_pgcrypto_key      text,
    OUT p_nick_valid    text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tuser         TYPE_TT_AU_USER
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

            
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    
    
    v_usnv_val NUMERIC;
    v_porcentaje NUMERIC;
    v_regex TEXT='aut.user.username.validation.regex';    

BEGIN
    
    SELECT *
        FROM "ge_qutils$getcnfg_value"('aut.user.username.validation.similary.porcent')
        INTO v_porcentaje;
   

    
    p_nick_valid := 'OK';
    
    -- Se valida que los usuarios ingresados no sean
    -- inhabilitados en el sistema.
    
    SELECT 
        COUNT (*)
        INTO STRICT v_usnv_val
        FROM AUT_TUSNV
        WHERE p_user_nick ilike '%' || pgp_sym_decrypt(USNV_USIN::bytea, p_pgcrypto_key) ||'%';
    
    IF (v_usnv_val > 0) THEN
        p_nick_valid := 'ERR_NICK_USNV_FULL';
        v_usnv_val := 0;
    END IF;
    
    --Se realiza validacion para comprobar si el usuario 
    --no es similar a usuarios inhabilitados en el sistema
    
    IF (p_nick_valid='OK') THEN
    
        SELECT 
            COUNT(*)
            INTO STRICT v_usnv_val
            FROM AUT_TUSNV
            WHERE similarity(pgp_sym_decrypt(USNV_USIN::bytea, p_pgcrypto_key),p_user_nick)>=v_porcentaje;
        
        IF(v_usnv_val>0)THEN
            p_nick_valid:='ERR_NICK_USNV_SIMILARY';
            v_usnv_val:=0;
        END IF; 

   
      
    --Se realiza validacion para comprobar si la contrasena 
    --cumple con la expresion regular
    
    IF (p_nick_valid='OK') THEN
            
        IF (select "ge_qutils$validateregex"(p_user_nick,v_regex)='false')THEN
            p_nick_valid:='ERR_NICK_REGEX_FAIL';
        END IF;
        
       
        
	END IF;
    
    
    END IF;   
      
       
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "aut_quser$nicknamecompliance"(text, text, text)
    
