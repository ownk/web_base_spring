-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi?n        GAP                Solicitud        Fecha        Realiz?            Descripci?n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================
CREATE OR REPLACE FUNCTION "prueba$llamadodinamico"
(
    p_tabla		    TEXT,
	p_var_where		TEXT,
	p_col_where		TEXT,
    OUT p_result  	TEXT,
	OUT p_pueba		TEXT
    
    
)
RETURNS record 
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

query text;
player_player VARCHAR(50);
player_state VARCHAR(50);
resultado		BOOLEAN;
p_proced	TEXT='prueba$pruebadinamico';
BEGIN


	/*
	query= 'select player_player from '||p_tabla||' WHERE '||p_col_where||' = '''||p_var_where||'''';
	
	EXECUTE query
	INTO player_player;
	*/
	
	query = 'SELECT 	
		* 
		FROM '||p_proced||'('''||p_tabla||''','''||p_var_where||''',''';
	
	
	query = query||p_col_where||''')';
	EXECUTE query
	INTO player_player,player_state;
	p_result:=player_player;
end;

$BODY$;

--ALTER FUNCTION "prueba$llamadodinamico"(text, text, text)
    
