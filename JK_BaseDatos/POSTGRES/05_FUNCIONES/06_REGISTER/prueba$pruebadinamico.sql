-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi?n        GAP                Solicitud        Fecha        Realiz?            Descripci?n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================
CREATE OR REPLACE FUNCTION "prueba$pruebadinamico"
(
    p_tabla		    TEXT,
	p_var_where		TEXT,
	p_col_where		TEXT,
	OUT p_result	TEXT
)
RETURNS TEXT 
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

query text;
resultado BOOLEAN;
player_player VARCHAR(50);

BEGIN

	query= 'select '||p_col_where||' from '||p_tabla||' WHERE '||p_col_where||' = '''||p_var_where||'''';
	
	EXECUTE query
	INTO p_result;
SELECT
	*
	FROM ge_qutils$insertlogt(100,query||' Resultado Select')
	INTO resultado;


end;

$BODY$;

--ALTER FUNCTION "prueba$pruebadinamico"(text, text, text)
    