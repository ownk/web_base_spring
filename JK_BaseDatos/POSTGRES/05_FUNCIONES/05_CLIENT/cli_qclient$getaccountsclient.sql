-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             05/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getaccountsclient"
(
	p_prcs              text,
	p_client_client     text,
    p_pgcrypto_key		text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_taccount      TYPE_TT_AC_ACNT,
    OUT p_tplacn      	TYPE_TT_PL_PLACN,
    OUT p_tplayer      	TYPE_TT_PL_PLAYER,
    OUT p_tacac      	TYPE_TT_AC_ACAC,
	OUT p_tacode		TYPE_TT_CL_ACODE
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_account CURSOR FOR
    SELECT
        ACNT_ACNT,
        PLACN_PLAYER,
        PLACN_ACNT,
        ACAC_ACODE,
		ACAC_ACNT
        FROM ACN_TACNT,ACN_TOWNER,GMS_TPLACN,ACN_TACAC
        WHERE 	OWNER_CLIENT = P_CLIENT_CLIENT AND 
				PLACN_ACNT=ACNT_ACNT 	AND 
				ACNT_ACNT = OWNER_ACNT 	AND
				ACAC_ACNT = ACNT_ACNT;
	
	 
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_ac_acnt TYPE_TO_AC_ACNT;
    tt_ac_acnt TYPE_TT_AC_ACNT;
	
	to_pl_placn TYPE_TO_PL_PLACN;
    tt_pl_placn TYPE_TT_PL_PLACN;
	
	to_pl_player TYPE_TO_PL_PLAYER;
    tt_pl_player TYPE_TT_PL_PLAYER;

	to_ac_acac TYPE_TO_AC_ACAC;
    tt_ac_acac TYPE_TT_AC_ACAC;
	
	to_cl_acode TYPE_TO_CL_ACODE;
    tt_cl_acode TYPE_TT_CL_ACODE;
	
	v_client_client VARCHAR(50);
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
  
    FOR i IN c_account
    LOOP
		
        to_ac_acnt := acn_qacnt$getaccount(i.ACNT_ACNT);
        tt_ac_acnt[COALESCE(array_length(tt_ac_acnt, 1), 0)] := to_ac_acnt;

		to_pl_player=gms_qgame$getplayer(i.PLACN_PLAYER,p_pgcrypto_key);
        tt_pl_player[COALESCE(array_length(tt_pl_player, 1), 0)] := to_pl_player;

		to_pl_placn := ROW (i.PLACN_PLAYER,i.PLACN_ACNT)::TYPE_TO_PL_PLACN;
        tt_pl_placn[COALESCE(array_length(tt_pl_placn, 1), 0)] := to_pl_placn;
		
		to_ac_acac := ROW (i.ACAC_ACODE,i.ACAC_ACNT)::TYPE_TO_AC_ACAC;
        tt_ac_acac[COALESCE(array_length(tt_ac_acac, 1), 0)] := to_ac_acac;
		
        to_cl_acode = cli_qclient$getacode(i.ACAC_ACODE,p_pgcrypto_key);
        tt_cl_acode[COALESCE(array_length(tt_cl_acode, 1), 0)] := to_cl_acode;
		
    END LOOP;

    p_taccount := tt_ac_acnt;
    p_tplacn   := tt_pl_placn;
    p_tplayer  := tt_pl_player;
    p_tacac    := tt_ac_acac;
    p_tacode   := tt_cl_acode;
	 
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getaccountsclient"(text, text, text)
    
