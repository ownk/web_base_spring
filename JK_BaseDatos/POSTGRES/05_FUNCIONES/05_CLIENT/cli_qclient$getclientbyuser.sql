-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             05/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getclientbyuser"
(
	p_prcs              text,
	p_user_user     	text,
    p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tclient       TYPE_TT_CL_CLIENT
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_client_user CURSOR FOR
	SELECT
		CLIENT_CLIENT
		FROM CLI_TCLIENT,CLI_TUSCL
		WHERE 
		USCL_CLIENT=CLIENT_CLIENT AND
		USCL_STATE='ACT' AND
		USCL_USER=p_user_user;

	c_client_player CURSOR FOR
	SELECT
	CLIENT_CLIENT
		FROM CLI_TCLIENT,GMS_TPLACN,GMS_TPLAYER,ACN_TOWNER,ACN_TACNT
		WHERE 
		PLAYER_USER=p_user_user AND
		PLACN_PLAYER=PLAYER_PLAYER AND
		PLACN_ACNT=ACNT_ACNT AND
		ACNT_ACNT=OWNER_ACNT AND
		OWNER_CLIENT=CLIENT_CLIENT AND
		ACNT_STATE='ACT';
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_cl_client TYPE_TO_CL_CLIENT;
    tt_cl_client TYPE_TT_CL_CLIENT;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
    FOR i IN c_client_user
    LOOP
        to_cl_client = cli_qclient$getclientbyid(i.client_client,p_pgcrypto_key);
        tt_cl_client[COALESCE(array_length(tt_cl_client, 1), 0)] := to_cl_client;

    END LOOP;

	IF (to_cl_client is null)THEN
		FOR j IN c_client_player
		LOOP
			to_cl_client = cli_qclient$getclientbyid(j.client_client,p_pgcrypto_key);
			tt_cl_client[COALESCE(array_length(tt_cl_client, 1), 0)] := to_cl_client;

		END LOOP; 
	END IF;
    p_tclient := tt_cl_client;
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getclientbyuser"(text, text, text)
    
