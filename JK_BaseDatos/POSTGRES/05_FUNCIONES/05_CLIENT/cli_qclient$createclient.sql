-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/10/2019      ownk           Se crea funcion 
-- 1001												20/04/2020		ownk		    Se ajusta funcion para genera ID
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "cli_qclient$createclient"
(
	p_prcs 				TEXT,
	p_tclient 			type_tt_cl_client,
	p_user_user		    TEXT,
	p_pgcrypto_key      TEXT,
	p_pgcrypto_alg      TEXT,
	p_codigo_acceso		TEXT,
	OUT p_cod_rpta 		text,
	OUT p_client_client	text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	c_sclient  CURSOR FOR				
	select nextval ('cli_sclient');
	
	v_nextval 	NUMERIC;
	v_client_client VARCHAR(50);
	v_rcode_rcode	VARCHAR(50);

	
	c_rcode CURSOR FOR
	select RCODE_RCODE FROM CLI_TRCODE
	WHERE p_codigo_acceso =  pgp_sym_decrypt(RCODE_CODE::bytea, p_pgcrypto_key);
		

BEGIN

    
	
	FOR i IN 0..COALESCE(array_length(p_tclient, 1)-1, 0)
	LOOP
	
	--OPEN c_sclient; --1001
	--FETCH c_sclient INTO v_nextval; --1001
	--CLOSE c_sclient; --1001
	
	OPEN c_rcode;
	FETCH c_rcode INTO v_rcode_rcode;
	CLOSE c_rcode;
	
	--v_client_client := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval); --1001
	
	select cast(uuid_generate_v4() as varchar) INTO v_client_client; --1001
	
		--Crear Cliente
		Insert Into CLI_TCLIENT(
				CLIENT_CLIENT,
				CLIENT_TPIDENT,
				CLIENT_NUMIDENT,
				CLIENT_NAME,
				CLIENT_STATE,
				CLIENT_DATECR,
				CLIENT_HASH_TPIDENT,
				CLIENT_HASH_NUMIDENT
		)VALUES(
				v_client_client,
				pgp_sym_encrypt(p_tclient[i].CLIENT_TPIDENT, p_pgcrypto_key,p_pgcrypto_alg),
				pgp_sym_encrypt(p_tclient[i].CLIENT_NUMIDENT, p_pgcrypto_key,p_pgcrypto_alg),
				pgp_sym_encrypt(p_tclient[i].CLIENT_NAME, p_pgcrypto_key,p_pgcrypto_alg),
				'INA',
				CURRENT_TIMESTAMP,
				md5(upper(p_tclient[i].CLIENT_TPIDENT)),
				md5(upper(p_tclient[i].CLIENT_NUMIDENT))
		);
		
		--Asignar El Usuario al Cliente
		Insert Into CLI_TUSCL(
				USCL_USER,
				USCL_CLIENT,
				USCL_STATE
		)VALUES(
				p_user_user,
				v_client_client,
				'ACT'
		);
		--Asignar el Rol de AdminCliente al Usuario 
		INSERT INTO AUT_TUROL (
				UROL_USER,
				UROL_ROL
		)values(
				p_user_user,
				'CLIENT_ADMIN'
		);
		
		--Actualizacion del estado de codigo de regsitro a Usado y El Cliente que lo Uso
		UPDATE CLI_TRCODE
		SET RCODE_STATE='USE', 
			RCODE_CLIENT = v_client_client,
			RCODE_DATE_USED = CURRENT_TIMESTAMP
		WHERE 	RCODE_RCODE=v_rcode_rcode;
	
	p_client_client:=v_client_client;
		
	END LOOP;
	
	
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "cli_qclient$createclient"(text, TYPE_TT_CL_CLIENT, text, text, text, text )
    

