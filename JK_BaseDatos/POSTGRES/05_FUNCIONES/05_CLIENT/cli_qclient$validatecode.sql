-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             22/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION juegohacker_us."cli_qclient$validatecode"(
	p_codigo_acceso text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_cant_user NUMERIC;
	

BEGIN


    SELECT

        COUNT(*)

        INTO STRICT v_cant_user

        FROM juegohacker_us.CLI_TACODE

        WHERE ACODE_CODE = p_codigo_acceso AND ACODE_STATE='ACT';

    IF (v_cant_user > 0) THEN

        RETURN TRUE;

    ELSE

       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION juegohacker_us."cli_qclient$validatecode"(text)
    
