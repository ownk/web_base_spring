-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             18/12/2019      ownk           Se crea funcion 
-- 1001												20/04/2020		ownk		    Se ajusta funcion para genera ID
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$creatercodescant"
(
	p_prcs 				TEXT,
	p_cantidad			NUMERIC,
	p_pgcrypto_key 		TEXT,
	p_pgcrypto_alg 		TEXT,
	OUT p_cod_rpta 		TEXT,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

	
	c_srcode CURSOR FOR				
	select nextval ('cli_srcode');
	
	
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
	v_cod_mensaje		VARCHAR(100):='OK';
	v_nextval_rcode 	NUMERIC;
	v_rcode_rcode 		VARCHAR(50);
	v_rcode_code 		TEXT;
	
	F$RESULT_LOGICA RECORD;
	tiempoValidez TIMESTAMP;
	v_tiempo_vigencia NUMERIC;
	v_variable_validez TEXT = 'DAY';
	
	
	
	
BEGIN
	
	SELECT CNFG_VALOR
	INTO v_tiempo_vigencia
	FROM GE_TCNFG
	WHERE cnfg_cnfg = 'tiempo.vigencia.rcode';
	
	tiempoValidez := CURRENT_TIMESTAMP + (v_tiempo_vigencia || v_variable_validez)::INTERVAL;

	
   FOR i IN 1..COALESCE(p_cantidad)
       LOOP
            
		--OPEN c_srcode; --1001
		--FETCH c_srcode INTO v_nextval_rcode; --1001
		--CLOSE c_srcode; --1001
		
		--v_rcode_rcode := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_rcode); --1001
        
		select cast(uuid_generate_v4() as varchar) INTO v_rcode_rcode; --1001
		
		select uuid_generate_v4() INTO v_rcode_code;
  
       --Se insertan los codigos de registro
		INSERT INTO cli_trcode(
				RCODE_RCODE,
				RCODE_CODE,
				RCODE_STATE,
				RCODE_DATE_START,
				RCODE_DATE_VALIDITY
		)VALUES(
				v_rcode_rcode,
				pgp_sym_encrypt(v_rcode_code, p_pgcrypto_key,p_pgcrypto_alg),
				'ACT',
				CURRENT_TIMESTAMP,
				tiempoValidez

		);
	
		
	END LOOP;
		

	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$creatercodescant"(text,numeric,text,text)
    

