-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             19/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getclientplayers"
(
	p_prcs              text,
	p_client_client     text,
	p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tplayer       TYPE_TT_PL_PLAYER,
    OUT p_tuser       	TYPE_TT_AU_USER
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	--Cursor para traer los jugadores por cliente
	
	c_players_biz CURSOR FOR
	SELECT 	
		player_player, player_user
		FROM GMS_TPLACN, 
             ACN_TACAC, 
             CLI_TACODE, 
             CLI_TCLIENT, 
             GMS_TPLAYER
		WHERE
			PLACN_ACNT=ACAC_ACNT AND
			ACAC_ACODE=ACODE_ACODE AND
			CLIENT_CLIENT=ACODE_CLIENT AND 
			PLAYER_PLAYER=PLACN_PLAYER AND
			CLIENT_CLIENT = P_CLIENT_CLIENT;
            
            
	
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_pl_player TYPE_TO_PL_PLAYER;
    tt_pl_player TYPE_TT_PL_PLAYER;
    
    to_au_user TYPE_TO_AU_USER;
    tt_au_user TYPE_TT_AU_USER;

	v_code VARCHAR(50);

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	

BEGIN


    FOR i IN c_players_biz
    LOOP
		to_pl_player=gms_qgame$getplayer(i.player_player,p_pgcrypto_key);
        tt_pl_player[COALESCE(array_length(tt_pl_player, 1), 0)] := to_pl_player;
        
        to_au_user = usr_quser$getuser(i.player_user,p_pgcrypto_key);
		tt_au_user [COALESCE(array_length(tt_au_user, 1), 0)] := to_au_user;
		
    END LOOP;
    p_tplayer := tt_pl_player;
	
    p_tuser   := tt_au_user;
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getclientplayers"(text,text,text)
    
