-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getacode"
(
	p_acode_acode     text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_CL_ACODE
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_acode CURSOR FOR
    SELECT
        ACODE_ACODE,
        pgp_sym_decrypt(ACODE_CODE::bytea,  p_pgcrypto_key) ACODE_CODE,		
        ACODE_STATE,
        ACODE_CLIENT,
		ACODE_DATE_START,
        ACODE_DATE_VALIDITY,
		ACODE_TEMPLT
        FROM CLI_TACODE
        WHERE ACODE_ACODE=p_acode_acode;
        
  
    to_cl_tacode TYPE_TO_CL_ACODE;
	
BEGIN
  
	--Obtener el Client por Id
	
    FOR i IN c_acode
    LOOP

        to_cl_tacode := ROW (i.ACODE_ACODE,i.ACODE_CODE,i.ACODE_STATE,i.ACODE_CLIENT,i.ACODE_TEMPLT,i.ACODE_DATE_START,i.ACODE_DATE_VALIDITY)::TYPE_TO_CL_ACODE;

    END LOOP;

    return to_cl_tacode;
 


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getacode"(text, text)
    
