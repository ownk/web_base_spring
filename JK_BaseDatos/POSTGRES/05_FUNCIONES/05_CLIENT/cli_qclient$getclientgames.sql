-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             05/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getclientgames"
(
	p_prcs              text,
	p_client_client     text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tgame       	TYPE_TT_GM_GAME
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_all_games CURSOR FOR
    SELECT GAME_GAME
	FROM GMS_TGAME;
	
    c_client_games CURSOR FOR
    SELECT DISTINCT GMAC_GAME
	FROM GMS_TGMAC,CLI_TACODE
	WHERE ACODE_CLIENT = p_client_client
    AND gmac_acode = acode_acode;
    
    
    
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_game TYPE_TO_GM_GAME;
    tt_gm_game TYPE_TT_GM_GAME;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    v_state_game TEXT;

BEGIN


    
    FOR g IN c_all_games
	LOOP
    
        v_state_game = 'INA';
	
		FOR i IN c_client_games
		LOOP
		
			IF (g.GAME_GAME=i.GMAC_GAME)THEN
				v_state_game = 'ACT';
			END IF;
		
		END LOOP;	
		to_gm_game=gms_qgame$getgame(g.GAME_GAME);
		to_gm_game.GAME_STATE=v_state_game;
		tt_gm_game[COALESCE(array_length(tt_gm_game, 1), 0)] := to_gm_game;
	END LOOP;
  
   
    p_tgame := tt_gm_game;
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getclientgames"(text, text)
    
