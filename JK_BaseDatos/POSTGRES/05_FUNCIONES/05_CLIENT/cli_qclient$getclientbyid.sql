-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getclientbyid"
(
	p_client_client     text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_CL_CLIENT
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_client CURSOR FOR
    SELECT
        CLIENT_CLIENT,
        pgp_sym_decrypt(CLIENT_TPIDENT::bytea,  p_pgcrypto_key) CLIENT_TPIDENT, 
        pgp_sym_decrypt(CLIENT_NUMIDENT::bytea,  p_pgcrypto_key) CLIENT_NUMIDENT, 
        pgp_sym_decrypt(CLIENT_NAME::bytea,  p_pgcrypto_key) CLIENT_NAME,
        CLIENT_STATE,
        CLIENT_IMAGE,
		CLIENT_DATECR,
        CASE CLIENT_STATE
            WHEN 'INA' THEN 'Inactivo'
            WHEN 'ACT' THEN 'Activo'
            WHEN 'PPA' THEN 'Pendiente'
            WHEN 'PRE' THEN 'Precargado'
            ELSE 'Other'
        END AS CLIENT_STATE_DESC
        FROM CLI_TCLIENT
        WHERE CLIENT_CLIENT=p_client_client;
        
  
    to_cl_client TYPE_TO_CL_CLIENT;
	
BEGIN
  
	--Obtener el Client por Id
	
    FOR i IN c_client
    LOOP

        to_cl_client := ROW (i.CLIENT_CLIENT,i.CLIENT_TPIDENT,i.CLIENT_NUMIDENT,i.CLIENT_NAME,i.CLIENT_STATE,i.CLIENT_IMAGE,i.CLIENT_DATECR, i.CLIENT_STATE_DESC)::TYPE_TO_CL_CLIENT;

    END LOOP;

    return to_cl_client;
 


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getclientbyid"(text, text)
    
