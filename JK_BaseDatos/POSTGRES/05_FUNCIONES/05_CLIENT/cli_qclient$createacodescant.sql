-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             18/12/2019      ownk           Se crea funcion 
-- 1001												20/04/2020		ownk		   Se ajusta funcion para genera ID
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$createacodescant"
(
    p_prcs                 TEXT,
    p_cantidad            NUMERIC,
    p_pgcrypto_key         TEXT,
    p_pgcrypto_alg         TEXT,
    p_client_client        TEXT,
    p_ttemplt             TYPE_TT_CL_TEMPLT,
    OUT p_cod_rpta         TEXT,
    OUT p_errores         type_tt_ge_erro,
    OUT p_mensajes         type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    
    c_sacode CURSOR FOR                
    select nextval ('cli_sacode');
    
    c_tempgm CURSOR (vc_templt VARCHAR(50))FOR
    Select TEMPGM_GAME 
    FROM CLI_TTEMPGM
    WHERE TEMPGM_TEMPLT=vc_templt;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
    v_cod_mensaje        VARCHAR(100):='OK';
    v_nextval_acode     NUMERIC;
    v_acode_acode         VARCHAR(50);
    v_tempg_game        VARCHAR(50);
    v_acode_code        TEXT;
    resultado BOOLEAN;
    F$RESULT_LOGICA RECORD;
	
	tiempoValidez TIMESTAMP;
    v_tiempo_vigencia NUMERIC;
	v_variable_validez TEXT = 'MONTH';
    
    
BEGIN
    
    SELECT CNFG_VALOR
    INTO v_tiempo_vigencia
    FROM GE_TCNFG
    WHERE cnfg_cnfg = 'tiempo.vigencia.acode';
    
    tiempoValidez := CURRENT_TIMESTAMP + (v_tiempo_vigencia || v_variable_validez)::INTERVAL;
    
        
       
	FOR i IN 1..COALESCE(p_cantidad)
	LOOP
      
		--OPEN c_sacode;
		--FETCH c_sacode INTO v_nextval_acode;
		--CLOSE c_sacode; --1001
		
		--v_acode_acode := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_acode); --1001     
		
		select cast(uuid_generate_v4() as varchar) INTO v_acode_acode; --1001
		
		select uuid_generate_v4() INTO v_acode_code;
		  
		   --Se insertan los codigos de acceso
		INSERT INTO CLI_TACODE(
				ACODE_ACODE,
				ACODE_CODE,
				ACODE_STATE,
				ACODE_CLIENT,
                ACODE_TEMPLT,
				ACODE_DATE_START,
				ACODE_DATE_VALIDITY
		)VALUES(
				v_acode_acode,
				pgp_sym_encrypt(v_acode_code, p_pgcrypto_key,p_pgcrypto_alg),
				'ACT',
				p_client_client,
                p_ttemplt[0].TEMPLT_TEMPLT,
				CURRENT_TIMESTAMP,
				tiempoValidez
		);

		FOR j IN 0..COALESCE(array_length(p_ttemplt, 1)-1, 0)
		LOOP
			
			FOR k IN c_tempgm(p_ttemplt[j].TEMPLT_TEMPLT)
			LOOP
				
				--Se agregan los juegos de la plantilla al codigo de acceso
				INSERT INTO GMS_TGMAC(
					GMAC_GAME,
					GMAC_ACODE
				)VALUES(
					k.TEMPGM_GAME,
					v_acode_acode
				);
			END LOOP;
			
		
		END LOOP;
    
        
    END LOOP;
    
    
    IF(v_cod_mensaje = 'OK')THEN
        p_cod_rpta := v_cod_mensaje;
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;
        
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
        p_mensajes := v_mensaje;
    ELSE
        p_cod_rpta := v_cod_mensaje;
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;
    END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$createacodescant"(text,numeric,text,text,text,type_tt_cl_templt)
    

