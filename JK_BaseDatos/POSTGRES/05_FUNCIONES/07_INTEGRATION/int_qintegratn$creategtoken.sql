-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qintegratn$creategtoken"
(	
	p_prcs              	text,
    p_player_player         text,
    p_game_game             text,
    p_app_session           text,
	OUT p_cod_rpta      	text,
	OUT p_errores       	TYPE_TT_GE_ERRO,
	OUT p_mensajes      	TYPE_TT_GE_MNSJ,
    OUT p_gtoken_gtoken 	TEXT
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
  
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	v_gtoken_gtoken	VARCHAR(50);
    


BEGIN

    --Se inactivan los toques que 
    UPDATE INT_TGTOKEN
    SET GTOKEN_STATE = 'CAD'
    	WHERE 	GTOKEN_PLAYER=p_player_player 		
        AND		GTOKEN_STATE='ACT'
        AND     CURRENT_TIMESTAMP<=GTOKEN_DATE_VALIDATE;
    
	select cast(uuid_generate_v4() as varchar) INTO v_gtoken_gtoken;
    
    
	--Asignacion de Token para la GTOKEN
	INSERT INTO INT_TGTOKEN (
				GTOKEN_GTOKEN,
				GTOKEN_PLAYER,
                GTOKEN_GAME,
				GTOKEN_SAPP,
				GTOKEN_DATE_STAR,
				GTOKEN_DATE_VALIDATE,
				GTOKEN_STATE
		)values(
				v_gtoken_gtoken,
				p_player_player,
                p_game_game,
                p_app_session,
				CURRENT_TIMESTAMP,
				CURRENT_TIMESTAMP + (1 * interval '2 hour'),
				'ACT'
		);
	
	p_gtoken_gtoken:=v_gtoken_gtoken;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$creategtoken"(text, text, text)
    
