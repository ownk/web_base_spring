-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qintegratn$starsmatch"
(	
	p_prcs              text,
	p_match_match     	text,
    p_session_session	text,
    p_gtoken_gtoken  	text,
    OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
  
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	c_ssmatch  CURSOR FOR				
	select nextval ('int_ssmatch');
	
	v_nextval_smatch 	NUMERIC;
	v_smatch_smatch	VARCHAR(50);

BEGIN
  
	
	OPEN c_ssmatch;
	FETCH c_ssmatch INTO v_nextval_smatch;
	CLOSE c_ssmatch;
  
    --v_smatch_smatch := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_smatch);
    
    select cast(uuid_generate_v4() as varchar) INTO v_smatch_smatch;

	--Asignacion de session para el match
	INSERT INTO INT_TSMATCH (
				SMATCH_SMATCH,
				SMATCH_SESSION,
				SMATCH_MATCH,
                SMATCH_GTOKEN,
				SMATCH_DATE_STAR
		)values(
				v_smatch_smatch,
				p_session_session,
				p_match_match,
                p_gtoken_gtoken,
				CURRENT_TIMESTAMP
		);
	

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$starsmatch"(text, text, text)
    
