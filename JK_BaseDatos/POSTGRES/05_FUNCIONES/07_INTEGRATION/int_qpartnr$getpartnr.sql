-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION juegohacker_us."int_qpartnr$getpartnr"
(
	p_prcs              text,
	p_sitekey         	text,
    p_accesskey      	text,
    p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       juegohacker_us.TYPE_TT_GE_ERRO,
	OUT p_mensajes      juegohacker_us.TYPE_TT_GE_MNSJ,
    OUT p_tpartnr         juegohacker_us.TYPE_TT_IN_PARTNR
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_partnr CURSOR FOR
    SELECT
        PARTNR_PARTNR,
        pgp_sym_decrypt(PARTNR_NAME::bytea,  p_pgcrypto_key) PARTNR_NAME, 
        pgp_sym_decrypt(PARTNR_SITEKEY::bytea,  p_pgcrypto_key) PARTNR_SITEKEY, 
        pgp_sym_decrypt(PARTNR_ACCESSKEY::bytea,  p_pgcrypto_key) PARTNR_ACCESSKEY
        FROM juegohacker_us.INT_TPARTNR
        WHERE 	upper(p_sitekey) = upper(pgp_sym_decrypt(PARTNR_SITEKEY::bytea,  p_pgcrypto_key)) AND
				upper(p_accesskey)=upper(pgp_sym_decrypt(PARTNR_ACCESSKEY::bytea,  p_pgcrypto_key));
        
    to_ge_erro juegohacker_us.TYPE_TO_GE_ERRO;
    tt_ge_erro juegohacker_us.TYPE_TT_GE_ERRO;

    to_in_partnr juegohacker_us.TYPE_TO_IN_PARTNR;
    tt_in_partnr juegohacker_us.TYPE_TT_IN_PARTNR;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje juegohacker_us.TYPE_TT_GE_MNSJ;

BEGIN
  
    FOR i IN c_partnr
    LOOP

        to_in_partnr := ROW (i.PARTNR_PARTNR,i.PARTNR_NAME,i.PARTNR_SITEKEY,i.PARTNR_ACCESSKEY)::juegohacker_us.TYPE_TO_IN_PARTNR;
        tt_in_partnr[COALESCE(array_length(tt_in_partnr, 1), 0)] := to_in_partnr;

    END LOOP;

    p_tpartnr := tt_in_partnr;
    
    SELECT
        *
        FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::juegohacker_us.TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION juegohacker_us."int_qpartnr$getpartnr"(text, text, text, text)
    
