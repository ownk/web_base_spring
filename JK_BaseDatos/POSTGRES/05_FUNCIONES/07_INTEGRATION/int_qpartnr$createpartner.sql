-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             03/02/2020      ownk           Se crea funcion 
-- 1001												20/04/2020		ownk		    Se ajusta funcion para genera ID
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qpartnr$createpartner"
(
	p_prcs 				TEXT,
	p_tpartnr			TYPE_TT_IN_PARTNR,
    p_pgcrypto_key      TEXT,
    p_pgcrypto_alg      TEXT,
	OUT p_cod_rpta 		TEXT,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

	c_spartner  CURSOR FOR				
	select nextval ('int_spartnr');
	
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
	v_cod_mensaje		VARCHAR(100):='OK';
	v_nextval_partner 	NUMERIC;
	v_partnr_partnr     VARCHAR(50);
    
	F$RESULT_LOGICA RECORD;
	resultado	BOOLEAN;

BEGIN
	
    FOR i IN 0..COALESCE(array_length(p_tpartnr, 1)-1, 0)
	LOOP
	
        --OPEN c_spartner; --1001
        --FETCH c_spartner INTO v_nextval_partner; --1001
        --CLOSE c_spartner; --1001

        --v_partnr_partnr := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_partner); --1001
		
		select cast(uuid_generate_v4() as varchar) INTO v_partnr_partnr; --1001
        
		--Crear Partner
		Insert Into INT_TPARTNR(
				PARTNR_PARTNR,
				PARTNR_NAME,
				PARTNR_SITEKEY,
				PARTNR_ACCESSKEY,
				PARTNR_STATE
		)VALUES(
				v_partnr_partnr,
				pgp_sym_encrypt(p_tpartnr[i].PARTNR_NAME, p_pgcrypto_key,p_pgcrypto_alg),
				pgp_sym_encrypt(p_tpartnr[i].PARTNR_SITEKEY, p_pgcrypto_key,p_pgcrypto_alg),
				pgp_sym_encrypt(p_tpartnr[i].PARTNR_ACCESSKEY, p_pgcrypto_key,p_pgcrypto_alg),
				'ACT'
		);
        
         SELECT
        *
        FROM ge_qutils$insertlogt(200,p_tpartnr[i].PARTNR_NAME||','||p_tpartnr[i].PARTNR_SITEKEY||','||p_tpartnr[i].PARTNR_ACCESSKEY)
        INTO resultado;
        
    END LOOP;
	
	

	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "int_qpartnr$createpartner"(text,TYPE_TT_IN_PARTNR,text,text)
    

