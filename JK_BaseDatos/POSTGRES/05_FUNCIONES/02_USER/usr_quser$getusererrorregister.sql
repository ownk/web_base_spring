-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             31/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$getusererrorregister"
(
    p_prcs              text,
    p_pgcrypto_key      text,
    OUT p_cod_rpta      text,
    OUT p_errores       TYPE_TT_GE_ERRO,
    OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tuser         TYPE_TT_AU_USER,
    OUT p_totpu         TYPE_TT_AU_OTPU
)
RETURNS record
LANGUAGE 'plpgsql' 
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_user_user CURSOR FOR
    SELECT
        USER_USER
        FROM AUT_TUSER,AUT_TOTPU
        WHERE USER_USER=OTPU_USER AND 
        USER_STATE='INA' AND OTPU_STATE='ACT';
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_aut_user TYPE_TO_AU_USER;
    tt_aut_user TYPE_TT_AU_USER;
    
    to_aut_otpu TYPE_TO_AU_OTPU;
    tt_aut_otpu TYPE_TT_AU_OTPU;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
    FOR i IN c_user_user
    LOOP

        to_aut_user = usr_quser$getuser(i.USER_USER,p_pgcrypto_key);
        tt_aut_user[COALESCE(array_length(tt_aut_user, 1), 0)] := to_aut_user;
        
        to_aut_otpu = usr_quser$getotpu(i.USER_USER);
        tt_aut_otpu[COALESCE(array_length(tt_aut_user, 1), 0)] := to_aut_otpu;

    END LOOP;

    p_tuser := tt_aut_user;
    p_totpu := tt_aut_otpu;
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;
    
