-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             24/02/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$updateusererrorregister"
(
	p_prcs              text,
    p_user_user         text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    v_client_client VARCHAR(50);

BEGIN
    p_cod_rpta := 'OK';
   
    -- Se activa el Usuario
    UPDATE AUT_TUSER
    SET USER_STATE='ACT',USER_FLAG_EMAIL='VAL'
	WHERE USER_USER = p_user_user;
    
    --Se activa el Cliente
    SELECT USCL_CLIENT 
        INTO v_client_client
        FROM CLI_TUSCL
        WHERE USCL_USER=p_user_user;
    
    UPDATE CLI_TCLIENT
    SET CLIENT_STATE='ACT'
    WHERE CLIENT_CLIENT=v_client_client;
    
    -- Se activa el Player
    UPDATE GMS_TPLAYER
    SET PLAYER_STATE='ACT', PLAYER_DATEAC=CURRENT_TIMESTAMP
	WHERE PLAYER_USER = p_user_user;
    
    -- Se actualiza el OTP a usado
    UPDATE AUT_TOTPU
    SET OTPU_STATE='USE',
        OTPU_DATE_USED = CURRENT_TIMESTAMP
    WHERE   OTPU_USER=p_user_user;
    
    
    -- Se crea registro del cambio de estado del usuario
    INSERT INTO AUT_TSTUS (
            STUS_USER,
            STUS_STATE_UPDATE,
            STUS_DATE_UPDATE
        )values(
            p_user_user,
            'ACT',
            CURRENT_TIMESTAMP
        );
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "usr_quser$updateusererrorregister"(text, text)
    
