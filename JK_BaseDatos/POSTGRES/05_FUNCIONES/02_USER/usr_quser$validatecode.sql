-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             22/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION juegohacker_us."usr_quser$validatecode"(
	p_codigo_acceso text,
	p_pgcrypto_key	text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_cant_codes NUMERIC;
	v_date TIMESTAMP;

BEGIN
	
	v_date = CURRENT_TIMESTAMP;

    SELECT

        COUNT(*)

        INTO STRICT v_cant_codes

        FROM juegohacker_us.CLI_TACODE
        WHERE 	p_codigo_acceso =  pgp_sym_decrypt(ACODE_CODE::bytea, p_pgcrypto_key) AND 
				ACODE_STATE = 'ACT'			 AND
				v_date <= ACODE_DATE_VALIDITY;

    IF (v_cant_codes > 0) THEN

        RETURN TRUE;

    ELSE

       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION juegohacker_us."usr_quser$validatecode"(text, text)
    
