-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$menutitlesbyuser"
(
	p_prcs          text,
	p_user_user     text,
	OUT p_cod_rpta  text,
	OUT p_errores   TYPE_TT_GE_ERRO,
	OUT p_mensajes  TYPE_TT_GE_MNSJ,
    OUT p_tmenu     TYPE_TT_AU_MENU
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_menutitlesbyuser CURSOR FOR
    SELECT DISTINCT
        ns_sccm_ext.to_char(m.menu_menu) AS menu_menu, ns_sccm_ext.to_char(m.menu_orden) AS menu_orden, m.menu_nombre, m.menu_descri, m.menu_icon
        FROM AUT_TUROL, AUT_TSROL, aut_tmsrv, aut_tmenu AS m
        WHERE UROL_USER = p_user_user AND UROL_ROL = SROL_ROL AND msrv_srvc = SROL_SRVC AND menu_menu = msrv_menu
        ORDER BY menu_orden ASC;

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_ge_menu TYPE_TO_AU_MENU;
    tt_ge_menu TYPE_TT_AU_MENU;

    v_crta_http_gral CHARACTER VARYING(5) := 'OK';

    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN

    FOR i IN c_menutitlesbyuser
    LOOP

        to_ge_menu := ROW (i.menu_menu, i.menu_orden, i.menu_nombre, i.menu_descri, i.menu_icon)::TYPE_TO_AU_MENU;
        tt_ge_menu[COALESCE(array_length(tt_ge_menu, 1), 0)] := to_ge_menu;

    END LOOP;

    p_tmenu := tt_ge_menu;
    p_cod_rpta := COALESCE(v_crta_http_gral, 'OK');
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$menutitlesbyuser"(text, text)
    
