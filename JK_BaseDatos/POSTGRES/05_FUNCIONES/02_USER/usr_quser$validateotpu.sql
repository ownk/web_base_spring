-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             22/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "usr_quser$validateotpu"(
	p_otpu 	text,
	p_user_user text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_token_valido NUMERIC;
	v_date TIMESTAMP;
	

BEGIN
	
	v_date := CURRENT_TIMESTAMP;

    SELECT

        COUNT(*)

        INTO STRICT v_token_valido

        FROM AUT_TOTPU

        WHERE 	OTPU_OTP = p_otpu 		AND 
				OTPU_STATE='ACT' 		AND 
				OTPU_USER=p_user_user	AND
				v_date<=OTPU_DATE_END;

    IF (v_token_valido > 0) THEN
		
        RETURN TRUE;

    ELSE
		--Actualizacion del estado del otp activo a invalido
		UPDATE AUT_TOTPU
		SET OTPU_STATE='INV'
		WHERE 	OTPU_OTP=p_otpu 		AND
				OTPU_USER=p_user_user	AND
				OTPU_STATE='ACT';
		
       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "usr_quser$validateotpu"(text,text)
    
