--VALIDACIONES
\i 05_FUNCIONES/02_USER/usr_quser$validateacode.sql
\i 05_FUNCIONES/02_USER/usr_quser$validateemail.sql
\i 05_FUNCIONES/02_USER/usr_quser$validatenick.sql
\i 05_FUNCIONES/02_USER/usr_quser$validateotpp.sql
\i 05_FUNCIONES/02_USER/usr_quser$validateotpu.sql

--FUNCIONES
\i 05_FUNCIONES/02_USER/usr_quser$getuser.sql
\i 05_FUNCIONES/02_USER/usr_quser$getotpu.sql

--NEGOCIO
\i 05_FUNCIONES/02_USER/usr_quser$changepassword.sql
\i 05_FUNCIONES/02_USER/usr_quser$createuser.sql    
\i 05_FUNCIONES/02_USER/usr_quser$forgotpassword.sql
\i 05_FUNCIONES/02_USER/usr_quser$getmenubyuser.sql
\i 05_FUNCIONES/02_USER/usr_quser$getusernick.sql
\i 05_FUNCIONES/02_USER/usr_quser$homepagebyuser.sql
\i 05_FUNCIONES/02_USER/usr_quser$menutitlesbyuser.sql
\i 05_FUNCIONES/02_USER/usr_quser$modulesbyuser.sql
\i 05_FUNCIONES/02_USER/usr_quser$tpserviceslistedbyuser.sql
\i 05_FUNCIONES/02_USER/usr_quser$userservicesbyurl.sql
\i 05_FUNCIONES/02_USER/usr_quser$getuseremail.sql
\i 05_FUNCIONES/02_USER/usr_quser$getusererrorregister.sql
\i 05_FUNCIONES/02_USER/usr_quser$updateusererrorregister.sql
\i 05_FUNCIONES/02_USER/usr_quser$deleteuser.sql

--API
\i 05_FUNCIONES/02_USER/api_quser$changepassword.sql        
\i 05_FUNCIONES/02_USER/api_quser$forgotpassword.sql        
\i 05_FUNCIONES/02_USER/api_quser$getmenubyuser.sql         
\i 05_FUNCIONES/02_USER/api_quser$getusernick.sql               
\i 05_FUNCIONES/02_USER/api_quser$tpserviceslistedbyuser.sql
\i 05_FUNCIONES/02_USER/api_quser$userservicesbyurl.sql     
\i 05_FUNCIONES/02_USER/api_quser$validateemail.sql         
\i 05_FUNCIONES/02_USER/api_quser$getuseremail.sql         
\i 05_FUNCIONES/02_USER/api_quser$getusererrorregister.sql         
\i 05_FUNCIONES/02_USER/api_quser$updateusererrorregister.sql         
\i 05_FUNCIONES/02_USER/api_quser$deleteuser.sql         
