-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             24/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "usr_quser$validateotpp"(
	p_otpp 		text,
	p_user_user text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_token_valido NUMERIC;
	v_date TIMESTAMP;
	

BEGIN
	
	v_date := CURRENT_TIMESTAMP;
	
    SELECT

        COUNT(*)

        INTO STRICT v_token_valido

        FROM AUT_TOTPP

        WHERE 	OTPP_OTP = p_otpp 		AND 
				OTPP_STATE='ACT' 		AND 
				OTPP_USER=p_user_user	AND
				v_date<=OTPP_DATE_END;

    IF (v_token_valido > 0) THEN
		
        RETURN TRUE;

    ELSE
		--Actualizacion del estado del otp activo a invalido
		UPDATE AUT_TOTPP
		SET OTPP_STATE='INV'
		WHERE 	OTPP_OTP=p_otpp 		AND
				OTPP_USER=p_user_user	AND
				OTPP_STATE='ACT';
		
       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "usr_quser$validateotpp"(text,text)
    
