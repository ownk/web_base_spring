-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             24/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "usr_quser$changepassword"
(
	p_prcs 				TEXT,
	p_otpp	        	TEXT,
	p_pass	        	TEXT,
	p_email	        	TEXT,
	p_pgcrypto_key	    TEXT,
	p_pgcrypto_alg	    TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
		
	v_cod_mensaje	VARCHAR(100):='OK';
	
	c_user CURSOR(vc_email text) FOR
	select USER_USER FROM AUT_TUSER
	where  USER_HASH_EMAIL=md5(upper(vc_email)) AND USER_STATE ='ACT';
	
	v_user_user VARCHAR(50);
	v_email 	TEXT;
    v_pass_val NUMERIC;
	
BEGIN

	OPEN c_user (p_email);
	FETCH c_user INTO v_user_user;
	CLOSE c_user;
	
	IF (usr_quser$validateotpp(p_otpp,v_user_user)='false')THEN
		v_cod_mensaje = 'ERR_TOKEN_PASS_INVAL';
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta(v_cod_mensaje, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	END IF;
    

		
	IF(v_cod_mensaje='OK')THEN
		
		--Actualizacion de contraseña
		UPDATE AUT_TUSER SET
			USER_PASS=pgp_sym_encrypt(p_pass, p_pgcrypto_key,p_pgcrypto_alg)
		WHERE USER_USER=v_user_user;
	
		--Actualizacion de estado del OTP a Usado
		
		UPDATE AUT_TOTPP
		SET OTPP_STATE='USE',
			OTPP_DATE_USED = CURRENT_TIMESTAMP
		WHERE 	OTPP_OTP=p_otpp AND	
				OTPP_USER=v_user_user;
	END IF;

	
    IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
	p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$changepassword"(text, text, text, text, text, text )
    

