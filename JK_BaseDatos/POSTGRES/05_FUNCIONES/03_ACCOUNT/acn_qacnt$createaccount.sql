-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/10/2019      ownk           Se crea funcion 
-- 1001                                             20/04/2020      ownk           Se ajusta funcion para generar id 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "acn_qacnt$createaccount"
(
	p_prcs 				TEXT,
	p_codigo_acceso		TEXT,
	p_pgcrypto_key		TEXT,
	OUT p_acnt_acnt		text,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	/*ini1001
	c_sacnt CURSOR FOR				
	select nextval ('acn_sacnt');
	c_sowner CURSOR FOR
	select nextval ('acn_sowner');
	fin1001*/
	c_datev_acode CURSOR FOR
	SELECT ACODE_DATE_VALIDITY FROM cli_tacode
	WHERE p_codigo_acceso =  pgp_sym_decrypt(ACODE_CODE::bytea, p_pgcrypto_key);
	
	c_client CURSOR FOR
	SELECT ACODE_CLIENT FROM cli_tacode
	WHERE p_codigo_acceso =  pgp_sym_decrypt(ACODE_CODE::bytea, p_pgcrypto_key);
	
	
	c_acode CURSOR FOR
	select ACODE_ACODE FROM CLI_TACODE
	WHERE p_codigo_acceso =  pgp_sym_decrypt(ACODE_CODE::bytea, p_pgcrypto_key);
 
	v_cod_mensaje	VARCHAR(100):='OK';
	
	v_client		VARCHAR(50);
	v_acnt_acnt 	VARCHAR(50);
	v_acode_acode	VARCHAR(50);
	v_owner_owner 	VARCHAR(50);
	v_nextval_acnt 	NUMERIC;
	v_nextval_owner	NUMERIC;
	v_datev_acode 	TIMESTAMP;
BEGIN

	OPEN c_datev_acode;
	FETCH c_datev_acode INTO v_datev_acode;
	CLOSE c_datev_acode;
	
	OPEN c_acode;
	FETCH c_acode INTO v_acode_acode;
	CLOSE c_acode;
	
	OPEN c_client;
	FETCH c_client INTO v_client;
	CLOSE c_client;
	
	/*ini1001
	OPEN c_sacnt;
	FETCH c_sacnt INTO v_nextval_acnt;
	CLOSE c_sacnt;
	
	OPEN c_sowner;
	FETCH c_sowner INTO v_nextval_owner;
	CLOSE c_sowner;
	
	
	
	
	v_acnt_acnt := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_acnt);
	v_owner_owner := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_owner);
	fin1001*/
	
	select cast(uuid_generate_v4() as varchar) INTO v_acnt_acnt;--1001
	select cast(uuid_generate_v4() as varchar) INTO v_owner_owner;--1001
	
	--Crear Una Nueva Cuenta
		Insert Into ACN_TACNT(
				ACNT_ACNT,
				ACNT_DATECR,
				ACNT_STATE,
				ACNT_DATE_OPENING,
				ACNT_DATE_VALIDITY,
				ACNT_TPAC,
				ACNT_ACODE_INIT
		)VALUES(
				v_acnt_acnt,
				CURRENT_DATE,
				'ACT',
				CURRENT_TIMESTAMP,
				v_datev_acode,
				'1',
				v_acode_acode
		);
	--Relacionar la cuenta con el codigo de acceso
		Insert Into ACN_TACAC(
				ACAC_ACODE,
				ACAC_ACNT
		)VALUES(
				v_acode_acode,
				v_acnt_acnt
		);
	--Crear al propietario de la cuenta
		
		Insert Into ACN_TOWNER(
			owner_owner,
			owner_client,
			owner_acnt
		)VALUES(
			v_owner_owner,
			v_client,
			v_acnt_acnt
		);
		
		--Actualizacion del estado de codigo de acceso a usado
		UPDATE CLI_TACODE
		SET ACODE_STATE='USE'
		WHERE 	ACODE_ACODE=v_acode_acode;
		
	IF(v_cod_mensaje = 'OK')THEN
		p_acnt_acnt := v_acnt_acnt;
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
	p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "acn_qacnt$createaccount"(text, text, text)
    

