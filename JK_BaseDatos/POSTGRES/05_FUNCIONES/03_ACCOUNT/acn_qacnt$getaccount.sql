-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "acn_qacnt$getaccount"
(
	p_acnt_acnt     text
)
RETURNS TYPE_TO_AC_ACNT
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_acnt CURSOR FOR
    SELECT
        ACNT_ACNT,
		ACNT_DATECR,
        ACNT_STATE,
        ACNT_DATE_OPENING,
		ACNT_DATE_VALIDITY,
		ACNT_TPAC,
        ACNT_ACODE_INIT
        FROM ACN_TACNT
        WHERE ACNT_ACNT=p_acnt_acnt;
        
  
    to_ac_acnt TYPE_TO_AC_ACNT;
	
BEGIN
  
	--Obtener la Cuenta por ID
	
    FOR i IN c_acnt
    LOOP

        to_ac_acnt := ROW (i.ACNT_ACNT,i.ACNT_DATECR,i.ACNT_STATE,i.ACNT_DATE_OPENING,i.ACNT_DATE_VALIDITY,i.ACNT_TPAC, i.ACNT_ACODE_INIT)::TYPE_TO_AC_ACNT;
	
    END LOOP;

    return to_ac_acnt;
 


END;

$BODY$;

    
