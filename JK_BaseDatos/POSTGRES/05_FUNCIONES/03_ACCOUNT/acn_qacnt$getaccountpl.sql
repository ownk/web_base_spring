-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             05/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "acn_qacnt$getaccountpl"
(
	p_prcs              text,
	p_client_client     text,
    p_player_player		text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_taccount      TYPE_TT_AC_ACNT
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_account CURSOR FOR
    SELECT DISTINCT
        ACNT_ACNT
        FROM ACN_TACNT,ACN_TOWNER,GMS_TPLACN,ACN_TACAC
        WHERE 	OWNER_CLIENT = P_CLIENT_CLIENT AND 
				PLACN_PLAYER = p_player_player AND
				PLACN_ACNT=ACNT_ACNT 	AND 
				ACNT_ACNT = OWNER_ACNT 	AND
				ACAC_ACNT = ACNT_ACNT;
	
	 
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_ac_acnt TYPE_TO_AC_ACNT;
    tt_ac_acnt TYPE_TT_AC_ACNT;
	
	v_acnt_valid BOOLEAN;
	
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
  
    FOR i IN c_account
    LOOP
		
		v_acnt_valid = acn_qacnt$validateacnt(i.ACNT_ACNT);
		
        to_ac_acnt := acn_qacnt$getaccount(i.ACNT_ACNT);
        tt_ac_acnt[COALESCE(array_length(tt_ac_acnt, 1), 0)] := to_ac_acnt;
		
    END LOOP;

    p_taccount := tt_ac_acnt;
	 
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

    
