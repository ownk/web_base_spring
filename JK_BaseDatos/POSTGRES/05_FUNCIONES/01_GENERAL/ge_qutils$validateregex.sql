-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             22/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "ge_qutils$validateregex"(
	p_dato_validar  text,
    p_cnfg_cnfg     text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_valido NUMERIC=0;
    v_expresion TEXT;


BEGIN

    SELECT * 
       FROM "ge_qutils$getcnfg_value"(p_cnfg_cnfg)
       INTO v_expresion;


    select COUNT (*) 
        INTO STRICT v_valido
        from (
            SELECT
            REGEXP_MATCHES(p_dato_validar,
            v_expresion,
            'g') 
            ) REGEX;

    


    IF (v_valido > 0) THEN

        RETURN TRUE;

    ELSE

       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "ge_qutils$validateregex"(text, text)
    
