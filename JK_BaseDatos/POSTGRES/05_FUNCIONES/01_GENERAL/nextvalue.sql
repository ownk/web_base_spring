-- FUNCTION: nextvalue(text)

CREATE OR REPLACE FUNCTION nextvalue(
	sequencename text)
    RETURNS double precision
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    result DOUBLE PRECISION;

BEGIN

    EXECUTE (CONCAT_WS('', 'SELECT      ', sequencename, '.nextval')) INTO STRICT result;

    RETURN (result);

END;

$BODY$;

--ALTER FUNCTION nextvalue(text)
    
