-- FUNCTION: dias(timestamp without time zone, timestamp without time zone)

CREATE OR REPLACE FUNCTION dias(
	fechamenor timestamp without time zone,
	fechamayor timestamp without time zone)
    RETURNS double precision
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

BEGIN

    RETURN TRUNC((EXTRACT (EPOCH FROM fechaMayor - fechaMenor) / 86400)::NUMERIC);

END;

$BODY$;

--ALTER FUNCTION dias(timestamp without time zone, timestamp without time zone)
    
