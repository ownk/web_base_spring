-- FUNCTION: "ge_qlgtr$finalizar_log"(text, text, text, text)

CREATE OR REPLACE FUNCTION "ge_qlgtr$finalizar_log"(
	p_lgtr text,
	p_crta text,
	p_id_entidad text,
	p_error text,
	OUT p_cod_rpta text,
	OUT p_cod_rpta_descri text)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    c_lgtr CURSOR FOR
    SELECT
        lgtr_lgtr
        FROM ge_tlgtr
        WHERE lgtr_lgtr = p_lgtr;

    r_lgtr text;
    
BEGIN

    r_lgtr := NULL;

    OPEN c_lgtr;
    FETCH c_lgtr INTO r_lgtr;
    CLOSE c_lgtr;

    IF (r_lgtr IS NULL) THEN

        p_cod_rpta := 'ER_LGTR_NE';
        p_cod_rpta_descri := 'Log de transaccion no existe o no se ha creado correctamente';

        RETURN;

    END IF;

    /* -------------------------------------------*/
    /* Se actualiza el log con el codigo de rspta */
    /* correspondiente y la fecha actual          */
    /* -------------------------------------------*/

    UPDATE ge_tlgtr
    SET lgtr_crta = p_crta, lgtr_fech_fin = CURRENT_TIMESTAMP, lgtr_id_entidad = COALESCE(p_id_entidad, lgtr_id_entidad)
        WHERE lgtr_lgtr = p_lgtr;

    p_cod_rpta := 'OK';
    p_cod_rpta_descri := 'Finalizacion de log realizada con exito';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ER_LGTR_NC';
            p_cod_rpta_descri := CONCAT_WS('', 'Error no controlado', SQLERRM);

END;

$BODY$;

--ALTER FUNCTION "ge_qlgtr$finalizar_log"(text, text, text, text)
    
