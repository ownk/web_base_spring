-- ***************************************************
-- **          PROYECTO NS UBP                      **
-- **          REV:01\02\2019                       ** 
-- **          CLIENTE: HackerGame                  **
-- ***************************************************

-- FUNCTION: "api_ge_qutils$generartypemensajerpta"(text, text, TYPE_TT_GE_MNSJ, text)

CREATE OR REPLACE FUNCTION "api_ge_qutils$generartypemensajerpta"(
	p_codigo                text,
	p_prcs                  text,
	INOUT p_type_mensaje    TYPE_TT_GE_MNSJ,
	p_textoadicional        text DEFAULT ' '::text)
    RETURNS TYPE_TT_GE_MNSJ
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

/* */

DECLARE

    mensajeObject type_to_ge_mnsj;

BEGIN

    mensajeObject := ROW (CURRENT_TIMESTAMP, NULL, p_codigo, CONCAT_WS('', api_ge_qutils$getmensajeporcodigo(p_codigo, p_prcs), ' ', p_textoAdicional))::type_to_ge_mnsj;
    p_type_mensaje[COALESCE(array_length(p_type_mensaje, 1), 0)] := mensajeObject;

END;

$BODY$;

--ALTER FUNCTION "api_ge_qutils$generartypemensajerpta"(text, text, TYPE_TT_GE_MNSJ, text)
    
