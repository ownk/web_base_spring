-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             08/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "ge_qutils$getcnfg"
(
	p_cnfg_cnfg     	text
)
RETURNS TYPE_TO_GE_CNFG
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_cnfg CURSOR FOR
    SELECT
        CNFG_CNFG,
        CNFG_CATEGORIA,
        CNFG_DESCRI,
        CNFG_TIPO_DATO,
		CNFG_VALOR,
		CNFG_CLASIF
        FROM GE_TCNFG
		WHERE CNFG_CNFG=p_cnfg_cnfg;

	to_ge_cnfg TYPE_TO_GE_CNFG;

BEGIN
	
	--Obtener el parametro general del app por id
	
	FOR i IN c_cnfg
	LOOP
        to_ge_cnfg := ROW (i.CNFG_CNFG,i.CNFG_CATEGORIA,i.CNFG_DESCRI,i.CNFG_TIPO_DATO,i.CNFG_VALOR,i.CNFG_CLASIF)::TYPE_TO_GE_CNFG;
	END LOOP;

	RETURN to_ge_cnfg;


END;

$BODY$;

--ALTER FUNCTION "ge_qutils$getcnfg"(text)
    
