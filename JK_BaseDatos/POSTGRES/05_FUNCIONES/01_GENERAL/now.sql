-- FUNCTION: now()

CREATE OR REPLACE FUNCTION now(
	)
    RETURNS timestamp without time zone
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

BEGIN

    RETURN (CURRENT_TIMESTAMP);

END;

$BODY$;

--ALTER FUNCTION now()
    
