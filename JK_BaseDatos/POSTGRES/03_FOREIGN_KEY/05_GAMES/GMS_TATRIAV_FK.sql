/* Create Foreign Key Constraints */

ALTER TABLE ONLY GMS_TATRIAV ADD CONSTRAINT FK_GMS_TATRIAV_GMS_TAVATAR
	FOREIGN KEY (ATRIAV_AVATAR) REFERENCES GMS_TAVATAR (AVATAR_AVATAR)
;
