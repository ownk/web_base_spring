--
-- Name: AUT_TLOGIN fk_AUT_TLOGIN_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TPASSCH
    ADD CONSTRAINT fk_aut_tpassch_aut_tuser FOREIGN KEY (passch_user) REFERENCES AUT_TUSER(user_user);
