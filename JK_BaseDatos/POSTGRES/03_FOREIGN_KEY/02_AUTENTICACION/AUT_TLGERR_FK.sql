--
-- Name: AUT_TLGERR fk_AUT_TLGERR_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TLGERR
    ADD CONSTRAINT fk_aut_tlgerr_aut_tuser FOREIGN KEY (LGERR_USER) REFERENCES AUT_TUSER(USER_USER);