--
-- Name: aut_tsurl fk_aut_tsurl_aut_tsrvc; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY aut_tsurl
    ADD CONSTRAINT fk_aut_tsurl_aut_tsrvc FOREIGN KEY (surl_srvc) REFERENCES AUT_TSRVC(SRVC_SRVC);