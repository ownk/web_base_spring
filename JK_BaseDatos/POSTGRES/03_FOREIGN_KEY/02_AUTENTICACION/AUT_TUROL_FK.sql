--
-- Name: aut_turol fk_aut_turol_aut_trol; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TUROL
    ADD CONSTRAINT fk_aut_turol_aut_trol FOREIGN KEY (UROL_ROL) REFERENCES AUT_TROL(ROL_ROL);

--
-- Name: aut_turol fk_aut_turol_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TUROL
    ADD CONSTRAINT fk_aut_turol_aut_tuser FOREIGN KEY (UROL_USER) REFERENCES AUT_TUSER(USER_USER);