--
-- Name: AUT_TLGOK fk_AUT_TLGOK_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TLGOK
    ADD CONSTRAINT fk_aut_tlgok_aut_tuser FOREIGN KEY (LGOK_USER) REFERENCES AUT_TUSER(USER_USER);