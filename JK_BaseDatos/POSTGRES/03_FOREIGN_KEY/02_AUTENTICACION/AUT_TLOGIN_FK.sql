--
-- Name: AUT_TLOGIN fk_AUT_TLOGIN_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TLOGIN
    ADD CONSTRAINT fk_aut_tlogin_aut_tuser FOREIGN KEY (login_user) REFERENCES AUT_TUSER(USER_USER);