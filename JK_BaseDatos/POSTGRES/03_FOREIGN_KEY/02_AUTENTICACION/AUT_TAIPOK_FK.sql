--
-- Name: AUT_TAIPOK fk_AUT_TAIPOK_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TAIPOK
    ADD CONSTRAINT fk_aut_taipok_aut_tuser FOREIGN KEY (AIPOK_USER) REFERENCES AUT_TUSER(USER_USER);