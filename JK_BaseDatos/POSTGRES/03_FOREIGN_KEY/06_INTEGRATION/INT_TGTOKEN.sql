/* Create Foreign Key Constraints */

ALTER TABLE ONLY INT_TGTOKEN ADD CONSTRAINT FK_INT_TGTOKEN_GMS_TPLAYER
	FOREIGN KEY (GTOKEN_PLAYER) REFERENCES GMS_TPLAYER (PLAYER_PLAYER)
;

ALTER TABLE ONLY INT_TGTOKEN ADD CONSTRAINT FK_INT_TGTOKEN_GMS_TGAME
	FOREIGN KEY (GTOKEN_GAME) REFERENCES GMS_TGAME (GAME_GAME)
;
 