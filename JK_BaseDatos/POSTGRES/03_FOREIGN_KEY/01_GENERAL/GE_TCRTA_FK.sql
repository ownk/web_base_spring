--
-- Name: ge_tcrta fk_ge_tcrta_ge_tprcs; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY ge_tcrta
    ADD CONSTRAINT fk_ge_tcrta_ge_tprcs FOREIGN KEY (crta_prcs) REFERENCES ge_tprcs(prcs_prcs);