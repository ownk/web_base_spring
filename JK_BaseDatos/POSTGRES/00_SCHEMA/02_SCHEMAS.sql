-- ***************************************************
-- **          PROYECTO WEB BASE                    **
-- **          REV:26\01\2019                       ** 
-- ***************************************************

--
-- PostgreSQL database dump
--

-- Database: JUEGOHACKER_BD

CREATE DATABASE "JUEGOHACKER_BD"
    WITH 
    OWNER = juegohacker_us    
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
	
grant all privileges on database "JUEGOHACKER_BD" TO juegohacker_us;

--Se ajusta path para creacion en el schema
ALTER DATABASE "JUEGOHACKER_BD" SET search_path TO juegohacker_schema,public;
SET search_path TO juegohacker_schema,public;

	
-- Cambio de base de datos
\c "JUEGOHACKER_BD" 



create extension pgcrypto;
create extension "uuid-ossp";
create extension pg_trgm;

--
-- Name: juegohacker_ext; Type: SCHEMA; Schema: -; Owner: juegohacker_us
--

CREATE SCHEMA juegohacker_ext;


ALTER SCHEMA juegohacker_ext OWNER TO juegohacker_us;


CREATE SCHEMA juegohacker_schema;

ALTER SCHEMA juegohacker_schema OWNER TO juegohacker_us;

