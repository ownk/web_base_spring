/* Create Foreign Key Constraints */

ALTER TABLE ONLY GMS_TCNVATB ADD CONSTRAINT FK_GMS_TCNVATB_GMS_TGAME
	FOREIGN KEY (CNVATB_GAME) REFERENCES GMS_TGAME (GAME_GAME)
;

ALTER TABLE ONLY GMS_TCNVATB ADD CONSTRAINT FK_GMS_TCNVATB_GMS_TATRB
	FOREIGN KEY (CNVATB_ATRB) REFERENCES GMS_TATRB (ATRB_ATRB)
;

ALTER TABLE ONLY GMS_TCNVATB ADD CONSTRAINT FK_GMS_TCNVATB_GMS_TCHRT
	FOREIGN KEY (CNVATB_CHRT) REFERENCES GMS_TCHRT (CHRT_CHRT)
;
