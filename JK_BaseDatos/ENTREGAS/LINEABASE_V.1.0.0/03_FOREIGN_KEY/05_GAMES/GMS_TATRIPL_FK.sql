/* Create Foreign Key Constraints */

ALTER TABLE ONLY GMS_TATRIPL ADD CONSTRAINT FK_GMS_TATRIPL_GMS_TPLAYER
	FOREIGN KEY (ATRIPL_PLAYER) REFERENCES GMS_TPLAYER (PLAYER_PLAYER)
;
