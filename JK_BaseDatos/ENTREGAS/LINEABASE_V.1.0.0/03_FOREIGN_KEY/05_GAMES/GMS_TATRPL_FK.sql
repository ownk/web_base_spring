/* Create Foreign Key Constraints */

ALTER TABLE ONLY GMS_TATRPL ADD CONSTRAINT FK_GMS_TATRPL_GMS_TPLAYER
	FOREIGN KEY (ATRPL_PLAYER) REFERENCES GMS_TPLAYER (PLAYER_PLAYER)
;

ALTER TABLE ONLY GMS_TATRPL ADD CONSTRAINT FK_GMS_TATRPL_GMS_TCHRT
	FOREIGN KEY (ATRPL_CHRT) REFERENCES GMS_TCHRT (CHRT_CHRT)
;

ALTER TABLE ONLY GMS_TATRPL ADD CONSTRAINT FK_GMS_TATRPL_GMS_TATRB
	FOREIGN KEY (ATRPL_ATRB) REFERENCES GMS_TATRB (ATRB_ATRB)
;