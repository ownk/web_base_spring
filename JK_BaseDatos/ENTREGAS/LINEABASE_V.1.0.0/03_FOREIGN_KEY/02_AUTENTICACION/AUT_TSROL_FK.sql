--
-- Name: aut_tsrol fk_aut_tsrol_aut_trol; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TSROL
    ADD CONSTRAINT fk_aut_tsrol_aut_trol FOREIGN KEY (SROL_ROL) REFERENCES AUT_TROL(ROL_ROL);

--
-- Name: aut_tsrol fk_aut_tsrol_aut_tsrvc; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TSROL
    ADD CONSTRAINT fk_aut_tsrol_aut_tsrvc FOREIGN KEY (SROL_SRVC) REFERENCES AUT_TSRVC(SRVC_SRVC);