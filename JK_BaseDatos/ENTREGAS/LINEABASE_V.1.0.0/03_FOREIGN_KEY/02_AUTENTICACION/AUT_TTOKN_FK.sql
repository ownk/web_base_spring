--
-- Name: aut_ttokn fk_aut_ttokn_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TTOKN
    ADD CONSTRAINT fk_aut_ttokn_aut_tuser FOREIGN KEY (tokn_user) REFERENCES AUT_TUSER(USER_USER);