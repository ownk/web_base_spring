--
-- Name: aut_tprsn fk_aut_tprsn_ge_tciud; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TPRSN
    ADD CONSTRAINT fk_aut_tprsn_ge_tciud FOREIGN KEY (prsn_ciud) REFERENCES GE_TCIUD(CIUD_CIUD);