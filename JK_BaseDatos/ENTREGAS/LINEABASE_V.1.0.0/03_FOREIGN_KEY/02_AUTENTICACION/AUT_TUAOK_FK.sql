--
-- Name: AUT_TUAOK fk_AUT_TUAOK_aut_tuser; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TUAOK
    ADD CONSTRAINT fk_aut_tuaok_aut_tuser FOREIGN KEY (UAOK_USER) REFERENCES AUT_TUSER(USER_USER);