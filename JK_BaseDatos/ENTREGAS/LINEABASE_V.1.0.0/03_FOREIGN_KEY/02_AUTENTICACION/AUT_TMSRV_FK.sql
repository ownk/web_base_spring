--
-- Name: aut_tmsrv fk_aut_tmsrv_aut_tmenu; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY aut_tmsrv
    ADD CONSTRAINT fk_aut_tmsrv_aut_tmenu FOREIGN KEY (msrv_menu) REFERENCES aut_tmenu(menu_menu);

--
-- Name: aut_tmsrv fk_aut_turol_useraut_tsrvc; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY aut_tmsrv
    ADD CONSTRAINT fk_aut_tmsrv_aut_tsrvc FOREIGN KEY (msrv_srvc) REFERENCES AUT_TSRVC(SRVC_SRVC);
