--
-- Name: AUT_TPSNV FK_AUT_TPSNV_AUT_TUSER; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TPSNV
ADD CONSTRAINT FK_AUT_TPSNV_AUT_TUSER FOREIGN KEY (PSNV_USER) REFERENCES AUT_TUSER(USER_USER);