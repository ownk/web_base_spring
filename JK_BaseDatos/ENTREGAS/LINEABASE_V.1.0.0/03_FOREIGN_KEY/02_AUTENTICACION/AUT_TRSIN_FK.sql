--
-- Name: aut_trsin fk_aut_trsin_aut_trol; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY aut_trsin
    ADD CONSTRAINT fk_aut_trsin_aut_trol FOREIGN KEY (rsin_rol) REFERENCES AUT_TROL(ROL_ROL);

--
-- Name: aut_trsin fk_aut_trsin_aut_tsrvc; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY aut_trsin
    ADD CONSTRAINT fk_aut_trsin_aut_tsrvc FOREIGN KEY (rsin_srvc) REFERENCES AUT_TSRVC(SRVC_SRVC);
