--
-- Name: aut_turol fk_aut_turol_aut_trol; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--
ALTER TABLE AUT_TSTUS ADD CONSTRAINT FK_AUT_TSTUS_AUT_TUSER
	FOREIGN KEY (STUS_USER) REFERENCES AUT_TUSER (USER_USER)
;