--
-- Name: aut_tsrvc fk_aut_tsrvc_aut_tmdlo; Type: FK CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

ALTER TABLE ONLY AUT_TSRVC
    ADD CONSTRAINT fk_aut_tsrvc_aut_tmdlo FOREIGN KEY (SRVC_MDLO) REFERENCES aut_tmdlo(mdlo_mdlo);