-- INSERTING into GMS_TATRB
Insert into GMS_TATRB
(
ATRB_ATRB,
ATRB_DESCRI,
ATRB_NAME,
ATRB_ICON,
ATRB_COLOR
)
values (
'life',
'GMS_TATRB.ATRB_DESCRI.life',
'GMS_TATRB.ATRB_NAME.life',
'icon',
'success'
);

Insert into GMS_TATRB
(
ATRB_ATRB,
ATRB_DESCRI,
ATRB_NAME,
ATRB_ICON,
ATRB_COLOR
)
values (
'energy',
'GMS_TATRB.ATRB_DESCRI.energy',
'GMS_TATRB.ATRB_NAME.energy',
'icon',
'info'
);

Insert into GMS_TATRB
(
ATRB_ATRB,
ATRB_DESCRI,
ATRB_NAME,
ATRB_ICON,
ATRB_COLOR
)
values (
'knowledge',
'GMS_TATRB.ATRB_DESCRI.knowledge',
'GMS_TATRB.ATRB_NAME.knowledge',
'icon',
'warning'
);

Insert into GMS_TATRB
(
ATRB_ATRB,
ATRB_DESCRI,
ATRB_NAME,
ATRB_ICON,
ATRB_COLOR
)
values (
'skill',
'GMS_TATRB.ATRB_DESCRI.skill',
'GMS_TATRB.ATRB_NAME.skill',
'icon',
'danger'
);

Insert into GMS_TATRB
(
ATRB_ATRB,
ATRB_DESCRI,
ATRB_NAME,
ATRB_ICON,
ATRB_COLOR
)
values (
'ability',
'GMS_TATRB.ATRB_DESCRI.ability',
'GMS_TATRB.ATRB_NAME.ability',
'icon',
'danger'
);

