/*

Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','AUT_QUSER.crearusuario', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_EXIST_EMAIL','AUT_QUSER.crearusuario','500', 'Ya existe este email registrado', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_EXIST_NICK','AUT_QUSER.crearusuario','500', 'Ya existe este nick para usuario', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_CODIGO_INVAL','AUT_QUSER.crearusuario','500', 'El codigo de Acceso es Invalido', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','AUT_QUSER.crearusuario','500', 'Error no controlado', 'es_CO');
 
--ACN_QACNT.crearcuenta
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','ACN_QACNT.crearcuenta', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_CODE_INVAL','ACN_QACNT.crearcuenta','500', 'Codigo de Acceso Invalido', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','ACN_QACNT.crearcuenta','500', 'Error no controlado', 'es_CO');
 
--GMS_QPLAYER.crearplayer
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','GMS_QPLAYER.crearplayer', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','GMS_QPLAYER.crearplayer','500', 'Error no controlado', 'es_CO');
 
 
--API_AUT_QUSER.validarregistro
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_AUT_QUSER.validarregistro', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_AUT_QUSER.validarregistro','500', 'Error no controlado', 'es_CO');

--AUT_QUSER.validarregistro
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','AUT_QUSER.validarregistro', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_OTP_INVAL','AUT_QUSER.validarregistro','500', 'Codigo OTP Invalido', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','AUT_QUSER.validarregistro','500', 'Error no controlado', 'es_CO');


--API_QUSER.olvidopassword
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_QUSER.olvidopassword', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_QUSER.olvidopassword','500', 'Error no controlado', 'es_CO');

--USR_QUSER.olvidopassword
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','USR_QUSER.olvidopassword', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_EMAIL_INVAL','USR_QUSER.olvidopassword','500', 'Email Ingresado es Invalido', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','USR_QUSER.olvidopassword','500', 'Error no controlado', 'es_CO');


--API_QACTUALIZAR.cambiarpassword
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','API_QACTUALIZAR.cambiarpassword', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','API_QACTUALIZAR.cambiarpassword','500', 'Error no controlado', 'es_CO');

--USR_QUSER.cambiopassword
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('OK','USR_QUSER.cambiopassword', '200','Operacion Exitosa','es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_OTP_PASS_INVAL','USR_QUSER.cambiopassword','500', 'Codigo OTP Invalido', 'es_CO');
Insert into GE_TCRTA (CRTA_CRTA,CRTA_PRCS,CRTA_HTTP_CODE,CRTA_DESCRI,CRTA_IDIOMA) values ('ERR_WHEN_OTHERS','USR_QUSER.cambiopassword','500', 'Error no controlado', 'es_CO');
*/