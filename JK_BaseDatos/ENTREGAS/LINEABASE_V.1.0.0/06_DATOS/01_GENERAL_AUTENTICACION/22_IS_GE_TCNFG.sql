-- Parametros de usuario
INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('files.upload.general.path','GENERAL','Ruta general de Archivos de aplicacion','2','D:\HackerGameFiles','USR');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF)
VALUES ('client.profile.image.upload.basepath','CLIENT','Ruta general para almacenar imagenes de profile. Debe estar dentro de la carpeta WEB-INF de la aplicacion [RUTA_ARCHIVO.WAR]/WEB-INF/classes/static/app_resources/private/img/clients','2','D:/Clientes/JuegoHacker/Ambientes/apache-tomcat-9.0.17/webapps/hackergame/WEB-INF/classes/static/app_resources/private/img/clients','USR');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('app.owner.internet.url','GENERAL','URL en Internet de la empresa Hackergame','2','https://juegohacker.com','USR');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('app.web.publico.urlbase','GENERAL','Url publica de la aplicacion. Se utiliza para envios de correo y otras operaciones. Ejemplo https://www.hackergame.co','2','http://localhost:9090','USR');


--Parametros de aplicacion
INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('game.player.attribute.scale','GAME','Escala para atributos de jugadores','2','100','APP');
                                                                                                   
INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('app.logo.internet.url','GENERAL','URL de logo de aplicacion que este activo en Internet','2','https://secureservercdn.net/160.153.138.177/u78.5ba.myftpupload.com/wp-content/uploads/2019/12/cropped-logoHGtrans-1.png','APP');
                                                                                       
INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('aut.user.password.validation.similary.porcent','USER','Porcentaje para verificar validez de password del usuario (de 0 a 1)','2','0.1','APP');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('aut.user.password.validation.regex','USER','Expresion regular para validar password del usuario','2','^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-+$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,30}$','APP');
                                                                                                   
INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF)
VALUES ('aut.user.username.validation.similary.porcent','USER','Procentaje para verificar validez de username del usuario (de 0 a 1)','2','0.1','APP');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('aut.user.username.validation.regex','USER','Expresion regular para validar nick del usuario','2','^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$','APP');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('aut.user.email.validation.regex','USER','Expresion regular para validar email del usuario','2','[^@]+@[^@]+\.[a-zA-Z]{2,}','APP');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('aut.user.username.validation.front.regex','USER','Expresion regular para validar nick del usuario desde el front','2','[^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}]?','APP');
                                                                                                   
INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF) 
VALUES ('client.profile.image.relative.baseurl','CLIENT','Ruta relativa para descarga de imagenes profile','2','/app_resources/private/img/clients/','APP');
                                                                                                   
INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF)
VALUES ('general.regex.alfanumeric','USER','Expresion regular para valores alfanumericos','2','[A-Za-z0-9\s]{1,200}$','APP');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF)
VALUES ('session.inactive.max_time','USER','Tiempo max de  inactividad de la session(en segundos) ','2','2400','APP');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF)
VALUES ('tiempo.vigencia.rcode','USER','Tiempo de validez que tendra el codigo de registro de clientes (en dias)','2','1','APP');

INSERT INTO GE_TCNFG (CNFG_CNFG,CNFG_CATEGORIA,CNFG_DESCRI,CNFG_TIPO_DATO,CNFG_VALOR,CNFG_CLASIF)
VALUES ('tiempo.vigencia.acode','USER','Tiempo de validez que tendra el codigo de acceso de players (en meses)','2','2','APP');


