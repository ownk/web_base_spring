-- INSERTING into AUT_TUSER
Insert into AUT_TUSER 
(
USER_USER     ,
USER_NAME     ,
USER_NICK     ,
USER_PASS     ,
USER_EMAIL    ,
USER_STATE    ,
USER_DCREA    ,
USER_HASH_NICK,
USER_HASH_EMAIL,
USER_FLAG_EMAIL,
USER_FLAG_TECON
)
values (
'0',
pgp_sym_encrypt('Administrador OWNK','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt(UPPER('admin.ownk'),'JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('$2a$10$FkqDa98z4zRUWSW0q/duDOhtn0DqIbmTwn3lG2MhMeNLhCco4I2QG','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('infraestructura@ownk.co','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
'ACT',
'2019-01-27',
md5(upper('admin.ownk')),
md5(upper('infraestructura@ownk.co')),
'VAL',
'VAL'
);

Insert into AUT_TUSER 
(
USER_USER     ,
USER_NAME     ,
USER_NICK     ,
USER_PASS     ,
USER_EMAIL    ,
USER_STATE    ,
USER_DCREA    ,
USER_HASH_NICK,
USER_HASH_EMAIL,
USER_FLAG_EMAIL,
USER_FLAG_TECON
)
values (
'1',
pgp_sym_encrypt('Administrador HackerGame','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt(UPPER('admin.hackergame'),'JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('$2a$10$FkqDa98z4zRUWSW0q/duDOhtn0DqIbmTwn3lG2MhMeNLhCco4I2QG','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('gustavo@hackergame.co','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
'ACT',
'2019-11-04',
md5(upper('admin.hackergame')),
md5(upper('gustavo@hackergame.co')),
'VAL',
'VAL'
);


Insert into AUT_TUSER 
(
USER_USER     ,
USER_NAME     ,
USER_NICK     ,
USER_PASS     ,
USER_EMAIL    ,
USER_STATE    ,
USER_DCREA    ,
USER_HASH_NICK,
USER_HASH_EMAIL,
USER_FLAG_EMAIL,
USER_FLAG_TECON
)
values (
'2',
pgp_sym_encrypt('Cliente HackerGame','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt(UPPER('cliente.hackergame'),'JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('$2a$10$FkqDa98z4zRUWSW0q/duDOhtn0DqIbmTwn3lG2MhMeNLhCco4I2QG','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('german@hackergame.co','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
'ACT',
'2019-11-04',
md5(upper('cliente.hackergame')),
md5(upper('german@hackergame.co')),
'VAL',
'VAL'
);

Insert into AUT_TUSER 
(
USER_USER     ,
USER_NAME     ,
USER_NICK     ,
USER_PASS     ,
USER_EMAIL    ,
USER_STATE    ,
USER_DCREA    ,
USER_HASH_NICK,
USER_HASH_EMAIL,
USER_FLAG_EMAIL,
USER_FLAG_TECON
)
values (
'3',
pgp_sym_encrypt('Jugador Test','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt(UPPER('jugador.test'),'JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('$2a$10$FkqDa98z4zRUWSW0q/duDOhtn0DqIbmTwn3lG2MhMeNLhCco4I2QG','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
pgp_sym_encrypt('juan.jerez@ownk.co','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
'ACT',
'2019-11-04',
md5(upper('jugador.test')),
md5(upper('juan.jerez@ownk.co')),
'VAL',
'VAL'
);

