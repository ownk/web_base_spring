/*
Identificador: Nombre del Avatar
Identificador_Personaje: Es el identificador que se ha creado en el script 99_GMS_TCHRT para este personaje
Imagen_avatar: imagen del avatar
*/

Insert into GMS_TAVATAR
(
AVATAR_AVATAR,
AVATAR_NAME,
AVATAR_DESCRI,
AVATAR_SKIN_URL,
AVATAR_CHRT
)
values (
'identificador',
'GMS_TAVATAR.AVATAR_NAME.identificador',
'GMS_TAVATAR.AVATAR_DESCRI.identificador',
'/app_resources/public/commons/img/avatars/Imagen_avatar',
'Identificador_Personaje'
);