/*
Identificador: Nombre de  la nave, o en su defecto un numero desde el 9
Identificador_Personaje: Es el identificador que se ha creado en el script 99_GMS_TCHRT para este personaje
Imagen_ship: imagen de la nave
*/
Insert into GMS_TSHIP
(
SHIP_SHIP,
SHIP_NAME,
SHIP_DESCRI,
SHIP_SKIN_URL,
SHIP_CHRT
)
values (
'Identificador',
'GMS_TSHIP.SHIP_NAME.Identificador',
'GMS_TSHIP.SHIP_DESCRI.Identificador',
'/app_resources/public/commons/img/ships/imagen_ship',
'Identificador_Personaje'
);