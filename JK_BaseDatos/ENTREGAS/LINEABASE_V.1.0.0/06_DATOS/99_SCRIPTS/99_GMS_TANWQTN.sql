/*
Identificador_pregunta: Sera el identificador que se ha creado anteriormente en la tabla GMS_TQUESTN
Identificador_respuesta: Sera el identificador que se ha creado anteriormente en la tabla GMS_TANSWER
*/

Insert into GMS_TANWQTN
(
ANWQTN_ANSWER,
ANWQTN_QUESTN
)
values (
'Identificador_pregunta',
'Identificador_respuesta'
);