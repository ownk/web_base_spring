/*
El indicativo se obtiene a traves de la siguiente consulta:

select cast(uuid_generate_v4() as varchar);
*/

Insert Into INT_TPARTNR(
				PARTNR_PARTNR,
				PARTNR_NAME,
				PARTNR_SITEKEY,
				PARTNR_ACCESSKEY,
				PARTNR_STATE
		)VALUES(
				'Indicativo',
				pgp_sym_encrypt('NombrePartner', 'JHBD_PGCrypto_RSA_:hX3*','compress-algo=1, cipher-algo=aes256'),
				pgp_sym_encrypt('SiteKeyPartner', 'JHBD_PGCrypto_RSA_:hX3*','compress-algo=1, cipher-algo=aes256'),
				pgp_sym_encrypt('AccessKeyPartner', 'JHBD_PGCrypto_RSA_:hX3*','compress-algo=1, cipher-algo=aes256'),
				'ACT'
		);
