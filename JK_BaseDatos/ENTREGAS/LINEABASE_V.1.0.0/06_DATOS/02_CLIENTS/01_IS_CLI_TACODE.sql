-- INSERTING into CLI_TACODE
Insert into CLI_TACODE 
(
acode_acode,
acode_code,
acode_state,
acode_client,
acode_templt,
acode_date_start,
acode_date_validity
)
values (
'1',
pgp_sym_encrypt('cd7d02d2-3e76-4f7b-ownk1-cb97e36b87ba','JHBD_PGCrypto_RSA_:hX3*', 'compress-algo=1, cipher-algo=aes256'),
'USE',
'1',
'1',
'2019/10/17 10:00:00.59',
'2022/12/31 10:00:00.59'
);

