-- Table: ge_tprcs

CREATE TABLE ge_tprcs
(
    prcs_prcs   VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    prcs_descri VARCHAR(512) COLLATE pg_catalog."default" NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_tprcs
    
COMMENT ON TABLE ge_tprcs
    IS 'TABLA QUE CONTINE LOS PROCESO QUE REALIZA LA APLICACION';

COMMENT ON COLUMN ge_tprcs.prcs_prcs
    IS 'IDENTIFICADOR UNICO DE PROCESO';

COMMENT ON COLUMN ge_tprcs.prcs_descri
    IS 'DESCRIPCION GENERAL DEL PROCESO';
    
--
-- Name: ge_tprcs pk_ge_tprcs; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY ge_tprcs
    ADD CONSTRAINT pk_ge_tprcs PRIMARY KEY (prcs_prcs);