-- Table: ge_tlogt

CREATE TABLE ge_tlogt
(
    logt_logt       INTEGER NOT NULL,
    logt_valor      TEXT COLLATE pg_catalog."default",
    logt_fecha      TIMESTAMP
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_tlogt
    
    
    
COMMENT ON TABLE ge_tlogt
    IS 'tabla de log de seguimiento';

COMMENT ON COLUMN ge_tlogt.logt_logt
    IS 'ID ';

COMMENT ON COLUMN ge_tlogt.logt_valor
    IS 'Valor';
    
COMMENT ON COLUMN ge_tlogt.logt_fecha
    IS 'FECHA DE CREACION';