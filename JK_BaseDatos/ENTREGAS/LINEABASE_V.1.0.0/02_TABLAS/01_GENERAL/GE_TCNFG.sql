-- ***************************************************
-- **          PROYECTO WEB BASE                    **
-- **          REV:26\01\2019                       ** 
-- ***************************************************

-- Table: ge_tcnfg

CREATE TABLE ge_tcnfg
(
    cnfg_cnfg      VARCHAR(128)    COLLATE pg_catalog."default" NOT NULL,
    cnfg_categoria VARCHAR(50)     COLLATE pg_catalog."default" NOT NULL,
    cnfg_descri    TEXT            COLLATE pg_catalog."default" NOT NULL,
    cnfg_tipo_dato INTEGER         NOT NULL,
    cnfg_valor     TEXT            COLLATE pg_catalog."default" NOT NULL,
    cnfg_expresion TEXT            COLLATE pg_catalog."default",
    cnfg_clasif    TEXT            COLLATE pg_catalog."default",
    CONSTRAINT ch_ge_tcnfg_tipo_dato CHECK (cnfg_tipo_dato = ANY (ARRAY[1::INTEGER, 2::INTEGER, 3::INTEGER]))
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_tcnfg
    
COMMENT ON TABLE ge_tcnfg
    IS 'TABLA QUE ALMACENA LAS VARIABLES GENERALES DEL SISTEMA';

COMMENT ON COLUMN ge_tcnfg.cnfg_cnfg
    IS 'IDENTIFICADOR UNICO DE LA ACTIVIDAD';

COMMENT ON COLUMN ge_tcnfg.cnfg_categoria
    IS 'CATEGORIA DE LA CONFIGURACIÓN';

COMMENT ON COLUMN ge_tcnfg.cnfg_descri
    IS 'DESCRIPCION DE LA CONFIGURACIĂ“N';

COMMENT ON COLUMN ge_tcnfg.cnfg_tipo_dato
    IS 'TIPO DE DATO. 1-NUMERICO, 2-STRING, 3-BOOLEAN';

COMMENT ON COLUMN ge_tcnfg.cnfg_valor
    IS 'VALOR DE LA CONFGIURACIÓN';
    
COMMENT ON COLUMN ge_tcnfg.cnfg_expresion
    IS 'EXPRESION REGULAR DE LA CONFGIURACIÓN';
    
    COMMENT ON COLUMN ge_tcnfg.cnfg_clasif
    IS 'CLASIFICADOR DE LA CONFGIURACIÓN';

--
-- Name: ge_tcnfg pk_ge_tcnfg; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY ge_tcnfg
    ADD CONSTRAINT pk_ge_tcnfg PRIMARY KEY (cnfg_cnfg);