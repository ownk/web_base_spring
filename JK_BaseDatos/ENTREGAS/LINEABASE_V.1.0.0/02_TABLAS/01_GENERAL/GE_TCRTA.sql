-- Table: ge_tcrta

CREATE TABLE ge_tcrta
(
    crta_crta       VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    crta_prcs       TEXT COLLATE pg_catalog."default" NOT NULL,
    crta_http_code  INTEGER NOT NULL,
    crta_descri     VARCHAR(256) COLLATE pg_catalog."default" NOT NULL,
    crta_idioma     VARCHAR(10) COLLATE pg_catalog."default" NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_tcrta
    
COMMENT ON TABLE ge_tcrta
    IS 'Codigos de respuesta utilizados por los servicios de integracion';

COMMENT ON COLUMN ge_tcrta.CRTA_CRTA
    IS 'Identificador interno para codigo de error';

COMMENT ON COLUMN ge_tcrta.CRTA_PRCS
    IS 'Proceso de integracion al cual esta asociado el codigo de respuesta';

COMMENT ON COLUMN ge_tcrta.CRTA_HTTP_CODE
    IS 'Codigo http que se retorna como respuesta. Ver codificacion estandar http 2XX-Peticiones correctas, 3XX-Redirecciones, 4XX-Errores de usuario, 5XX-Errores de servidor';

COMMENT ON COLUMN ge_tcrta.CRTA_DESCRI
    IS 'Descripcion funcional de codigo de respuesta. ';

COMMENT ON COLUMN ge_tcrta.crta_idioma
    IS 'Codigos de Paises e idiomas I18N';

--
-- Name: ge_tcrta pk_ge_tcrta; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY ge_tcrta
    ADD CONSTRAINT pk_ge_tcrta PRIMARY KEY (crta_crta, crta_prcs, crta_idioma);