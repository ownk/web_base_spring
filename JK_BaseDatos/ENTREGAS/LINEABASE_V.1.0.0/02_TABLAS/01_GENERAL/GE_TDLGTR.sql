-- Table: ge_tdlgtr

CREATE TABLE ge_tdlgtr
(
    dlgtr_dlgtr     VARCHAR(100)    COLLATE pg_catalog."default" NOT NULL,
    dlgtr_lgtr      VARCHAR(100)    COLLATE pg_catalog."default" NOT NULL,
    dlgtr_feccre    timestamp       NOT NULL,
    dlgtr_prcs      VARCHAR(100)    COLLATE pg_catalog."default" NOT NULL,
    dlgtr_crta      VARCHAR(100)    COLLATE pg_catalog."default" NOT NULL,
    dlgtr_observ    VARCHAR(1000)   COLLATE pg_catalog."default" NOT NULL,
    dlgtr_error     VARCHAR(4000)   COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GE_tdlgtr
    
COMMENT ON TABLE ge_tdlgtr
    IS 'Detalle de Log de transacciones de integraciones SIFI-SAF';

COMMENT ON COLUMN ge_tdlgtr.dlgtr_dlgtr
    IS 'Identificador unico del detalle de log';

COMMENT ON COLUMN ge_tdlgtr.dlgtr_lgtr
    IS 'Log de transaccion al que pertenece el detalle';

COMMENT ON COLUMN ge_tdlgtr.dlgtr_feccre
    IS 'Fecha de creacion del detalle';

COMMENT ON COLUMN ge_tdlgtr.dlgtr_prcs
    IS 'Proceso asociado a la transaccion. Ver tabla IS_TPRCS';

COMMENT ON COLUMN ge_tdlgtr.dlgtr_crta
    IS 'Codigo de respuesta que ocurre durante la transaccion. Ver tabnla IS_TCRTA';

COMMENT ON COLUMN ge_tdlgtr.dlgtr_observ
    IS 'Observacion referente a la operacion realizada';

COMMENT ON COLUMN ge_tdlgtr.dlgtr_error
    IS 'Descripcion error generado en el proceso';
    
--
-- Name: ge_tdlgtr pk_ge_tdlgtr; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY ge_tdlgtr
    ADD CONSTRAINT pk_ge_tdlgtr PRIMARY KEY (dlgtr_dlgtr);