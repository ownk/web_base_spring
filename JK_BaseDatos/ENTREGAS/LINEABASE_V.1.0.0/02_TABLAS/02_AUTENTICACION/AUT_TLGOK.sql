/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 13.5         */
/*  Created On : 22-Ene.-2020 04:30:00 p. m.             */
/*  DBMS       : PostgreSQL                             */
/* ---------------------------------------------------- */


/* Create Tables */

CREATE TABLE AUT_TLGOK  
(
    LGOK_LGOK varchar(50)  NOT NULL,        -- Identificador de LGOK
    LGOK_USER varchar(50)  NOT NULL,        -- Identificador del usuario
    LGOK_BROWSER text      NOT NULL,        -- Navegador donde realiza LGOK  
    LGOK_IP text           NOT NULL,        -- IP del computador donde realiza LGOK
    LGOK_SESSION text      NOT NULL         -- IP del computador donde realiza LGOK
)
TABLESPACE pg_default;

--ALTER TABLE AUT_TLGOK
    

/* Create Primary Keys, Indexes, Uniques, Checks */

 ALTER TABLE ONLY AUT_TLGOK ADD CONSTRAINT PK_AUT_TLGOK
    PRIMARY KEY (LGOK_LGOK)
;

CREATE INDEX IXFK_AUT_TLGOK_AUT_TUSER
    ON AUT_TLGOK USING btree
    (LGOK_USER)
    TABLESPACE pg_default;

/* Create Table Comments, Sequences for Autonumber Columns */

COMMENT ON TABLE AUT_TLGOK
    IS 'Tabla con informacion de LGOK'
;

COMMENT ON COLUMN AUT_TLGOK.LGOK_LGOK
    IS 'Identificador de LGOK'
;

COMMENT ON COLUMN AUT_TLGOK.LGOK_USER
    IS 'Identificador del Usuario'
;

COMMENT ON COLUMN AUT_TLGOK.LGOK_BROWSER
    IS 'Navegador donde realiza LGOK'
;

COMMENT ON COLUMN AUT_TLGOK.LGOK_IP
    IS 'IP del computador donde realiza LGOK'
;

COMMENT ON COLUMN AUT_TLGOK.LGOK_SESSION
    IS 'Session del computador donde realiza LGOK'
;