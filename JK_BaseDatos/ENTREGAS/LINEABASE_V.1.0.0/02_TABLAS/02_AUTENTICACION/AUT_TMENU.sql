-- Table: aut_tmenu

CREATE TABLE aut_tmenu
(
    menu_menu   INTEGER NOT NULL,
    menu_orden  INTEGER NOT NULL,
    menu_nombre VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    menu_descri VARCHAR(512) COLLATE pg_catalog."default" NOT NULL,
    menu_icon   VARCHAR(100) COLLATE pg_catalog."default" NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE aut_tmenu
    
    
COMMENT ON TABLE aut_tmenu
    IS 'TABLA QUE ALMACENA LOS MENUS DE LA APP';

COMMENT ON COLUMN aut_tmenu.menu_menu
    IS 'ID DEL GRUPO DE MODULOS EN EL SISTEMA';

COMMENT ON COLUMN aut_tmenu.menu_orden
    IS 'ORDEN DEL GRUPO DE MODILOS DENTRO DEL SISTEM';

COMMENT ON COLUMN aut_tmenu.menu_nombre
    IS 'NOMBRE DEL GRUPO DE MODULOS';

COMMENT ON COLUMN aut_tmenu.menu_descri
    IS 'OBSERVACION DE AYUDA';

COMMENT ON COLUMN aut_tmenu.menu_icon
    IS 'NOMBRE DE ICON FONT ICON';
    
--
-- Name: aut_tmenu pk_aut_tmenu; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY aut_tmenu
    ADD CONSTRAINT pk_aut_tmenu PRIMARY KEY (menu_menu);