-- Table: aut_tmdlo

CREATE TABLE aut_tmdlo
(
    mdlo_mdlo       VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    mdlo_descri     VARCHAR(200) COLLATE pg_catalog."default" NOT NULL,
    mdlo_nomb       VARCHAR(100) COLLATE pg_catalog."default" NOT NULL,
    mdlo_orden      INTEGER NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE aut_tmdlo
    
COMMENT ON TABLE aut_tmdlo
    IS 'MODULOS DEL SISTEMA';

COMMENT ON COLUMN aut_tmdlo.mdlo_mdlo
    IS 'IDENTIFICADOR MODULO';

COMMENT ON COLUMN aut_tmdlo.mdlo_descri
    IS 'DESCRIPCION DEL MODULO';

COMMENT ON COLUMN aut_tmdlo.mdlo_nomb
    IS 'NOMBRE DEL MODULO';

COMMENT ON COLUMN aut_tmdlo.mdlo_orden
    IS 'ORDEN QUE TENDRA EL MODULO';
    
    --
-- Name: aut_tmdlo pk_aut_tmdlo; Type: CONSTRAINT; Schema: juegohacker_us; Owner: juegohacker_us
--

 ALTER TABLE ONLY aut_tmdlo
    ADD CONSTRAINT pk_aut_tmdlo PRIMARY KEY (mdlo_mdlo);