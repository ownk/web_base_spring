/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 13.5 		*/
/*  Created On : 11-oct.-2019 11:45:28 a. m. 				*/
/*  DBMS       : PostgreSQL 						*/
/* ---------------------------------------------------- */

/* Create Tables */

CREATE TABLE AUT_TSTUS
(
	STUS_USER varchar(50) COLLATE pg_catalog."default" NOT NULL,    		-- Identificador de Usuario
	STUS_STATE_UPDATE varchar(50) COLLATE pg_catalog."default" NOT NULL,   	-- Estado al cual se modifico el usuario
	STUS_DATE_UPDATE timestamp NOT NULL    									-- Feecha de actualizacion de estado

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE AUT_TSTUS
    

/* Create Primary Keys, Indexes, Uniques, Checks */

CREATE INDEX IXFK_AUT_TLOGRL_AUT_TUSER
    ON AUT_TSTUS USING btree
    (STUS_USER)
    TABLESPACE pg_default;

/* Create Table Comments, Sequences for Autonumber Columns */

COMMENT ON TABLE AUT_TSTUS
	IS 'Tabla de log para cambios de estado del usuario'
;

COMMENT ON COLUMN AUT_TSTUS.STUS_USER
	IS 'Identificador de Usuario'
;

COMMENT ON COLUMN AUT_TSTUS.STUS_STATE_UPDATE
	IS 'Estado al cual se modifico el usuario'
;

COMMENT ON COLUMN AUT_TSTUS.STUS_DATE_UPDATE
	IS 'Fecha de actualizacion de estado'
;



