/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 13.5 		*/
/*  Created On : 15-oct.-2019 11:09:18 a. m. 				*/
/*  DBMS       : PostgreSQL 						*/
/* ---------------------------------------------------- */


/* Create Tables */

CREATE TABLE ACN_TACAC
(
	ACAC_ACODE varchar(50) COLLATE pg_catalog."default" NOT NULL,   -- Identificador de Codigo
	ACAC_ACNT varchar(50) COLLATE pg_catalog."default" NOT NULL    	-- Identificador de Cuenta
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE ACN_TACAC
    

/* Create Primary Keys, Indexes, Uniques, Checks */

CREATE INDEX IXFK_ACN_TACAC_ACN_TACNT
    ON ACN_TACAC USING btree
    (ACAC_ACNT)
    TABLESPACE pg_default;

CREATE INDEX IXFK_ACN_TACAC_CLI_TACODE
    ON ACN_TACAC USING btree
    (ACAC_ACODE)
    TABLESPACE pg_default;



/* Create Table Comments, Sequences for Autonumber Columns */

COMMENT ON TABLE ACN_TACAC
	IS 'Tabla para relacionar los codigos de acceso por cuenta'
;

COMMENT ON COLUMN ACN_TACAC.ACAC_ACODE
	IS 'Identificador de Codigo'
;

COMMENT ON COLUMN ACN_TACAC.ACAC_ACNT
	IS 'Identificador de Cuenta'
;
