/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 13.5 		*/
/*  Created On : 11-oct.-2019 11:49:24 a. m. 				*/
/*  DBMS       : PostgreSQL 						*/
/* ---------------------------------------------------- */


/* Create Tables */

CREATE TABLE GMS_TTPGAME
(
	TPGAME_TPGAME varchar(50) COLLATE pg_catalog."default"	NOT NULL,   -- Identificador del Tipo de Juego
	TPGAME_DESCRI varchar(100) COLLATE pg_catalog."default"	NOT NULL   	-- Descripcion del Tipo de Juego
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GMS_TTPGAME
    

/* Create Primary Keys, Indexes, Uniques, Checks */

 ALTER TABLE ONLY GMS_TTPGAME ADD CONSTRAINT PK_APP_TTPGAME
	PRIMARY KEY (TPGAME_TPGAME)
;

/* Create Table Comments, Sequences for Autonumber Columns */

COMMENT ON TABLE GMS_TTPGAME
	IS 'Tabla con Informacion de Tipos de Juego'
;

COMMENT ON COLUMN GMS_TTPGAME.TPGAME_TPGAME
	IS 'Identificador del Tipo de Juego'
;

COMMENT ON COLUMN GMS_TTPGAME.TPGAME_DESCRI
	IS 'Descripcion del Tipo de Juego'
;
