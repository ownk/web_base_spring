/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 13.5 		*/
/*  Created On : 11-oct.-2019 11:50:19 a. m. 				*/
/*  DBMS       : PostgreSQL 						*/
/* ---------------------------------------------------- */


/* Create Tables */

CREATE TABLE GMS_TCNVATB
(
	CNVATB_GAME varchar(50) COLLATE pg_catalog."default" NOT NULL,          -- Identificador del juego
	CNVATB_ATRB varchar(50) COLLATE pg_catalog."default" NOT NULL,          -- Identificador del atributo
	CNVATB_CHRT varchar(50) COLLATE pg_catalog."default" NOT NULL,          -- Identificador del caracter
	CNVATB_ATRB_APP NUMERIC NOT NULL,    						            -- Valor del atributo en la plataforma
	CNVATB_ATRB_GAME NUMERIC NOT NULL,       					            -- Valor del atributo en el juego
    CNVATB_DATECR timestamp NOT NULL    							        -- Fecha de creacion 

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE GMS_TCNVATB
    

/* Create Primary Keys, Indexes, Uniques, Checks */

CREATE INDEX IXFK_GMS_TCNVATB_GMS_TGAME
    ON GMS_TCNVATB USING btree
    (CNVATB_GAME)
    TABLESPACE pg_default;

CREATE INDEX IXFK_GMS_TCNVATB_GMS_TATRB
    ON GMS_TCNVATB USING btree
    (CNVATB_ATRB)
    TABLESPACE pg_default;
    
CREATE INDEX IXFK_GMS_TCNVATB_GMS_TCHRT
    ON GMS_TCNVATB USING btree
    (CNVATB_CHRT)
    TABLESPACE pg_default;

/* Create Table Comments, Sequences for Autonumber Columns */

COMMENT ON TABLE GMS_TCNVATB
	IS 'Tabla de conversion de puntajes del atributos a la plataforma'
;

COMMENT ON COLUMN GMS_TCNVATB.CNVATB_GAME
	IS 'Identificador del juego'
;

COMMENT ON COLUMN GMS_TCNVATB.CNVATB_ATRB
	IS 'Identificador del atributo'
;

COMMENT ON COLUMN GMS_TCNVATB.CNVATB_CHRT
	IS 'Identificador del caracter'
;

COMMENT ON COLUMN GMS_TCNVATB.CNVATB_ATRB_APP
	IS 'Valor del atributo en el juego'
;

COMMENT ON COLUMN GMS_TCNVATB.CNVATB_ATRB_GAME
	IS 'Valor del atributo  en el juego'
;

COMMENT ON COLUMN GMS_TCNVATB.CNVATB_DATECR
	IS 'Fecha de creacion'
;

