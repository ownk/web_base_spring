-- ***************************************************
-- **          PROYECTO WEB BASE                    **
-- **          REV:26\01\2019                       ** 
-- ***************************************************

-- USUARIO DEL APLICATIVO 
-- User: juegohacker_us

CREATE USER juegohacker_us WITH
  LOGIN
  ENCRYPTED PASSWORD 'juegohacker_us'
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;

