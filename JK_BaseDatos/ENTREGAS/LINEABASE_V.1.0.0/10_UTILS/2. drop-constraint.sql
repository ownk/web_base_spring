-- ------------ Write DROP-CONSTRAINT-stage scripts -----------

ALTER TABLE aut_tinlog DROP CONSTRAINT pk_aut_tinlog;



ALTER TABLE aut_tmenu DROP CONSTRAINT pk_aut_tmenu;



ALTER TABLE aut_tmsrv DROP CONSTRAINT pk_aut_tmsrv;



ALTER TABLE aut_tmdlo DROP CONSTRAINT pk_aut_tmdlo;



ALTER TABLE AUT_TPRSN DROP CONSTRAINT pk_aut_tprsn;



ALTER TABLE AUT_TROL DROP CONSTRAINT pk_aut_trol;



ALTER TABLE aut_trsin DROP CONSTRAINT pk_aut_trsin;



ALTER TABLE AUT_TSRVC DROP CONSTRAINT ch_aut_tsrvc_tipo;



ALTER TABLE AUT_TSRVC DROP CONSTRAINT pk_aut_tsrvc;



ALTER TABLE AUT_TSROL DROP CONSTRAINT ch_pi_taut_tsrol_visible;



ALTER TABLE AUT_TSROL DROP CONSTRAINT pk_aut_tsrol;



ALTER TABLE aut_tsurl DROP CONSTRAINT ch_aut_tsurl_tipo;



ALTER TABLE aut_tsurl DROP CONSTRAINT pk_aut_tsurl;



ALTER TABLE AUT_TTOKN DROP CONSTRAINT pk_aut_ttokn;



ALTER TABLE AUT_TUROL DROP CONSTRAINT pk_aut_turol;



ALTER TABLE AUT_TUSER DROP CONSTRAINT pk_aut_tuser;



ALTER TABLE ge_tcder DROP CONSTRAINT pk_ge_tcder;



ALTER TABLE GE_TCIUD DROP CONSTRAINT pk_ge_tciud;



ALTER TABLE ge_tconfig DROP CONSTRAINT ch_ge_tconfig_tipo_dato;



ALTER TABLE ge_tconfig DROP CONSTRAINT pk_ge_tconfig;



ALTER TABLE ge_tcrta DROP CONSTRAINT pk_ge_tcrta;



ALTER TABLE GE_TDPTO DROP CONSTRAINT pk_ge_tdpto;



ALTER TABLE ge_tdlgtr DROP CONSTRAINT pk_ge_tdlgtr;



ALTER TABLE ge_tlgtr DROP CONSTRAINT pk_ge_tlgtr;



ALTER TABLE ge_tlogt DROP CONSTRAINT pk_ge_tlogt;



ALTER TABLE GE_TPAIS DROP CONSTRAINT pk_ge_tPAIS_PAIS;



ALTER TABLE ge_tprcs DROP CONSTRAINT pk_ge_tprcs;



ALTER TABLE ge_ttdoc DROP CONSTRAINT pk_ge_ttdoc;



ALTER TABLE ge_ttper DROP CONSTRAINT pk_ge_ttper;



