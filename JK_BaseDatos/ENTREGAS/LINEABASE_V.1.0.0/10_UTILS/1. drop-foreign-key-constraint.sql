-- ------------ Write DROP-FOREIGN-KEY-CONSTRAINT-stage scripts -----------

ALTER TABLE aut_tmsrv DROP CONSTRAINT fk_aut_tmsrv_aut_tmenu;



ALTER TABLE aut_tmsrv DROP CONSTRAINT fk_aut_tmsrv_aut_tsrvc;



ALTER TABLE AUT_TPRSN DROP CONSTRAINT fk_aut_tprsn_ge_tciud;



ALTER TABLE aut_trsin DROP CONSTRAINT fk_aut_trsin_aut_trol;



ALTER TABLE aut_trsin DROP CONSTRAINT fk_aut_trsin_aut_tsrvc;



ALTER TABLE AUT_TSRVC DROP CONSTRAINT fk_aut_tsrvc_aut_tmdlo;



ALTER TABLE AUT_TSROL DROP CONSTRAINT fk_aut_tsrol_aut_trol;



ALTER TABLE AUT_TSROL DROP CONSTRAINT fk_aut_tsrol_aut_tsrvc;



ALTER TABLE aut_tsurl DROP CONSTRAINT fk_aut_tsurl_aut_tsrvc;



ALTER TABLE AUT_TTOKN DROP CONSTRAINT fk_aut_ttokn_aut_tuser;



ALTER TABLE AUT_TUROL DROP CONSTRAINT fk_aut_turol_aut_trol;



ALTER TABLE AUT_TUROL DROP CONSTRAINT fk_aut_turol_aut_tuser;



ALTER TABLE AUT_TUSER DROP CONSTRAINT fk_aut_tuser_aut_tprsn;



ALTER TABLE GE_TCIUD DROP CONSTRAINT fk_ge_tciud_ge_tdpto;



ALTER TABLE ge_tcrta DROP CONSTRAINT fk_ge_tcrta_ge_tprcs;



ALTER TABLE GE_TDPTO DROP CONSTRAINT fk_ge_tdpto_ge_tpais;



ALTER TABLE ge_ttdoc DROP CONSTRAINT fk_ge_ttdoc_ge_ttper;



