--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_SCORE AS (
	SCORE_SCORE				TEXT,
    SCORE_MATCH  			TEXT,
    SCORE_CHRT 				TEXT,
    SCORE_ATRB				TEXT,
    SCORE_VALUE 			NUMERIC
);

--ALTER TYPE TYPE_TO_GM_SCORE OWNER TO juegohacker_us;