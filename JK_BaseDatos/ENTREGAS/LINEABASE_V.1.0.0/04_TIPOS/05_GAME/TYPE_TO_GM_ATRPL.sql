--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_ATRPL AS (
	ATRPL_PLAYER	TEXT,
    ATRPL_CHRT   	TEXT,    
    ATRPL_ATRB  	TEXT,
	ATRPL_VALUE		NUMERIC,
	ATRB_DESCRI		TEXT,
	ATRB_NAME		TEXT,
	ATRB_ICON		TEXT,
	ATRB_COLOR		TEXT
);

--ALTER TYPE TYPE_TO_GM_ATRPL OWNER TO juegohacker_us;