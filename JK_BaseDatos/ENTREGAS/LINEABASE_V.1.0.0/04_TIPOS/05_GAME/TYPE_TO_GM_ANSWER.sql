--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GM_ANSWER AS (
    ANSWER_ANSWER  			TEXT,
	ANSWER_DESCRI			TEXT,
	ANSWER_VALUE			TEXT,
	ANSWER_ACATEG			TEXT
);

--ALTER TYPE TYPE_TO_GM_ANSWER OWNER TO juegohacker_us;