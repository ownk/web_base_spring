--
-- Name: TYPE_TO_GE_ERRO; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GE_ERRO AS (
	FECCRE  TIMESTAMP,
	PRCS    VARCHAR(100),
	CODIGO  VARCHAR(100),
	MENSAJE TEXT
);

--ALTER TYPE TYPE_TO_GE_ERRO OWNER TO juegohacker_us;