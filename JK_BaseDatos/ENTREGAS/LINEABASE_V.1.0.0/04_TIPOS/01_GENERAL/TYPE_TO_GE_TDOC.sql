--
-- Name: type_to_ge_tdoc; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_GE_TDOC AS (
	TDOC_TDOC VARCHAR(3),
	TDOC_DOC VARCHAR(64),
	TDOC_TPER VARCHAR(3)
);

--ALTER TYPE TYPE_TO_GE_TDOC OWNER TO juegohacker_us;