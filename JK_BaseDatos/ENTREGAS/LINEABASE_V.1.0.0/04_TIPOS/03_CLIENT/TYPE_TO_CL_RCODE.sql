--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_RCODE AS (
	RCODE_RCODE				TEXT,
    RCODE_CODE   			TEXT,    
    RCODE_STATE    			TEXT,
	RCODE_CLIENT       		TEXT,
	RCODE_DATE_START      	TIMESTAMP,
	RCODE_DATE_VALIDITY     TIMESTAMP,
	RCODE_DATE_USED			TIMESTAMP
);

--ALTER TYPE TYPE_TO_CL_RCODE OWNER TO juegohacker_us;