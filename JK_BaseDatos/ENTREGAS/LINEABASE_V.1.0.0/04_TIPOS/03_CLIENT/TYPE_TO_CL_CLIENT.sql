--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_CLIENT AS (
	CLIENT_CLIENT		TEXT,
    CLIENT_TPIDENT   	TEXT,    
    CLIENT_NUMIDENT    	TEXT,
	CLIENT_NAME       	TEXT,
	CLIENT_STATE      	TEXT,
	CLIENT_IMAGE	    TEXT,
	CLIENT_DATECR      	TIMESTAMP,
	CLIENT_STATE_DESC 	TEXT

);

--ALTER TYPE TYPE_TO_CL_CLIENT OWNER TO juegohacker_us;