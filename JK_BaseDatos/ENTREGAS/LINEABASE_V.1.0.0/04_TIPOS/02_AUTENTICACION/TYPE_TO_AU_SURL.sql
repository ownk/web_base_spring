--
-- Name: TYPE_TO_AU_SURL; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_SURL AS (
	surl_url        TEXT,
    surl_srvc       INTEGER,    
    surl_tipo       TEXT
);

--ALTER TYPE TYPE_TO_AU_SURL OWNER TO juegohacker_us;