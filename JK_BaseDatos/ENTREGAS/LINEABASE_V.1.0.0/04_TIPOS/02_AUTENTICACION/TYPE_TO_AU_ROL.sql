--
-- Name: type_to_au_rol; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_ROL AS (
	ROL_ROL         VARCHAR(20),
	ROL_DESCRI      VARCHAR(100),
	ROL_IS_SISTEMA  VARCHAR(10)
);

--ALTER TYPE TYPE_TO_AU_ROL OWNER TO juegohacker_us;