--
-- Name: type_to_au_menu; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_MENU AS (
	MENU_MENU   VARCHAR(100),
	MENU_ORDEN  VARCHAR(50),
	MENU_NOMBRE VARCHAR(100),
	MENU_DESCRI VARCHAR(1000),
	MENU_ICON   VARCHAR(100)
);

--ALTER TYPE TYPE_TO_AU_MENU OWNER TO juegohacker_us;
