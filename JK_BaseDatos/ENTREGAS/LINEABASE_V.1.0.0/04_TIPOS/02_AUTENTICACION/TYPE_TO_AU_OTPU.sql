--
-- Name: type_to_au_user; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_AU_OTPU AS (
	OTPU_OTPU     	TEXT,
    OTPU_OTP      	TEXT,    
    OTPU_DATE_STAR  TIMESTAMP,
	OTPU_DATE_END   TIMESTAMP,
	OTPU_DATE_USED  TIMESTAMP,
	OTPU_STATE   	TEXT,
    OTPU_USER       TEXT
);

