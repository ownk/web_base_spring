-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             22/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "acn_qacnt$validateacnt"(
    p_acnt_acnt text
    )
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE
    v_date TIMESTAMP;
    v_acnt_valid NUMERIC;
	v_acode VARCHAR(50);

BEGIN
    v_date := CURRENT_TIMESTAMP;

    --Obtener el codigo de acceso de la cuenta
	SELECT ACODE_ACODE
		INTO v_acode
		FROM CLI_TACODE, ACN_TACAC 
		WHERE ACODE_ACODE=ACAC_ACODE AND ACAC_ACNT=p_acnt_acnt
		ORDER BY ACODE_DATE_VALIDITY DESC
		LIMIT 1;
	
    
    SELECT

        COUNT(*)

        INTO STRICT v_acnt_valid

        FROM ACN_TACNT

        WHERE	ACNT_ACNT=p_acnt_acnt AND
				v_date<=ACNT_DATE_VALIDITY;

    IF (v_acnt_valid > 0) THEN
        
        RETURN TRUE;

    ELSE
        --Actualizacion de la cuenta a INA(Inactiva)
        UPDATE ACN_TACNT
        SET ACNT_STATE='INA'
        WHERE   ACNT_ACNT=p_acnt_acnt    AND
                ACNT_STATE='ACT';
        
		--Actualizacion del codigo de acceso a VEN(Vencido)
		
		UPDATE CLI_TACODE
        SET ACODE_STATE='VEN'
        WHERE   ACODE_ACODE=v_acode    AND
                ACODE_STATE='ACT';
		
		RETURN FALSE;

    END IF;


END;

$BODY$;

    
