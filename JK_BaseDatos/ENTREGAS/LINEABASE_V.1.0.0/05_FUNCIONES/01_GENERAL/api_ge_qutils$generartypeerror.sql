-- ***************************************************
-- **          PROYECTO NS UBP                      **
-- **          REV:01\02\2019                       ** 
-- **          CLIENTE: HackerGame                  **
-- ***************************************************

-- FUNCTION: "api_ge_qutils$generartypemensajerpta"(text, text, TYPE_TT_GE_MNSJ, text)

CREATE OR REPLACE FUNCTION "api_ge_qutils$generartypeerror"(
	p_codigo                text,
	p_prcs                  text,
	INOUT p_type_error      TYPE_TT_GE_ERRO,
	p_textoadicional        text DEFAULT ' '::text)
    RETURNS TYPE_TT_GE_ERRO
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

/* */

DECLARE

    errorObject type_to_ge_erro;

BEGIN

    errorObject := ROW (CURRENT_TIMESTAMP, p_prcs, p_codigo, CONCAT_WS('', api_ge_qutils$getmensajeporcodigo(p_codigo, p_prcs), ' ', p_textoAdicional))::TYPE_TO_GE_ERRO;
    
	p_type_error[COALESCE(array_length(p_type_error, 1), 0)] := errorObject;

END;

$BODY$;

--ALTER FUNCTION "api_ge_qutils$generartypemensajerpta"(text, text, TYPE_TT_GE_MNSJ, text)
    
