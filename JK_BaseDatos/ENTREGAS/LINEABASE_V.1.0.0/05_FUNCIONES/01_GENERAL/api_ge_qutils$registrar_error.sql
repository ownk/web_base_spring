-- ***************************************************
-- **          PROYECTO NS UBP                      **
-- **          REV:01\02\2019                       ** 
-- **          CLIENTE: NEW SAPIENS                 **
-- ***************************************************

-- FUNCTION: "api_ge_qutils$registrar_error"(TYPE_TT_GE_ERRO, text, text, text)

CREATE OR REPLACE FUNCTION "api_ge_qutils$registrar_error"(
	INOUT p_errores     TYPE_TT_GE_ERRO,
	p_prcs              text,
	p_crta              text,
	p_detalle           text DEFAULT NULL::text)
    RETURNS TYPE_TT_GE_ERRO
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_object_error TYPE_TO_GE_ERRO;

BEGIN

    v_object_error := ROW (CURRENT_TIMESTAMP, p_prcs, p_crta, p_detalle)::TYPE_TO_GE_ERRO;

    p_errores[COALESCE(array_length(p_errores, 1), 0)] := v_object_error;

END;

$BODY$;

--ALTER FUNCTION "api_ge_qutils$registrar_error"(TYPE_TT_GE_ERRO, text, text, text)
    
