-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             08/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "ge_qutils$createcnfg"
(
	p_prcs 				TEXT,
	p_tcnfg 			type_tt_ge_cnfg,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;


		

BEGIN

    
	
	FOR i IN 0..COALESCE(array_length(p_tcnfg, 1)-1, 0)
	LOOP
		--Crear Parametro de Configuracion General
		Insert Into GE_TCNFG(
				CNFG_CNFG,
				CNFG_CATEGORIA,
				CNFG_DESCRI,
				CNFG_TIPO_DATO,
				CNFG_VALOR,
                CNFG_CLASIF
		)VALUES(
				p_tcnfg[i].CNFG_CNFG,
				p_tcnfg[i].CNFG_CATEGORIA,
				p_tcnfg[i].CNFG_DESCRI,
				p_tcnfg[i].CNFG_TIPO_DATO,
				p_tcnfg[i].CNFG_VALOR,
				p_tcnfg[i].cnfg_clasif
		);
		
		
	END LOOP;
	
	
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "ge_qutils$createcnfg"(text, TYPE_TT_GE_CNFG)
    

