-- FUNCTION: "ge_qlgtr$iniciar_log"(TYPE_TT_GE_LGTR, text)

CREATE OR REPLACE FUNCTION "ge_qlgtr$iniciar_log"(
	p_ctrl_operacion TYPE_TT_GE_LGTR,
	p_prcs text,
	OUT p_cod_rpta text,
	OUT p_cod_rpta_descri text,
	OUT p_lgtr text)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$


DECLARE

    c_lgtr CURSOR FOR
    SELECT CURRENT_TIMESTAMP;

    v_lgtr GE_TLGTR.LGTR_LGTR%TYPE;
    
    c_prcs CURSOR FOR
    SELECT
        prcs_prcs
        FROM ge_tprcs
        WHERE UPPER(prcs_prcs) = UPPER(p_prcs);

    r_prcs text;

BEGIN

    OPEN c_lgtr;
    FETCH c_lgtr INTO v_lgtr;
    CLOSE c_lgtr;


    IF p_ctrl_operacion IS NOT NULL AND COALESCE(array_length(p_ctrl_operacion, 1), 0) = 1 THEN
		
	
        FOR i IN 0..COALESCE(array_length(p_ctrl_operacion, 1)-1, 0)

        /* ---------------------------------------------- */
        /* Validacion de campos de entrada                */
        /* ---------------------------------------------- */

        LOOP
                    
            IF (p_ctrl_operacion[i].LGTR_TRANS IS NULL) THEN
				
				
                p_cod_rpta := 'ER_TRANS_NU';
                p_cod_rpta_descri := 'Numero de transaccion no puede ser nulo';

				
                RETURN;

            END IF;

            IF (p_ctrl_operacion[i].LGTR_TERMINAL IS NULL) THEN

                p_cod_rpta := 'ER_TERM_NU';
                p_cod_rpta_descri := 'Numero de terminal no puede ser nulo';
				
                RETURN;

            END IF;

            IF (p_ctrl_operacion[i].LGTR_USER IS NULL) THEN

                p_cod_rpta := 'ER_USUA_NU';
                p_cod_rpta_descri := 'Usuario de transaccion no puede ser nulo';
				INSERT INTO GMS_TPET
				(PET_PET,PET_SKIN)VALUES(p_cod_rpta,p_cod_rpta_descri);
                RETURN;

            END IF;

            IF (p_ctrl_operacion[i].LGTR_FECH_OPER IS NULL) THEN

                p_cod_rpta := 'ER_FECH_NU';
                p_cod_rpta_descri := 'Fecha de operacion no puede ser nula';
				
                RETURN;

            END IF;

            IF (p_ctrl_operacion[i].LGTR_HORA_OPER IS NULL) THEN

                p_cod_rpta := 'ER_HORA_NU';
                p_cod_rpta_descri := 'Hora de operacion no puede ser nula';
				
                RETURN;

            END IF;

            IF (p_ctrl_operacion[i].LGTR_OBSERVACION IS NULL) THEN

                p_cod_rpta := 'ER_OBSERV_NU';
                p_cod_rpta_descri := 'Hora de operacion no puede ser nulo';
				
                RETURN;

            END IF;
            
            IF (p_prcs IS NULL) THEN
            
                p_cod_rpta := 'ER_PRCS_NU';
                p_cod_rpta_descri := 'Proceso de integracion no puede ser nulo';
				
                RETURN;

            ELSE
            
                OPEN c_prcs;
                FETCH c_prcs INTO r_prcs;
                CLOSE c_prcs;
                
                IF (r_prcs IS NULL) THEN

                    p_cod_rpta := 'ER_PRCS_NE';
                    p_cod_rpta_descri := 'Proceso de integracion no existe o no se ha creado correctamente';
					
                    RETURN;

                END IF;

            END IF

            /* ---------------------------------------------- */
            /* crea registro de para la log                   */
            /* ---------------------------------------------- */;

            INSERT INTO ge_tlgtr (lgtr_lgtr, lgtr_trans, lgtr_terminal, lgtr_user, lgtr_fech_oper, lgtr_hora_oper, lgtr_observ, lgtr_prcs, lgtr_fech_ini, lgtr_id_entidad, lgtr_fech_fin, lgtr_crta)
            VALUES (v_lgtr, p_ctrl_operacion[i].lgtr_trans, p_ctrl_operacion[i].lgtr_terminal, p_ctrl_operacion[i].lgtr_user, p_ctrl_operacion[i].lgtr_fech_oper, p_ctrl_operacion[i].lgtr_hora_oper, p_ctrl_operacion[i].lgtr_observacion, r_prcs, CURRENT_TIMESTAMP, '-'
            /* Campo que se diligencia al finalizar el log */, NULL
            /* Campo que se diligencia al finalizar el log */, NULL
            /* Campo que se diligencia al finalizar el log */);

        END LOOP;

        p_lgtr := v_lgtr;
        p_cod_rpta := 'OK';
        p_cod_rpta_descri := 'Inicio de log realizada con exito';

    ELSIF (p_ctrl_operacion IS NULL) THEN

        ROLLBACK;
        
        p_lgtr := NULL;
        p_cod_rpta := 'ER_LGTR_NL';
        p_cod_rpta_descri := 'La variable p_ctrl_operacion no puede ser nula';

        RETURN;

    ELSIF (COALESCE(array_length(p_ctrl_operacion, 1), 0) > 1) THEN

        ROLLBACK;

        p_lgtr := NULL;
        p_cod_rpta := 'ER_LGTR_MR';
        p_cod_rpta_descri := 'La variable p_ctrl_operacion solo debe tener un registro';

        RETURN;

    END IF;

    EXCEPTION

        WHEN unique_violation THEN

            p_lgtr := NULL;
            p_cod_rpta := 'ER_LGTR_REG';
            p_cod_rpta_descri := 'Ya existe log de operacion en BD';

        WHEN others THEN

            p_lgtr := NULL;
            p_cod_rpta := 'ER_LGTR_NC';
            p_cod_rpta_descri := CONCAT_WS('', 'Error no controlado: ', SQLERRM);

END;

$BODY$;

--ALTER FUNCTION "ge_qlgtr$iniciar_log"(TYPE_TT_GE_LGTR, text)
    
