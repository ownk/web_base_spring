-- ***************************************************
-- **          PROYECTO NS UBP                      **
-- **          REV:01\02\2019                       ** 
-- **          CLIENTE: NEW SAPIENS                 **
-- ***************************************************

-- FUNCTION: "api_ge_qutils$getmensajeporcodigo"(text, text)

CREATE OR REPLACE FUNCTION "api_ge_qutils$getmensajeporcodigo"(
	p_codigo    text,
	p_prcs      text)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    c_mensaje_rpta CURSOR FOR
    SELECT
        CRTA_DESCRI
        FROM GE_TCRTA
        WHERE CRTA_CRTA = p_codigo AND CRTA_PRCS = p_prcs;

    v_mensaje_rpta GE_TCRTA.CRTA_DESCRI%TYPE;

BEGIN

    OPEN c_mensaje_rpta;
    FETCH c_mensaje_rpta INTO v_mensaje_rpta;
    CLOSE c_mensaje_rpta;

    RETURN v_mensaje_rpta;

END;

$BODY$;

--ALTER FUNCTION "api_ge_qutils$getmensajeporcodigo"(text, text)
    
