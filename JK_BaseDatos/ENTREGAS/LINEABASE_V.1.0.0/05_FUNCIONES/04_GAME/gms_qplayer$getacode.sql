-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             13/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$getacode"(
	p_player_player text
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_acode TEXT;
	

BEGIN



		
	SELECT 
		cli_tacode.acode_acode
		
		INTO STRICT v_acode
		
		FROM GMS_TPLACN,acn_tacac,cli_tacode 
		WHERE
			PLACN_PLAYER = p_player_player AND
			placn_acnt=acac_acnt AND
			acac_acode=acode_acode and acode_state ='USE'
		;

        RETURN v_acode;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getacode"(text)
    
