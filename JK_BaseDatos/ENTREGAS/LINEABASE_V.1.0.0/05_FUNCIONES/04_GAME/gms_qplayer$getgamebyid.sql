-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             14/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getgamebyid"
(
	p_prcs              text,
	p_game_game     	text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tgame       	TYPE_TT_GM_GAME

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE


    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_game TYPE_TO_GM_GAME;
    tt_gm_game TYPE_TT_GM_GAME;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;


BEGIN

	to_gm_game=gms_qgame$getgame(p_game_game);
	tt_gm_game[COALESCE(array_length(tt_gm_game, 1), 0)] := to_gm_game;

	p_tgame := tt_gm_game;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getgamebyid"(text, text)
    
