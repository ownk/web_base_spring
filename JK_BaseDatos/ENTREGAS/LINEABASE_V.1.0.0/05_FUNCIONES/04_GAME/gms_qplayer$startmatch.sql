-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION juegohacker_us."gms_qplayer$startmatch"
(
	p_prcs 				TEXT,
	p_player_player		TEXT,
	p_game_game			TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		juegohacker_us.type_tt_ge_erro,
	OUT p_mensajes 		juegohacker_us.type_tt_ge_mnsj,
	OUT p_match_match	TEXT
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro juegohacker_us.TYPE_TO_GE_ERRO;
    tt_ge_erro juegohacker_us.TYPE_TT_GE_ERRO;

	c_smatch CURSOR FOR				
	select nextval ('gms_smatch');
	
	c_sscore CURSOR FOR				
	select nextval ('gms_sscore');

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje juegohacker_us.TYPE_TT_GE_MNSJ;
 
	v_nextval_match 	NUMERIC;
	v_nextval_score 	NUMERIC;
	v_match_match 		VARCHAR(50);
	v_score_score 		VARCHAR(50);
	v_cod_mensaje		VARCHAR(100):='OK';

BEGIN
	
	OPEN c_smatch;
	FETCH c_smatch INTO v_nextval_match;
	CLOSE c_smatch;
	
	OPEN c_sscore;
	FETCH c_sscore INTO v_nextval_score;
	CLOSE c_sscore;

	v_match_match := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_match);
	v_score_score := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_score);
	
		--Se crea un nuevo Match 
		Insert Into juegohacker_us.GMS_TMATCH(
				MATCH_MATCH,
				MATCH_PLAYER,
				MATCH_GAME,
				MATCH_START,
				MATCH_FINAL_SCORE,
				MATCH_STATE
		)VALUES(
				v_match_match,
				p_player_player,
				p_game_game,
				CURRENT_TIMESTAMP,
				0,
				'INI'
		);
		
		--Se crea un score para el match
		Insert Into juegohacker_us.GMS_TSCORE(
				SCORE_SCORE,
				SCORE_MATCH,
				SCORE_ATTRIB_LIFE,
				SCORE_ATTRIB_FORCE,
				SOCRE_ATTRIB_TENACITY
		)VALUES(
				v_score_score,
				v_match_match,
				0,
				0,
				0
		);
		
	p_match_match = v_match_match;

	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::juegohacker_us.TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::juegohacker_us.TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::juegohacker_us.TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION juegohacker_us."gms_qplayer$startmatch"(text, text, text)
    

