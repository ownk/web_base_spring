-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             14/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getavatar"
(
	p_avatar_avatar     	text
)
RETURNS TYPE_TO_GM_AVATAR
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_avatar CURSOR FOR
    SELECT
        AVATAR_AVATAR,
        AVATAR_NAME,
        AVATAR_DESCRI,
        AVATAR_SKIN_URL,
		AVATAR_CHRT
        FROM GMS_TAVATAR
		WHERE AVATAR_AVATAR=p_avatar_avatar;

	to_gm_avatar TYPE_TO_GM_AVATAR;

BEGIN
	
	--Obtener el avatar por id
	
	FOR i IN c_avatar
	LOOP
        to_gm_avatar := ROW (i.AVATAR_AVATAR,i.AVATAR_NAME,i.AVATAR_DESCRI,i.AVATAR_SKIN_URL,i.AVATAR_CHRT)::TYPE_TO_GM_AVATAR;
	END LOOP;

	RETURN to_gm_avatar;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getavatar"(text)
    
