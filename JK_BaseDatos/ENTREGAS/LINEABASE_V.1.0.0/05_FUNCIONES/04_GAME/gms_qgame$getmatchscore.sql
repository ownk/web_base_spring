-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             31/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getmatchscore"
(
	p_prcs              text,
	p_match_match     	text, 
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tscore       	TYPE_TT_GM_SCORE
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_scores CURSOR FOR
    SELECT
        SCORE_SCORE
		FROM GMS_TSCORE
		WHERE SCORE_MATCH=p_match_match;
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gms_score TYPE_TO_GM_SCORE;
    tt_gms_score TYPE_TT_GM_SCORE;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
	
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
    
    FOR i IN c_scores
    LOOP
		
		to_gms_score=gms_qgame$getscore(i.SCORE_SCORE);
        tt_gms_score[COALESCE(array_length(tt_gms_score, 1), 0)] := to_gms_score;

    END LOOP;

    p_tscore := tt_gms_score;
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getmatchscore"(text, text)
    
