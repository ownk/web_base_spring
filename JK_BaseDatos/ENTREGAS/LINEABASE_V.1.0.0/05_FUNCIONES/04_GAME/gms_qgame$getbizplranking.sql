-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             19/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getbizplranking"
(
	p_prcs              text,
	p_client_client     text,
	p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tplayer       TYPE_TT_pl_player
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	--Cursor para traer los 100 pirmeros jugadores por cliente ordenados por su score
	
	c_players_biz CURSOR FOR
	SELECT 	
		player_player,row_number() over (order by player_score DESC) as ranking, player_score
		FROM GMS_TPLACN,acn_tacac,cli_tacode,cli_tclient,gms_tplayer
		WHERE
			placn_acnt=acac_acnt AND
			acac_acode=acode_acode AND
			client_client=acode_client AND 
			player_player=placn_player and
			client_client = p_client_client;
			
	
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_pl_player TYPE_TO_PL_PLAYER;
    tt_pl_player TYPE_TT_PL_PLAYER;

	v_code VARCHAR(50);
	v_client VARCHAR(50);

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	

BEGIN
	
    FOR i IN c_players_biz
    LOOP
		to_pl_player=gms_qgame$getplayer(i.player_player,p_pgcrypto_key);
        tt_pl_player[COALESCE(array_length(tt_pl_player, 1), 0)] := to_pl_player;
		
    END LOOP;
    p_tplayer := tt_pl_player;
	
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getbizplranking"(text,text,text)
    
