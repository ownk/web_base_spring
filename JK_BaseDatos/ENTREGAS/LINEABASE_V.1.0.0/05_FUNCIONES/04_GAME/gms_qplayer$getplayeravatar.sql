-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             30/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getplayeravatar"
(
	p_prcs              text,
	p_player_player     text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tavatar       TYPE_TT_GM_AVATAR,
    OUT p_tatrpl       	TYPE_TT_GM_ATRPL

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_avpl cursor FOR
	SELECT AVPL_AVATAR 	
	FROM GMS_TAVPL
	WHERE p_player_player=AVPL_PLAYER AND AVPL_STATE='ACT';
	
	c_avskin cursor FOR
	SELECT AVPL_SKIN_URL
	FROM GMS_TAVPL
	WHERE p_player_player=AVPL_PLAYER AND AVPL_STATE='ACT';
	
    c_avatars CURSOR (vc_avpl VARCHAR(50)) FOR
    SELECT
        AVATAR_AVATAR,
        AVATAR_NAME,
        AVATAR_DESCRI,
        AVATAR_SKIN_URL,
		AVATAR_CHRT
        FROM GMS_TAVATAR
		WHERE AVATAR_AVATAR = vc_avpl;
        
	
	c_atrpl_chrt CURSOR(vc_atrpl_ch VARCHAR(50)) FOR
    SELECT
        ATRPL_PLAYER,
        ATRPL_CHRT,
		ATRPL_ATRB
        FROM GMS_TATRPL I
		WHERE ATRPL_PLAYER=p_player_player AND ATRPL_CHRT=vc_atrpl_ch;


    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_avatar TYPE_TO_GM_AVATAR;
    tt_gm_avatar TYPE_TT_GM_AVATAR;
	
	to_gm_atrpl TYPE_TO_GM_ATRPL;
    tt_gm_atrpl TYPE_TT_GM_ATRPL;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

	v_avpl VARCHAR(50);
	v_avskin TEXT;


BEGIN
    
	OPEN c_avpl;
	FETCH c_avpl INTO v_avpl;
	CLOSE c_avpl;
	
	IF (v_avpl IS NOT NULL)THEN

		OPEN c_avskin;
		FETCH c_avskin INTO v_avskin;
		CLOSE c_avskin;
		
		FOR i IN c_avatars (v_avpl)
		LOOP

			to_gm_avatar=gms_qgame$getavatar(i.AVATAR_AVATAR);
			to_gm_avatar.AVATAR_SKIN_URL=v_avskin;
			tt_gm_avatar[COALESCE(array_length(tt_gm_avatar, 1), 0)] := to_gm_avatar;
			
			--Obtener los atributos del personaje
			FOR j IN c_atrpl_chrt(i.AVATAR_CHRT)
			LOOP
				to_gm_atrpl=gms_qgame$getatrpl(j.ATRPL_PLAYER,j.ATRPL_CHRT,j.ATRPL_ATRB);
				tt_gm_atrpl[COALESCE(array_length(tt_gm_atrpl, 1), 0)] := to_gm_atrpl;
			END LOOP;
		END LOOP;

		p_tavatar := tt_gm_avatar;
		p_tatrpl := tt_gm_atrpl;
    END IF;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getplayeravatar"(text, text)
    
