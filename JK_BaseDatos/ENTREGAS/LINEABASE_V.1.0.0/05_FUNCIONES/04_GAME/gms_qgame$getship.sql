-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             14/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getship"
(
	p_ship_ship     	text
)
RETURNS TYPE_TO_GM_SHIP
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_ship CURSOR FOR
    SELECT
        SHIP_SHIP,
        SHIP_NAME,
        SHIP_DESCRI,
        SHIP_SKIN_URL,
		SHIP_CHRT
        FROM GMS_TSHIP
		WHERE SHIP_SHIP=p_ship_ship;

	to_gm_ship TYPE_TO_GM_SHIP;

BEGIN
	
	--Obtener el ship por id
	
	FOR i IN c_ship
	LOOP
        to_gm_ship := ROW (i.SHIP_SHIP,i.SHIP_NAME,i.SHIP_DESCRI,i.SHIP_SKIN_URL,i.SHIP_CHRT)::TYPE_TO_GM_SHIP;
	END LOOP;

	RETURN to_gm_ship;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getship"(text)
    
