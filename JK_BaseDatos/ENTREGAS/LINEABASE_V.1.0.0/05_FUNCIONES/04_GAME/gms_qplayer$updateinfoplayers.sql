-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             24/03/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$updateinfoplayers"
(
	p_prcs 				TEXT,
	p_client_client		TEXT,
	p_tplayer 			type_tt_pl_player,
	p_pgcrypto_key      TEXT,
	p_pgcrypto_alg      TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	v_crta_http_gral CHARACTER VARYING(3) := 'OK';
	v_mensajes TYPE_TT_GE_MNSJ;
	v_errores   TYPE_TT_GE_ERRO;
	
	v_player_ident  TEXT;
    v_message       TEXT;
    v_player_id_ext TEXT;
	v_client_exist NUMERIC=0;
	v_player_exist NUMERIC;
	resultado BOOLEAN;
	v_player_client BOOLEAN;
		
	v_cod_message    VARCHAR(100):='OK';
	
	CLIENT_PLAYERS RECORD;
	tt_client_players TYPE_TT_PL_PLAYER; 

BEGIN

	--Cliente no existe
	
	SELECT COUNT(*)
	INTO v_client_exist
	FROM CLI_TCLIENT
	where CLIENT_CLIENT=p_client_client;
	
	
	SELECT
		*
		FROM cli_qclient$getclientplayers(p_prcs,p_client_client,p_pgcrypto_key)
		INTO CLIENT_PLAYERS;
	
	tt_client_players:=CLIENT_PLAYERS.P_TPLAYER;	
	
	
	IF (v_client_exist=0) THEN
    
    
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('ERROR'::TEXT, p_prcs, v_mensajes, 'CLIENT NOT EXIST')
            INTO v_mensajes;
	
		
        SELECT
            *
            FROM api_ge_qutils$generartypeerror('ERROR_CLNT_NE', p_prcs, v_errores, 'CLIENT NOT EXIST')
            INTO v_errores;
	ELSE
	
		FOR i IN 0..COALESCE(array_length(p_tplayer, 1)-1, 0)
		LOOP
            v_cod_message := 'ERROR';
        
			v_player_ident := ''||COALESCE(p_tplayer[i].PLAYER_TPIDENT, '')||'-'||COALESCE(p_tplayer[i].PLAYER_NUMIDENT, '');
            v_message      := '';

			v_player_exist:=0;
			
			SELECT COUNT(*)
			INTO v_player_exist
			FROM GMS_TPLAYER
			WHERE  	PLAYER_HASH_TPIDENT= md5(upper(p_tplayer[i].PLAYER_TPIDENT)) AND 
					PLAYER_HASH_NUMIDENT= md5(upper(p_tplayer[i].PLAYER_NUMIDENT));
		
			--Validacion si el player existe
			IF (v_player_exist>0)THEN
			
			
				--Validaciones si el player es del cliente
				v_player_client:=false;
				FOR j IN 0..COALESCE(array_length(tt_client_players,1)-1,0)
				LOOP
					
					IF (p_tplayer[i].PLAYER_NUMIDENT=tt_client_players[j].PLAYER_NUMIDENT AND p_tplayer[i].PLAYER_TPIDENT=tt_client_players[j].PLAYER_TPIDENT)THEN
						
						v_player_client = true;
					
					END IF;
				END LOOP;	
				
				IF (v_player_client='true')THEN
					
				
				
					BEGIN
                    
                        v_player_id_ext := COALESCE(p_tplayer[i].PLAYER_ID_EXT, '#');
                        
                        if(v_player_id_ext = '#' or v_player_id_ext='') then
                           v_player_id_ext:= null;
                        else
                           v_player_id_ext := pgp_sym_encrypt(p_tplayer[i].PLAYER_ID_EXT, p_pgcrypto_key,p_pgcrypto_alg);
                        end if;
                        
                    
						--Actualizar Id Extrerno de Players
						UPDATE GMS_TPLAYER
						SET 
							PLAYER_ID_EXT = v_player_id_ext
						WHERE 	PLAYER_HASH_TPIDENT= md5(upper(p_tplayer[i].PLAYER_TPIDENT)) AND 
								PLAYER_HASH_NUMIDENT= md5(upper(p_tplayer[i].PLAYER_NUMIDENT));
							
                        
						v_cod_message := 'OK';
						v_message := ' : EXTERNAL ID = '||p_tplayer[i].PLAYER_ID_EXT;        
					
					EXCEPTION
                        WHEN OTHERS THEN
                        
                            v_message := ': CHECK ERROR DETAILS'; 
                             
							SELECT
							*
							FROM api_ge_qutils$generartypeerror('ERROR_PLAYER_UPDATE'::TEXT, p_prcs, v_errores, v_player_ident||': '||SQLERRM)
							INTO v_errores;
						
							
					
					END;					
					
				ELSE
				
                    v_message := ': PLAYER NOT VALID';   
						
					SELECT
						*
						FROM api_ge_qutils$generartypeerror('ERROR_PLAYER_NV'::TEXT, p_prcs, v_errores, v_player_ident||v_message)
						INTO v_errores;
				
				END IF;
			ELSE
				
                v_message := ': PLAYER NOT EXIST';   
                
				SELECT
					*
					FROM api_ge_qutils$generartypeerror('ERROR_PLAYER_NE'::TEXT, p_prcs, v_errores, v_player_ident||v_message)
					INTO v_errores;
			END IF;
            
           			
                        
            SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta(v_cod_message::TEXT, p_prcs, v_mensajes, v_player_ident||v_message)
            INTO v_mensajes;
			
			
		END LOOP;
	
	END IF;

	p_cod_rpta := 'OK';

	p_errores  := v_errores;

	p_mensajes := v_mensajes;
	
	
	EXCEPTION

		WHEN others THEN
		
			p_cod_rpta := 'ERROR';
		
			SELECT
				*
				FROM api_ge_qutils$generartypeerror(SQLSTATE, p_prcs, v_errores, SQLERRM)
				INTO v_errores;
			
			p_errores := v_errores;

			SELECT
				*
				FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensajes)
				INTO v_mensajes;

			p_mensajes := v_mensajes;

END;

$BODY$;


