-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             31/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION juegohacker_us."gms_qplayer$getplayermatchs"
(
	p_prcs              text,
	p_player_player     text,
	OUT p_cod_rpta      text,
	OUT p_errores       juegohacker_us.TYPE_TT_GE_ERRO,
	OUT p_mensajes      juegohacker_us.TYPE_TT_GE_MNSJ,
    OUT p_tmatch       	juegohacker_us.TYPE_TT_GM_MATCH,
    OUT p_tgame       	juegohacker_us.TYPE_TT_GM_GAME
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_macth CURSOR FOR
    SELECT
        MATCH_MATCH,
        MATCH_PLAYER,
        MATCH_GAME,
        MATCH_START,
        MATCH_END,
		MATCH_FINAL_SCORE,
		MATCH_STATE,
		MATCH_RESULT
        FROM juegohacker_us.GMS_TMATCH
		WHERE MATCH_PLAYER=p_player_player;
        
	
    to_ge_erro juegohacker_us.TYPE_TO_GE_ERRO;
    tt_ge_erro juegohacker_us.TYPE_TT_GE_ERRO;

    to_gms_match juegohacker_us.TYPE_TO_GM_MATCH;
    tt_gms_match juegohacker_us.TYPE_TT_GM_MATCH;
	
	to_gms_game juegohacker_us.TYPE_TO_GM_GAME;
    tt_gms_game juegohacker_us.TYPE_TT_GM_GAME;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
	
    v_mensaje juegohacker_us.TYPE_TT_GE_MNSJ;
	v_game_match VARCHAR(50) = '0';
	

BEGIN
    
    FOR i IN c_macth
    LOOP

        to_gms_match := ROW (i.MATCH_MATCH,i.MATCH_PLAYER,i.MATCH_GAME,i.MATCH_START,i.MATCH_END,i.MATCH_FINAL_SCORE,i.MATCH_STATE,i.MATCH_RESULT)::juegohacker_us.TYPE_TO_GM_MATCH;
        tt_gms_match[COALESCE(array_length(tt_gms_match, 1), 0)] := to_gms_match;

    END LOOP;

    p_tmatch := tt_gms_match;
	
	--Obtener los games jugados por el player
	
	FOR i IN 0..COALESCE(array_length(p_tmatch, 1)-1, 0)
	LOOP
	
		to_gms_game=juegohacker_us.gms_qgame$getgame(p_tmatch[i].MATCH_GAME);

		IF (v_game_match!=p_tmatch[i].MATCH_GAME)THEN
			tt_gms_game[COALESCE(array_length(tt_gms_game, 1), 0)] := to_gms_game;
		END IF;	

		v_game_match:=p_tmatch[i].MATCH_GAME;
		p_tgame := tt_gms_game;

	END LOOP;

	
	
    SELECT
        *
        FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::juegohacker_us.TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION juegohacker_us."gms_qplayer$getplayermatchs"(text, text)
    
