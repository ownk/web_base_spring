-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getplayerbyid"
(
	p_prcs              text,
	p_player_player         text,
	p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tplayer       TYPE_TT_PL_PLAYER,
    OUT p_tatrpl       	TYPE_TT_GM_ATRPL
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_players CURSOR FOR
    SELECT
        PLAYER_PLAYER
        FROM GMS_TPLAYER
		WHERE PLAYER_PLAYER=p_player_player;
        
	c_atrpl CURSOR(vc_atrpl_pl VARCHAR(50)) FOR
    SELECT
        ATRPL_PLAYER,
        '' ATRPL_CHRT,
		ATRPL_ATRB,
		SUM(ATRPL_VALUE) ATRPL_VALUE,
		ATRB_DESCRI,
		ATRB_NAME,
		ATRB_ICON,
		ATRB_COLOR
        FROM GMS_TATRPL inner join gms_tatrb on GMS_TATRPL.ATRPL_ATRB=gms_tatrb.atrb_atrb
		WHERE ATRPL_PLAYER=vc_atrpl_pl
		GROUP BY ATRPL_PLAYER,ATRPL_ATRB,ATRB_DESCRI,ATRB_NAME,ATRB_ICON,ATRB_COLOR;
		
	
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_pl_player TYPE_TO_PL_PLAYER;
    tt_pl_player TYPE_TT_PL_PLAYER;
	
	to_gm_atrpl TYPE_TO_GM_ATRPL;
    tt_gm_atrpl TYPE_TT_GM_ATRPL;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	v_porcent NUMERIC;
	v_confg VARCHAR(50) = 'game.player.attribute.scale';

BEGIN
    
	SELECT *
        FROM "ge_qutils$getcnfg_value"(v_confg)
        INTO v_porcent;
	
    FOR i IN c_players
    LOOP

		to_pl_player=gms_qgame$getplayer(i.PLAYER_PLAYER,p_pgcrypto_key);
        tt_pl_player[COALESCE(array_length(tt_pl_player, 1), 0)] := to_pl_player;

		--Obtener los atributos por personaje
		FOR j IN c_atrpl(i.PLAYER_PLAYER)
		LOOP
			to_gm_atrpl := ROW (j.ATRPL_PLAYER,j.ATRPL_CHRT,j.ATRPL_ATRB,(j.ATRPL_VALUE*100)/v_porcent,j.ATRB_DESCRI,j.ATRB_NAME,j.ATRB_ICON,j.ATRB_COLOR)::TYPE_TO_GM_ATRPL;
			tt_gm_atrpl[COALESCE(array_length(tt_gm_atrpl, 1), 0)] := to_gm_atrpl;
		END LOOP;

    END LOOP;

    p_tplayer := tt_pl_player;
	p_tatrpl := tt_gm_atrpl;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getplayerbyid"(text, text, text)
    
