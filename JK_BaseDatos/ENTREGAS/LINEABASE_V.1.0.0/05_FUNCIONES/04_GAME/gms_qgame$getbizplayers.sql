-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             19/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION juegohacker_us."gms_qgame$getbizplayers"
(
	p_prcs              text,
	p_client_client     text,
	p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       juegohacker_us.TYPE_TT_GE_ERRO,
	OUT p_mensajes      juegohacker_us.TYPE_TT_GE_MNSJ,
    OUT p_tplayer       juegohacker_us.TYPE_TT_pl_player
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	--Cursor para traer los jugadores por cliente
	
	c_players_biz CURSOR FOR
	SELECT 	
		player_player
		FROM GMS_TPLACN,acn_tacac,cli_tacode,cli_tclient,gms_tplayer
		WHERE
			placn_acnt=acac_acnt AND
			acac_acode=acode_acode AND
			acode_date_validity = (select max(acode_date_validity) from cli_tacode)and 
			client_client=acode_client AND 
			player_player=placn_player and
			client_client = p_client_client;
	
	
    to_ge_erro juegohacker_us.TYPE_TO_GE_ERRO;
    tt_ge_erro juegohacker_us.TYPE_TT_GE_ERRO;

    to_pl_player juegohacker_us.TYPE_TO_PL_PLAYER;
    tt_pl_player juegohacker_us.TYPE_TT_PL_PLAYER;

	v_code VARCHAR(50);

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje juegohacker_us.TYPE_TT_GE_MNSJ;
	
	

BEGIN


    FOR i IN c_players_biz
    LOOP
		to_pl_player=juegohacker_us.gms_qgame$getplayer(i.player_player,p_pgcrypto_key);
        tt_pl_player[COALESCE(array_length(tt_pl_player, 1), 0)] := to_pl_player;
		
    END LOOP;
    p_tplayer := tt_pl_player;
	
    
    SELECT
        *
        FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::juegohacker_us.TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION juegohacker_us."gms_qgame$getbizplayers"(text,text,text)
    
