-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             31/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getplayermatches"
(
	p_prcs              text,
	p_player_player     text,
	p_game_game		    text,
	p_limit				NUMERIC,
	p_offset			NUMERIC,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tmatch       	TYPE_TT_GM_MATCH,
    OUT p_tgame       	TYPE_TT_GM_GAME
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_macth CURSOR (vc_limit numeric, vc_offset numeric, vc_game_game VARCHAR(50)) FOR
    SELECT
        MATCH_MATCH,
        MATCH_PLAYER,
        MATCH_GAME,
        MATCH_START,
        MATCH_END,
		MATCH_FINAL_SCORE,
		MATCH_GAME_SCORE,
		MATCH_STATE,
		MATCH_RESULT
        FROM GMS_TMATCH
		WHERE MATCH_PLAYER=p_player_player
		AND MATCH_GAME=COALESCE(vc_game_game,MATCH_GAME)
        ORDER BY MATCH_START DESC
        LIMIT vc_limit OFFSET vc_offset;
        
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gms_match TYPE_TO_GM_MATCH;
    tt_gms_match TYPE_TT_GM_MATCH;
	
	to_gms_game TYPE_TO_GM_GAME;
    tt_gms_game TYPE_TT_GM_GAME;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
	
    v_mensaje TYPE_TT_GE_MNSJ;
	v_game_match VARCHAR(50) = '0';
	v_limit NUMERIC;
	v_offset NUMERIC;
	v_game_game VARCHAR(50);
	resultado BOOLEAN;
	

BEGIN
    
	
	
	IF(p_limit=0)THEN
		v_limit:=null;
	ELSE
		v_limit:=p_limit;
	END IF;
	
	IF(p_offset=0)THEN
		v_offset:=null;
	ELSE
		v_limit:=p_offset;
	END IF;
	
	IF(p_game_game='')THEN
		v_game_game:=null;
	ELSE
		v_game_game:=p_game_game;
	END IF;
	
    FOR i IN c_macth(v_limit,v_offset,v_game_game)
    LOOP

        to_gms_match := ROW (i.MATCH_MATCH,i.MATCH_PLAYER,i.MATCH_GAME,i.MATCH_START,i.MATCH_END,i.MATCH_FINAL_SCORE,i.MATCH_GAME_SCORE,i.MATCH_STATE,i.MATCH_RESULT)::TYPE_TO_GM_MATCH;
        tt_gms_match[COALESCE(array_length(tt_gms_match, 1), 0)] := to_gms_match;

    END LOOP;

    p_tmatch := tt_gms_match;
	
	--Obtener los games jugados por el player
	
	FOR i IN 0..COALESCE(array_length(p_tmatch, 1)-1, 0)
	LOOP
	
		to_gms_game=gms_qgame$getgame(p_tmatch[i].MATCH_GAME);

		IF (v_game_match!=p_tmatch[i].MATCH_GAME)THEN
			tt_gms_game[COALESCE(array_length(tt_gms_game, 1), 0)] := to_gms_game;
		END IF;	

		v_game_match:=p_tmatch[i].MATCH_GAME;
		p_tgame := tt_gms_game;

	END LOOP;

	
	
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getplayermatches"(text, text, text, numeric,numeric)
    
