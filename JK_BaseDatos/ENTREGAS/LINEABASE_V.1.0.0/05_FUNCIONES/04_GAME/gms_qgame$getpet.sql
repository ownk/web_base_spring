-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             14/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getpet"
(
	p_pet_pet     	text
)
RETURNS TYPE_TO_GM_PET
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_pet CURSOR FOR
    SELECT
        PET_PET,
        PET_NAME,
		PET_DESCRI,
        PET_SKIN_URL,
		PET_CHRT
        FROM GMS_TPET
		WHERE PET_PET=p_pet_pet;

	to_gm_pet TYPE_TO_GM_PET;

BEGIN
	
	--Obtener el PET por id
	
	FOR i IN c_pet
	LOOP
        to_gm_pet := ROW (i.PET_PET,i.PET_NAME,i.PET_DESCRI,i.PET_SKIN_URL,i.PET_CHRT)::TYPE_TO_GM_PET;
		
	END LOOP;

	RETURN to_gm_pet;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getpet"(text)
    
