-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             28/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getplayership"
(
	p_prcs              text,
	p_player_player 	text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tship	        TYPE_TT_GM_SHIP,
    OUT p_tatrpl       	TYPE_TT_GM_ATRPL
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
	
	c_shippl cursor FOR
	SELECT SHIPPL_SHIP
	FROM GMS_TSHIPPL
	WHERE p_player_player=SHIPPL_PLAYER AND SHIPPL_STATE='ACT';	
     
	c_shipskin cursor FOR
	SELECT SHIPPL_SKIN_URL
	FROM GMS_TSHIPPL
	WHERE p_player_player=SHIPPL_PLAYER AND SHIPPL_STATE='ACT'; 

    c_ships CURSOR (vc_shippl VARCHAR(50)) FOR
    SELECT
        SHIP_SHIP,
        SHIP_NAME,
        SHIP_DESCRI,
        SHIP_SKIN_URL,
		SHIP_CHRT
        FROM GMS_TSHIP
		WHERE SHIP_SHIP = vc_shippl;
		
	c_atrpl_atrpl CURSOR(vc_atrpl_ch VARCHAR(50)) FOR
    SELECT
        ATRPL_PLAYER,
        ATRPL_CHRT,
		ATRPL_ATRB
        FROM GMS_TATRPL 
		WHERE ATRPL_PLAYER=p_player_player AND ATRPL_CHRT=vc_atrpl_ch;
		
	
			
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_ship TYPE_TO_GM_SHIP;
    tt_gm_ship TYPE_TT_GM_SHIP;
	
	to_gm_atrpl TYPE_TO_GM_ATRPL;
    tt_gm_atrpl TYPE_TT_GM_ATRPL;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

	v_shippl VARCHAR(50);
	v_skinship TEXT;

BEGIN
    
	OPEN c_shippl;
	FETCH c_shippl INTO v_shippl;
	CLOSE c_shippl;
	
	
	IF (v_shippl IS NOT NULL) THEN
		
		OPEN c_shipskin;
		FETCH c_shipskin INTO v_skinship;
		CLOSE c_shipskin;
	
		FOR i IN c_ships (v_shippl)
		LOOP

			to_gm_ship=gms_qgame$getship(i.SHIP_SHIP);
			to_gm_ship.SHIP_SKIN_URL=v_skinship;
			tt_gm_ship[COALESCE(array_length(tt_gm_ship, 1), 0)] := to_gm_ship;

			--Obtener los atributos del personaje
			FOR j IN c_atrpl_atrpl(i.SHIP_CHRT)
			LOOP
				to_gm_atrpl=gms_qgame$getatrpl(j.ATRPL_PLAYER,j.ATRPL_CHRT,j.ATRPL_ATRB);
				tt_gm_atrpl[COALESCE(array_length(tt_gm_atrpl, 1), 0)] := to_gm_atrpl;
			END LOOP;	
		END LOOP;
		
		p_tship := tt_gm_ship;
		p_tatrpl := tt_gm_atrpl;
		
	END IF;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getplayership"(text,text)
    
