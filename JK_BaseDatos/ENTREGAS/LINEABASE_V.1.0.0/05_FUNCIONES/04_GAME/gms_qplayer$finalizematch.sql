-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION juegohacker_us."gms_qplayer$finalizematch"
(
	p_prcs 				TEXT,
	p_tscore			juegohacker_us.type_tt_gm_score,
	p_final_score		numeric,
	p_match_result		TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		juegohacker_us.type_tt_ge_erro,
	OUT p_mensajes 		juegohacker_us.type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro juegohacker_us.TYPE_TO_GE_ERRO;
    tt_ge_erro juegohacker_us.TYPE_TT_GE_ERRO;

	

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje juegohacker_us.TYPE_TT_GE_MNSJ;
 
	v_cod_mensaje		VARCHAR(100):='OK';

BEGIN
	
	FOR i IN 0..COALESCE(array_length(p_tscore, 1)-1, 0)
	LOOP
	
		--Se actualiza el score final obtenido en la partida
		UPDATE GMS_TMATCH SET
			MATCH_END=CURRENT_TIMESTAMP,
			MATCH_FINAL_SCORE=p_final_score,
			MATCH_STATE='END',
			MATCH_RESULT=p_match_result
		WHERE MATCH_MATCH=p_tscore[i].SCORE_MATCH;
		
		
		--Se actualizan los atributos obtenidos en la partida y la hora en que termina
		
		UPDATE 	GMS_TSCORE SET
				SCORE_ATTRIB_LIFE=p_tscore[i].SCORE_ATTRIB_LIFE,
				SCORE_ATTRIB_FORCE=p_tscore[i].SCORE_ATTRIB_FORCE,
				SOCRE_ATTRIB_TENACITY=p_tscore[i].SOCRE_ATTRIB_TENACITY
		WHERE 	SCORE_MATCH=p_tscore[i].SCORE_MATCH;
		
	END LOOP;

	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::juegohacker_us.TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::juegohacker_us.TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::juegohacker_us.TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION juegohacker_us."gms_qplayer$finalizematch"(text, juegohacker_us.type_tt_gm_score, numeric, text)
    

