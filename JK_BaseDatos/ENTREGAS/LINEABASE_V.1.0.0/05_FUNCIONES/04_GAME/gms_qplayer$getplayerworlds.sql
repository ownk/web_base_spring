-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getplayerworlds"
(
	p_prcs              text,
	p_player_player     text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tworld       	TYPE_TT_GM_WORLD

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	--Cursor para traer los mundos del player
	c_plwl CURSOR (vc_acode VARCHAR(50))FOR
	SELECT GMS_TWORLD.WORLD_WORLD
		FROM GMS_TPLACN, GMS_TGMWL, GMS_TWORLD, GMS_TGMAC, ACN_TACAC
		WHERE PLACN_PLAYER = p_player_player
		AND ACAC_ACODE = vc_acode
		AND ACAC_ACNT = PLACN_ACNT
		AND GMAC_ACODE = ACAC_ACODE
		AND GMAC_GAME = GMWL_GAME
		AND GMWL_WORLD = WORLD_WORLD;

		
	--Cursor para traer la informacion de los mundos
    c_worlds CURSOR FOR
    SELECT
        WORLD_WORLD,
		WORLD_NAME,
        WORLD_DESCRI,
		WORLD_POSITN_X,
		WORLD_POSITN_Y,
		WORLD_SIZE,
		WORLD_PICTURE,
		WORLD_STATE
        FROM GMS_TWORLD
		WHERE WORLD_STATE='ACT';

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_world TYPE_TO_GM_WORLD;
    tt_gm_world TYPE_TT_GM_WORLD;
	

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

	v_code VARCHAR(50);
	v_state TEXT;
	v_valid NUMERIC;

BEGIN
    
	--Se obtiene el codigo de acceso mas reciente del jugador
	v_code=gms_qplayer$getacode(p_player_player);
	
    --Estado inactivo
    v_state = 'INA';
	
	
	FOR j IN c_worlds
	LOOP
	v_valid=1;
		FOR i IN c_plwl (v_code)
		LOOP
			IF (v_valid = 1)THEN
				IF (j.WORLD_WORLD=i.WORLD_WORLD)THEN
					v_state = 'ACT';
					v_valid=0;
				ELSE
					v_state ='INA';
				END IF;
			END IF;
		END LOOP;	
		to_gm_world=gms_qgame$getworld(j.WORLD_WORLD);
		to_gm_world.WORLD_STATE=v_state;
		tt_gm_world[COALESCE(array_length(tt_gm_world, 1), 0)] := to_gm_world;
	END LOOP;
	p_tworld := tt_gm_world;

		
		
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getplayerworlds"(text, text)
    
