-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             30/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$getgamesworld"
(
	p_prcs              text,
	p_world_world     	text,
	p_player_player   	text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tgame       	TYPE_TT_GM_GAME
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	--Cursor para traer los juegos del player
	c_plgm CURSOR (vc_acode VARCHAR(50))FOR
	SELECT GMS_TGAME.game_game
		FROM GMS_TPLACN, GMS_TGMWL, GMS_TGAME, GMS_TGMAC, ACN_TACAC
		WHERE PLACN_PLAYER = p_player_player
		AND ACAC_ACODE = vc_acode
		AND ACAC_ACNT = PLACN_ACNT
		AND GMAC_ACODE = ACAC_ACODE
		AND GMAC_GAME = GMWL_GAME
		AND GMWL_GAME = GAME_GAME;
		
	--Cursor para traer los juegos por mundo
	c_gmwl CURSOR FOR
	SELECT 
		GMWL_GAME
		FROM GMS_TGMWL
		WHERE GMWL_WORLD=p_world_world;
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_game TYPE_TO_GM_GAME;
    tt_gm_game TYPE_TT_GM_GAME;
	
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	v_code VARCHAR(50);
	v_valid NUMERIC;
	v_state TEXT;
	

BEGIN

	
	--Se obtiene el codigo de acceso mas reciente del jugador
	v_code=gms_qplayer$getacode(p_player_player);
	
	FOR j IN c_gmwl
	LOOP
	v_valid =1;
		FOR i IN c_plgm (v_code)
		LOOP
			IF (v_valid = 1)THEN
				IF (j.GMWL_GAME=i.GAME_GAME)THEN
					v_state = 'ACT';
					v_valid=0;
				ELSE
					v_state ='INA';
				END IF;
			END IF;
		END LOOP;	
		to_gm_game=gms_qgame$getgame(j.GMWL_GAME);
		to_gm_game.GAME_STATE=v_state;
		tt_gm_game[COALESCE(array_length(tt_gm_game, 1), 0)] := to_gm_game;
	END LOOP;

	p_tgame := tt_gm_game;

    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$getgamesworld"(text,text,text)
    
