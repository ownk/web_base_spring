-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             09/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qgame$getscore"
(
	p_score_score     	text
)
RETURNS TYPE_TO_GM_SCORE
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_score CURSOR FOR
    SELECT
        SCORE_SCORE,
		SCORE_MATCH,
		SCORE_CHRT,
		SCORE_ATRB,
		SCORE_VALUE
        FROM GMS_TSCORE
		WHERE SCORE_SCORE=p_score_score;

	to_gm_score TYPE_TO_GM_SCORE;

BEGIN
	
	--Obtener el SCORE por id
	
	FOR i IN c_score
	LOOP
        to_gm_score := ROW (i.SCORE_SCORE,i.SCORE_MATCH,i.SCORE_CHRT,i.SCORE_ATRB,i.SCORE_VALUE)::TYPE_TO_GM_SCORE;
		
	END LOOP;

	RETURN to_gm_score;


END;

$BODY$;

--ALTER FUNCTION "gms_qgame$getscore"(text)
    
