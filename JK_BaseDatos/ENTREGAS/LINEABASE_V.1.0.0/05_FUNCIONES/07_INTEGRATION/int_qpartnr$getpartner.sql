-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             03/02/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qpartnr$getpartner"
(
	p_partnr_partnr     	text,
    p_pgcrypto_key          text
)
RETURNS TYPE_TO_IN_PARTNR
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	c_partner CURSOR FOR
	SELECT 
		PARTNR_PARTNR,
	    pgp_sym_decrypt(PARTNR_NAME::bytea,  p_pgcrypto_key) PARTNR_NAME, 
	    pgp_sym_decrypt(PARTNR_SITEKEY::bytea,  p_pgcrypto_key) PARTNR_SITEKEY, 
		pgp_sym_decrypt(PARTNR_ACCESSKEY::bytea,  p_pgcrypto_key) PARTNR_ACCESSKEY, 
		PARTNR_STATE
		FROM INT_TPARTNR
		WHERE 	PARTNR_PARTNR=p_partnr_partnr AND
                PARTNR_STATE='ACT';

	to_in_partnr TYPE_TO_IN_PARTNR;

BEGIN
	
	--Obtener el game por id
	
	FOR j IN c_partner
	LOOP
		to_in_partnr := ROW (j.PARTNR_PARTNR,j.PARTNR_NAME,j.PARTNR_SITEKEY,j.PARTNR_ACCESSKEY,j.PARTNR_STATE)::TYPE_TO_IN_PARTNR;
	END LOOP;

	RETURN to_in_partnr;


END;

$BODY$;

--ALTER FUNCTION "int_qpartnr$getpartner"(text,text)
    
