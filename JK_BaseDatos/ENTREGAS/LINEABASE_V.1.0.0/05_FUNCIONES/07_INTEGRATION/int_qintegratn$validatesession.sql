-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             02/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "int_qintegratn$validatesession"(
	p_session_session 		text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_session_valida NUMERIC;
	v_date TIMESTAMP;
	

BEGIN
	
	v_date := CURRENT_TIMESTAMP;
	
    SELECT

        COUNT(*)

        INTO STRICT v_session_valida

        FROM INT_TSESSION

        WHERE 	SESSION_SESSION = p_session_session 		AND 
				SESSION_STATE='ACT' 		AND 
				v_date<=SESSION_DATE_VALIDATE;

    IF (v_session_valida > 0) THEN
		
        RETURN TRUE;

    ELSE
		--Actualizacion del estado de la session de activo a caducada
		UPDATE INT_TSESSION
		SET SESSION_STATE='CAD'
		WHERE 	SESSION_SESSION=p_session_session 		AND
				SESSION_STATE='ACT';
		
       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$validatesession"(text)
    
