-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qintegratn$starsession"
(	
	p_prcs              	text,
	p_partnr_partnr     	text,
    p_token      			text,
	OUT p_cod_rpta      	text,
	OUT p_errores       	TYPE_TT_GE_ERRO,
	OUT p_mensajes      	TYPE_TT_GE_MNSJ,
    OUT p_session_session 	TEXT
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
  
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	c_ssession  CURSOR FOR				
	select nextval ('int_ssession');
	
	v_nextval_session 	NUMERIC;
	v_session_session	VARCHAR(50);
    


BEGIN
  
	
	OPEN c_ssession;
	FETCH c_ssession INTO v_nextval_session;
	CLOSE c_ssession;
  
	--v_session_session := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_session);
    
    select cast(uuid_generate_v4() as varchar) INTO v_session_session;
    
    
    
	--Asignacion de Token para la session
	INSERT INTO INT_TSESSION (
				SESSION_SESSION,
				SESSION_PARTNR,
				SESSION_TKN,
				SESSION_DATE_STAR,
				SESSION_DATE_VALIDATE,
				SESSION_STATE
		)values(
				v_session_session,
				p_partnr_partnr,
				p_token,
				CURRENT_TIMESTAMP,
				CURRENT_TIMESTAMP + (1 * interval '1 hour'),
				'ACT'
		);
	
	p_session_session:=v_session_session;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$starsession"(text, text, text)
    
