-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$startmatch"
(
	p_prcs 				TEXT,
	p_player_player		TEXT,
	p_game_game			TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj,
	OUT p_match_match	TEXT
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

	c_smatch CURSOR FOR				
	select nextval ('gms_smatch');
	
	

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
	v_nextval_match 	NUMERIC;
	v_match_match 		VARCHAR(50);
	v_cod_mensaje		VARCHAR(100):='OK';

BEGIN
	
	OPEN c_smatch;
	FETCH c_smatch INTO v_nextval_match;
	CLOSE c_smatch;
	
	

	--v_match_match := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_match);
    select cast(uuid_generate_v4() as varchar) INTO v_match_match;
	
    --Se crea un nuevo Match 
    Insert Into GMS_TMATCH(
            MATCH_MATCH,
            MATCH_PLAYER,
            MATCH_GAME,
            MATCH_START,
            MATCH_FINAL_SCORE,
            MATCH_GAME_SCORE,
            MATCH_STATE,
            MATCH_RESULT
    )VALUES(
            v_match_match,
            p_player_player,
            p_game_game,
            CURRENT_TIMESTAMP,
            0,
            0,
            'INI',
            'PEND'
    );
		
	p_match_match = v_match_match;

	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$startmatch"(text, text, text)
    

