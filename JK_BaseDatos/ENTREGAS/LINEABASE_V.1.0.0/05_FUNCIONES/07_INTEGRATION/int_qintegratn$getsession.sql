-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             02/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qintegratn$getsession"
(
	p_prcs              text,
	p_session_session   text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tsession      TYPE_TT_IN_SESSION
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_session CURSOR FOR
    SELECT
		SESSION_SESSION,
		SESSION_PARTNR,
		SESSION_TKN,
		SESSION_DATE_STAR,
		SESSION_DATE_VALIDATE,
		SESSION_DATE_END,
		SESSION_STATE
        FROM INT_TSESSION
        WHERE p_session_session = SESSION_SESSION;
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_in_session TYPE_TO_IN_SESSION;
    tt_in_session TYPE_TT_IN_SESSION;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
	IF(int_qintegratn$validatesession(p_session_session)='true') THEN
  
    FOR i IN c_session
    LOOP

        to_in_session := ROW (i.SESSION_SESSION,i.SESSION_PARTNR,i.SESSION_TKN,i.SESSION_DATE_STAR,i.SESSION_DATE_VALIDATE,i.SESSION_DATE_END,i.SESSION_STATE)::TYPE_TO_IN_SESSION;
        tt_in_session[COALESCE(array_length(tt_in_session, 1), 0)] := to_in_session;

    END LOOP;

    p_tsession := tt_in_session;
    
	END IF;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$getsession"(text, text)
    
