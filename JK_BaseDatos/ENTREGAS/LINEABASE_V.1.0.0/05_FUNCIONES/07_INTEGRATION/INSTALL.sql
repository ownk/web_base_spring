--Funciones
\i 05_FUNCIONES/07_INTEGRATION/int_qintegratn$validatesession.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qpartnr$getpartner.sql


--Procedimientos
\i 05_FUNCIONES/07_INTEGRATION/int_qpartnr$getpartnrbykeys.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qpartnr$getpartners.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qpartnr$createpartner.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qpartnr$inactivepartner.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qintegratn$starsession.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qintegratn$starsmatch.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qintegratn$finalizesession.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qintegratn$updatesmatch.sql

\i 05_FUNCIONES/07_INTEGRATION/gms_qplayer$startmatch.sql

\i 05_FUNCIONES/07_INTEGRATION/gms_qplayer$updatematch.sql

\i 05_FUNCIONES/07_INTEGRATION/int_qintegratn$getsession.sql


--API
\i 05_FUNCIONES/07_INTEGRATION/api_int_qpartnr$getpartnrbykeys.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qpartnr$getpartners.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qpartnr$createpartner.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qpartnr$inactivepartner.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qintegratn$starsession.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qintegratn$finalizesession.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qplayer$startmatch.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qplayer$updatematch.sql

\i 05_FUNCIONES/07_INTEGRATION/api_int_qintegratn$getsession.sql

