-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             03/03/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$deleteuser"
(
	p_prcs              text,
    p_user_user         text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    v_client_client VARCHAR(50);
    v_player_player VARCHAR(50);
    v_access_code   VARCHAR(50);
    v_acnt_acnt	    VARCHAR(50);
	
	c_uscl CURSOR (vc_client_client VARCHAR) FOR
	SELECT USCL_USER
        FROM CLI_TUSCL
        WHERE USCL_CLIENT=vc_client_client;
		
	c_acnt CURSOR (vc_player_player VARCHAR) FOR
	SELECT acnt_acode_init,acnt_acnt
	FROM acn_tacnt,gms_tplacn
	WHERE 	PLACN_ACNT=ACNT_ACNT AND
			PLACN_PLAYER = v_player_player; 
	

BEGIN
    p_cod_rpta := 'OK';
	--Se elimina rol del usuario
	DELETE FROM AUT_TUROL
	WHERE UROL_USER = p_user_user;
	
	--Se eliminan OTPU asociadas al User
	DELETE FROM AUT_TOTPU
	WHERE OTPU_USER = p_user_user;
	
	--Se elimina registro de cambios de estado del Usuario
	DELETE FROM AUT_TSTUS
	WHERE STUS_USER = p_user_user;
	
	
	--Se elimina cuenta del jugador
	Select player_player
	INTO v_player_player
	FROM GMS_TPLAYER
	WHERE PLAYER_USER=p_user_user;

	
	FOR j IN c_acnt (v_player_player)
	LOOP
		v_access_code:=j.acnt_acode_init;
		v_acnt_acnt:=j.acnt_acnt;
		
		UPDATE cli_tacode
		SET ACODE_STATE = 'ACT'
		WHERE ACODE_ACODE=v_access_code;
		
		DELETE FROM acn_towner
		WHERE OWNER_ACNT = v_acnt_acnt;
		
		DELETE FROM acn_tacac
		WHERE ACAC_ACNT = v_acnt_acnt;
		
		DELETE FROM GMS_TPLACN
		WHERE PLACN_ACNT = v_acnt_acnt;
		
		DELETE FROM acn_tacnt
		WHERE ACNT_ACNT = v_acnt_acnt;
		
	END LOOP;	

	
    -- Se elimina el Player 
    DELETE FROM GMS_TPLAYER
	WHERE PLAYER_USER = p_user_user;
	
	
    -- Se elimina el Cliente 
    SELECT DISTINCT USCL_CLIENT 
        INTO v_client_client
        FROM CLI_TUSCL
        WHERE USCL_USER=p_user_user;
	
	UPDATE cli_trcode
	SET rcode_client = null,
		rcode_state = 'ACT'
	WHERE rcode_client=v_client_client;
	
	DELETE FROM CLI_TUSCL
    WHERE USCL_CLIENT=v_client_client;
	
    FOR i IN c_uscl(v_client_client)
	LOOP
		DELETE FROM AUT_TUSER
		WHERE USER_USER = i.USCL_USER; 
	END LOOP;
	
    
	
    DELETE FROM CLI_TCLIENT
    WHERE CLIENT_CLIENT=v_client_client;
	
   
    -- Se elimina el Usuario 
    DELETE FROM AUT_TUSER
	WHERE USER_USER = p_user_user;
    
   
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

    
