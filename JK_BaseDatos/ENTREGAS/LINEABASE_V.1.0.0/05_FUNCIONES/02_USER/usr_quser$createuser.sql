-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "usr_quser$createuser"
(
	p_prcs 				TEXT,
	p_tuser 			type_tt_au_user,
	p_pgcrypto_key      TEXT,
	p_pgcrypto_alg      TEXT,
	p_otpu			    TEXT,
	OUT p_cod_rpta 		text,
	OUT p_user_user		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	c_suser  CURSOR FOR				
	select nextval ('aut_suser');
	
	c_sotpu  CURSOR FOR				
	select nextval ('aut_sotpu');
	
	v_nextval_user 		NUMERIC;
	v_nextval_otpu 		NUMERIC;
	v_email_validado  	NUMERIC;
	v_nick				TEXT;
	v_email				TEXT;
	
	v_cod_mensaje	VARCHAR(100):='OK';
	
	v_user_user VARCHAR(50);
	v_otpu_otpu VARCHAR(50);

BEGIN

	FOR i IN 0..COALESCE(array_length(p_tuser, 1)-1, 0)
	LOOP
		v_user_user = p_tuser[i].USER_USER;
        v_nick = p_tuser[i].USER_NICK;
		v_email = p_tuser[i].USER_EMAIL;    

	IF (usr_quser$validatenick(v_nick)='true')THEN
		v_cod_mensaje = 'ERR_EXIST_NICK';
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta(v_cod_mensaje, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	END IF;
	
	IF (usr_quser$validateemail(v_email)='true')THEN
		v_cod_mensaje = 'ERR_EXIST_EMAIL';
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta(v_cod_mensaje, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	END IF;
	
	
	
	IF(v_cod_mensaje='OK')THEN
    
        
        v_nick = md5(upper(p_tuser[i].USER_NICK));
		v_email = md5(upper(p_tuser[i].USER_EMAIL));
	
		IF (v_user_user IS NULL)THEN
		
			OPEN c_suser;
			FETCH c_suser INTO v_nextval_user;
			CLOSE c_suser;
			
			OPEN c_sotpu;
			FETCH c_sotpu INTO v_nextval_otpu;
			CLOSE c_sotpu;
			
			
			v_user_user := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_user);
			v_otpu_otpu := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_otpu);

			--Crear Usuario
			Insert Into AUT_TUSER(
					USER_USER,
					USER_NAME,
					USER_NICK,
					USER_PASS,
					USER_EMAIL,
					USER_STATE,
					USER_DCREA,
					USER_HASH_NICK,
					USER_HASH_EMAIL,
					USER_FLAG_EMAIL,
					USER_FLAG_TECON
			)VALUES(
					v_user_user,
					pgp_sym_encrypt(p_tuser[i].USER_NAME, p_pgcrypto_key,p_pgcrypto_alg),
					pgp_sym_encrypt(p_tuser[i].USER_NICK, p_pgcrypto_key,p_pgcrypto_alg),
					pgp_sym_encrypt(p_tuser[i].USER_PASS, p_pgcrypto_key,p_pgcrypto_alg),
					pgp_sym_encrypt(p_tuser[i].USER_EMAIL, p_pgcrypto_key,p_pgcrypto_alg),
					'INA',
					CURRENT_TIMESTAMP,
					v_nick,
					v_email,
					'INV',
					'VAL'
			);
			
		--Asignar el rol de user al nuevo usuario
			INSERT INTO AUT_TUROL (
					UROL_USER,
					UROL_ROL
			)values(
					v_user_user,
					'USER'
			);
		--Creacion de registro en la tabla de log de estados del usuario
			INSERT INTO AUT_TSTUS (
					STUS_USER,
					STUS_STATE_UPDATE,
					STUS_DATE_UPDATE
			)values(
					v_user_user,
					'INA',
					CURRENT_TIMESTAMP
			);
		ELSE 
		
			UPDATE AUT_TUSER SET
					USER_NAME=pgp_sym_encrypt(p_tuser[i].USER_NAME, p_pgcrypto_key,p_pgcrypto_alg),
					USER_NICK=pgp_sym_encrypt(p_tuser[i].USER_NICK, p_pgcrypto_key,p_pgcrypto_alg),
					USER_PASS=pgp_sym_encrypt(p_tuser[i].USER_PASS, p_pgcrypto_key,p_pgcrypto_alg),
					USER_EMAIL=pgp_sym_encrypt(p_tuser[i].USER_EMAIL, p_pgcrypto_key,p_pgcrypto_alg),
					USER_HASH_NICK=v_nick,
					USER_HASH_EMAIL=v_email		
			WHERE USER_USER=v_user_user;
			
		END IF;
	--Asignacion de OTP
		INSERT INTO AUT_TOTPU (
				OTPU_OTPU,
				OTPU_OTP,
				OTPU_DATE_STAR,
				OTPU_DATE_END,
				OTPU_STATE,
				OTPU_USER
		)values(
				v_otpu_otpu,
				p_otpu,
				CURRENT_TIMESTAMP,
				CURRENT_TIMESTAMP + (1 * interval '1 hour'),
				'ACT',
				v_user_user		
		);
	
	p_user_user:=v_user_user;
	END IF;
	
	END LOOP;
	
	
    IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
	p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, 'ERROR', p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$createuser"(text, TYPE_TT_AU_USER, text, text, text )
    

