-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$modulesbyuser"
(
	p_prcs          text,
	p_user_user     text,
	OUT p_cod_rpta  text,
	OUT p_errores   TYPE_TT_GE_ERRO,
	OUT p_mensajes  TYPE_TT_GE_MNSJ,
    OUT p_tmodulo   TYPE_TT_AU_MDLO
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_modulesbyuser CURSOR FOR
    SELECT DISTINCT
        aut_tmdlo.*
        FROM AUT_TUROL, AUT_TSROL, AUT_TSRVC, aut_tmdlo, aut_tsurl
        WHERE UROL_USER = p_user_user 
        AND UROL_ROL = SROL_ROL 
        AND SRVC_SRVC = SROL_SRVC 
        AND surl_srvc = SRVC_SRVC 
        AND mdlo_mdlo = SRVC_MDLO 
        AND SROL_VISIBLE = 'S'
        ORDER BY mdlo_orden ASC;

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_au_mdlo type_to_au_mdlo;
    tt_au_mdlo type_tt_au_mdlo;

    v_crta_http_gral CHARACTER VARYING(5) := 'OK';

    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN

    FOR i IN c_modulesbyuser

    LOOP

        to_au_mdlo := ROW (i.mdlo_mdlo, i.mdlo_descri, i.mdlo_nomb)::type_to_au_mdlo;
        tt_au_mdlo[COALESCE(array_length(tt_au_mdlo, 1), 0)] := to_au_mdlo;

    END LOOP;

    p_tmodulo := tt_au_mdlo;
    p_cod_rpta := COALESCE(v_crta_http_gral, 'OK');
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, p_mensajes)
                INTO p_mensajes;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$modulesbyuser"(text, text)
    
