-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$homepagebyuser"
(
	p_prcs          text,
	p_user_user     text,
	OUT p_cod_rpta  text,
	OUT p_errores   TYPE_TT_GE_ERRO,
	OUT p_mensajes  TYPE_TT_GE_MNSJ,
    OUT p_surl_url  text
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_paginausuario CURSOR FOR
    SELECT
        surl_url
        FROM aut_trsin, aut_tsurl, AUT_TUROL
        WHERE rsin_srvc = surl_srvc 
        AND UROL_USER = p_user_user 
        AND UROL_ROL = rsin_rol 
        AND SURL_TIPO = 'P'
        LIMIT 1;

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;
    v_crta_http_gral CHARACTER VARYING(5) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN

    OPEN c_paginausuario;
    FETCH c_paginausuario INTO p_surl_url;
    CLOSE c_paginausuario;

    p_cod_rpta := COALESCE(v_crta_http_gral, 'OK');
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$homepagebyuser"(text, text)
    
