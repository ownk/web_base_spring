-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$getmenubyuser"
(
	p_prcs          text,
	p_user_user     text,
	OUT p_cod_rpta  text,
	OUT p_errores   TYPE_TT_GE_ERRO,
	OUT p_mensajes  TYPE_TT_GE_MNSJ,
    OUT p_tservicio TYPE_TT_AU_SRVC
)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    c_menuporusuario CURSOR FOR

    SELECT DISTINCT
        aut_tmenu.*, aut_tmsrv.*, AUT_TSRVC.*, aut_tmdlo.*, aut_tsurl.*
        FROM AUT_TUROL, AUT_TSROL, AUT_TSRVC, aut_tmdlo, aut_tsurl, aut_tmsrv, aut_tmenu
        WHERE UROL_USER = p_user_user 
        AND UROL_ROL = SROL_ROL 
        AND SRVC_SRVC = SROL_SRVC 
        AND surl_srvc = SRVC_SRVC   
        AND surl_tipo = 'P' 
        AND mdlo_mdlo = SRVC_MDLO 
        AND SROL_VISIBLE = 'S' 
        AND SRVC_SRVC = msrv_srvc 
        AND menu_menu = msrv_menu
        ORDER BY menu_orden, msrv_orden ASC;

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_au_servicio TYPE_TO_AU_SRVC;
    tt_au_servicio TYPE_TT_AU_SRVC;

    v_crta_http_gral CHARACTER VARYING(5) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;


BEGIN

    FOR i IN c_menuporusuario

    LOOP

        to_au_servicio := ROW (i.SRVC_SRVC, i.SRVC_MDLO, i.SRVC_DESCRI, i.SRVC_NOMB, i.surl_url, i.mdlo_mdlo, i.mdlo_descri, i.mdlo_nomb, i.menu_nombre, i.menu_icon, i.menu_menu)::TYPE_TO_AU_SRVC;
        tt_au_servicio[COALESCE(array_length(tt_au_servicio, 1), 0)] := to_au_servicio;

    END LOOP;

    p_tservicio := tt_au_servicio;
    p_cod_rpta := COALESCE(v_crta_http_gral, 'OK');
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;

    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$getmenubyuser"(text, text)
    
