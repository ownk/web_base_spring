-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             22/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "usr_quser$validateacode"(
	p_codigo_acceso text,
	p_pgcrypto_key	text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_cant_codes NUMERIC;
	v_date TIMESTAMP;

BEGIN
	
	v_date = CURRENT_TIMESTAMP;

    SELECT

        COUNT(*)

        INTO STRICT v_cant_codes

        FROM CLI_TACODE
        WHERE 	p_codigo_acceso =  pgp_sym_decrypt(ACODE_CODE::bytea, p_pgcrypto_key) AND 
				ACODE_STATE = 'ACT'			 AND
				v_date <= ACODE_DATE_VALIDITY;

    IF (v_cant_codes > 0) THEN

        RETURN TRUE;

    ELSE
        
        --Actualizacion del estado del codigo de acceso activo a invalido
		UPDATE CLI_TACODE
		SET ACODE_STATE='INV'
		WHERE 	p_codigo_acceso =  pgp_sym_decrypt(ACODE_CODE::bytea, p_pgcrypto_key) AND 
				ACODE_STATE='ACT';

       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "usr_quser$validateacode"(text, text)
    
