-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION juegohacker_us."usr_quser$validateregister"
(
	p_prcs              text,
	p_user_user			text,
    p_otpu			    text,
    p_codigo_acceso	    text,
	OUT p_cod_rpta      text,
	OUT p_errores       juegohacker_us.TYPE_TT_GE_ERRO,
	OUT p_mensajes      juegohacker_us.TYPE_TT_GE_MNSJ
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro juegohacker_us.TYPE_TO_GE_ERRO;
    tt_ge_erro juegohacker_us.TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje juegohacker_us.TYPE_TT_GE_MNSJ;
	
	c_validacion CURSOR (vc_token VARCHAR(200),vc_date TIMESTAMP)FOR
	select count(*) FROM AUT_TOTPU
	WHERE vc_token = OTPU_OTP AND vc_date<=OTPU_DATE_END AND OTPU_STATE='ACT';
		
	v_cod_mensaje	VARCHAR(100):='OK';

	

BEGIN
	
	IF (juegohacker_us.usr_quser$validateotpu(p_otpu,p_user_user)='false')THEN
		v_cod_mensaje = 'ERR_OTP_INVAL';
		SELECT
			*
			FROM juegohacker_us.api_ge_qutils$generartypemensajerpta(v_cod_mensaje, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	END IF;
	
	IF (v_cod_mensaje='OK')THEN
		-- Activar el Usuario
		UPDATE AUT_TUSER
		SET USER_STATE='ACT',USER_FLAG_EMAIL='VAL'
		WHERE USER_USER = p_user_user;
		
		-- Activar el Player
		UPDATE GMS_TPLAYER
		SET PLAYER_STATE='ACT', PLAYER_DATEAC=CURRENT_TIMESTAMP
		WHERE PLAYER_USER = p_user_user;
		
		--Actualizacion del estado de token a usado
		UPDATE AUT_TOTPU
		SET OTPU_STATE='USE',
			OTPU_DATE_USED = CURRENT_TIMESTAMP
		WHERE 	OTPU_OTP=p_otpu AND	
				OTPU_USER=p_user_user;
				
		--Actualizacion del estado de codigo de acceso a usado
		UPDATE CLI_TACODE
		SET ACODE_STATE='USE'
		WHERE 	ACODE_CODE=p_codigo_acceso;
				
		--Creacion de registro en la tabla de log de estados del usuario
		INSERT INTO juegohacker_us.AUT_TSTUS (
			STUS_USER,
			STUS_STATE_UPDATE,
			STUS_DATE_UPDATE
		)values(
			p_user_user,
			'ACT',
			CURRENT_TIMESTAMP
		);
	END IF;
	
    IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::juegohacker_us.TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
	p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::juegohacker_us.TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::juegohacker_us.TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::juegohacker_us.TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM juegohacker_us.api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION juegohacker_us."usr_quser$validateregister"(text, text, text, text)
    
