-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$getuser"
(
	p_user_user         text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_AU_USER
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_usuario CURSOR FOR
    SELECT
        USER_USER,
        pgp_sym_decrypt(USER_NAME::bytea,  p_pgcrypto_key) USER_NAME, 
        pgp_sym_decrypt(USER_NICK::bytea,  p_pgcrypto_key) USER_NICK, 
        pgp_sym_decrypt(USER_PASS::bytea,  p_pgcrypto_key) USER_PASS,
        pgp_sym_decrypt(USER_EMAIL::bytea, p_pgcrypto_key) USER_EMAIL,
        USER_STATE,
		USER_DCREA,
        CASE USER_STATE
            WHEN 'INA' THEN 'Inactivo'
            WHEN 'ACT' THEN 'Activo'
            WHEN 'PPA' THEN 'Pendiente'
            WHEN 'PRE' THEN 'Precargado'
            ELSE 'Other'
        END AS USER_STATE_DESC
        FROM AUT_TUSER
        WHERE USER_USER = p_user_user;

    to_aut_user TYPE_TO_AU_USER;


BEGIN
  
  
	--Obtener el usuario por ID
	
    FOR i IN c_usuario
    LOOP

        to_aut_user := ROW (i.USER_USER,i.USER_NAME,i.USER_NICK,i.USER_PASS,i.USER_EMAIL,i.USER_STATE,i.USER_DCREA, i.USER_STATE_DESC)::TYPE_TO_AU_USER;

    END LOOP;

    return to_aut_user;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$getuser"(text, text)
    
