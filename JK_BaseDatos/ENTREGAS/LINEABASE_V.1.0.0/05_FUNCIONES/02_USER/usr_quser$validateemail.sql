-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             22/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "usr_quser$validateemail"(
	p_email text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_cant_user NUMERIC;
	v_email TEXT;

BEGIN

    v_email = md5(upper(p_email));

    SELECT

        COUNT(*)

        INTO STRICT v_cant_user

        FROM AUT_TUSER

        WHERE USER_HASH_EMAIL = v_email; 

    IF (v_cant_user > 0) THEN

        RETURN TRUE;

    ELSE

       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "usr_quser$validateemail"(text)
    
