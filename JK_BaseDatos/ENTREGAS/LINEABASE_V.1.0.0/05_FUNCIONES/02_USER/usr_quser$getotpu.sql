-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             03/03/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "usr_quser$getotpu"
(
    p_user_user         text
)
RETURNS TYPE_TO_AU_OTPU
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_otpu CURSOR FOR
    SELECT
        OTPU_OTPU,
        OTPU_OTP, 
        OTPU_DATE_STAR, 
        OTPU_DATE_END,
        OTPU_DATE_USED,
        OTPU_STATE,
        OTPU_USER
        FROM AUT_TOTPU
        WHERE OTPU_USER = p_user_user;

    to_aut_otpu TYPE_TO_AU_OTPU;
	v_otpu_otpu VARCHAR(50);
	v_otpu_otp VARCHAR(50);
	v_otpuv BOOLEAN;

BEGIN

	OPEN c_otpu;
	FETCH c_otpu INTO v_otpu_otpu,v_otpu_otp;
	CLOSE c_otpu;
  
	v_otpuv:=usr_quser$validateotpu(v_otpu_otp,p_user_user);
		
	
    --Obtener el otpu por usuario
    
    FOR i IN c_otpu
    LOOP

        to_aut_otpu := ROW (i.OTPU_OTPU,i.OTPU_OTP,i.OTPU_DATE_STAR,i.OTPU_DATE_END,i.OTPU_DATE_USED,i.OTPU_STATE,i.OTPU_USER)::TYPE_TO_AU_OTPU;

    END LOOP;

    return to_aut_otpu;

END;

$BODY$;

    
