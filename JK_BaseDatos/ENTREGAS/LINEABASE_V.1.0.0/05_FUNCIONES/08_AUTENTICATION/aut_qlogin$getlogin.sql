-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             23/01/2020      ownk           Se crea funcion para obtener informacion del login
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_qlogin$getlogin"
(
	p_prcs 				TEXT,
	p_pgcrypto_key      TEXT,
    p_userid            TEXT,
    OUT p_tlogin 		type_tt_au_login,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 

AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_au_login TYPE_TO_AU_LOGIN;
    tt_au_login TYPE_TT_AU_LOGIN;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	C_LOGIN CURSOR FOR
        SELECT LOGIN_LOGIN,
               LOGIN_USER,
               pgp_sym_decrypt(LOGIN_NICK::bytea,  p_pgcrypto_key) LOGIN_NICK, 
               pgp_sym_decrypt(LOGIN_PASS::bytea,  p_pgcrypto_key) LOGIN_PASS,
               LOGIN_DATEREG,
               LOGIN_BROWSER,
               pgp_sym_decrypt(LOGIN_IP::bytea,  p_pgcrypto_key) LOGIN_IP
        FROM AUT_TLOGIN
        WHERE LOGIN_USER = p_userid
        ORDER BY LOGIN_DATEREG DESC 
        LIMIT 10;
            
BEGIN

		--Consulta Login
        
		FOR I IN C_LOGIN
        LOOP
        
            to_au_login := ROW (i.LOGIN_LOGIN,i.LOGIN_USER,i.LOGIN_NICK,i.LOGIN_PASS,i.LOGIN_DATEREG,i.LOGIN_BROWSER,i.LOGIN_IP)::TYPE_TO_AU_LOGIN;
            tt_au_login[COALESCE(array_length(tt_au_login, 1), 0)] := to_au_login;
            
        END LOOP;    
            
	p_tlogin := tt_au_login;
   	
    
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

    WHEN OTHERS THEN
    
        p_cod_rpta := 'ERROR';
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;

            SELECT *
            FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
    
            p_mensajes := v_mensaje;

END;

$BODY$;

    --ALTER FUNCTION "aut_qlogin$getlogin"(text, text, text)
    

