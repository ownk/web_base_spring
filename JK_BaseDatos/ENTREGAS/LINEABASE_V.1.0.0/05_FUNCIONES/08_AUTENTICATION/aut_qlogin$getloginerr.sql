-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi?n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             11/02/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "aut_qlogin$getloginerr"
(
	p_prcs 				TEXT,
	p_user_user         TEXT,
	p_pgcrypto_key      TEXT,
	OUT p_cod_rpta 		text,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj,
    OUT p_tiflgn 		type_tt_au_iflgn
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 

AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_au_iflgn TYPE_TO_AU_IFLGN;
    tt_au_iflgn TYPE_TT_AU_IFLGN;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	c_loginok CURSOR FOR
        SELECT LGERR_LGERR,
               LGERR_USER,
               LGERR_BROWSER,
               pgp_sym_decrypt(LGERR_IP::bytea,  p_pgcrypto_key) LGERR_IP,
               LGERR_SESSION
        FROM AUT_TLGERR
        WHERE LGERR_USER=p_user_user;
        
BEGIN

		--Consulta LoginOk
        
		FOR I IN c_loginok
        LOOP
        
            to_au_iflgn := ROW (i.LGERR_LGERR,i.LGERR_USER,i.LGERR_BROWSER,i.LGERR_IP,i.LGERR_SESSION,null)::TYPE_TO_AU_IFLGN;
            tt_au_iflgn[COALESCE(array_length(tt_au_iflgn, 1), 0)] := to_au_iflgn;
            
        END LOOP;    
            
	p_tiflgn := tt_au_iflgn;
   	
    
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

    WHEN OTHERS THEN
    
        p_cod_rpta := 'ERROR';
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;

            SELECT *
            FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
    
            p_mensajes := v_mensaje;

END;

$BODY$;

    --ALTER FUNCTION "aut_qlogin$getloginerr"(text, text, text)
    