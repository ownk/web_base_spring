-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             05/02/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "aut_quser$getpasswordhistory"
(
	p_prcs              text,
	p_user_user         text,
	p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tplayer       TYPE_TT_PL_PLAYER,
    OUT p_tpassch       TYPE_TT_AU_PASSCH
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	--Cursor para traer el historial de passwords de un usuario
	
	c_passch_user CURSOR FOR
	SELECT 	
		PASSCH_PASSCH, 
        PASSCH_USER,
        pgp_sym_decrypt(PASSCH_PASSWORD::bytea,p_pgcrypto_key) PASSCH_PASSWORD,
        pgp_sym_decrypt(PASSCH_NEWPASS::bytea,p_pgcrypto_key) PASSCH_NEWPASS,
        PASSCH_DATEREG,
        PASSCH_BROWSER,
        pgp_sym_decrypt(PASSCH_IP::bytea,p_pgcrypto_key) PASSCH_IP
		FROM AUT_TPASSCH   
		WHERE PASSCH_USER=p_user_user;
            
            
	
	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    
    to_au_passch TYPE_TO_AU_PASSCH; 
	tt_au_passch TYPE_TT_AU_PASSCH;

	v_code VARCHAR(50);

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	
	

BEGIN


    FOR i IN c_passch_user
    LOOP
		to_au_passch := ROW (i.PASSCH_PASSCH,i.PASSCH_USER,i.PASSCH_PASSWORD,i.PASSCH_NEWPASS,i.PASSCH_DATEREG,i.PASSCH_BROWSER,i.PASSCH_IP)::TYPE_TO_AU_PASSCH;
        tt_au_passch[COALESCE(array_length(tt_au_passch, 1), 0)] := to_au_passch;
		
    END LOOP;
    p_tpassch := tt_au_passch;

    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "aut_quser$getpasswordhistory"(text,text,text)
    
