-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             05/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getacodesclient"
(
	p_prcs              text,
	p_client_client     text,
    p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tacode       TYPE_TT_CL_ACODE
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_acodes CURSOR FOR
    SELECT
        ACODE_ACODE,
        pgp_sym_decrypt(ACODE_CODE::bytea,  p_pgcrypto_key) ACODE_CODE, 
        ACODE_STATE, 
        ACODE_CLIENT,
        ACODE_TEMPLT,
        ACODE_DATE_START,
		ACODE_DATE_VALIDITY
    FROM CLI_TACODE
    WHERE ACODE_CLIENT = p_client_client;
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_cl_acode TYPE_TO_CL_ACODE;
    tt_cl_acode TYPE_TT_CL_ACODE;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
    FOR i IN c_acodes
    LOOP
        to_cl_acode := ROW (i.ACODE_ACODE,i.ACODE_CODE,i.ACODE_STATE,i.ACODE_CLIENT,i.ACODE_TEMPLT,i.ACODE_DATE_START,i.ACODE_DATE_VALIDITY)::TYPE_TO_CL_ACODE;
        tt_cl_acode[COALESCE(array_length(tt_cl_acode, 1), 0)] := to_cl_acode;

    END LOOP;

    p_tacode := tt_cl_acode;
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getacodesclient"(text, text, text)
    
