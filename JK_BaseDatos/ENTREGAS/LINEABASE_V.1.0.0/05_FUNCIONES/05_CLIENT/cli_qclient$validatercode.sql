-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             25/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "cli_qclient$validatercode"(
	p_codigo_acceso text,
	p_pgcrypto_key	text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_cant_codes NUMERIC;
	v_date TIMESTAMP;

BEGIN
	
	v_date = CURRENT_TIMESTAMP;

    SELECT

        COUNT(*)

        INTO STRICT v_cant_codes

        FROM CLI_TRCODE
        WHERE 	p_codigo_acceso =  pgp_sym_decrypt(RCODE_CODE::bytea, p_pgcrypto_key) AND 
				RCODE_STATE = 'ACT'			 AND
				v_date <= RCODE_DATE_VALIDITY;

    IF (v_cant_codes > 0) THEN

        RETURN TRUE;

    ELSE

        UPDATE CLI_TRCODE
		SET RCODE_STATE='INV'
		WHERE 	p_codigo_acceso =  pgp_sym_decrypt(RCODE_CODE::bytea, p_pgcrypto_key) AND 
				RCODE_STATE='ACT';

       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$validatercode"(text, text)
    
