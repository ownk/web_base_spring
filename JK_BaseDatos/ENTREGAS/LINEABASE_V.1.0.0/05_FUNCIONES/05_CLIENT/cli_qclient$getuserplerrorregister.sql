-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             31/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getuserplerrorregister"
(
	p_prcs              text,
    p_client_client     text,
    p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tuser         TYPE_TT_AU_USER,
    OUT p_totpu         TYPE_TT_AU_OTPU

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_user_user CURSOR FOR
    SELECT
        USER_USER
        FROM AUT_TUSER,AUT_TOTPU,GMS_TPLAYER
        WHERE USER_USER=OTPU_USER AND PLAYER_USER=USER_USER AND 
        USER_STATE='INA' AND OTPU_STATE='ACT' and PLAYER_STATE='INA';
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_aut_user TYPE_TO_AU_USER;
    tt_aut_user TYPE_TT_AU_USER;
    
	to_aut_otpu TYPE_TO_AU_OTPU;
    tt_aut_otpu TYPE_TT_AU_OTPU;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    v_users_client TYPE_TT_AU_USER;
    
    GET_CLIENT_PLAYERS RECORD;
    resultado boolean;
BEGIN

    p_cod_rpta := 'OK';

    SELECT
        *
        FROM cli_qclient$getclientplayers(p_prcs,p_client_client,p_pgcrypto_key)
        INTO GET_CLIENT_PLAYERS;
    
  
   
    v_users_client := GET_CLIENT_PLAYERS.P_TUSER;
    
    
    
	FOR j IN 0..COALESCE(array_length(v_users_client, 1)-1, 0)
    LOOP
        
        FOR i IN c_user_user
        LOOP
            
            
            
            IF (v_users_client[j].USER_USER=i.USER_USER)THEN
                
                to_aut_user = usr_quser$getuser(i.USER_USER,p_pgcrypto_key);
                tt_aut_user[COALESCE(array_length(tt_aut_user, 1), 0)] := to_aut_user;
				
				to_aut_otpu = usr_quser$getotpu(i.USER_USER);
				tt_aut_otpu[COALESCE(array_length(tt_aut_user, 1), 0)] := to_aut_otpu;
                
            END IF;
        END LOOP;
    END LOOP;
    p_tuser := tt_aut_user;
    p_totpu := tt_aut_otpu;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getuserplerrorregister"(text, text, text)
    
