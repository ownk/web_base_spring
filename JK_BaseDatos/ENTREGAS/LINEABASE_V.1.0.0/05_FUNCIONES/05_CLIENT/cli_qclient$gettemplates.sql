-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             16/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$gettemplates"
(
	p_prcs              text,
	p_client_client     text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_ttemplt       TYPE_TT_CL_TEMPLT,
	OUT p_tgame       	TYPE_TT_GM_GAME,
	OUT p_ttempgm      	TYPE_TT_CL_TEMPGM

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_templt CURSOR FOR
    SELECT
        TEMPLT_TEMPLT, 
        TEMPLT_CLIENT,
        TEMPLT_DESCRI
	FROM CLI_TTEMPLT
    WHERE TEMPLT_CLIENT = p_client_client;
	
	
	c_games CURSOR FOR
	SELECT DISTINCT GAME_GAME
	FROM GMS_TGAME,CLI_TTEMPGM,CLI_TTEMPLT
	WHERE GAME_GAME=TEMPGM_GAME AND 
		  TEMPLT_CLIENT=p_client_client;
	
        
		
	c_tempgm CURSOR FOR
	SELECT TEMPGM_GAME,TEMPGM_TEMPLT
	FROM CLI_TTEMPGM,CLI_TTEMPLT
	WHERE 	TEMPLT_CLIENT = p_client_client AND
			TEMPLT_TEMPLT=TEMPGM_TEMPLT;
	

	
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_cl_templt TYPE_TO_CL_TEMPLT;
    tt_cl_templt TYPE_TT_CL_TEMPLT;
	
    to_cl_tempgm TYPE_TO_CL_TEMPGM;
    tt_cl_tempgm TYPE_TT_CL_TEMPGM;
	
	to_gm_game TYPE_TO_GM_GAME;
    tt_gm_game TYPE_TT_GM_GAME;

	v_game_game VARCHAR(50);
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
	resultado BOOLEAN;

BEGIN
  
    FOR i IN c_templt
    LOOP
        to_cl_templt := ROW (i.TEMPLT_TEMPLT,i.TEMPLT_CLIENT,i.TEMPLT_DESCRI)::TYPE_TO_CL_TEMPLT;
        tt_cl_templt[COALESCE(array_length(tt_cl_templt, 1), 0)] := to_cl_templt;		
    END LOOP;

    p_ttemplt := tt_cl_templt;
	 
	FOR j IN c_games
	LOOP
			
		to_gm_game=gms_qgame$getgame(j.GAME_GAME);   
		tt_gm_game[COALESCE(array_length(tt_gm_game, 1), 0)] := to_gm_game;
	END LOOP;
	
    p_tgame := tt_gm_game;
	
	FOR k IN c_tempgm
	LOOP
			
		to_cl_tempgm:= ROW (k.TEMPGM_TEMPLT,k.TEMPGM_GAME)::TYPE_TO_CL_TEMPGM;
		tt_cl_tempgm[COALESCE(array_length(tt_cl_tempgm, 1), 0)] := to_cl_tempgm;
	END LOOP;
	
    p_ttempgm := tt_cl_tempgm;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$gettemplates"(text, text)
    
