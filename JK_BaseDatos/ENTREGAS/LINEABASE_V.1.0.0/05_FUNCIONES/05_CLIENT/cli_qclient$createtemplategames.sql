-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             18/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$createtemplategames"
(
	p_prcs 				TEXT,
	p_tgame				TYPE_TT_GM_GAME,
	p_templt_descri		TEXT,
	p_client_client		TEXT,
	OUT p_cod_rpta 		TEXT,
	OUT p_errores 		type_tt_ge_erro,
	OUT p_mensajes 		type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

	
	c_stemplt CURSOR FOR				
	select nextval ('cli_stemplt');
	

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
	v_cod_mensaje		VARCHAR(100):='OK';
	v_nextval_templt 	NUMERIC;
	v_templt_templt 	VARCHAR(50);
	resultado BOOLEAN;
	F$RESULT_LOGICA RECORD;

BEGIN
               
	OPEN c_stemplt;
	FETCH c_stemplt INTO v_nextval_templt;
	CLOSE c_stemplt;
			
	v_templt_templt := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_templt);		

	--Se insertan una nueva plantilla
	INSERT INTO CLI_TTEMPLT(
			TEMPLT_TEMPLT,
			TEMPLT_CLIENT,
			TEMPLT_DESCRI
	)VALUES(
			v_templt_templt,
			p_client_client,
			p_templt_descri
	);
		
		
	FOR i IN 0..COALESCE(array_length(p_tgame, 1)-1, 0)
	LOOP
			
		--Se agregan los juegos de la plantilla 
		INSERT INTO CLI_TTEMPGM(
			TEMPGM_TEMPLT,
			TEMPGM_GAME
		)VALUES(
			v_templt_templt,
			p_tgame[i].GAME_GAME
			);
	END LOOP;

		

	IF(v_cod_mensaje = 'OK')THEN
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
		
		SELECT
			*
			FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
			INTO v_mensaje;
		p_mensajes := v_mensaje;
	ELSE
		p_cod_rpta := v_cod_mensaje;
		to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
		tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
		p_errores := tt_ge_erro;
	END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$createtemplategames"(text,type_tt_gm_game,text,text)
    


