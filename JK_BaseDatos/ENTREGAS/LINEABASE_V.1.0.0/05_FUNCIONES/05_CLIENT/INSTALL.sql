--Funciones
\i 05_FUNCIONES/05_CLIENT/cli_qclient$validatercode.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getclientbyid.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getacode.sql

--Procedimientos
\i 05_FUNCIONES/05_CLIENT/cli_qclient$createclient.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$validateregister.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getclient.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getclients.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getaccountsclient.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getacodesclient.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getclientusers.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getclientplayers.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getclientbyuser.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$updateclientimage.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getacodegames.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$createacodes.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$creatercodes.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$inactivateacode.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$inactivatercode.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$deletercode.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getclientgames.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getrcodesclient.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$gettemplates.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$createacodescant.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$creatercodescant.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$createtemplategames.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getreportplclient.sql
\i 05_FUNCIONES/05_CLIENT/cli_qclient$getuserplerrorregister.sql

--API
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$validateregister.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getclient.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getclients.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getaccountsclient.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getacodesclient.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getclientusers.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getclientplayers.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getclientbyuser.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$updateclientimage.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getacodegames.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$createacodes.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$creatercodes.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$inactivateacode.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$inactivatercode.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$deletercode.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getclientgames.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getrcodesclient.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$gettemplates.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$createacodescant.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$creatercodescant.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$createtemplategames.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getreportplclient.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getreportplclient.sql
\i 05_FUNCIONES/05_CLIENT/api_cli_qclient$getuserplerrorregister.sql
