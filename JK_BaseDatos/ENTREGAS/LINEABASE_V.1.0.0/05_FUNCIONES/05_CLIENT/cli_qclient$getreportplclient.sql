-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             17/01/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getreportplclient"
(
	p_prcs              text,
	p_client_client     text,
	p_pgcrypto_key      text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tgame         TYPE_TT_GM_GAME,
    OUT p_tplayer       TYPE_TT_PL_PLAYER,
    OUT p_tmatch        TYPE_TT_GM_MATCH,
    OUT p_tlrnobj       TYPE_TT_GM_LRNOBJ,
    OUT p_tlrnogm       TYPE_TT_GM_LRNOGM

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

	
    c_lrnogms CURSOR  FOR
    SELECT distinct
        LRNOGM_LRNOBJ,LRNOGM_GAME
        FROM gms_tlrnogm;
    
    c_lrnobjs CURSOR FOR
    SELECT distinct
        LRNOBJ_LRNOBJ
        FROM GMS_TLRNOBJ;
    
    c_gms_match CURSOR (vc_player VARCHAR(50)) FOR
    SELECT 
        MIN(MATCH_MATCH) MATCH_MATCH,
        MATCH_GAME
        FROM GMS_TMATCH 
        WHERE MATCH_RESULT='WIN' AND MATCH_PLAYER=VC_PLAYER
        GROUP BY MATCH_GAME;
    
    c_games CURSOR FOR
    SELECT 
        GAME_GAME
        FROM GMS_TGAME;

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_gm_game TYPE_TO_GM_GAME;
    tt_gm_game TYPE_TT_GM_GAME;
    
    tt_pl_player TYPE_TT_PL_PLAYER;
    
    
    to_gm_match TYPE_TO_GM_MATCH;
    tt_gm_match TYPE_TT_GM_MATCH;
    
    to_gm_lrnogm TYPE_TO_GM_LRNOGM;
    tt_gm_lrnogm TYPE_TT_GM_LRNOGM;
    
    to_gm_lrnobj TYPE_TO_GM_LRNOBJ;
    tt_gm_lrnobj TYPE_TT_GM_LRNOBJ;
	
    
    v_player_player VARCHAR(50);

    
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

    resultado BOOLEAN;
    F$RESULT_LOGICA RECORD;

    
BEGIN
    
    SELECT
		*
		FROM cli_qclient$getclientplayers(p_prcs,p_client_client,p_pgcrypto_key)
        INTO F$RESULT_LOGICA; 
    tt_pl_player := F$RESULT_LOGICA.P_TPLAYER; 
    
    FOR l IN 0..COALESCE(array_length(tt_pl_player, 1)-1, 0)    
    LOOP
        
        FOR j IN c_gms_match(tt_pl_player[l].player_player)
        LOOP
                      
            to_gm_match=gms_qgame$getmatch(j.MATCH_MATCH);
            tt_gm_match[COALESCE(array_length(tt_gm_match, 1), 0)] := to_gm_match;
            
        END LOOP;

    END LOOP;

    FOR i IN c_lrnogms
    LOOP
        to_gm_lrnogm := ROW (i.LRNOGM_LRNOBJ,i.LRNOGM_GAME)::TYPE_TO_GM_LRNOGM;
        tt_gm_lrnogm[COALESCE(array_length(tt_gm_lrnogm, 1), 0)] := to_gm_lrnogm;
    END LOOP;
    
    
    FOR p IN c_lrnobjs
    LOOP
        to_gm_lrnobj=gms_qgame$getlrnobj(p.LRNOBJ_LRNOBJ);
        tt_gm_lrnobj[COALESCE(array_length(tt_gm_lrnobj, 1), 0)] := to_gm_lrnobj;
    END LOOP;

    FOR k IN c_games
    LOOP
        to_gm_game=gms_qgame$getgame(k.GAME_GAME);
        tt_gm_game[COALESCE(array_length(tt_gm_game, 1), 0)] := to_gm_game;
    END LOOP;
    
    
    p_tgame     := tt_gm_game;
    p_tmatch    := tt_gm_match;
    p_tplayer   := tt_pl_player;
    p_tlrnobj   := tt_gm_lrnobj;
    p_tlrnogm   := tt_gm_lrnogm;
   
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getreportplclient"(text, text, text)
    
