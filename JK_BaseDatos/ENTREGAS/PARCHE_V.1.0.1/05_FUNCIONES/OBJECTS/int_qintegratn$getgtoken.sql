-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             02/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qintegratn$getgtoken"
(
	p_prcs              text,
	p_gtoken_gtoken     text,
	OUT p_cod_rpta      text,
	OUT p_errores       TYPE_TT_GE_ERRO,
	OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tgtoken       TYPE_TT_IN_GTOKEN
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_gtoken CURSOR FOR
    SELECT
		GTOKEN_GTOKEN,
		GTOKEN_PLAYER,
		GTOKEN_GAME,
        GTOKEN_SAPP,
		GTOKEN_DATE_STAR,
		GTOKEN_DATE_VALIDATE,
		GTOKEN_DATE_END,
		GTOKEN_STATE
        FROM INT_TGTOKEN
        WHERE p_gtoken_gtoken = gtoken_gtoken;
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_in_GTOKEN TYPE_TO_IN_GTOKEN;
    tt_in_GTOKEN TYPE_TT_IN_GTOKEN;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN

  
	IF(int_qintegratn$validategtoken(p_gtoken_gtoken)='true') THEN
  
    FOR i IN c_GTOKEN
    LOOP

        to_in_GTOKEN := ROW (i.GTOKEN_GTOKEN,i.GTOKEN_PLAYER,i.GTOKEN_GAME,i.GTOKEN_SAPP, i.GTOKEN_DATE_STAR,i.GTOKEN_DATE_VALIDATE,i.GTOKEN_DATE_END,i.GTOKEN_STATE)::TYPE_TO_IN_GTOKEN;
        tt_in_GTOKEN[COALESCE(array_length(tt_in_GTOKEN, 1), 0)] := to_in_GTOKEN;

    END LOOP;

    p_tGTOKEN := tt_in_GTOKEN;
    
	END IF;
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$getgtoken"(text, text)
    
