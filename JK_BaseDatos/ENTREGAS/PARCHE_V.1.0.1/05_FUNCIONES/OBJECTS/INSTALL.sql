--Funciones
\i 05_FUNCIONES/OBJECTS/int_qintegratn$validategtoken.sql


--Procedimientos
\i 05_FUNCIONES/OBJECTS/int_qintegratn$starsession.sql

\i 05_FUNCIONES/OBJECTS/int_qintegratn$starsmatch.sql

\i 05_FUNCIONES/OBJECTS/int_qintegratn$getgtoken.sql

\i 05_FUNCIONES/OBJECTS/int_qintegratn$creategtoken.sql

\i 05_FUNCIONES/OBJECTS/int_qintegratn$usegtoken.sql


--API

\i 05_FUNCIONES/OBJECTS/gms_qgame$getbizplranking.sql

\i 05_FUNCIONES/OBJECTS/api_int_qplayer$startmatch.sql

\i 05_FUNCIONES/OBJECTS/api_int_qplayer$updatematch.sql

\i 05_FUNCIONES/OBJECTS/api_int_qintegratn$getgtoken.sql

\i 05_FUNCIONES/OBJECTS/api_int_qintegratn$creategtoken.sql


