-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/11/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "int_qintegratn$usegtoken"
(	
	p_prcs              	text,
	p_session_session     	text,
    OUT p_cod_rpta      	text,
	OUT p_errores       	TYPE_TT_GE_ERRO,
	OUT p_mensajes      	TYPE_TT_GE_MNSJ
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
  
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
   
BEGIN

    p_cod_rpta := 'OK';

    if COALESCE(p_session_session, '#')!= '#' then
        --Finalizar el Token de la gtoken
          UPDATE INT_Tgtoken
            SET GTOKEN_STATE='USE',
                GTOKEN_DATE_END = CURRENT_TIMESTAMP
            WHERE 	gtoken_gtoken in (select SMATCH_GTOKEN FROM INT_TSMATCH, INT_TGTOKEN, GMS_TMATCH
                                        where  SMATCH_MATCH=MATCH_MATCH
                                        and    SMATCH_GTOKEN = GTOKEN_GTOKEN
                                        and    SMATCH_SESSION=p_session_session
                                        and    GTOKEN_STATE ='ACT' 
                                        and    MATCH_STATE = 'END');
                    
        
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;

        p_mensajes := v_mensaje;
    
    end if;



EXCEPTION

    WHEN others THEN

        p_cod_rpta := 'ERROR';
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;

        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;

        p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$usegtoken"(text, text, text)
    
