-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             02/12/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "int_qintegratn$validategtoken"(
	p_gtoken_gtoken 		text
	)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_gtoken_valida NUMERIC;
	v_date TIMESTAMP;
	

BEGIN
	
	v_date := CURRENT_TIMESTAMP;
	
    SELECT

        COUNT(*)

        INTO STRICT v_gtoken_valida

        FROM INT_TGTOKEN

        WHERE 	GTOKEN_GTOKEN = p_gtoken_gtoken 		AND 
				GTOKEN_STATE='ACT' 		AND 
				v_date<=GTOKEN_DATE_VALIDATE;

    IF (v_gtoken_valida > 0) THEN
		
        RETURN TRUE;

    ELSE
		--Actualizacion del estado de la gtoken de activo a caducada
		UPDATE INT_TGTOKEN
		SET GTOKEN_STATE='CAD'
		WHERE 	GTOKEN_GTOKEN=p_gtoken_gtoken 		AND
				GTOKEN_STATE='ACT';
		
       RETURN FALSE;

    END IF;


END;

$BODY$;

--ALTER FUNCTION "int_qintegratn$validategtoken"(text)
    
