-- INSERTING into GE_TPRCS
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.creategtoken', 'Api con procedimiento para crear gtoken');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.getgtoken', 'Api con procedimiento para obtener gtoken');
Insert into GE_TPRCS (PRCS_PRCS,PRCS_DESCRI) values ('api_int_qintegratn.usegtoken', 'Api con procedimiento para usar un gtoken');