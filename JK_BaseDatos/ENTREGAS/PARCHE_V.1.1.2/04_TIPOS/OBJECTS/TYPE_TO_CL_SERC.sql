--
-- Name: TYPE_TO_CL_SERC; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_SERC AS (

    SERC_SERC             TEXT,
    SERC_EMAIL            TEXT,
    SERC_RCODE            TEXT,
    SERC_USER             TEXT,
    SERC_DATE_SEND        TIMESTAMP

);

--ALTER TYPE TYPE_TO_CL_SERC OWNER TO juegohacker_us;