--
-- Name: TYPE_TO_CL_LMAC; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_LMAC AS (
    LMAC_LMAC          TEXT,
    LMAC_STATE         TEXT,
    LMAC_FILE_TRG      NUMERIC,
    LMAC_FILE_URL      TEXT,
    LMAC_DATE_INIT     TIMESTAMP,
    LMAC_DATE_END      TIMESTAMP,
    LMAC_USER          TEXT,
    LMAC_CLIENT        TEXT,
    LMAC_TEMPLT        TEXT,
    LMAC_DESCRI        TEXT
);

--ALTER TYPE TYPE_TO_CL_LMAC OWNER TO juegohacker_us;