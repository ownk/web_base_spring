--
-- Name: TYPE_TO_CL_DLMAC; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_DLMAC AS (
    DLMAC_DLMAC            TEXT,
    DLMAC_LMAC             TEXT,
    DLMAC_NAME             TEXT,
    DLMAC_EMAIL            TEXT,
    DLMAC_STATE            TEXT,
    DLMAC_MNSG             TEXT,
    DLMAC_FILE_ID_REG      NUMERIC,
    DLMAC_DATE_REG         TIMESTAMP,
    DLMAC_DATE_UPD         TIMESTAMP
);

--ALTER TYPE TYPE_TO_CL_DLMAC OWNER TO juegohacker_us;