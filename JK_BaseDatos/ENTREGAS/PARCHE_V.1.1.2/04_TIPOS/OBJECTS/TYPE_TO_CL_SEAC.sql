--
-- Name: TYPE_TO_CL_SEAC; Type: TYPE; Schema: juegohacker_us; Owner: juegohacker_us
--

CREATE TYPE TYPE_TO_CL_SEAC AS (

    SEAC_SEAC             TEXT,
    SEAc_NAME             TEXT,
    SEAC_EMAIL            TEXT,
    SEAC_ACODE            TEXT,
    SEAC_USER             TEXT,
    SEAC_DATE_SEND        TIMESTAMP

);

--ALTER TYPE TYPE_TO_CL_SEAC OWNER TO juegohacker_us;