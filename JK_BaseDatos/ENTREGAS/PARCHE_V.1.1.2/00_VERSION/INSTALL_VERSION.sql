-- ***************************************************
-- **          Actualizacion de Version             **
-- **                                               ** 
-- ***************************************************
update ge_tapp
set app_vers = '1.1.2', app_factu = CURRENT_TIMESTAMP
where app_app = 'APP';

/*

version mayor (a) = cambios que cambian la manera de funcionar el app. Siguiente consecutivo que se tiene la version actual
nueva_funcionalidad (b) = agrega nuevas funciones al app. Siguiente consecutivo que se tiene la version actual 
correccion_errores (c) = ajuste a funciones existentes. Siguiente consecutivo que se tiene la version actual 


*/
