/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 13.5 		*/
/*  Created On : 07-abr.-2020 11:00:00 a. m. 			*/
/*  DBMS       : PostgreSQL 							*/
/* ---------------------------------------------------- */


/* Create Tables */

CREATE TABLE CLI_TDLMAC
(
	
	DLMAC_DLMAC 		varchar(50) 	COLLATE pg_catalog."default"	NOT NULL,
	DLMAC_LMAC 			varchar(50) 	COLLATE pg_catalog."default"	NOT NULL,
	DLMAC_NAME			TEXT			NOT NULL,
	DLMAC_STATE 		varchar(10)		COLLATE pg_catalog."default"	NOT NULL,
	DLMAC_MNSG 			TEXT	                                    	NOT NULL,
	DLMAC_EMAIL 		TEXT 											NOT NULL, 
	DLMAC_FILE_ID_REG 	NUMERIC 										NOT NULL,
	DLMAC_DATE_REG 		timestamp										NOT NULL,
	DLMAC_DATE_UPD		timestamp

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE CLI_TACODE
    

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE ONLY CLI_TDLMAC ADD CONSTRAINT PK_CLI_TDLMAC
	PRIMARY KEY (DLMAC_DLMAC)
;

CREATE INDEX IXFK_CLI_TDLMAC_CLI_TLMAC
    ON CLI_TDLMAC USING btree
    (DLMAC_LMAC)
    TABLESPACE pg_default;

/* Create Table Comments, Sequences for Autonumber Columns */

COMMENT ON TABLE CLI_TDLMAC
	IS 'Tabla con informacion procesos del cargue de los codigos de acceso'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_DLMAC
	IS 'Indicador del detalle del registro'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_LMAC
	IS 'Identificador del precoseo'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_NAME
	IS 'Identificador normbre de la persona'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_STATE
	IS 'Estado del detalle del proceso'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_MNSG
	IS 'Mensaje del detalle del proceso'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_EMAIL
	IS 'Coreo de envio'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_FILE_ID_REG
	IS 'Identficador fecha de registro'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_DATE_REG
	IS 'Fecha de registro'
;

COMMENT ON COLUMN CLI_TDLMAC.DLMAC_DATE_UPD
	IS 'Fecha de actualización'
;