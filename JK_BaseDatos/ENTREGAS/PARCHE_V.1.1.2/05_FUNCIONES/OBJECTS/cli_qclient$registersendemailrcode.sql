-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             13/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$registersendemailrcode"
(
    p_prcs                 TEXT,
    p_tserc                TYPE_TT_CL_SERC,
    p_pgcrypto_key         TEXT,
    p_pgcrypto_alg         TEXT,
    OUT p_cod_rpta         TEXT,
    OUT p_errores          type_tt_ge_erro,
    OUT p_mensajes         type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
    v_cod_mensaje        VARCHAR(100):='OK';
    
    v_serc_serc         VARCHAR(50);
    v_mensajes TYPE_TT_GE_MNSJ;
    v_errores   TYPE_TT_GE_ERRO;


    
    
BEGIN

    
    FOR i IN 0..COALESCE(array_length(p_tserc, 1)-1, 0)
    LOOP
     
        select uuid_generate_v4() INTO v_serc_serc;
           --Se insertan los procesos
        INSERT INTO CLI_TSERC(
                SERC_SERC,
                SERC_EMAIL,
                SERC_RCODE,
                SERC_USER,
                SERC_DATE_SEND
        )VALUES(
                v_serc_serc,
                pgp_sym_encrypt(p_tserc[i].SERC_EMAIL, p_pgcrypto_key,p_pgcrypto_alg),
                p_tserc[i].SERC_RCODE,
                p_tserc[i].SERC_USER,
                CURRENT_TIMESTAMP
        );
        
            
        
    END LOOP;
    
    
    IF(v_cod_mensaje = 'OK')THEN
        p_cod_rpta := v_cod_mensaje;
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
        p_mensajes := v_mensaje;
    ELSE
            
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('ERROR'::TEXT, p_prcs, v_mensajes, 'CLIENT NOT EXIST')
            INTO v_mensajes;
    END IF;
    
    EXCEPTION

        WHEN others THEN
        
            p_cod_rpta := 'ERROR';
        
            SELECT
                *
                FROM api_ge_qutils$generartypeerror(SQLSTATE, p_prcs, v_errores, SQLERRM)
                INTO v_errores;
            
            p_errores := v_errores;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensajes)
                INTO v_mensajes;

            p_mensajes := v_mensajes;

END;

$BODY$;

    

