-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getrcode"
(
    p_rcode_rcode     text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_CL_RCODE
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_rcode CURSOR FOR
    SELECT
        RCODE_RCODE,
        pgp_sym_decrypt(RCODE_CODE::bytea,  p_pgcrypto_key) RCODE_CODE,        
        RCODE_STATE,
        RCODE_CLIENT,
        RCODE_DATE_START,
        RCODE_DATE_VALIDITY,
        RCODE_DATE_USED
        FROM CLI_TRCODE
        WHERE RCODE_RCODE=p_rcode_rcode;
        
  
    to_cl_trcode TYPE_TO_CL_RCODE;
    
BEGIN
  
    --Obtener el Client por Id
    
    FOR i IN c_rcode
    LOOP

        to_cl_trcode := ROW (i.RCODE_RCODE,i.RCODE_CODE,i.RCODE_STATE,i.RCODE_CLIENT,i.RCODE_DATE_START,i.RCODE_DATE_VALIDITY,i.RCODE_DATE_USED)::TYPE_TO_CL_RCODE;

    END LOOP;

    return to_cl_trcode;
 


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getrcode"(text, text)
    
