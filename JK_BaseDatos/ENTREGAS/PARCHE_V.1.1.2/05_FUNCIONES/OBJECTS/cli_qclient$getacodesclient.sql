-- =========================================================================================================================================================================
-- #VERSION:0000001001
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             05/12/2019      ownk           Se crea funcion 
-- 1001                                             13/04/2020      ownk           Se ajusta funcion para retornar la info de los envios de correo por codigo de acceso
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getacodesclient"
(
    p_prcs              text,
    p_client_client     text,
    p_pgcrypto_key      text,
    OUT p_cod_rpta      text,
    OUT p_errores       TYPE_TT_GE_ERRO,
    OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tacode        TYPE_TT_CL_ACODE,
    OUT p_tseac         TYPE_TT_CL_SEAC   --1001
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_acodes CURSOR FOR
    SELECT
        ACODE_ACODE
    FROM CLI_TACODE
    WHERE ACODE_CLIENT = p_client_client;
    
    --ini 1001
    c_seacs CURSOR (vc_acode VARCHAR(50))FOR
    SELECT 
        SEAC_SEAC
        FROM CLI_TSEAC
        WHERE SEAC_ACODE=vc_acode; 
    --fin 1001    
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_cl_acode TYPE_TO_CL_ACODE;
    tt_cl_acode TYPE_TT_CL_ACODE;
    
    to_cl_seac TYPE_TO_CL_SEAC; --1001
    tt_cl_seac TYPE_TT_CL_SEAC; --1001

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
    FOR i IN c_acodes
    LOOP
        to_cl_acode=cli_qclient$getacode(i.ACODE_ACODE, p_pgcrypto_key);   --1001
        tt_cl_acode[COALESCE(array_length(tt_cl_acode, 1), 0)] := to_cl_acode;
        
        --ini1001
            FOR k IN c_seacs(i.ACODE_ACODE)
            LOOP
                to_cl_seac=cli_qclient$getseac(k.SEAC_SEAC, p_pgcrypto_key);    --1001
                tt_cl_seac[COALESCE(array_length(tt_cl_seac, 1), 0)] := to_cl_seac;        --1001
            END LOOP;
        --fin 1001
        
        

    END LOOP;

    p_tacode := tt_cl_acode;
    p_tseac := tt_cl_seac; --1001
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getacodesclient"(text, text, text)
    
