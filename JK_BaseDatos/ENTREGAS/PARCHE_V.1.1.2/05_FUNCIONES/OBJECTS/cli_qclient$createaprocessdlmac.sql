-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             07/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$createaprocessdlmac"
(
    p_prcs                     TEXT,
    p_tdlmac                   TYPE_TT_CL_DLMAC,
    p_pgcrypto_key             TEXT,
    p_pgcrypto_alg             TEXT,
    OUT p_cod_rpta             TEXT,
    OUT p_errores              type_tt_ge_erro,
    OUT p_mensajes             type_tt_ge_mnsj,
    OUT p_tdlmac_out           type_tt_cl_dlmac
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
    v_cod_mensaje        VARCHAR(100):='OK';
    
    v_dlmac_dlmac         VARCHAR(50);
    v_client_exist         NUMERIC(8);
    v_mensajes     TYPE_TT_GE_MNSJ;
    v_errores   TYPE_TT_GE_ERRO;

    to_cl_dlmac TYPE_TO_CL_DLMAC;
    tt_cl_dlmac TYPE_TT_CL_DLMAC;
    
    
BEGIN

    
    FOR i IN 0..COALESCE(array_length(p_tdlmac, 1)-1, 0)
    LOOP
          
        select uuid_generate_v4() INTO v_dlmac_dlmac;
           --Se insertan los procesos
        INSERT INTO CLI_TDLMAC(
                DLMAC_DLMAC          ,
                DLMAC_LMAC             ,
                DLMAC_NAME            ,
                DLMAC_EMAIL            ,
                DLMAC_STATE            ,
                DLMAC_MNSG           ,
                DLMAC_FILE_ID_REG   ,
                DLMAC_DATE_REG      ,
                DLMAC_DATE_UPD         
        )VALUES(
                v_dlmac_dlmac,
                p_tdlmac[i].DLMAC_LMAC,
                pgp_sym_encrypt(p_tdlmac[i].DLMAC_NAME, p_pgcrypto_key,p_pgcrypto_alg),
                pgp_sym_encrypt(p_tdlmac[i].DLMAC_EMAIL, p_pgcrypto_key,p_pgcrypto_alg),
                'PEND',
                pgp_sym_encrypt(p_tdlmac[i].DLMAC_MNSG, p_pgcrypto_key,p_pgcrypto_alg),
                p_tdlmac[i].DLMAC_FILE_ID_REG,
                CURRENT_TIMESTAMP,
                NULL
        );
        
        to_cl_dlmac = cli_qclient$getdetailslmacbyid(v_dlmac_dlmac,p_pgcrypto_key);
        tt_cl_dlmac[COALESCE(array_length(tt_CL_DLMAC, 1), 0)] := to_cl_dlmac;
        
    END LOOP;
    
    p_tdlmac_out := tt_cl_dlmac;
    
    v_cod_mensaje:='OK';
    
    IF(v_cod_mensaje = 'OK')THEN
        p_cod_rpta := v_cod_mensaje;        
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
        p_mensajes := v_mensaje;
    END IF;
    
    EXCEPTION

        WHEN others THEN
        
            p_cod_rpta := 'ERROR';
        
            SELECT
                *
                FROM api_ge_qutils$generartypeerror(SQLSTATE, p_prcs, v_errores, SQLERRM)
                INTO v_errores;
            
            p_errores := v_errores;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensajes)
                INTO v_mensajes;

            p_mensajes := v_mensajes;

END;
$BODY$;