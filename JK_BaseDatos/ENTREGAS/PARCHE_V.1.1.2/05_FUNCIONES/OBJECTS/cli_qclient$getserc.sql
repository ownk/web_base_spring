-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             13/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getserc"
(
    p_serc_serc         text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_CL_SERC
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_serc CURSOR FOR
    SELECT
        SERC_SERC,
        pgp_sym_decrypt(SERC_EMAIL::bytea,  p_pgcrypto_key) SERC_EMAIL,        
        SERC_RCODE,
        SERC_USER,
        SERC_DATE_SEND
        FROM CLI_TSERC
        WHERE SERC_SERC=p_serc_serc;
        
  
    to_cl_tserc TYPE_TO_CL_SERC;
    
BEGIN
  
    --Obtener el Client por Id
    
    FOR i IN c_serc
    LOOP

        to_cl_tserc := ROW (i.SERC_SERC,i.SERC_EMAIL,i.SERC_RCODE,i.SERC_USER,i.SERC_DATE_SEND)::TYPE_TO_CL_SERC;

    END LOOP;

    return to_cl_tserc;
 


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getrcode"(text, text)
    
