--Por cambio de parametros de salida se debe eliminar la funcion existente
drop function  cli_qclient$createacodes;
drop function  api_cli_qclient$createacodes;
drop function  cli_qclient$getacodesclient;
drop function  api_cli_qclient$getacodesclient;
drop function  cli_qclient$getrcodesclient;
drop function  api_cli_qclient$getrcodesclient;
drop function  usr_quser$createuser;
drop function  usr_quser$forgotpassword;
drop function  acn_qacnt$createaccount;
drop function  gms_qplayer$createplayer;
drop function  cli_qclient$createacodescant;
drop function  cli_qclient$createclient;
drop function  cli_qclient$creatercodes;
drop function  cli_qclient$creatercodescant;
drop function  cli_qclient$createtemplategames;
drop function  gms_qplayer$updatematch;
drop function  int_qpartnr$createpartner;
drop function  aut_qlogin$registerlogin;
drop function  aut_qlogin$registernewpassword;

--Funciones
\i 05_FUNCIONES/OBJECTS/cli_qclient$getdetailslmacbyid.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getprocesslmacbyid.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getseac.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getserc.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getrcode.sql
--Procedimientos
\i 05_FUNCIONES/OBJECTS/cli_qclient$createaprocesslmac.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$createaprocessdlmac.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$updateprocesslmac.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getdetailslmac.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getprocesslmac.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$updatedetailslmac.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$deleteaccesscode.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getclientid.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$createacodes.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$registersendemailacode.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$registersendemailrcode.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getacodesclient.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$getrcodesclient.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$createacodescant.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$createclient.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$creatercodes.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$creatercodescant.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$createtemplategames.sql
\i 05_FUNCIONES/OBJECTS/gms_qplayer$updatematch.sql
\i 05_FUNCIONES/OBJECTS/int_qpartnr$createpartner.sql
\i 05_FUNCIONES/OBJECTS/aut_qlogin$registerlogin.sql
\i 05_FUNCIONES/OBJECTS/aut_qlogin$registernewpassword.sql
\i 05_FUNCIONES/OBJECTS/usr_quser$createuser.sql
\i 05_FUNCIONES/OBJECTS/usr_quser$forgotpassword.sql
\i 05_FUNCIONES/OBJECTS/acn_qacnt$createaccount.sql
\i 05_FUNCIONES/OBJECTS/gms_qplayer$createplayer.sql
\i 05_FUNCIONES/OBJECTS/gms_qplayer$startmatch.sql
\i 05_FUNCIONES/OBJECTS/ge_qlgtr$iniciar_log.sql
\i 05_FUNCIONES/OBJECTS/cli_qclient$deleteregistercode.sql

--API
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$createaprocesslmac.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$createaprocessdlmac.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$updateprocesslmac.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$getdetailslmac.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$getprocesslmac.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$updatedetailslmac.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$deleteaccesscode.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$getclientid.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$createacodes.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$registersendemailacode.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$registersendemailrcode.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$getacodesclient.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$getrcodesclient.sql
\i 05_FUNCIONES/OBJECTS/api_cli_qclient$deleteregistercode.sql