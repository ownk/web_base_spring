-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             08/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getprocesslmac"
(
    p_prcs              text,
    p_client_client     text,
    p_pgcrypto_key      text,
    OUT p_cod_rpta      text,
    OUT p_errores       TYPE_TT_GE_ERRO,
    OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tlmac         TYPE_TT_CL_LMAC
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_lmacs CURSOR FOR
    SELECT LMAC_LMAC
        FROM CLI_TLMAC
        WHERE LMAC_CLIENT=p_client_client
        ORDER BY LMAC_DATE_INIT DESC
        LIMIT 100;
    
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_cl_lmac TYPE_TO_CL_LMAC;
    tt_cl_lmac TYPE_TT_CL_LMAC;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    

BEGIN
    
    FOR i IN c_lmacs
    LOOP
        
        to_cl_lmac=cli_qclient$getprocesslmacbyid(i.lmac_lmac, p_pgcrypto_key);
        tt_cl_lmac[COALESCE(array_length(tt_cl_lmac, 1), 0)] := to_cl_lmac;
        
    END LOOP;
    p_tlmac := tt_cl_lmac;
    
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;
    
