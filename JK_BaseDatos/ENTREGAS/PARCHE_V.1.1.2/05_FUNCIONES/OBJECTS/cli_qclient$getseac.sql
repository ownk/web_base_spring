-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             13/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getseac"
(
    p_seac_seac         text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_CL_SEAC
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_seac CURSOR FOR
    SELECT
        SEAC_SEAC,
        pgp_sym_decrypt(SEAC_NAME::bytea,  p_pgcrypto_key) SEAC_NAME,        
        pgp_sym_decrypt(SEAC_EMAIL::bytea,  p_pgcrypto_key) SEAC_EMAIL,        
        SEAC_ACODE,
        SEAC_USER,
        SEAC_DATE_SEND
        FROM CLI_TSEAC
        WHERE SEAC_SEAC=p_seac_seac;
        
  
    to_cl_tseac TYPE_TO_CL_SEAC;
    
BEGIN
  
    --Obtener el Client por Id
    
    FOR i IN c_seac
    LOOP

        to_cl_tseac := ROW (i.SEAC_SEAC,i.SEAC_NAME,i.SEAC_EMAIL,i.SEAC_ACODE,i.SEAC_USER,i.SEAC_DATE_SEND)::TYPE_TO_CL_SEAC;

    END LOOP;

    return to_cl_tseac;
 


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getacode"(text, text)
    
