-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             12/10/2019      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getdetailslmacbyid"
(
    p_dlmac_dlmac       text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_CL_DLMAC
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_dlmac CURSOR FOR
    SELECT
        DLMAC_DLMAC,
        DLMAC_LMAC,
        pgp_sym_decrypt(DLMAC_NAME::bytea,  p_pgcrypto_key) DLMAC_NAME, 
        pgp_sym_decrypt(DLMAC_EMAIL::bytea,  p_pgcrypto_key) DLMAC_EMAIL, 
        DLMAC_STATE,
        pgp_sym_decrypt(DLMAC_MNSG::bytea,  p_pgcrypto_key) DLMAC_MNSG,
        DLMAC_FILE_ID_REG,
        DLMAC_DATE_REG,
        DLMAC_DATE_UPD
        FROM CLI_TDLMAC
        WHERE DLMAC_DLMAC=p_dlmac_dlmac;
        
  
    to_cl_dlmac TYPE_TO_CL_DLMAC;
    
BEGIN
  
    --Obtener el DLMAC por Id
    
    FOR i IN c_DLMAC
    LOOP

        to_cl_dlmac := ROW (i.DLMAC_DLMAC,i.DLMAC_LMAC,i.DLMAC_NAME,i.DLMAC_EMAIL,i.DLMAC_STATE,i.DLMAC_MNSG,i.DLMAC_FILE_ID_REG,i.DLMAC_DATE_REG, i.DLMAC_DATE_UPD)::TYPE_TO_CL_DLMAC;

    END LOOP;

    return to_cl_dlmac;
 


END;

$BODY$;

--ALTER FUNCTION "cli_qDLMAC$getDLMACbyid"(text, text)
    
