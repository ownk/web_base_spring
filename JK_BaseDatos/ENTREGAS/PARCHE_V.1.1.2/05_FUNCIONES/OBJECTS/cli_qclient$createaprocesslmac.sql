-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             07/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$createaprocesslmac"
(
    p_prcs                 TEXT,
    p_tlmac                TYPE_TT_CL_LMAC,
    p_pgcrypto_key         TEXT,
    p_pgcrypto_alg         TEXT,
    OUT p_cod_rpta         TEXT,
    OUT p_errores          type_tt_ge_erro,
    OUT p_mensajes         type_tt_ge_mnsj,
    OUT p_lmac_lmac        TEXT
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
 
    v_cod_mensaje        VARCHAR(100):='OK';
    
    v_lmac_lmac         VARCHAR(50);
    v_client_exist         NUMERIC(8);
    v_mensajes TYPE_TT_GE_MNSJ;
    v_errores   TYPE_TT_GE_ERRO;


    
    
BEGIN

    --Cliente no existe
    
    FOR i IN 0..COALESCE(array_length(p_tlmac, 1)-1, 0)
    LOOP
      
        
        SELECT COUNT(*)
        INTO v_client_exist
        FROM CLI_TCLIENT
        where CLIENT_CLIENT=p_tlmac[i].LMAC_CLIENT;
        
        
        IF (v_client_exist=0) THEN
            
            v_cod_mensaje := 'ERROR_CLNT_NE';
        
        ELSE
          
            select uuid_generate_v4() INTO v_lmac_lmac;
               --Se insertan los procesos
            INSERT INTO CLI_TLMAC(
                    LMAC_LMAC,
                    LMAC_STATE,
                    LMAC_FILE_TRG,
                    LMAC_FILE_URL,
                    LMAC_DATE_INIT,
                    LMAC_USER,
                    LMAC_CLIENT,
                    LMAC_TEMPLT,
                    LMAC_DESCRI
            )VALUES(
                    v_lmac_lmac,
                    'PEND',
                    p_tlmac[i].LMAC_FILE_TRG,
                    p_tlmac[i].LMAC_FILE_URL,
                    CURRENT_TIMESTAMP,
                    p_tlmac[i].LMAC_USER,
                    p_tlmac[i].LMAC_CLIENT,
                    p_tlmac[i].LMAC_TEMPLT,
                    pgp_sym_encrypt(p_tlmac[i].LMAC_DESCRI, p_pgcrypto_key,p_pgcrypto_alg)
            );
            
            p_lmac_lmac := v_lmac_lmac;
        END IF;
            
        
    END LOOP;
    
    
    IF(v_cod_mensaje = 'OK')THEN
        p_cod_rpta := v_cod_mensaje;        
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
        p_mensajes := v_mensaje;
    ELSE
            
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('ERROR'::TEXT, p_prcs, v_mensajes, 'CLIENT NOT EXIST')
            INTO v_mensajes;
        
        
        SELECT
            *
            FROM api_ge_qutils$generartypeerror('ERROR_CLNT_NE', p_prcs, v_errores, 'CLIENT NOT EXIST')
            INTO v_errores;
    END IF;
    
    EXCEPTION

        WHEN others THEN
        
            p_cod_rpta := 'ERROR';
        
            SELECT
                *
                FROM api_ge_qutils$generartypeerror(SQLSTATE, p_prcs, v_errores, SQLERRM)
                INTO v_errores;
            
            p_errores := v_errores;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensajes)
                INTO v_mensajes;

            p_mensajes := v_mensajes;

END;

$BODY$;

    

