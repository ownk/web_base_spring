-- =========================================================================================================================================================================
-- #VERSION:0000001001
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             14/12/2019      ownk           Se crea funcion 
-- 1001                                             13/04/2020      ownk           Se ajusta funcion para retornar la info de los envios de correo por codigo de registro
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getrcodesclient"
(
    p_prcs              text,
    p_pgcrypto_key      text,
    OUT p_cod_rpta      text,
    OUT p_errores       TYPE_TT_GE_ERRO,
    OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_trcode        TYPE_TT_CL_RCODE,
    OUT p_tclient       TYPE_TT_CL_CLIENT,
    OUT p_tserc         TYPE_TT_CL_SERC   --1001

)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_rcodes CURSOR FOR
    SELECT
        RCODE_RCODE,
        RCODE_CLIENT
    FROM CLI_TRCODE
    order  by RCODE_STATE desc, RCODE_DATE_START desc;
    
    --ini 1001
    c_sercs CURSOR (vc_rcode VARCHAR(50))FOR
    SELECT 
        SERC_SERC
        FROM CLI_TSERC
        WHERE SERC_RCODE=vc_rcode; 
    --fin 1001    
    
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_cl_rcode TYPE_TO_CL_RCODE;
    tt_cl_rcode TYPE_TT_CL_RCODE;
    
    to_cl_client TYPE_TO_CL_CLIENT;
    tt_cl_client TYPE_TT_CL_CLIENT;
    
    to_cl_serc TYPE_TO_CL_SERC; --1001
    tt_cl_serc TYPE_TT_CL_SERC; --1001

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
    FOR i IN c_rcodes
    LOOP
        to_cl_rcode=cli_qclient$getrcode(i.RCODE_RCODE, p_pgcrypto_key);   --1001
        tt_cl_rcode[COALESCE(array_length(tt_cl_rcode, 1), 0)] := to_cl_rcode;
        
        IF (i.RCODE_CLIENT IS NOT NULL)THEN
            to_cl_client = cli_qclient$getclientbyid(i.RCODE_CLIENT,p_pgcrypto_key);
            tt_cl_client[COALESCE(array_length(tt_cl_client, 1), 0)] := to_cl_client;
        END IF;
        
        --ini1001
            FOR k IN c_sercs(i.RCODE_RCODE)
            LOOP
                to_cl_serc=cli_qclient$getserc(k.SERC_SERC, p_pgcrypto_key);            --1001
                tt_cl_serc[COALESCE(array_length(tt_cl_serc, 1), 0)] := to_cl_serc;        --1001
            END LOOP;
        --fin 1001
        
    END LOOP;

    p_trcode := tt_cl_rcode;
    p_tclient := tt_cl_client;
    p_tserc := tt_cl_serc;         --1001
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "cli_qclient$getrcodesclient"(text, text)
    
