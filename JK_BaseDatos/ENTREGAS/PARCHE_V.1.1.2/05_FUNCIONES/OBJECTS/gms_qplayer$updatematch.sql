-- =========================================================================================================================================================================
-- #VERSION:0000001001
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             29/10/2019      ownk           Se crea funcion 
-- 1001                                             20/04/2020      ownk           Se ajusta funcion para genera ID
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "gms_qplayer$updatematch"
(
    p_prcs                 TEXT,
    p_tscore               type_tt_gm_score,
    p_tmatch               type_tt_gm_match,
    OUT p_cod_rpta         TEXT,
    OUT p_errores          type_tt_ge_erro,
    OUT p_mensajes         type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    
    c_sscore CURSOR FOR                
    select nextval ('gms_sscore');

    c_game CURSOR (vc_match_match VARCHAR(50))FOR
    SELECT MATCH_GAME 
    FROM GMS_TMATCH
    WHERE MATCH_MATCH=vc_match_match;
    
    c_cnvscr CURSOR (vc_game VARCHAR(50))FOR
    SELECT CNVSCR_SCR_APP
    FROM GMS_TCNVSCR
    WHERE CNVSCR_GAME=vc_game;
    
    
    c_cnvatb CURSOR (vc_player VARCHAR(50), vc_game VARCHAR(50))FOR
    SELECT  ATRPL_CHRT,
            ATRPL_ATRB,
            CNVATB_CHRT,
            CNVATB_ATRB,
            CNVATB_ATRB_APP,
            CNVATB_ATRB_GAME      
    FROM GMS_TATRPL,GMS_TCNVATB
    WHERE   ATRPL_PLAYER=vc_player and CNVATB_ATRB=ATRPL_ATRB AND 
            CNVATB_CHRT=ATRPL_CHRT and CNVATB_GAME=vc_game;
    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    
    
    v_mensaje TYPE_TT_GE_MNSJ;
 
    v_cod_mensaje         VARCHAR(100):='OK';
    v_nextval_score       NUMERIC;
    v_score_score         VARCHAR(50);
    v_match_state         VARCHAR(50);
    v_match_match         VARCHAR(50);
    v_score_app           DECIMAL (8,2);
    v_atrb_app            NUMERIC;
    v_final_score         NUMERIC;
    v_final_atrb          NUMERIC;
    v_game_score          NUMERIC;
    v_game                VARCHAR(50);
    
    to_gms_match TYPE_TO_GM_MATCH;

    F$RESULT_LOGICA RECORD;
    resultado    BOOLEAN;

BEGIN
    
    FOR j IN 0..COALESCE(array_length(p_tmatch, 1)-1, 0)
    LOOP
        v_match_match := p_tmatch[j].MATCH_MATCH;
        v_game_score := p_tmatch[j].MATCH_GAME_SCORE;
        
        OPEN c_game (v_match_match);
        FETCH c_game INTO v_game;
        CLOSE c_game;
        
        to_gms_match=gms_qgame$getmatch(v_match_match);
        
        --Se verifica que el match exista
        if(to_gms_match.match_match is not null) then
            
            --Solo se actualiza informacion si el match no esta terminado
            if(to_gms_match.match_state not in ('END')) then
            
                    v_match_state := p_tmatch[j].MATCH_STATE;
                    
                    --Se actualiza el score final obtenido en la partida
                    UPDATE GMS_TMATCH SET
                        MATCH_END=CURRENT_TIMESTAMP,
                        MATCH_GAME_SCORE=p_tmatch[j].MATCH_GAME_SCORE,
                        MATCH_STATE=v_match_state,
                        MATCH_RESULT=p_tmatch[j].MATCH_RESULT
                    WHERE MATCH_MATCH=p_tmatch[j].MATCH_MATCH;
                    
                    
                    --eliminar scores existentes del match
                        DELETE FROM GMS_TSCORE
                        WHERE SCORE_MATCH=p_tmatch[j].MATCH_MATCH;
                    
                    
                    if(p_tscore is not null) then
                    
                        FOR i IN 0..COALESCE(array_length(p_tscore, 1)-1, 0)
                        LOOP
                            
                            
                            if(p_tscore[i].SCORE_MATCH = p_tmatch[j].MATCH_MATCH) then
                                --OPEN c_sscore; --1001
                                --FETCH c_sscore INTO v_nextval_score; --1001
                                --CLOSE c_sscore; --1001
                            
                                --v_score_score := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_score); --1001    
                                
                                select cast(uuid_generate_v4() as varchar) INTO v_score_score; --1001
                                
                                --Se insertan los atributos obtenidos en el score
                                INSERT INTO GMS_TSCORE(
                                        SCORE_SCORE ,
                                        SCORE_MATCH,
                                        SCORE_CHRT,
                                        SCORE_ATRB,
                                        SCORE_VALUE
                                )VALUES(
                                        v_score_score,
                                        p_tmatch[j].MATCH_MATCH,
                                        p_tscore[i].SCORE_CHRT,
                                        p_tscore[i].SCORE_ATRB,
                                        p_tscore[i].SCORE_VALUE
                                );
                            
                            end if;
                        END LOOP;
                    else
                    
                    FOR k IN c_cnvatb(to_gms_match.match_player,v_game)
                    LOOP
                        
                        --OPEN c_sscore; --1001
                        --FETCH c_sscore INTO v_nextval_score; --1001
                        --CLOSE c_sscore; --1001
                    
                        --v_score_score := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_score); --1001        
                        
                        select cast(uuid_generate_v4() as varchar) INTO v_score_score; --1001
                    
                        v_atrb_app:=k.CNVATB_ATRB_APP;
                        v_final_atrb := v_game_score*v_atrb_app;
                            
                        INSERT INTO GMS_TSCORE(
                                SCORE_SCORE ,
                                SCORE_MATCH,
                                SCORE_CHRT,
                                SCORE_ATRB,
                                SCORE_VALUE
                        )VALUES(
                                v_score_score,
                                p_tmatch[j].MATCH_MATCH,
                                k.ATRPL_CHRT,
                                k.ATRPL_ATRB,
                                v_final_atrb
                        );
                    END LOOP;
                    
                    end if;
                
                --Validar si el match esta finalizado
                IF (v_match_state='END')THEN
                    
                    OPEN c_cnvscr(v_game);
                    FETCH c_cnvscr INTO v_score_app;
                    CLOSE c_cnvscr;
                    
                    
                    v_final_score = v_game_score*v_score_app;
                    
                    --Actualizar el Final_Score
                    UPDATE GMS_TMATCH SET
                        MATCH_FINAL_SCORE=v_final_score
                    WHERE MATCH_MATCH=v_match_match;

                    SELECT
                        *
                        FROM gms_qplayer$updateplayerscore(p_prcs,v_match_match)
                        INTO F$RESULT_LOGICA;
                    SELECT
                        *
                        FROM gms_qplayer$updateplayeratrb(p_prcs,v_match_match)
                        INTO F$RESULT_LOGICA;
                    
                END IF;
                
            ELSE
                v_cod_mensaje := 'ERROR_MATCH_ENDED';
            
            
            end if;
        
        else
            v_cod_mensaje := 'ERROR_MATCH_NL';
        
        end if;
    END LOOP;
    
    

    IF(v_cod_mensaje = 'OK')THEN
        p_cod_rpta := v_cod_mensaje;
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;
        
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
        p_mensajes := v_mensaje;
    ELSE
        p_cod_rpta := v_cod_mensaje;
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;
    END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$updatematch"(text,type_tt_gm_score,type_tt_gm_match)
    

