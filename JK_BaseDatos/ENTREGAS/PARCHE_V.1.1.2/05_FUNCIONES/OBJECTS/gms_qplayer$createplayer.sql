-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             28/10/2019      ownk           Se crea funcion 
-- 1001                                             20/04/2020      ownk           Se ajusta funcion para generar id 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "gms_qplayer$createplayer"
(
    p_prcs                 TEXT,
    p_codigo_acceso        TEXT,
    p_user_user            TEXT,
    p_acnt_acnt            TEXT,
    p_tplayer              TYPE_TT_PL_PLAYER,
    p_pgcrypto_key         TEXT,
    p_pgcrypto_alg         TEXT,
    OUT p_cod_rpta         text,
    OUT p_errores          type_tt_ge_erro,
    OUT p_mensajes         type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    /*ini1001
    c_splayer CURSOR FOR                
    select nextval ('gms_splayer');
    fin1001*/
    c_nick CURSOR (vc_user_user VARCHAR(50))FOR
    select USER_NICK FROM AUT_TUSER
    WHERE vc_user_user = USER_USER;
    
    c_hash_nick CURSOR (vc_user_user VARCHAR(50))FOR
    select USER_HASH_NICK FROM AUT_TUSER
    WHERE vc_user_user = USER_USER;
    
    
    v_user_user            VARCHAR(50);
    v_acnt_acnt         VARCHAR(50);
    v_nick                TEXT;
    v_hash_nick            TEXT;
    v_player_player        VARCHAR(50);
    v_nextval_player     NUMERIC;

BEGIN


    FOR i IN 0..COALESCE(array_length(p_tplayer, 1)-1, 0)
    LOOP

        /*ini1001
        OPEN c_splayer;
        FETCH c_splayer INTO v_nextval_player;
        CLOSE c_splayer;
        
        v_player_player := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_player);
        fin1001*/

        OPEN c_nick(p_user_user);
        FETCH c_nick INTO v_nick;
        CLOSE c_nick;
        
        OPEN c_hash_nick(p_user_user);
        FETCH c_hash_nick INTO v_hash_nick;
        CLOSE c_hash_nick;
        
        select cast(uuid_generate_v4() as varchar) INTO v_player_player;
        
        v_user_user:=(p_user_user::varchar(50));
        
        v_acnt_acnt:=(p_acnt_acnt::varchar(50));
        
        
        --Crear Un Nuevo Player
            Insert Into GMS_TPLAYER(
                    PLAYER_PLAYER,
                    PLAYER_USER,
                    PLAYER_NAME,
                    PLAYER_DATECR,
                    PLAYER_SCORE,
                    PLAYER_STATE,
                    PLAYER_HASH_NAME,
                    PLAYER_TPIDENT,
                    PLAYER_NUMIDENT,
                    PLAYER_HASH_TPIDENT,
                    PLAYER_HASH_NUMIDENT,
                    PLAYER_ID_EXT
            )VALUES(
                    v_player_player,
                    v_user_user,
                    v_nick,
                    CURRENT_TIMESTAMP,
                    0,
                    'INA',
                    v_hash_nick,
                    pgp_sym_encrypt(p_tplayer[i].PLAYER_TPIDENT, p_pgcrypto_key,p_pgcrypto_alg),
                    pgp_sym_encrypt(p_tplayer[i].PLAYER_NUMIDENT, p_pgcrypto_key,p_pgcrypto_alg),
                    md5(upper(p_tplayer[i].PLAYER_TPIDENT)),
                    md5(upper(p_tplayer[i].PLAYER_NUMIDENT)),
                    pgp_sym_encrypt(p_tplayer[i].PLAYER_ID_EXT, p_pgcrypto_key,p_pgcrypto_alg)
                    
            );
        
        
        --Relacionar el player con la cuenta 
            Insert Into GMS_TPLACN(
                    PLACN_PLAYER,
                    PLACN_ACNT
            )VALUES(
                    v_player_player,
                    v_acnt_acnt
            );
        --Asignar el rol de Player al usuario
            INSERT INTO AUT_TUROL (
                    UROL_USER,
                    UROL_ROL
            )values(
                    v_user_user,
                    'PLAYER'
            );
    END LOOP;
    p_cod_rpta := 'OK';
    to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
    tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
    p_errores := tt_ge_erro;

    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "gms_qplayer$createplayer"(text, text, text, text)
    

