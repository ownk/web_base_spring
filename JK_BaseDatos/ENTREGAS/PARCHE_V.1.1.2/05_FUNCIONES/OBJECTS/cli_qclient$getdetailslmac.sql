-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             07/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getdetailslmac"
(
    p_prcs              text,
    p_lmac_lmac         text,
    p_pgcrypto_key      text,
    OUT p_cod_rpta      text,
    OUT p_errores       TYPE_TT_GE_ERRO,
    OUT p_mensajes      TYPE_TT_GE_MNSJ,
    OUT p_tdlmac        TYPE_TT_CL_DLMAC
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_dlmac_all CURSOR FOR
    SELECT
        DLMAC_DLMAC
        FROM CLI_TDLMAC
        WHERE DLMAC_LMAC = p_lmac_lmac;
        
    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;

    to_cl_dlmac TYPE_TO_CL_DLMAC;
    tt_cl_dlmac TYPE_TT_CL_DLMAC;

    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;

BEGIN
  
    FOR i IN c_dlmac_all
    LOOP

        to_cl_dlmac = cli_qclient$getdetailslmacbyid(i.DLMAC_DLMAC,p_pgcrypto_key);
        tt_cl_dlmac[COALESCE(array_length(tt_CL_DLMAC, 1), 0)] := to_cl_dlmac;

    END LOOP;

    p_tdlmac := tt_cl_dlmac;
    
    SELECT
        *
        FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
        INTO v_mensaje;

    p_mensajes := v_mensaje;
    
    p_cod_rpta := 'OK';

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;


END;

$BODY$;

    
