-- =========================================================================================================================================================================
-- #VERSION:0000001000
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versi�n        GAP                Solicitud        Fecha        Realiz�            Descripci�n
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             08/04/2020      ownk           Se crea funcion 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================

CREATE OR REPLACE FUNCTION "cli_qclient$getprocesslmacbyid"
(
    p_lmac_lmac         text,
    p_pgcrypto_key      text
)
RETURNS TYPE_TO_CL_LMAC
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    c_lmac CURSOR FOR
    SELECT
        LMAC_LMAC,
        LMAC_STATE,
        LMAC_FILE_TRG, 
        LMAC_FILE_URL,
        LMAC_DATE_INIT,
        LMAC_DATE_END,
        LMAC_USER,
        LMAC_CLIENT,
        LMAC_TEMPLT,
        pgp_sym_decrypt(LMAC_DESCRI::bytea,  p_pgcrypto_key) LMAC_DESCRI
        FROM CLI_TLMAC
        WHERE LMAC_LMAC=p_lmac_lmac;
        
  
    to_cl_lmac TYPE_TO_CL_LMAC;
    
BEGIN
  
    --Obtener el DLMAC por Id
    
    FOR j IN c_lmac
    LOOP

        to_cl_lmac := ROW (j.LMAC_LMAC,j.LMAC_STATE,j.LMAC_FILE_TRG,j.LMAC_FILE_URL,j.LMAC_DATE_INIT,j.LMAC_DATE_END,j.LMAC_USER,j.LMAC_CLIENT,j.LMAC_TEMPLT,j.LMAC_DESCRI)::TYPE_TO_CL_LMAC;

    END LOOP;

    return to_cl_lmac;
 


END;

$BODY$;

--ALTER FUNCTION "cli_qDLMAC$getDLMACbyid"(text, text)
    
