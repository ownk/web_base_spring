-- =========================================================================================================================================================================
-- #VERSION:0000001001
-- =========================================================================================================================================================================
-- HISTORIAL DE CAMBIOS
--
-- Versión        GAP                Solicitud        Fecha        Realizó            Descripción
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- 1000                                             15/10/2019      ownk           Se crea funcion 
-- 1001                                             20/04/2020      ownk           Se ajusta funcion para generar id 
-- -----------    -------------    -------------    ----------    -------------    -----------------------------------------------------------------------------------------
-- =========================================================================================================================================================================


CREATE OR REPLACE FUNCTION "usr_quser$forgotpassword"
(
    p_prcs                 TEXT,
    p_otpp                 TEXT,
    p_email                text,
    OUT p_cod_rpta         text,
    OUT p_errores          type_tt_ge_erro,
    OUT p_mensajes         type_tt_ge_mnsj
)
RETURNS record
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$

DECLARE

    to_ge_erro TYPE_TO_GE_ERRO;
    tt_ge_erro TYPE_TT_GE_ERRO;


    v_crta_http_gral CHARACTER VARYING(3) := 'OK';
    v_mensaje TYPE_TT_GE_MNSJ;
    
    /*ini1001
    c_sotpp  CURSOR FOR                
    select nextval ('aut_sotpp');
    fin1001*/
    c_user CURSOR FOR
    select USER_USER FROM AUT_TUSER
    where  USER_HASH_EMAIL=md5(upper(p_email));

    v_nextval_otpp      NUMERIC;

    v_cod_mensaje       VARCHAR(100):='OK';
    
    v_otpp_otpp         VARCHAR(50);
    
    v_user_user         VARCHAR(50);
    

BEGIN
    
    IF (usr_quser$validateemail(p_email)='false')THEN
        v_cod_mensaje = 'ERR_EMAIL_INVAL';
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta(v_cod_mensaje, p_prcs, v_mensaje)
            INTO v_mensaje;
        p_mensajes := v_mensaje;
    END IF;
        
    IF(v_cod_mensaje='OK')THEN
        
        OPEN c_user;
        FETCH c_user INTO v_user_user;
        CLOSE c_user;
        
        
        /*ini 1001
        OPEN c_sotpp;
        FETCH c_sotpp INTO v_nextval_otpp;
        CLOSE c_sotpp;
        
        v_otpp_otpp := (TO_CHAR(CURRENT_DATE,'YYYYMMDD')||'-'||v_nextval_otpp);

        
        fin 1001*/
        
        select cast(uuid_generate_v4() as varchar) INTO v_otpp_otpp;--1001
        
        --Inactivar los OTTP Activos hasta el momento para el cambio de contraseña
        UPDATE AUT_TOTPP SET
            OTPP_STATE='INA'
        WHERE OTPP_USER=v_user_user;
        
        
        --Asignacion de OTP para cambio de contraseña
        INSERT INTO AUT_TOTPP (
                OTPP_OTPP,
                OTPP_OTP,
                OTPP_DATE_STAR,
                OTPP_DATE_END,
                OTPP_STATE,
                OTPP_USER
        )values(
                v_otpp_otpp,
                p_otpp,
                CURRENT_TIMESTAMP,
                CURRENT_TIMESTAMP + (1 * interval '1 hour'),
                'ACT',
                v_user_user        
        );
            
    END IF;
    IF(v_cod_mensaje = 'OK')THEN
        p_cod_rpta := v_cod_mensaje;
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;
        
        SELECT
            *
            FROM api_ge_qutils$generartypemensajerpta('OK'::TEXT, p_prcs, v_mensaje)
            INTO v_mensaje;
    p_mensajes := v_mensaje;
    ELSE
        p_cod_rpta := v_cod_mensaje;
        to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, p_cod_rpta, p_cod_rpta)::TYPE_TO_GE_ERRO;
        tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
        p_errores := tt_ge_erro;
    END IF;

    EXCEPTION

        WHEN others THEN

            p_cod_rpta := 'ERROR';
            to_ge_erro := ROW (CURRENT_TIMESTAMP, p_prcs, SQLSTATE, SQLERRM)::TYPE_TO_GE_ERRO;
            tt_ge_erro := ARRAY[to_ge_erro]::TYPE_TT_GE_ERRO;
            p_errores := tt_ge_erro;

            SELECT
                *
                FROM api_ge_qutils$generartypemensajerpta('ERR_WHEN_OTHERS'::TEXT, p_prcs, v_mensaje)
                INTO v_mensaje;

            p_mensajes := v_mensaje;

END;

$BODY$;

--ALTER FUNCTION "usr_quser$forgotpassword"(text, text, text)
    

